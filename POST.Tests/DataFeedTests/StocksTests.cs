﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Models;
using POST.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace POST.Tests.DataFeedTests {

	[TestClass]
	public class StocksTests {

		[TestMethod]
		public async Task StockInit() {
			StockByIdStore store = new StockByIdStore();

			List<StockInitMessage> stockInitMessages = new List<StockInitMessage>();
			bool connected = false;
			bool initialized = false;
			bool received = false;
			bool disconnected = false;

			DataFeedClient client = new DataFeedClient();

			using (CancellationTokenSource cancellationTokenSource = new CancellationTokenSource()) {
				cancellationTokenSource.Token.Register(() => {
					client.Dispose();
				});
				cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

				client.Connected += async sender => {
					connected = true;
					await sender.SendAsync(new DesktopModeRequest());
					await sender.SendAsync(new StockInitRequest());
					initialized = true;
				};
				client.DataReceived += (sender, arg) => {
					switch (arg.Message) {
						case StockInitMessage stockInitMessage:
							if (!store.GetState().ContainsKey(new StockId(stockInitMessage.StockCode, stockInitMessage.MarketType))) {
								stockInitMessages.Add(stockInitMessage);
							}
							break;
						case StockInitEndMessage stockInitEndMessage:
							if (!store.GetState().Any()) {
								store.Init(stockInitMessages);
							}
							stockInitMessages.Clear();
							received = true;

							// Disconnect
							client.Dispose();
							break;
					}
				};
				client.Disconnected += (sender, arg) => {
					disconnected = true;
				};

				await client.ConnectAsync();
			}

			Assert.IsTrue(connected);
			Assert.IsTrue(initialized);
			Assert.IsFalse(disconnected);

			if (!stockInitMessages.Any()) {
				Assert.IsTrue(received);
				IReadOnlyDictionary<StockId, Stock> stockById = store.GetState();
				Assert.IsTrue(stockById.Any());
			}
		}
	}
}
