﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Models;
using POST.Stores;
using POST.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace POST.Tests.DataFeedTests {

	[TestClass]
	public class IndicesTests {

		[TestMethod]
		public async Task InitializeFeed() {
			IndexByCodeStore store = new IndexByCodeStore();

			bool connected = false;
			bool initialized = false;
			bool received = false;
			bool disconnected = false;

			DataFeedClient client = new DataFeedClient();

			using (CancellationTokenSource cancellationTokenSource = new CancellationTokenSource()) {
				cancellationTokenSource.Token.Register(() => {
					client.Dispose();
				});
				cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(5));

				client.Connected += async sender => {
					connected = true;
					await sender.SendAsync(new DesktopModeRequest());
					await sender.SendAsync(new SubscribeAllIndexQuotesRequest());
					initialized = true;
				};
				client.DataReceived += (sender, arg) => {
					switch (arg.Message) {
						case IndexQuoteMessage indexQuoteMessage:
							received = true;
							store.Dispatch(indexQuoteMessage);

							// Disconnect
							client.Dispose();
							break;
					}
				};
				client.Disconnected += (sender, arg) => {
					disconnected = true;
				};

				await client.ConnectAsync();
			}

			Assert.IsTrue(connected);
			Assert.IsTrue(initialized);
			Assert.IsTrue(received);
			Assert.IsFalse(disconnected);

			IReadOnlyDictionary<string, Index> indexByCode = store.GetState();
			Assert.AreEqual(1, indexByCode.Count);
		}
	}
}
