﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Models;
using POST.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace POST.Tests.DataFeedTests {

	[TestClass]
	public class OrderBookTests {

		[TestMethod]
		public async Task OrderBookInit() {
			OrderBookStore store = new OrderBookStore();

			bool connected = false;
			bool requested = false;
			bool received = false;
			bool parseFailed = false;
			bool disconnected = false;

			DataFeedClient client = new DataFeedClient();

			StockId stockId = new StockId("ASII", MarketType.RG);

			using (CancellationTokenSource cancellationTokenSource = new CancellationTokenSource()) {
				cancellationTokenSource.Token.Register(() => {
					client.Dispose();
				});
				cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(30));

				client.Connected += async sender => {
					connected = true;
					bool sent = await sender.SendAsync(new SubscribeOrderBookRequest(stockId));
					requested = sent;
				};
				client.DataReceived += (sender, arg) => {
					switch (arg.Message) {
						case OrderBookMessage orderBookMessage:
							received = true;
							client.Dispose();
							break;
						case ServerRequestPingMessage serverRequestPingMessage:
							break;
						default:
							break;
					}
				};
				client.ParseFailed += (sender, arg) => {
					parseFailed = true;
				};
				client.Disconnected += (sender, arg) => {
					disconnected = true;
				};

				await client.ConnectAsync();
			}

			Assert.IsTrue(connected);
			Assert.IsTrue(requested);
			Assert.IsTrue(received);
			Assert.IsFalse(parseFailed);
			Assert.IsFalse(disconnected);
		}
	}
}
