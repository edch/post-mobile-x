﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.Models.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POST.Tests.PrimitiveTests {

	[TestClass]
	public class LotTests {

		[TestMethod]
		public void TestConstructor() {

			Lot lot = new Lot(
				lot: 31.5m,
				sharesPerLot: 500
			);

			Assert.AreEqual(31.5m, lot.Value);
		}

		[TestMethod]
		public void TestParameterlessConstructor() {

			// Parameterless constructor is not supported
			Lot lot = new Lot();

			// All operations should be banned
			Assert.ThrowsException<InvalidOperationException>(() => {
				decimal value = lot.Value;
			});

			Assert.ThrowsException<InvalidOperationException>(() => {
				Shares shares = lot;
			});

			Assert.ThrowsException<InvalidOperationException>(() => {
				int hashCode = lot.GetHashCode();
			});

			Assert.ThrowsException<InvalidOperationException>(() => {
				bool equals = lot.Equals(lot);
			});
		}

		[TestMethod]
		public void TestExplicitConversionToShares() {

			Lot lot = new Lot(
				lot: 31.5m,
				sharesPerLot: 500
			);

			Shares shares = lot.Shares;

			Assert.AreEqual(15_750m, shares.Value);
		}

		[TestMethod]
		public void TestImplicitConversionToShares() {

			Lot lot = new Lot(
				lot: 31.5m,
				sharesPerLot: 500
			);

			Shares shares = lot;

			Assert.AreEqual(15_750m, shares.Value);
		}

		[TestMethod]
		public void TestImplicitConversionToSharesWithFractions() {

			Lot lot = new Lot(
				lot: 31.555m,
				sharesPerLot: 500
			);

			Assert.ThrowsException<ArgumentException>(() => {
				Shares shares = lot; // 15777.5 shares, which is invalid
			});
		}

		[TestMethod]
		public void TestEquality() {

			HashSet<Lot> set = new HashSet<Lot>();

			Assert.IsTrue(set.Add(new Lot(31.5m, 500)));
			Assert.IsTrue(set.Add(new Lot(31.5m, 200)));
			Assert.IsTrue(set.Add(new Lot(32.8m, 500)));
			Assert.IsFalse(set.Add(new Lot(31.5m, 500)));
		}
	}
}
