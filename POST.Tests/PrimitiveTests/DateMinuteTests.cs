﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.Models.Primitives;
using System;

namespace POST.Tests.PrimitiveTests {

	[TestClass]
	public class DateMinuteTests {

		[TestMethod]
		public void TestConstructor() {

			DateTime dateTime = DateTime.Now;
			DateMinute minute = new DateMinute(dateTime);

			Assert.AreEqual(dateTime.AddTicks(-dateTime.Ticks % 600_000_000), minute.Value);
		}

		[TestMethod]
		public void TestParameterlessConstructor() {

			// Parameterless constructor is not supported
			DateMinute minute = new DateMinute();

			// All operations should be banned
			Assert.ThrowsException<InvalidOperationException>(() => {
				DateTime value = minute.Value;
			});

			Assert.ThrowsException<InvalidOperationException>(() => {
				DateTime value = minute;
			});

			Assert.ThrowsException<InvalidOperationException>(() => {
				int hashCode = minute.GetHashCode();
			});

			Assert.ThrowsException<InvalidOperationException>(() => {
				bool equals = minute.Equals(minute);
			});
		}

		[TestMethod]
		public void TestImplicitConversionToDateTime() {

			DateTime dateTime = DateTime.Now;
			DateMinute date = new DateMinute(dateTime);
			DateTime converted = date;

			Assert.AreEqual(dateTime.AddTicks(-dateTime.Ticks % 600_000_000), converted);
		}

		[TestMethod]
		public void TestImplicitConversionFromDateTime() {

			DateTime dateTime = new DateTime(2018, 12, 31, 18, 31, 11, 500);
			DateMinute date = dateTime;
			DateTime converted = date;

			Assert.AreNotEqual(dateTime, converted);
			Assert.AreEqual(date.Value, converted);
			Assert.AreEqual(dateTime.AddTicks(-dateTime.Ticks % 600_000_000), converted);
		}
	}
}
