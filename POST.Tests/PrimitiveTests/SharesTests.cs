﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.Models.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POST.Tests.PrimitiveTests {

	[TestClass]
	public class SharesTests {

		[TestMethod]
		public void TestConstructor() {

			Shares shares = new Shares(
				shares: 1_300,
				sharesPerLot: 500
			);

			Assert.AreEqual(1_300, shares.Value);
		}

		[TestMethod]
		public void TestConstructorWithFractions() {

			ArgumentException exc = Assert.ThrowsException<ArgumentException>(() => {
				Shares shares = new Shares(
					shares: 500.5m,
					sharesPerLot: 500
				);
			});

			Assert.AreEqual("shares", exc.ParamName);
		}

		[TestMethod]
		public void TestParameterlessConstructor() {

			// Parameterless constructor is not supported
			Shares shares = new Shares();

			// All operations should be banned
			Assert.ThrowsException<InvalidOperationException>(() => {
				decimal value = shares.Value;
			});

			Assert.ThrowsException<InvalidOperationException>(() => {
				Lot lot = shares;
			});

			Assert.ThrowsException<InvalidOperationException>(() => {
				int hashCode = shares.GetHashCode();
			});

			Assert.ThrowsException<InvalidOperationException>(() => {
				bool equals = shares.Equals(shares);
			});
		}

		[TestMethod]
		public void TestImplicitConversionToDecimal() {

			Shares shares = new Shares(
				shares: 1_300,
				sharesPerLot: 500
			);

			decimal value = shares;

			Assert.AreEqual(1_300, value);
		}

		[TestMethod]
		public void TestImplicitConversionToLot() {

			Shares shares = new Shares(
				shares: 1_300,
				sharesPerLot: 500
			);

			Lot lot = shares;

			Assert.AreEqual(2.6m, lot.Value);

			Shares convertedBack = lot;

			Assert.AreEqual(1_300, convertedBack.Value);
		}

		[TestMethod]
		public void TestExplicitConversionToLot() {

			Shares shares = new Shares(
				shares: 1_300,
				sharesPerLot: 500
			);

			Lot lot = shares.Lot;

			Assert.AreEqual(2.6m, lot.Value);

			Shares convertedBack = lot.Shares;

			Assert.AreEqual(1_300, convertedBack.Value);
		}
	}
}
