﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.Models.Primitives;
using System;

namespace POST.Tests.PrimitiveTests {

	[TestClass]
	public class DateTests {

		[TestMethod]
		public void TestConstructor() {

			DateTime dateTime = DateTime.Now;
			Date date = new Date(dateTime);

			Assert.AreEqual(dateTime.Date, date.Value);
		}

		[TestMethod]
		public void TestParameterlessConstructor() {

			// Parameterless constructor is not supported
			Date date = new Date();

			// All operations should be banned
			Assert.ThrowsException<InvalidOperationException>(() => {
				DateTime value = date.Value;
			});

			Assert.ThrowsException<InvalidOperationException>(() => {
				DateTime value = date;
			});

			Assert.ThrowsException<InvalidOperationException>(() => {
				int hashCode = date.GetHashCode();
			});

			Assert.ThrowsException<InvalidOperationException>(() => {
				bool equals = date.Equals(date);
			});
		}

		[TestMethod]
		public void TestImplicitConversionToDateTime() {

			DateTime dateTime = DateTime.Now;
			Date date = new Date(dateTime);
			DateTime converted = date;

			Assert.AreEqual(dateTime.Date, converted);
		}

		[TestMethod]
		public void TestImplicitConversionFromDateTime() {

			DateTime dateTime = new DateTime(2018, 12, 31, 18, 31, 11, 500);
			Date date = dateTime;
			DateTime converted = date;

			Assert.AreNotEqual(dateTime, converted);
			Assert.AreEqual(date.Value, converted);
			Assert.AreEqual(dateTime.Date, converted);
		}
	}
}
