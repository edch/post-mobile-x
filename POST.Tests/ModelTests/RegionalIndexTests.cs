﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.Models;
using System;
using System.Collections.Generic;

namespace POST.Tests.ModelTests {

	[TestClass]
	public class RegionalIndexTests {

		[TestMethod]
		public void TestConstructor() {

			RegionalIndex regionalIndex = new RegionalIndex(
				code: "NI225",
				typeOrRegion: "Nikkei 225",
				previous: 22_450.79m,
				open: 22_488.95m,
				high: 22_547.67m,
				low: 22_410.91m,
				last: 22_481.09m,
				volume: 100_000_000m
			);

			Assert.AreEqual("NI225", regionalIndex.Code);
			Assert.AreEqual("Nikkei 225", regionalIndex.TypeOrRegion);
			Assert.AreEqual(22_450.79m, regionalIndex.Previous);
			Assert.AreEqual(22_488.95m, regionalIndex.OHLC.Open);
			Assert.AreEqual(22_547.67m, regionalIndex.OHLC.High);
			Assert.AreEqual(22_410.91m, regionalIndex.OHLC.Low);
			Assert.AreEqual(22_481.09m, regionalIndex.OHLC.Close);
			Assert.AreEqual(100_000_000m, regionalIndex.Volume.Value);
		}

		[TestMethod]
		public void TestConstructorWithNullCode() {

			ArgumentNullException exc = Assert.ThrowsException<ArgumentNullException>(() => {
				RegionalIndex regionalIndex = new RegionalIndex(
					code: null,
					typeOrRegion: "Nikkei 225",
					previous: 22_450.79m,
					open: 22_488.95m,
					high: 22_547.67m,
					low: 22_410.91m,
					last: 22_481.09m,
					volume: 100_000_000m
				);
			});

			Assert.AreEqual("code", exc.ParamName);
		}

		[TestMethod]
		public void TestConstructorWithNullTypeOrRegion() {

			// We don't expect any exception for now
			RegionalIndex regionalIndex = new RegionalIndex(
				code: "NI225",
				typeOrRegion: null,
				previous: 22_450.79m,
				open: 22_488.95m,
				high: 22_547.67m,
				low: 22_410.91m,
				last: 22_481.09m,
				volume: 100_000_000m
			);
		}

		[TestMethod]
		public void TestUpdate() {

			RegionalIndex regionalIndex = new RegionalIndex(
				code: "NI225",
				typeOrRegion: "Nikkei 225",
				previous: 22_450.79m,
				open: 22_488.95m,
				high: 22_547.67m,
				low: 22_410.91m,
				last: 22_481.09m,
				volume: 100_000_000m
			);

			List<string> changedPropertyNames = new List<string>();

			regionalIndex.PropertyChanged += (sender, args) => {
				Assert.IsNotNull(sender);
				Assert.AreEqual(regionalIndex, sender);
				Assert.IsNotNull(args);
				Assert.IsNotNull(args.PropertyName);
				changedPropertyNames.Add(args.PropertyName);
			};

			regionalIndex.UpdateWith(
				previous: 22_450.79m,
				open: 22_488.95m,
				high: 22_547.67m,
				low: 22_410.91m,
				last: 22_481.09m,
				volume: 200_000_000m
			);

			Assert.IsFalse(changedPropertyNames.Contains(nameof(RegionalIndex.Previous)));
			Assert.IsFalse(changedPropertyNames.Contains(nameof(RegionalIndex.OHLC)));
			Assert.IsTrue(changedPropertyNames.Contains(nameof(RegionalIndex.Volume)));
		}
	}
}
