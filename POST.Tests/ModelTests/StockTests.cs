﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace POST.Tests.ModelTests {

	[TestClass]
	public class StockTests {

		[TestMethod]
		public void TestConstructor() {

			Stock stock = new Stock(
				code: "ABCD",
				marketType: MarketType.RG,
				name: "Alpha Beta Charlie Delta Tbk.",
				sectorId: 8,
				subSector: "Echo Foxtrot Golf Industry",
				isSyariah: false,
				isLQ45: true,
				sharesPerLot: 500,
				totalShares: 100_000_000_000_000,
				remarks: "",
				previous: 9_450
			);

			Assert.AreEqual("ABCD", stock.Id.Code);
			Assert.AreEqual(MarketType.RG, stock.Id.MarketType);
			Assert.AreEqual("Alpha Beta Charlie Delta Tbk.", stock.Name);
			Assert.AreEqual(8, stock.SectorId);
			Assert.AreEqual("Echo Foxtrot Golf Industry", stock.SubSector);
			Assert.AreEqual(false, stock.IsSyariah);
			Assert.AreEqual(true, stock.IsLQ45);
			Assert.AreEqual(500, stock.SharesPerLot);
			Assert.AreEqual(100_000_000_000_000, stock.TotalShares.Value.Value);
			Assert.AreEqual("", stock.Remarks);
			Assert.AreEqual(9_450, stock.Previous);
			Assert.AreEqual(null, stock.OHLC);
			Assert.AreEqual(null, stock.Magnitudes);
		}

		[TestMethod]
		public void TestConstructorWithNullCode() {

			ArgumentNullException exc = Assert.ThrowsException<ArgumentNullException>(() => {
				Stock stock = new Stock(
					code: null,
					marketType: MarketType.RG,
					name: "Alpha Beta Charlie Delta Tbk.",
					sectorId: 8,
					subSector: "Echo Foxtrot Golf Industry",
					isSyariah: false,
					isLQ45: true,
					sharesPerLot: 500,
					totalShares: 100_000_000_000_000,
					remarks: "",
					previous: 9_450
				);
			});

			Assert.AreEqual("code", exc.ParamName);
		}

		[TestMethod]
		public void TestConstructorWithNullName() {

			ArgumentNullException exc = Assert.ThrowsException<ArgumentNullException>(() => {
				Stock stock = new Stock(
					code: "ABCD",
					marketType: MarketType.RG,
					name: null,
					sectorId: 8,
					subSector: "Echo Foxtrot Golf Industry",
					isSyariah: false,
					isLQ45: true,
					sharesPerLot: 500,
					totalShares: 100_000_000_000_000,
					remarks: "",
					previous: 9_450
				);
			});

			Assert.AreEqual("name", exc.ParamName);
		}

		[TestMethod, Ignore]
		public void TestUpdateWithBasicInformation() {

			Stock stock = new Stock(
				code: "ABCD",
				marketType: MarketType.RG,
				name: "Alpha Beta Charlie Delta Tbk.",
				sectorId: 8,
				subSector: "Echo Foxtrot Golf Industry",
				isSyariah: false,
				isLQ45: true,
				sharesPerLot: 500,
				totalShares: 100_000_000_000_000,
				remarks: "",
				previous: 9_450
			);

			List<string> changedPropertyNames = new List<string>();

			//stock.PropertyChanged += (sender, args) => {
			//	Assert.IsNotNull(sender);
			//	Assert.AreEqual(stock, sender);
			//	Assert.IsNotNull(args);
			//	Assert.IsNotNull(args.PropertyName);
			//	changedPropertyNames.Add(args.PropertyName);
			//};

			stock.UpdateWithBasicInformation(
				name: "PT Alpha Beta Charlie Delta", // changed
				sectorId: 8,
				subSector: "Echo Foxtrot Golf Industry",
				isSyariah: true, // changed
				isLQ45: false, // changed
				sharesPerLot: 500,
				totalShares: 100_000_000_000_000,
				remarks: "",
				previous: 9_500 // changed
			);

			Assert.IsTrue(changedPropertyNames.Contains("Change"));
			Assert.IsTrue(changedPropertyNames.Contains("ChangePercent"));
			Assert.IsTrue(changedPropertyNames.Contains("ChangeForeground"));
			Assert.IsTrue(changedPropertyNames.Contains("ChangeBackground"));
			Assert.IsTrue(changedPropertyNames.Contains("ChangeGlyph"));

			changedPropertyNames.Remove("Change");
			changedPropertyNames.Remove("ChangePercent");
			changedPropertyNames.Remove("ChangeForeground");
			changedPropertyNames.Remove("ChangeBackground");
			changedPropertyNames.Remove("ChangeGlyph");

			Assert.AreEqual(12, changedPropertyNames.Count);
			Assert.AreEqual("Name", changedPropertyNames[0]);
			Assert.AreEqual("IsSyariah", changedPropertyNames[1]);
			Assert.AreEqual("IsLQ45", changedPropertyNames[2]);
			Assert.AreEqual("Previous", changedPropertyNames[3]);
			Assert.AreEqual("LastOrPrevious", changedPropertyNames[4]);
			Assert.AreEqual("BestBidPriceForeground", changedPropertyNames[5]);
			Assert.AreEqual("BestOfferPriceForeground", changedPropertyNames[6]);
			Assert.AreEqual("HasNoBestBidPrice", changedPropertyNames[7]);
			Assert.AreEqual("HasNoBestOfferPrice", changedPropertyNames[8]);
			Assert.AreEqual("Updated", changedPropertyNames[9]);
			Assert.AreEqual("HasTimestamp", changedPropertyNames[10]);
			Assert.AreEqual("Timestamp", changedPropertyNames[11]);

			changedPropertyNames.Clear();

			stock.UpdateWithBasicInformation(
				name: "PT Alpha Beta Charlie Delta",
				sectorId: 9, // changed
				subSector: "Echo Foxtrot Golf Services", // changed
				isSyariah: true,
				isLQ45: false,
				sharesPerLot: 100, // changed
				totalShares: 200_000_000_000_000, // changed
				remarks: "--", // changed
				previous: 9_500
			);

			Assert.AreEqual(8, changedPropertyNames.Count);
			Assert.AreEqual("SectorId", changedPropertyNames[0]);
			Assert.AreEqual("SubSector", changedPropertyNames[1]);
			Assert.AreEqual("SharesPerLot", changedPropertyNames[2]);
			Assert.AreEqual("TotalShares", changedPropertyNames[3]);
			Assert.AreEqual("Remarks", changedPropertyNames[4]);
			Assert.AreEqual("Updated", changedPropertyNames[5]);
			Assert.AreEqual("HasTimestamp", changedPropertyNames[6]);
			Assert.AreEqual("Timestamp", changedPropertyNames[7]);
		}
	}
}
