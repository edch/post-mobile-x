﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.Models;
using POST.Models.Primitives;
using System;
using System.Collections.Generic;

namespace POST.Tests.ModelTests {

	[TestClass]
	public class MagnitudesTests {

		[TestMethod]
		public void TestConstructor() {

			Magnitudes<Volume> magnitudes = new Magnitudes<Volume>(
				volume: 1_000,
				value: 1_000_000_000,
				frequency: 50
			);

			Assert.AreEqual(1_000, magnitudes.Volume);
			Assert.AreEqual(1_000_000_000, magnitudes.Value);
			Assert.AreEqual(50, magnitudes.Frequency);
		}

		//[TestMethod]
		public void TestConstructorWithInvalidFrequency() {

			ArgumentException exc = Assert.ThrowsException<ArgumentException>(() => {
				Magnitudes<Volume> magnitudes = new Magnitudes<Volume>(
					volume: 1_000,
					value: 1_000_000_000,
					frequency: 0
				);
			});

			Assert.AreEqual("frequency", exc.ParamName);
		}

		[TestMethod]
		public void TestEquality() {

			HashSet<Magnitudes<Volume>> set = new HashSet<Magnitudes<Volume>>();

			Assert.IsTrue(set.Add(new Magnitudes<Volume>(1_000, 1_000_000_000, 50)));
			Assert.IsTrue(set.Add(new Magnitudes<Volume>(1_000, 1_200_000_000, 50)));
			Assert.IsTrue(set.Add(new Magnitudes<Volume>(1_100, 1_000_000_000, 50)));
			Assert.IsTrue(set.Add(new Magnitudes<Volume>(1_000, 1_000_000_000, 40)));
			Assert.IsFalse(set.Add(new Magnitudes<Volume>(1_000, 1_000_000_000, 50)));
		}
	}
}
