﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.Models;
using System;
using System.Collections.Generic;

namespace POST.Tests.ModelTests {

	[TestClass]
	public class OHLCTests {

		[TestMethod]
		public void TestConstructor() {

			OHLC ohlc = new OHLC(
				open: 1_000,
				high: 1_050,
				low: 900,
				close: 950
			);

			Assert.AreEqual(1_000, ohlc.Open);
			Assert.AreEqual(1_050, ohlc.High);
			Assert.AreEqual(900, ohlc.Low);
			Assert.AreEqual(950, ohlc.Close);
		}

		//[TestMethod]
		public void TestConstructorWithInvalidHigh() {

			ArgumentException exc = Assert.ThrowsException<ArgumentException>(() => {
				OHLC ohlc = new OHLC(
					open: 1_050,
					high: 1_000,
					low: 900,
					close: 950
				);
			});

			Assert.AreEqual("high", exc.ParamName);
		}

		//[TestMethod]
		public void TestConstructorWithInvalidLow() {

			ArgumentException exc = Assert.ThrowsException<ArgumentException>(() => {
				OHLC ohlc = new OHLC(
					open: 1_000,
					high: 1_050,
					low: 950,
					close: 900
				);
			});

			Assert.AreEqual("low", exc.ParamName);
		}

		[TestMethod]
		public void TestEquality() {

			HashSet<OHLC> set = new HashSet<OHLC>();

			Assert.IsTrue(set.Add(new OHLC(2, 4, 1, 3)));
			Assert.IsTrue(set.Add(new OHLC(3, 4, 1, 2)));
			Assert.IsFalse(set.Add(new OHLC(2, 4, 1, 3)));
			Assert.IsFalse(set.Add(new OHLC(3, 4, 1, 2)));
		}
	}
}
