﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.DataSources;
using POST.DataSources.ApiResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace POST.Tests.ServicesTests {

	[TestClass]
	public class IntradaySparkLineTests {

		[TestMethod]
		public async Task TestClient() {
			IntradaySparkLineData data = await IntradaySparkLineClient.Default.GetSparkLineAsync("ASII", CancellationToken.None);
			Assert.IsNotNull(data);
			Assert.AreEqual("ASII", data.StockCode);
			Assert.IsNotNull(data.Data);
		}

		[TestMethod]
		public async Task TestPooledRequest() {
			Task<IntradaySparkLineData> asiiTask = IntradaySparkLineClient.Default.GetSparkLineAsync("ASII", CancellationToken.None);
			Task<IntradaySparkLineData> bbcaTask = IntradaySparkLineClient.Default.GetSparkLineAsync("BBCA", CancellationToken.None);
			Task<IntradaySparkLineData> tlkmTask = IntradaySparkLineClient.Default.GetSparkLineAsync("TLKM", CancellationToken.None);
			await Task.WhenAll(asiiTask, bbcaTask, tlkmTask);
			Assert.IsTrue(asiiTask.IsCompleted);
			Assert.IsTrue(bbcaTask.IsCompleted);
			Assert.IsTrue(tlkmTask.IsCompleted);
			Assert.IsNotNull(asiiTask.Result);
			Assert.IsNotNull(bbcaTask.Result);
			Assert.IsNotNull(tlkmTask.Result);
			Assert.AreEqual("ASII", asiiTask.Result.StockCode);
			Assert.AreEqual("BBCA", bbcaTask.Result.StockCode);
			Assert.AreEqual("TLKM", tlkmTask.Result.StockCode);
		}
	}
}
