﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.DataSources;
using POST.DataSources.AthenaMessages.Requests;
using POST.DataSources.AthenaMessages.Responses;
using POST.Tests.DevOps;
using System.Threading;
using System.Threading.Tasks;

namespace POST.Tests.MercuryTests {

	[TestClass]
	public class LoginTests {

		[TestInitialize]
		public void Initialize() {
			System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
		}

		public static async Task<AthenaClient> LoginAsync() {
			string userId, password;
			if (ProductionCheck.IsProductionReady()) {
				userId = "edward_it";
				password = "855586";
			} else if (ProductionCheck.IsDevelopmentReady()) {
				userId = "edward_it";
				password = "855586";
			} else {
				Assert.Inconclusive("Configuration is not purely set to either production or development.");
				return null;
			}
			LoginRequest loginRequest = new LoginRequest(userId, password);
			LoginResponse loginResponse = await new AthenaClient(userId).LoginAsync(loginRequest, CancellationToken.None);
			Assert.IsNotNull(loginResponse);
			Assert.AreEqual("Login Success", loginResponse.Message);

			return new AthenaClient(
				userId: userId
			);
		}

		[TestMethod]
		public async Task Login() {
			await LoginAsync();
		}

		[TestMethod]
		public async Task LoginUsingWrongPassword() {
			LoginRequest loginRequest = new LoginRequest("edward_it", "321677");
			LoginResponse loginResponse = await new AthenaClient("edward_it").LoginAsync(loginRequest, CancellationToken.None);
			Assert.IsNotNull(loginResponse);
			Assert.AreEqual("Invalid Password", loginResponse.Message);
		}

		[TestMethod]
		public async Task LoginUsingEmptyPassword() {
			LoginRequest loginRequest = new LoginRequest("edward_it", "");
			LoginResponse loginResponse = await new AthenaClient("edward_it").LoginAsync(loginRequest, CancellationToken.None);
			Assert.IsNotNull(loginResponse);
			Assert.AreEqual("Invalid Password", loginResponse.Message);
		}

		[TestMethod]
		public async Task LoginUsingUnregisteredUser() {
			LoginRequest loginRequest = new LoginRequest("edward_itu", "321670");
			LoginResponse loginResponse = await new AthenaClient("edward_itu").LoginAsync(loginRequest, CancellationToken.None);
			Assert.IsNotNull(loginResponse);
			Assert.AreEqual("Invalid User", loginResponse.Message);
		}

		[TestMethod]
		public async Task LoginUsingEmptyUser() {
			LoginRequest loginRequest = new LoginRequest("", "321670");
			LoginResponse loginResponse = await new AthenaClient("").LoginAsync(loginRequest, CancellationToken.None);
			Assert.IsNotNull(loginResponse);
			Assert.AreEqual("Invalid Parameters", loginResponse.Message);
		}

		[TestMethod]
		public async Task LoginUsingEmptyForm() {
			LoginRequest loginRequest = new LoginRequest("", "");
			LoginResponse loginResponse = await new AthenaClient("").LoginAsync(loginRequest, CancellationToken.None);
			Assert.IsNotNull(loginResponse);
			Assert.AreEqual("Invalid Parameters", loginResponse.Message);
		}
	}
}
