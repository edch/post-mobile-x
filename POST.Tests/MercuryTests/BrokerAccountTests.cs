﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.DataSources;
using POST.DataSources.AthenaMessages.Responses;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace POST.Tests.MercuryTests {

	[TestClass]
	public class BrokerAccountTests {

		[TestInitialize]
		public void Initialize() {
			System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
		}

		public static async Task<TradingAccount[]> GetTradingAccountsAsync(AthenaClient athenaClient) {
			Assert.IsNotNull(athenaClient);

			TradingAccount[] tradingAccounts = await athenaClient.GetTradingAccountsAsync(CancellationToken.None);
			Assert.IsNotNull(tradingAccounts);

			return tradingAccounts;
		}

		[TestMethod]
		public async Task GetTradingAccounts() {
			AthenaClient AthenaClient = await LoginTests.LoginAsync();
			await GetTradingAccountsAsync(AthenaClient);
		}

		public static async Task<AthenaClient> LoginPinAsync(AthenaClient AthenaClient, TradingAccount tradingAccount, string pin) {
			PinLoginResponse pinLoginResponse = await AthenaClient.PinLoginAsync(
				brokerAccountId: tradingAccount.BrokerAccountId,
				pin: pin,
				cancellationToken: CancellationToken.None
			);

			Assert.IsNotNull(pinLoginResponse);
			Assert.AreNotEqual(null, pinLoginResponse.Message);

			return new AthenaClient(
				userId: AthenaClient.UserId
			);
		}

		[TestMethod]
		public async Task LoginPin() {
			AthenaClient AthenaClient = await LoginTests.LoginAsync();
			TradingAccount[] tradingAccounts = await GetTradingAccountsAsync(AthenaClient);

			if (!tradingAccounts.Any()) Assert.Inconclusive("This account does not have any testable trading account.");
			TradingAccount tradingAccount = tradingAccounts.First(account => account.BrokerAccountId == "9146");

			await LoginPinAsync(AthenaClient, tradingAccount, "534006");
		}

		[TestMethod]
		public async Task GetCashBalance() {
			AthenaClient AthenaClient = await LoginTests.LoginAsync();
			TradingAccount[] tradingAccounts = await GetTradingAccountsAsync(AthenaClient);

			if (!tradingAccounts.Any()) Assert.Inconclusive("This account does not have any testable trading account.");
			TradingAccount tradingAccount = tradingAccounts.First(account => account.BrokerAccountId == "9146");

			AthenaClient = await LoginPinAsync(AthenaClient, tradingAccount, "534006");

			CashBalance[] cashBalance = await AthenaClient.GetCashBalanceAsync(tradingAccount.BrokerAccountId, CancellationToken.None);
			Assert.IsNotNull(cashBalance);
			Assert.AreNotEqual(0, cashBalance.Length);
		}

		[TestMethod]
		public async Task GetStockBalance() {
			AthenaClient AthenaClient = await LoginTests.LoginAsync();
			TradingAccount[] tradingAccounts = await GetTradingAccountsAsync(AthenaClient);

			if (!tradingAccounts.Any()) Assert.Inconclusive("This account does not have any testable trading account.");
			TradingAccount tradingAccount = tradingAccounts.First(account => account.BrokerAccountId == "9146");

			AthenaClient = await LoginPinAsync(AthenaClient, tradingAccount, "534006");

			PortfolioStock[] stockBalance = await AthenaClient.GetStockBalanceAsync(tradingAccount.BrokerAccountId, CancellationToken.None);
			Assert.IsNotNull(stockBalance);
		}

		[TestMethod]
		public async Task GetOrderList() {
			AthenaClient AthenaClient = await LoginTests.LoginAsync();
			TradingAccount[] tradingAccounts = await GetTradingAccountsAsync(AthenaClient);

			if (!tradingAccounts.Any()) Assert.Inconclusive("This account does not have any testable trading account.");
			TradingAccount tradingAccount = tradingAccounts.First(account => account.BrokerAccountId == "9146");

			AthenaClient = await LoginPinAsync(AthenaClient, tradingAccount, "534006");

			Order[] orders = await AthenaClient.GetOrdersAsync(tradingAccount.BrokerAccountId, CancellationToken.None);
			Assert.IsNotNull(orders);
		}

		[TestMethod]
		public async Task GetTradeList() {
			AthenaClient AthenaClient = await LoginTests.LoginAsync();
			TradingAccount[] tradingAccounts = await GetTradingAccountsAsync(AthenaClient);

			if (!tradingAccounts.Any()) Assert.Inconclusive("This account does not have any testable trading account.");
			TradingAccount tradingAccount = tradingAccounts.First(account => account.BrokerAccountId == "9146");

			AthenaClient = await LoginPinAsync(AthenaClient, tradingAccount, "534006");

			Trade[] trades = await AthenaClient.GetHistoryTradeAsync(tradingAccount.BrokerAccountId, CancellationToken.None);
			Assert.IsNotNull(trades);
		}

	}
}
