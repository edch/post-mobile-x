﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POST.DataSources;

namespace POST.Tests.DevOps {

	[TestClass]
	public class ProductionCheck {

		// TODO: Set production settings here
		public const string PRODUCTION_DATAFEED_HOST = "xxxx.post-pro.co.id";
		public const int PRODUCTION_DATAFEED_PORT = 1235;
		public const string PRODUCTION_MERCURY_URL = "https://202.78.195.3:8443/mercury/webresources";
		public const string PRODUCTION_MERCURY_SECRET = "4xteoPs312";
		public const string PRODUCTION_ATHENA_URL = "";
		public const string PRODUCTION_INTRADAY_SPARKLINE = "http://202.78.206.115:8080/chart/spark10min";
		public const string PRODUCTION_SPARKLINE = "http://202.78.206.115:8080/chart/sparkline";
		public const string PRODUCTION_FINANCIALS = "https://chart.post-pro.co.id/api/v1/finance/newFR";
		public const string PRODUCTION_NEWSFEED = "http://chart.post-pro.co.id:8080/api/v1/news";

		// Test settings
		public const string DEVELOPMENT_DATAFEED_HOST = "dev.post-pro.co.id";
		public const int DEVELOPMENT_DATAFEED_PORT = 1235;
		public const string DEVELOPMENT_MERCURY_URL = "https://202.72.223.162:8443/mercury/webresources";
		public const string DEVELOPMENT_MERCURY_SECRET = "con3ec7_xtos!";
		public const string DEVELOPMENT_ATHENA_URL = "http://202.158.55.172/mi2";
		public const string DEVELOPMENT_INTRADAY_SPARKLINE = "http://202.78.206.115:8080/chart/spark10min";
		public const string DEVELOPMENT_SPARKLINE = "http://202.78.206.115:8080/chart/sparkline";
		public const string DEVELOPMENT_FINANCIALS = "https://chart.post-pro.co.id/api/v1/finance/newFR";
		public const string DEVELOPMENT_NEWSFEED = "http://chart.post-pro.co.id:8080/api/v1/news";

		public static bool IsProductionReady() {
			if (PRODUCTION_DATAFEED_HOST != DataFeedClient.DEFAULT_HOST) return false;
			if (PRODUCTION_DATAFEED_PORT != DataFeedClient.DEFAULT_PORT) return false;
			if (PRODUCTION_ATHENA_URL != AthenaClient.BASE_URL) return false;
			if (PRODUCTION_INTRADAY_SPARKLINE != IntradaySparkLineClient.URL) return false;
			if (PRODUCTION_SPARKLINE != SparkLineClient.URL) return false;
			if (PRODUCTION_FINANCIALS != FinancialsClient.URL) return false;
			if (PRODUCTION_NEWSFEED != NewsClient.URL) return false;
			return true;
		}

		public static bool IsDevelopmentReady() {
			if (DEVELOPMENT_DATAFEED_HOST != DataFeedClient.DEFAULT_HOST) return false;
			if (DEVELOPMENT_DATAFEED_PORT != DataFeedClient.DEFAULT_PORT) return false;
			if (DEVELOPMENT_ATHENA_URL != AthenaClient.BASE_URL) return false;
			if (DEVELOPMENT_INTRADAY_SPARKLINE != IntradaySparkLineClient.URL) return false;
			if (DEVELOPMENT_SPARKLINE != SparkLineClient.URL) return false;
			if (DEVELOPMENT_FINANCIALS != FinancialsClient.URL) return false;
			if (DEVELOPMENT_NEWSFEED != NewsClient.URL) return false;
			return true;
		}

		[TestMethod]
		public void ProductionReady() {
			Assert.IsTrue(IsProductionReady() || IsDevelopmentReady());
			if (!IsProductionReady()) Assert.Inconclusive();
		}

		[TestMethod]
		public void DevelopmentReady() {
			Assert.IsTrue(IsProductionReady() || IsDevelopmentReady());
			if (!IsDevelopmentReady()) Assert.Inconclusive();
		}
	}
}
