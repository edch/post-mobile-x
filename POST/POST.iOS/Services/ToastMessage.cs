﻿using GlobalToast;
using POST.Services;

namespace POST.iOS.Services {
	public class ToastMessage : IToastMessage {
		public void Show(string message) {
			Toast.ShowToast(message);
		}
	}
}