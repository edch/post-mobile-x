﻿using System;
using System.Threading.Tasks;
using Foundation;
using POST.Services;
using UIKit;

namespace POST.iOS.Services {
	public class ShareDialog : IShareDialog {
		public async Task ShowAsync(string title, string message, string url) {
			if (title == null) throw new ArgumentNullException(nameof(title));

			NSObject[] items;
			if (url != null) {
				items = new NSObject[] {
					NSObject.FromObject(title),
					NSUrl.FromString(url)
				};
			} else {
				items = new NSObject[] {
					NSObject.FromObject(title),
					NSObject.FromObject(message)
				};
			}
			UIActivityViewController activityViewController = new UIActivityViewController(items, null);
			UIViewController viewController = GetVisibleViewController();

			NSString[] excludedActivityTypes = null;

			if (excludedActivityTypes != null && excludedActivityTypes.Length > 0)
				activityViewController.ExcludedActivityTypes = excludedActivityTypes;

			if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0)) {
				if (activityViewController.PopoverPresentationController != null) {
					activityViewController.PopoverPresentationController.SourceView = viewController.View;
				}
			}

			await viewController.PresentViewControllerAsync(activityViewController, true);
		}

		private UIViewController GetVisibleViewController() {
			var rootController = UIApplication.SharedApplication.KeyWindow.RootViewController;

			if (rootController.PresentedViewController == null)
				return rootController;

			if (rootController.PresentedViewController is UINavigationController) {
				return ((UINavigationController)rootController.PresentedViewController).TopViewController;
			}

			if (rootController.PresentedViewController is UITabBarController) {
				return ((UITabBarController)rootController.PresentedViewController).SelectedViewController;
			}

			return rootController.PresentedViewController;
		}
	}
}