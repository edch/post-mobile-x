﻿using Foundation;
using POST.Controls;
using POST.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(KernedLabel), typeof(KernedLabelRenderer))]
namespace POST.iOS.Renderers {

	public class KernedLabelRenderer : LabelRenderer {

		private static readonly NSString NSKERN = new NSString("NSKern");

		protected override void OnElementChanged(ElementChangedEventArgs<Label> e) {
			base.OnElementChanged(e);

			if (Control != null && Element is KernedLabel kernedLabel) {
				string text = Control.Text;
				var attributedString = new NSMutableAttributedString(text);
				var spacing = FromObject(kernedLabel.LetterSpacing * 10);
				var range = new NSRange(0, text.Length);
				attributedString.AddAttribute(NSKERN, spacing, range);
				Control.AttributedText = attributedString;
			}
		}
	}
}