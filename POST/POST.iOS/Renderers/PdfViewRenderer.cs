﻿using Foundation;
using POST.Controls;
using POST.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(PdfView), typeof(PdfViewRenderer))]
namespace POST.iOS.Renderers {

	public class PdfViewRenderer : ViewRenderer<PdfView, UIWebView> {

		protected override void OnElementChanged(ElementChangedEventArgs<PdfView> e) {
			base.OnElementChanged(e);

			if (Control == null) {
				SetNativeControl(new UIWebView());
			}
			if (e.OldElement != null) {
				// Cleanup
			}
			if (e.NewElement != null) {
				PdfView pdfView = Element as PdfView;
				Control.LoadRequest(new NSUrlRequest(new NSUrl(pdfView.Uri, false)));
				Control.ScalesPageToFit = true;
			}
		}
	}
}