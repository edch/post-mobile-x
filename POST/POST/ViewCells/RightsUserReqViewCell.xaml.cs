﻿using POST.DataSources.RightsMessages.Responses;
using POST.Dialogs;
using Rg.Plugins.Popup.Services;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Behaviors;
using XLabs.Forms.Controls;

namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RightsUserReqViewCell : ViewCell
    {
        public RightsUserReqViewCell()
        {
            InitializeComponent();
        }

        private async void GesturesContentView_GestureRecognized(object sender, GestureResult e)
        {
            if (BindingContext is RightsUserReqItem rightsUserReqItem)
            {
                switch (e.GestureType)
                {
                    case GestureType.SingleTap:
                        {
                            try
                            {
                                //var strString = rightsUserReqItem.rightid.Substring(0, 4);
                                //if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                                //            stockById.TryGetValue(new StockId(strString, MarketType.RG), out Stock stock)
                                //         )
                                //{
                                //    if (stock != null) {
                                //        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) await PopupNavigation.Instance.PopAllAsync();
                                //        RightsWthdrawalDetailDialog rightsWthdrawalDetailDialog = new RightsWthdrawalDetailDialog(rightsUserReqItem,stock.SharesPerLot.Value);
                                //        await PopupNavigation.Instance.PushAsync(rightsWthdrawalDetailDialog);
                                //    }
                                //    else
                                //    {
                                //        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) await PopupNavigation.Instance.PopAllAsync();
                                //        RightsWthdrawalDetailDialog rightsWthdrawalDetailDialog = new RightsWthdrawalDetailDialog(rightsUserReqItem,100);
                                //        await PopupNavigation.Instance.PushAsync(rightsWthdrawalDetailDialog);
                                //    }
                                //}
                                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                {
                                    await PopupNavigation.Instance.PopAllAsync();
                                }

                                RightsWthdrawalDetailDialog rightsWthdrawalDetailDialog = new RightsWthdrawalDetailDialog(rightsUserReqItem, 100);
                                await PopupNavigation.Instance.PushAsync(rightsWthdrawalDetailDialog);
                            }
                            catch
                            {

                            }

                            break;
                        }
                    default:
                        break;
                }
            }
        }
    }
}