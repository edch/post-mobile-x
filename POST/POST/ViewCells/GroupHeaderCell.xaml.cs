﻿using POST.Models.Collections;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GroupHeaderCell : ViewCell
    {

        public GroupHeaderCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (BindingContext is IGroup group)
            {
                SeeAllButton.IsVisible = group.GroupPageType != null;
            }
        }

        private async void SeeAllButton_Clicked(object sender, EventArgs e)
        {
            if (BindingContext is IGroup group &&
                group.GroupPageType is Type groupPageType)
            {
                Page page = (Page)Activator.CreateInstance(groupPageType);
                await MainPage.NavigationPage.PushAsync(page);
            }
        }
    }
}