﻿using POST.Models.Primitives;

namespace POST.ViewCells
{
    public class OrderButtonEventArgs
    {
        public Price Price { get; }
        public OrderButtonEventArgs(Price price)
        {
            Price = price;
        }
    }
}
