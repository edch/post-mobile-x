﻿using POST.Constants;
using POST.Converters;
using POST.Models;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells.OrderBookView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TradeBookViewCell : ViewCell
    {
        public TradeBookViewCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            if (BindingContext is TradeBookLevel tradeBookLevel)
            {
                try
                {
                    OrderOfMagnitudeConverter orderOfMagnitudeConverter = new OrderOfMagnitudeConverter();

                    lblPrice.Text = tradeBookLevel.Price.ToString("N0");
                    lblVol.Text = orderOfMagnitudeConverter.Convert(tradeBookLevel.Volume, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                    lblFreq.Text = orderOfMagnitudeConverter.Convert(tradeBookLevel.Frequency, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();

                    if (Store.TradeBook.GetState().Previous.HasValue)
                    {
                        decimal prev = Store.TradeBook.GetState().Previous.Value;
                        if (tradeBookLevel.Price > prev)
                        {
                            lblPrice.TextColor = Colors.GainForeground;
                            lblVol.TextColor = Colors.GainForeground;
                            lblFreq.TextColor = Colors.GainForeground;
                        }
                        else if (tradeBookLevel.Price < prev)
                        {
                            lblPrice.TextColor = Colors.LoseForeground;
                            lblVol.TextColor = Colors.LoseForeground;
                            lblFreq.TextColor = Colors.LoseForeground;
                        }
                        else
                        {
                            lblPrice.TextColor = Colors.UnchangedForeground;
                            lblVol.TextColor = Colors.UnchangedForeground;
                            lblFreq.TextColor = Colors.UnchangedForeground;
                        }
                    }
                    else
                    {
                        lblPrice.TextColor = Colors.FieldValueDefaultColor;
                        lblVol.TextColor = Colors.FieldValueDefaultColor;
                        lblFreq.TextColor = Colors.FieldValueDefaultColor;
                    }

                }
                catch (Exception)
                {
                }
            }

            base.OnAppearing();
        }
    }
}