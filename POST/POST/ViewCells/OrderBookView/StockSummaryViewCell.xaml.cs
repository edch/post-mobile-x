﻿using POST.Converters;
using POST.Models;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells.OrderBookView
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StockSummaryViewCell : ViewCell
    {
        public StockSummaryViewCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            if (BindingContext is StockSummaryLevel stockSummaryLevel)
            {
                try
                {
                    lblBrokerCode.Text = stockSummaryLevel.BrokerCode;

                    lblBrokerCode.TextColor = stockSummaryLevel.Broker_Foreground;

                    OrderOfMagnitudeConverter orderOfMagnitudeConverter = new OrderOfMagnitudeConverter();

                    lblBVol.Text = orderOfMagnitudeConverter.Convert(stockSummaryLevel.Total_Buy_Volume, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();

                    lblSVol.Text = orderOfMagnitudeConverter.Convert(stockSummaryLevel.Total_Sell_Volume, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();

                    lblNet.Text = orderOfMagnitudeConverter.Convert(stockSummaryLevel.Net_Volume, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                    lblNet.TextColor = stockSummaryLevel.Net_Volume_Foreground;
                }
                catch (Exception)
                {
                }
            }

            base.OnAppearing();
        }
    }


}