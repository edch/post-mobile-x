﻿using POST.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells.RunningTradeFilter
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RunningTradeStockFilterCell : ViewCell
    {
        private readonly string strGroupName = "";

        public RunningTradeStockFilterCell(string strGroupName)
        {
            InitializeComponent();
            this.strGroupName = strGroupName;

            gridRemoveStock.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    if (BindingContext is Stock stock)
                    {
                        if (Store.CustomRunningTradeFilterStockIdsStore.GetState().Keys.Count > 0)
                        {
                            bool AnswerYes = await Application.Current.MainPage.DisplayAlert("Clear Confirmation", "Are you sure want to remove " + stock.Id.Code + "?", "Ok", "Cancel");
                            if (!AnswerYes)
                            {
                                return;
                            }
                            Store.CustomRunningTradeFilterStockIdsStore.RemoveStock(strGroupName, stock.Id.Code);
                        }
                    }
                })
            });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (BindingContext is Stock stock)
            {
                lblStockCode.TextDecorations = stock.TextDecoration;
                lblStockName.TextDecorations = stock.TextDecoration;
            }
        }
    }
}