﻿using POST.Dialogs;
using POST.Pages.RunningTradeFilterPages;
using Rg.Plugins.Popup.Services;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells.RunningTradeFilter
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FilterGroupCell : ViewCell
    {
        public FilterGroupCell()
        {
            InitializeComponent();

            gridGroupName.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    if (BindingContext is string strGroupName)
                    {
                        StockFilterPage stockFilterPage = new StockFilterPage(strGroupName)
                        {
                            Title = strGroupName
                        };
                        await MainPage.NavigationPage.PushAsync(stockFilterPage);
                    }
                })
            });

            gridRemoveGroup.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    if (BindingContext is string strGroupName)
                    {
                        if (Store.CustomRunningTradeFilterStockIdsStore.GetState().Keys.Count > 0)
                        {
                            bool AnswerYes = await Application.Current.MainPage.DisplayAlert("Clear Confirmation", "Are you sure want to remove " + strGroupName + "?", "Ok", "Cancel");
                            if (!AnswerYes)
                            {
                                return;
                            }
                            Store.CustomRunningTradeFilterStockIdsStore.RemoveGroup(strGroupName);
                        }
                    }
                })
            });

            gridEditGroup.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    if (BindingContext is string strGroupName)
                    {
                        RenameFilterGroupDialog renameFilterGroupDialog = new RenameFilterGroupDialog(strGroupName);
                        await PopupNavigation.Instance.PushAsync(renameFilterGroupDialog);
                    }
                })
            });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (BindingContext is string strGroupName)
            {
                int nStocks = Store.CustomRunningTradeFilterStockIdsStore.GetState()[strGroupName].Count();
                if (nStocks > 0)
                {
                    lblStockNumber.Text = Store.CustomRunningTradeFilterStockIdsStore.GetState()[strGroupName].Count().ToString() + " Stocks";
                }
                else
                {
                    lblStockNumber.Text = "0 Stock";
                }
            }
        }
    }


}