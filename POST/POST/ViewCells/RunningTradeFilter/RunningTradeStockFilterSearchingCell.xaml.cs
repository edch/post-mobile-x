﻿using POST.Constants;
using POST.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells.RunningTradeFilter
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RunningTradeStockFilterSearchingCell : ViewCell
    {
        private bool isChecked = false;

        private readonly string strGroupName = "";

        public RunningTradeStockFilterSearchingCell(string strGroupName)
        {
            InitializeComponent();
            this.strGroupName = strGroupName;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (BindingContext is Stock stock)
            {
                IsChecked = Store.CustomRunningTradeFilterStockIdsStore.GetState().isAnyStock(strGroupName, stock.Id.Code) ? true : false;

                lblStockCode.TextDecorations = stock.TextDecoration;
                lblStockName.TextDecorations = stock.TextDecoration;
            }
        }

        protected override void OnTapped()
        {
            base.OnTapped();

            if (BindingContext is Stock stock)
            {
                if (Store.CustomRunningTradeFilterStockIdsStore.GetState().isAnyStock(strGroupName, stock.Id.Code))
                {
                    Store.CustomRunningTradeFilterStockIdsStore.RemoveStock(strGroupName, stock.Id.Code);
                    IsChecked = false;
                }
                else
                {
                    if (Store.CustomRunningTradeFilterStockIdsStore.GetState()[strGroupName].Count < 20)
                    {
                        Store.CustomRunningTradeFilterStockIdsStore.AddStock(strGroupName, stock.Id.Code);
                        IsChecked = true;
                    }
                    else
                    {
                        Application.Current.MainPage.DisplayAlert("Error", "Maximum is 20 Stocks.", "Ok");
                    }
                }
            }
        }

        public bool IsChecked
        {
            get => isChecked;
            set
            {
                isChecked = value;
                lblChecked.Text = isChecked ? "\uf00c" : "\uf067";
                lblChecked.TextColor = isChecked ? Colors.Accent : Colors.FieldNameColor;
            }
        }
    }
}