﻿using POST.Constants;
using POST.Converters;
using POST.Models;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Behaviors;
using XLabs.Forms.Controls;

namespace POST.ViewCells
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompactStockCell : ViewCell
    {
        public event StockCellEventHandler StockCellSingleTap;
        public event StockCellEventHandler StockCellLongPress;

        private bool isBlinking = false;

        public CompactStockCell()
        {
            InitializeComponent();
        }

        public CompactStockCell(StockCellEventHandler StockCellSingleTap, StockCellEventHandler StockCellLongPress)
        {
            InitializeComponent();
            this.StockCellSingleTap += StockCellSingleTap;
            this.StockCellLongPress += StockCellLongPress;
        }

        private async void GesturesContentView_GestureRecognized(object sender, GestureResult e)
        {
            if (BindingContext is Stock stock)
            {
                switch (e.GestureType)
                {
                    case GestureType.SingleTap:
                        StockCellSingleTap?.Invoke(this, new StockCellEventArgs(stock));

                        break;
                    case GestureType.LongPress:
                        StockCellLongPress?.Invoke(this, new StockCellEventArgs(stock));

                        break;
                    default:
                        break;
                }
            }
        }

        protected override void OnAppearing()
        {
            if (BindingContext is Stock stock)
            {
                try
                {
                    //ChartIntradaySparkLine.StockCode = stock.Id.Code;

                    lblStockCode.TextColor = stock.StockCodeForeground;

                    if (stock.IsInitialized)
                    {
                        lblPrevious.Text = stock.Previous.HasValue ? stock.Previous.Value.ToString("N0") : "";
                    }

                    if (stock.HasMagnitudes)
                    {
                        OrderOfMagnitudeConverter orderOfMagnitudeConverter = new OrderOfMagnitudeConverter();
                        lblVol.Text = orderOfMagnitudeConverter.Convert(stock.Magnitudes.Volume.Value, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblFreq.Text = orderOfMagnitudeConverter.Convert(stock.Magnitudes.Frequency, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();

                        orderOfMagnitudeConverter = null;
                    }
                    else
                    {
                        lblVol.Text = "";
                        lblFreq.Text = "";
                    }

                    if (lblStockCode.Text == stock.Id.Code)
                    {
                        if (decimal.TryParse(lblLastOrPrevious.Text, out decimal oldPrice))
                        {
                            if (stock.LastOrPrevious.Value > oldPrice)
                            {
                                blinkPriceBackgroundAsync(Colors.GainBackground);
                            }
                            else if (stock.LastOrPrevious.Value < oldPrice)
                            {
                                blinkPriceBackgroundAsync(Colors.LoseBackground);
                            }
                        }
                    }
                    else
                    {
                        lblStockCode.Text = stock.Id.Code;
                        SetPriceLabelForeground();
                    }

                    //Syariah
                    lblStockCode.TextDecorations = stock.TextDecoration;
                }
                catch (Exception)
                {
                }
            }

            base.OnAppearing();
        }

        private void SetPriceLabelForeground()
        {
            Stock stock = (Stock)BindingContext;

            if (stock.LastOrPrevious.HasValue)
            {
                lblLastOrPrevious.Text = stock.LastOrPrevious.Value.ToString("N0");
                lblLastOrPrevious.TextColor = stock.ChangeForeground;
            }
            else
            {
                lblLastOrPrevious.Text = "";
            }

            if (stock.ChangePercent.HasValue)
            {
                lblChangePercent.Text = stock.ChangePercent.Value.ToString("+0.00\\%;-0.00\\%;0.00\\%");
                lblChangePercent.TextColor = stock.ChangeForeground;
            }
            else
            {
                lblChangePercent.Text = "";
            }

            if (stock.Change.HasValue)
            {
                lblChangePoint.Text = stock.Change.Value.ToString("+#,##0;-#,##0;+0");
                lblChangePoint.TextColor = stock.ChangeForeground;
            }
            else
            {
                lblChangePoint.Text = "";
            }
        }

        private void SetPriceLabelForegroundToWhite()
        {
            lblLastOrPrevious.TextColor = Colors.FieldValueDefaultColor;
        }

        private async void blinkPriceBackgroundAsync(Color colorBackground)
        {
            if (isBlinking)
            {
                return;
            }

            isBlinking = true;
            SetPriceLabelForeground();
            SetPriceLabelForegroundToWhite();
            frameHighlight.BackgroundColor = colorBackground;
            frameHighlight.IsVisible = true;
            await Task.Delay(500);
            frameHighlight.IsVisible = false;
            SetPriceLabelForeground();
            isBlinking = false;
        }
    }


}