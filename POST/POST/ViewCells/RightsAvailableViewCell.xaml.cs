﻿using POST.DataSources.RightsMessages.Responses;
using POST.Dialogs;
using POST.Models;
using POST.Services;
using Rg.Plugins.Popup.Services;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Behaviors;
using XLabs.Forms.Controls;

namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RightsAvailableViewCell : ViewCell
    {
        public RightsAvailableViewCell()
        {
            InitializeComponent();
        }

        private async void GesturesContentView_GestureRecognized(object sender, GestureResult e)
        {
            if (BindingContext is RightsAvailableItem rightsAvailableItem)
            {
                switch (e.GestureType)
                {
                    case GestureType.SingleTap:
                        {
                            if (rightsAvailableItem.sstatus == "OPEN")
                            {
                                //if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                                //    stockById.TryGetValue(new StockId(rightsAvailableItem.stockId, MarketType.RG), out Stock stock)
                                // )
                                //{
                                //    RightsRequestDialog rightsRequestDialog = new RightsRequestDialog(rightsAvailableItem, stock.Id, stock.SharesPerLot.Value);
                                //    await PopupNavigation.Instance.PushAsync(rightsRequestDialog);
                                //}

                                RightsRequestDialog rightsRequestDialog = new RightsRequestDialog(rightsAvailableItem, new StockId(rightsAvailableItem.stockId, MarketType.RG), 100);
                                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                {
                                    await PopupNavigation.Instance.PopAllAsync();
                                }

                                await PopupNavigation.Instance.PushAsync(rightsRequestDialog);
                            }
                            else
                            {
                                DependencyService.Get<IToastMessage>().Show("Rights Status is not Open!");
                            }

                            break;
                        }
                    default:
                        break;
                }
            }
        }
    }


}