﻿using POST.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RunningTradeCell : ViewCell
    {
        public RunningTradeCell()
        {
            InitializeComponent();
        }

        private void Grid_SizeChanged(object sender, System.EventArgs e)
        {
            if (sender is Grid grid)
            {
                double width = grid.Width;
                Column0.Width = width < 320 ? new GridLength(0) : (width < 560 ? new GridLength(60) : new GridLength(1.8, GridUnitType.Star));
                Column3.Width = width < 500 ? new GridLength(3, GridUnitType.Star) : new GridLength(80);
                Column4.Width = width < 500 ? new GridLength(3, GridUnitType.Star) : new GridLength(80);
                Column6.Width = width < 380 ? new GridLength(0) : (width < 560 ? new GridLength(30) : new GridLength(0.9, GridUnitType.Star));
                Column7.Width = width < 380 ? new GridLength(0) : (width < 560 ? new GridLength(30) : new GridLength(0.9, GridUnitType.Star));
                lblTime.IsVisible = width >= 320;
                lblBuyerCode.IsVisible = width >= 380;
                lblSellerCode.IsVisible = width >= 380;
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (BindingContext is Trade trade)
            {
                if (trade.StockCode == "")
                {
                    return;
                }
                lblTime.Text = trade.TimeString;
                lblTime.TextColor = trade.Foreground;
                lblStockCode.Text = trade.StockCode;
                lblStockCode.TextColor = trade.Foreground;
                if (trade.NonRGMarketType.HasValue)
                {
                    lblNonRGMarketType.Text = trade.NonRGMarketType.Value.ToString();
                }
                else
                {
                    lblNonRGMarketType.Text = "";
                }
                lblPrice.Text = trade.PriceString;
                lblPrice.TextColor = trade.ChangeForeground;
                lblChangePoin.Text = trade.ChangeString;
                lblChangePoin.TextColor = trade.ChangeForeground;
                lblChangePercent.Text = trade.ChangePercent;
                lblChangePercent.TextColor = trade.ChangeForeground;
                lblLot.Text = trade.LotString;
                lblLot.TextColor = trade.Foreground;
                lblBuyerCode.Text = trade.BuyerCode;
                lblBuyerCode.TextColor = trade.BuyerColor;
                lblSellerCode.Text = trade.SellerCode;
                lblSellerCode.TextColor = trade.SellerColor;

                //Syariah
                if (Store.StockById.GetState().TryGetValue(new StockId(trade.StockCode, MarketType.RG), out Stock stock))
                {
                    lblStockCode.TextDecorations = stock.TextDecoration;
                }
            }
        }
    }
}