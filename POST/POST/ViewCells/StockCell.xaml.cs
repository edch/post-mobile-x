﻿using POST.Constants;
using POST.Converters;
using POST.Models;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Behaviors;
using XLabs.Forms.Controls;

namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StockCell : ViewCell
    {
        public event StockCellEventHandler StockCellSingleTap;
        public event StockCellEventHandler StockCellLongPress;

        private bool isBlinking = false;

        public StockCell()
        {
            InitializeComponent();
        }

        public StockCell(StockCellEventHandler StockCellSingleTap, StockCellEventHandler StockCellLongPress)
        {
            InitializeComponent();
            this.StockCellSingleTap += StockCellSingleTap;
            this.StockCellLongPress += StockCellLongPress;
        }

        private async void GesturesContentView_GestureRecognized(object sender, GestureResult e)
        {
            if (BindingContext is Stock stock)
            {
                switch (e.GestureType)
                {
                    case GestureType.SingleTap:
                        StockCellSingleTap?.Invoke(this, new StockCellEventArgs(stock));

                        break;
                    case GestureType.LongPress:
                        StockCellLongPress?.Invoke(this, new StockCellEventArgs(stock));

                        break;
                    default:
                        break;

                }
            }

        }

        protected override void OnAppearing()
        {
            if (BindingContext is Stock stock)
            {
                try
                {
                    gridChangeForeground.BackgroundColor = stock.ChangeForeground;

                    lblStockCode.TextColor = stock.StockCodeForeground;

                    if (stock.HasMagnitudes)
                    {
                        lblVolNull.IsVisible = false;
                        lblValNull.IsVisible = false;
                        lblVolData.IsVisible = true;
                        lblValData.IsVisible = true;

                        OrderOfMagnitudeConverter orderOfMagnitudeConverter = new OrderOfMagnitudeConverter();
                        lblVolData.Text = "Vol: " + orderOfMagnitudeConverter.Convert(stock.Magnitudes.Volume.Value, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblValData.Text = "Val: " + orderOfMagnitudeConverter.Convert(stock.Magnitudes.Value, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        orderOfMagnitudeConverter = null;
                    }
                    else
                    {
                        lblVolNull.IsVisible = true;
                        lblValNull.IsVisible = true;
                        lblVolData.IsVisible = false;
                        lblValData.IsVisible = false;
                    }

                    if (ChartIntradaySparkLine.StockCode == stock.Id.Code)
                    {
                        ChartIntradaySparkLine.Refresh();
                    }
                    else
                    {
                        ChartIntradaySparkLine.StockCode = stock.Id.Code;
                        frameHighlight.IsVisible = false;
                    }

                    if (stock.IsInitialized)
                    {
                        lblPrev.IsVisible = true;
                        FormattedString formattedString = new FormattedString();
                        formattedString.Spans.Add(new Span { Text = "Prev: ", ForegroundColor = Colors.FieldValueDefaultColor });
                        formattedString.Spans.Add(new Span { Text = stock.Previous.Value.ToString("N0"), ForegroundColor = Colors.UnchangedForeground });
                        lblPrev.FormattedText = formattedString;
                        formattedString = null;
                    }
                    else
                    {
                        lblPrev.IsVisible = false;
                    }

                    if (stock.OHLC.HasValue)
                    {
                        lblOpenNull.IsVisible = false;
                        lblOpenData.IsVisible = true;
                        lblHighNull.IsVisible = false;
                        lblHighData.IsVisible = true;
                        lblLowNull.IsVisible = false;
                        lblLowData.IsVisible = true;

                        FormattedString formattedString = new FormattedString();
                        formattedString.Spans.Add(new Span { Text = "Open: ", ForegroundColor = Colors.FieldValueDefaultColor });
                        formattedString.Spans.Add(new Span { Text = stock.OHLC.Value.Open.ToString("N0"), ForegroundColor = stock.OpenPriceForeground });
                        lblOpenData.FormattedText = formattedString;
                        formattedString = null;

                        formattedString = new FormattedString();
                        formattedString.Spans.Add(new Span { Text = "High: ", ForegroundColor = Colors.FieldValueDefaultColor });
                        formattedString.Spans.Add(new Span { Text = stock.OHLC.Value.High.ToString("N0"), ForegroundColor = stock.HighPriceForeground });
                        lblHighData.FormattedText = formattedString;
                        formattedString = null;

                        formattedString = new FormattedString();
                        formattedString.Spans.Add(new Span { Text = "Low: ", ForegroundColor = Colors.FieldValueDefaultColor });
                        formattedString.Spans.Add(new Span { Text = stock.OHLC.Value.Low.ToString("N0"), ForegroundColor = stock.LowPriceForeground });
                        lblLowData.FormattedText = formattedString;
                        formattedString = null;

                    }
                    else
                    {
                        lblOpenNull.IsVisible = true;
                        lblOpenData.IsVisible = false;
                        lblHighNull.IsVisible = true;
                        lblHighData.IsVisible = false;
                        lblLowNull.IsVisible = true;
                        lblLowData.IsVisible = false;
                    }

                    if (stock.HasBestBidPrice)
                    {
                        lblBidNull.IsVisible = false;
                        lblBidData.IsVisible = true;
                        FormattedString formattedString = new FormattedString();
                        formattedString.Spans.Add(new Span { Text = "Bid: ", ForegroundColor = Colors.FieldValueDefaultColor });
                        formattedString.Spans.Add(new Span { Text = stock.BestBidPrice.Value.ToString("N0"), ForegroundColor = stock.BestBidPriceForeground });
                        lblBidData.FormattedText = formattedString;
                        formattedString = null;
                    }
                    else
                    {
                        lblBidNull.IsVisible = true;
                        lblBidData.IsVisible = false;
                    }

                    if (stock.HasBestOfferPrice)
                    {
                        lblAskNull.IsVisible = false;
                        lblAskData.IsVisible = true;

                        FormattedString formattedString = new FormattedString();
                        formattedString.Spans.Add(new Span { Text = "Ask: ", ForegroundColor = Colors.FieldValueDefaultColor });
                        formattedString.Spans.Add(new Span { Text = stock.BestOfferPrice.Value.ToString("N0"), ForegroundColor = stock.BestOfferPriceForeground });
                        lblAskData.FormattedText = formattedString;
                        formattedString = null;
                    }
                    else
                    {
                        lblAskNull.IsVisible = true;
                        lblAskData.IsVisible = false;
                    }

                    if (lblStockCode.Text == stock.Id.Code)
                    {

                        if (decimal.TryParse(lblLastOrPrevious.Text, out decimal oldPrice))
                        {
                            if (stock.LastOrPrevious.Value > oldPrice)
                            {
                                blinkPriceBackgroundAsync(Colors.GainBackground);
                            }
                            else if (stock.LastOrPrevious.Value < oldPrice)
                            {
                                blinkPriceBackgroundAsync(Colors.LoseBackground);
                            }
                        }
                        else
                        {
                            oldPrice = 0;
                            if (stock.LastOrPrevious.Value > oldPrice)
                            {
                                blinkPriceBackgroundAsync(Colors.GainBackground);
                            }
                            else if (stock.LastOrPrevious.Value < oldPrice)
                            {
                                blinkPriceBackgroundAsync(Colors.LoseBackground);
                            }
                        }

                    }
                    else
                    {
                        lblStockCode.Text = stock.Id.Code;
                        SetPriceLabelForeground();
                    }

                    //Syariah
                    lblStockCode.TextDecorations = stock.TextDecoration;
                }
                catch (Exception)
                {
                }
            }

            base.OnAppearing();
        }

        private void SetPriceLabelForeground()
        {
            Stock stock = (Stock)BindingContext;

            if (stock.LastOrPrevious.HasValue)
            {
                lblLastOrPrevious.Text = stock.LastOrPrevious.Value.ToString("N0");
                lblLastOrPrevious.TextColor = stock.ChangeForeground;
            }
            else
            {
                lblLastOrPrevious.Text = "";
            }

            if (stock.ChangePercent.HasValue)
            {
                lblChangePercent.Text = stock.ChangePercent.Value.ToString("+0.00\\%;-0.00\\%;0.00\\%");
                lblChangePercent.TextColor = stock.ChangeForeground;
            }
            else
            {
                lblChangePercent.Text = "";
            }

            if (stock.Change.HasValue)
            {
                lblChangePoint.Text = stock.Change.Value.ToString("+#,##0;-#,##0;+0");
                lblChangePoint.TextColor = stock.ChangeForeground;
            }
            else
            {
                lblChangePoint.Text = "";
            }
        }

        private void SetPriceLabelForegroundToWhite()
        {
            lblLastOrPrevious.TextColor = Colors.FieldValueDefaultColor;
            lblChangePercent.TextColor = Colors.FieldValueDefaultColor;
            lblChangePoint.TextColor = Colors.FieldValueDefaultColor;
        }

        private async void blinkPriceBackgroundAsync(Color colorBackground)
        {
            if (isBlinking)
            {
                return;
            }

            isBlinking = true;
            SetPriceLabelForeground();
            SetPriceLabelForegroundToWhite();
            frameHighlight.BackgroundColor = colorBackground;
            frameHighlight.IsVisible = true;
            await Task.Delay(500);
            frameHighlight.IsVisible = false;
            SetPriceLabelForeground();
            isBlinking = false;
        }
    }
}