﻿using POST.Constants;
using POST.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TradeDoneViewCell : ViewCell
    {
        public TradeDoneViewCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (BindingContext is DataSources.AthenaMessages.Responses.TradeDone trade)
            {
                switch (trade.OrderAction)
                {
                    case OrderAction.Buy:
                        LblAction.TextColor = Colors.LoseBackground;
                        break;
                    case OrderAction.Sell:
                        LblAction.TextColor = Colors.GainBackground;
                        break;
                    default:
                        LblAction.TextColor = Colors.FieldValueDefaultColor;
                        break;
                }

                //Syariah
                if (Store.StockById.GetState().TryGetValue(new StockId(trade.StockCode, MarketType.RG), out Stock stock2))
                {
                    lblStockCode.TextDecorations = stock2.TextDecoration;
                }
            }
        }
    }
}