﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompactCurrencyCell : ViewCell
    {
        public CompactCurrencyCell()
        {
            InitializeComponent();
        }
    }
}