﻿using POST.Constants;
using POST.Models;
using POST.Models.Primitives;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderBookLevelCell : ViewCell
    {

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();
        private const bool SUBTLE_HINT = false;

        public event OrderButtonEventHandler BuyClicked;
        public event OrderButtonEventHandler SellClicked;

        public OrderBookLevelCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.OrderBook
                .DistinctUntilChanged(orderBook => new
                {
                    orderBook.Previous,
                    orderBook.BestBid,
                    orderBook.BestAsk
                })
                .Subscribe(orderBook =>
                {
                    if (BindingContext is OrderBookLevel orderBookLevel)
                    {
                        UpdateTextColors(orderBookLevel, orderBook.Previous, orderBook.BestBid, orderBook.BestAsk);
                    }
                })
            );

            _observers.Add(Store.OrderBook
                .DistinctUntilChanged(orderBook => orderBook.MaxVolume)
                .Subscribe(orderBook =>
                {
                    if (BindingContext is OrderBookLevel orderBookLevel)
                    {
                        UpdateBars(orderBookLevel, orderBook.MaxVolume);
                    }
                })
            );
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
            base.OnDisappearing();
        }

        private void OrderBookLevelCell_BindingContextChanged(object sender, System.EventArgs e)
        {
            if (BindingContext is OrderBookLevel orderBookLevel)
            {
                OrderBook orderBook = Store.OrderBook.GetState();
                UpdateTextColors(orderBookLevel, orderBook.Previous, orderBook.BestBid, orderBook.BestAsk);
                UpdateBars(orderBookLevel, orderBook.MaxVolume);
            }
        }

        private void UpdateTextColors(OrderBookLevel orderBookLevel, decimal? previous, Price bestBid, Price bestAsk)
        {

            // Label opacity
            if (orderBookLevel.BidVolume.Value == 0m &&
                orderBookLevel.AskVolume.Value == 0m)
            {
                BidVolumeLabel.Opacity = 0.4;
                PriceLabel.Opacity = 0.4;
                AskVolumeLabel.Opacity = 0.4;
            }
            else
            {
                BidVolumeLabel.Opacity = orderBookLevel.BidVolume.Value > 0 ? 1 : 0.4;
                PriceLabel.Opacity = 1;
                AskVolumeLabel.Opacity = orderBookLevel.AskVolume.Value > 0 ? 1 : 0.4;
            }

            if (SUBTLE_HINT)
            {
                #region Subtle
                // Buy button
                if ((bestBid != null && orderBookLevel.Price <= bestBid) || orderBookLevel.Price == bestAsk)
                {
                    BuyButton.BorderWidth = 2;
                    BuyButton.FontFamily = (OnPlatform<string>)App.Current.Resources["DINProBold"];
                    BuyButton.Opacity = 1;
                }
                else
                {
                    BuyButton.BorderWidth = 1;
                    BuyButton.FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"];
                    BuyButton.Opacity = 0.8;

                }

                // Sell button
                if ((bestAsk != null && orderBookLevel.Price >= bestAsk) || orderBookLevel.Price == bestBid)
                {
                    SellButton.BorderWidth = 2;
                    SellButton.FontFamily = (OnPlatform<string>)App.Current.Resources["DINProBold"];
                    SellButton.Opacity = 1;
                }
                else
                {
                    SellButton.BorderWidth = 1;
                    SellButton.FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"];
                    SellButton.Opacity = 0.8;
                }
                #endregion
            }
            else
            {
                #region Prominent
                // Buy button
                if ((bestBid != null && orderBookLevel.Price <= bestBid) || orderBookLevel.Price == bestAsk)
                {
                    BuyButton.BackgroundColor = (Color)App.Current.Resources["DangerColor"];
                    BuyButton.TextColor = Color.White;
                }
                else
                {
                    BuyButton.BackgroundColor = Color.Transparent;
                    BuyButton.TextColor = (Color)App.Current.Resources["DangerColor"];

                }

                // Sell button
                if ((bestAsk != null && orderBookLevel.Price >= bestAsk) || orderBookLevel.Price == bestBid)
                {
                    SellButton.BackgroundColor = (Color)App.Current.Resources["SuccessColor"];
                    SellButton.TextColor = Color.White;
                }
                else
                {
                    SellButton.BackgroundColor = Color.Transparent;
                    SellButton.TextColor = (Color)App.Current.Resources["SuccessColor"];
                }
                #endregion
            }

            // Price label
            if (previous.HasValue)
            {
                if (orderBookLevel.Price > previous.Value)
                {
                    PriceLabel.TextColor = Colors.GainForeground;
                }
                else if (orderBookLevel.Price < previous.Value)
                {
                    PriceLabel.TextColor = Colors.LoseForeground;
                }
                else
                {
                    PriceLabel.TextColor = Colors.UnchangedForeground;
                }
            }
            else
            {
                PriceLabel.TextColor = Color.White;
            }
        }

        private void UpdateBars(OrderBookLevel orderBookLevel, decimal maxVolume)
        {
            if (maxVolume < orderBookLevel.BidVolume)
            {
                maxVolume = orderBookLevel.BidVolume;
            }

            if (maxVolume < orderBookLevel.AskVolume)
            {
                maxVolume = orderBookLevel.AskVolume;
            }

            decimal bidFraction = orderBookLevel.BidVolume / maxVolume * 100m;
            decimal askFraction = orderBookLevel.AskVolume / maxVolume * 100m;

            double bidBarWidth = (double)bidFraction;
            double bidSpacerWidth = Math.Max((double)(100m - bidFraction), 0.0);
            double askBarWidth = (double)askFraction;
            double askSpacerWidth = Math.Max((double)(100m - askFraction), 0.0);

            BidVolumeBar.Width = bidBarWidth > 0.0 ? new GridLength(bidBarWidth, GridUnitType.Star) : new GridLength(1, GridUnitType.Absolute);
            BidVolumeSpacer.Width = bidSpacerWidth > 0.0 ? new GridLength(bidSpacerWidth, GridUnitType.Star) : new GridLength(1, GridUnitType.Absolute);
            AskVolumeBar.Width = askBarWidth > 0.0 ? new GridLength(askBarWidth, GridUnitType.Star) : new GridLength(1.0, GridUnitType.Absolute);
            AskVolumeSpacer.Width = askSpacerWidth > 0.0 ? new GridLength(askSpacerWidth, GridUnitType.Star) : new GridLength(1.0, GridUnitType.Absolute);
        }

        private void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (BindingContext is OrderBookLevel orderBookLevel)
            {
                BuyClicked?.Invoke(this, new OrderButtonEventArgs(orderBookLevel.Price));
            }
        }

        private void SellButton_Clicked(object sender, EventArgs e)
        {
            if (BindingContext is OrderBookLevel orderBookLevel)
            {
                SellClicked?.Invoke(this, new OrderButtonEventArgs(orderBookLevel.Price));
            }
        }
    }
}