﻿using POST.Constants;
using POST.Models;
using POST.Models.Primitives;
using System;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderBookLevelTwoColumnCell : ViewCell
    {
        private const bool SUBTLE_HINT = false;

        public event OrderButtonEventHandler PriceClicked;

        public OrderBookLevelTwoColumnCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            FramePrice.IsVisible = false;
            FrameSelected.IsVisible = false;

            if (BindingContext is OrderBookLevelTwoColumn orderBookLevel)
            {
                OrderBook orderBook = Store.OrderBookTwoColumn.GetState();
                UpdateCell(orderBookLevel, orderBook.Previous, orderBook.MaxVolumeShortLevel, orderBook.LastPrice, orderBook.BestBid, orderBook.BestAsk);
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void UpdateCell(OrderBookLevelTwoColumn orderBookLevel, decimal? previous, decimal maxVolume, decimal? lastPrice, Price bestBid, Price bestAsk)
        {
            if (maxVolume < orderBookLevel.Volume)
            {
                maxVolume = orderBookLevel.Volume;
            }

            if (orderBookLevel.Price != 0m)
            {
                GridPrice.IsVisible = true;
                PriceLabel.IsVisible = true;
                GridVolume.IsVisible = true;
                VolumeLabel.IsVisible = true;

                VolumeLabel.Text = orderBookLevel.Volume.Value.ToString("N0");
                PriceLabel.Text = orderBookLevel.Price.Value.ToString("N0");

                if (lastPrice.HasValue && orderBookLevel.Price == lastPrice.Value)
                {
                    FramePrice.IsVisible = true;
                }

                //Grid Bid
                if (bestBid != null)
                {
                    if (orderBookLevel.Price <= bestBid.Value)
                    {
                        gridVolumeBar.BackgroundColor = Colors.AskBackground_WhiteTheme;
                        GridPrice.BackgroundColor = Colors.BidBackground_WhiteTheme;
                    }
                    else
                    {
                        gridVolumeBar.BackgroundColor = Colors.BidBackground_WhiteTheme;
                        GridPrice.BackgroundColor = Colors.AskBackground_WhiteTheme;
                    }
                }
                //Grid Ask
                if (bestAsk != null)
                {
                    if (orderBookLevel.Price >= bestAsk.Value)
                    {
                        gridVolumeBar.BackgroundColor = Colors.BidBackground_WhiteTheme;
                        GridPrice.BackgroundColor = Colors.AskBackground_WhiteTheme;
                    }
                    else
                    {
                        gridVolumeBar.BackgroundColor = Colors.AskBackground_WhiteTheme;
                        GridPrice.BackgroundColor = Colors.BidBackground_WhiteTheme;
                    }
                }


                // Label price
                if (previous.HasValue)
                {
                    if (orderBookLevel.Price > previous.Value)
                    {
                        PriceLabel.TextColor = Colors.GainForeground_WhiteTheme;
                    }
                    else if (orderBookLevel.Price < previous.Value)
                    {
                        PriceLabel.TextColor = Colors.LoseForeground_WhiteTheme;
                    }
                    else
                    {
                        PriceLabel.TextColor = Colors.UnchangedForeground_WhiteTheme;
                    }
                }
                else
                {
                    PriceLabel.TextColor = Colors.FieldValueDefaultColor;
                }

                decimal Fraction = orderBookLevel.Volume / maxVolume * 100m;
                double BarWidth = (double)Fraction;
                double SpacerWidth = Math.Max((double)(100m - Fraction), 0.0);

                Device.BeginInvokeOnMainThread(() =>
                {
                    VolumeBar.Width = BarWidth > 0.0 ? new GridLength(BarWidth, GridUnitType.Star) : new GridLength(0, GridUnitType.Absolute);
                    VolumeSpacer.Width = SpacerWidth > 0.0 ? new GridLength(SpacerWidth, GridUnitType.Star) : new GridLength(0, GridUnitType.Absolute);
                });
            }
            else
            {
                GridPrice.IsVisible = false;
                PriceLabel.IsVisible = false;
                GridVolume.IsVisible = false;
                VolumeLabel.IsVisible = false;
            }
        }

        private void OrderBookLevelTwoColumnCell_ItemTapped(object sender, System.EventArgs e)
        {
            if (BindingContext is OrderBookLevelTwoColumn orderBookLevel && orderBookLevel.Price != 0)
            {
                Thread th = new Thread(new ThreadStart(BlinkPricelLabel))
                {
                    IsBackground = true
                };
                th.Start();
            }
        }

        private void BlinkPricelLabel()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    FrameSelected.IsVisible = true;
                }
                catch
                {

                }
            });

            Thread.Sleep(200);

            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    FrameSelected.IsVisible = false;
                }
                catch
                {

                }
            });
        }

    }
}