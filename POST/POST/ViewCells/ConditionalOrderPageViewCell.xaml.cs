﻿using POST.DataSources.AthenaMessages.Responses;
using POST.Dialogs;
using POST.Models;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Behaviors;
using XLabs.Forms.Controls;

namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConditionalOrderPageViewCell : ViewCell
    {
        public ConditionalOrderPageViewCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (BindingContext is ConditionalOrder conditionalOrder)
            {
                //Syariah
                if (Store.StockById.GetState().TryGetValue(new StockId(conditionalOrder.StockCode, MarketType.RG), out Stock stock2))
                {
                    lblStockCode.TextDecorations = stock2.TextDecoration;
                }
            }
        }

        private async void GesturesContentView_GestureRecognized(object sender, GestureResult e)
        {
            if (BindingContext is ConditionalOrder conditionalOrder)
            {
                switch (e.GestureType)
                {
                    case GestureType.SingleTap:
                        if (Store.StockById.GetState().TryGetValue(new StockId(conditionalOrder.StockCode, conditionalOrder.MarketType), out Stock stock))
                        {
                            Store.RecentStockIds.AddStock(stock.Id);
                            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                            {
                                await PopupNavigation.Instance.PopAllAsync();
                            }

                            ConditionalOrderDetailDialog conditionalOrderDetailDialog = new ConditionalOrderDetailDialog(conditionalOrder, stock.SharesPerLot.Value);
                            await PopupNavigation.Instance.PushAsync(conditionalOrderDetailDialog);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}