﻿using POST.DataSources.AlertMessages.Responses;

namespace POST.ViewCells
{
    public class AlertCellEventArgs
    {
        public Notice notice { get; }
        public AlertCellEventArgs(Notice notice)
        {
            this.notice = notice;
        }
    }
}
