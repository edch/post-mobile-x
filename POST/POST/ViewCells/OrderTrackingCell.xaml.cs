﻿using POST.Constants;
using POST.DataSources.AthenaMessages.Responses;
using POST.Dialogs;
using POST.Models;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Behaviors;
using XLabs.Forms.Controls;

namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderTrackingCell : ViewCell
    {
        public OrderTrackingCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (BindingContext is Order order)
            {
                switch (order.OrderAction)
                {
                    case OrderAction.Buy:
                        LblAction.TextColor = Colors.LoseBackground;
                        break;
                    case OrderAction.Sell:
                        LblAction.TextColor = Colors.GainBackground;
                        break;
                    default:
                        LblAction.TextColor = Colors.FieldValueDefaultColor;
                        break;
                }

                switch (order.OrderStatus)
                {
                    case OrderStatus.Open:
                        LblOrderStatusCode.TextColor = Colors.OpenOrderStatus;
                        break;
                    case OrderStatus.Canceled:
                        LblOrderStatusCode.TextColor = Colors.CancelledOrderStatus;
                        break;
                    case OrderStatus.Matched:
                        LblOrderStatusCode.TextColor = Colors.MatchedOrderStatus;
                        break;
                    case OrderStatus.Rejected:
                        LblOrderStatusCode.TextColor = Colors.RejectedOrderStatus;
                        break;
                    default:
                        LblOrderStatusCode.TextColor = Colors.FieldValueDefaultColor;
                        break;
                }

                //Syariah
                if (Store.StockById.GetState().TryGetValue(new StockId(order.StockCode, MarketType.RG), out Stock stock2))
                {
                    lblStockCode.TextDecorations = stock2.TextDecoration;
                }
            }
        }

        private async void GesturesContentView_GestureRecognized(object sender, GestureResult e)
        {

            if (BindingContext is Order order)
            {
                switch (e.GestureType)
                {
                    case GestureType.SingleTap:
                        if (Store.StockById.GetState().TryGetValue(new StockId(order.StockCode, order.MarketType), out Stock stock))
                        {
                            Store.RecentStockIds.AddStock(stock.Id);
                            OrderTrackingDialog orderTrackingDialog = new OrderTrackingDialog(
                                stockId: stock.Id,
                                order: order
                            );
                            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                            {
                                await PopupNavigation.Instance.PopAllAsync();
                            }

                            await PopupNavigation.Instance.PushAsync(orderTrackingDialog);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}