﻿using POST.Constants;
using POST.Models;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StockSearchingCell : ViewCell
    {
        private bool isChecked = false;

        public StockSearchingCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (BindingContext is Stock stock)
            {
                IsChecked = Store.WatchedStockIds.GetState().Any(stockId => string.Compare(stockId.Code, stock.Id.Code) == 0) ? true : false;

                lblStockCode.TextDecorations = stock.TextDecoration;
                lblStockName.TextDecorations = stock.TextDecoration;
            }
        }

        protected override void OnTapped()
        {
            base.OnTapped();

            if (BindingContext is Stock stock)
            {
                if (Store.WatchedStockIds.GetState().ToList().Any(stockId => stockId.Code == stock.Id.Code))
                {
                    Store.WatchedStockIds.RemoveStock(stock.Id.Code);
                    IsChecked = false;
                }
                else
                {
                    Store.WatchedStockIds.AddStock(stock.Id.Code);
                    IsChecked = true;
                }
            }
        }

        public bool IsChecked
        {
            get => isChecked;
            set
            {
                isChecked = value;
                lblChecked.Text = isChecked ? "\uf00c" : "\uf067";
                lblChecked.TextColor = isChecked ? Colors.Accent : Colors.FieldNameColor;
            }
        }
    }
}