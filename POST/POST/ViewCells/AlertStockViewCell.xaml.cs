﻿using POST.DataSources.AlertMessages.Responses;
using POST.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Behaviors;
using XLabs.Forms.Controls;


namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlertStockViewCell : ViewCell
    {
        public event AlertCellEventHandler alertCellSingleTap;
        public event AlertCellEventHandler alertCellLongPress;

        public AlertStockViewCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (BindingContext is Notice notice)
            {
                //Syariah
                if (Store.StockById.GetState().TryGetValue(new StockId(notice.stockid, MarketType.RG), out Stock stock2))
                {
                    lblStockCode.TextDecorations = stock2.TextDecoration;
                }
            }
        }

        private async void GesturesContentView_GestureRecognized(object sender, GestureResult e)
        {
            if (BindingContext is Notice notice)
            {
                switch (e.GestureType)
                {
                    case GestureType.SingleTap:
                        alertCellSingleTap?.Invoke(this, new AlertCellEventArgs(notice));
                        break;

                    case GestureType.LongPress:
                        alertCellLongPress?.Invoke(this, new AlertCellEventArgs(notice));
                        break;
                    default:
                        break;

                }
            }
        }
    }
}