﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells.CorporateAction
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WarrantsViewCell : ViewCell
    {
        public WarrantsViewCell()
        {
            InitializeComponent();
        }
    }
}