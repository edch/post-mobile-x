﻿using POST.Constants;
using POST.DataSources.AthenaMessages.Responses;
using POST.Dialogs;
using POST.Models;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Behaviors;
using XLabs.Forms.Controls;

namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrdersPageViewCell : ViewCell
    {
        public OrdersPageViewCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (BindingContext is Order order)
            {
                switch (order.OrderAction)
                {
                    case OrderAction.Buy:
                        LblAction.TextColor = Colors.LoseBackground;
                        break;
                    case OrderAction.Sell:
                        LblAction.TextColor = Colors.GainBackground;
                        break;
                    default:
                        LblAction.TextColor = Colors.FieldValueDefaultColor;
                        break;
                }

                switch (order.OrderStatus)
                {
                    case OrderStatus.Open:
                        LblOrderStatusCode.TextColor = Colors.OpenOrderStatus;
                        break;
                    case OrderStatus.Canceled:
                        LblOrderStatusCode.TextColor = Colors.CancelledOrderStatus;
                        break;
                    case OrderStatus.Matched:
                        LblOrderStatusCode.TextColor = Colors.MatchedOrderStatus;
                        break;
                    case OrderStatus.Rejected:
                        LblOrderStatusCode.TextColor = Colors.RejectedOrderStatus;
                        break;
                    default:
                        LblOrderStatusCode.TextColor = Colors.FieldValueDefaultColor;
                        break;
                }
            }
        }

        private async void GesturesContentView_GestureRecognized(object sender, GestureResult e)
        {

            if (BindingContext is Order order)
            {
                switch (e.GestureType)
                {
                    case GestureType.SingleTap:
                        if (order.OrderStatus == OrderStatus.Open)
                        {
                            if (Store.StockById.GetState().TryGetValue(new StockId(order.StockCode, order.MarketType), out Stock stock))
                            {
                                Store.RecentStockIds.AddStock(stock.Id);

                                if (stock.OpenOrPrevious.HasValue)
                                {
                                    if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                    {
                                        await PopupNavigation.Instance.PopAllAsync();
                                    }

                                    AmendWithdrawDialog amendWithdrawDialog = new AmendWithdrawDialog(
                                    order: order,
                                    openPrice: stock.OpenOrPrevious.Value,
                                    sharesPerLot: stock.SharesPerLot.Value
                                    );
                                    await PopupNavigation.Instance.PushAsync(amendWithdrawDialog);
                                }
                                else
                                {
                                    if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                    {
                                        await PopupNavigation.Instance.PopAllAsync();
                                    }

                                    AmendWithdrawDialog amendWithdrawDialog = new AmendWithdrawDialog(
                                    order: order,
                                    openPrice: 0,
                                    sharesPerLot: stock.SharesPerLot.Value
                                    );
                                    await PopupNavigation.Instance.PushAsync(amendWithdrawDialog);
                                }
                            }
                        }
                        else
                        {
                            if (Store.StockById.GetState().TryGetValue(new StockId(order.StockCode, order.MarketType), out Stock stock))
                            {
                                Store.RecentStockIds.AddStock(stock.Id);
                                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                {
                                    await PopupNavigation.Instance.PopAllAsync();
                                }

                                OrderInformationDialog orderInformationDialog = new OrderInformationDialog(
                                order: order,
                                sharesPerLot: stock.SharesPerLot.Value
                            );
                                await PopupNavigation.Instance.PushAsync(orderInformationDialog);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}