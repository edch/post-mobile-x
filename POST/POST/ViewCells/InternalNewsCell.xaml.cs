﻿using POST.DataSources.ApiResponses;
using POST.Services;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InternalNewsCell : ViewCell
    {

        public InternalNewsCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            UpdateSaveButton();
        }

        private void UpdateSaveButton()
        {
            switch (BindingContext)
            {
                case InternalNews internalNews:
                    SaveButtonLabel.IsVisible = !(SavedButtonLabel.IsVisible = internalNews.Saved);
                    break;
            }
        }

        private void SaveButton_Clicked(object sender, EventArgs e)
        {
            switch (BindingContext)
            {
                case InternalNews internalNews:
                    if (internalNews.Saved)
                    {
                        Store.SavedNewsCollection.RemoveNews(internalNews);
                    }
                    else
                    {
                        Store.SavedNewsCollection.AddNews(internalNews);
                    }
                    UpdateSaveButton();
                    break;
            }
        }

        private async void ShareButton_Clicked(object sender, EventArgs e)
        {
            switch (BindingContext)
            {
                case InternalNews internalNews:
                    await DependencyService.Get<IShareDialog>().ShowAsync(internalNews.Title, internalNews.Notes, $"http://chart.post-pro.co.id:8080{internalNews.FileName}");
                    break;
            }
        }
    }
}