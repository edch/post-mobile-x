﻿using POST.Constants;
using POST.Converters;
using POST.Dialogs;
using POST.Models;
using POST.Stores;
using Rg.Plugins.Popup.Services;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Behaviors;
using XLabs.Forms.Controls;

namespace POST.ViewCells
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BrokerQuoteCell : ViewCell
    {
        public BrokerQuoteCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            if (BindingContext is Broker broker)
            {
                try
                {
                    lblRowNumber.Text = broker.RowNumber.ToString();
                    lblBrokerCode.Text = broker.Id.Code;
                    lblBrokerCode.TextColor = AppSettings.BrokerTypeColor[broker.Type];

                    lblBrokerName.Text = broker.Name;

                    OrderOfMagnitudeConverter orderOfMagnitudeConverter = new OrderOfMagnitudeConverter();

                    if (AppSettings.BrokerQuoteCategory == BrokerQuoteCategory.Total)
                    {
                        lblVal.Text = orderOfMagnitudeConverter.Convert(broker.TotalVal, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblVal.TextColor = broker.TotalValForeground;

                        lblVol.Text = orderOfMagnitudeConverter.Convert(broker.TotalVol, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblVol.TextColor = broker.TotalVolForeground;

                        lblFreq.Text = orderOfMagnitudeConverter.Convert(broker.TotalFreq, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblFreq.TextColor = Colors.FieldValueDefaultColor;
                    }
                    else if (AppSettings.BrokerQuoteCategory == BrokerQuoteCategory.Buy)
                    {
                        lblVal.Text = orderOfMagnitudeConverter.Convert(broker.BuyVal, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblVal.TextColor = broker.BuyValForeground;

                        lblVol.Text = orderOfMagnitudeConverter.Convert(broker.BuyVol, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblVol.TextColor = broker.BuyVolForeground;

                        lblFreq.Text = orderOfMagnitudeConverter.Convert(broker.BuyFreq, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblFreq.TextColor = Colors.FieldValueDefaultColor;
                    }
                    else if (AppSettings.BrokerQuoteCategory == BrokerQuoteCategory.Sell)
                    {
                        lblVal.Text = orderOfMagnitudeConverter.Convert(broker.SellVal, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblVal.TextColor = broker.SellValForeground;

                        lblVol.Text = orderOfMagnitudeConverter.Convert(broker.SellVol, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblVol.TextColor = broker.SellVolForeground;

                        lblFreq.Text = orderOfMagnitudeConverter.Convert(broker.SellFreq, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblFreq.TextColor = Colors.FieldValueDefaultColor;
                    }
                    else // Net
                    {
                        lblVal.Text = orderOfMagnitudeConverter.Convert(broker.NetVal, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblVal.TextColor = broker.NetValForeground;

                        lblVol.Text = orderOfMagnitudeConverter.Convert(broker.NetVol, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblVol.TextColor = broker.NetVolForeground;

                        lblFreq.Text = orderOfMagnitudeConverter.Convert(broker.NetFreq, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString();
                        lblFreq.TextColor = broker.NetFreqForeground;
                    }
                }
                catch (Exception)
                {
                }
            }

            base.OnAppearing();
        }

        private async void GesturesContentView_GestureRecognized(object sender, GestureResult e)
        {
            if (BindingContext is Broker broker)
            {
                switch (e.GestureType)
                {
                    case GestureType.SingleTap:
                        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                        {
                            await PopupNavigation.Instance.PopAllAsync();
                        }

                        BrokerQuoteDetailPage brokerQuoteDetailPage = new BrokerQuoteDetailPage($"[{broker.Id.Code}] {broker.Name}");
                        await PopupNavigation.Instance.PushAsync(brokerQuoteDetailPage);
                        break;
                    default:
                        break;
                }
            }
        }
    }


}