﻿using POST.Models;

namespace POST.ViewCells
{
    public class StockCellEventArgs
    {
        public Stock stock { get; }
        public StockCellEventArgs(Stock stock)
        {
            this.stock = stock;
        }
    }
}
