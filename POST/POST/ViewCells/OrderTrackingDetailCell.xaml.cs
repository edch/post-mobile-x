﻿using POST.Constants;
using POST.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderTrackingDetailCell : ViewCell
    {
        public OrderTrackingDetailCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            if (BindingContext is OrderTracking orderTracking)
            {
                if (orderTracking.OrderNo.ToString() == Store.OrderTrackingStore.GetState().JsxId)
                {
                    grd.BackgroundColor = Colors.Accent;
                }
                else
                {
                    grd.BackgroundColor = Colors.PageBackgroundColor;
                }
            }
            base.OnAppearing();
        }
    }
}