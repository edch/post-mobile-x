﻿using POST.DataSources;
using POST.DataSources.ApiResponses;
using POST.Services;
using System;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExternalNewsCell : ViewCell
    {

        public ExternalNewsCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            UpdateSaveButton();
        }

        private void UpdateSaveButton()
        {
            switch (BindingContext)
            {
                case ExternalNews externalNews:
                    SaveButtonLabel.IsVisible = !(SavedButtonLabel.IsVisible = externalNews.Saved);
                    break;
                case ExternalNewsSnippet externalNewsSnippet:
                    SaveButtonLabel.IsVisible = !(SavedButtonLabel.IsVisible = externalNewsSnippet.Saved);
                    break;
            }
        }

        private void SaveButton_Clicked(object sender, EventArgs e)
        {
            switch (BindingContext)
            {
                case ExternalNews externalNews:
                    if (externalNews.Saved)
                    {
                        Store.SavedNewsCollection.RemoveNews(externalNews);
                    }
                    else
                    {
                        Store.SavedNewsCollection.AddNews(externalNews);
                    }
                    UpdateSaveButton();
                    break;
                case ExternalNewsSnippet externalNewsSnippet:
                    if (externalNewsSnippet.Saved)
                    {
                        Store.SavedNewsCollection.RemoveNews(externalNewsSnippet);
                    }
                    else
                    {
                        Store.SavedNewsCollection.AddNews(externalNewsSnippet);
                    }
                    UpdateSaveButton();
                    break;
            }
        }

        private async void ShareButton_Clicked(object sender, EventArgs e)
        {
            switch (BindingContext)
            {
                case ExternalNews externalNews:
                    await DependencyService.Get<IShareDialog>().ShowAsync(externalNews.Title, externalNews.Content, null);
                    break;
                case ExternalNewsSnippet externalNewsSnippet:
                    try
                    {
                        ExternalNews news = await NewsClient.Default.GetExternalNewsAsync(externalNewsSnippet.MessageNo, CancellationToken.None);
                        Store.SavedNewsCollection.SetContent(news);
                        await DependencyService.Get<IShareDialog>().ShowAsync(news.Title, news.Content, null);
                    }
                    catch { }
                    break;
            }
        }
    }
}