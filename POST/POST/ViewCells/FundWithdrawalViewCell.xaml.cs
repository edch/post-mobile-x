﻿using POST.DataSources.WdMessages.Responses;
using POST.Dialogs;
using Rg.Plugins.Popup.Services;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Behaviors;
using XLabs.Forms.Controls;

namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FundWithdrawalViewCell : ViewCell
    {
        public FundWithdrawalViewCell()
        {
            InitializeComponent();
        }

        private async void GesturesContentView_GestureRecognized(object sender, GestureResult e)
        {
            if (BindingContext is FundWithdrawItem fundWithdrawItem)
            {
                switch (e.GestureType)
                {
                    case GestureType.SingleTap:
                        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) await PopupNavigation.Instance.PopAllAsync();
                        FundWithdrawalDetailDialog fundWithdrawalDetailDialog = new FundWithdrawalDetailDialog(fundWithdrawItem);
                        await PopupNavigation.Instance.PushAsync(fundWithdrawalDetailDialog);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}