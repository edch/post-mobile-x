﻿using POST.Models;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StockDeletingCell : ViewCell
    {
        public event StockCellEventHandler StockCellTapped;

        public StockDeletingCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (BindingContext is Stock stock)
            {
                lblStockCode.TextDecorations = stock.TextDecoration;
                lblStockName.TextDecorations = stock.TextDecoration;
            }
        }

        private void btnRemove_Clicked(object sender, EventArgs e)
        {
            if (BindingContext is Stock stock)
            {
                StockCellTapped?.Invoke(this, new StockCellEventArgs(stock));
            }
        }
    }
}