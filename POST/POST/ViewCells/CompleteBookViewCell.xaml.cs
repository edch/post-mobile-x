﻿using POST.Constants;
using POST.Models;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.ViewCells
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompleteBookViewCell : ViewCell
    {
        private const bool SUBTLE_HINT = false;

        public event OrderButtonEventHandler BidPriceClicked;
        public event OrderButtonEventHandler AskPriceClicked;

        public CompleteBookViewCell()
        {
            InitializeComponent();

            BidPriceLabel.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    if (BindingContext is OrderBookLevelType2 orderBookLevel)
                    {
                        BidPriceClicked?.Invoke(this, new OrderButtonEventArgs(orderBookLevel.BidPrice));
                    }
                })
            });

            AskPriceLabel.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    if (BindingContext is OrderBookLevelType2 orderBookLevel)
                    {
                        AskPriceClicked?.Invoke(this, new OrderButtonEventArgs(orderBookLevel.AskPrice));
                    }
                })
            });
        }

        public CompleteBookViewCell(OrderButtonEventHandler bidPriceClicked, OrderButtonEventHandler askPriceClicked)
        {
            InitializeComponent();

            BidPriceClicked += bidPriceClicked;
            AskPriceClicked += askPriceClicked;

            BidPriceLabel.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    if (BindingContext is OrderBookLevelType2 orderBookLevel)
                    {
                        BidPriceClicked?.Invoke(this, new OrderButtonEventArgs(orderBookLevel.BidPrice));
                    }
                })
            });

            AskPriceLabel.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    if (BindingContext is OrderBookLevelType2 orderBookLevel)
                    {
                        AskPriceClicked?.Invoke(this, new OrderButtonEventArgs(orderBookLevel.AskPrice));
                    }
                })
            });
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();

            FrameAskPrice.IsVisible = false;
            FrameBidPrice.IsVisible = false;

            if (BindingContext is OrderBookLevelType2 orderBookLevel)
            {
                OrderBook orderBook = Store.OrderBook.GetState();
                UpdateCell(orderBookLevel, orderBook.Previous, orderBook.MaxVolume, orderBook.LastPrice);
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void UpdateCell(OrderBookLevelType2 orderBookLevel, decimal? previous, decimal maxVolume, decimal? lastPrice)
        {
            if (maxVolume < orderBookLevel.BidVolume)
            {
                maxVolume = orderBookLevel.BidVolume;
            }

            if (maxVolume < orderBookLevel.AskVolume)
            {
                maxVolume = orderBookLevel.AskVolume;
            }

            if (orderBookLevel.BidPrice != null)
            {
                BidFrequencyLabel.IsVisible = true;
                BidPriceLabel.IsVisible = true;
                BidGridVolume.IsVisible = true;
                BidVolumeLabel.IsVisible = true;

                BidVolumeLabel.Text = orderBookLevel.BidVolume.Value.ToString("N0");
                BidFrequencyLabel.Text = orderBookLevel.BidFrequency.Value.ToString("N0");
                BidPriceLabel.Text = orderBookLevel.BidPrice.Value.ToString("N0");

                if (lastPrice.HasValue && orderBookLevel.BidPrice == lastPrice.Value)
                {
                    FrameBidPrice.IsVisible = true;
                }

                // Label price
                if (previous.HasValue)
                {
                    if (orderBookLevel.BidPrice > previous.Value)
                    {
                        BidPriceLabel.TextColor = Colors.GainForeground;
                    }
                    else if (orderBookLevel.BidPrice < previous.Value)
                    {
                        BidPriceLabel.TextColor = Colors.LoseForeground;
                    }
                    else
                    {
                        BidPriceLabel.TextColor = Colors.UnchangedForeground;
                    }
                }
                else
                {
                    BidPriceLabel.TextColor = Color.White;
                }

                decimal bidFraction = orderBookLevel.BidVolume / maxVolume * 100m;
                double bidBarWidth = (double)bidFraction;
                double bidSpacerWidth = Math.Max((double)(100m - bidFraction), 0.0);

                Device.BeginInvokeOnMainThread(() =>
                {
                    BidVolumeBar.Width = bidBarWidth > 0.0 ? new GridLength(bidBarWidth, GridUnitType.Star) : new GridLength(1, GridUnitType.Absolute);
                    BidVolumeSpacer.Width = bidSpacerWidth > 0.0 ? new GridLength(bidSpacerWidth, GridUnitType.Star) : new GridLength(1, GridUnitType.Absolute);
                }
                );
            }
            else
            {
                BidFrequencyLabel.IsVisible = false;
                BidPriceLabel.IsVisible = false;
                BidGridVolume.IsVisible = false;
                BidVolumeLabel.IsVisible = false;
            }

            if (orderBookLevel.AskPrice != null)
            {
                AskFrequencyLabel.IsVisible = true;
                AskPriceLabel.IsVisible = true;
                AskGridVolume.IsVisible = true;
                AskVolumeLabel.IsVisible = true;

                AskPriceLabel.Text = orderBookLevel.AskPrice.Value.ToString("N0");
                AskFrequencyLabel.Text = orderBookLevel.AskFrequency.Value.ToString("N0");
                AskVolumeLabel.Text = orderBookLevel.AskVolume.Value.ToString("N0");

                if (lastPrice.HasValue && orderBookLevel.AskPrice.Value == lastPrice.Value)
                {
                    FrameAskPrice.IsVisible = true;
                }

                if (previous.HasValue)
                {
                    if (orderBookLevel.AskPrice > previous.Value)
                    {
                        AskPriceLabel.TextColor = Colors.GainForeground;
                    }
                    else if (orderBookLevel.AskPrice < previous.Value)
                    {
                        AskPriceLabel.TextColor = Colors.LoseForeground;
                    }
                    else
                    {
                        AskPriceLabel.TextColor = Colors.UnchangedForeground;
                    }
                }
                else
                {
                    AskPriceLabel.TextColor = Color.White;
                }

                decimal askFraction = orderBookLevel.AskVolume / maxVolume * 100m;
                double askBarWidth = (double)askFraction;
                double askSpacerWidth = Math.Max((double)(100m - askFraction), 0.0);

                Device.BeginInvokeOnMainThread(() =>
                {
                    AskVolumeBar.Width = askBarWidth > 0.0 ? new GridLength(askBarWidth, GridUnitType.Star) : new GridLength(1.0, GridUnitType.Absolute);
                    AskVolumeSpacer.Width = askSpacerWidth > 0.0 ? new GridLength(askSpacerWidth, GridUnitType.Star) : new GridLength(1.0, GridUnitType.Absolute);
                });

            }
            else
            {
                AskFrequencyLabel.IsVisible = false;
                AskPriceLabel.IsVisible = false;
                AskGridVolume.IsVisible = false;
                AskVolumeLabel.IsVisible = false;
            }

        }

    }
}