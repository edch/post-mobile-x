﻿using POST.Models.Primitives;

namespace POST.Models
{

    public class OrderBookLevelTwoColumn
    {
        public Price Price { get; set; }
        public Volume Volume { get; set; }

        public OrderBookLevelTwoColumn(
            Price price,
            Volume volume

        )
        {
            Price = price;
            Volume = volume;
        }
    }
}
