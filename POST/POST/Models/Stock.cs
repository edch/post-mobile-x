﻿using POST.Constants;
using POST.Models.Primitives;
using POST.Stores;
using System;
using System.Text;
using Xamarin.Forms;

namespace POST.Models
{

    public class Stock : IUniqueIndexable<StockId>
    {

        public StockId Id { get; }

        /// <summary>
        /// Marks whether this instance has been initialized using StockInit packet.
        /// </summary>
        public bool IsInitialized => Previous.HasValue;
        public string Name { get; private set; }
        public int? SectorId { get; private set; }
        public string SubSector { get; private set; }
        public bool IsSyariah { get; private set; }
        public bool IsLQ45 { get; private set; }
        public long? SharesPerLot { get; private set; }
        public Shares? TotalShares { get; private set; }
        public string Remarks { get; private set; }
        public bool IsPreOpening { get; private set; }
        public bool IsRecent { get; set; }
        public bool IsWatchlistBoard { get; private set; }


        // Quote - Basic
        public decimal? Previous { get; private set; }
        public OHLC? OHLC { get; private set; }
        public decimal? OpenOrPrevious
        {
            get
            {
                if (IsPreOpening && OHLC.HasValue)
                {
                    return OHLC.Value.Open;
                }
                else
                {
                    return Previous;
                }
            }
        }
        public decimal? LastOrPrevious => OHLC?.Close ?? Previous;
        public string Notice { get; private set; }
        public Magnitudes<Shares> Magnitudes { get; private set; }
        public bool HasMagnitudes => Magnitudes != null;

        // Quote - Market
        public decimal? BestBidPrice { get; private set; }
        public Shares? BestBidVolume { get; private set; }
        public decimal? BestOfferPrice { get; private set; }
        public Shares? BestOfferVolume { get; private set; }
        public Shares? LastTradeVolume { get; private set; }

        // Quote - Totals
        public Magnitudes<Shares> TotalForeignSell { get; private set; }
        public Magnitudes<Shares> TotalForeignBuy { get; private set; }
        public Magnitudes<Shares> TotalDomesticSell { get; private set; }
        public Magnitudes<Shares> TotalDomesticBuy { get; private set; }
        public DateTime? Updated { get; private set; }

        /// <summary>
        /// Create an empty instance of Stock, then only initialize it using code, marketType, and name from placeholder data
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// code, marketType, or name is null. This is a logic exception and should not be displayed to User.
        /// </exception>
        public Stock(
            string code,
            MarketType marketType,
            string name)
        {

            Id = new StockId(
                code: code ?? throw new ArgumentNullException(nameof(code)),
                marketType: marketType
            );
            Name = name ?? throw new ArgumentNullException(nameof(name));

            IsRecent = false;
        }

        /// <summary>
        /// Create an instance of Stock, then initialize it using data from StockInit.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// code, marketType, or name is null. This is a logic exception and should not be displayed to User.
        /// </exception>
        public Stock(
            string code,
            MarketType marketType,
            string name,
            long sharesPerLot,
            long totalShares,
            string remarks,
            decimal? previous)
        {

            Id = new StockId(
                code: code ?? throw new ArgumentNullException(nameof(code)),
                marketType: marketType
            );
            Name = name ?? throw new ArgumentNullException(nameof(name));
            SharesPerLot = sharesPerLot;
            TotalShares = new Shares(
                shares: totalShares,
                sharesPerLot: sharesPerLot
            );
            Remarks = remarks;
            Previous = previous;

            string strTmp;
            if (!string.IsNullOrEmpty(Remarks))
            {
                try
                {
                    strTmp = Remarks[3].ToString();
                    switch (strTmp)
                    {
                        case "-":
                            IsPreOpening = false;
                            break;
                        case "O":
                            IsPreOpening = true;
                            break;
                    }
                    strTmp = null;

                    strTmp = Remarks[6].ToString();
                    if (!string.IsNullOrEmpty(strTmp))
                    {
                        if (strTmp == "3" || strTmp == "7" || strTmp == "A" || strTmp == "D" || strTmp == "E" || strTmp == "G" || strTmp == "K" || strTmp == "L"
                             || strTmp == "N" || strTmp == "P" || strTmp == "R" || strTmp == "S" || strTmp == "T" || strTmp == "V" || strTmp == "W" || strTmp == "X")
                        {
                            IsSyariah = true;
                        }
                        else
                        {
                            IsSyariah = false;
                        }
                    }
                    strTmp = null;
                }
                catch { }

                try
                {
                    //corp action
                    StringBuilder sbSpecialNotation1LongNoNewLine = new StringBuilder();
                    StringBuilder sbSpecialNotation1Long = new StringBuilder();
                    StringBuilder sbSpecialNotation1Short = new StringBuilder();
                    strTmp = string.Concat(Remarks[0], Remarks[1]);
                    switch (strTmp)
                    {
                        case "--":
                            sbSpecialNotation1LongNoNewLine.Append("No Corp. Act., ");
                            sbSpecialNotation1Long.Append("No Corp. Act." + "\r\n");
                            sbSpecialNotation1Short.Append("--");
                            break;
                        case "CD":
                            sbSpecialNotation1LongNoNewLine.Append("Cum Dividend, ");
                            sbSpecialNotation1Long.Append("Cum Dividend" + "\r\n");
                            sbSpecialNotation1Short.Append("CD");
                            break;
                        case "CB":
                            sbSpecialNotation1LongNoNewLine.Append("Cum Bonus, ");
                            sbSpecialNotation1Long.Append("Cum Bonus" + "\r\n");
                            sbSpecialNotation1Short.Append("CB");
                            break;
                        case "CR":
                            sbSpecialNotation1LongNoNewLine.Append("Cum Right, ");
                            sbSpecialNotation1Long.Append("Cum Right" + "\r\n");
                            sbSpecialNotation1Short.Append("CR");
                            break;
                        case "CA":
                            sbSpecialNotation1LongNoNewLine.Append("Any Combination of cum remarks (Cash dividend, stock dividend and/ or stock bonus), ");
                            sbSpecialNotation1Long.Append("Any Combination of cum remarks (Cash dividend, stock dividend and/ or stock bonus)" + "\r\n");
                            sbSpecialNotation1Short.Append("CA");
                            break;
                        case "XD":
                            sbSpecialNotation1LongNoNewLine.Append("Ex Dividend, ");
                            sbSpecialNotation1Long.Append("Ex Dividend" + "\r\n");
                            sbSpecialNotation1Short.Append("XD");
                            break;
                        case "XB":
                            sbSpecialNotation1LongNoNewLine.Append("Ex Bonus, ");
                            sbSpecialNotation1Long.Append("Ex Bonus" + "\r\n");
                            sbSpecialNotation1Short.Append("XB");
                            break;
                        case "XR":
                            sbSpecialNotation1LongNoNewLine.Append("Ex Right, ");
                            sbSpecialNotation1Long.Append("Ex Right" + "\r\n");
                            sbSpecialNotation1Short.Append("XR");
                            break;
                        case "XA":
                            sbSpecialNotation1LongNoNewLine.Append("Any Combination of Ex remarks (Cash dividend, stock dividend and/ or stock bonus), ");
                            sbSpecialNotation1Long.Append("Any Combination of Ex remarks (Cash dividend, stock dividend and/ or stock bonus)" + "\r\n");
                            sbSpecialNotation1Short.Append("XA");
                            break;
                        case "RS":
                            sbSpecialNotation1LongNoNewLine.Append("Reverse Stock, ");
                            sbSpecialNotation1Long.Append("Reverse Stock" + "\r\n");
                            sbSpecialNotation1Short.Append("RS");
                            break;
                        case "SS":
                            sbSpecialNotation1LongNoNewLine.Append("Stock Split, ");
                            sbSpecialNotation1Long.Append("Stock Split" + "\r\n");
                            sbSpecialNotation1Short.Append("SS");
                            break;
                        default:
                            sbSpecialNotation1Short.Append("--");
                            break;
                    }

                    strTmp = Remarks[2].ToString();
                    switch (strTmp)
                    {
                        case "M":
                            sbSpecialNotation1LongNoNewLine.Append("Marginable, ");
                            sbSpecialNotation1Long.Append("Marginable" + "\r\n");
                            sbSpecialNotation1Short.Append("M");
                            break;
                        case "S":
                            sbSpecialNotation1LongNoNewLine.Append("Marginable & Short Selling, ");
                            sbSpecialNotation1Long.Append("Marginable & Short Selling" + "\r\n");
                            sbSpecialNotation1Short.Append("S");
                            break;
                        case "U":
                            sbSpecialNotation1LongNoNewLine.Append("Unmarginable, ");
                            sbSpecialNotation1Long.Append("Unmarginable" + "\r\n");
                            sbSpecialNotation1Short.Append("-");
                            break;
                        case "D":
                            sbSpecialNotation1LongNoNewLine.Append("Designated, ");
                            sbSpecialNotation1Long.Append("Designated" + "\r\n");
                            sbSpecialNotation1Short.Append("D");
                            break;
                        default:
                            sbSpecialNotation1Short.Append("-");
                            break;
                    }

                    strTmp = Remarks[3].ToString();
                    switch (strTmp)
                    {
                        case "-":
                            sbSpecialNotation1LongNoNewLine.Append("Non Preop., ");
                            sbSpecialNotation1Long.Append("Non Preop." + "\r\n");
                            sbSpecialNotation1Short.Append("-");
                            break;
                        case "O":
                            sbSpecialNotation1LongNoNewLine.Append("Preop., ");
                            sbSpecialNotation1Long.Append("Preop." + "\r\n");
                            sbSpecialNotation1Short.Append("O");
                            break;
                        default:
                            sbSpecialNotation1Short.Append("-");
                            break;
                    }

                    strTmp = Remarks[4].ToString();
                    switch (strTmp)
                    {
                        case "1":
                            sbSpecialNotation1LongNoNewLine.Append("MBX, ");
                            sbSpecialNotation1Long.Append("MBX" + "\r\n");
                            sbSpecialNotation1Short.Append("M");
                            break;
                        case "2":
                            sbSpecialNotation1LongNoNewLine.Append("DBX, ");
                            sbSpecialNotation1Long.Append("DBX" + "\r\n");
                            sbSpecialNotation1Short.Append("D");
                            break;
                        case "3":
                            sbSpecialNotation1LongNoNewLine.Append("ACCELERATION, ");
                            sbSpecialNotation1Long.Append("ACCELERATION" + "\r\n");
                            sbSpecialNotation1Short.Append("A");
                            break;
                        default:
                            sbSpecialNotation1Short.Append("-");
                            break;
                    }


                    SpecialNotation1LongNoNewLine = sbSpecialNotation1LongNoNewLine.ToString().TrimEnd(',', ' ');

                    SpecialNotation1Long = sbSpecialNotation1Long.ToString();

                    SpecialNotation1Short = sbSpecialNotation1Short.ToString();

                    sbSpecialNotation1LongNoNewLine.Clear();

                    sbSpecialNotation1Long.Clear();

                    sbSpecialNotation1Short.Clear();
                }
                catch { }

                try
                {
                    // Special Notations
                    StringBuilder sbSpecialNotation2Long = new StringBuilder();
                    strTmp = Remarks[18].ToString();
                    switch (strTmp)
                    {
                        case "B":
                            sbSpecialNotation2Long.Append("Bankruptcy filing against the company" + "\r\n");
                            break;

                        case "M":
                            sbSpecialNotation2Long.Append("Moratorioum of debt payment" + "\r\n");
                            break;
                    }

                    strTmp = Remarks[19].ToString();
                    switch (strTmp)
                    {
                        case "E":
                            sbSpecialNotation2Long.Append("Negative equity" + "\r\n");
                            break;
                    }

                    strTmp = Remarks[20].ToString();
                    switch (strTmp)
                    {
                        case "A":
                            sbSpecialNotation2Long.Append("Adverse opinion of the audited financial report" + "\r\n");
                            break;

                        case "D":
                            sbSpecialNotation2Long.Append("Disclaimer opinion of the audited financial report" + "\r\n");
                            break;
                    }

                    strTmp = Remarks[21].ToString();
                    switch (strTmp)
                    {
                        case "L":
                            sbSpecialNotation2Long.Append("Late submission of financial report" + "\r\n");
                            break;
                    }

                    strTmp = Remarks[22].ToString();
                    switch (strTmp)
                    {
                        case "S":
                            sbSpecialNotation2Long.Append("No sales based on latest financial report" + "\r\n");
                            break;
                    }

                    strTmp = Remarks[23].ToString();
                    switch (strTmp)
                    {
                        case "C":
                            sbSpecialNotation2Long.Append("Lawsuit against Listed Company, its subsidiary, and/or member of Board of Directors and Board of Commissioners of Listed Company which has Material impact." + "\r\n");
                            break;
                    }

                    strTmp = Remarks[24].ToString();
                    switch (strTmp)
                    {
                        case "Q":
                            sbSpecialNotation2Long.Append("Restriction of business activity of Listed Company and/or its subsidiary by regulator" + "\r\n");
                            break;
                    }

                    strTmp = Remarks[25].ToString();
                    switch (strTmp)
                    {
                        case "Y":
                            sbSpecialNotation2Long.Append("Listed Company has not held Annual General Meeting of Shareholders until 6(six) months after the end of previous year" + "\r\n");
                            break;
                    }

                    strTmp = Remarks[26].ToString();
                    switch (strTmp)
                    {
                        case "F":
                            sbSpecialNotation2Long.Append("Administrative sanction from OJK due to minor offense" + "\r\n");
                            break;
                        case "G":
                            sbSpecialNotation2Long.Append("Administrative sanction from OJK due to moderate offense" + "\r\n");
                            break;
                        case "V":
                            sbSpecialNotation2Long.Append("Administrative sanction from OJK due to serious offense" + "\r\n");
                            break;
                    }

                    strTmp = Remarks[29].ToString();

                    switch (strTmp)
                    {
                        case "X":
                            IsWatchlistBoard = true;
                            sbSpecialNotation2Long.Append("WATCHLIST" + "\r\n");
                            break;
                    }


                    SpecialNotation2Long = (sbSpecialNotation2Long.ToString()).TrimEnd('\r', '\n');

                    sbSpecialNotation2Long.Clear();
                }
                catch { }

                try
                {
                    if (Remarks.Length > 18)
                    {
                        SpecialNotation2Short = Remarks.Substring(18);
                    }
                }
                catch { }

                /*
                strTmp = remarks[15].ToString();
                if(int.TryParse(strTmp, out intTmp))
                {
                    SectorId = intTmp;
                }
                */
            }

            Updated = DateTime.Now;
        }

        /// <exception cref="System.ArgumentNullException">
        /// name is null. This is a logic exception and should not be displayed to User.
        /// </exception>
        public void UpdateWithBasicInformation(
            string name,
            int sectorId,
            string subSector,
            bool isSyariah,
            bool isLQ45,
            long sharesPerLot,
            long totalShares,
            string remarks,
            decimal? previous,
            bool isPreOpening,
            bool isWatchlistBoard)
        {

            Name = name ?? throw new ArgumentNullException(nameof(name));
            SectorId = sectorId;
            SubSector = subSector;
            IsSyariah = isSyariah;
            IsLQ45 = isLQ45;
            SharesPerLot = sharesPerLot;
            TotalShares = new Shares(
                shares: totalShares,
                sharesPerLot: sharesPerLot
            );
            Remarks = remarks;
            Previous = previous;
            IsPreOpening = isPreOpening;
            IsWatchlistBoard = isWatchlistBoard;
            Updated = DateTime.Now;
        }

        /// <exception cref="System.ArgumentException">
        /// Supplied argument doesn't satisfy validation rules. This is a logic exception and should not be displayed to User.
        /// </exception>
        /// <exception cref="System.InvalidOperationException">
        /// This instance has not been initialized using StockInit yet.
        /// </exception>
        public void UpdateWithStockQuote(
            decimal? previous,
            decimal? open,
            decimal? high,
            decimal? low,
            decimal? last,
            decimal volume,
            decimal value,
            decimal frequency,
            decimal? bestBidPrice,
            decimal? bestBidVolume,
            decimal? bestOfferPrice,
            decimal? bestOfferVolume,
            decimal? lastTradeVolume,
            decimal totalForeignSellVolume,
            decimal totalForeignSellValue,
            decimal? totalForeignSellFrequency,
            decimal totalForeignBuyVolume,
            decimal totalForeignBuyValue,
            decimal? totalForeignBuyFrequency,
            decimal? totalDomesticSellVolume,
            decimal? totalDomesticSellValue,
            decimal? totalDomesticSellFrequency,
            decimal? totalDomesticBuyVolume,
            decimal? totalDomesticBuyValue,
            decimal? totalDomesticBuyFrequency)
        {

            //if (!IsInitialized) throw new InvalidOperationException("This instance has not been initialized using StockInit yet.");

            //if (open.HasValue && last == null) last = open;
            //if (last.HasValue && high == null) high = last;
            //if (last.HasValue && low == null) low = last;

            if (open.HasValue != last.HasValue ||
                high.HasValue != last.HasValue ||
                low.HasValue != last.HasValue)
            {
                open = null;
                high = null;
                low = null;
                last = null;
            }

            //if (bestBidPrice.HasValue != bestBidVolume.HasValue) throw new ArgumentException("BestBidPrice and BestBidVolume must either be null or not null together.");
            //if (bestOfferPrice.HasValue != bestOfferVolume.HasValue) throw new ArgumentException("BestOfferPrice and BestOfferVolume must either be null or not null together.");

            if (previous.HasValue)
            {
                Previous = previous;
            }

            if (last.HasValue)
            {
                OHLC = OHLC.CreateOrUpdate(
                    open: open.Value,
                    high: high.Value,
                    low: low.Value,
                    close: last.Value
                );
                Notice = null;
            }
            else
            {
                OHLC = null;
                Notice = "CLOSED";
            }
            Magnitudes = Magnitudes.CreateOrUpdate(
                volume: (Magnitudes?.Volume).CreateOrUpdate(
                    shares: volume,
                    sharesPerLot: SharesPerLot.Value
                ),
                value: value,
                frequency: frequency
            );
            BestBidPrice = bestBidPrice;
            if (bestBidVolume.HasValue)
            {
                BestBidVolume = BestBidVolume.CreateOrUpdate(
                    shares: bestBidVolume.Value,
                    sharesPerLot: SharesPerLot.Value
                );
            }
            else
            {
                BestBidVolume = null;
            }
            BestOfferPrice = bestOfferPrice;
            if (bestOfferVolume.HasValue)
            {
                BestOfferVolume = BestOfferVolume.CreateOrUpdate(
                    shares: bestOfferVolume.Value,
                    sharesPerLot: SharesPerLot.Value
                );
            }
            else
            {
                BestOfferVolume = null;
            }
            if (lastTradeVolume.HasValue)
            {
                LastTradeVolume = LastTradeVolume.CreateOrUpdate(
                    shares: lastTradeVolume.Value,
                    sharesPerLot: SharesPerLot.Value
                );
            }
            else
            {
                LastTradeVolume = null;
            }
            TotalForeignBuy = TotalForeignBuy.CreateOrUpdate(
                volume: (TotalForeignBuy?.Volume).CreateOrUpdate(
                    shares: totalForeignBuyVolume,
                    sharesPerLot: SharesPerLot.Value
                ),
                value: totalForeignBuyValue,
                frequency: totalForeignBuyFrequency ?? 0
            );
            TotalForeignSell = TotalForeignSell.CreateOrUpdate(
                volume: (TotalForeignSell?.Volume).CreateOrUpdate(
                    shares: totalForeignSellVolume,
                    sharesPerLot: SharesPerLot.Value
                ),
                value: totalForeignSellValue,
                frequency: totalForeignSellFrequency ?? 0
            );
            TotalDomesticBuy = totalDomesticBuyVolume != null ? TotalDomesticBuy.CreateOrUpdate(
                volume: (TotalDomesticBuy?.Volume).CreateOrUpdate(
                    shares: totalDomesticBuyVolume.Value,
                    sharesPerLot: SharesPerLot.Value
                ),
                value: totalDomesticBuyValue ?? 0,
                frequency: totalDomesticBuyFrequency ?? 0
            ) : null;
            TotalDomesticSell = totalDomesticSellVolume != null ? TotalDomesticSell.CreateOrUpdate(
                volume: (TotalDomesticSell?.Volume).CreateOrUpdate(
                    shares: totalDomesticSellVolume.Value,
                    sharesPerLot: SharesPerLot.Value
                ),
                value: totalDomesticSellValue ?? 0,
                frequency: totalDomesticSellFrequency ?? 0
            ) : null;
            Updated = DateTime.Now;
        }

        public void UpdateWith(object obj)
        {
            if (obj is Stock stock)
            {
                if (stock.Id != Id)
                {
                    throw new ArgumentException("obj must have same Id as this instance.", nameof(obj));
                }

                Name = stock.Name;
                SectorId = stock.SectorId;
                SubSector = stock.SubSector;
                IsSyariah = stock.IsSyariah;
                IsLQ45 = stock.IsLQ45;
                SharesPerLot = stock.SharesPerLot;
                TotalShares = stock.TotalShares;
                Remarks = stock.Remarks;
                IsPreOpening = stock.IsPreOpening;
                IsWatchlistBoard = stock.IsWatchlistBoard;
                Previous = stock.Previous;
                OHLC = stock.OHLC;
                Magnitudes = stock.Magnitudes;
                BestBidPrice = stock.BestBidPrice;
                BestBidVolume = stock.BestBidVolume;
                BestOfferPrice = stock.BestOfferPrice;
                BestOfferVolume = stock.BestOfferVolume;
                LastTradeVolume = stock.LastTradeVolume;
                TotalForeignSell = stock.TotalForeignSell;
                TotalForeignBuy = stock.TotalForeignBuy;
                TotalDomesticSell = stock.TotalDomesticSell;
                TotalDomesticBuy = stock.TotalDomesticBuy;
                Updated = stock.Updated;
            }
            else
            {
                throw new ArgumentException("obj must be of type Stock.", nameof(obj));
            }
        }

        public Color StockCodeForeground
        {
            get
            {
                if (SectorId.HasValue)
                {
                    return AppSettings.SectorColor[SectorId.Value];
                }
                else
                {
                    return AppSettings.SectorColor[0];
                }
            }
        }

        public string SpecialNotation1LongNoNewLine { get; } = "";

        public string SpecialNotation1Long { get; } = "";

        public string SpecialNotation1Short { get; } = "";

        public string SpecialNotation2Long { get; } = "";

        public string SpecialNotation2Short { get; } = "-";

        #region Change

        public decimal? Change
        {
            get
            {
                if (OHLC.HasValue &&
                    Previous.HasValue &&
                    Previous.Value != 0)
                {
                    return OHLC.Value.Close - Previous.Value;
                }
                else
                {
                    return null;
                }
            }
        }

        public decimal? ChangePercent
        {
            get
            {
                if (OHLC.HasValue &&
                    Previous.HasValue &&
                    Previous.Value != 0)
                {
                    return (OHLC.Value.Close - Previous.Value) / Previous.Value * 100m;
                }
                else
                {
                    return null;
                }
            }
        }

        public Color ChangeForeground
        {
            get
            {
                if (OHLC.HasValue &&
                    Previous.HasValue &&
                    Previous.Value != 0)
                {
                    if (OHLC.Value.Close > Previous.Value)
                    {
                        return Colors.GainForeground;
                    }
                    else if (OHLC.Value.Close < Previous.Value)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
                else
                {
                    return Colors.UndeterminedForeground;
                }
            }
        }

        public Color ChangeForeground_WhiteTheme
        {
            get
            {
                if (OHLC.HasValue &&
                    Previous.HasValue &&
                    Previous.Value != 0)
                {
                    if (OHLC.Value.Close > Previous.Value)
                    {
                        return Colors.GainForeground_WhiteTheme;
                    }
                    else if (OHLC.Value.Close < Previous.Value)
                    {
                        return Colors.LoseForeground_WhiteTheme;
                    }
                    else
                    {
                        return Colors.UnchangedForeground_WhiteTheme;
                    }
                }
                else
                {
                    return Colors.UndeterminedForeground_WhiteTheme;
                }
            }
        }

        public Color ChangeBackground
        {
            get
            {
                if (OHLC.HasValue &&
                    Previous.HasValue &&
                    Previous.Value != 0)
                {
                    if (OHLC.Value.Close > Previous.Value)
                    {
                        return Colors.GainBackground;
                    }
                    else if (OHLC.Value.Close < Previous.Value)
                    {
                        return Colors.LoseBackground;
                    }
                    else
                    {
                        return Colors.UnchangedBackground;
                    }
                }
                else
                {
                    return Colors.UndeterminedBackground;
                }
            }
        }

        public string ChangeGlyph
        {
            get
            {
                if (OHLC.HasValue &&
                    Previous.HasValue &&
                    Previous.Value != 0)
                {
                    if (OHLC.Value.Close > Previous.Value)
                    {
                        return Glyphs.Gaining;
                    }
                    else if (OHLC.Value.Close < Previous.Value)
                    {
                        return Glyphs.Losing;
                    }
                    else
                    {
                        return Glyphs.Unchanged;
                    }
                }
                else
                {
                    return Glyphs.Undetermined;
                }
            }
        }

        #endregion

        public Color OpenPriceForeground
        {
            get
            {
                if (OHLC.HasValue &&
                    Previous.HasValue &&
                    Previous.Value != 0)
                {
                    if (OHLC.Value.Open > Previous.Value)
                    {
                        return Colors.GainForeground;
                    }
                    else if (OHLC.Value.Open < Previous.Value)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
                else
                {
                    return Colors.UndeterminedForeground;
                }
            }
        }

        public Color HighPriceForeground
        {
            get
            {
                if (OHLC.HasValue &&
                    Previous.HasValue &&
                    Previous.Value != 0)
                {
                    if (OHLC.Value.High > Previous.Value)
                    {
                        return Colors.GainForeground;
                    }
                    else if (OHLC.Value.High < Previous.Value)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
                else
                {
                    return Colors.UndeterminedForeground;
                }
            }
        }

        public Color LowPriceForeground
        {
            get
            {
                if (OHLC.HasValue &&
                    Previous.HasValue &&
                    Previous.Value != 0)
                {
                    if (OHLC.Value.Low > Previous.Value)
                    {
                        return Colors.GainForeground;
                    }
                    else if (OHLC.Value.Low < Previous.Value)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
                else
                {
                    return Colors.UndeterminedForeground;
                }
            }
        }

        #region Bid and Ask

        public Color BestBidPriceForeground
        {
            get
            {
                if (BestBidPrice.HasValue &&
                    Previous.HasValue &&
                    Previous.Value != 0)
                {
                    if (BestBidPrice.Value > Previous.Value)
                    {
                        return Colors.GainForeground;
                    }
                    else if (BestBidPrice.Value < Previous.Value)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
                else
                {
                    return Colors.UndeterminedForeground;
                }
            }
        }

        public bool HasBestBidPrice => BestBidPrice.HasValue;
        public bool HasNoBestBidPrice => !BestBidPrice.HasValue && Previous.HasValue;

        public Color BestOfferPriceForeground
        {
            get
            {
                if (BestOfferPrice.HasValue &&
                    Previous.HasValue &&
                    Previous.Value != 0)
                {
                    if (BestOfferPrice.Value > Previous.Value)
                    {
                        return Colors.GainForeground;
                    }
                    else if (BestOfferPrice.Value < Previous.Value)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
                else
                {
                    return Colors.UndeterminedForeground;
                }
            }
        }

        public bool HasBestOfferPrice => BestOfferPrice.HasValue;
        public bool HasNoBestOfferPrice => !BestOfferPrice.HasValue && Previous.HasValue;

        #endregion

        #region Timestamp

        public string Timestamp
        {
            get
            {
                if (Updated.HasValue)
                {
                    if (Updated.Value.Date == DateTime.Today)
                    {
                        return Updated.Value.ToString("H:mm:ss");
                    }
                    else if (Updated.Value.Date == DateTime.Today.AddDays(-1))
                    {
                        return Updated.Value.ToString("Yesterday H:mm");
                    }
                    else if (
                        Updated.Value.Date < DateTime.Today &&
                        Updated.Value.Date >= DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek)
                    )
                    {
                        return $"{Updated.Value.DayOfWeek} {Updated.Value:H:mm:ss}";
                    }
                    else
                    {
                        return Updated.Value.ToString("MMM d, yyyy H:mm:ss");
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public bool HasTimestamp => Updated.HasValue;

        #endregion

        #region SparkLine

        public decimal?[] SparkLineCloses { get; set; } = null;
        public string SparkLineLabelText { get; set; }
        public Color SparkLineLabelTextColor { get; set; } = Colors.UndeterminedForeground;

        #endregion


        #region Syariah
        public TextDecorations TextDecoration
        {
            get
            {
                if (Store.TradingAccounts.IsSyariahAcc && !IsSyariah)
                {
                    return TextDecorations.Strikethrough;
                }
                else
                {
                    return TextDecorations.None;
                }
            }

        }
        #endregion

    }
}
