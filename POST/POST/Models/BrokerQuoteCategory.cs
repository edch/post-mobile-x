﻿namespace POST.Models
{
    public enum BrokerQuoteCategory
    {
        Total,
        Buy,
        Sell,
        Net
    }
}
