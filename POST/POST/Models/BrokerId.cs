﻿using System;

namespace POST.Models
{

    public readonly struct BrokerId : IComparable
    {

        public string Code { get; }
        public BrokerQuoteMarketType MarketType { get; }

        public BrokerId(string code, BrokerQuoteMarketType marketType)
        {
            Code = code ?? throw new ArgumentNullException(nameof(code));
            MarketType = marketType;
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode() ^ MarketType.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is BrokerId other &&
Code == other.Code &&
MarketType == other.MarketType;
        }

        public static bool operator ==(BrokerId brokerId1, BrokerId brokerId2)
        {
            return brokerId1.Code == brokerId2.Code &&
brokerId1.MarketType == brokerId2.MarketType;
        }

        public static bool operator !=(BrokerId brokerId1, BrokerId brokerId2)
        {
            return brokerId1.Code != brokerId2.Code ||
brokerId1.MarketType != brokerId2.MarketType;
        }

        public int CompareTo(object obj)
        {
            if (obj is BrokerId other)
            {
                if (Code == other.Code)
                {
                    return (int)MarketType - (int)other.MarketType;
                }
                else
                {
                    return Code.CompareTo(other.Code);
                }
            }
            else
            {
                return -1;
            }
        }
    }
}
