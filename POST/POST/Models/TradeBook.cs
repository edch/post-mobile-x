﻿using System.Collections.Generic;
using System.Linq;

namespace POST.Models
{

    public class TradeBook : Dictionary<decimal, TradeBookLevel>
    {
        public StockId StockId { get; }
        public decimal? Previous { get; set; } = null;

        public TradeBook(StockId stockId)
        {
            StockId = stockId;
        }

        public void UpdateLevel(decimal price, decimal volume, decimal frequency)
        {
            if (ContainsKey(price))
            {
                this[price].Price = price;
                this[price].Volume = volume;
                this[price].Frequency = frequency;
            }
            else
            {
                Add(price, new TradeBookLevel(price, volume, frequency));
            }
        }

        public IList<TradeBookLevel> GetListTradeBookLevel()
        {
            return Values.OrderByDescending(item => item.Price).ToList();
        }

    }
}
