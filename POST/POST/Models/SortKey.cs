﻿namespace POST.Models
{

    public enum SortKey
    {
        CodeAsc,
        CodeDesc,
        ChangePercentAsc,
        ChangePercentDesc
    }
}
