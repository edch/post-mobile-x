﻿namespace POST.Models
{
    public enum CorporateActionType
    {
        CASHDIVIDEND,
        RUPS,
        RIGHTSISSUE,
        WARRANTS,
        SPLIT,
        REVERSE,
        STOCKDIVIDEND
    }
}
