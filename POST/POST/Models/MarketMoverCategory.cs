﻿namespace POST.Models
{
    public enum MarketMoversCategory
    {
        TopGainers,
        TopLosers,
        MostTradedByVolume,
        MostTradedByFrequency,
        MostTradedByValue
    }
}
