﻿using System;

namespace POST.Models.Collections
{

    public interface IGroup
    {
        string GroupName { get; }
        Type GroupPageType { get; }
    }
}
