﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace POST.Models.Collections
{

    public class ObservableSet<TKey, TElement> : ObservableCollection<TElement> where TElement : IUniqueIndexable<TKey>
    {

        /// <summary>
        /// Initializes a new instance of the ObservableSet
        /// class.
        /// </summary>
        public ObservableSet() : base() { }

        /// <summary>
        /// Initializes a new instance of the ObservableSet
        /// class that contains elements copied from the specified list.
        /// </summary>
        /// <param name="list">The list from which the elements are copied.</param>
        /// <exception cref="System.ArgumentNullException">The list parameter cannot be null.</exception>
        /// <exception cref="System.ArgumentException">The list parameter cannot contain elements with duplicate Code.</exception>
        public ObservableSet(List<TElement> list) : base(list)
        {
            if ((from element in list
                 group element by element.Id into g
                 select g.Count()).Max() > 1)
            {
                throw new ArgumentException("list contains elements with duplicate Code.", nameof(list));
            }
        }

        /// <summary>
        /// Initializes a new instance of the ObservableSet
        /// class that contains elements copied from the specified collection.
        /// </summary>
        /// <param name="collection">The collection from which the elements are copied.</param>
        /// <exception cref="System.ArgumentNullException">The collection parameter cannot be null.</exception>
        /// <exception cref="System.ArgumentException">The list parameter cannot contain elements with duplicate Code.</exception>
        public ObservableSet(IEnumerable<TElement> collection) : base(collection)
        {
            if ((from element in collection
                 group element by element.Id into g
                 select g.Count()).Max() > 1)
            {
                throw new ArgumentException("list contains elements with duplicate Code.", nameof(collection));
            }
        }

        public void SetItems(List<TElement> elements)
        {
            for (int i = 0; i < elements.Count; i++)
            {
                TElement element = elements[i];
                if (i < Count)
                {
                    if (EqualityComparer<TKey>.Default.Equals(this[i].Id, elements[i].Id))
                    {
                        // Same Code, same index -> Update
                        this[i].UpdateWith(element);
                    }
                    else if (!elements.Any(e => EqualityComparer<TKey>.Default.Equals(e.Id, this[i].Id)))
                    {
                        // Item in current collection is missing from new elements, remove item from current collection
                        RemoveItem(i--);
                    }
                    else if (!this.Any(e => EqualityComparer<TKey>.Default.Equals(e.Id, element.Id)))
                    {
                        // Item is missing from current collection, insert
                        InsertItem(i, elements[i]);
                    }
                    else
                    {
                        // Item exists on different index, move it to correct index then update
                        int index = IndexOf(this.First(e => EqualityComparer<TKey>.Default.Equals(e.Id, elements[i].Id)));
                        Move(index, i);
                        this[i].UpdateWith(element);
                    }
                }
                else
                {
                    // Add remaining items
                    Add(element);
                }
            }
            while (Count > elements.Count)
            {
                RemoveItem(elements.Count);
            }

#if DEBUG
            if (Count != elements.Count)
            {
                throw new Exception("SetItems: faulty algorithm");
            }
            for (int i = 0; i < Count; i++)
            {
                if (!EqualityComparer<TKey>.Default.Equals(this[i].Id, elements[i].Id))
                {
                    throw new Exception("SetItems: faulty algorithm");
                }
            }
#endif
        }
    }
}
