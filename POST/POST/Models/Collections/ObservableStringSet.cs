﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace POST.Models.Collections
{

    public class ObservableStringSet : ObservableCollection<string>
    {

        /// <summary>
        /// Initializes a new instance of the ObservableStringSet
        /// class.
        /// </summary>
        public ObservableStringSet() : base() { }

        /// <summary>
        /// Initializes a new instance of the ObservableStringSet
        /// class that contains elements copied from the specified list.
        /// </summary>
        /// <param name="list">The list from which the elements are copied.</param>
        /// <exception cref="System.ArgumentNullException">The list parameter cannot be null.</exception>
        /// <exception cref="System.ArgumentException">The list parameter cannot contain duplicate elements.</exception>
        public ObservableStringSet(List<string> list) : base(list)
        {
            if ((from element in list
                 group element by element into g
                 select g.Count()).Max() > 1)
            {
                throw new ArgumentException("list contains duplicate elements.", nameof(list));
            }
        }

        /// <summary>
        /// Initializes a new instance of the ObservableStringSet
        /// class that contains elements copied from the specified collection.
        /// </summary>
        /// <param name="collection">The collection from which the elements are copied.</param>
        /// <exception cref="System.ArgumentNullException">The collection parameter cannot be null.</exception>
        /// <exception cref="System.ArgumentException">The list parameter cannot contain elements with duplicate Code.</exception>
        public ObservableStringSet(IEnumerable<string> collection) : base(collection)
        {
            if ((from element in collection
                 group element by element into g
                 select g.Count()).Max() > 1)
            {
                throw new ArgumentException("list contains duplicate elements.", nameof(collection));
            }
        }

        public void SetItems(IReadOnlyList<string> elements)
        {
            for (int i = 0; i < elements.Count; i++)
            {
                string element = elements[i];
                if (i < Count)
                {
                    if (this[i] != elements[i])
                    {
                        if (!elements.Contains(this[i]))
                        {
                            // Item in current collection is missing from new elements, remove item from current collection
                            RemoveItem(i--);
                        }
                        else if (!Contains(element))
                        {
                            // Item is missing from current collection, insert
                            InsertItem(i, elements[i]);
                        }
                        else
                        {
                            // Item exists on different index, move it to correct index
                            int index = IndexOf(elements[i]);
                            Move(index, i);
                        }
                    }
                }
                else
                {
                    // Add remaining items
                    Add(element);
                }
            }
            while (Count > elements.Count)
            {
                RemoveItem(elements.Count);
            }

#if DEBUG
            if (Count != elements.Count)
            {
                throw new Exception("SetItems: faulty algorithm");
            }
            for (int i = 0; i < Count; i++)
            {
                if (this[i] != elements[i])
                {
                    throw new Exception("SetItems: faulty algorithm");
                }
            }
#endif
        }
    }
}
