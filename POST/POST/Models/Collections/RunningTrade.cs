﻿using POST.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace POST.Models.Collections
{

    public class RunningTrade : ObservableCollection<Trade>
    {
        private const int HISTORY_LENGTH = 200;

        private readonly List<Trade> _trades;

        private int _pageSize = 1;
        /// <summary>
        /// Gets or sets number of lines displayed at a time.
        /// </summary>
        public int PageSize
        {
            get => _pageSize;
            set
            {
                if (value < 1)
                {
                    _pageSize = 1;
                }
                else
                {
                    _pageSize = value;
                }
                RenderCollection();
            }
        }

        private int CursorCounter = 0;

        private Color theColor = Colors.RunningTradeColor1;
        private bool isChangeColor = false;

        /// <summary>
        /// Initializes a new instance of RunningTrade
        /// </summary>
        public RunningTrade() : base()
        {
            _trades = new List<Trade>();
            RenderCollection();
        }

        /// <summary>
        /// Adds a trade into RunningTrade
        /// </summary>
        public void AddTrade(Trade trade)
        {
            if (trade == null)
            {
                throw new ArgumentNullException(nameof(trade));
            }

            if (++CursorCounter >= _pageSize / 2)
            {
                CursorCounter = 0;
                isChangeColor = true;
            }
            else
            {
                isChangeColor = false;
            }

            DateTime now = DateTime.Now;
            if (isChangeColor)
            {
                if (now.Millisecond % 2 == 0)
                {
                    theColor = Colors.RunningTradeColor1;
                }
                else
                {
                    theColor = Colors.RunningTradeColor2;
                }
            }
            trade.Foreground = theColor;

            _trades.Add(trade);
            while (_trades.Count > HISTORY_LENGTH)
            {
                _trades.RemoveAt(0);
            }
        }

        public void RenderCollection()
        {
            Trade[] trades = _trades.ToArray();
            Array.Reverse(trades);
            for (int i = 0; i < _pageSize; i++)
            {
                if (i < trades.Length)
                {
                    if (i >= Count)
                    {
                        Add(trades[i]);
                    }
                    else if (this[i] != trades[i])
                    {
                        SetItem(i, trades[i]);
                    }
                }
                else
                {
                    Add(new Trade("", 0, DateTime.Now, MarketType.RG, 0, 0, 0, "", "", "", "", ""));
                }
            }
            while (Count > _pageSize)
            {
                RemoveItem(_pageSize);
            }
        }
    }
}
