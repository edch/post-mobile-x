﻿using System;
using System.Collections.Generic;

namespace POST.Models.Collections
{

    public class ObservableGroup<TKey, TElement> : ObservableSet<TKey, TElement>, IGroup where TElement : IUniqueIndexable<TKey>
    {

        /// <summary>
        /// Gets name of this group.
        /// </summary>
        public string GroupName { get; }

        /// <summary>
        /// Gets or sets the type of page to navigate to when the SEE ALL button is tapped.
        /// Set this to null if you want to hide SEE ALL button.
        /// </summary>
        public Type GroupPageType { get; set; }

        /// <summary>
        /// Initializes a new instance of the ObservableGroup
        /// class.
        /// </summary>
        /// <param name="groupName">Name of this group.</param>
        public ObservableGroup(string groupName) : base()
        {
            GroupName = groupName ?? throw new ArgumentNullException(nameof(groupName));
        }

        /// <summary>
        /// Initializes a new instance of the ObservableGroup
        /// class that contains elements copied from the specified list.
        /// </summary>
        /// <param name="groupName">Name of this group.</param>
        /// <param name="list">The list from which the elements are copied.</param>
        /// <exception cref="System.ArgumentNullException">The list parameter cannot be null.</exception>
        /// <exception cref="System.ArgumentException">The list parameter cannot contain elements with duplicate Code.</exception>
        public ObservableGroup(string groupName, List<TElement> list) : base(list)
        {
            GroupName = groupName ?? throw new ArgumentNullException(nameof(groupName));
        }

        /// <summary>
        /// Initializes a new instance of the ObservableGroup
        /// class that contains elements copied from the specified collection.
        /// </summary>
        /// <param name="groupName">Name of this group.</param>
        /// <param name="collection">The collection from which the elements are copied.</param>
        /// <exception cref="System.ArgumentNullException">The collection parameter cannot be null.</exception>
        /// <exception cref="System.ArgumentException">The list parameter cannot contain elements with duplicate Code.</exception>
        public ObservableGroup(string groupName, IEnumerable<TElement> collection) : base(collection)
        {
            GroupName = groupName ?? throw new ArgumentNullException(nameof(groupName));
        }
    }
}
