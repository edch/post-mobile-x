﻿namespace POST.Models
{

    public class TradeBookLevel
    {
        public decimal Price { get; set; }
        public decimal Volume { get; set; }
        public decimal Frequency { get; set; }

        public TradeBookLevel(
            decimal price,
            decimal volume,
            decimal frequency
        )
        {
            Price = price;
            Volume = volume;
            Frequency = frequency;
        }
    }
}
