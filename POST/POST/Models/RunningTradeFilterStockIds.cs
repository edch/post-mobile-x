﻿using System.Collections.Generic;
using System.Linq;

namespace POST.Models
{

    public class RunningTradeFilterStockIds : Dictionary<string, List<string>>
    {

        public RunningTradeFilterStockIds()
        {
        }

        public void AddStock(string key, string stockId)
        {

            if (TryGetValue(key, out List<string> lstStockIds))
            {
                lstStockIds.Add(stockId);
            }
        }

        public void RemoveStock(string key, string stockId)
        {

            if (TryGetValue(key, out List<string> lstStockIds))
            {
                lstStockIds.Remove(stockId);
            }
        }

        public void ClearStock(string key)
        {

            if (TryGetValue(key, out List<string> lstStockIds))
            {
                lstStockIds.Clear();
            }
        }

        public void AddGroup(string key)
        {
            Add(key, new List<string>());
        }

        public void RemoveGroup(string key)
        {
            Remove(key);
        }

        public void ClearGroup()
        {
            Clear();
        }

        public bool isAnyGroup(string key)
        {
            return Keys.Any(field => field.ToString().ToLower() == key.ToLower());
        }

        public bool isAnyStock(string key, string stockId)
        {
            bool isThere = false;


            if (TryGetValue(key, out List<string> lstStockIds))
            {
                if (lstStockIds.Any(s => string.Compare(s, stockId) == 0))
                {
                    isThere = true;
                };
            }

            return isThere;
        }

        public void RenameGroup(string oldName, string newName)
        {
            List<string> lstTmp = this[oldName];
            Remove(oldName);
            Add(newName, lstTmp);
        }
    }
}
