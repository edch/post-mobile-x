﻿using System;

namespace POST.Models
{

    public class MenuItem
    {

        public int Id { get; }
        public string Icon { get; }
        public string Title { get; }
        public string UppercaseTitle => Title.ToUpper();
        public Type TargetType { get; }

        public MenuItem(
            int id,
            string icon,
            string title,
            Type targetType)
        {

            Id = id;
            Icon = icon;
            Title = title ?? throw new ArgumentNullException(nameof(title));
            TargetType = targetType ?? throw new ArgumentNullException(nameof(targetType));
        }

        public class LogoutMenu { }
    }
}