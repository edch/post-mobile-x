﻿using POST.Constants;
using POST.Stores;
using System;
using Xamarin.Forms;

namespace POST.Models
{

    public class Broker : IUniqueIndexable<BrokerId>
    {

        public BrokerId Id { get; }
        public string Name { get; private set; }
        public int Type { get; private set; }

        public int RowNumber { get; set; }

        // Quote - Basic
        private decimal buyFreq;
        private decimal buyVol;
        private decimal buyVal;

        private decimal sellFreq;
        private decimal sellVol;
        private decimal sellVal;

        public DateTime? Updated { get; private set; }

        public Broker(
            string code,
            BrokerQuoteMarketType marketType,
            string name,
            int type)
        {
            Id = new BrokerId(
                code: code ?? throw new ArgumentNullException(nameof(code)),
                marketType: marketType
            );
            if (name is null)
            {
                Name = "";
            }
            else
            {
                Name = name;
            }

            Type = type;
        }

        public void UpdateWithBasicInformation(
            string name,
            int type)
        {
            if (name is null)
            {
                Name = "";
            }
            else
            {
                Name = name;
            }
            Type = type;
            Updated = DateTime.Now;
        }

        public void UpdateWithBrokerQuote(
            decimal buyFreq,
            decimal buyVol,
            decimal buyVal,
            decimal sellFreq,
            decimal sellVol,
            decimal sellVal)
        {
            this.buyFreq = buyFreq;
            this.buyVol = buyVol;
            this.buyVal = buyVal;

            this.sellFreq = sellFreq;
            this.sellVol = sellVol;
            this.sellVal = sellVal;

            Updated = DateTime.Now;
        }

        public void UpdateWith(object obj)
        {
            if (obj is Broker broker)
            {
                if (broker.Id != Id)
                {
                    throw new ArgumentException("obj must have same Id as this instance.", nameof(obj));
                }

                Name = broker.Name;
                Type = broker.Type;
                buyFreq = broker.BuyFreq;
                buyVol = broker.BuyVol;
                buyVal = broker.BuyVal;
                sellFreq = broker.SellFreq;
                sellVol = broker.SellVol;
                sellVal = broker.SellVal;

                Updated = broker.Updated;
            }
            else
            {
                throw new ArgumentException("obj must be of type Broker.", nameof(obj));
            }
        }

        public decimal BuyFreq
        {
            get
            {
                decimal buyFreqAll = 0m;
                if (Id.MarketType == BrokerQuoteMarketType.ALL)
                {
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.RG), out Broker brokerRG))
                    {
                        buyFreqAll += brokerRG.BuyFreq;
                    }
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.NG), out Broker brokerNG))
                    {
                        buyFreqAll += brokerNG.BuyFreq;
                    }
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.TN), out Broker brokerTN))
                    {
                        buyFreqAll += brokerTN.BuyFreq;
                    }
                    return buyFreqAll;
                }
                else
                {
                    return buyFreq;
                }
            }
        }

        public decimal BuyVol
        {
            get
            {
                decimal buyVolAll = 0m;
                if (Id.MarketType == BrokerQuoteMarketType.ALL)
                {
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.RG), out Broker brokerRG))
                    {
                        buyVolAll += brokerRG.BuyVol;
                    }
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.NG), out Broker brokerNG))
                    {
                        buyVolAll += brokerNG.BuyVol;
                    }
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.TN), out Broker brokerTN))
                    {
                        buyVolAll += brokerTN.BuyVol;
                    }
                    return buyVolAll;
                }
                else
                {
                    return buyVol;
                }
            }
        }

        public decimal BuyVal
        {
            get
            {
                decimal buyValAll = 0m;
                if (Id.MarketType == BrokerQuoteMarketType.ALL)
                {
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.RG), out Broker brokerRG))
                    {
                        buyValAll += brokerRG.BuyVal;
                    }
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.NG), out Broker brokerNG))
                    {
                        buyValAll += brokerNG.BuyVal;
                    }
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.TN), out Broker brokerTN))
                    {
                        buyValAll += brokerTN.BuyVal;
                    }
                    return buyValAll;
                }
                else
                {
                    return buyVal;
                }
            }
        }

        public decimal SellFreq
        {
            get
            {
                decimal sellFreqAll = 0m;
                if (Id.MarketType == BrokerQuoteMarketType.ALL)
                {
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.RG), out Broker brokerRG))
                    {
                        sellFreqAll += brokerRG.SellFreq;
                    }
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.NG), out Broker brokerNG))
                    {
                        sellFreqAll += brokerNG.SellFreq;
                    }
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.TN), out Broker brokerTN))
                    {
                        sellFreqAll += brokerTN.SellFreq;
                    }
                    return sellFreqAll;
                }
                else
                {
                    return sellFreq;
                }
            }
        }

        public decimal SellVol
        {
            get
            {
                decimal sellVolAll = 0m;
                if (Id.MarketType == BrokerQuoteMarketType.ALL)
                {
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.RG), out Broker brokerRG))
                    {
                        sellVolAll += brokerRG.SellVol;
                    }
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.NG), out Broker brokerNG))
                    {
                        sellVolAll += brokerNG.SellVol;
                    }
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.TN), out Broker brokerTN))
                    {
                        sellVolAll += brokerTN.SellVol;
                    }
                    return sellVolAll;
                }
                else
                {
                    return sellVol;
                }
            }
        }

        public decimal SellVal
        {
            get
            {
                decimal sellValAll = 0m;
                if (Id.MarketType == BrokerQuoteMarketType.ALL)
                {
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.RG), out Broker brokerRG))
                    {
                        sellValAll += brokerRG.SellVal;
                    }
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.NG), out Broker brokerNG))
                    {
                        sellValAll += brokerNG.SellVal;
                    }
                    if (Store.BrokerById.GetState().TryGetValue(new BrokerId(Id.Code, BrokerQuoteMarketType.TN), out Broker brokerTN))
                    {
                        sellValAll += brokerTN.SellVal;
                    }
                    return sellValAll;
                }
                else
                {
                    return sellVal;
                }
            }
        }

        public decimal TotalVal => BuyVal + SellVal;

        public decimal TotalVol => BuyVol + SellVol;

        public decimal TotalFreq => BuyFreq + SellFreq;

        public decimal NetVal => BuyVal - SellVal;

        public decimal NetVol => BuyVol - SellVol;

        public decimal NetFreq => BuyFreq - SellFreq;

        public Color BrokerCodeForeground => AppSettings.BrokerTypeColor[Type];

        public Color BuyValForeground => Colors.GainForeground;

        public Color BuyVolForeground => Colors.GainForeground;

        public Color SellValForeground => Colors.LoseForeground;

        public Color SellVolForeground => Colors.LoseForeground;

        public Color NetValForeground
        {
            get
            {
                if (NetVal > 0m)
                {
                    return Colors.GainForeground;
                }
                else if (NetVal < 0m)
                {
                    return Colors.LoseForeground;
                }
                else
                {
                    return Colors.UnchangedForeground;
                }
            }
        }

        public Color NetVolForeground
        {
            get
            {
                if (NetVol > 0m)
                {
                    return Colors.GainForeground;
                }
                else if (NetVol < 0m)
                {
                    return Colors.LoseForeground;
                }
                else
                {
                    return Colors.UnchangedForeground;
                }
            }
        }

        public Color NetFreqForeground
        {
            get
            {
                if (NetFreq > 0m)
                {
                    return Colors.GainForeground;
                }
                else if (NetFreq < 0m)
                {
                    return Colors.LoseForeground;
                }
                else
                {
                    return Colors.UnchangedForeground;
                }
            }
        }

        public Color TotalValForeground => Colors.MatchedOrderStatus;

        public Color TotalVolForeground => Colors.MatchedOrderStatus;


        #region Timestamp

        public string Timestamp
        {
            get
            {
                if (Updated.HasValue)
                {
                    if (Updated.Value.Date == DateTime.Today)
                    {
                        return Updated.Value.ToString("H:mm:ss");
                    }
                    else if (Updated.Value.Date == DateTime.Today.AddDays(-1))
                    {
                        return Updated.Value.ToString("Yesterday H:mm");
                    }
                    else if (
                        Updated.Value.Date < DateTime.Today &&
                        Updated.Value.Date >= DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek)
                    )
                    {
                        return $"{Updated.Value.DayOfWeek} {Updated.Value:H:mm:ss}";
                    }
                    else
                    {
                        return Updated.Value.ToString("MMM d, yyyy H:mm:ss");
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public bool HasTimestamp => Updated.HasValue;

        #endregion
    }
}
