﻿using System.Text.RegularExpressions;

namespace POST.Models
{
    public class PIN
    {
        public static bool IsValid(string strPIN)
        {
            string strFormula = @"^[0-9]{6}$";
            return Regex.IsMatch(strPIN, strFormula);
        }
    }
}
