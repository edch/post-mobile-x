﻿using System.Collections.Generic;
using System.Linq;

namespace POST.Models
{
    public enum StockSearchFilter
    {
        ALL,
        WATCHLIST
    }

    public static class StockSearchFilterDict
    {
        public static Dictionary<string, StockSearchFilter> Dict = new Dictionary<string, StockSearchFilter>()
        {
            {"All",StockSearchFilter.ALL},
            {"IDX Watchlist",StockSearchFilter.WATCHLIST}
        };

        public static string GetKey(StockSearchFilter stockSearchFilter)
        {
            return Dict.FirstOrDefault(x => x.Value == stockSearchFilter).Key;
        }
    }
}
