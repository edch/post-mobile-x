﻿using System;
using System.Text.RegularExpressions;

namespace POST.Models
{
    public class Password
    {
        public static bool IsValid(string strPassword)
        {
            string strFormula = @"(?!^[0-9]*$)(?!^[a-z]*$)(?=.*[A-Z])^([a-zA-Z0-9]{8})$";
            return Regex.IsMatch(strPassword, strFormula);
        }

        public static string encrypt(string rawPassword)
        {
            Random randNum = new Random();
            string param, param1, param2, result = "";

            param = DateTime.Now.Millisecond.ToString();

            if (param.Length > 2)
            {
                param = param.Substring(param.Length - 2);
            }

            if (param.Length == 1)
            {
                param1 = "0";
                param2 = param;
            }
            else
            {
                param1 = param.Substring(0, 1);
                param2 = param.Substring(1, 1);
            }

            param1 = ((char.Parse(param1) + randNum.Next(9)) % 10).ToString();
            param2 = ((char.Parse(param2) + randNum.Next(9)) % 10).ToString();

            if (param1 != "0")
            {
                for (int i = 0; i < rawPassword.Length; i++)
                {
                    result += (char)(rawPassword[i] + char.Parse(param1) + 40);
                }
            }
            else
            {
                for (int i = 0; i < rawPassword.Length; i++)
                {
                    result += (char)(rawPassword[i] + char.Parse(param2) + 40);
                }
            }

            param1 = ((char)(char.Parse(param1) + 40)).ToString();
            param2 = ((char)(char.Parse(param2) + 40)).ToString();

            return param1 + result + param2;
        }

        public static string decrypt(string password)
        {
            int param1, param2;
            string result = "";

            param1 = password[0] - 40;
            param2 = password[password.Length - 1] - 40;

            if (param1 != 48) // 48 angka ascii 0
            {
                for (int i = 1; i < password.Length - 1; i++)
                {
                    result += (char)(password[i] - param1 - 40);
                }
            }
            else
            {
                for (int i = 1; i < password.Length - 1; i++)
                {
                    result += (char)(password[i] - param2 - 40);
                }
            }

            return result;
        }
    }
}
