﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace POST.Models
{

    public class StockChart : ObservableCollection<StockChartTuple>, IUniqueIndexable<string>
    {

        public string StockCode { get; }
        public string Id => StockCode;

        public StockChartTimeUnit TimeUnit { get; }

        public StockChart(
            string stockCode,
            StockChartTimeUnit timeUnit) : base()
        {

            StockCode = stockCode ?? throw new ArgumentNullException(nameof(stockCode));
            TimeUnit = timeUnit;
        }

        public void UpdateWith(object obj)
        {
            // TODO
        }

        public void UpdateWithChartTuple(StockChartTuple chartTuple)
        {
            if (Count == 0)
            {
                // Collection is empty -> Add tuple as first element
                Add(chartTuple);
            }
            else
            {
                StockChartTuple lastChartTuple = this.Last();
                DateTime lastChartTupleTimeSlot = GetTimeSlot(lastChartTuple.Time, TimeUnit);
                DateTime chartTupleTimeSlot = GetTimeSlot(chartTuple.Time, TimeUnit);
                if (chartTupleTimeSlot > lastChartTupleTimeSlot)
                {
                    // Tuple is more recent than last tuple in collection -> Append tuple to the end of collection
                    Add(chartTuple);
                }
                else if (chartTupleTimeSlot == lastChartTupleTimeSlot)
                {
                    if (chartTuple.Time >= lastChartTuple.Time)
                    {
                        // Tuple falls in same time slot as last tuple in collection and has newer data -> Replace last tuple in collection with new tuple
                        this[Count - 1] = chartTuple;
                    }
                }
                else
                {
                    // Tuple is older than last tuple in collection -> Insert tuple to its proper index in collection
                    for (int i = Count - 1; i >= 0; i--)
                    {
                        StockChartTuple chartTupleAtI = this[i];
                        DateTime chartTupleAtITimeSlot = GetTimeSlot(chartTupleAtI.Time, TimeUnit);
                        if (chartTupleTimeSlot > chartTupleAtITimeSlot)
                        {
                            InsertItem(i + 1, chartTuple);
                            return;
                        }
                        else if (chartTupleTimeSlot == chartTupleAtITimeSlot)
                        {
                            if (chartTuple.Time >= chartTupleAtI.Time)
                            {
                                this[i] = chartTuple;
                            }
                            return;
                        }
                    }
                    InsertItem(0, chartTuple);
                }
            }
        }

        private static DateTime GetTimeSlot(DateTime time, StockChartTimeUnit unit)
        {
            switch (unit)
            {
                case StockChartTimeUnit.Minute:
                    return new DateTime(
                        year: time.Year,
                        month: time.Month,
                        day: time.Day,
                        hour: time.Hour,
                        minute: time.Minute,
                        second: 0,
                        kind: time.Kind
                    );
                case StockChartTimeUnit.FiveMinutes:
                    return new DateTime(
                        year: time.Year,
                        month: time.Month,
                        day: time.Day,
                        hour: time.Hour,
                        minute: time.Minute / 5 * 5,
                        second: 0,
                        kind: time.Kind
                    );
                case StockChartTimeUnit.FifteenMinutes:
                    return new DateTime(
                        year: time.Year,
                        month: time.Month,
                        day: time.Day,
                        hour: time.Hour,
                        minute: time.Minute / 15 * 15,
                        second: 0,
                        kind: time.Kind
                    );
                case StockChartTimeUnit.HalfHour:
                    return new DateTime(
                        year: time.Year,
                        month: time.Month,
                        day: time.Day,
                        hour: time.Hour,
                        minute: time.Minute / 30 * 30,
                        second: 0,
                        kind: time.Kind
                    );
                case StockChartTimeUnit.Hour:
                    return new DateTime(
                        year: time.Year,
                        month: time.Month,
                        day: time.Day,
                        hour: time.Hour,
                        minute: 0,
                        second: 0,
                        kind: time.Kind
                    );
                case StockChartTimeUnit.Day:
                    return time.Date;
                case StockChartTimeUnit.Week:
                    return time.Date.AddDays(-(int)time.DayOfWeek);
                default:
                    throw new NotImplementedException($"Time Unit {unit} not supported.");
            }
        }
    }
}
