﻿namespace POST.Models
{
    public static class RightsAvailableStatusDict
    {
        public static string[] Items =
       {
            "ALL",
            "OPEN",
            "CLOSED",
            "EXPIRED",
            "ERROR",
            "SUCCESS"
        };
    }


}
