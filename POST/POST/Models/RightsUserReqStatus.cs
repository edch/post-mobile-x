﻿namespace POST.Models
{
    public static class RightsUserReqStatusDict
    {
        public static string[] Items =
        {
            "ALL",
            "OPEN",
            "WITHDRAWN",
            "WITHDRAWING",
            "INPROCESS",
            "DONE",
            "REJECTED"
        };
    }
}
