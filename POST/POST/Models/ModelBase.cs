﻿using System.Collections.Generic;
using System.ComponentModel;

namespace POST.Models
{
    public abstract class ModelBase : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetProperty<T>(ref T store, T value, params string[] propertyNames)
        {
            if (!EqualityComparer<T>.Default.Equals(store, value))
            {
                store = value;
                foreach (string propertyName in propertyNames)
                {
                    NotifyPropertyChanged(propertyName);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
