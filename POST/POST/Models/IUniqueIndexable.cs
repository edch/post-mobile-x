﻿namespace POST.Models
{

    public interface IUniqueIndexable<TKey>
    {

        TKey Id { get; }
        void UpdateWith(object obj);
    }
}
