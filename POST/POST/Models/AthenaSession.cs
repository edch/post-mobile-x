﻿using POST.DataSources;
using System;

namespace POST.Models
{
    public class AthenaSession
    {
        public string UserId { get; }
        public string BrokerAccountId { get; }
        public bool UseAppWithoutBrokerAccount { get; }
        public AthenaClient Client { get; }

        public AthenaSession(
            string userId,
            string brokerAccountId,
            bool useAppWithoutBrokerAccount
        )
        {
            UserId = userId ?? throw new ArgumentNullException(nameof(userId));
            BrokerAccountId = brokerAccountId;
            UseAppWithoutBrokerAccount = useAppWithoutBrokerAccount;
            Client = new AthenaClient(userId);
        }

        public AthenaSession(
          string userId,
          string brokerAccountId,
          bool useAppWithoutBrokerAccount,
          AthenaClient client
        )
        {
            UserId = userId ?? throw new ArgumentNullException(nameof(userId));
            BrokerAccountId = brokerAccountId;
            UseAppWithoutBrokerAccount = useAppWithoutBrokerAccount;
            Client = client;
        }
    }
}
