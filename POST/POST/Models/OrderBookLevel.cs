﻿using POST.Models.Primitives;
using System;

namespace POST.Models
{

    public class OrderBookLevel
    {

        public Price Price { get; }
        public Volume BidVolume { get; }
        public Volume AskVolume { get; }
        public Volume BidFrequency { get; }
        public Volume AskFrequency { get; }
        public string BidBroker { get; }
        public string AskBroker { get; }

        public OrderBookLevel(
            Price price,
            Volume bidVolume,
            Volume askVolume,
            Volume bidFrequency,
            Volume askFrequency,
            string bidBroker,
            string askBroker
        )
        {
            if (price == null)
            {
                throw new ArgumentNullException(nameof(price));
            }

            if (price.Value <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(price), "Price must be a positive value.");
            }

            if (bidVolume.Value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(bidVolume), "BidVolume cannot be negative.");
            }

            if (askVolume.Value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(askVolume), "AskVolume cannot be negative.");
            }

            if (bidFrequency.Value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(bidFrequency), "BidFrequency cannot be negative.");
            }

            if (askFrequency.Value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(askFrequency), "AskFrequency cannot be negative.");
            }

            Price = price;
            BidVolume = bidVolume;
            AskVolume = askVolume;
            BidFrequency = bidFrequency;
            AskFrequency = askFrequency;
            BidBroker = bidBroker;
            AskBroker = askBroker;
        }

        public OrderBookLevel CloneAndSetBidVolume(Volume bidVolume, Volume bidFrequency, string bidBroker, string askBroker)
        {
            if (bidVolume.Value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(bidVolume), "BidVolume cannot be negative.");
            }

            if (bidFrequency.Value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(bidFrequency), "BidFrequency cannot be negative.");
            }

            return new OrderBookLevel(
                price: Price,
                bidVolume: bidVolume,
                askVolume: AskVolume,
                bidFrequency: bidFrequency,
                askFrequency: AskFrequency,
                bidBroker: bidBroker,
                askBroker: askBroker
            );
        }

        public OrderBookLevel CloneAndSetAskVolume(Volume askVolume, Volume askFrequency, string bidBroker, string askBroker)
        {
            if (askVolume.Value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(askVolume), "AskVolume cannot be negative.");
            }

            if (askFrequency.Value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(askFrequency), "askFrequency cannot be negative.");
            }

            return new OrderBookLevel(
                price: Price,
                bidVolume: BidVolume,
                askVolume: askVolume,
                bidFrequency: BidFrequency,
                askFrequency: askFrequency,
                bidBroker: bidBroker,
                askBroker: askBroker
            );
        }
    }
}
