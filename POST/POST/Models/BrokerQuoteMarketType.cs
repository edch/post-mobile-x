﻿namespace POST.Models
{

    public enum BrokerQuoteMarketType
    {
        ALL = 0,
        RG = 1,
        NG = 2,
        TN = 3
    }
}
