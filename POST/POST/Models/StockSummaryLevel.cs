﻿using POST.Constants;
using POST.Stores;
using System.Drawing;

namespace POST.Models
{
    public class StockSummaryLevel
    {
        public string BrokerCode { get; set; }
        public decimal D_Buy_Value { get; set; }
        public decimal D_Buy_Volume { get; set; }
        public decimal D_Buy_Frequency { get; set; }
        public decimal D_Sell_Value { get; set; }
        public decimal D_Sell_Volume { get; set; }
        public decimal D_Sell_Frequency { get; set; }
        public decimal F_Buy_Value { get; set; }
        public decimal F_Buy_Volume { get; set; }
        public decimal F_Buy_Frequency { get; set; }
        public decimal F_Sell_Value { get; set; }
        public decimal F_Sell_Volume { get; set; }
        public decimal F_Sell_Frequency { get; set; }


        public StockSummaryLevel(
            string brokerCode,
            decimal d_Buy_Value,
            decimal d_Buy_Volume,
            decimal d_Buy_Frequency,
            decimal d_Sell_Value,
            decimal d_Sell_Volume,
            decimal d_Sell_Frequency,
            decimal f_Buy_Value,
            decimal f_Buy_Volume,
            decimal f_Buy_Frequency,
            decimal f_Sell_Value,
            decimal f_Sell_Volume,
            decimal f_Sell_Frequency
            )
        {
            BrokerCode = brokerCode;

            D_Buy_Value = d_Buy_Value;
            D_Buy_Volume = d_Buy_Volume;
            D_Buy_Frequency = d_Buy_Frequency;

            D_Sell_Value = d_Sell_Value;
            D_Sell_Volume = d_Sell_Volume;
            D_Sell_Frequency = d_Sell_Frequency;

            F_Buy_Value = f_Buy_Value;
            F_Buy_Volume = f_Buy_Volume;
            F_Buy_Frequency = f_Buy_Frequency;

            F_Sell_Value = f_Sell_Value;
            F_Sell_Volume = f_Sell_Volume;
            F_Sell_Frequency = f_Sell_Frequency;


            if (Store.BrokerById.GetState().TryGetValue(new BrokerId(brokerCode, BrokerQuoteMarketType.RG), out Broker existingBroker))
            {
                Broker_Foreground = AppSettings.BrokerTypeColor[existingBroker.Type];
            }
        }


        public decimal Total_Buy_Volume => D_Buy_Volume + F_Buy_Volume;

        public decimal Total_Sell_Volume => D_Sell_Volume + F_Sell_Volume;

        public decimal Net_Volume => Total_Buy_Volume - Total_Sell_Volume;

        public Color Net_Volume_Foreground
        {
            get
            {
                if (Net_Volume > 0m)
                {
                    return Colors.GainForeground;
                }
                else if (Net_Volume < 0m)
                {
                    return Colors.LoseForeground;
                }
                else
                {
                    return Colors.UnchangedForeground;
                }
            }
        }

        public Color Broker_Foreground { get; set; }

    }

}
