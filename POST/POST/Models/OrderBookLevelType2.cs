﻿using POST.Models.Primitives;

namespace POST.Models
{

    public class OrderBookLevelType2
    {
        public Price BidPrice { get; }
        public Price AskPrice { get; }
        public Volume BidVolume { get; }
        public Volume AskVolume { get; }
        public Volume BidFrequency { get; }
        public Volume AskFrequency { get; }
        public string BidBroker { get; }
        public string AskBroker { get; }


        public OrderBookLevelType2(
            Price bidPrice,
            Volume bidVolume,
            Volume bidFrequency,
            Price askPrice,
            Volume askVolume,
            Volume askFrequency,
            string bidBroker,
            string askBroker
        )
        {
            BidPrice = bidPrice;
            BidVolume = bidVolume;
            BidFrequency = bidFrequency;
            AskPrice = askPrice;
            AskVolume = askVolume;
            AskFrequency = askFrequency;
            BidBroker = bidBroker;
            AskBroker = askBroker;
        }
    }
}
