﻿using POST.DataSources.ApiResponses;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace POST.Models
{

    public class IndexChart : ObservableCollection<IndexChartTuple>, IUniqueIndexable<string>
    {

        public string IndexCode { get; }
        public string Id => IndexCode;

        public IndexChartTimeUnit TimeUnit { get; }

        public IndexChart(
            string indexCode,
            IndexChartTimeUnit timeUnit) : base()
        {

            IndexCode = indexCode ?? throw new ArgumentNullException(nameof(indexCode));
            TimeUnit = timeUnit;
        }

        public void UpdateWith(object obj)
        {
            // TODO
        }

        public void UpdateWithChartTuple(IndexChartTuple chartTuple)
        {
            if (Count == 0)
            {
                // Collection is empty -> Add tuple as first element
                Add(chartTuple);
            }
            else
            {
                IndexChartTuple lastChartTuple = this.Last();
                DateTime lastChartTupleTimeSlot = GetTimeSlot(lastChartTuple.Time, TimeUnit);
                DateTime chartTupleTimeSlot = GetTimeSlot(chartTuple.Time, TimeUnit);
                if (chartTupleTimeSlot > lastChartTupleTimeSlot)
                {
                    // Tuple is more recent than last tuple in collection -> Append tuple to the end of collection
                    Add(chartTuple);
                }
                else if (chartTupleTimeSlot == lastChartTupleTimeSlot)
                {
                    if (chartTuple.Time >= lastChartTuple.Time)
                    {
                        // Tuple falls in same time slot as last tuple in collection and has newer data -> Replace last tuple in collection with new tuple
                        this[Count - 1] = chartTuple;
                    }
                }
                else
                {
                    // Tuple is older than last tuple in collection -> Insert tuple to its proper index in collection
                    for (int i = Count - 1; i >= 0; i--)
                    {
                        IndexChartTuple chartTupleAtI = this[i];
                        DateTime chartTupleAtITimeSlot = GetTimeSlot(chartTupleAtI.Time, TimeUnit);
                        if (chartTupleTimeSlot > chartTupleAtITimeSlot)
                        {
                            InsertItem(i + 1, chartTuple);
                            return;
                        }
                        else if (chartTupleTimeSlot == chartTupleAtITimeSlot)
                        {
                            if (chartTuple.Time >= chartTupleAtI.Time)
                            {
                                this[i] = chartTuple;
                            }
                            return;
                        }
                    }
                    InsertItem(0, chartTuple);
                }
            }
        }

        public void UpdateWithIntradayIndexChartData(IntradayIndexChartData[] data)
        {
            ClearItems();
            foreach (IntradayIndexChartData d in data)
            {
                DateTime jakartaTime = d.Date.AddHours(7);
                if (jakartaTime.Date == DateTime.Today)
                {
                    Add(new IndexChartTuple(
                        time: jakartaTime,
                        open: d.Open,
                        high: d.High,
                        low: d.Low,
                        close: d.Close,
                        volume: 0m
                    ));
                }
            }
        }

        private static DateTime GetTimeSlot(DateTime time, IndexChartTimeUnit unit)
        {
            switch (unit)
            {
                case IndexChartTimeUnit.Minute:
                    return new DateTime(
                        year: time.Year,
                        month: time.Month,
                        day: time.Day,
                        hour: time.Hour,
                        minute: time.Minute,
                        second: 0,
                        kind: time.Kind
                    );
                case IndexChartTimeUnit.FiveMinutes:
                    return new DateTime(
                        year: time.Year,
                        month: time.Month,
                        day: time.Day,
                        hour: time.Hour,
                        minute: time.Minute / 5 * 5,
                        second: 0,
                        kind: time.Kind
                    );
                case IndexChartTimeUnit.FifteenMinutes:
                    return new DateTime(
                        year: time.Year,
                        month: time.Month,
                        day: time.Day,
                        hour: time.Hour,
                        minute: time.Minute / 15 * 15,
                        second: 0,
                        kind: time.Kind
                    );
                case IndexChartTimeUnit.HalfHour:
                    return new DateTime(
                        year: time.Year,
                        month: time.Month,
                        day: time.Day,
                        hour: time.Hour,
                        minute: time.Minute / 30 * 30,
                        second: 0,
                        kind: time.Kind
                    );
                case IndexChartTimeUnit.Hour:
                    return new DateTime(
                        year: time.Year,
                        month: time.Month,
                        day: time.Day,
                        hour: time.Hour,
                        minute: 0,
                        second: 0,
                        kind: time.Kind
                    );
                case IndexChartTimeUnit.Day:
                    return time.Date;
                case IndexChartTimeUnit.Week:
                    return time.Date.AddDays(-(int)time.DayOfWeek);
                default:
                    throw new NotImplementedException($"Time Unit {unit} not supported.");
            }
        }
    }
}
