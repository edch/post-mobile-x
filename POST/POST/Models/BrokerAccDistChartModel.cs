﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using POST.DataSources.ApiResponses;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reactive.Linq;

namespace POST.Models
{
    internal class BrokerAccDistChartModel
    {
        public static PlotModel CreateTopBuyers(List<BrokerAccDist> lstBrokerAccDist)
        {
            CategoryAxis xaxis = new CategoryAxis() { IsPanEnabled = false, IsZoomEnabled = false };
            xaxis.Position = AxisPosition.Bottom;
            xaxis.MajorGridlineStyle = LineStyle.Solid;
            xaxis.MinorGridlineStyle = LineStyle.Dot;
            xaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.MinorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.TextColor = OxyColors.GhostWhite;
            xaxis.FontSize = 13;

            List<BrokerAccDist> lstBuyers = lstBrokerAccDist.Where(br => br.BuyTVol > 0).OrderByDescending(br => br.BuyTVol).Take(10).ToList();

            foreach (BrokerAccDist br in lstBuyers)
            {
                xaxis.Labels.Add(br.BrokerCode);
            }

            LinearAxis yaxis = new LinearAxis() { MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0, IsPanEnabled = false, IsZoomEnabled = false };
            yaxis.Position = AxisPosition.Left;
            yaxis.MajorGridlineStyle = LineStyle.Dot;
            yaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            yaxis.TextColor = OxyColors.GhostWhite;
            yaxis.IntervalLength = 40;
            yaxis.FontSize = 13;

            Func<double, string> converter = val =>
            {
                if (val > 999999999 || val < -999999999)
                {
                    return val.ToString("0,,,.###B", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999999 || val < -999999)
                {
                    return val.ToString("0,,.##M", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999 || val < -999)
                {
                    return val.ToString("0,.#K", CultureInfo.InvariantCulture);
                }
                else
                {
                    return val.ToString(CultureInfo.InvariantCulture);
                }
            };
            yaxis.LabelFormatter = converter;

            ColumnSeries s1 = new ColumnSeries() { FillColor = OxyColor.FromRgb(0x1b, 0xe2, 0x8c), Title = "Buy Vol (Shares)" };
            s1.IsStacked = false;

            foreach (BrokerAccDist br in lstBuyers)
            {
                s1.Items.Add(new ColumnItem((double)br.BuyTVol));
            }

            ColumnSeries s2 = new ColumnSeries() { FillColor = OxyColor.FromRgb(0xff, 0x41, 0x3a), Title = "Sell Vol (Shares)" };
            s2.IsStacked = false;

            foreach (BrokerAccDist br in lstBuyers)
            {
                s2.Items.Add(new ColumnItem((double)br.SellTVol));
            }

            for (int i = lstBuyers.Count; i < 10; i++)
            {
                xaxis.Labels.Add("");
                s1.Items.Add(new ColumnItem(0));
                s2.Items.Add(new ColumnItem(0));
            }

            PlotModel Model = new PlotModel()
            {
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0,
                LegendTextColor = OxyColors.GhostWhite
            };
            Model.Title = "Top Buyers";
            Model.PlotAreaBorderColor = OxyColors.GhostWhite;
            Model.TitleColor = OxyColors.GhostWhite;
            Model.TitleFontSize = 13;

            Model.Axes.Add(xaxis);
            Model.Axes.Add(yaxis);
            Model.Series.Add(s1);
            Model.Series.Add(s2);

            return Model;
        }

        public static PlotModel CreateTopBuyers_Null()
        {
            CategoryAxis xaxis = new CategoryAxis() { IsPanEnabled = false, IsZoomEnabled = false };
            xaxis.Position = AxisPosition.Bottom;
            xaxis.MajorGridlineStyle = LineStyle.Solid;
            xaxis.MinorGridlineStyle = LineStyle.Dot;
            xaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.MinorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.TextColor = OxyColors.GhostWhite;
            xaxis.FontSize = 13;

            LinearAxis yaxis = new LinearAxis() { MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0, IsPanEnabled = false, IsZoomEnabled = false };
            yaxis.Position = AxisPosition.Left;
            yaxis.MajorGridlineStyle = LineStyle.Dot;
            yaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            yaxis.TextColor = OxyColors.GhostWhite;
            yaxis.IntervalLength = 40;
            yaxis.FontSize = 13;

            Func<double, string> converter = val =>
            {
                if (val > 0 && val < 1)
                {
                    return "0";
                }

                if (val > 999999999 || val < -999999999)
                {
                    return val.ToString("0,,,.###B", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999999 || val < -999999)
                {
                    return val.ToString("0,,.##M", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999 || val < -999)
                {
                    return val.ToString("0,.#K", CultureInfo.InvariantCulture);
                }
                else
                {
                    return val.ToString(CultureInfo.InvariantCulture);
                }
            };
            yaxis.LabelFormatter = converter;

            ColumnSeries s1 = new ColumnSeries() { FillColor = OxyColor.FromRgb(0x1b, 0xe2, 0x8c), Title = "Buy Vol (Shares)" };
            s1.IsStacked = false;

            ColumnSeries s2 = new ColumnSeries() { FillColor = OxyColor.FromRgb(0xff, 0x41, 0x3a), Title = "Sell Vol (Shares)" };
            s2.IsStacked = false;

            for (int i = 0; i < 10; i++)
            {
                xaxis.Labels.Add("");
                s1.Items.Add(new ColumnItem(0));
                s2.Items.Add(new ColumnItem(0));
            }

            PlotModel Model = new PlotModel()
            {
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0,
                LegendTextColor = OxyColors.GhostWhite
            };
            Model.Title = "Top Buyers";
            Model.PlotAreaBorderColor = OxyColors.GhostWhite;
            Model.TitleColor = OxyColors.GhostWhite;
            Model.TitleFontSize = 13;

            Model.Axes.Add(xaxis);
            Model.Axes.Add(yaxis);
            Model.Series.Add(s1);
            Model.Series.Add(s2);

            return Model;
        }

        public static PlotModel CreateTopSellers(List<BrokerAccDist> lstBrokerAccDist)
        {
            CategoryAxis xaxis = new CategoryAxis() { IsPanEnabled = false, IsZoomEnabled = false };
            xaxis.Position = AxisPosition.Bottom;
            xaxis.MajorGridlineStyle = LineStyle.Solid;
            xaxis.MinorGridlineStyle = LineStyle.Dot;
            xaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.MinorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.TextColor = OxyColors.GhostWhite;
            xaxis.FontSize = 13;

            List<BrokerAccDist> lstSellers = lstBrokerAccDist.Where(br => br.SellTVol > 0).OrderByDescending(br => br.SellTVol).Take(10).ToList();

            foreach (BrokerAccDist br in lstSellers)
            {
                xaxis.Labels.Add(br.BrokerCode);
            }

            LinearAxis yaxis = new LinearAxis() { MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0, IsPanEnabled = false, IsZoomEnabled = false };
            yaxis.Position = AxisPosition.Left;
            yaxis.MajorGridlineStyle = LineStyle.Dot;
            yaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            yaxis.TextColor = OxyColors.GhostWhite;
            yaxis.IntervalLength = 40;
            yaxis.FontSize = 13;

            Func<double, string> converter = val =>
            {
                if (val > 999999999 || val < -999999999)
                {
                    return val.ToString("0,,,.###B", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999999 || val < -999999)
                {
                    return val.ToString("0,,.##M", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999 || val < -999)
                {
                    return val.ToString("0,.#K", CultureInfo.InvariantCulture);
                }
                else
                {
                    return val.ToString(CultureInfo.InvariantCulture);
                }
            };
            yaxis.LabelFormatter = converter;

            ColumnSeries s1 = new ColumnSeries() { FillColor = OxyColor.FromRgb(0x1b, 0xe2, 0x8c), Title = "Buy Vol (Shares)" };
            s1.IsStacked = false;

            foreach (BrokerAccDist br in lstSellers)
            {
                s1.Items.Add(new ColumnItem((double)br.BuyTVol));
            }

            ColumnSeries s2 = new ColumnSeries() { FillColor = OxyColor.FromRgb(0xff, 0x41, 0x3a), Title = "Sell Vol (Shares)" };
            s2.IsStacked = false;

            foreach (BrokerAccDist br in lstSellers)
            {
                s2.Items.Add(new ColumnItem((double)br.SellTVol));
            }

            for (int i = lstSellers.Count; i < 10; i++)
            {
                xaxis.Labels.Add("");
                s1.Items.Add(new ColumnItem(0));
                s2.Items.Add(new ColumnItem(0));
            }

            PlotModel Model = new PlotModel()
            {
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0,
                LegendTextColor = OxyColors.GhostWhite
            };
            Model.Title = "Top Sellers";
            Model.PlotAreaBorderColor = OxyColors.GhostWhite;
            Model.TitleColor = OxyColors.GhostWhite;
            Model.TitleFontSize = 13;

            Model.Axes.Add(xaxis);
            Model.Axes.Add(yaxis);
            Model.Series.Add(s1);
            Model.Series.Add(s2);

            return Model;
        }

        public static PlotModel CreateTopSellers_Null()
        {
            CategoryAxis xaxis = new CategoryAxis() { IsPanEnabled = false, IsZoomEnabled = false };
            xaxis.Position = AxisPosition.Bottom;
            xaxis.MajorGridlineStyle = LineStyle.Solid;
            xaxis.MinorGridlineStyle = LineStyle.Dot;
            xaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.MinorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.TextColor = OxyColors.GhostWhite;
            xaxis.FontSize = 13;

            LinearAxis yaxis = new LinearAxis() { MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0, IsPanEnabled = false, IsZoomEnabled = false };
            yaxis.Position = AxisPosition.Left;
            yaxis.MajorGridlineStyle = LineStyle.Dot;
            yaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            yaxis.TextColor = OxyColors.GhostWhite;
            yaxis.IntervalLength = 40;
            yaxis.FontSize = 13;

            Func<double, string> converter = val =>
            {
                if (val > 0 && val < 1)
                {
                    return "0";
                }

                if (val > 999999999 || val < -999999999)
                {
                    return val.ToString("0,,,.###B", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999999 || val < -999999)
                {
                    return val.ToString("0,,.##M", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999 || val < -999)
                {
                    return val.ToString("0,.#K", CultureInfo.InvariantCulture);
                }
                else
                {
                    return val.ToString(CultureInfo.InvariantCulture);
                }
            };
            yaxis.LabelFormatter = converter;

            ColumnSeries s1 = new ColumnSeries() { FillColor = OxyColor.FromRgb(0x1b, 0xe2, 0x8c), Title = "Buy Vol (Shares)" };
            s1.IsStacked = false;

            ColumnSeries s2 = new ColumnSeries() { FillColor = OxyColor.FromRgb(0xff, 0x41, 0x3a), Title = "Sell Vol (Shares)" };
            s2.IsStacked = false;

            for (int i = 0; i < 10; i++)
            {
                xaxis.Labels.Add("");
                s1.Items.Add(new ColumnItem(0));
                s2.Items.Add(new ColumnItem(0));
            }

            PlotModel Model = new PlotModel()
            {
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0,
                LegendTextColor = OxyColors.GhostWhite
            };
            Model.Title = "Top Sellers";
            Model.PlotAreaBorderColor = OxyColors.GhostWhite;
            Model.TitleColor = OxyColors.GhostWhite;
            Model.TitleFontSize = 13;

            Model.Axes.Add(xaxis);
            Model.Axes.Add(yaxis);
            Model.Series.Add(s1);
            Model.Series.Add(s2);

            return Model;
        }

        public static PlotModel CreateTopNetBuyers(List<BrokerAccDist> lstBrokerAccDist)
        {
            CategoryAxis xaxis = new CategoryAxis() { IsPanEnabled = false, IsZoomEnabled = false };
            xaxis.Position = AxisPosition.Bottom;
            xaxis.MajorGridlineStyle = LineStyle.Solid;
            xaxis.MinorGridlineStyle = LineStyle.Dot;
            xaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.MinorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.TextColor = OxyColors.GhostWhite;
            xaxis.FontSize = 13;

            List<BrokerAccDist> lstTopNetBuyers = lstBrokerAccDist.Where(br => br.NetTVol > 0).OrderByDescending(br => br.NetTVol).Take(10).ToList();

            foreach (BrokerAccDist br in lstTopNetBuyers)
            {
                xaxis.Labels.Add(br.BrokerCode);
            }

            LinearAxis yaxis = new LinearAxis() { MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0, IsPanEnabled = false, IsZoomEnabled = false };
            yaxis.Position = AxisPosition.Left;
            yaxis.MajorGridlineStyle = LineStyle.Dot;
            yaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            yaxis.TextColor = OxyColors.GhostWhite;
            yaxis.IntervalLength = 40;
            yaxis.FontSize = 13;

            Func<double, string> converter = val =>
            {
                if (val > 999999999 || val < -999999999)
                {
                    return val.ToString("0,,,.###B", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999999 || val < -999999)
                {
                    return val.ToString("0,,.##M", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999 || val < -999)
                {
                    return val.ToString("0,.#K", CultureInfo.InvariantCulture);
                }
                else
                {
                    return val.ToString(CultureInfo.InvariantCulture);
                }
            };
            yaxis.LabelFormatter = converter;

            ColumnSeries s1 = new ColumnSeries() { FillColor = OxyColor.FromRgb(0x1b, 0xe2, 0x8c), Title = "Net Buy Vol (Shares)" };
            s1.IsStacked = false;

            foreach (BrokerAccDist br in lstTopNetBuyers)
            {
                s1.Items.Add(new ColumnItem((double)br.NetTVol));
            }

            for (int i = lstTopNetBuyers.Count; i < 10; i++)
            {
                xaxis.Labels.Add("");
                s1.Items.Add(new ColumnItem(0));
            }

            PlotModel Model = new PlotModel()
            {
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0,
                LegendTextColor = OxyColors.GhostWhite
            };
            Model.Title = "Top Net Buyers";
            Model.PlotAreaBorderColor = OxyColors.GhostWhite;
            Model.TitleColor = OxyColors.GhostWhite;
            Model.TitleFontSize = 13;

            Model.Axes.Add(xaxis);
            Model.Axes.Add(yaxis);
            Model.Series.Add(s1);

            return Model;
        }

        public static PlotModel CreateTopNetBuyers_Null()
        {
            CategoryAxis xaxis = new CategoryAxis() { IsPanEnabled = false, IsZoomEnabled = false };
            xaxis.Position = AxisPosition.Bottom;
            xaxis.MajorGridlineStyle = LineStyle.Solid;
            xaxis.MinorGridlineStyle = LineStyle.Dot;
            xaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.MinorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.TextColor = OxyColors.GhostWhite;
            xaxis.FontSize = 13;

            LinearAxis yaxis = new LinearAxis() { MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0, IsPanEnabled = false, IsZoomEnabled = false };
            yaxis.Position = AxisPosition.Left;
            yaxis.MajorGridlineStyle = LineStyle.Dot;
            yaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            yaxis.TextColor = OxyColors.GhostWhite;
            yaxis.IntervalLength = 40;
            yaxis.FontSize = 13;

            Func<double, string> converter = val =>
            {
                if (val > 0 && val < 1)
                {
                    return "0";
                }

                if (val > 999999999 || val < -999999999)
                {
                    return val.ToString("0,,,.###B", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999999 || val < -999999)
                {
                    return val.ToString("0,,.##M", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999 || val < -999)
                {
                    return val.ToString("0,.#K", CultureInfo.InvariantCulture);
                }
                else
                {
                    return val.ToString(CultureInfo.InvariantCulture);
                }
            };
            yaxis.LabelFormatter = converter;

            ColumnSeries s1 = new ColumnSeries() { FillColor = OxyColor.FromRgb(0x1b, 0xe2, 0x8c), Title = "Net Buy Vol (Shares)" };
            s1.IsStacked = false;

            for (int i = 0; i < 10; i++)
            {
                xaxis.Labels.Add("");
                s1.Items.Add(new ColumnItem(0));
            }

            PlotModel Model = new PlotModel()
            {
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0,
                LegendTextColor = OxyColors.GhostWhite
            };
            Model.Title = "Top Net Buyers";
            Model.PlotAreaBorderColor = OxyColors.GhostWhite;
            Model.TitleColor = OxyColors.GhostWhite;
            Model.TitleFontSize = 13;

            Model.Axes.Add(xaxis);
            Model.Axes.Add(yaxis);
            Model.Series.Add(s1);

            return Model;
        }

        public static PlotModel CreateTopNetSellers(List<BrokerAccDist> lstBrokerAccDist)
        {
            CategoryAxis xaxis = new CategoryAxis() { IsPanEnabled = false, IsZoomEnabled = false };
            xaxis.Position = AxisPosition.Bottom;
            xaxis.MajorGridlineStyle = LineStyle.Solid;
            xaxis.MinorGridlineStyle = LineStyle.Dot;
            xaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.MinorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.TextColor = OxyColors.GhostWhite;
            xaxis.FontSize = 13;

            List<BrokerAccDist> lstTopNetSellers = lstBrokerAccDist.Where(br => br.NetTVol < 0).OrderBy(br => br.NetTVol).Take(10).ToList();

            foreach (BrokerAccDist br in lstTopNetSellers)
            {
                xaxis.Labels.Add(br.BrokerCode);
            }

            LinearAxis yaxis = new LinearAxis() { MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0, IsPanEnabled = false, IsZoomEnabled = false };
            yaxis.Position = AxisPosition.Left;
            yaxis.MajorGridlineStyle = LineStyle.Dot;
            yaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            yaxis.TextColor = OxyColors.GhostWhite;
            yaxis.IntervalLength = 40;
            yaxis.FontSize = 13;

            Func<double, string> converter = val =>
            {
                if (val > 999999999 || val < -999999999)
                {
                    return val.ToString("0,,,.###B", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999999 || val < -999999)
                {
                    return val.ToString("0,,.##M", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999 || val < -999)
                {
                    return val.ToString("0,.#K", CultureInfo.InvariantCulture);
                }
                else
                {
                    return val.ToString(CultureInfo.InvariantCulture);
                }
            };
            yaxis.LabelFormatter = converter;

            ColumnSeries s2 = new ColumnSeries() { FillColor = OxyColor.FromRgb(0xff, 0x41, 0x3a), Title = "Net Sell Vol (Shares)" };
            s2.IsStacked = false;

            foreach (BrokerAccDist br in lstTopNetSellers)
            {
                s2.Items.Add(new ColumnItem((double)-(br.NetTVol)));
            }

            for (int i = lstTopNetSellers.Count; i < 10; i++)
            {
                xaxis.Labels.Add("");
                s2.Items.Add(new ColumnItem(0));
            }

            PlotModel Model = new PlotModel()
            {
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0,
                LegendTextColor = OxyColors.GhostWhite
            };
            Model.Title = "Top Net Sellers";
            Model.PlotAreaBorderColor = OxyColors.GhostWhite;
            Model.TitleColor = OxyColors.GhostWhite;
            Model.TitleFontSize = 13;

            Model.Axes.Add(xaxis);
            Model.Axes.Add(yaxis);
            Model.Series.Add(s2);

            return Model;
        }

        public static PlotModel CreateTopNetSellers_Null()
        {
            CategoryAxis xaxis = new CategoryAxis() { IsPanEnabled = false, IsZoomEnabled = false };
            xaxis.Position = AxisPosition.Bottom;
            xaxis.MajorGridlineStyle = LineStyle.Solid;
            xaxis.MinorGridlineStyle = LineStyle.Dot;
            xaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.MinorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            xaxis.TextColor = OxyColors.GhostWhite;
            xaxis.FontSize = 13;

            LinearAxis yaxis = new LinearAxis() { MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0, IsPanEnabled = false, IsZoomEnabled = false };
            yaxis.Position = AxisPosition.Left;
            yaxis.MajorGridlineStyle = LineStyle.Dot;
            yaxis.MajorGridlineColor = OxyColor.FromRgb(0x17, 0x17, 0x17);
            yaxis.TextColor = OxyColors.GhostWhite;
            yaxis.IntervalLength = 40;
            yaxis.FontSize = 13;

            Func<double, string> converter = val =>
            {
                if (val > 0 && val < 1)
                {
                    return "0";
                }

                if (val > 999999999 || val < -999999999)
                {
                    return val.ToString("0,,,.###B", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999999 || val < -999999)
                {
                    return val.ToString("0,,.##M", CultureInfo.InvariantCulture);
                }
                else
                if (val > 999 || val < -999)
                {
                    return val.ToString("0,.#K", CultureInfo.InvariantCulture);
                }
                else
                {
                    return val.ToString(CultureInfo.InvariantCulture);
                }
            };
            yaxis.LabelFormatter = converter;

            ColumnSeries s2 = new ColumnSeries() { FillColor = OxyColor.FromRgb(0xff, 0x41, 0x3a), Title = "Net Sell Vol (Shares)" };
            s2.IsStacked = false;

            for (int i = 0; i < 10; i++)
            {
                xaxis.Labels.Add("");
                s2.Items.Add(new ColumnItem(0));
            }

            PlotModel Model = new PlotModel()
            {
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0,
                LegendTextColor = OxyColors.GhostWhite
            };
            Model.Title = "Top Net Sellers";
            Model.PlotAreaBorderColor = OxyColors.GhostWhite;
            Model.TitleColor = OxyColors.GhostWhite;
            Model.TitleFontSize = 13;

            Model.Axes.Add(xaxis);
            Model.Axes.Add(yaxis);
            Model.Series.Add(s2);

            return Model;
        }
    }
}
