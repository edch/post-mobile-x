﻿using System.Collections.ObjectModel;

namespace POST.Models
{

    public class OrderTrackingList : ObservableCollection<OrderTracking>
    {
        public long SharesPerLot { get; }
        public string OrderTrackingCode { get; }
        public string JsxId { get; }

        public OrderTrackingList(string orderTrackingCode, string jsxId, long sharesPerLot)
        {
            OrderTrackingCode = orderTrackingCode;
            JsxId = jsxId;
            SharesPerLot = sharesPerLot;
        }
    }
}
