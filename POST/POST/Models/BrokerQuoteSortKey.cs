﻿namespace POST.Models
{

    public enum BrokerQuoteSortKey
    {
        CodeAsc,
        CodeDesc,
        ValAsc,
        ValDesc,
        VolAsc,
        VolDesc,
        FreqAsc,
        FreqDesc
    }
}
