﻿using System;

namespace POST.Models
{

    public class OrderTracking : ModelBase
    {

        public string OrderTrackingCode { get; }
        public decimal OrderNo { get; }
        public DateTime Time { get; }
        public string Status { get; }
        public decimal Volume { get; }
        public decimal Remaining { get; }

        public OrderTracking(
            string orderTrackingCode,
            decimal orderNo,
            DateTime time,
            string status,
            decimal volume,
            decimal remaining
            )
        {
            OrderTrackingCode = orderTrackingCode;
            OrderNo = orderNo;
            OrderNoString = orderNo.ToString();
            Time = time;
            Status = status;
            Volume = volume;
            Remaining = remaining;
            TimeString = time.ToString("HH:mm:ss");

            VolumeLot = Math.Floor(volume / Store.OrderTrackingStore.GetState().SharesPerLot);

            RemainingLot = Math.Floor(remaining / Store.OrderTrackingStore.GetState().SharesPerLot);

            VolumeLotString = $"{VolumeLot:N0}";

            if (status == "Q")
            {
                if (RemainingLot > 0m)
                {
                    RemainingLotString = $"{VolumeLot - RemainingLot:N0}";
                }
                else
                {
                    RemainingLotString = $"{VolumeLot:N0}";
                }
            }
            else if (status == "D")
            {
                RemainingLotString = "0";
            }
            else if (status == "C")
            {
                if (RemainingLot > 0m)
                {
                    RemainingLotString = $"{VolumeLot - RemainingLot:N0}";
                }
                else
                {
                    RemainingLotString = $"{VolumeLot:N0}";
                }
            }
        }

        #region Caches

        public string TimeString { get; }

        public string OrderNoString { get; }

        public decimal VolumeLot { get; }
        public decimal RemainingLot { get; }

        public string VolumeLotString { get; }
        public string RemainingLotString { get; }

        #endregion
    }
}
