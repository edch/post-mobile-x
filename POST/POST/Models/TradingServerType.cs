﻿using System.Collections.Generic;
using System.Linq;

namespace POST.Models
{
    public enum TradingServerType
    {
        Main,
        Backup
    }

    public static class TradingServerTypeDict
    {
        public static Dictionary<string, TradingServerType> Dict = new Dictionary<string, TradingServerType>()
        {
            {"Main",TradingServerType.Main},
            {"Backup", TradingServerType.Backup}
        };

        public static string GetKey(TradingServerType serverTradingType)
        {
            return Dict.FirstOrDefault(x => x.Value == serverTradingType).Key;
        }
    }
}
