﻿using System;

namespace POST.Models
{
    public class LoginForm
    {
        public string UserId { get; }
        public string Password { get; }
        public string HashedPassword { get; }

        public LoginForm(
            string userId,
            string password,
            string hashedPassword
        )
        {
            UserId = userId ?? throw new ArgumentNullException(nameof(userId));
            Password = password;
            HashedPassword = hashedPassword;
        }
    }
}
