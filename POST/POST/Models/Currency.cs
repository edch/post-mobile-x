﻿using POST.Constants;
using POST.Models.Primitives;
using System;

namespace POST.Models
{

    public class Currency : ModelBase, IUniqueIndexable<string>
    {

        public string Code { get; }
        public string Id => Code;

        public string NumeratorCode => Code.Length > 3 ? Code.Substring(0, 3) : Code;
        public string DenominatorCode => Code.Length > 3 ? Code.Substring(3) : string.Empty;
        public string Flag
        {
            get
            {
                if (Code.Length == 6)
                {
                    if (Code.EndsWith("IDR"))
                    {
                        return CurrencyFlag.For(Code.Substring(0, 3));
                    }
                    else if (Code.StartsWith("USD"))
                    {
                        return CurrencyFlag.For(Code.Substring(3));
                    }
                    else
                    {
                        return CurrencyFlag.For(Code.Substring(0, 3));
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string Type { get; }

        private decimal _bid;
        public decimal Bid
        {
            get => _bid;
            private set => SetProperty(ref _bid, value, nameof(Bid));
        }

        private decimal _ask;
        public decimal Ask
        {
            get => _ask;
            private set => SetProperty(ref _ask, value, nameof(Ask));
        }

        private decimal _last;
        public decimal Last
        {
            get => _last;
            private set => SetProperty(ref _last, value, nameof(Last), nameof(FlooredLast), nameof(LastDecimals));
        }

        public decimal FlooredLast => Math.Floor(Math.Round(Last, 4));
        public decimal LastDecimals => Math.Round((Last - Math.Floor(Last)) * 10_000m);

        private Date _date;
        public Date Date
        {
            get => _date;
            private set => SetProperty(ref _date, value, nameof(Date), nameof(FormattedDate));
        }

        public string FormattedDate => _date.ToString("MMM d, yyyy");

        public Currency(
            string code,
            string type,
            decimal bid,
            decimal ask,
            decimal last,
            Date date)
        {

            Code = code ?? throw new ArgumentNullException(nameof(code));
            Type = type;
            _bid = bid;
            _ask = ask;
            _last = last;
            _date = date;
        }

        public void UpdateWith(
            decimal bid,
            decimal ask,
            decimal last,
            Date date)
        {

            Bid = bid;
            Ask = ask;
            Last = last;
            Date = date;
        }

        public void UpdateWith(object obj)
        {
            if (obj is Currency currency)
            {
                UpdateWith(
                    bid: currency.Bid,
                    ask: currency.Ask,
                    last: currency.Last,
                    date: currency.Date
                );
            }
            else
            {
                throw new ArgumentException($"{nameof(obj)} must be of type {nameof(Currency)}.");
            }
        }
    }
}
