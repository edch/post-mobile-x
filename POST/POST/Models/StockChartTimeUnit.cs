﻿namespace POST.Models
{

    public enum StockChartTimeUnit
    {

        Minute = 0,
        FiveMinutes = 1,
        FifteenMinutes = 2,
        HalfHour = 3,
        Hour = 4,
        Day = 5,
        Week = 6,
        Year = 7
    }
}
