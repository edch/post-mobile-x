﻿namespace POST.Models
{
    public enum ConditionalOrderStatus
    {
        OrderExecuted,
        NotExecuted,
        NoShares
    }
}
