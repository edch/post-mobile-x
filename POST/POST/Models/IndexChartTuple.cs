﻿using System;

namespace POST.Models
{

    public readonly struct IndexChartTuple
    {

        public DateTime Time { get; }
        public decimal Open { get; }
        public decimal High { get; }
        public decimal Low { get; }
        public decimal Close { get; }
        public decimal Volume { get; }

        public IndexChartTuple(
            DateTime time,
            decimal open,
            decimal high,
            decimal low,
            decimal close,
            decimal volume)
        {

            Time = time;
            Open = open;
            High = high;
            Low = low;
            Close = close;
            Volume = volume;
        }

        public override int GetHashCode()
        {
            return Time.GetHashCode() ^
Open.GetHashCode() ^
High.GetHashCode() ^
Low.GetHashCode() ^
Close.GetHashCode() ^
Volume.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is StockChartTuple other &&
Time == other.Time &&
Open == other.Open &&
High == other.High &&
Low == other.Low &&
Close == other.Close &&
Volume == other.Volume;
        }

        public override string ToString()
        {
            return $"{Time:yyyy/MM/dd HH:mm:ss} O:{Open:N0} H:{High:N0} L:{Low:N0} C:{Close:N0} V:{Volume:N0}";
        }
    }
}
