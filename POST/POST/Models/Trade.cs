﻿using POST.Constants;
using POST.Models.Primitives;
using System;
using Xamarin.Forms;

namespace POST.Models
{

    public class Trade : ModelBase
    {

        public string StockCode { get; }
        public int? StockType { get; }
        public DateTime Time { get; }
        public MarketType MarketType { get; }
        public decimal Previous { get; }
        public decimal Price { get; }
        public Lot Lot { get; }
        public string BuyerCode { get; }
        public string BuyerType { get; }
        public string SellerCode { get; }
        public string SellerType { get; }
        public string TradeType { get; }

        public Trade(
            string stockCode,
            int? stockType,
            DateTime time,
            MarketType marketType,
            decimal previous,
            decimal price,
            decimal lot,
            string buyerCode,
            string buyerType,
            string sellerCode,
            string sellerType,
            string tradeType)
        {

            StockCode = stockCode ?? throw new ArgumentNullException(nameof(stockCode));
            StockType = stockType;
            Time = time;
            MarketType = marketType;
            Previous = previous;
            Price = price;
            Lot = new Lot(lot);
            BuyerCode = buyerCode;
            BuyerType = buyerType;
            SellerCode = sellerCode;
            SellerType = sellerType;
            TradeType = tradeType;

            TimeString = time.ToString("HH:mm:ss");
            PriceString = price.ToString("#,##0");
            LotString = lot.ToString("#,##0");
            decimal change = price - previous;
            if (previous != 0)
            {
                ChangeString = $"{change:+#,##0;-#,##0;+0}";
                ChangePercent = (change / previous * 100m).ToString("+0.00;-0.00;+0.00");
            }
            if (price > previous)
            {
                ChangeForeground = Colors.GainForeground;
                ChangeBackground = Colors.GainBackground;
            }
            else if (price < previous)
            {
                ChangeForeground = Colors.LoseForeground;
                ChangeBackground = Colors.LoseBackground;
            }
            else
            {
                ChangeForeground = Colors.UnchangedForeground;
                ChangeBackground = Colors.UnchangedBackground;
            }
            if (buyerType == "D")
            {
                BuyerColor = Colors.D_BrokerType;
            }
            else
            {
                BuyerColor = Colors.F_BrokerType;
            }
            if (sellerType == "D")
            {
                SellerColor = Colors.D_BrokerType;
            }
            else
            {
                SellerColor = Colors.F_BrokerType;
            }
            if (MarketType != MarketType.RG)
            {
                NonRGMarketType = MarketType;
            }
        }

        #region Caches

        public string TimeString { get; }
        public string PriceString { get; }
        public string LotString { get; }
        public string ChangeString { get; }
        public string ChangePercent { get; }
        public Color ChangeForeground { get; }
        public Color ChangeBackground { get; }
        public Color BuyerColor { get; }
        public Color SellerColor { get; }
        public MarketType? NonRGMarketType { get; }

        #endregion

        #region Running Trade Cursor
        public Color Foreground { get; set; } = Color.White;
        #endregion
    }
}
