﻿using System;

namespace POST.Models.Primitives
{

    public readonly struct Lot : IVolume
    {

        private readonly decimal? _lot;
        private decimal _lotValue => _lot ?? throw new InvalidOperationException("Use of an uninitialized POST.Models.Primitives.Shares. The likely cause is that the instance was created using a parameterless constructor.");
        private readonly long? _sharesPerLot;
        private long _sharesPerLotValue => _sharesPerLot ?? throw new InvalidOperationException("Use of an uninitialized POST.Models.Primitives.Shares. The likely cause is that the instance was created using a parameterless constructor.");

        /// <summary>
        /// Creates an instance of Lot without specifying SharesPerLot.
        /// Any attempt to convert this instance to Shares will raise an exception.
        /// </summary>
        public Lot(decimal lot)
        {
            _lot = lot;
            _sharesPerLot = null;
        }

        public Lot(decimal lot, long sharesPerLot)
        {
            if (sharesPerLot <= 0)
            {
                throw new ArgumentException("SharesPerLot must be a positive integer.", nameof(sharesPerLot));
            }

            _lot = lot;
            _sharesPerLot = sharesPerLot;
        }

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public decimal Value => _lotValue;

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor or the constructor without sharesPerLot
        /// </exception>
        public Shares Shares => new Shares(
            shares: _lotValue * _sharesPerLotValue,
            sharesPerLot: _sharesPerLotValue
        );

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor or the constructor without sharesPerLot
        /// </exception>
        public static implicit operator Shares(Lot lot)
        {
            return new Shares(
                shares: lot._lotValue * lot._sharesPerLotValue,
                sharesPerLot: lot._sharesPerLotValue
            );
        }

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public override int GetHashCode()
        {
            return _lotValue.GetHashCode() ^
_sharesPerLot.GetHashCode();
        }

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public override bool Equals(object obj)
        {
            return obj is Lot other &&
_lotValue == other._lotValue &&
_sharesPerLot == other._sharesPerLot;
        }

        public int CompareTo(object obj)
        {
            if (obj is Lot other)
            {
                return _lotValue.CompareTo(other._lotValue);
            }
            else
            {
                return -1;
            }
        }

#if DEBUG
        public override string ToString()
        {
            return $"Lot:{_lot} SharesPerLot:{_sharesPerLot}";
        }
#endif
    }
}
