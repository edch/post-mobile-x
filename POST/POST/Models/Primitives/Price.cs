﻿using System;
using System.Collections.Generic;

namespace POST.Models.Primitives
{

    public class Price
    {

        public decimal Value { get; }

        public decimal OpenPrice { get; }

        public decimal LHigh { get; set; }

        public decimal LLow { get; set; }

        public StockType StockType { get; set; }


        public string LHigh_String
        {
            get
            {
                string strLHigh = "";

                if (LHigh == 0)
                {
                    strLHigh = "-";
                }
                else
                {
                    strLHigh = $"{LHigh:N0}";
                }

                return strLHigh;
            }
        }

        public string LLow_String
        {
            get
            {
                string strLHigh = "";

                if (LLow == 0)
                {
                    strLHigh = "-";
                }
                else
                {
                    strLHigh = $"{LLow:N0}";
                }

                return strLHigh;
            }
        }

        public Price(decimal price)
        {
            Value = price;
        }

        public Price(decimal price, decimal openPrice, string stockCode, StockType stockType)
        {
            Value = price;

            OpenPrice = openPrice;

            decimal limitHighLow = 0m;

            StockType = stockType;

            if (stockCode.Contains("-") || stockType == StockType.ETF)
            {
                LLow = 0;
                LHigh = 0;
            }
            else
            {
                if (openPrice < 200)
                {
                    limitHighLow = 0.35m;
                }
                else if (openPrice < 5000)
                {
                    limitHighLow = 0.25m;
                }
                else
                {
                    limitHighLow = 0.20m;
                }

                LLow = Math.Ceiling(openPrice * (1m - limitHighLow));
                if (LLow < 1m)
                {
                    LLow = 1m;
                }

                AdjustLLow();

                LHigh = Math.Floor(openPrice * (1m + limitHighLow));
                AdjustLHigh();
            }
        }

        private void AdjustLHigh()
        {
            if (LHigh < 200m)
            {
                LHigh -= LHigh % 1M;
            }
            else if (LHigh < 500m)
            {
                LHigh -= LHigh % 2M;
            }
            else if (LHigh < 2000m)
            {
                LHigh -= LHigh % 5M;
            }
            else if (LHigh < 5000m)
            {
                LHigh -= LHigh % 10M;
            }
            else
            {
                LHigh -= LHigh % 25M;
            }
        }

        private void AdjustLLow()
        {
            if (LLow < 200m)
            {
                LLow += LLow % 1M;
            }
            else if (LLow < 500m)
            {
                LLow += LLow % 2M;
            }
            else if (LLow < 2000m)
            {
                if ((LLow % 5M) > 0m)
                {
                    LLow += 5M - (LLow % 5M);
                }
            }
            else if (LLow < 5000m)
            {
                if ((LLow % 10M) > 0m)
                {
                    LLow += 10M - (LLow % 10M);
                }
            }
            else
            {
                if ((LLow % 25M) > 0m)
                {
                    LLow += 25M - (LLow % 25M);
                }

            }
        }

        public static implicit operator decimal(Price price)
        {
            return price.Value;
        }

        public static implicit operator Price(decimal price)
        {
            return new Price(price);
        }

        public static bool operator ==(Price price1, Price price2)
        {
            return price1?.Value == price2?.Value;
        }

        public static bool operator !=(Price price1, Price price2)
        {
            return price1?.Value != price2?.Value;
        }

        public static bool operator <(Price price1, Price price2)
        {
            return price1.Value < price2.Value;
        }

        public static bool operator >(Price price1, Price price2)
        {
            return price1.Value > price2.Value;
        }

        public static bool operator <=(Price price1, Price price2)
        {
            return price1.Value <= price2.Value;
        }

        public static bool operator >=(Price price1, Price price2)
        {
            return price1.Value >= price2.Value;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is Price price && Value == price.Value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public Price Increment()
        {
            if (Value < 200m)
            {
                return Value + 1m;
            }
            else if (Value < 500m)
            {
                return Value + 2m;
            }
            else if (Value < 2000m)
            {
                return Value + 5m;
            }
            else if (Value < 5000m)
            {
                return Value + 10m;
            }
            else
            {
                return Value + 25m;
            }
        }

        public Price IncrementETF()
        {
            return Value + 1m;
        }

        public Price Decrement()
        {
            if (Value == 1m)
            {
                throw new InvalidOperationException("Cannot decrement 1.");
            }
            else if (Value <= 200m)
            {
                return Value - 1m;
            }
            else if (Value <= 500m)
            {
                return Value - 2m;
            }
            else if (Value <= 2000m)
            {
                return Value - 5m;
            }
            else if (Value <= 5000m)
            {
                return Value - 10m;
            }
            else
            {
                return Value - 25m;
            }
        }

        public Price DecrementETF()
        {
            if (Value == 1m)
            {
                throw new InvalidOperationException("Cannot decrement 1.");
            }
            else
            {
                return Value - 1m;
            }
        }

        public IEnumerable<Price> Spread(int levels)
        {
            List<Price> prices = new List<Price>();
            Price p = this;
            for (int i = 0; i < levels; i++)
            {
                try
                {

                    p = (StockType == StockType.ETF) ? p.DecrementETF() : p.Decrement();
                    if (p >= LLow)
                    {
                        prices.Add(p);
                    }
                    else
                    {
                        break;
                    }
                }
                catch (InvalidOperationException)
                {
                    // Cannot decrement 1
                    break;
                }
            }
            prices.Reverse();
            prices.Add(this);
            p = this;
            for (int i = 0; i < levels; i++)
            {
                p = (StockType == StockType.ETF) ? p.IncrementETF() : p.Increment();
                if (p <= LHigh)
                {
                    prices.Add(p);
                }
                else
                {
                    if (LHigh == LLow)
                    {
                        prices.Add(p);
                    }
                    else
                    {
                        break;
                    }

                }
            }
            return prices;
        }
    }
}
