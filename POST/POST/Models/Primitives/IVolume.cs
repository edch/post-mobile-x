﻿using System;

namespace POST.Models.Primitives
{

    public interface IVolume : IComparable
    {
        decimal Value { get; }
    }
}
