﻿namespace POST.Models.Primitives
{

    public readonly struct Volume : IVolume
    {

        public decimal Value { get; }

        public Volume(decimal volume)
        {
            Value = volume;
        }

        public static implicit operator decimal(Volume volume)
        {
            return volume.Value;
        }

        public static implicit operator Volume(decimal value)
        {
            return new Volume(value);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is Volume other &&
Value == other.Value;
        }

        public int CompareTo(object obj)
        {
            if (obj is Volume other)
            {
                return Value.CompareTo(other.Value);
            }
            else
            {
                return -1;
            }
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
