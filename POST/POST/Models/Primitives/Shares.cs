﻿using System;

namespace POST.Models.Primitives
{

    public struct Shares : IVolume
    {

        private long? _shares;
        private long _sharesValue => _shares ?? throw new InvalidOperationException("Use of an uninitialized POST.Models.Primitives.Shares. The likely cause is that the instance was created using a parameterless constructor.");
        private long? _sharesPerLot;
        private long _sharesPerLotValue => _sharesPerLot ?? throw new InvalidOperationException("Use of an uninitialized POST.Models.Primitives.Shares. The likely cause is that the instance was created using a parameterless constructor.");

        public Shares(long shares, long sharesPerLot)
        {
            if (sharesPerLot <= 0)
            {
                throw new ArgumentException("SharesPerLot must be a positive integer.", nameof(sharesPerLot));
            }

            _shares = shares;
            _sharesPerLot = sharesPerLot;
        }

        public Shares(decimal shares, long sharesPerLot)
        {
            if (Math.Round(shares) != shares)
            {
                throw new ArgumentException("Shares must not contain fractions.", nameof(shares));
            }

            if (sharesPerLot <= 0)
            {
                throw new ArgumentException("SharesPerLot must be a positive integer.", nameof(sharesPerLot));
            }

            _shares = (long)shares;
            _sharesPerLot = sharesPerLot;
        }

        public Shares UpdateWith(long shares, long sharesPerLot)
        {
            if (sharesPerLot <= 0)
            {
                throw new ArgumentException("SharesPerLot must be a positive integer.", nameof(sharesPerLot));
            }

            _shares = shares;
            _sharesPerLot = sharesPerLot;
            return this;
        }

        public Shares UpdateWith(decimal shares, long sharesPerLot)
        {
            if (Math.Round(shares) != shares)
            {
                throw new ArgumentException("Shares must not contain fractions.", nameof(shares));
            }

            if (sharesPerLot <= 0)
            {
                throw new ArgumentException("SharesPerLot must be a positive integer.", nameof(sharesPerLot));
            }

            _shares = (long)shares;
            _sharesPerLot = sharesPerLot;
            return this;
        }

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public decimal Value => _sharesValue;

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public Lot Lot => new Lot(
            lot: (decimal)_sharesValue / _sharesPerLotValue,
            sharesPerLot: _sharesPerLotValue
        );

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public static implicit operator decimal(Shares shares)
        {
            return shares._sharesValue;
        }

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public static implicit operator Lot(Shares shares)
        {
            return new Lot(
                lot: (decimal)shares._sharesValue / shares._sharesPerLotValue,
                sharesPerLot: shares._sharesPerLotValue
            );
        }

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public override int GetHashCode()
        {
            return _sharesValue.GetHashCode() ^
_sharesPerLotValue.GetHashCode();
        }

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public override bool Equals(object obj)
        {
            return obj is Shares other &&
_sharesValue == other._sharesValue &&
_sharesPerLotValue == other._sharesPerLotValue;
        }

        public int CompareTo(object obj)
        {
            if (obj is Shares other)
            {
                return _sharesValue.CompareTo(other._sharesValue);
            }
            else
            {
                return -1;
            }
        }

#if DEBUG
        public override string ToString()
        {
            return $"Shares:{_shares} SharesPerLot:{_sharesPerLot}";
        }
#endif
    }

    public static class SharesExtensions
    {
        public static Shares CreateOrUpdate(this Shares? obj, decimal shares, long sharesPerLot)
        {
            if (obj.HasValue)
            {
                return obj.Value.UpdateWith(shares, sharesPerLot);
            }
            else
            {
                return new Shares(shares, sharesPerLot);
            }
        }
        public static Shares CreateOrUpdate(this Shares? obj, long shares, long sharesPerLot)
        {
            if (obj.HasValue)
            {
                return obj.Value.UpdateWith(shares, sharesPerLot);
            }
            else
            {
                return new Shares(shares, sharesPerLot);
            }
        }
    }
}
