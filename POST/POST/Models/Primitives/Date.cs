﻿using System;

namespace POST.Models.Primitives
{

    public readonly struct Date
    {

        private readonly DateTime? _dateTime;
        private DateTime _dateTimeValue => _dateTime ?? throw new InvalidOperationException("Use of an uninitialized POST.Models.Primitives.Date. The likely cause is that the instance was created using a parameterless constructor.");

        public Date(DateTime dateTime)
        {
            _dateTime = dateTime.Date;
        }

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public DateTime Value => _dateTimeValue;

        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public static implicit operator DateTime(Date dateMinute)
        {
            return dateMinute._dateTimeValue;
        }

        public static implicit operator Date(DateTime dateTime)
        {
            return new Date(dateTime);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public override int GetHashCode()
        {
            return _dateTimeValue.GetHashCode();
        }

        /// <summary>
        /// Returns a value indicating whether the value of this instance is equal to the
        /// value of the specified System.DateTime instance.
        /// </summary>
        /// <param name="obj">The object to compare to this instance.</param>
        /// <returns>true if the value parameter equals the value of this instance; otherwise, false.</returns>
        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public override bool Equals(object obj)
        {
            switch (obj)
            {
                case Date dm:
                    return _dateTimeValue.Equals(dm._dateTimeValue);
                case DateTime dt:
                    return _dateTimeValue.Equals(dt);
                default:
                    return false;
            }
        }

        /// <summary>
        /// Converts the value of the current System.DateTime object to its equivalent string 
        /// representation using the specified format and the formatting conventions of the
        /// current culture.
        /// </summary>
        /// <param name="format">A standard or custom date and time format string.</param>
        /// <returns>
        /// A string representation of value of the current System.DateTime object as specified
        /// by format.
        /// </returns>
        /// <exception cref="System.FormatException">
        /// The length of format is 1, and it is not one of the format specifier characters
        /// defined for System.Globalization.DateTimeFormatInfo. -or- format does not contain
        /// a valid custom format pattern.
        /// </exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// The date and time is outside the range of dates supported by the calendar used
        /// by the current culture.
        /// </exception>
        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public string ToString(string format)
        {
            return _dateTimeValue.ToString(format);
        }

        /// <summary>
        /// Converts the value of the current System.DateTime object to its equivalent string
        /// representation using the specified culture-specific format information.
        /// </summary>
        /// <param name="provider">An object that supplies culture-specific formatting information.</param>
        /// <returns>
        /// A string representation of value of the current System.DateTime object as specified
        /// by provider.
        /// </returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// The date and time is outside the range of dates supported by the calendar used
        /// by provider.
        /// </exception>
        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public string ToString(IFormatProvider provider)
        {
            return _dateTimeValue.ToString(provider);
        }

        /// <summary>
        /// Converts the value of the current System.DateTime object to its equivalent string
        /// representation using the formatting conventions of the current culture.
        /// </summary>
        /// <returns>
        /// A string representation of the value of the current System.DateTime object.
        /// </returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// The date and time is outside the range of dates supported by the calendar used
        /// by the current culture.
        /// </exception>
        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public override string ToString()
        {
            return _dateTimeValue.ToString();
        }

        /// <summary>
        /// Converts the value of the current System.DateTime object to its equivalent string
        /// representation using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">A standard or custom date and time format string.</param>
        /// <param name="provider">An object that supplies culture-specific formatting information.</param>
        /// <returns>
        /// A string representation of value of the current System.DateTime object as specified
        /// by format and provider.
        /// </returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// The date and time is outside the range of dates supported by the calendar used
        /// by provider.
        /// </exception>
        /// <exception cref="System.InvalidOperationException">
        /// Instance was created using parameterless constructor.
        /// </exception>
        public string ToString(string format, IFormatProvider provider)
        {
            return _dateTimeValue.ToString(format, provider);
        }
    }
}
