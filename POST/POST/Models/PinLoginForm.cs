﻿namespace POST.Models
{
    public class PinLoginForm
    {
        public string BrokerAccountId { get; }

        public PinLoginForm(
            string brokerAccountId
        )
        {
            BrokerAccountId = brokerAccountId;
        }
    }
}
