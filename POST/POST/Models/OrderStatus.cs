﻿namespace POST.Models
{
    public enum OrderStatus
    {
        Rejected,
        Open,
        Matched,
        Amending,
        Canceled = 5
    }
}
