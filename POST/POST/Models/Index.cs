﻿using POST.Constants;
using POST.Models.Primitives;
using System;
using Xamarin.Forms;

namespace POST.Models
{

    public class Index : ModelBase, IUniqueIndexable<string>
    {

        public string Code { get; }
        public string Id => Code;

        private decimal _previous;
        public decimal Previous
        {
            get => _previous;
            private set => SetProperty(ref _previous, value,
                nameof(Previous),
                nameof(Change),
                nameof(ChangePercent),
                nameof(ChangeForeground),
                nameof(ChangeBackground),
                nameof(ChangeGlyph)
            );
        }

        private OHLC _ohlc;
        public OHLC OHLC
        {
            get => _ohlc;
            private set => SetProperty(ref _ohlc, value,
                nameof(OHLC),
                nameof(FlooredLast),
                nameof(LastDecimals),
                nameof(Change),
                nameof(ChangePercent),
                nameof(ChangeForeground),
                nameof(ChangeBackground),
                nameof(ChangeGlyph),
                nameof(NonZeroLast)
            );
        }

        public decimal FlooredLast => Math.Floor(Math.Round(_ohlc.Close, 3));
        public decimal LastDecimals => Math.Round((_ohlc.Close - Math.Floor(_ohlc.Close)) * 1_000m);
        public bool NonZeroLast => _ohlc.Close != 0;

        private Magnitudes<Volume> _magnitudes;
        public Magnitudes<Volume> Magnitudes
        {
            get => _magnitudes;
            private set => SetProperty(ref _magnitudes, value, nameof(Magnitudes));
        }

        public Index(
            string code,
            decimal previous,
            decimal open,
            decimal high,
            decimal low,
            decimal last,
            decimal volume,
            decimal value,
            decimal frequency)
        {

            Code = code ?? throw new ArgumentNullException(nameof(code));
            _previous = previous;
            _ohlc = new OHLC(
                open: open,
                high: high,
                low: low,
                close: last
            );
            _magnitudes = new Magnitudes<Volume>(
                volume: volume,
                value: value,
                frequency: frequency
            );
        }

        public void UpdateWith(
            decimal previous,
            decimal open,
            decimal high,
            decimal low,
            decimal last,
            decimal volume,
            decimal value,
            decimal frequency)
        {

            Previous = previous;
            OHLC = OHLC.UpdateWith(
                open: open,
                high: high,
                low: low,
                close: last
            );
            Magnitudes = Magnitudes.UpdateWith(
                volume: volume,
                value: value,
                frequency: frequency
            );
        }

        public void UpdateWith(object obj)
        {
            if (obj is Index other)
            {
                Previous = other.Previous;
                OHLC = other.OHLC;
                Magnitudes = other.Magnitudes;
            }
            else
            {
                throw new ArgumentException($"{nameof(obj)} must be of type {nameof(Index)}.");
            }
        }

        #region Change

        public decimal Change => _ohlc.Close - _previous;

        public decimal? ChangePercent
        {
            get
            {
                if (_previous != 0)
                {
                    return (_ohlc.Close - _previous) / _previous * 100m;
                }
                else
                {
                    return null;
                }
            }
        }

        public Color ChangeForeground
        {
            get
            {
                if (_previous != 0)
                {
                    if (_ohlc.Close > _previous)
                    {
                        return Colors.GainForeground;
                    }
                    else if (_ohlc.Close < _previous)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
                else
                {
                    return Colors.UndeterminedForeground;
                }
            }
        }

        public Color ChangeBackground
        {
            get
            {
                if (_previous != 0)
                {
                    if (_ohlc.Close > _previous)
                    {
                        return Colors.GainBackground;
                    }
                    else if (_ohlc.Close < _previous)
                    {
                        return Colors.LoseBackground;
                    }
                    else
                    {
                        return Colors.UnchangedBackground;
                    }
                }
                else
                {
                    return Colors.UndeterminedBackground;
                }
            }
        }

        public string ChangeGlyph
        {
            get
            {
                if (_previous != 0)
                {
                    if (_ohlc.Close > _previous)
                    {
                        return Glyphs.Gaining;
                    }
                    else if (_ohlc.Close < _previous)
                    {
                        return Glyphs.Losing;
                    }
                    else
                    {
                        return Glyphs.Unchanged;
                    }
                }
                else
                {
                    return Glyphs.Undetermined;
                }
            }
        }

        #endregion
    }
}
