﻿using System.Collections.Generic;
using System.Linq;

namespace POST.Models
{
    public enum DatafeedServerType
    {
        Live,
        Mock
    }

    public static class DatafeedServerTypeDict
    {
        public static Dictionary<string, DatafeedServerType> Dict = new Dictionary<string, DatafeedServerType>()
        {
            {"Live",DatafeedServerType.Live},
            {"Mock", DatafeedServerType.Mock}
        };

        public static string GetKey(DatafeedServerType datafeedServerType)
        {
            return Dict.FirstOrDefault(x => x.Value == datafeedServerType).Key;
        }
    }
}
