﻿using POST.Models.Primitives;
using System.Collections.Generic;

namespace POST.Models
{

    /// <summary>
    /// This struct groups Volume, Value, and Frequency of any kind of quotes together.
    /// </summary>
    public class Magnitudes<TVolume> where TVolume : IVolume
    {

        public TVolume Volume { get; private set; }
        public decimal Value { get; private set; }
        public decimal Frequency { get; private set; }

        public Magnitudes(
            TVolume volume,
            decimal value,
            decimal frequency)
        {

            //if ((volume.Value > 0 || value > 0) && frequency == 0) throw new ArgumentException("Failed to construct Magnitudes. Non-zero magnitudes must have non-zero frequency.", nameof(frequency));

            Volume = volume;
            Value = value;
            Frequency = frequency;
        }

        public Magnitudes<TVolume> UpdateWith(
            TVolume volume,
            decimal value,
            decimal frequency)
        {

            //if ((volume.Value > 0 || value > 0) && frequency == 0) throw new ArgumentException("Failed to construct Magnitudes. Non-zero magnitudes must have non-zero frequency.", nameof(frequency));

            Volume = volume;
            Value = value;
            Frequency = frequency;

            return this;
        }

#if DEBUG
        public override string ToString()
        {
            return $"Volume:{Volume.Value} Value:{Value} Frequency:{Frequency}";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Magnitudes<TVolume>))
            {
                return false;
            }

            Magnitudes<TVolume> magnitudes = (Magnitudes<TVolume>)obj;
            return EqualityComparer<TVolume>.Default.Equals(Volume, magnitudes.Volume) &&
                   Value == magnitudes.Value &&
                   Frequency == magnitudes.Frequency;
        }

        public override int GetHashCode()
        {
            int hashCode = 417407930;
            hashCode = hashCode * -1521134295 + EqualityComparer<TVolume>.Default.GetHashCode(Volume);
            hashCode = hashCode * -1521134295 + Value.GetHashCode();
            hashCode = hashCode * -1521134295 + Frequency.GetHashCode();
            return hashCode;
        }
#endif
    }

    public static class MagnitudesExtensions
    {
        public static Magnitudes<TVolume> CreateOrUpdate<TVolume>(this Magnitudes<TVolume> magnitudes, TVolume volume, decimal value, decimal frequency) where TVolume : IVolume
        {
            if (magnitudes != null)
            {
                return magnitudes.UpdateWith(volume, value, frequency);
            }
            else
            {
                return new Magnitudes<TVolume>(volume, value, frequency);
            }
        }
    }
}
