﻿using POST.Constants;
using POST.Models.Primitives;
using System;
using Xamarin.Forms;

namespace POST.Models
{

    public class RegionalIndex : ModelBase, IUniqueIndexable<string>
    {

        public string Code { get; }

        public string Id => Code;

        public string TypeOrRegion { get; private set; }

        private decimal _previous;
        public decimal Previous
        {
            get => _previous;
            private set => SetProperty(ref _previous, value,
                nameof(Previous),
                nameof(FlooredPrevious),
                nameof(Change),
                nameof(ChangePercent),
                nameof(ChangeForeground),
                nameof(ChangeBackground),
                nameof(ChangeGlyph)
            );
        }
        public decimal FlooredPrevious => Math.Floor(_previous);

        private OHLC _ohlc;
        public OHLC OHLC
        {
            get => _ohlc;
            private set => SetProperty(ref _ohlc, value,
                nameof(OHLC),
                nameof(FlooredLast),
                nameof(Change),
                nameof(ChangePercent),
                nameof(ChangeForeground),
                nameof(ChangeBackground),
                nameof(ChangeGlyph),
                nameof(LastDecimals)
            );
        }
        public decimal FlooredLast => Math.Floor(Math.Round(_ohlc.Close, 3));
        public decimal LastDecimals => Math.Round((_ohlc.Close - Math.Floor(_ohlc.Close)) * 1_000m);
        public bool NonZeroLast => _ohlc.Close != 0;

        private Volume _volume;
        public Volume Volume
        {
            get => _volume;
            private set => SetProperty(ref _volume, value, nameof(Volume));
        }

        public RegionalIndex(
            string code,
            string typeOrRegion,
            decimal previous,
            decimal open,
            decimal high,
            decimal low,
            decimal last,
            decimal volume)
        {

            Code = code ?? throw new ArgumentNullException(nameof(code));
            TypeOrRegion = typeOrRegion;
            _previous = previous;
            _ohlc = new OHLC(
                open: open,
                high: high,
                low: low,
                close: last
            );
            _volume = volume;
        }

        public void UpdateWith(
            decimal previous,
            decimal open,
            decimal high,
            decimal low,
            decimal last,
            decimal volume)
        {

            Previous = previous;
            OHLC = new OHLC(
                open: open,
                high: high,
                low: low,
                close: last
            );
            Volume = volume;
        }

        public void UpdateWith(object obj)
        {
            if (obj is RegionalIndex regionalIndex)
            {
                if (regionalIndex.Id != Id)
                {
                    throw new ArgumentException("obj must have same Id as this instance", nameof(obj));
                }

                TypeOrRegion = regionalIndex.TypeOrRegion;
                Previous = regionalIndex.Previous;
                OHLC = regionalIndex.OHLC;
                Volume = regionalIndex.Volume;
            }
            else
            {
                throw new ArgumentException($"{nameof(obj)} must be of type {nameof(RegionalIndex)}.", nameof(obj));
            }
        }

        #region Change

        public decimal Change => _ohlc.Close - _previous;

        public decimal? ChangePercent
        {
            get
            {
                if (_previous != 0)
                {
                    return (_ohlc.Close - _previous) / _previous * 100m;
                }
                else
                {
                    return null;
                }
            }
        }

        public Color ChangeForeground
        {
            get
            {
                if (_previous != 0)
                {
                    if (_ohlc.Close > _previous)
                    {
                        return Colors.GainForeground;
                    }
                    else if (_ohlc.Close < _previous)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
                else
                {
                    return Colors.UndeterminedForeground;
                }
            }
        }

        public Color ChangeBackground
        {
            get
            {
                if (_previous != 0)
                {
                    if (_ohlc.Close > _previous)
                    {
                        return Colors.GainBackground;
                    }
                    else if (_ohlc.Close < _previous)
                    {
                        return Colors.LoseBackground;
                    }
                    else
                    {
                        return Colors.UnchangedBackground;
                    }
                }
                else
                {
                    return Colors.UndeterminedBackground;
                }
            }
        }

        public string ChangeGlyph
        {
            get
            {
                if (_previous != 0)
                {
                    if (_ohlc.Close > _previous)
                    {
                        return Glyphs.Gaining;
                    }
                    else if (_ohlc.Close < _previous)
                    {
                        return Glyphs.Losing;
                    }
                    else
                    {
                        return Glyphs.Unchanged;
                    }
                }
                else
                {
                    return Glyphs.Undetermined;
                }
            }
        }

        #endregion
    }
}
