﻿using POST.Models.Primitives;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace POST.Models
{

    public class OrderBook : ObservableCollection<OrderBookLevel>
    {

        private static readonly Volume ZERO_VOLUME = 0m;

        public StockId StockId { get; }
        public decimal MaxVolume { get; private set; } = 1m;
        public decimal MaxVolumeShortLevel { get; private set; } = 1m;
        public decimal? Previous { get; set; } = null;
        public decimal? LastPrice { get; set; } = null;
        public decimal? BidVolSum { get; set; } = null;
        public decimal? AskVolSum { get; set; } = null;
        public decimal? BidFreqSum { get; set; } = null;
        public decimal? AskFreqSum { get; set; } = null;

        private bool _bestBidHasValue = false;
        private Price _bestBid = null;
        public Price BestBid
        {
            get
            {
                if (!_bestBidHasValue)
                {
                    _bestBid = this.Any(level => level.BidVolume > 0) ? (Price)this.Where(level => level.BidVolume > 0).Max(level => level.Price.Value) : null;
                    _bestBidHasValue = true;
                }
                return _bestBid;
            }
        }

        private bool _bestAskHasValue = false;
        private Price _bestAsk = null;
        public Price BestAsk
        {
            get
            {
                if (!_bestAskHasValue)
                {
                    _bestAsk = this.Any(level => level.AskVolume > 0) ? (Price)this.Where(level => level.AskVolume > 0).Min(level => level.Price.Value) : null;
                    _bestAskHasValue = true;
                }
                return _bestAsk;
            }
        }


        public OrderBook(StockId stockId)
        {
            StockId = stockId;
        }

        private void InvalidateBests()
        {
            _bestBidHasValue = false;
            _bestAskHasValue = false;
        }

        public void UpdateBidLevel(Price price, Volume bidVolume, Volume bidFrequency, string bidBroker)
        {
            try
            {
                if (StockId.MarketType == MarketType.NG)
                {
                    Add(new OrderBookLevel(price, bidVolume, ZERO_VOLUME, bidFrequency, ZERO_VOLUME, bidBroker, null));
                }
                else
                {
                    bool isThere = false;

                    for (int i = 0; i < Count; i++)
                    {
                        if (this[i].Price == price)
                        {
                            SetItem(i, this[i].CloneAndSetBidVolume(bidVolume, bidFrequency, bidBroker, null));
                            isThere = true;
                            return;
                        }
                    }

                    if (!isThere)
                    {
                        Add(new OrderBookLevel(price, bidVolume, ZERO_VOLUME, bidFrequency, ZERO_VOLUME, bidBroker, null));
                    }
                }

                //Price p = this.First().Price;
                //if (price > p)
                //{
                //    for (p = p.Increment(); p < price; p = p.Increment())
                //    {
                //        InsertItem(0, new OrderBookLevel(p, ZERO_VOLUME, ZERO_VOLUME, ZERO_VOLUME, ZERO_VOLUME));
                //    }
                //    InsertItem(0, new OrderBookLevel(p, bidVolume, ZERO_VOLUME, bidFrequency, ZERO_VOLUME));
                //    return;
                //}

                //for (int i = 0; i < Count; i++)
                //{
                //    if (this[i].Price == price)
                //    {
                //        SetItem(i, this[i].CloneAndSetBidVolume(bidVolume, bidFrequency));
                //        return;
                //    }
                //}

                //for (p = this.Last().Price.Decrement(); p > price; p = p.Decrement())
                //{
                //    Add(new OrderBookLevel(p, ZERO_VOLUME, ZERO_VOLUME, ZERO_VOLUME, ZERO_VOLUME));
                //}
                //Add(new OrderBookLevel(p, bidVolume, ZERO_VOLUME, bidFrequency, ZERO_VOLUME));
            }
            catch
            {

            }
            finally
            {
                if (bidVolume.Value > MaxVolume)
                {
                    MaxVolume = bidVolume.Value;
                }

                InvalidateBests();
            }
        }

        public void UpdateAskLevel(Price price, Volume askVolume, Volume askFrequency, string askBroker)
        {
            try
            {
                if (StockId.MarketType == MarketType.NG)
                {
                    Add(new OrderBookLevel(price, ZERO_VOLUME, askVolume, ZERO_VOLUME, askFrequency, null, askBroker));
                }
                else
                {
                    bool isThere = false;

                    for (int i = 0; i < Count; i++)
                    {
                        if (this[i].Price == price)
                        {
                            SetItem(i, this[i].CloneAndSetAskVolume(askVolume, askFrequency, null, askBroker));
                            isThere = true;
                            return;
                        }
                    }

                    if (!isThere)
                    {
                        Add(new OrderBookLevel(price, ZERO_VOLUME, askVolume, ZERO_VOLUME, askFrequency, null, askBroker));
                    }
                }

                //Price p = this.First().Price;
                //if (price > p)
                //{
                //    for (p = p.Increment(); p < price; p = p.Increment())
                //    {
                //        InsertItem(0, new OrderBookLevel(p, ZERO_VOLUME, ZERO_VOLUME, ZERO_VOLUME, ZERO_VOLUME));
                //    }
                //    InsertItem(0, new OrderBookLevel(p, ZERO_VOLUME, askVolume, ZERO_VOLUME, askFrequency));
                //    return;
                //}

                //for (int i = 0; i < Count; i++)
                //{
                //    if (this[i].Price == price)
                //    {
                //        SetItem(i, this[i].CloneAndSetAskVolume(askVolume, askFrequency));
                //        return;
                //    }
                //}

                //for (p = this.Last().Price.Decrement(); p > price; p = p.Decrement())
                //{
                //    Add(new OrderBookLevel(p, ZERO_VOLUME, ZERO_VOLUME, ZERO_VOLUME, ZERO_VOLUME));
                //}
                //Add(new OrderBookLevel(p, ZERO_VOLUME, askVolume, ZERO_VOLUME, askFrequency));
            }
            catch
            {

            }
            finally
            {
                if (askVolume.Value > MaxVolume)
                {
                    MaxVolume = askVolume.Value;
                }

                InvalidateBests();
            }
        }

        public IList<OrderBookLevelType2> GetListOrderBookLevelType2()
        {
            List<OrderBookLevelType2> lstOrderBookLevelType2 = new List<OrderBookLevelType2>();
            IEnumerable<OrderBookLevel> lstBid = this.ToList().Where(item => item.BidVolume.Value > 0m).OrderByDescending(item => item.Price.Value);
            IEnumerable<OrderBookLevel> lstAsk = this.ToList().Where(item => item.AskVolume.Value > 0m).OrderBy(item => item.Price.Value);

            int nLstBid = lstBid.Count();
            int nLstAsk = lstAsk.Count();
            int nMaxRow = (nLstBid > nLstAsk) ? nLstBid : nLstAsk;

            OrderBookLevel orderBidBookLevel;
            OrderBookLevel orderAskBookLevel;

            for (int i = 0; i < nMaxRow; i++)
            {
                if (i < nLstBid && i < nLstAsk)
                {
                    orderBidBookLevel = lstBid.ElementAt(i);
                    orderAskBookLevel = lstAsk.ElementAt(i);
                    lstOrderBookLevelType2.Add(new OrderBookLevelType2(orderBidBookLevel.Price, orderBidBookLevel.BidVolume, orderBidBookLevel.BidFrequency, orderAskBookLevel.Price, orderAskBookLevel.AskVolume, orderAskBookLevel.AskFrequency, orderBidBookLevel.BidBroker, orderAskBookLevel.AskBroker));
                }
                else
                {
                    if (i >= lstBid.Count())
                    {
                        orderAskBookLevel = lstAsk.ElementAt(i);
                        lstOrderBookLevelType2.Add(new OrderBookLevelType2(null, ZERO_VOLUME, ZERO_VOLUME, orderAskBookLevel.Price, orderAskBookLevel.AskVolume, orderAskBookLevel.AskFrequency, null, orderAskBookLevel.AskBroker));
                    }
                    else
                    {
                        orderBidBookLevel = lstBid.ElementAt(i);
                        lstOrderBookLevelType2.Add(new OrderBookLevelType2(orderBidBookLevel.Price, orderBidBookLevel.BidVolume, orderBidBookLevel.BidFrequency, null, ZERO_VOLUME, ZERO_VOLUME, orderBidBookLevel.BidBroker, null));
                    }
                }
            }

            BidVolSum = lstBid.Sum(item => item.BidVolume);
            AskVolSum = lstAsk.Sum(item => item.AskVolume);
            BidFreqSum = lstBid.Sum(item => item.BidFrequency);
            AskFreqSum = lstAsk.Sum(item => item.AskFrequency);
            return lstOrderBookLevelType2.Take(nMaxRow).ToList();
        }

        public IList<OrderBookLevelTwoColumn> GetListOrderBookLevelTwoColumn()
        {
            List<OrderBookLevelTwoColumn> lstOrderBookLevelTwoColumn = new List<OrderBookLevelTwoColumn>(20);
            for (int i = 0; i < 20; i++)
            {
                lstOrderBookLevelTwoColumn.Add(new OrderBookLevelTwoColumn(new Price(0), new Volume(0)));
            }
            List<OrderBookLevel> lstBid = this.ToList().Where(item => item.BidVolume.Value > 0m).OrderByDescending(item => item.Price.Value).ToList();
            List<OrderBookLevel> lstAsk = this.ToList().Where(item => item.AskVolume.Value > 0m).OrderBy(item => item.Price.Value).ToList();

            int nLstBid = lstBid.Count();
            int nLstAsk = lstAsk.Count();

            int iBid = 9;
            int iAsk = 10;

            for (int i = 0; i < 10; i++)
            {
                if (i < lstBid.Count())
                {
                    lstOrderBookLevelTwoColumn[iBid].Price = new Price(lstBid[i].Price);
                    lstOrderBookLevelTwoColumn[iBid].Volume = new Volume(lstBid[i].BidVolume);
                }
                if (i < lstAsk.Count())
                {
                    lstOrderBookLevelTwoColumn[iAsk].Price = new Price(lstAsk[i].Price);
                    lstOrderBookLevelTwoColumn[iAsk].Volume = new Volume(lstAsk[i].AskVolume);
                }
                iBid--;
                iAsk++;
            }

            MaxVolumeShortLevel = lstOrderBookLevelTwoColumn.OrderByDescending(ob => ob.Volume).ElementAt(0).Volume;

            return lstOrderBookLevelTwoColumn;
        }

    }
}
