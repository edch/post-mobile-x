﻿using System;

namespace POST.Models
{

    public struct StockId : IComparable
    {

        public string Code { get; }
        public MarketType MarketType { get; set; }

        public StockId(string code, MarketType marketType)
        {
            Code = code ?? throw new ArgumentNullException(nameof(code));
            MarketType = marketType;
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode() ^ MarketType.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is StockId other &&
Code == other.Code &&
MarketType == other.MarketType;
        }

        public static bool operator ==(StockId stockId1, StockId stockId2)
        {
            return stockId1.Code == stockId2.Code &&
stockId1.MarketType == stockId2.MarketType;
        }

        public static bool operator !=(StockId stockId1, StockId stockId2)
        {
            return stockId1.Code != stockId2.Code ||
stockId1.MarketType != stockId2.MarketType;
        }

        public int CompareTo(object obj)
        {
            if (obj is StockId other)
            {
                if (Code == other.Code)
                {
                    return (int)MarketType - (int)other.MarketType;
                }
                else
                {
                    return Code.CompareTo(other.Code);
                }
            }
            else
            {
                return -1;
            }
        }
    }
}
