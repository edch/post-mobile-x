﻿namespace POST.Models
{

    /// <summary>
    /// Open-High-Low-Close data tuple.
    /// </summary>
    public struct OHLC
    {

        public decimal Open { get; private set; }
        public decimal High { get; private set; }
        public decimal Low { get; private set; }

        /// <summary>
        /// Close or Last
        /// </summary>
        public decimal Close { get; private set; }

        /// <summary>
        /// Creates an instance of OHLC
        /// </summary>
        /// <param name="open">Opening value of time period</param>
        /// <param name="high">Highest reached value during time period</param>
        /// <param name="low">Lowest reached value during time period</param>
        /// <param name="close">Closing or Last value of time period</param>
        /// <exception cref="System.ArgumentException">
        /// Value of either high or low doesn't satisfy validation rules. This is a logic exception and should not be displayed to User.
        /// </exception>
        public OHLC(
            decimal open,
            decimal high,
            decimal low,
            decimal close)
        {

            // HACK: Known Backend Issue. Close can be lower than Low or be higher than High.
            //if (open > high || low > high || close > high) throw new ArgumentException("Failed to construct OHLC. No other value can be higher than High.", nameof(high));
            //if (open < low || high < low || close < low) throw new ArgumentException("Failed to construct OHLC. No other value can be lower than Low.", nameof(low));

            Open = open;
            High = high;
            Low = low;
            Close = close;
        }

        public OHLC UpdateWith(
            decimal open,
            decimal high,
            decimal low,
            decimal close)
        {

            // HACK: Known Backend Issue. Close can be lower than Low or be higher than High.
            //if (open > high || low > high || close > high) throw new ArgumentException("Failed to construct OHLC. No other value can be higher than High.", nameof(high));
            //if (open < low || high < low || close < low) throw new ArgumentException("Failed to construct OHLC. No other value can be lower than Low.", nameof(low));

            Open = open;
            High = high;
            Low = low;
            Close = close;

            return this;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is OHLC))
            {
                return false;
            }

            OHLC oHLC = (OHLC)obj;
            return Open == oHLC.Open &&
                   High == oHLC.High &&
                   Low == oHLC.Low &&
                   Close == oHLC.Close;
        }

        public override int GetHashCode()
        {
            int hashCode = -1567368614;
            hashCode = hashCode * -1521134295 + Open.GetHashCode();
            hashCode = hashCode * -1521134295 + High.GetHashCode();
            hashCode = hashCode * -1521134295 + Low.GetHashCode();
            hashCode = hashCode * -1521134295 + Close.GetHashCode();
            return hashCode;
        }

#if DEBUG
        public override string ToString()
        {
            return $"O:{Open} H:{High} L:{Low} C:{Close}";
        }
#endif
    }

    public static class OHLCExtensions
    {
        public static OHLC CreateOrUpdate(this OHLC? ohlc, decimal open, decimal high, decimal low, decimal close)
        {
            if (ohlc.HasValue)
            {
                return ohlc.Value.UpdateWith(open, high, low, close);
            }
            else
            {
                return new OHLC(open, high, low, close);
            }
        }
    }
}
