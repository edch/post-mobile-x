﻿using System.Collections.Generic;
using System.Linq;

namespace POST.Models
{

    public class StockSummary : Dictionary<string, StockSummaryLevel>
    {
        public StockId StockId { get; }

        public StockSummary(StockId stockId)
        {
            StockId = stockId;
        }

        public void UpdateLevel(
            string brokerCode,
            decimal d_Buy_Value,
            decimal d_Buy_Volume,
            decimal d_Buy_Frequency,
            decimal d_Sell_Value,
            decimal d_Sell_Volume,
            decimal d_Sell_Frequency,
            decimal f_Buy_Value,
            decimal f_Buy_Volume,
            decimal f_Buy_Frequency,
            decimal f_Sell_Value,
            decimal f_Sell_Volume,
            decimal f_Sell_Frequency
            )
        {
            if (ContainsKey(brokerCode))
            {
                this[brokerCode].BrokerCode = brokerCode;
                this[brokerCode].D_Buy_Value = d_Buy_Value;
                this[brokerCode].D_Buy_Volume = d_Buy_Volume;
                this[brokerCode].D_Buy_Frequency = d_Buy_Frequency;
                this[brokerCode].D_Sell_Value = d_Sell_Value;
                this[brokerCode].D_Sell_Volume = d_Sell_Volume;
                this[brokerCode].D_Sell_Frequency = d_Sell_Frequency;
                this[brokerCode].F_Buy_Value = f_Buy_Value;
                this[brokerCode].F_Buy_Volume = f_Buy_Volume;
                this[brokerCode].F_Buy_Frequency = f_Buy_Frequency;
                this[brokerCode].F_Sell_Value = f_Sell_Value;
                this[brokerCode].F_Sell_Volume = f_Sell_Volume;
                this[brokerCode].F_Sell_Frequency = f_Sell_Frequency;
            }
            else
            {
                Add(brokerCode, new StockSummaryLevel(
                    brokerCode,
                    d_Buy_Value,
                    d_Buy_Volume,
                    d_Buy_Frequency,
                    d_Sell_Value,
                    d_Sell_Volume,
                    d_Sell_Frequency,
                    f_Buy_Value,
                    f_Buy_Volume,
                    f_Buy_Frequency,
                    f_Sell_Value,
                    f_Sell_Volume,
                    f_Sell_Frequency
                    ));
            }
        }

        public IList<StockSummaryLevel> GetListStockSummaryLevel()
        {
            return Values.OrderBy(item => item.BrokerCode).ToList();
        }

    }
}
