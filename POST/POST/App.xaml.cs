﻿using POST.DataSources;
using POST.Pages;
using System;
using Xamarin.Forms;

namespace POST
{

    public partial class App : Application
    {

        private readonly System.Timers.Timer timerGCCollect = null;

        public App()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MzYwMTZAMzEzNjJlMzMyZTMwSzZ1a21oYzFVa0swSWttYW1HQTNjdDNwakZaeXBkY3UxaFBTZEY0RSt2ST0=");

            InitializeComponent();

            OxyPlot.Xamarin.Forms.Platform.Android.PlotViewRenderer.Init();

            timerGCCollect = new System.Timers.Timer
            {
                Interval = 30000
            };
            timerGCCollect.Elapsed += (object sender, System.Timers.ElapsedEventArgs e) =>
            {
                GC.Collect(0);
            };
            timerGCCollect.Enabled = true;
            timerGCCollect.Start();

            //MainPage = new MainPage();
            MainPage = new LoginPage();

            DataFeedClient.StateManager.Current.StartClient();
        }

        ~App()
        {
            DataFeedClient.StateManager.Current.StopClient();

            if (timerGCCollect != null && timerGCCollect.Enabled)
            {
                timerGCCollect.Enabled = false;
                timerGCCollect.Stop();
                timerGCCollect.Dispose();
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps            
            DataFeedClient.StateManager.Current.StopClient();
        }

        protected override void OnResume()
        {
            // Handle when your app resumes            
            DataFeedClient.StateManager.Current.StartClient();
        }

        public void GotoAlertPage()
        {
            if (Current.MainPage is MainPage mainPage)
            {
                mainPage.GotoAlertPage();
            }
        }

        public void GotoTradeDonePage()
        {
            if (Current.MainPage is MainPage MainPage)
            {
                MainPage.GotoTradeDonePage();
            }
        }
    }
}
