﻿using System;
using System.ComponentModel;
using System.Reactive.Linq;

namespace POST.Extensions
{

    public static class INotifyPropertyChangedExtensions
    {

        public static IObservable<PropertyChangedEventArgs> WhenPropertyChanged(this INotifyPropertyChanged notifyPropertyChanged)
        {
            return Observable.FromEvent<PropertyChangedEventHandler, PropertyChangedEventArgs>(
addHandler: handler => notifyPropertyChanged.PropertyChanged += handler,
removeHandler: handler => notifyPropertyChanged.PropertyChanged -= handler
);
        }
    }
}
