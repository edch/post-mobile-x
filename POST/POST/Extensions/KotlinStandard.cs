﻿using System;

namespace POST.Extensions
{

    public static class KotlinStandard
    {

        public static T Apply<T>(this T obj, Action<T> action)
        {
            action(obj);
            return obj;
        }
    }
}
