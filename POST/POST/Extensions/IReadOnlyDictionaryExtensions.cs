﻿using System;
using System.Collections.Generic;

namespace POST.Extensions
{

    public static class IReadOnlyDictionaryExtensions
    {

        public static IReadOnlyDictionary<TKey, TValue> CloneThenAdd<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary,
            TKey key, TValue value)
        {

            Dictionary<TKey, TValue> clone = new Dictionary<TKey, TValue>(dictionary)
            {
                { key, value }
            };
            return clone;
        }

        public static IReadOnlyDictionary<TKey, TValue> CloneThenSet<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary,
            TKey key, TValue value)
        {

            Dictionary<TKey, TValue> clone = new Dictionary<TKey, TValue>(dictionary)
            {
                [key] = value
            };
            return clone;
        }

        public static IReadOnlyDictionary<TKey, TValue> CloneThenRemove<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary,
            TKey key)
        {

            if (dictionary.ContainsKey(key))
            {
                Dictionary<TKey, TValue> clone = new Dictionary<TKey, TValue>(dictionary);
                clone.Remove(key);
                return clone;
            }
            else
            {
                return dictionary;
            }
        }

        public static IReadOnlyDictionary<TKey, TValue> CloneThenAddOrUpdate<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary,
            TKey key,
            TValue addIfNotExist,
            Action<TValue> updateIfExist)
        {

            Dictionary<TKey, TValue> clone = new Dictionary<TKey, TValue>(dictionary);
            if (clone.TryGetValue(key, out TValue existingValue))
            {
                updateIfExist(existingValue);
            }
            else
            {
                clone.Add(key, addIfNotExist);
            }
            return clone;
        }
    }
}
