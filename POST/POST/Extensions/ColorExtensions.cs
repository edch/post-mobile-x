﻿using SkiaSharp;
using Xamarin.Forms;

namespace POST.Extensions
{

    public static class ColorExtensions
    {

        public static SKColor ToSKColor(this Color color)
        {
            return new SKColor(
                red: (byte)(color.R * 255),
                green: (byte)(color.G * 255),
                blue: (byte)(color.B * 255),
                alpha: (byte)(color.A * 255)
            );
        }
    }
}
