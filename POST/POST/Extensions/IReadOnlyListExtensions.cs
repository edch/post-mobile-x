﻿using System.Collections.Generic;

namespace POST.Extensions
{

    public static class IReadOnlyListExtensions
    {

        public static IReadOnlyList<T> CloneThenAdd<T>(this IReadOnlyList<T> list,
            T item)
        {

            List<T> clone = new List<T>(list)
            {
                item
            };
            return clone;
        }

        public static IReadOnlyList<T> CloneThenSet<T>(this IReadOnlyList<T> list,
            int index,
            T item)
        {

            List<T> clone = new List<T>(list)
            {
                [index] = item
            };
            return clone;
        }

        public static IReadOnlyList<T> CloneThenRemove<T>(this IReadOnlyList<T> list,
            T item)
        {

            List<T> clone = new List<T>(list);
            clone.Remove(item);
            return clone;
        }
    }
}
