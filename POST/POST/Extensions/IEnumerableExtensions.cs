﻿using System;
using System.Collections.Generic;

namespace POST.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<TResult> TrySelect<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            foreach (TSource item in source)
            {
                TResult result;
                try
                {
                    result = selector(item);
                }
                catch
                {
                    continue;
                }
                yield return result;
            }
            yield break;
        }
    }
}
