﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace POST.JsonConverters
{
    public class StringDecimalConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }
            decimal d = (decimal)value;
            writer.WriteValue(d.ToString());
        }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                throw new JsonSerializationException("Cannot convert null value to decimal.");
            }
            try
            {
                if (reader.TokenType == JsonToken.String)
                {
                    string s = reader.Value.ToString();
                    CultureInfo usProvider = new CultureInfo("en-US");
                    return decimal.Parse(s, System.Globalization.NumberStyles.Float, usProvider);
                }
                else if (reader.TokenType == JsonToken.Integer)
                {
                    return (decimal)(long)reader.Value;
                }
                else if (reader.TokenType == JsonToken.Float)
                {
                    return (decimal)(double)reader.Value;
                }
                else
                {
                    throw new JsonSerializationException($"Unexpected token {reader.Value?.ToString() ?? "null"} when parsing decimal.");
                }
            }
            catch (Exception exc)
            {
                throw new JsonSerializationException($"Error converting value {reader.Value?.ToString() ?? "null"} to type 'decimal'.", exc);
            }
        }
    }
}
