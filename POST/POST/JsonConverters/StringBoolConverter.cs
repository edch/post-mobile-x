﻿using Newtonsoft.Json;
using System;

namespace POST.JsonConverters
{
    public class StringBoolConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }
            bool b = (bool)value;
            writer.WriteValue(b ? "1" : "0");
        }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                throw new JsonSerializationException("Cannot convert null value to bool.");
            }
            try
            {
                if (reader.TokenType == JsonToken.String)
                {
                    string s = reader.Value.ToString();
                    if (s == "1")
                    {
                        return true;
                    }
                    else if (s == "0")
                    {
                        return false;
                    }
                    else
                    {
                        throw new JsonSerializationException($"Unexpected token {s} when parsing bool.");
                    }
                }
                else if (reader.TokenType == JsonToken.Boolean)
                {
                    return (bool)reader.Value;
                }
                else
                {
                    throw new JsonSerializationException($"Unexpected token {reader.Value?.ToString() ?? "null"} when parsing bool.");
                }
            }
            catch (Exception exc)
            {
                throw new JsonSerializationException($"Error converting value {reader.Value?.ToString() ?? "null"} to type 'bool'.", exc);
            }
        }
    }
}
