﻿using Newtonsoft.Json;
using System;

namespace POST.JsonConverters
{
    public class StringIntConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }
            int i = (int)value;
            writer.WriteValue(i.ToString());
        }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                throw new JsonSerializationException("Cannot convert null value to int.");
            }
            try
            {
                if (reader.TokenType == JsonToken.String)
                {
                    string s = reader.Value.ToString();
                    return int.Parse(s);
                }
                else if (reader.TokenType == JsonToken.Integer)
                {
                    return (int)reader.Value;
                }
                else
                {
                    throw new JsonSerializationException($"Unexpected token {reader.Value?.ToString() ?? "null"} when parsing int.");
                }
            }
            catch (Exception exc)
            {
                throw new JsonSerializationException($"Error converting value {reader.Value?.ToString() ?? "null"} to type 'int'.", exc);
            }
        }
    }
}
