﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace POST.Converters
{
    public class OrderOfMagnitudeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is decimal d)
            {
                int decimalPlaces = 2;
                if (parameter is int p)
                {
                    decimalPlaces = Clamp(p, min: 1, max: 4);
                }
                string suffix = string.Empty;
                if (Math.Abs(d) >= 1_000_000_000_000m)
                {
                    d /= 1_000_000_000_000m;
                    suffix = "T"; // Trillion
                }
                else if (Math.Abs(d) >= 1_000_000_000m)
                {
                    d /= 1_000_000_000m;
                    suffix = "B"; // Billion
                }
                else if (Math.Abs(d) >= 1_000_000m)
                {
                    d /= 1_000_000m;
                    suffix = "M"; // Million
                }
                else if (Math.Abs(d) >= 1_000m)
                {
                    d /= 1_000m;
                    suffix = "K"; // Thousand
                    decimalPlaces = Clamp(decimalPlaces, min: 2, max: 3);
                }
                else
                {
                    decimalPlaces = 0;
                }

                switch (decimalPlaces)
                {
                    case 0: return $"{d:N0}{suffix}";
                    case 1: return $"{d:N1}{suffix}";
                    case 2: return $"{d:N2}{suffix}";
                    case 3: return $"{d:N3}{suffix}";
                    case 4: return $"{d:N4}{suffix}";
                    default: return $"{d:N}{suffix}";
                }
            }
            else
            {
                return string.Empty;
            }
        }

        public object Convert_NoZeroFraction(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is decimal d)
            {
                int decimalPlaces = 2;
                if (parameter is int p)
                {
                    decimalPlaces = Clamp(p, min: 1, max: 4);
                }
                string suffix = string.Empty;
                if (Math.Abs(d) >= 1_000_000_000_000m)
                {
                    d /= 1_000_000_000_000m;
                    suffix = "T"; // Trillion
                }
                else if (Math.Abs(d) >= 1_000_000_000m)
                {
                    d /= 1_000_000_000m;
                    suffix = "B"; // Billion
                }
                else if (Math.Abs(d) >= 1_000_000m)
                {
                    d /= 1_000_000m;
                    suffix = "M"; // Million
                }
                else if (Math.Abs(d) >= 1_000m)
                {
                    d /= 1_000m;
                    suffix = "K"; // Thousand
                    decimalPlaces = Clamp(decimalPlaces, min: 2, max: 3);
                }
                else
                {
                    decimalPlaces = 0;
                }

                if (d.ToString().Contains('.'))
                {
                    switch (decimalPlaces)
                    {
                        case 0: return $"{d:N0}{suffix}";
                        case 1: return $"{d:N1}{suffix}";
                        case 2: return $"{d:N2}{suffix}";
                        case 3: return $"{d:N3}{suffix}";
                        case 4: return $"{d:N4}{suffix}";
                        default: return $"{d:N}{suffix}";
                    }
                }
                else
                {
                    return $"{d:N0}{suffix}";
                }
            }
            else
            {
                return string.Empty;
            }
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException($"{nameof(OrderOfMagnitudeConverter)} is implemented one-way.");
        }

        private static int Clamp(int value, int min = 1, int max = 4)
        {
            if (value > max)
            {
                return max;
            }
            else if (value < min)
            {
                return min;
            }
            else
            {
                return value;
            }
        }
    }
}
