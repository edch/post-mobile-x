﻿namespace POST.Services
{
    public interface IDeviceProperties
    {
        string OS { get; }
        string OSVersion { get; }
        string DeviceType { get; }
        string Carrier { get; }
        string DeviceId { get; }
        string PackageName { get; }
        string VersionName { get; }
        string VersionCode { get; }



    }
}
