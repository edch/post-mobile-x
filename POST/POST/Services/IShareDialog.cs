﻿using System.Threading.Tasks;

namespace POST.Services
{
    public interface IShareDialog
    {
        Task ShowAsync(string title, string message, string url);
    }
}
