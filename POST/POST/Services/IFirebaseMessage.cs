﻿namespace POST.Services
{
    public interface IFirebaseMessage
    {
        string Token { get; }
    }
}
