﻿namespace POST.Services
{
    public interface IToastMessage
    {
        void Show(string message);
    }
}
