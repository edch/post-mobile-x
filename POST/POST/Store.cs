﻿using POST.Stores;

namespace POST
{

    /// <summary>
    /// Primary application pub-sub stores
    /// </summary>
    public static class Store
    {
        public static readonly DataFeedStatusStore DataFeedStatus = new DataFeedStatusStore();
        public static readonly WatchedStockIdsStore WatchedStockIds = new WatchedStockIdsStore();
        public static readonly StockByIdStore StockById = new StockByIdStore();
        public static readonly BrokerByIdStore BrokerById = new BrokerByIdStore();
        public static readonly WatchListSortKeyStore WatchListSortKey = new WatchListSortKeyStore();
        public static readonly WatchListViewModeStore WatchListViewMode = new WatchListViewModeStore();
        public static readonly RunningTradeFilterStockIdsStore DefaultRunningTradeFilterStockIdsStore = new RunningTradeFilterStockIdsStore(AppSettings.DefaultRunningTradeFilterStockIds);
        public static readonly RunningTradeFilterStockIdsStore CustomRunningTradeFilterStockIdsStore = new RunningTradeFilterStockIdsStore(AppSettings.CustomRunningTradeFilterStockIds);
        public static readonly RunningTradeFilterGroupSelectedStore RunningTradeFilterGroupSelectedStore = new RunningTradeFilterGroupSelectedStore();
        public static readonly RunningTradeStore RunningTrade = new RunningTradeStore();
        public static readonly MarketMoversViewModeStore MarketMoversViewMode = new MarketMoversViewModeStore();
        public static readonly RegionalIndexByCodeStore RegionalIndexByCode = new RegionalIndexByCodeStore();
        public static readonly RegionalIndicesSortKeyStore RegionalIndicesSortKey = new RegionalIndicesSortKeyStore();
        public static readonly CurrencyByCodeStore CurrencyByCode = new CurrencyByCodeStore();
        public static readonly StockChartStore StockChart = new StockChartStore();
        public static readonly IndexByCodeStore IndexByCode = new IndexByCodeStore();
        public static readonly IndicesSortKeyStore IndicesSortKey = new IndicesSortKeyStore();
        public static readonly InternalNewsFeedStore InternalNewsFeed = new InternalNewsFeedStore();
        public static readonly ExternalNewsFeedStore ExternalNewsFeed = new ExternalNewsFeedStore();
        public static readonly SavedNewsCollectionStore SavedNewsCollection = new SavedNewsCollectionStore();
        public static readonly ChartModeStore ChartMode = new ChartModeStore();
        public static readonly OrderBookStore OrderBook = new OrderBookStore();
        public static readonly OrderBookStore OrderBookTwoColumn = new OrderBookStore();
        public static readonly CompositeChartStore CompositeChart = new CompositeChartStore();
        public static readonly RecentStockIdsStore RecentStockIds = new RecentStockIdsStore();
        public static readonly TradeBookStore TradeBook = new TradeBookStore();
        public static readonly StockSummaryStore StockSummary = new StockSummaryStore();
        public static readonly OrderTrackingStore OrderTrackingStore = new OrderTrackingStore();

        public static readonly LoginFormStore LoginForm = new LoginFormStore();
        public static readonly AthenaSessionStore AthenaSession = new AthenaSessionStore();
        public static readonly TradingAccountsStore TradingAccounts = new TradingAccountsStore();
        public static readonly PinLoginFormStore PinLoginForm = new PinLoginFormStore();
        public static readonly CurrentDisplayStockStore CurrentDisplayStockStore = new CurrentDisplayStockStore();
    }
}
