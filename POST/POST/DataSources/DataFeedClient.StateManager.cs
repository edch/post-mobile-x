﻿using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Models;
using POST.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace POST.DataSources
{
    public partial class DataFeedClient
    {
        public class StateManager : DataFeedClientStateManager<State>
        {

            public static readonly StateManager Current = new StateManager();

            private StateManager() : base(
                initialState: new State(),
                clientFactory: CreateClient
            )
            { }

            private static DataFeedClient CreateClient(State state)
            {
                DataFeedClient client = new DataFeedClient();

                client.Connected += async sender =>
                {
                    await sender.SendAsync(new DesktopModeRequest());
                    await sender.SendAsync(new SubscribeAllIndexQuotesRequest());
                };
                client.DataReceived += Client_DataReceived;
                Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());

                return client;
            }

            private static void Client_DataReceived(DataFeedClient client, DataReceivedEventArgs arg)
            {
                switch (arg.Message)
                {
                    case StockInitMessage stockInitMessage:
                        if (!Store.StockById.GetState().ContainsKey(new StockId(stockInitMessage.StockCode, stockInitMessage.MarketType)))
                        {
                            Current.State.StockInitState = StockInitState.Receiving;
                            Current.State.StockInitRequestedOrReceived = DateTime.Now;
                            Current.State.StockInitMessages.Add(stockInitMessage);
                        }
                        break;
                    case StockInitEndMessage stockInitEndMessage:
                        if (Current.State.StockInitMessages.Any())
                        {
                            Current.State.StockInitState = StockInitState.Received;
                            Current.State.StockInitRequestedOrReceived = DateTime.Now;
                            Store.StockById.Init(Current.State.StockInitMessages);
                        }
                        Current.State.StockInitMessages.Clear();
                        break;
                    case StockQuoteMessage stockQuoteMessage:
                        if (Current.State.SubscribedToAllStocks)
                        {
                            Current.State.StockQuotesThrottledDispatcher.Dispatch(stockQuoteMessage);
                        }

                        break;
                    case IndexQuoteMessage indexQuoteMessage:
                        Store.IndexByCode.Dispatch(indexQuoteMessage);
                        break;
                    case StockChartMessage stockChartMessage when stockChartMessage.StockCode == "COMPOSITE":
                        Current.State.CompositeChartMessages.Add(stockChartMessage);
                        break;
                    case StockChartEndMessage stockChartEndMessage when stockChartEndMessage.StockCode == "COMPOSITE":
                        IndexChart indexChart = new IndexChart("COMPOSITE", IndexChartTimeUnit.Minute);
                        foreach (StockChartMessage stockChartMessage in from message in Current.State.CompositeChartMessages
                                                                        where message.Time.Date == Current.State.CompositeChartMessages.Last().Time.Date
                                                                        select message)
                        {
                            indexChart.Add(new IndexChartTuple(
                                time: stockChartMessage.Time,
                                open: stockChartMessage.Open,
                                high: stockChartMessage.High,
                                low: stockChartMessage.Low,
                                close: stockChartMessage.Close,
                                volume: stockChartMessage.Volume
                            ));
                        }
                        Store.CompositeChart.Set(indexChart);
                        Current.State.CompositeChartMessages.Clear();
                        break;
                    case BrokerInitMessage brokerInitMessage:
                        Store.BrokerById.Dispatch(brokerInitMessage);
                        break;
                    case BrokerInitEndMessage brokerInitEndMessage:
                        break;
                    case ServerRequestPingMessage serverRequestPingMessage:
                        break;
                    default:
                        break;
                }
            }

            public async Task<bool> RequestStockInitAsync()
            {
                switch (State.StockInitState)
                {
                    case StockInitState.NotRequested:
                        bool success = await SendAsync(new StockInitRequest());
                        if (success)
                        {
                            State.StockInitState = StockInitState.Requested;
                            State.StockInitRequestedOrReceived = DateTime.Now;
                        }
                        return success;
                    case StockInitState.Requested:
                        if (State.StockInitRequestedOrReceived == null ||
                            DateTime.Now.Subtract(State.StockInitRequestedOrReceived.Value).Seconds > 5)
                        {
                            return await SendAsync(new StockInitRequest());
                        }
                        else
                        {
                            return false;
                        }
                    case StockInitState.Receiving:
                        if (State.StockInitRequestedOrReceived == null ||
                            DateTime.Now.Subtract(State.StockInitRequestedOrReceived.Value).Seconds > 10)
                        {
                            return await SendAsync(new StockInitRequest());
                        }
                        else
                        {
                            return false;
                        }
                    case StockInitState.Received:
                        if (State.StockInitRequestedOrReceived == null ||
                            DateTime.Now.Subtract(State.StockInitRequestedOrReceived.Value).Seconds > 60)
                        {
                            return await SendAsync(new StockInitRequest());
                        }
                        else
                        {
                            return false;
                        }
                    default:
                        return false;
                }
            }

            public async Task<bool> SetSubscribedStocksAsync(ISet<string> stockCodes)
            {
                if (State.SubscribedToAllStocks)
                {
                    State.SubscribedStocks.Clear();
                    foreach (string stockCode in stockCodes)
                    {
                        State.SubscribedStocks.Add(stockCode);
                    }
                    return true;
                }
                else
                {
                    bool failed = false;

                    foreach (string stockCode in stockCodes)
                    {
                        State.SubscribedStocks.Add(stockCode);

                        if (!await SendAsync(new SubscribeStockQuoteRequest(new StockId(stockCode, MarketType.RG))))
                        {
                            failed = true;
                        }
                    }

                    foreach (string stockCode in State.SubscribedStocks.Except(stockCodes).ToList())
                    {
                        State.SubscribedStocks.Remove(stockCode);

                        if (!await SendAsync(new UnsubscribeStockQuoteRequest(new StockId(stockCode, MarketType.RG))))
                        {
                            failed = true;
                        }
                    }

                    return !failed;
                }

            }

            public async Task<bool> SubscribeToAllStocksAsync(bool subscribeToAllStocks)
            {
                if (subscribeToAllStocks)
                {
                    State.SubscribedToAllStocks = true;
                    bool success = await SendAsync(new SubscribeAllStockQuotesRequest());
                    return success;
                }
                else
                {
                    State.SubscribedToAllStocks = false;
                    bool success = await SendAsync(new UnsubscribeAllStockQuotesRequest());
                    return success;
                }
            }

            public async void BrokerInitRequestAsync()
            {
                if (!Store.BrokerById.GetState().Any())
                {
                    await SendAsync(new BrokerInitRequest());
                }
            }

            public async Task<bool> SubscribeCompositeChartAsync()
            {
                return await SendAsync(new IndexChartInitRequest("COMPOSITE", IndexChartTimeUnit.Minute)) &&
                    await SendAsync(new SubscribeIndexChartQuoteRequest("COMPOSITE"));
            }

            public async Task<bool> UnsubscribeCompositeChartAsync()
            {
                return await SendAsync(new UnsubscribeIndexChartQuoteRequest("COMPOSITE"));
            }
        }

        public class State
        {

            #region Stock Init
            public StockInitState StockInitState { get; set; }

            /// <summary>
            /// Requested timeout: 5 seconds
            /// Receiving timeout: 10 seconds
            /// Received cooldown: 60 seconds
            /// </summary>
            public DateTime? StockInitRequestedOrReceived { get; set; }

            public List<StockInitMessage> StockInitMessages { get; } = new List<StockInitMessage>();
            #endregion

            #region Stock Quotes
            public HashSet<string> SubscribedStocks { get; } = new HashSet<string>();
            public bool SubscribedToAllStocks { get; set; } = false;
            public ThrottledDispatcher<IReadOnlyDictionary<StockId, Stock>, StockQuoteMessage> StockQuotesThrottledDispatcher { get; } = new ThrottledDispatcher<IReadOnlyDictionary<StockId, Stock>, StockQuoteMessage>(
                store: Store.StockById,
                actionDispatcher: messages => Store.StockById.UpdateQuotes(messages.ToList()),
                coalesceInterval: TimeSpan.FromMilliseconds(250),
                flushInterval: TimeSpan.FromMilliseconds(500)
            );
            #endregion

            #region Broker Init
            public List<BrokerInitMessage> BrokerInitMessages { get; } = new List<BrokerInitMessage>();
            #endregion

            #region Composite Chart
            public CompositeChartInitState CompositeChartInitState { get; set; }
            public List<StockChartMessage> CompositeChartMessages { get; } = new List<StockChartMessage>();
            #endregion
        }

        public enum StockInitState
        {
            NotRequested,
            Requested,
            Receiving,
            Received
        }

        public enum CompositeChartInitState
        {
            NotRequested,
            Requested,
            Received
        }
    }
}
