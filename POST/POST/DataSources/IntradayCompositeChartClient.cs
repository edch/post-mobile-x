﻿using Newtonsoft.Json;
using POST.DataSources.ApiResponses;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{
    public class IntradayCompositeChartClient
    {

        private const string URL = "https://chart.post-pro.co.id/api/v1/chart/mobile/1min/COMPOSITE";

        private static readonly Lazy<IntradayCompositeChartClient> _singletonFactory = new Lazy<IntradayCompositeChartClient>(() => new IntradayCompositeChartClient());
        public static IntradayCompositeChartClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        public IntradayCompositeChartClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<IntradayIndexChartData[]> GetIntradayIndexChartAsync(CancellationToken cancellationToken)
        {
            using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, URL))
            using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(httpRequestMessage, cancellationToken))
            {
                string json = await httpResponseMessage.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IntradayIndexChartData[]>(json);
            }
        }
    }
}
