﻿using Newtonsoft.Json;
using POST.DataSources.AthenaMessages.Requests;
using POST.DataSources.AthenaMessages.Responses;
using POST.DataSources.RightsMessages.Requests;
using POST.DataSources.RightsMessages.Responses;
using POST.DataSources.WdMessages.Requests;
using POST.DataSources.WdMessages.Responses;
using POST.Stores;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{
    public class AthenaClient : IDisposable
    {
        //public const string BASE_URL = "http://139.255.250.211:80/mi2";
        //public const string BASE_URL = "http://172.31.166.131:80/mi2";
        //public const string BASE_URL = "http://202.158.55.172/mi2";
        //public const string BASE_URL = "http://server1.post-pro.co.id/mi2";
        //public const string BASE_URL = "http://athena2.post-pro.co.id/mi2";
        public string BASE_URL { get; } = "http://trx.lc.pansolt.co.id/mi2";
        public string BASE_URL_MI { get; }
        public string BASE_URL_ORDERSTATUS { get; }
        public string BASE_URL_OLTORDER { get; }

        private readonly CookieContainer _cookieContainer;
        private readonly HttpClientHandler _httpClientHandler;
        private readonly HttpClient _httpClient;

        private readonly CultureInfo usProvider = new CultureInfo("en-US");

        public string UserId { get; }

        public static AthenaClient Current => Store.AthenaSession.GetState()?.Client;

        public AthenaClient(string userId)
        {
            if (AppSettings.TradingServer_Selected == (int)Models.TradingServerType.Main)
            {
                BASE_URL = "http://trx.lc.pansolt.co.id/mi2";
            }
            else
            if (AppSettings.TradingServer_Selected == (int)Models.TradingServerType.Backup)
            {
                BASE_URL = "http://athena2.post-pro.co.id/mi2";
            }

            //BASE_URL = "http://172.31.166.172:8080/mi2";

            BASE_URL_MI = BASE_URL + "/marketInfoData";
            BASE_URL_ORDERSTATUS = BASE_URL + "/orderStatus";
            BASE_URL_OLTORDER = BASE_URL + "/oltOrder";

            _cookieContainer = new CookieContainer();
            _httpClientHandler = new HttpClientHandler
            {
                CookieContainer = _cookieContainer
            };
            _httpClient = new HttpClient(_httpClientHandler);
            UserId = userId ?? throw new ArgumentNullException(nameof(userId));
        }

        public async Task<LoginResponse> LoginAsync(LoginRequest loginRequest, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_MI}",
                request: loginRequest,
                cancellationToken: cancellationToken
            );
            return new LoginResponse(strResult.Trim());
        }

        public async Task<string> LogoutAsync(CancellationToken cancellationToken)
        {
            string strResult = "";

            try
            {
                strResult = await PostAsync(
                    httpClient: _httpClient,
                    url: $"{BASE_URL_MI}",
                    request: new LogoutRequest(),
                    cancellationToken: cancellationToken
                );
            }
            catch { }

            return strResult;
        }

        public async Task<TradingAccount[]> GetTradingAccountsAsync(CancellationToken cancellationToken)
        {
            string strResult = "";

            // \.[{(*+?^$|
            //"[\"5381\",\"R\",\"Ir. BUDI HARTO, MM (REG)\",\"budihartohs@gmail.com\"]\n[\"9678\",\"M\",\"Ir. BUDI HARTO, MM (M)\",\"budihartohs@gmail.com\"]\n"

            strResult += await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_ORDERSTATUS}",
                request: new TradingAccountsRequest(),
                cancellationToken: cancellationToken
            );

            List<TradingAccount> lstTradeAcc = new List<TradingAccount>();

            string regex = @"\[""(?<id>.*?)"",""(?<type>.*?)"",""(?<name>.*?)"",""(?<email>.*?)""";

            MatchCollection matchCollection = Regex.Matches(strResult, regex);

            int i = 0;
            foreach (Match match in matchCollection)
            {
                lstTradeAcc.Add(new TradingAccount(i++, match.Groups["id"].Value, match.Groups["type"].Value, match.Groups["name"].Value, match.Groups["email"].Value));
            }

            return lstTradeAcc.ToArray();
        }

        public async Task<PinLoginResponse> PinLoginAsync(string brokerAccountId, string pin, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                           httpClient: _httpClient,
                           url: $"{BASE_URL_ORDERSTATUS}",
                           request: new PinLoginRequest(brokerAccountId, pin),
                           cancellationToken: cancellationToken
                       );

            return new PinLoginResponse(strResult.Trim());
        }

        public async Task<PinValidateResponse> PinValidateAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                           httpClient: _httpClient,
                           url: $"{BASE_URL_ORDERSTATUS}",
                           request: new PinValidateRequest(brokerAccountId),
                           cancellationToken: cancellationToken
                       );

            return new PinValidateResponse(strResult.Trim());
        }

        public async Task<string> IsLoginAsync(CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                           httpClient: _httpClient,
                           url: $"{BASE_URL_MI}",
                           request: new IsLoginRequest(),
                           cancellationToken: cancellationToken
                       );

            return strResult.Trim();
        }

        public async Task<string> IsConsolidateAsync(CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                           httpClient: _httpClient,
                           url: $"{BASE_URL_ORDERSTATUS}",
                           request: new IsConsolidateRequest(),
                           cancellationToken: cancellationToken
                       );

            return strResult.Trim();
        }

        /*
        public async Task<PortfolioStock[]> GetStockBalanceAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new PortfolioRequest(brokerAccountId),
                          cancellationToken: cancellationToken
                      );

            //"{\"data\":[\"BEKS\",50,50,100,0,1.0E-4,100],\"id\":0}\n{\"data\":[\"LSIP\",1445,1445,9300,0,0.8,9300],\"id\":1}\n{\"data\":[\"TAXI\",189,90,40800,-4039200,1.0E-4,40800],\"id\":2}\n{\"data\":[\"ISAT\",2710,3490,1400,1092000,0.75,1400],\"id\":3}\n{\"data\":[\"DKFT\",242,252,1200,12000,0.05,1200],\"id\":4}\n"

            string regex = @"\{""data"":\[""(?<stockId>.*?)"",(?<avgPrice>.*?),(?<lastPrice>.*?),(?<vol>.*?),(?<gainLoss>.*?),(?<hairCut>.*?),(?<availableVol>.*?)],""id"":(?<id>.*?)}";

            MatchCollection matchCollection = Regex.Matches(strResult, regex);
            List<PortfolioStock> lstPortfolioStocks = new List<PortfolioStock>();

            foreach (Match match in matchCollection)
            {
                lstPortfolioStocks.Add(new PortfolioStock(int.Parse(match.Groups["id"].Value), match.Groups["stockId"].Value, decimal.Parse(match.Groups["lastPrice"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["vol"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["avgPrice"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["availableVol"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["gainLoss"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["hairCut"].Value, System.Globalization.NumberStyles.Any, usProvider)));
            }

            return lstPortfolioStocks.ToArray();
        }
        
        public async Task<CashBalance[]> GetCashBalanceAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new PortfolioBalanceRequest(brokerAccountId),
                          cancellationToken: cancellationToken
                      );

            //"[\"Ir. BUDI HARTO. MM (REG)\",7669812,8442.7802734375,2663925,2663925,0,171485544,0,31552,\"5381\"]\n"
            string regex = @"\[""(?<name>.*?)"",(?<tradingLimit>.*?),(?<currentRatio>.*?),(?<currentCash>.*?),(?<t3>.*?),(?<todayTransaction>.*?),(?<sumStockValue>.*?),(?<points>.*?),(?<haircutValue>.*?),""(?<brokerAccId>.*?)""";

            MatchCollection matchCollection = Regex.Matches(strResult, regex);
            List<CashBalance> lstCashBalance = new List<CashBalance>();

            foreach (Match match in matchCollection)
            {
                lstCashBalance.Add(new CashBalance(int.Parse(match.Groups["brokerAccId"].Value), decimal.Parse(match.Groups["tradingLimit"].Value, System.Globalization.NumberStyles.Any, usProvider), match.Groups["name"].Value, decimal.Parse(match.Groups["currentCash"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["currentRatio"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["sumStockValue"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["t3"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["todayTransaction"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["points"].Value, System.Globalization.NumberStyles.Any, usProvider)));
            }

            return lstCashBalance.ToArray();
        }

        public async Task<List<CashFlow>> GetPayableReceivableAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new PayableReceivableRequest(brokerAccountId),
                          cancellationToken: cancellationToken
                      );

            //"[-7,\"03 Dec 2018\",0]\n[-6,\"04 Dec 2018\",0]\n[-5,\"05 Dec 2018\",0]\n[-4,\"06 Dec 2018\",0]\n[-3,\"07 Dec 2018\",0]\n[-2,\"10 Dec 2018\",0]\n[-1,\"11 Dec 2018\",0]\n[0,\"12 Dec 2018\",0]\n[1,\"13 Dec 2018\",0]\n[2,\"14 Dec 2018\",0]\n[3,\"17 Dec 2018\",0]\n[4,\"18 Dec 2018\",0]\n"
            string regex = @"\[(?<due>.*?),""(?<date>.*?)"",(?<amount>.*?)]";

            MatchCollection matchCollection = Regex.Matches(strResult, regex);
            List<CashFlow> lstCashBalance = new List<CashFlow>();

            foreach (Match match in matchCollection)
            {
                lstCashBalance.Add(new CashFlow(match.Groups["date"].Value, decimal.Parse(match.Groups["amount"].Value, System.Globalization.NumberStyles.Any, usProvider), int.Parse(match.Groups["due"].Value)));
            }

            return lstCashBalance;
        }
        */

        public async Task<string> GetCashFlowAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new CashFlowRequest(brokerAccountId),
                          cancellationToken: cancellationToken
                      );

            return strResult;
        }

        public async Task<PortfolioAI1Response> GetPortfolioAI1Async(string brokerAccountId, string user, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new PortfolioAI1Request(brokerAccountId, user),
                          cancellationToken: cancellationToken
                      );

            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings() { Culture = usProvider };
            PortfolioAI1Response response = JsonConvert.DeserializeObject<PortfolioAI1Response>(strResult, jsonSerializerSettings);

            return response;
        }

        public async Task<Order[]> GetOrdersAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new OrderListRequest(brokerAccountId),
                          cancellationToken: cancellationToken
                      );

            //{"data":["16:47:17","","BUMI",0,50,1,1,0,"RG",1,"DEPT1_IT","0",null],"id":"181203000007"}            
            string regex = @"\{""data"":\[""(?<OrderTime>.*?)"",""(?<JSXId>.*?)"",""(?<StockCode>.*?)"",(?<BuySell>.*?),(?<Price>.*?),(?<OrderVolume>.*?),(?<RemainingVolume>.*?),(?<TradedVolume>.*?),""(?<MarketCode>.*?)"",(?<OrderStatus>.*?),""(?<InputUser>.*?)"",""(?<ExpireFlag>.*?)"",(?<RejectNote>.*?)],""id"":""(?<OrderId>.*?)""}";

            MatchCollection matchCollection = Regex.Matches(strResult, regex);
            List<Order> lstOrder = new List<Order>();

            foreach (Match match in matchCollection)
            {
                try
                {
                    lstOrder.Add(new Order(match.Groups["OrderId"].Value, match.Groups["JSXId"].Value, match.Groups["OrderTime"].Value, match.Groups["InputUser"].Value, match.Groups["StockCode"].Value, match.Groups["MarketCode"].Value, decimal.Parse(match.Groups["Price"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["OrderVolume"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["RemainingVolume"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["TradedVolume"].Value, System.Globalization.NumberStyles.Any, usProvider), match.Groups["OrderStatus"].Value, match.Groups["BuySell"].Value, int.Parse(match.Groups["ExpireFlag"].Value), match.Groups["RejectNote"].Value));
                }
                catch
                {

                }
            }

            return lstOrder.ToArray();
        }

        public async Task<ConditionalOrder[]> GetConditionalOrdersAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new ConditionalOrderListRequest(brokerAccountId),
                          cancellationToken: cancellationToken
                      );

            //"[345837,\"2019-04-25 14:25:45.797\",null,null,\"RAJA\",\"RG\",5,272,\"0\",\"10\",0,0,\"Not Executed\",\"0\",\"1\",\"26 Apr 2019 08:00:00\"]\n"            
            string regex = @"\[(?<id>.*?),(?<createdTime>.*?),(?<executedTime>.*?),(?<updateTime>.*?),(?<stockCode>.*?),(?<boardCode>.*?),(?<oVolume>.*?),(?<oPrice>.*?),(?<bs>.*?),(?<condition>.*?),(?<cPrice>.*?),(?<cVolume>.*?),(?<status>.*?),(?<refId>.*?),(?<active>.*?),(?<timeCondition>.*?)]";

            MatchCollection matchCollection = Regex.Matches(strResult.Replace("\"", ""), regex);
            List<ConditionalOrder> lstOrder = new List<ConditionalOrder>();

            foreach (Match match in matchCollection)
            {
                lstOrder.Add(new ConditionalOrder(match.Groups["id"].Value, match.Groups["createdTime"].Value, match.Groups["executedTime"].Value, match.Groups["updateTime"].Value, match.Groups["stockCode"].Value, match.Groups["boardCode"].Value, decimal.Parse(match.Groups["oVolume"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["oPrice"].Value, System.Globalization.NumberStyles.Any, usProvider), match.Groups["bs"].Value, match.Groups["condition"].Value, decimal.Parse(match.Groups["cPrice"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["cVolume"].Value, System.Globalization.NumberStyles.Any, usProvider), match.Groups["status"].Value, match.Groups["refId"].Value, match.Groups["active"].Value, match.Groups["timeCondition"].Value));
            }

            return lstOrder.ToArray();
        }

        public async Task<Trade[]> GetHistoryTradeAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new HistoryTradeRequest(brokerAccountId),
                          cancellationToken: cancellationToken
                      );

            string regex = @"\[""(?<tradeTime>.*?)"",""(?<tradeId>.*?)"",""(?<stockCode>.*?)"",""(?<buySell>.*?)"",(?<price>.*?),(?<volume>.*?),(?<tradeFee>.*?)]";

            MatchCollection matchCollection = Regex.Matches(strResult, regex);
            List<Trade> lstTrade = new List<Trade>();

            foreach (Match match in matchCollection)
            {
                lstTrade.Add(new Trade(match.Groups["tradeId"].Value, match.Groups["stockCode"].Value, match.Groups["tradeTime"].Value, decimal.Parse(match.Groups["price"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["volume"].Value, System.Globalization.NumberStyles.Any, usProvider), match.Groups["buySell"].Value, decimal.Parse(match.Groups["tradeFee"].Value, System.Globalization.NumberStyles.Any, usProvider)));
            }

            return lstTrade.ToArray();
        }

        public async Task<TradeDone[]> GetTradeDoneAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new TradeDoneRequest(brokerAccountId),
                          cancellationToken: cancellationToken
                      );
            //"{\"data\":[\"000568198567\",\"09:52:11\",\"DKFT\",250,1,0,50],\"id\":\"000568198567\"}\n"
            string regex = @"\{""data"":\[""(?<tradeId>.*?)"",""(?<tradeTime>.*?)"",""(?<stockCode>.*?)"",(?<price>.*?),(?<volume>.*?),(?<buySell>.*?),(?<tradeFee>.*?)],""id"":""(?<tradeId>.*?)""}";

            MatchCollection matchCollection = Regex.Matches(strResult, regex);
            List<TradeDone> lstTradeDone = new List<TradeDone>();

            foreach (Match match in matchCollection)
            {
                lstTradeDone.Add(new TradeDone(match.Groups["tradeId"].Value, match.Groups["stockCode"].Value, match.Groups["tradeTime"].Value, decimal.Parse(match.Groups["price"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["volume"].Value, System.Globalization.NumberStyles.Any, usProvider), match.Groups["buySell"].Value, decimal.Parse(match.Groups["tradeFee"].Value, System.Globalization.NumberStyles.Any, usProvider)));
            }

            return lstTradeDone.ToArray();
        }

        public async Task<BankAccount> GetBankAccountAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new BankAccountRequest(brokerAccountId),
                          cancellationToken: cancellationToken
                      );
            // bankAccount accountHolder bankName                                             investorAccount/rdn name 
            //"[\"1442088922\",\"STEPHEN GUNAWAN\",\"PANIN\",\"Sawah Besar\",\"1\",\"T\"]\n[\"1202687352\",\"STEPHEN GUNAWAN\",\"PANIN\",\"BEI\",\"0\",\"I\"]\n"
            string regex = @"\[""(?<bankAccount>.*?)"",""(?<accountHolder>.*?)"",""(?<bankName>.*?)"",""(?<kcp>.*?)"",""(?<defaultAccount>.*?)"",""(?<accountType>.*?)""]";

            MatchCollection matchCollection = Regex.Matches(strResult, regex);

            BankAccount bankAccount = new BankAccount();

            foreach (Match match in matchCollection)
            {
                if (match.Groups["accountType"].Value == "T")
                {
                    bankAccount.BankAccountNumber = match.Groups["bankAccount"].Value;
                    bankAccount.BankAccountHolderName = match.Groups["accountHolder"].Value;
                    bankAccount.BankName = match.Groups["bankName"].Value;
                }
                else // "I"
                {
                    bankAccount.RdnAccountNumber = match.Groups["bankAccount"].Value;
                    bankAccount.RdnAccountHolderName = match.Groups["accountHolder"].Value;
                    bankAccount.RdnName = match.Groups["bankName"].Value;
                }
            }

            return bankAccount;
        }

        public async Task<TradingLimitResponse> GetTradingLimitAsync(string brokerAccountId, CancellationToken cancellationToken, string stockId = null)
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new TradingLimitRequest(brokerAccountId, stockId),
                          cancellationToken: cancellationToken
                      );

            //{"data":["16:47:17","","BUMI",0,50,1,1,0,"RG",1,"DEPT1_IT","0",null],"id":"181203000007"}            
            string regex = @"\[(?<CashT3>.*?),(?<CashT1>.*?),(?<FeeBuy>.*?),(?<FeeSell>.*?)]";

            MatchCollection matchCollection = Regex.Matches(strResult, regex);
            List<TradingLimitResponse> lstTradingLimit = new List<TradingLimitResponse>();

            foreach (Match match in matchCollection)
            {
                lstTradingLimit.Add(new TradingLimitResponse(decimal.Parse(match.Groups["CashT3"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["CashT1"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["FeeBuy"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["FeeSell"].Value, System.Globalization.NumberStyles.Any, usProvider)));
            }

            return lstTradingLimit[0];
        }

        public async Task<AthenaResponse> PlaceCashWithdrawAsync(
            string brokerAccountId,
            string pin,
            string date,
            decimal amount,
            string amountWords,
            string bankAccount,
            string rdn,
            string bankAccountName,
            string refNo,
            CancellationToken cancellationToken
        )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_ORDERSTATUS}",
                request: new CashWithdrawRequest(
                    brokerAccountId: brokerAccountId,
                    pin: pin,
                    date: date,
                    amount: amount,
                    bankAccount: bankAccount,
                    rdn: rdn,
                    amountWords: amountWords,
                    bankAccountName: bankAccountName,
                    refNo: refNo
                ),
                cancellationToken: cancellationToken
            );

            return new AthenaResponse(strResult);
        }

        public async Task<AthenaResponse> ChangePasswordAsync(
            string oldPwd,
            string newPwd,
            CancellationToken cancellationToken
        )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_MI}",
                request: new PasswordChangeRequest(
                    oldPwd: oldPwd,
                    newPwd: newPwd
                ),
                cancellationToken: cancellationToken
            );

            return new AthenaResponse(strResult.TrimEnd());
        }

        public async Task<AthenaResponse> ChangePinAsync(
            string brokerAccount,
            string oldPin,
            string newPin,
            CancellationToken cancellationToken
      )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_MI}",
                request: new PinChangeRequest(
                    brokerAccount: brokerAccount,
                    oldPin: oldPin,
                    newPin: newPin
                ),
                cancellationToken: cancellationToken
            );

            return new AthenaResponse(strResult.TrimEnd());
        }

        public async Task<AthenaResponse> PlaceBuyOrderAsync(
            string brokerAccountId,
            string stockCode,
            string boardCode,
            decimal price,
            decimal volume,
            int expiry,
            CancellationToken cancellationToken
        )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_OLTORDER}",
                request: new PlaceBuyOrderRequest(
                    brokerAccountId: brokerAccountId,
                    stockCode: stockCode,
                    boardCode: boardCode,
                    price: price,
                    volume: volume,
                    expiry: expiry
                ),
                cancellationToken: cancellationToken
            );

            return new AthenaResponse(strResult.TrimEnd());
        }

        public async Task<AthenaResponse> PlaceSellOrderAsync(
            string brokerAccountId,
            string stockCode,
            string boardCode,
            decimal price,
            decimal volume,
            int expiry,
            CancellationToken cancellationToken
        )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_OLTORDER}",
                request: new PlaceSellOrderRequest(
                    brokerAccountId: brokerAccountId,
                    stockCode: stockCode,
                    boardCode: boardCode,
                    price: price,
                    volume: volume,
                    expiry: expiry
                ),
                cancellationToken: cancellationToken
            );

            return new AthenaResponse(strResult.TrimEnd());
        }

        public async Task<AthenaResponse> PlaceAmendOrderAsync(
            string brokerAccountId,
            string stockCode,
            string boardCode,
            decimal price,
            decimal volume,
            int expiry,
            string orderId,
            string jsxId,
            string command,
            CancellationToken cancellationToken
        )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_OLTORDER}",
                request: new PlaceAmendOrderRequest(
                    brokerAccountId: brokerAccountId,
                    stockCode: stockCode,
                    boardCode: boardCode,
                    price: price,
                    volume: volume,
                    expiry: expiry,
                    orderId: orderId,
                    jsxId: jsxId,
                    command: command

                ),
                cancellationToken: cancellationToken
            );

            return new AthenaResponse(strResult.TrimEnd());
        }

        public async Task<AthenaResponse> PlaceWithdrawOrderAsync(
           string brokerAccountId,
           string stockCode,
           string boardCode,
           decimal price,
           string orderId,
           string jsxId,
           string command,
           CancellationToken cancellationToken
       )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_OLTORDER}",
                request: new PlaceWithdrawOrderRequest(
                    brokerAccountId: brokerAccountId,
                    stockCode: stockCode,
                    boardCode: boardCode,
                    price: price,
                    orderId: orderId,
                    jsxId: jsxId,
                    command: command
                ),
                cancellationToken: cancellationToken
            );

            return new AthenaResponse(strResult.TrimEnd());
        }

        public async Task<AthenaResponse> PlaceBuyConditionalOrderAsync(
            string brokerAccountId,
            string stockCode,
            string boardCode,
            string price,
            string volume,
            string condition,
            string priceCondition,
            string volCondition,
            string timeCondition,
            string expDate,
            CancellationToken cancellationToken
        )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_OLTORDER}",
                request: new PlaceBuyConditionalOrderRequest(
                    brokerAccountId: brokerAccountId,
                    stockCode: stockCode,
                    boardCode: boardCode,
                    price: price,
                    volume: volume,
                    condition: condition,
                    priceCondition: priceCondition,
                    volCondition: volCondition,
                    timeCondition: timeCondition,
                    expDate: expDate
                ),
                cancellationToken: cancellationToken
            );

            return new AthenaResponse(strResult.TrimEnd());
        }

        public async Task<AthenaResponse> PlaceSellConditionalOrderAsync(
            string brokerAccountId,
            string stockCode,
            string boardCode,
            string price,
            string volume,
            string condition,
            string priceCondition,
            string volCondition,
            string timeCondition,
            string expDate,
            CancellationToken cancellationToken
        )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_OLTORDER}",
                request: new PlaceSellConditionalOrderRequest(
                    brokerAccountId: brokerAccountId,
                    stockCode: stockCode,
                    boardCode: boardCode,
                    price: price,
                    volume: volume,
                    condition: condition,
                    priceCondition: priceCondition,
                    volCondition: volCondition,
                    timeCondition: timeCondition,
                    expDate: expDate
                ),
                cancellationToken: cancellationToken
            );

            return new AthenaResponse(strResult.TrimEnd());
        }

        public async Task<AthenaResponse> PlaceActivateConditionalOrderAsync(
            string brokerAccountId,
            string orderId,
            CancellationToken cancellationToken
        )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_OLTORDER}",
                request: new PlaceActivateConditionalOrderRequest(
                    brokerAccountId: brokerAccountId,
                    orderId: orderId
                ),
                cancellationToken: cancellationToken
            );

            return new AthenaResponse(strResult.TrimEnd());
        }

        public async Task<AthenaResponse> PlaceDeactivateConditionalOrderAsync(
           string brokerAccountId,
           string orderId,
           CancellationToken cancellationToken
        )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_OLTORDER}",
                request: new PlaceDeactivateConditionalOrderRequest(
                    brokerAccountId: brokerAccountId,
                    orderId: orderId
                ),
                cancellationToken: cancellationToken
            );

            return new AthenaResponse(strResult.TrimEnd());
        }

        public async Task<AthenaResponse> PlaceDeleteConditionalOrderAsync(
          string brokerAccountId,
          string orderId,
          CancellationToken cancellationToken
        )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_OLTORDER}",
                request: new PlaceDeleteConditionalOrderRequest(
                    brokerAccountId: brokerAccountId,
                    orderId: orderId
                ),
                cancellationToken: cancellationToken
            );

            return new AthenaResponse(strResult.TrimEnd());
        }

        public async Task<RightsAvailableResponse> GetRightsAvailableAsync(
         string brokerAccountId,
         CancellationToken cancellationToken
       )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_ORDERSTATUS}",
                request: new RightsAvailableRequest(
                    brokerAccountId: brokerAccountId
                ),
                cancellationToken: cancellationToken
            );

            RightsAvailableResponse rightsAvailableResponse = JsonConvert.DeserializeObject<RightsAvailableResponse>(strResult);

            return rightsAvailableResponse;
        }

        public async Task<RightsUserReqListResponse> GetRightsUserReqAsync(
         string brokerAccountId,
         string custid,
         CancellationToken cancellationToken
       )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_ORDERSTATUS}",
                request: new RightsUserReqListRequest(
                    brokerAccountId: brokerAccountId,
                    custid: custid
                ),
                cancellationToken: cancellationToken
            );

            RightsUserReqListResponse rightsUserReqListResponse = JsonConvert.DeserializeObject<RightsUserReqListResponse>(strResult);

            return rightsUserReqListResponse;
        }

        public async Task<PlaceRightsResponse> PlaceRightsAsync(
             string brokerAccountId,
              string userid,
              string custid,
              string rightid,
              decimal share,
            CancellationToken cancellationToken
            )
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new PlaceRightsRequest(
                              brokerAccountId,
                              userid,
                              custid,
                              rightid,
                              share
                              ),
                          cancellationToken: cancellationToken
                      );

            PlaceRightsResponse placeRightsResponse = JsonConvert.DeserializeObject<PlaceRightsResponse>(strResult);

            return placeRightsResponse;
        }

        public async Task<RightsCancelResponse> CancelRightsAsync(
          string brokerAccountId,
           string id,
         CancellationToken cancellationToken
         )
        {
            string strResult = await PostAsync(
                          httpClient: _httpClient,
                          url: $"{BASE_URL_ORDERSTATUS}",
                          request: new RightsCancelRequest(
                              brokerAccountId,
                              id
                              ),
                          cancellationToken: cancellationToken
                      );

            RightsCancelResponse rightsCancelResponse = JsonConvert.DeserializeObject<RightsCancelResponse>(strResult);

            return rightsCancelResponse;
        }

        public async Task<WdListResponse> GetWdListAsync(
          string brokerAccountId,
          CancellationToken cancellationToken
        )
        {
            string strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_ORDERSTATUS}",
                request: new WdListRequest(
                    brokerAccountId: brokerAccountId
                ),
                cancellationToken: cancellationToken
            );

            //{"list":[{"tfdate":"2021-06-03","amount":100000,"status":1,"inputUser":"stpg032468","bankAccount":"1442088922","bankAccName":"STEPHEN GUNAWAN","bankName":"PANIN","sdesc":"Open","custId":"32468","transferId":30}],"ecode":200}

            WdListResponse wdListResponse = JsonConvert.DeserializeObject<WdListResponse>(strResult);

            return wdListResponse;
        }

        public async Task<WdFundInfoResponse> GetWdFundInfoAsync(
          string brokerAccountId,
          CancellationToken cancellationToken
        )
        {
            string strResult;

            strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_ORDERSTATUS}",
                request: new WdFundInfoRequest(
                    brokerAccountId: brokerAccountId
                ),
                cancellationToken: cancellationToken
            );

            //strResult = @"{""custid"":""32468"",""code"":0,""maxday"":0,""dsrc"":""Cached"",""recs"":[{""ti"":0,""ddate"":20210524,""value"":0},{""ti"":1,""ddate"":20210525,""value"":0},{""ti"":2,""ddate"":20210527,""value"":0},{""ti"":3,""ddate"":20210528,""value"":0}],""ecode"":200}";

            WdFundInfoResponse wdFundInfoResponse = JsonConvert.DeserializeObject<WdFundInfoResponse>(strResult);
            return wdFundInfoResponse;
        }

        public async Task<BankAccount> GetWdCustInfoAsync(
          string brokerAccountId,
          CancellationToken cancellationToken
        )
        {
            string strResult;

            strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_ORDERSTATUS}",
                request: new WdCustInfoRequest(
                    brokerAccountId: brokerAccountId
                ),
                cancellationToken: cancellationToken
            );

            //strResult = @"{""custName"":""STEPHEN GUNAWAN(RB)"",""custType"":""R"",""banks"":[{""AccountNo"":""1442088922"",""UnderName"":""STEPHEN GUNAWAN"",""BankName"":""PANIN"",""DefaultAccount"":""1"",""AccountStatus"":""A"",""AccountType"":""T""},{""AccountNo"":""1202687352"",""UnderName"":""STEPHEN GUNAWAN"",""BankName"":""PANIN"",""DefaultAccount"":""0"",""AccountStatus"":""A"",""AccountType"":""I""}],""retcode"":0,""transferid"":0,""refid"":0,""ecode"":200}";

            WdCustInfoResponse wdCustInfoResponse = JsonConvert.DeserializeObject<WdCustInfoResponse>(strResult);

            BankAccount bankAccount = new BankAccount();

            if (wdCustInfoResponse != null && wdCustInfoResponse.banks != null && wdCustInfoResponse.banks.Count > 0)
            {
                foreach (Bank bank in wdCustInfoResponse.banks)
                {
                    if (bank.AccountType == "T")
                    {
                        if (bank.AccountStatus == "A" && bank.DefaultAccount == "1")
                        {
                            bankAccount.BankAccountNumber = bank.AccountNo;
                            bankAccount.BankAccountHolderName = bank.UnderName;
                            bankAccount.BankName = bank.BankName;
                        }
                    }
                    else if (bank.AccountType == "I")
                    {
                        if (bank.AccountStatus == "A")
                        {
                            bankAccount.RdnAccountNumber = bank.AccountNo;
                            bankAccount.RdnAccountHolderName = bank.UnderName;
                            bankAccount.RdnName = bank.BankName;
                        }
                    }
                }
            }

            return bankAccount;
        }

        public async Task<WdCancelResponse> WdCancelAsync(
            string brokerAccountId,
            string custId,
            string id,
         CancellationToken cancellationToken
       )
        {
            string strResult;

            strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_ORDERSTATUS}",
                request: new WdCancelRequest(
                    brokerAccountId: brokerAccountId,
                    custId: custId,
                    id: id
                ),
                cancellationToken: cancellationToken
            );

            //{"transferId":0,"ecode":200,"msg":"Request withdrawn"}

            WdCancelResponse wdCancelResponse = JsonConvert.DeserializeObject<WdCancelResponse>(strResult);
            return wdCancelResponse;
        }

        public async Task<WdSubmitNewResponse> WdSubmitAsync(
            string custId,
            decimal amount,
            string payDate,
            string bankAccount,
            string rdnAccount,
            string userid,
            string pincek,
            string passcek,
            string brokerAccountId,
            string inWords,
         CancellationToken cancellationToken
       )
        {
            string strResult;

            strResult = await PostAsync(
                httpClient: _httpClient,
                url: $"{BASE_URL_ORDERSTATUS}",
                request: new WdSubmitNewRequest(
                    custId: custId,
                    amount: amount,
                    payDate: payDate,
                    bankAccount: bankAccount,
                    rdnAccount: rdnAccount,
                    userid: userid,
                    pincek: pincek,
                    passcek: passcek,
                    brokerAccountId: brokerAccountId,
                    inWords: inWords
                ),
                cancellationToken: cancellationToken
            );

            //{"retcode":30,"transferid":30,"refid":186845,"ecode":200,"msg":"Withdrawal Request Accepted"}

            WdSubmitNewResponse wdSubmitNewResponse = JsonConvert.DeserializeObject<WdSubmitNewResponse>(strResult);
            return wdSubmitNewResponse;
        }


        private static async Task<string> PostAsync(HttpClient httpClient, string url, Dictionary<string, string> request, CancellationToken cancellationToken)
        {
            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Post, url))
            {
                httpRequest.Content = new FormUrlEncodedContent(request);
                using (HttpResponseMessage httpResponse = await httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string result = await httpResponse.Content.ReadAsStringAsync();
                    return result;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
