﻿using System;

namespace POST.DataSources
{
    public class ParseFailedEventArgs
    {
        public int BytesDropped { get; }
        public string Message { get; }
        public ParseFailedEventArgs(int bytesDropped, string message)
        {
            BytesDropped = bytesDropped;
            Message = message ?? throw new ArgumentNullException(message);
        }
    }
}
