﻿namespace POST.DataSources
{

    public enum DataFeedStatus
    {

        /// <summary>
        /// Initial state.
        /// </summary>
        Unconnected,

        /// <summary>
        /// Connected to server and receives healthy heartbeats.
        /// </summary>
        Connected,

        /// <summary>
        /// Connected to server but suffers some ping losses.
        /// </summary>
        PingLoss,

        /// <summary>
        /// Server disconnected either gracefully or forcefully. App must try to reconnect automatically.
        /// </summary>
        Disconnected,

        /// <summary>
        /// Server is unreachable. App must listen to connection state changes.
        /// </summary>
        Unreachable,

        /// <summary>
        /// User manually disconnected data feed.
        /// </summary>
        UserDisconnected,

        /// <summary>
        /// Unknown error occured. This shouldn't happen. But when it does, then the code to handle this error should be added to next version.
        /// </summary>
        Error
    }
}
