﻿using System.Collections.Generic;

namespace POST.DataSources.WdMessages.Requests
{
    public class WdListRequest : Dictionary<string, string>
    {
        public WdListRequest(
            string brokerAccountId
        )
        {
            Add("request", "wdList");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
