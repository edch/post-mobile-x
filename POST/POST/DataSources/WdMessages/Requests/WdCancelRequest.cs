﻿using POST.Stores;
using System.Collections.Generic;

namespace POST.DataSources.WdMessages.Requests
{
    public class WdCancelRequest : Dictionary<string, string>
    {
        public WdCancelRequest(
            string brokerAccountId,
            string custId,
            string id

        )
        {
            Add("request", "wdCancel");
            Add("brokerAccountId", brokerAccountId);
            Add("custId", custId);
            Add("sourceId", AppSettings.SourceId);
            //Add("clientIp", AppSettings.ClientIpForWD);
            Add("id", id);
        }
    }
}
