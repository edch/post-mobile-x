﻿using POST.Stores;
using System.Collections.Generic;

namespace POST.DataSources.WdMessages.Requests
{
    public class WdSubmitNewRequest : Dictionary<string, string>
    {
        public WdSubmitNewRequest(
            string custId,
            decimal amount,
            string payDate,
            string bankAccount,
            string rdnAccount,
            string userid,
            string pincek,
            string passcek,
            string brokerAccountId,
            string inWords
        )
        {
            Add("request", "wdSubmitNew");
            Add("custId", custId);
            Add("amount", amount.ToString());
            //Add("clientIp", AppSettings.ClientIpForWD);
            Add("payDate", payDate);
            Add("bankAccount", bankAccount);
            Add("rdnAccount", rdnAccount);
            Add("userid", userid);
            Add("sourceId", AppSettings.SourceId);
            Add("pincek", pincek);
            Add("passcek", passcek);
            Add("brokerAccountId", brokerAccountId);
            Add("inWords", inWords);
        }
    }
}
