﻿using System.Collections.Generic;

namespace POST.DataSources.WdMessages.Requests
{
    public class WdFundInfoRequest : Dictionary<string, string>
    {
        public WdFundInfoRequest(
            string brokerAccountId
        )
        {
            Add("request", "wdInfo");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
