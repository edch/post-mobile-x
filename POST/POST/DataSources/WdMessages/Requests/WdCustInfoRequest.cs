﻿using System.Collections.Generic;

namespace POST.DataSources.WdMessages.Requests
{
    public class WdCustInfoRequest : Dictionary<string, string>
    {
        public WdCustInfoRequest(
            string brokerAccountId
        )
        {
            Add("request", "wdCustInfo");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
