﻿using Newtonsoft.Json;

namespace POST.DataSources.WdMessages.Responses
{
    public class WdCancelResponse
    {
        [JsonProperty("custId")]
        public string custId { get; set; }

        [JsonProperty("transferId")]
        public int transferId { get; set; }

        [JsonProperty("ecode")]
        public int ecode { get; set; }

        [JsonProperty("msg")]
        public string msg { get; set; }
    }
}
