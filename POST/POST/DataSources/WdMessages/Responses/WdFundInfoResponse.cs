﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace POST.DataSources.WdMessages.Responses
{
    public class Rec
    {

        [JsonProperty("ti")]
        public int ti { get; set; }

        [JsonProperty("ddate")]
        public int ddate { get; set; }

        [JsonProperty("value")]
        public decimal value { get; set; }

        [JsonIgnore]
        public DateTime? Date
        {
            get
            {
                DateTime? dateTime = null;

                try
                {
                    if (DateTime.TryParseExact(ddate.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dt))
                    {
                        dateTime = dt;
                    }
                }
                catch
                {

                }


                return dateTime;
            }
        }
    }

    public class WdFundInfoResponse
    {

        [JsonProperty("custid")]
        public string custid { get; set; }

        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("maxday")]
        public int maxday { get; set; }

        [JsonProperty("dsrc")]
        public string dsrc { get; set; }

        [JsonProperty("recs")]
        public IList<Rec> recs { get; set; }

        [JsonProperty("ecode")]
        public int ecode { get; set; }
    }
}
