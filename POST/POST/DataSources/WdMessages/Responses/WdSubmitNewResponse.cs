﻿using Newtonsoft.Json;

namespace POST.DataSources.WdMessages.Responses
{
    public class WdSubmitNewResponse
    {
        [JsonProperty("retcode")]
        public int retcode { get; set; }

        [JsonProperty("transferid")]
        public int transferid { get; set; }

        [JsonProperty("refid")]
        public int refid { get; set; }

        [JsonProperty("ecode")]
        public int ecode { get; set; }

        [JsonProperty("msg")]
        public string msg { get; set; }
    }
}
