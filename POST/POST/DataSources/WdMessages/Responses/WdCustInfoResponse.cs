﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace POST.DataSources.WdMessages.Responses
{
    public class Bank
    {

        [JsonProperty("AccountNo")]
        public string AccountNo { get; set; }

        [JsonProperty("UnderName")]
        public string UnderName { get; set; }

        [JsonProperty("BankName")]
        public string BankName { get; set; }

        [JsonProperty("DefaultAccount")]
        public string DefaultAccount { get; set; }

        [JsonProperty("AccountStatus")]
        public string AccountStatus { get; set; }

        [JsonProperty("AccountType")]
        public string AccountType { get; set; }
    }

    public class WdCustInfoResponse
    {

        [JsonProperty("custName")]
        public string custName { get; set; }

        [JsonProperty("custType")]
        public string custType { get; set; }

        [JsonProperty("banks")]
        public IList<Bank> banks { get; set; }

        [JsonProperty("retcode")]
        public int retcode { get; set; }

        [JsonProperty("transferid")]
        public int transferid { get; set; }

        [JsonProperty("refid")]
        public int refid { get; set; }

        [JsonProperty("ecode")]
        public int ecode { get; set; }
    }
}
