﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace POST.DataSources.WdMessages.Responses
{
    public class FundWithdrawItem
    {
        [JsonProperty("rqdate")]
        public string RqDate { get; set; }

        [JsonProperty("txdate")]
        public string TxDate { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("inputUser")]
        public string InputUser { get; set; }

        [JsonProperty("bankAccount")]
        public string BankAccount { get; set; }

        [JsonProperty("bankAccName")]
        public string BankAccName { get; set; }

        [JsonProperty("bankName")]
        public string BankName { get; set; }

        [JsonProperty("sdesc")]
        public string Sdesc { get; set; }

        [JsonProperty("custId")]
        public string CustId { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("wdtype")]
        public int WdType { get; set; }

        [JsonProperty("swdtype")]
        public string SwdType { get; set; }

        [JsonIgnore]
        public string DestinationAccount
        {
            get
            {
                return BankName + " " + BankAccount;
            }
        }
    }

    public class WdListResponse
    {
        [JsonProperty("list")]
        public List<FundWithdrawItem> List { get; set; }

        [JsonProperty("ecode")]
        public int Ecode { get; set; }
    }

}
