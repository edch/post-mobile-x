﻿using Newtonsoft.Json;
using POST.DataSources.ApiResponses;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class CompanyProfileClient : IDisposable
    {
        public const string URL = "https://chart.post-pro.co.id/companyprofile";

        private static readonly Lazy<CompanyProfileClient> _singletonFactory = new Lazy<CompanyProfileClient>(() => new CompanyProfileClient());
        public static CompanyProfileClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        public CompanyProfileClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<CompanyProfile> GetCompanyProfileAsync(string stockCode, CancellationToken cancellationToken)
        {
            if (stockCode == null)
            {
                throw new ArgumentNullException(nameof(stockCode));
            }

            string requestUrl = $"{URL}/{stockCode}";
            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, requestUrl))
            {
                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string json = await httpResponse.Content.ReadAsStringAsync();
                    CompanyProfile companyProfile = JsonConvert.DeserializeObject<CompanyProfile>(json);
                    return companyProfile;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

    }
}
