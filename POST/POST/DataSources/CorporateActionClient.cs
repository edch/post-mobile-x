﻿using Newtonsoft.Json;
using POST.DataSources.ApiResponses;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class CorporateActionClient : IDisposable
    {
        public const string URL = "https://chart.post-pro.co.id/api/v1/news/corpact";

        private static readonly Lazy<CorporateActionClient> _singletonFactory = new Lazy<CorporateActionClient>(() => new CorporateActionClient());
        public static CorporateActionClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        public CorporateActionClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<CorporateAction[]> GetCorporateActionAsync(string stockCode, CancellationToken cancellationToken)
        {
            if (stockCode == null)
            {
                throw new ArgumentNullException(nameof(stockCode));
            }

            string requestUrl = $"{URL}/{stockCode.ToUpper()}";
            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, requestUrl))
            {
                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string json = await httpResponse.Content.ReadAsStringAsync();
                    CorporateAction[] corporateAction = JsonConvert.DeserializeObject<CorporateAction[]>(json);
                    return corporateAction;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

    }
}
