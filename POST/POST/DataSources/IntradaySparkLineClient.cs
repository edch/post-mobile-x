﻿using Newtonsoft.Json;
using POST.DataSources.ApiResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class IntradaySparkLineClient : IDisposable
    {

        //public const string URL = "http://202.78.206.115:8080/chart/spark10min";
        public const string URL = "https://chart.post-pro.co.id/mobilex/v1/chart/spark10min";

        private static readonly Lazy<IntradaySparkLineClient> _singletonFactory = new Lazy<IntradaySparkLineClient>(() => new IntradaySparkLineClient());
        public static IntradaySparkLineClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        public IntradaySparkLineClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<IReadOnlyDictionary<string, IntradaySparkLineData>> GetSparkLinesAsync(ISet<string> stockCodes, CancellationToken cancellationToken)
        {
            Dictionary<string, IntradaySparkLineData> sparkLineModelByStockCode = new Dictionary<string, IntradaySparkLineData>();
            Queue<string> stockCodeQueue = new Queue<string>(stockCodes);
            while (stockCodeQueue.Any())
            {
                List<string> stockCodeBatch = new List<string>();
                while (stockCodeBatch.Count < 10 && stockCodeQueue.Any())
                {
                    stockCodeBatch.Add(stockCodeQueue.Dequeue());
                }
                string requestUrl = $"{URL}?code={string.Join(",", stockCodeBatch)}";
                using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, requestUrl))
                {
                    try
                    {
                        using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                        {
                            string json = await httpResponse.Content.ReadAsStringAsync();
                            IntradaySparkLineData[] sparkLineModels = JsonConvert.DeserializeObject<IntradaySparkLineData[]>(json);
                            foreach (IntradaySparkLineData sparkLineModel in sparkLineModels)
                            {
                                sparkLineModelByStockCode.TryAdd(sparkLineModel.StockCode, sparkLineModel);
                            }
                        }
                    }
                    catch (OperationCanceledException)
                    {
                        throw;
                    }
                    catch (ObjectDisposedException)
                    {
                        throw new OperationCanceledException();
                    }
                    catch
                    {
                        // Exception handling policy: skip failed items
                        continue;
                    }
                }
            }
            return sparkLineModelByStockCode;
        }

        public async Task<IntradaySparkLineData> GetSparkLineAsync(string stockCode, CancellationToken cancellationToken)
        {
            IReadOnlyDictionary<string, IntradaySparkLineData> sparkLineModelByStockCode = await GetSparkLinesAsync(new HashSet<string> { stockCode }, cancellationToken);
            if (sparkLineModelByStockCode.TryGetValue(stockCode, out IntradaySparkLineData sparkLineModel))
            {
                return sparkLineModel;
            }
            else
            {
                return null;
            }
        }

        private class RequestPool
        {
            private readonly IntradaySparkLineClient _client;
            private readonly ISet<string> _stockCodes;
            private readonly CancellationTokenSource _commencementTrigger;
            private readonly TaskCompletionSource<ISet<string>> _commencementSource;
            private TaskCompletionSource<IReadOnlyDictionary<string, IntradaySparkLineData>> _completionSource;
            public bool Commenced { get; private set; }

            public RequestPool(IntradaySparkLineClient client, string stockCode)
            {
                _client = client ?? throw new ArgumentNullException(nameof(client));
                _stockCodes = new HashSet<string> { stockCode ?? throw new ArgumentNullException(nameof(stockCode)) };
                _commencementSource = new TaskCompletionSource<ISet<string>>();
                Commenced = false;

                _commencementTrigger = new CancellationTokenSource(TimeSpan.FromMilliseconds(500));
                _commencementTrigger.Token.Register(Commence);
            }

            public async Task<IntradaySparkLineData> GetSparkLineAsync(string stockCode, CancellationToken cancellationToken)
            {
                lock (this)
                {
                    if (Commenced)
                    {
                        throw new InvalidOperationException("This RequestPool is already locked.");
                    }
                }
                if (_stockCodes.Add(stockCode) &&
                    _stockCodes.Count >= 10)
                {
                    Commence();
                }
                ISet<string> stockCodes = await _commencementSource.Task;
                if (_completionSource == null)
                {
                    _completionSource = new TaskCompletionSource<IReadOnlyDictionary<string, IntradaySparkLineData>>();
                    try
                    {
                        _completionSource.SetResult(await _client.GetSparkLinesAsync(stockCodes, cancellationToken));
                    }
                    catch (Exception exc)
                    {
                        _completionSource.SetException(exc);
                    }
                }
                IReadOnlyDictionary<string, IntradaySparkLineData> sparkLineModelByStockCode = await _completionSource.Task;
                if (sparkLineModelByStockCode.TryGetValue(stockCode, out IntradaySparkLineData sparkLineModel))
                {
                    return sparkLineModel;
                }
                else
                {
                    return null;
                }
            }

            private void Commence()
            {
                lock (this)
                {
                    if (!Commenced)
                    {
                        Commenced = true;
                        _client._requestQueue.Remove(this);
                        _commencementTrigger.Dispose();
                        _commencementSource.SetResult(_stockCodes);
                    }
                }
            }
        }

        private readonly List<RequestPool> _requestQueue = new List<RequestPool>();

        public async Task<IntradaySparkLineData> GetSparkLinePooledAsync(string stockCode, CancellationToken cancellationToken)
        {
            if (_requestQueue.Count == 0 ||
                _requestQueue.Last().Commenced)
            {
                RequestPool pool = new RequestPool(this, stockCode);
                _requestQueue.Add(pool);
                return await pool.GetSparkLineAsync(stockCode, cancellationToken);
            }
            else
            {
                RequestPool pool = _requestQueue.Last();
                return await pool.GetSparkLineAsync(stockCode, cancellationToken);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
