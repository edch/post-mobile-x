﻿using POST.DataSources.DataFeedMessages;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class StockInitClient : IDisposable
    {
        //private const string ATHENA_URL_MI_STOCKINIT = "http://athena.post-pro.co.id/mi2/marketInfoData?request=stockInit";
        private const string ATHENA_URL_MI_STOCKINIT = "http://trx.lc.pansolt.co.id/mi2/marketInfoData?request=stockInit";

        private static readonly Lazy<StockInitClient> _singletonFactory = new Lazy<StockInitClient>(() => new StockInitClient());
        public static StockInitClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        private readonly CultureInfo usProvider = new CultureInfo("en-US");

        public StockInitClient()
        {
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
        }

        private class Response
        {
            public List<object> data { get; set; }
            public string id { get; set; }
        }

        public async Task<List<StockInitMessage>> GetStockInitAsync(CancellationToken cancellationToken)
        {
            //{"data":["AALI","NG","Astra Agro Lestari Tbk.",12650,1,"Plantation",false,true,100,1924688333,"--S1K--1"],"id":"AALI.NG"}
            string regex = @"\{""data"":\[""(?<StockCode>.*?)"",""(?<marketTypeString>.*?)"",""(?<StockName>.*?)"",(?<PrevPrice>.*?),(?<SectorId>.*?),""(?<SubSector>.*?)"",(?<IsLQ45>.*?),(?<IsSyariah>.*?),(?<SharesPerLot>.*?),(?<TotalShare>.*?),""(?<Remarks>.*?)""],""id"":""(?<StockId>.*?)""}";

            List<StockInitMessage> lstStockInitMessage = new List<StockInitMessage>();

            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, ATHENA_URL_MI_STOCKINIT))
            {
                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string strResult = "";

                    if (httpResponse.Content.Headers.ContentEncoding.Contains("gzip"))
                    {
                        using (Stream s = await httpResponse.Content.ReadAsStreamAsync())
                        {
                            using (GZipStream decompressed = new GZipStream(s, CompressionMode.Decompress))
                            {
                                using (StreamReader rdr = new StreamReader(decompressed))
                                {
                                    strResult = await rdr.ReadToEndAsync();
                                }
                            }
                        }
                    }
                    else
                    {   // Use standard implementation if not compressed
                        strResult = await httpResponse.Content.ReadAsStringAsync();
                    }

                    MatchCollection matchCollection = Regex.Matches(strResult, regex);

                    foreach (Match match in matchCollection)
                    {
                        lstStockInitMessage.Add(new StockInitMessage(match.Groups["StockCode"].Value, match.Groups["marketTypeString"].Value, match.Groups["StockName"].Value, decimal.Parse(match.Groups["PrevPrice"].Value, System.Globalization.NumberStyles.Any, usProvider), int.Parse(match.Groups["SectorId"].Value), match.Groups["SubSector"].Value, match.Groups["IsSyariah"].Value, match.Groups["IsLQ45"].Value, int.Parse(match.Groups["SharesPerLot"].Value), long.Parse(match.Groups["TotalShare"].Value), match.Groups["Remarks"].Value));
                    }
                }
            }

            return lstStockInitMessage;
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

    }
}
