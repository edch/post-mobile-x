﻿using Newtonsoft.Json;

namespace POST.DataSources.TaxReportMessages
{
    public class TaxReportMessageReq
    {
        public int ReportYear { get; set; }
        /// <summary>
        /// 0 = Transaction Report; 1 = Dividend Report
        /// </summary>
        public int ReportType { get; set; }
        public int CustomerNo { get; set; }
        /// <summary>
        /// 'A' = All; 'B' = Buy; 'S' = Sell
        /// </summary>
        public char OrderType { get; set; }

        [JsonIgnore]
        public string ReportType_String
        {
            get
            {
                string strReportType = null;

                if (ReportType == 0)
                {
                    strReportType = "Transaction Report";
                    if (OrderType == 'A')
                    {
                        strReportType += " (All)";
                    }
                    else if (OrderType == 'B')
                    {
                        strReportType += " (Buy)";
                    }
                    else if (OrderType == 'S')
                    {
                        strReportType += " (Sell)";
                    }
                }
                else if (ReportType == 1)
                {
                    strReportType = "Dividend Report";
                }

                return strReportType;
            }
        }

    }
}
