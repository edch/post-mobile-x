﻿using POST.DataSources.ApiResponses;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class BrokerAccDistClient : IDisposable
    {
        public const string URL = "http://chart.post-pro.co.id:3000/bdrD?";

        private static readonly Lazy<BrokerAccDistClient> _singletonFactory = new Lazy<BrokerAccDistClient>(() => new BrokerAccDistClient());
        public static BrokerAccDistClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        private readonly CultureInfo usProvider = new CultureInfo("en-US");

        public BrokerAccDistClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<List<BrokerAccDist>> GetBrokerAccDistAsync(string stockCode, string startDate, string endDate, CancellationToken cancellationToken, string fd = null, string board = null)
        {
            if (stockCode == null)
            {
                throw new ArgumentNullException(nameof(stockCode));
            }

            string requestUrl = $"{URL}stock={stockCode.ToUpper()}&start={startDate}&end={endDate}";
            if (fd != null)
            {
                requestUrl += $"&fd={fd}";
            }
            if (board != null)
            {
                requestUrl += $"&board={board}";
            }

            List<BrokerAccDist> lstBrokerAccDist = new List<BrokerAccDist>();

            //{"code":"YU","selltvol":"357036142","selltval":"7927950780100","buytvol":"304513612","buytval":"6758943460600","netvol":"-52522530"}#
            string regex = @"\{""code"":""(?<code>.*?)"",""selltvol"":""(?<selltvol>.*?)"",""selltval"":""(?<selltval>.*?)"",""buytvol"":""(?<buytvol>.*?)"",""buytval"":""(?<buytval>.*?)"",""netvol"":""(?<netvol>.*?)""}#";

            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, requestUrl))
            {
                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string strResult = await httpResponse.Content.ReadAsStringAsync();
                    MatchCollection matchCollection = Regex.Matches(strResult, regex);
                    foreach (Match match in matchCollection)
                    {
                        lstBrokerAccDist.Add(new BrokerAccDist(match.Groups["code"].Value, decimal.Parse(match.Groups["selltvol"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["selltval"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["buytvol"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["buytval"].Value, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(match.Groups["netvol"].Value, System.Globalization.NumberStyles.Any, usProvider)));
                    }
                }
            }

            return lstBrokerAccDist;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

    }
}
