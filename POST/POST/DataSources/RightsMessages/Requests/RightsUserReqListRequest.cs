﻿using System.Collections.Generic;

namespace POST.DataSources.RightsMessages.Requests
{
    public class RightsUserReqListRequest : Dictionary<string, string>
    {
        public RightsUserReqListRequest(
              string brokerAccountId,
              string custid
        )
        {
            Add("request", "rightsList");
            Add("brokerAccountId", brokerAccountId);
            Add("custid", custid);
        }
    }
}
