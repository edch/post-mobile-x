﻿using System.Collections.Generic;

namespace POST.DataSources.RightsMessages.Requests
{
    public class RightsAvailableRequest : Dictionary<string, string>
    {
        public RightsAvailableRequest(
              string brokerAccountId
        )
        {
            Add("request", "rightsAvailable");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
