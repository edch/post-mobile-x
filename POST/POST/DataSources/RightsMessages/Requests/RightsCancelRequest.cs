﻿using System.Collections.Generic;

namespace POST.DataSources.RightsMessages.Requests
{
    public class RightsCancelRequest : Dictionary<string, string>
    {
        public RightsCancelRequest(
              string brokerAccountId,
              string id
        )
        {
            Add("request", "rightsCancel");
            Add("brokerAccountId", brokerAccountId);
            Add("id", id);
        }
    }
}
