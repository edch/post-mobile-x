﻿using POST.Stores;
using System.Collections.Generic;

namespace POST.DataSources.RightsMessages.Requests
{
    public class PlaceRightsRequest : Dictionary<string, string>
    {
        public PlaceRightsRequest(
              string brokerAccountId,
              string userid,
              string custid,
              string rightid,
              decimal share
        )
        {
            Add("request", "rightsNew");
            Add("brokerAccountId", brokerAccountId);
            Add("userid", userid);
            Add("custid", custid);
            Add("rightid", rightid);
            Add("share", share.ToString());
            Add("sourceId", AppSettings.SourceId);
        }
    }
}
