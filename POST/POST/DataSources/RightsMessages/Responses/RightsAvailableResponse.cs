﻿namespace POST.DataSources.RightsMessages.Responses
{
    public class RightsAvailableResponse
    {
        public string serverDate { get; set; }
        public RightsAvailableItem[] actions { get; set; }
        public int ecode { get; set; }
        public string msg { get; set; }
    }

    public class RightsAvailableItem
    {
        public string rightId { get; set; }
        public string stockId { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string action { get; set; }
        public decimal exPrice { get; set; }
        public int status { get; set; }
        public string sstatus { get; set; }
        public decimal excost_perlot { get; set; }
        public decimal excost_pershare { get; set; }
        public decimal excost_pertrx { get; set; }
    }

}
