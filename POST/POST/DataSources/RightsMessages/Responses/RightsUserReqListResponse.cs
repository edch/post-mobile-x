﻿namespace POST.DataSources.RightsMessages.Responses
{
    public class RightsUserReqListResponse
    {
        public RightsUserReqItem[] items { get; set; }
        public int ecode { get; set; }
        public string msg { get; set; }
    }

    public class RightsUserReqItem
    {
        public string rightid { get; set; }
        public string startdate { get; set; }
        public decimal share { get; set; }
        public int status { get; set; }
        public decimal value { get; set; }
        public string sstatus { get; set; }
        public int id { get; set; }
        public int transferid { get; set; }
        public string notetoclient { get; set; }
        public decimal? doneshare { get; set; }
        public string requesttime { get; set; }
        public int? fundstatus { get; set; }
        public string custId { get; set; }
        public int sourceid { get; set; }
        public int serverid { get; set; }
    }

}
