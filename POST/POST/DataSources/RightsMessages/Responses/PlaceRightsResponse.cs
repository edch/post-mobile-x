﻿namespace POST.DataSources.RightsMessages.Responses
{
    public class PlaceRightsResponse
    {
        public int newid { get; set; }
        public int tranferid { get; set; }
        public int ecode { get; set; }
        public string msg { get; set; }
    }

}
