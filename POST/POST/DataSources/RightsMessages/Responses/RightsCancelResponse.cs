﻿namespace POST.DataSources.RightsMessages.Responses
{
    public class RightsCancelResponse
    {
        public int ecode { get; set; }
        public string msg { get; set; }
    }

}
