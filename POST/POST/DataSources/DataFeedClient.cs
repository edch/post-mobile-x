﻿using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Models;
using POST.Stores;
using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public partial class DataFeedClient : IDisposable
    {

        //public const string DEFAULT_HOST = "orca.post-pro.co.id";
        //public const string DEFAULT_HOST = "df.lc.pansolt.co.id";
        //public const int DEFAULT_PORT = 1235;

        private const int BUFFER_SIZE = 100000;
        private static readonly byte[] BUFFER = new byte[BUFFER_SIZE];
        private static readonly SemaphoreSlim BUFFER_SEMAPHORE = new SemaphoreSlim(1, 1);

        public string Host { get; } = "df.lc.pansolt.co.id";
        public int Port { get; } = 1235;

        public event DataReceivedEventHandler DataReceived;
        public event ParseFailedEventHandler ParseFailed;
        public event ClientConnectedEventHandler Connected;
        public event ClientDisconnectedEventHandler Disconnected;

        private TcpClient _tcpClient;
        private TaskCompletionSource<bool> _clientConnectedSource;

        //private byte[] _buffer = new byte[BUFFER_SIZE];

        public bool IsConnected => _tcpClient?.Connected ?? false;

        public DataFeedClient()
        {
            if (AppSettings.DatafeedServer_Selected == (int)DatafeedServerType.Live)
            {
                Host = "df.lc.pansolt.co.id";
                Port = 1235;
            }
            else
            if (AppSettings.DatafeedServer_Selected == (int)DatafeedServerType.Mock)
            {
                Host = "orca.post-pro.co.id";
                Port = 1255;
            }
        }

        public async Task<bool> SendAsync<TRequest>(TRequest request)
            where TRequest : DataFeedRequestBase
        {
            if (request == default)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (!disposedValue &&
                _clientConnectedSource != null &&
                await _clientConnectedSource.Task &&
                _tcpClient != null &&
                _tcpClient.Connected)
            {

                //Debug.WriteLine($"SEND: {request.RequestType} {string.Join(" ", request.Parameters ?? new object[0])}");

                try
                {
                    NetworkStream networkStream = _tcpClient.GetStream();
                    byte[] commandBytes = request.ToBinary();
                    await networkStream.WriteAsync(commandBytes, 0, commandBytes.Length);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public async Task ConnectAsync()
        {
            _clientConnectedSource = new TaskCompletionSource<bool>();
            try
            {
                // Throws SocketException
                _tcpClient = new TcpClient(Host, Port)
                {
                    ReceiveBufferSize = BUFFER_SIZE
                };
                byte[] packetSizeBuffer = new byte[2];

                if (_tcpClient != null && _tcpClient.Connected)
                {
                    _clientConnectedSource.SetResult(true);
                    Connected?.Invoke(this);

                    try
                    {
                        using (NetworkStream networkStream = _tcpClient.GetStream())
                        {
                            while (!disposedValue && _tcpClient != null && _tcpClient.Connected)
                            {

                                // Read packet size
                                int readCount = await networkStream.ReadAsync(packetSizeBuffer, 0, 2);

                                await BUFFER_SEMAPHORE.WaitAsync();
                                try
                                {
                                    if (readCount < 2)
                                    {
                                        // Ignore remaining bytes
                                        if (_tcpClient?.Available > 0)
                                        {
                                            readCount = await networkStream.ReadAsync(BUFFER, 0, BUFFER_SIZE);
                                            //Debug.WriteLine($"BAD: {readCount} bytes dropped.");
                                            ParseFailed?.Invoke(this, new ParseFailedEventArgs(readCount, "Stream contains less than 2 bytes."));
                                        }
                                        continue;
                                    }

                                    int packetSize = packetSizeBuffer[0] << 8 | packetSizeBuffer[1];

                                    // Read packet
                                    readCount = await networkStream.ReadAsync(BUFFER, 0, packetSize);
                                    if (readCount != packetSize)
                                    {
                                        // Ignore remaining bytes
                                        if (_tcpClient?.Available > 0)
                                        {
                                            readCount = await networkStream.ReadAsync(BUFFER, 0, BUFFER_SIZE);
                                            //Debug.WriteLine($"BAD: {readCount} bytes dropped.");
                                            ParseFailed?.Invoke(this, new ParseFailedEventArgs(readCount, "Stream contains less bytes than expected."));
                                        }
                                        continue;
                                    }

                                    // Parse packet
                                    HandlePacket(0, packetSize);
                                }
                                finally
                                {
                                    BUFFER_SEMAPHORE.Release();
                                }
                            }
                        }
                    }
                    catch (OperationCanceledException)
                    {
                        Disconnected?.Invoke(this, new ClientDisconnectedEventArgs(SocketError.Disconnecting));
                    }
                    catch (ObjectDisposedException)
                    {
                        return;
                    }
                    catch (SocketException exc)
                    {
                        Disconnected?.Invoke(this, new ClientDisconnectedEventArgs(exc.SocketErrorCode));
                    }
                    catch (IOException)
                    {
                        Disconnected?.Invoke(this, new ClientDisconnectedEventArgs(SocketError.ConnectionReset));
                    }
                    catch (Exception exc)
                    {
                        // Unhandled exception. Use this statement as a break point.
                        if (exc != null)
                        {
                            return;
                        }
                    }
                    finally
                    {
                        Dispose();
                    }
                }
                else
                {
                    _clientConnectedSource.SetException(new SocketException((int)SocketError.NotConnected));
                }
            }
            catch (SocketException exc)
            {
                Disconnected?.Invoke(this, new ClientDisconnectedEventArgs(exc.SocketErrorCode));
                return;
            }
            catch (Exception exc)
            {
                // Unhandled exception. Use this statement as a break point.
                if (exc != null)
                {
                    return;
                }
            }
        }

        private void HandlePacket(int offset, int size)
        {
            MessageType messageType = (MessageType)BUFFER[offset];
            //Debug.WriteLine($"RECV: {messageType} {packet.Count} Bytes");
            try
            {
                switch (messageType)
                {
                    case MessageType.StockInit:
                        StockInitMessage StockInitMsg = new StockInitMessage(BUFFER, offset, size);
                        DataReceivedEventArgs StockInitArgs = new DataReceivedEventArgs(StockInitMsg);
                        DataReceived?.Invoke(this, StockInitArgs);
                        StockInitArgs = null;
                        StockInitMsg = null;
                        break;
                    case MessageType.StockInitEnd:
                        StockInitEndMessage StockInitEndMsg = new StockInitEndMessage(BUFFER, offset, size);
                        DataReceivedEventArgs StockInitEndArgs = new DataReceivedEventArgs(StockInitEndMsg);
                        DataReceived?.Invoke(this, StockInitEndArgs);
                        StockInitEndArgs = null;
                        StockInitEndMsg = null;
                        break;
                    case MessageType.StockChart:
                        StockChartMessage StockChartMsg = new StockChartMessage(BUFFER, offset, size);
                        DataReceivedEventArgs StockChartArgs = new DataReceivedEventArgs(StockChartMsg);
                        DataReceived?.Invoke(this, StockChartArgs);
                        StockChartArgs = null;
                        StockChartMsg = null;
                        break;
                    case MessageType.StockChartEnd:
                        StockChartEndMessage StockChartEndMsg = new StockChartEndMessage(BUFFER, offset, size);
                        DataReceivedEventArgs StockChartEndArgs = new DataReceivedEventArgs(StockChartEndMsg);
                        DataReceived?.Invoke(this, StockChartEndArgs);
                        StockChartEndArgs = null;
                        StockChartEndMsg = null;
                        break;
                    case MessageType.RegionalIndex:
                        RegionalIndexMessage RegionalIndexMsg = new RegionalIndexMessage(BUFFER, offset, size);
                        DataReceivedEventArgs RegionalIndexArgs = new DataReceivedEventArgs(RegionalIndexMsg);
                        DataReceived?.Invoke(this, RegionalIndexArgs);
                        RegionalIndexArgs = null;
                        RegionalIndexMsg = null;
                        break;
                    case MessageType.RegionalIndexEnd:
                        RegionalIndexEndMessage RegionalIndexEndMsg = new RegionalIndexEndMessage(BUFFER, offset, size);
                        DataReceivedEventArgs RegionalIndexEndArgs = new DataReceivedEventArgs(RegionalIndexEndMsg);
                        DataReceived?.Invoke(this, RegionalIndexEndArgs);
                        RegionalIndexEndArgs = null;
                        RegionalIndexEndMsg = null;
                        break;
                    case MessageType.Currency:
                        CurrencyMessage CurrencyMsg = new CurrencyMessage(BUFFER, offset, size);
                        DataReceivedEventArgs CurrencyArgs = new DataReceivedEventArgs(CurrencyMsg);
                        DataReceived?.Invoke(this, CurrencyArgs);
                        CurrencyArgs = null;
                        CurrencyMsg = null;
                        break;
                    case MessageType.CurrencyEnd:
                        CurrencyEndMessage CurrencyEndMsg = new CurrencyEndMessage(BUFFER, offset, size);
                        DataReceivedEventArgs CurrencyEndArgs = new DataReceivedEventArgs(CurrencyEndMsg);
                        DataReceived?.Invoke(this, CurrencyEndArgs);
                        CurrencyEndArgs = null;
                        CurrencyEndMsg = null;
                        break;
                    case MessageType.NewsList:
                        break;
                    case MessageType.NewsListEnd:
                        break;
                    case MessageType.RunningTrade:
                        //Debug.WriteLine("RT");
                        RunningTradeMessage RunningTradeMsg = new RunningTradeMessage(BUFFER, offset, size);
                        DataReceivedEventArgs RunningTradeArgs = new DataReceivedEventArgs(RunningTradeMsg);
                        DataReceived?.Invoke(this, RunningTradeArgs);
                        RunningTradeArgs = null;
                        RunningTradeMsg = null;
                        break;
                    case MessageType.Broker:
                        BrokerInitMessage BrokerInitMsg = new BrokerInitMessage(BUFFER, offset, size);
                        DataReceivedEventArgs BrokerInitArgs = new DataReceivedEventArgs(BrokerInitMsg);
                        DataReceived?.Invoke(this, BrokerInitArgs);
                        BrokerInitArgs = null;
                        BrokerInitMsg = null;
                        break;
                    case MessageType.BrokerEnd:
                        BrokerInitEndMessage BrokerInitEndMsg = new BrokerInitEndMessage(BUFFER, offset, size);
                        DataReceivedEventArgs BrokerInitEndArgs = new DataReceivedEventArgs(BrokerInitEndMsg);
                        DataReceived?.Invoke(this, BrokerInitEndArgs);
                        BrokerInitEndArgs = null;
                        BrokerInitEndMsg = null;
                        break;
                    case MessageType.BrokerQuote:
                        BrokerQuoteMessage BrokerQuoteMsg = new BrokerQuoteMessage(BUFFER, offset, size);
                        DataReceivedEventArgs BrokerQuoteArgs = new DataReceivedEventArgs(BrokerQuoteMsg);
                        DataReceived?.Invoke(this, BrokerQuoteArgs);
                        BrokerQuoteArgs = null;
                        BrokerQuoteMsg = null;
                        break;
                    case MessageType.StockQuote:
                        StockQuoteMessage StockQuoteMsg = new StockQuoteMessage(BUFFER, offset, size);
                        DataReceivedEventArgs StockQuoteArgs = new DataReceivedEventArgs(StockQuoteMsg);
                        DataReceived?.Invoke(this, StockQuoteArgs);
                        StockQuoteArgs = null;
                        StockQuoteMsg = null;
                        break;
                    case MessageType.IndexQuote:
                        IndexQuoteMessage IndexQuoteMsg = new IndexQuoteMessage(BUFFER, offset, size);
                        DataReceivedEventArgs IndexQuoteArgs = new DataReceivedEventArgs(IndexQuoteMsg);
                        DataReceived?.Invoke(this, IndexQuoteArgs);
                        IndexQuoteArgs = null;
                        IndexQuoteMsg = null;
                        break;
                    case MessageType.NewsDetails:
                        break;
                    case MessageType.OrderBook:
                        OrderBookMessage OrderBookMsg = new OrderBookMessage(BUFFER, offset, size);
                        DataReceivedEventArgs OrderBookArgs = new DataReceivedEventArgs(OrderBookMsg);
                        DataReceived?.Invoke(this, OrderBookArgs);
                        OrderBookArgs = null;
                        OrderBookMsg = null;
                        break;
                    case MessageType.TradeBook:
                        TradeBookMessage TradeBookMsg = new TradeBookMessage(BUFFER, offset, size);
                        DataReceivedEventArgs TradeBookArgs = new DataReceivedEventArgs(TradeBookMsg);
                        DataReceived?.Invoke(this, TradeBookArgs);
                        TradeBookArgs = null;
                        TradeBookMsg = null;
                        break;
                    case MessageType.Fundamental:
                        break;
                    case MessageType.String:
                        break;
                    case MessageType.ServerRequestPing:
                        ServerRequestPingMessage ServerRequestPingMsg = new ServerRequestPingMessage(BUFFER, offset, size);
                        DataReceivedEventArgs ServerRequestPingArgs = new DataReceivedEventArgs(ServerRequestPingMsg);
                        DataReceived?.Invoke(this, ServerRequestPingArgs);
                        ServerRequestPingArgs = null;
                        ServerRequestPingMsg = null;
                        break;
                    case MessageType.ServerResponsePing:
                        ServerResponsePingMessage ServerResponsePingMsg = new ServerResponsePingMessage(BUFFER, offset, size);
                        DataReceivedEventArgs ServerResponsePingArgs = new DataReceivedEventArgs(ServerResponsePingMsg);
                        DataReceived?.Invoke(this, ServerResponsePingArgs);
                        ServerResponsePingArgs = null;
                        ServerResponsePingMsg = null;
                        break;
                    case MessageType.Transaction:
                        break;
                    case MessageType.StockSummary:
                        StockSummaryMessage StockSummaryMsg = new StockSummaryMessage(BUFFER, offset, size);
                        DataReceivedEventArgs StockSummaryArgs = new DataReceivedEventArgs(StockSummaryMsg);
                        DataReceived?.Invoke(this, StockSummaryArgs);
                        StockSummaryArgs = null;
                        StockSummaryMsg = null;
                        break;
                    case MessageType.StockAdvertisement:
                        StockAdvertisementMessage StockAdvertisementMsg = new StockAdvertisementMessage(BUFFER, offset, size);
                        DataReceivedEventArgs StockAdvertisementArgs = new DataReceivedEventArgs(StockAdvertisementMsg);
                        DataReceived?.Invoke(this, StockAdvertisementArgs);
                        StockAdvertisementArgs = null;
                        StockAdvertisementMsg = null;
                        break;
                    case MessageType.BrokerSummary:
                        break;
                    case MessageType.BrokerDetail:
                        break;
                    case MessageType.OrderTracking:
                        OrderTrackingMessage OrderTrackingMsg = new OrderTrackingMessage(BUFFER, offset, size);
                        DataReceivedEventArgs OrderTrackingArgs = new DataReceivedEventArgs(OrderTrackingMsg);
                        DataReceived?.Invoke(this, OrderTrackingArgs);
                        OrderTrackingArgs = null;
                        OrderTrackingMsg = null;
                        break;
                    case MessageType.IndexChart:
                        IndexChartMessage IndexChartMsg = new IndexChartMessage(BUFFER, offset, size);
                        DataReceivedEventArgs IndexChartArgs = new DataReceivedEventArgs(IndexChartMsg);
                        DataReceived?.Invoke(this, IndexChartArgs);
                        IndexChartArgs = null;
                        IndexChartMsg = null;
                        break;
                    case MessageType.IndexChartEnd:
                        IndexChartMessage IndexChartEndMsg = new IndexChartMessage(BUFFER, offset, size);
                        DataReceivedEventArgs IndexChartEndArgs = new DataReceivedEventArgs(IndexChartEndMsg);
                        DataReceived?.Invoke(this, IndexChartEndArgs);
                        IndexChartEndArgs = null;
                        IndexChartEndMsg = null;
                        break;
                    default:
                        ParseFailedEventArgs ParseFailedArgs = new ParseFailedEventArgs(size, $"Unknown message type: {messageType}");
                        ParseFailed?.Invoke(this, ParseFailedArgs);
                        ParseFailedArgs = null;
                        break;
                }
            }
            catch (Exception exc)
            {
                //Debug.WriteLine($"EXC: {exc.GetType()} {exc.Message} {exc.StackTrace}");
                ParseFailed?.Invoke(this, new ParseFailedEventArgs(size, exc.Message));
                return;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    if (_tcpClient != null)
                    {
                        _tcpClient.Close();
                        _tcpClient = null;
                    }
                    TaskCompletionSource<bool> temp = _clientConnectedSource;
                    _clientConnectedSource = new TaskCompletionSource<bool>();
                    if (temp != null)
                    {
                        temp.TrySetResult(false);
                    }
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
