﻿using POST.Constants;
using POST.Models;
using System;
using Xamarin.Forms;

namespace POST.DataSources.AthenaMessages.Responses
{
    public class ConditionalOrder
    {
        public string OrderId { get; }

        public string CreatedTime { get; }

        public string ExecutedTime { get; }

        public string UpdateTime { get; }

        public string StockCode { get; }

        public string MarketCode { get; }

        public decimal O_Volume { get; }

        public decimal O_Price { get; }

        public string BuyOrSell { get; }

        public string Condition { get; }

        public decimal C_Price { get; }

        public decimal C_Volume { get; }

        public string Status { get; }

        public string RefId { get; }

        public string Active { get; }

        public string TimeCondition { get; set; }

        public MarketType MarketType { get; }

        public OrderAction OrderAction { get; }

        public ConditionalOrderStatus OrderStatus { get; }

        public string Action { get; set; }

        public Color ActionColor { get; }

        public string ActiveLabelText { get; set; }

        public string ExecutedLabelText { get; set; }

        public ConditionalOrder(
            string orderId,
            string createdTime,
            string executedTime,
            string updateTime,
            string stockCode,
            string marketCode,
            decimal oVolume,
            decimal oPrice,
            string bs,
            string condition,
            decimal cPrice,
            decimal cVolume,
            string status,
            string refId,
            string active,
            string timeCondition
        )
        {
            OrderId = orderId;
            CreatedTime = createdTime;
            ExecutedTime = executedTime;
            UpdateTime = updateTime;
            StockCode = stockCode;
            MarketCode = marketCode;
            O_Volume = oVolume;
            O_Price = oPrice;
            BuyOrSell = bs;
            Condition = condition;
            C_Price = cPrice;
            C_Volume = cVolume;
            Status = status;
            RefId = refId;
            Active = active;
            ActiveLabelText = (active == "0") ? "✕" : "✓";
            TimeCondition = timeCondition;

            switch (bs)
            {
                case "0":
                    OrderAction = OrderAction.Buy;
                    ActionColor = Colors.LoseForeground;
                    break;
                case "1":
                    OrderAction = OrderAction.Sell;
                    ActionColor = Colors.GainForeground;
                    break;
                default:
                    break;
            }

            if (Enum.TryParse(marketCode, out MarketType marketType))
            {
                MarketType = marketType;
            }
            else
            {
                MarketType = MarketType.RG;
            }

            if (Enum.TryParse(status.Replace(" ", ""), out ConditionalOrderStatus conditionalOrderStatus))
            {
                OrderStatus = conditionalOrderStatus;
            }
            else
            {
                OrderStatus = ConditionalOrderStatus.NotExecuted;
            }

            ExecutedLabelText = (OrderStatus == ConditionalOrderStatus.OrderExecuted) ? "✓" : "✕";
            Action = OrderAction.ToString();
            MarketCode = MarketType.ToString();
        }
    }
}
