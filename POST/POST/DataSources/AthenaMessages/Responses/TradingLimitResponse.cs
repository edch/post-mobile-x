﻿namespace POST.DataSources.AthenaMessages.Responses
{

    public class TradingLimitResponse
    {
        public decimal CashT3 { get; }

        public decimal CashT1 { get; }

        public decimal FeeBuy { get; }

        public decimal FeeSell { get; }

        public TradingLimitResponse(
            decimal cashT3,
            decimal cashT1,
            decimal feeBuy,
            decimal feeSell
        )
        {
            CashT3 = cashT3;
            CashT1 = cashT1;
            FeeBuy = feeBuy;
            FeeSell = feeSell;
        }
    }
}
