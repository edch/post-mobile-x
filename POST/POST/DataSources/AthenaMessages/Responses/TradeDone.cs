﻿
using POST.Models;

namespace POST.DataSources.AthenaMessages.Responses
{

    public class TradeDone
    {
        public string TradeId { get; }

        public string StockCode { get; }

        public string TradeTime { get; }

        public decimal Price { get; }

        public decimal Volume { get; }

        public string BuyOrSell { get; }

        public decimal Fee { get; }

        public OrderAction OrderAction { get; }

        public string Action { get; set; }

        public TradeDone(
            string tradeId,
            string stockCode,
            string tradeTime,
            decimal donePrice,
            decimal doneVolume,
            string buyOrSell,
            decimal tradeFee
        )
        {
            TradeId = tradeId;
            StockCode = stockCode;
            TradeTime = tradeTime;
            Price = donePrice;
            Volume = doneVolume;
            BuyOrSell = buyOrSell;
            Fee = tradeFee;

            switch (buyOrSell)
            {
                case "0":
                    OrderAction = OrderAction.Buy;
                    break;
                case "1":
                    OrderAction = OrderAction.Sell;
                    break;
                default:
                    break;
            }

            Action = OrderAction.ToString();
        }
    }
}
