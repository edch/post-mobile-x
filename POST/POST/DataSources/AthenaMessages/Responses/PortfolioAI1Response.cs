﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Responses
{
    public class PortfStock
    {

        [JsonProperty("volume")]
        public decimal volume { get; set; }

        [JsonProperty("avg")]
        public decimal avg { get; set; }

        [JsonProperty("availVolume")]
        public decimal availVolume { get; set; }

        [JsonProperty("gainLoss")]
        public decimal gainLoss { get; set; }

        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("haircutVal")]
        public decimal haircutVal { get; set; }

        [JsonProperty("stockCode")]
        public string stockCode { get; set; }

        [JsonProperty("lastPrice")]
        public decimal lastPrice { get; set; }
    }

    public class ARAP
    {

        [JsonProperty("T1Date")]
        public string T1Date { get; set; }

        [JsonProperty("T3Net")]
        public decimal T3Net { get; set; }

        [JsonProperty("T1AR")]
        public decimal T1AR { get; set; }

        [JsonProperty("T2Net")]
        public decimal T2Net { get; set; }

        [JsonProperty("T3AP")]
        public decimal T3AP { get; set; }

        [JsonProperty("T2AR")]
        public decimal T2AR { get; set; }

        [JsonProperty("T1AP")]
        public decimal T1AP { get; set; }

        [JsonProperty("T0AR")]
        public decimal T0AR { get; set; }

        [JsonProperty("T2AP")]
        public decimal T2AP { get; set; }

        [JsonProperty("T3Date")]
        public string T3Date { get; set; }

        [JsonProperty("T0AP")]
        public decimal T0AP { get; set; }

        [JsonProperty("T0Date")]
        public string T0Date { get; set; }

        [JsonProperty("T2Date")]
        public string T2Date { get; set; }

        [JsonProperty("T2CW")]
        public decimal T2CW { get; set; }

        [JsonProperty("T3CW")]
        public decimal T3CW { get; set; }

        [JsonProperty("T0CW")]
        public decimal T0CW { get; set; }

        [JsonProperty("T3AR")]
        public decimal T3AR { get; set; }

        [JsonProperty("T0Net")]
        public decimal T0Net { get; set; }

        [JsonProperty("T1CW")]
        public decimal T1CW { get; set; }

        [JsonProperty("T1Net")]
        public decimal T1Net { get; set; }
    }

    public class PortfolioAI1Response
    {

        [JsonProperty("isError")]
        public int isError { get; set; }

        [JsonProperty("OLT_TL")]
        public decimal OLT_TL { get; set; }

        [JsonProperty("todayTrx")]
        public decimal todayTrx { get; set; }

        [JsonProperty("CurrentRatio")]
        public decimal CurrentRatio { get; set; }

        [JsonProperty("CurrentCash")]
        public decimal CurrentCash { get; set; }

        [JsonProperty("portfStock")]
        public IList<PortfStock> portfStock { get; set; }

        [JsonProperty("cashT3")]
        public decimal cashT3 { get; set; }

        [JsonProperty("ARAP")]
        public ARAP ARAP { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }
    }
}