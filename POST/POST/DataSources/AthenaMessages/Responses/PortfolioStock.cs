﻿using POST.Constants;
using POST.Models;
using System;
using Xamarin.Forms;

namespace POST.DataSources.AthenaMessages.Responses
{
    public class PortfolioStock
    {
        public int Id { get; }

        public Stock Stock
        {
            get
            {
                Stock stockTmp = null;
                if (StockCode.ToLower().Contains("-r"))
                {
                    Store.StockById.GetState().TryGetValue(new StockId(StockCode, MarketType.TN), out stockTmp);
                }
                else
                {
                    Store.StockById.GetState().TryGetValue(new StockId(StockCode, MarketType.RG), out stockTmp);
                }
                return stockTmp;
            }
        }

        public string StockCode { get; }

        public decimal LastPrice { get; }

        public decimal VolumeLot
        {
            get
            {
                decimal volLot = 0m;
                long sharesPerLot = 100;
                if (Stock != null)
                {
                    sharesPerLot = Stock.SharesPerLot.Value;
                }
                volLot = Math.Floor(Volume / sharesPerLot);
                return volLot;
            }
        }

        public decimal Volume { get; }

        public decimal Average { get; }

        public decimal AvailableVolume { get; }

        public decimal AvailableVolumeLot
        {
            get
            {
                decimal availVolLot = 0m;
                long sharesPerLot = 100;
                if (Stock != null)
                {
                    sharesPerLot = Stock.SharesPerLot.Value;
                }
                availVolLot = Math.Floor(AvailableVolume / sharesPerLot);
                return availVolLot;
            }
        }

        public decimal Gain { get; }

        public decimal Haircut { get; }

        public decimal PreviousPrice => LastPrice - (Gain / Volume);

        public decimal GainPercent
        {
            get
            {
                decimal previous = PreviousPrice;
                if (previous == 0m)
                {
                    return 0m;
                }

                return (LastPrice - previous) / previous * 100m;
            }
        }

        public Color GainForeground
        {
            get
            {
                int comparison = LastPrice.CompareTo(PreviousPrice);
                if (comparison > 0)
                {
                    return Colors.GainForeground;
                }
                else if (comparison < 0)
                {
                    return Colors.LoseForeground;
                }
                else
                {
                    return Colors.UnchangedForeground;
                }
            }
        }

        public Color AvgForeground
        {
            get
            {
                int comparison = Average.CompareTo(LastPrice);
                if (comparison > 0)
                {
                    return Colors.LoseForeground;
                }
                else if (comparison < 0)
                {
                    return Colors.GainForeground;
                }
                else
                {
                    return Colors.UnchangedForeground;
                }
            }
        }

        public Color LastPriceForeground
        {
            get
            {
                if (Stock != null && Stock.Previous.HasValue)
                {
                    decimal PrevPrice = Stock.Previous.Value;
                    int comparison = LastPrice.CompareTo(PrevPrice);
                    if (comparison > 0)
                    {
                        return Colors.GainForeground;
                    }
                    else if (comparison < 0)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }

                return Colors.UnchangedForeground;
            }
        }

        public decimal TotalValue => LastPrice * Volume;

        public PortfolioStock(
            int id,
            string stockCode,
            decimal lastPrice,
            decimal volume,
            decimal average,
            decimal availVolume,
            decimal gainLoss,
            decimal haircut
        )
        {
            Id = id;
            StockCode = stockCode;
            LastPrice = lastPrice;
            Volume = volume;
            Average = average;
            AvailableVolume = availVolume;
            Gain = gainLoss;
            Haircut = haircut;
        }
    }
}
