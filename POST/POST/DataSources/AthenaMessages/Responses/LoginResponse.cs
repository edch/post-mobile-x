﻿using System;

namespace POST.DataSources.AthenaMessages.Responses
{
    public class LoginResponse
    {
        public string Message { get; }

        public bool IsSuccessStatusMessage => Message == "true";

        public bool IsPasswordExpiredMessage => Message == "change";

        public LoginResponse(
            string message
        )
        {
            Message = message ?? throw new ArgumentNullException(nameof(message));
        }
    }
}
