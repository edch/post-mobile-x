﻿using System;

namespace POST.DataSources.AthenaMessages.Responses
{
    public class TradingAccount
    {

        public int Id { get; set; }

        public string BrokerAccountId { get; }

        public string CustomerType { get; }

        public string CustomerName { get; }

        public string Email { get; }

        public TradingAccount(
            int id,
            string brokerAccountId,
            string customerType,
            string customerName,
            string email
        )
        {
            Id = id;
            BrokerAccountId = brokerAccountId ?? throw new ArgumentNullException(nameof(brokerAccountId));
            CustomerType = customerType;
            CustomerName = customerName;
            Email = email;
        }
    }
}
