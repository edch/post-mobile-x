﻿namespace POST.DataSources.AthenaMessages.Responses
{
    public class CashBalance
    {
        public int Id { get; }

        public decimal OltTl { get; }

        public string Name { get; }

        public decimal CurrentCash { get; }

        public decimal CurrentRatio { get; }

        public decimal SumStockValue { get; set; }

        public decimal CashOnT3 { get; set; }

        public decimal TodayTransaction { get; }

        public decimal Points { get; }

        public CashBalance(
            int id,
            decimal oltTl,
            string name,
            decimal currentCash,
            decimal currentRatio,
            decimal sumStockValue,
            decimal cashT3,
            decimal todayTransaction,
            decimal points
        )
        {
            Id = id;
            OltTl = oltTl;
            Name = name;
            CurrentCash = currentCash;
            CurrentRatio = currentRatio;
            SumStockValue = sumStockValue;
            CashOnT3 = cashT3;
            TodayTransaction = todayTransaction;
            Points = points;
        }
    }
}
