﻿
using POST.Models;
using System;
using System.Globalization;

namespace POST.DataSources.AthenaMessages.Responses
{

    public class Trade
    {
        public string TradeId { get; }

        public string StockCode { get; }

        public string TradeTime { get; }

        public DateTime TradeTime_Compare { get; }

        public decimal Price { get; }

        public decimal Volume { get; }

        public string BuyOrSell { get; }

        public decimal Fee { get; }

        public OrderAction OrderAction { get; }

        public string Action { get; set; }

        public Trade(
            string tradeId,
            string stockCode,
            string tradeTime,
            decimal donePrice,
            decimal doneVolume,
            string buyOrSell,
            decimal tradeFee
        )
        {
            TradeId = tradeId;
            StockCode = stockCode;
            TradeTime = tradeTime;
            if (DateTime.TryParseExact(tradeTime, "dd MMM yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime result))
            {
                TradeTime_Compare = result;
            }
            Price = donePrice;
            Volume = doneVolume;
            BuyOrSell = buyOrSell;
            Fee = tradeFee;

            switch (buyOrSell)
            {
                case "0":
                    OrderAction = OrderAction.Buy;
                    break;
                case "1":
                    OrderAction = OrderAction.Sell;
                    break;
                default:
                    break;
            }

            Action = OrderAction.ToString();
        }
    }
}
