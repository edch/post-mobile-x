﻿namespace POST.DataSources.AthenaMessages.Responses
{
    public class BankAccount
    {
        public string BankAccountNumber { get; set; }

        public string BankAccountHolderName { get; set; }

        public string BankName { get; set; }

        public string RdnAccountNumber { get; set; }

        public string RdnAccountHolderName { get; set; }

        public string RdnName { get; set; }

        public BankAccount()
        {

        }

        public BankAccount(
            string bankAccountNumber,
            string bankAccountHolderName,
            string bankName,
            string rdnAccountNumber,
            string rdnAccountHolderName,
            string rdnName
        )
        {
            BankAccountNumber = bankAccountNumber;
            BankAccountHolderName = bankAccountHolderName;
            BankName = bankName;
            RdnAccountNumber = rdnAccountNumber;
            RdnAccountHolderName = rdnAccountHolderName;
            RdnName = rdnName;
        }
    }
}
