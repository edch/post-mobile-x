﻿using System;

namespace POST.DataSources.AthenaMessages.Responses
{
    public class PinLoginResponse
    {
        public string Message { get; }

        public bool IsSuccessStatusMessage => Message == "true";

        public PinLoginResponse(
            string message
        )
        {
            Message = message ?? throw new ArgumentNullException(nameof(message));
        }
    }
}
