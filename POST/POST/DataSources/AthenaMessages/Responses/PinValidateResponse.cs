﻿using System;

namespace POST.DataSources.AthenaMessages.Responses
{
    public class PinValidateResponse
    {
        public string Message { get; }

        public bool IsSuccessStatusMessage => Message == "true";

        public PinValidateResponse(
            string message
        )
        {
            Message = message ?? throw new ArgumentNullException(nameof(message));
        }
    }
}
