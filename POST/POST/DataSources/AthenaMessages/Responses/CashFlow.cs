﻿
namespace POST.DataSources.AthenaMessages.Responses
{
    public class CashFlow
    {
        public string Date { get; }

        public decimal Amount { get; }

        public int Due { get; }

        public CashFlow(
            string date,
            decimal amount,
            int due
        )
        {
            Date = date;
            Amount = amount;
            Due = due;
        }
    }
}
