﻿
using POST.Constants;
using POST.Models;
using System;
using Xamarin.Forms;

namespace POST.DataSources.AthenaMessages.Responses
{
    public class Order
    {
        public string OrderId { get; }

        public string JsxId { get; }

        public string Placed { get; }

        public string PlacedBy { get; }

        public string StockCode { get; }

        public MarketType MarketType { get; }

        public string MarketCode { get; }

        public decimal Price { get; }

        public decimal Volume { get; }

        public decimal RemainingVolume { get; }

        public decimal TradedVolume { get; }

        public OrderStatus OrderStatus { get; }

        public string OrderStatusCode { get; }

        public string BuyOrSell { get; }

        public int ExpireFlag { get; }

        public OrderExpiry OrderExpiry { get; }

        public string RejectNote { get; }

        public OrderAction OrderAction { get; }

        public string Action { get; set; }

        public Color ActionForeground { get; }

        public Color OrderStatusForeground { get; }


        public Order(
            string orderId,
            string jsxId,
            string oTime,
            string inputUser,
            string stockCode,
            string marketCode,
            decimal price,
            decimal oVolume,
            decimal rVolume,
            decimal tradedVolume,
            string orderStatusCode,
            string bs,
            int expireFlag,
            string rejectNote
        )
        {
            OrderId = orderId;
            JsxId = jsxId;
            Placed = oTime;
            PlacedBy = inputUser;
            StockCode = stockCode;
            MarketCode = marketCode;
            Price = price;
            Volume = oVolume;
            RemainingVolume = rVolume;
            TradedVolume = tradedVolume;
            BuyOrSell = bs;
            ExpireFlag = expireFlag;
            if (rejectNote != "null")
            {
                RejectNote = rejectNote;
            }
            else
            {
                RejectNote = "-";
            }

            switch (bs)
            {
                case "0":
                    OrderAction = OrderAction.Buy;
                    ActionForeground = Colors.LoseForeground;
                    break;
                case "1":
                    OrderAction = OrderAction.Sell;
                    ActionForeground = Colors.GainForeground;
                    break;
                default:
                    ActionForeground = Colors.UnchangedForeground;
                    break;
            }

            if (Enum.TryParse(marketCode, out MarketType marketType))
            {
                MarketType = marketType;
            }
            else
            {
                throw new ArgumentException($"{marketCode} is not a valid MarketType value.");
            }

            OrderExpiry = (OrderExpiry)expireFlag;

            switch (orderStatusCode)
            {
                case "0":
                    OrderStatus = OrderStatus.Rejected;
                    OrderStatusForeground = Colors.RejectedOrderStatus;
                    break;
                case "1":
                    OrderStatus = OrderStatus.Open;
                    OrderStatusForeground = Colors.OpenOrderStatus;
                    break;
                case "2":
                    OrderStatus = OrderStatus.Matched;
                    OrderStatusForeground = Colors.MatchedOrderStatus;
                    break;
                case "3":
                    OrderStatus = OrderStatus.Amending;
                    OrderStatusForeground = Colors.UnchangedForeground;
                    break;
                case "5":
                    OrderStatus = OrderStatus.Canceled;
                    OrderStatusForeground = Colors.CancelledOrderStatus;
                    break;
                default:
                    OrderStatusForeground = Colors.UnchangedForeground;
                    break;
            }

            Action = OrderAction.ToString();
            MarketCode = MarketType.ToString();
            OrderStatusCode = OrderStatus.ToString();
        }
    }
}
