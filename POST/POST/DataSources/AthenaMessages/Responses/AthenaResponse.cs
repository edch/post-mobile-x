﻿
namespace POST.DataSources.AthenaMessages.Responses
{
    public class AthenaResponse
    {
        public string Message { get; }

        protected internal AthenaResponse(
            string message = null
        )
        {
            Message = message;
        }
    }
}
