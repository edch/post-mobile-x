﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class HistoryTradeRequest : Dictionary<string, string>
    {
        public HistoryTradeRequest(
            string brokerAccountId
        )
        {
            Add("request", "viewHistoryTrade");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
