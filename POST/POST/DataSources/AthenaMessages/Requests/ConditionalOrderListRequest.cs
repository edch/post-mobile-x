﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class ConditionalOrderListRequest : Dictionary<string, string>
    {
        public ConditionalOrderListRequest(
            string brokerAccountId
        )
        {
            Add("request", "viewOrderConditional");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
