﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class PinChangeRequest : Dictionary<string, string>
    {
        public PinChangeRequest(
            string brokerAccount,
            string oldPin,
            string newPin
        )
        {
            Add("request", "changePin");
            Add("brokerAccount", brokerAccount);
            Add("oldPin", oldPin);
            Add("newPin", newPin);
        }
    }
}
