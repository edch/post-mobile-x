﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    internal class PortfolioRequest : Dictionary<string, string>
    {
        public PortfolioRequest(
            string brokerAccountId
        )
        {
            Add("request", "viewPortfolio");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
