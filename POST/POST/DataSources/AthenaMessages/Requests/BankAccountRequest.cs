﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class BankAccountRequest : Dictionary<string, string>
    {
        public BankAccountRequest(
            string brokerAccountId
        )
        {
            Add("request", "viewBankAccount");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
