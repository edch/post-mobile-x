﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class LogoutRequest : Dictionary<string, string>
    {
        public LogoutRequest(
        )
        {
            Add("request", "logout");
        }
    }
}
