﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    internal class PortfolioBalanceRequest : Dictionary<string, string>
    {
        public PortfolioBalanceRequest(
            string brokerAccountId
        )
        {
            Add("request", "viewPortfolioBalance");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
