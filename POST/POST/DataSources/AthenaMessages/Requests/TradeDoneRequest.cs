﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class TradeDoneRequest : Dictionary<string, string>
    {
        public TradeDoneRequest(
            string brokerAccountId
        )
        {
            Add("request", "viewTradeDone");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
