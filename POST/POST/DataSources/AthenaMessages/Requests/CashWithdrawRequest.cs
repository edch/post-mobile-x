﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class CashWithdrawRequest : Dictionary<string, string>
    {
        public CashWithdrawRequest(
            string brokerAccountId,
            string pin,
            string date,
            decimal amount,
            string bankAccount,
            string rdn,
            string amountWords,
            string bankAccountName,
            string refNo
        )
        {
            Add("request", "withdrawRequest");
            Add("brokerAccountId", brokerAccountId);
            Add("pin", pin);
            Add("date", date);
            Add("amount", amount.ToString());
            Add("bankAccount", bankAccount);
            Add("rdn", rdn);
            Add("amountWords", amountWords);
            Add("bankAccountName", bankAccountName);
            Add("refNo", refNo);
        }
    }
}
