﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class TradingLimitRequest : Dictionary<string, string>
    {
        public TradingLimitRequest(
            string brokerAccountId,
            string stockCode
        )
        {
            Add("request", "viewTransactionLimit");
            Add("brokerAccountId", brokerAccountId);
            if (stockCode != null)
            {
                Add("stockCode", stockCode);
            }
        }
    }
}
