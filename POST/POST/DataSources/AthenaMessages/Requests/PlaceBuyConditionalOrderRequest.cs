﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class PlaceBuyConditionalOrderRequest : Dictionary<string, string>
    {
        public string Command => "0";

        public PlaceBuyConditionalOrderRequest(
            string brokerAccountId,
            string stockCode,
            string boardCode,
            string price,
            string volume,
            string condition,
            string priceCondition,
            string volCondition,
            string timeCondition,
            string expDate
        )
        {
            Add("request", "orderConditional");
            Add("brokerAccountId", brokerAccountId);
            Add("stockCode", stockCode);
            Add("boardCode", boardCode);
            Add("volume", volume);
            Add("price", price);
            Add("BS", Command);
            Add("condition", condition);
            Add("priceCondition", priceCondition);
            Add("volCondition", volCondition);
            Add("timeCondition", timeCondition);
            Add("status", "Not Executed");
            Add("active", "1");
            Add("expDate", expDate);


        }
    }
}
