﻿using POST.Stores;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;


namespace POST.DataSources.AthenaMessages.Requests
{
    public class LoginRequest : Dictionary<string, string>
    {
        public string HashedPassword { get; }

        public string Password { get; }

        public LoginRequest(
            string userId,
            string password,
            string version,
            string hashedPassword = null
        )
        {
            Add("request", "login");
            Add("user", userId);
            Add("password", password);
            Add("sourceId", AppSettings.SourceId);
            Add("ver", version);

            if (hashedPassword != null)
            {
                HashedPassword = hashedPassword;
                Password = password;
            }
            else
            {
                Password = password ?? throw new ArgumentNullException(nameof(password));
                using (MD5 md5 = MD5.Create())
                {
                    byte[] hash = md5.ComputeHash(Encoding.UTF8.GetBytes(password));
                    StringBuilder sb = new StringBuilder();
                    foreach (byte b in hash)
                    {
                        sb.Append(b.ToString("X2"));
                    }
                    HashedPassword = sb.ToString();
                }
            }
        }
    }
}
