﻿using POST.Stores;
using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class PlaceWithdrawOrderRequest : Dictionary<string, string>
    {
        public PlaceWithdrawOrderRequest(
            string brokerAccountId,
            string stockCode,
            string boardCode,
            decimal price,
            string orderId,
            string jsxId,
            string command
        )
        {
            Add("request", "orderWithdraw");
            Add("brokerAccountId", brokerAccountId);
            Add("source", AppSettings.SourceId);
            Add("command", command);
            Add("price", price.ToString());
            Add("stockCode", stockCode);
            Add("boardCode", boardCode);
            Add("orderId", orderId);
            Add("jsxId", jsxId);
        }
    }
}
