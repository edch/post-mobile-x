﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class PlaceActivateConditionalOrderRequest : Dictionary<string, string>
    {
        public PlaceActivateConditionalOrderRequest(
            string brokerAccountId,
            string orderId
        )
        {
            Add("request", "setActiveConditional");
            Add("brokerAccountId", brokerAccountId);
            Add("id", orderId);
            Add("active", "1");
        }
    }
}
