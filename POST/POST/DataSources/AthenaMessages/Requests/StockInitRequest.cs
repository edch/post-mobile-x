﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    internal class StockInitRequest : Dictionary<string, string>
    {

        public StockInitRequest()
        {
            Add("request", "stockInit");
        }
    }
}
