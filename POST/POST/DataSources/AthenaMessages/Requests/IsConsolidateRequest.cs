﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class IsConsolidateRequest : Dictionary<string, string>
    {
        public IsConsolidateRequest()
        {
            Add("request", "isConsolidate");
        }
    }
}
