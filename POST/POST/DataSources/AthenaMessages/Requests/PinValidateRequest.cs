﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    internal class PinValidateRequest : Dictionary<string, string>
    {
        public PinValidateRequest(
             string brokerAccountId
          )
        {
            Add("request", "isPinValid");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
