﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    internal class PayableReceivableRequest : Dictionary<string, string>
    {
        public PayableReceivableRequest(
            string brokerAccountId
        )
        {
            Add("request", "viewPayableReceivable");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
