﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class PlaceDeactivateConditionalOrderRequest : Dictionary<string, string>
    {
        public PlaceDeactivateConditionalOrderRequest(
            string brokerAccountId,
            string orderId
        )
        {
            Add("request", "setActiveConditional");
            Add("brokerAccountId", brokerAccountId);
            Add("id", orderId);
            Add("active", "0");
        }
    }
}
