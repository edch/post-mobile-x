﻿using POST.Stores;
using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class PlaceAmendOrderRequest : Dictionary<string, string>
    {
        public PlaceAmendOrderRequest(
            string brokerAccountId,
            string stockCode,
            string boardCode,
            decimal price,
            decimal volume,
            int expiry,
            string orderId,
            string jsxId,
            string command
        )
        {
            Add("request", "orderAmend");
            Add("brokerAccountId", brokerAccountId);
            Add("source", AppSettings.SourceId);
            Add("command", command);
            Add("expiry", expiry.ToString());
            Add("price", price.ToString());
            Add("volume", volume.ToString());
            Add("stockCode", stockCode);
            Add("boardCode", boardCode);
            Add("orderId", orderId);
            Add("jsxId", jsxId);
        }
    }
}
