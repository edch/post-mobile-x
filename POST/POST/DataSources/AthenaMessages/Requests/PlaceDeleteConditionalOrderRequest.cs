﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class PlaceDeleteConditionalOrderRequest : Dictionary<string, string>
    {
        public PlaceDeleteConditionalOrderRequest(
            string brokerAccountId,
            string orderId
        )
        {
            Add("request", "deleteConditional");
            Add("brokerAccountId", brokerAccountId);
            Add("id", orderId);
        }
    }
}
