﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class TradingAccountsRequest : Dictionary<string, string>
    {

        public TradingAccountsRequest()
        {
            Add("request", "brokerAccountId");
        }
    }
}
