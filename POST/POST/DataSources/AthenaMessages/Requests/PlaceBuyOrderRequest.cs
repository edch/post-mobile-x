﻿using POST.Stores;
using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class PlaceBuyOrderRequest : Dictionary<string, string>
    {
        public string Command => "0";

        public PlaceBuyOrderRequest(
            string brokerAccountId,
            string stockCode,
            string boardCode,
            decimal price,
            decimal volume,
            int expiry
        )
        {
            Add("request", "orderTrade");
            Add("brokerAccountId", brokerAccountId);
            Add("source", AppSettings.SourceId);
            Add("command", Command);
            Add("expiry", expiry.ToString());
            Add("price", price.ToString());
            Add("volume", volume.ToString());
            Add("stockCode", stockCode);
            Add("boardCode", boardCode);
        }
    }
}
