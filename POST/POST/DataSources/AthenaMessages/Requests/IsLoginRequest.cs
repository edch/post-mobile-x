﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class IsLoginRequest : Dictionary<string, string>
    {
        public IsLoginRequest()
        {
            Add("request", "isLogin");
        }
    }
}
