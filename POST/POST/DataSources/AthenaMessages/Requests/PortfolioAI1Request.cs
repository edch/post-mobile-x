﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    internal class PortfolioAI1Request : Dictionary<string, string>
    {
        public PortfolioAI1Request(
            string brokerAccountId,
            string user
        )
        {
            Add("request", "viewPortfolioAI1");
            Add("brokerAccountId", brokerAccountId);
            Add("user", user);
        }
    }
}