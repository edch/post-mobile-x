﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class PasswordChangeRequest : Dictionary<string, string>
    {
        public PasswordChangeRequest(
            string oldPwd,
            string newPwd
        )
        {
            Add("request", "changePwd");
            Add("oldPwd", oldPwd);
            Add("newPwd", newPwd);
        }
    }
}
