﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class PinLoginRequest : Dictionary<string, string>
    {
        public PinLoginRequest(
            string brokerAccountId,
            string Pin
         )
        {
            Add("request", "storePin");
            Add("brokerAccountId", brokerAccountId);
            Add("pin", Pin);
        }
    }
}
