﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class OrderListRequest : Dictionary<string, string>
    {
        public OrderListRequest(
            string brokerAccountId
        )
        {
            Add("request", "viewOrder");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
