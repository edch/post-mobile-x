﻿using System.Collections.Generic;

namespace POST.DataSources.AthenaMessages.Requests
{
    public class CashFlowRequest : Dictionary<string, string>
    {
        public CashFlowRequest(
            string brokerAccountId
        )
        {
            Add("request", "viewCashFlow");
            Add("brokerAccountId", brokerAccountId);
        }
    }
}
