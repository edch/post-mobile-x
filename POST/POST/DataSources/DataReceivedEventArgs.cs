﻿using POST.DataSources.DataFeedMessages;
using System;

namespace POST.DataSources
{

    public class DataReceivedEventArgs
    {

        public DataFeedMessageBase Message { get; }

        public DataReceivedEventArgs(DataFeedMessageBase message)
        {
            Message = message ?? throw new ArgumentNullException(nameof(message));
        }
    }
}
