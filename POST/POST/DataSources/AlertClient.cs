﻿using Newtonsoft.Json;
using POST.DataSources.AlertMessages.Requests;
using POST.DataSources.AlertMessages.Responses;
using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class AlertClient : IDisposable
    {
        //public const string URL = "http://172.31.2.92:8081/p4";
        //public const string URL = "http://202.78.206.115:8081/p4";
        //public const string URL = "http://103.86.161.222:8081/p4";
        public const string URL = "https://uber.post-pro.co.id:8443/p4x";

        private static readonly Lazy<AlertClient> _singletonFactory = new Lazy<AlertClient>(() => new AlertClient());
        public static AlertClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        public AlertClient()
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            _httpClient = new HttpClient(clientHandler);
        }

        public async Task<string> RegisterDeviceAsync(string userid, string firebaseid, string bundleid, CancellationToken cancellationToken)
        {
            RegDeviceRequest regDeviceRequest = new RegDeviceRequest(userid, firebaseid, bundleid);

            string urlRegDevice = URL + "/regdevice/";

            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Post, urlRegDevice))
            {
                httpRequest.Content = new StringContent(regDeviceRequest.ToContent,
                                    Encoding.Default);

                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string result = await httpResponse.Content.ReadAsStringAsync();
                    return result;
                }
            }

        }

        public async Task<AlertResponse> GetNoticeListAsync(string userid, CancellationToken cancellationToken)
        {
            NoticeListRequest noticeListRequest = new NoticeListRequest(userid);

            string urlNoticeList = URL + "/noticelist/";

            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Post, urlNoticeList))
            {
                httpRequest.Content = new StringContent(noticeListRequest.ToContent);
                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string result = await httpResponse.Content.ReadAsStringAsync();
                    AlertResponse alertResponse = JsonConvert.DeserializeObject<AlertResponse>(result);
                    return alertResponse;
                }
            }
        }

        public async Task<string> AddNoticeAsync(string userid, string stockid, string noticetype, string price, CancellationToken cancellationToken)
        {
            AddNoticeRequest addNoticeRequest = new AddNoticeRequest(userid, stockid, noticetype, price);

            string urlAddNotice = URL + "/addnotice/";

            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Post, urlAddNotice))
            {
                httpRequest.Content = new StringContent(addNoticeRequest.ToContent);
                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string result = await httpResponse.Content.ReadAsStringAsync();
                    return result;
                }
            }
        }

        public async Task<string> DeleteNoticeAsync(string userid, string noticeid, CancellationToken cancellationToken)
        {
            DeleteNoticeRequest deleteNoticeRequest = new DeleteNoticeRequest(userid, noticeid);

            string urlDeleteNotice = URL + "/deletenotice/";

            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Post, urlDeleteNotice))
            {
                httpRequest.Content = new StringContent(deleteNoticeRequest.ToContent);
                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string result = await httpResponse.Content.ReadAsStringAsync();
                    return result;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

    }
}
