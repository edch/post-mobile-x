﻿using Newtonsoft.Json;
using POST.DataSources.RemarksMessages.Requests;
using POST.DataSources.RemarksMessages.Responses;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class RemarksClient : IDisposable
    {
        //public const string URL = "http://172.31.2.92:3231/stocknote/";
        //public const string URL = "http://202.78.206.115:8081/p4/stocknote/";
        public const string URL = "http://103.86.161.222:8081/p4/stocknote/";

        private static readonly Lazy<RemarksClient> _singletonFactory = new Lazy<RemarksClient>(() => new RemarksClient());
        public static RemarksClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        public RemarksClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<RemarksResponse> GetRemarksAsync(string stockId, CancellationToken cancellationToken)
        {
            RemarksRequest remarksRequest = new RemarksRequest(stockId);

            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Post, URL))
            {
                httpRequest.Content = new StringContent(remarksRequest.ToContent);
                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string result = await httpResponse.Content.ReadAsStringAsync();
                    RemarksResponse remarksResponse = JsonConvert.DeserializeObject<RemarksResponse>(result);
                    return remarksResponse;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

    }
}
