﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace POST.DataSources.RemarksMessages.Requests
{
    public class RemarksRequest : Dictionary<string, string>
    {
        public string ToContent { get; set; }

        public RemarksRequest(
            string stockId
        )
        {
            Add("stockId", stockId);

            ToContent = JsonConvert.SerializeObject(this);
        }
    }
}
