﻿using Newtonsoft.Json;
using POST.Models;
using System.Collections.Generic;

namespace POST.DataSources.RemarksMessages.Responses
{
    public class Data
    {

        [JsonProperty("aclBoard")]
        public bool aclBoard { get; set; }

        [JsonProperty("shortsell")]
        public bool shortsell { get; set; }

        [JsonProperty("mainBoard")]
        public bool mainBoard { get; set; }

        [JsonProperty("notes")]
        public IList<object> notes { get; set; }

        [JsonProperty("sectype")]
        public string sectype { get; set; }

        [JsonProperty("marginable")]
        public bool marginable { get; set; }

        [JsonProperty("devBoard")]
        public bool devBoard { get; set; }

        [JsonProperty("isPreOp")]
        public bool isPreOp { get; set; }

        [JsonProperty("corpactions")]
        public IList<object> corpactions { get; set; }

        [JsonProperty("stockId")]
        public string stockId { get; set; }

        [JsonProperty("indexGroup")]
        public IList<object> indexGroup { get; set; }

        [JsonProperty("indexV1")]
        public IList<string> indexV1 { get; set; }

        [JsonProperty("remark1")]
        public string remark1 { get; set; }

        [JsonProperty("sector")]
        public string sector { get; set; }

        [JsonProperty("remark2")]
        public string remark2 { get; set; }

        public StockType StockType
        {
            get
            {
                if (sectype.Contains("ORDI"))
                {
                    return StockType.ORDI;
                }
                else if (sectype.Contains("WARI"))
                {
                    return StockType.WARRANT;
                }
                else if (sectype.Contains("RGHI"))
                {
                    return StockType.RIGHTS;
                }
                else if (sectype.Contains("MUTI"))
                {
                    return StockType.ETF;
                }
                else
                {
                    return StockType.ORDI;
                }
            }
        }
    }

    public class RemarksResponse
    {

        [JsonProperty("msg")]
        public string msg { get; set; }

        [JsonProperty("data")]
        public Data data { get; set; }

        [JsonProperty("ecode")]
        public int ecode { get; set; }
    }
}
