﻿using Newtonsoft.Json;
using POST.DataSources.ApiResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class SparkLineClient : IDisposable
    {

        public const string URL = "http://202.78.206.115:8080/chart/sparkline";

        private static readonly Lazy<SparkLineClient> _singletonFactory = new Lazy<SparkLineClient>(() => new SparkLineClient());
        public static SparkLineClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        public SparkLineClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<IReadOnlyDictionary<string, SparkLineData>> GetSparkLinesAsync(ISet<string> stockCodes, CancellationToken cancellationToken)
        {
            Dictionary<string, SparkLineData> sparkLineModelByStockCode = new Dictionary<string, SparkLineData>();
            Queue<string> stockCodeQueue = new Queue<string>(stockCodes);
            while (stockCodeQueue.Any())
            {
                List<string> stockCodeBatch = new List<string>();
                while (stockCodeBatch.Count < 10 && stockCodeQueue.TryDequeue(out string stockCode))
                {
                    stockCodeBatch.Add(stockCode);
                }
                string requestUrl = $"{URL}?code={string.Join(",", stockCodeBatch)}";
                using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, requestUrl))
                {
                    try
                    {
                        using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                        {
                            string json = await httpResponse.Content.ReadAsStringAsync();
                            SparkLineData[] sparkLineModels = JsonConvert.DeserializeObject<SparkLineData[]>(json);
                            foreach (SparkLineData sparkLineModel in sparkLineModels)
                            {
                                try
                                {
                                    sparkLineModelByStockCode.Add(sparkLineModel.StockCode, sparkLineModel);
                                }
                                catch (ArgumentException)
                                {
                                    // Exception handling policy: skip if item is already added
                                    continue;
                                }
                            }
                        }
                    }
                    catch (OperationCanceledException)
                    {
                        throw;
                    }
                    catch (ObjectDisposedException)
                    {
                        throw new OperationCanceledException();
                    }
                    catch
                    {
                        // Exception handling policy: skip failed items
                        continue;
                    }
                }
            }
            return sparkLineModelByStockCode;
        }

        public async Task<SparkLineData> GetSparkLineAsync(string stockCode, CancellationToken cancellationToken)
        {
            IReadOnlyDictionary<string, SparkLineData> sparkLineModelByStockCode = await GetSparkLinesAsync(new HashSet<string> { stockCode }, cancellationToken);
            if (sparkLineModelByStockCode.TryGetValue(stockCode, out SparkLineData sparkLineModel))
            {
                return sparkLineModel;
            }
            else
            {
                return null;
            }
        }

        private class RequestPool
        {
            private readonly SparkLineClient _client;
            private readonly ISet<string> _stockCodes;
            private readonly CancellationTokenSource _commencementTrigger;
            private readonly TaskCompletionSource<ISet<string>> _commencementSource;
            private TaskCompletionSource<IReadOnlyDictionary<string, SparkLineData>> _completionSource;
            public bool Commenced { get; private set; }

            public RequestPool(SparkLineClient client, string stockCode)
            {
                _client = client ?? throw new ArgumentNullException(nameof(client));
                _stockCodes = new HashSet<string> { stockCode ?? throw new ArgumentNullException(nameof(stockCode)) };
                _commencementSource = new TaskCompletionSource<ISet<string>>();
                Commenced = false;

                _commencementTrigger = new CancellationTokenSource(TimeSpan.FromMilliseconds(500));
                _commencementTrigger.Token.Register(Commence);
            }

            public async Task<SparkLineData> GetSparkLineAsync(string stockCode, CancellationToken cancellationToken)
            {
                lock (this)
                {
                    if (Commenced)
                    {
                        throw new InvalidOperationException("This RequestPool is already locked.");
                    }
                }
                if (_stockCodes.Add(stockCode) &&
                    _stockCodes.Count >= 10)
                {
                    Commence();
                }
                ISet<string> stockCodes = await _commencementSource.Task;
                if (_completionSource == null)
                {
                    _completionSource = new TaskCompletionSource<IReadOnlyDictionary<string, SparkLineData>>();
                    try
                    {
                        _completionSource.SetResult(await _client.GetSparkLinesAsync(stockCodes, cancellationToken));
                    }
                    catch (Exception exc)
                    {
                        _completionSource.SetException(exc);
                    }
                }
                IReadOnlyDictionary<string, SparkLineData> sparkLineModelByStockCode = await _completionSource.Task;
                if (sparkLineModelByStockCode.TryGetValue(stockCode, out SparkLineData sparkLineModel))
                {
                    return sparkLineModel;
                }
                else
                {
                    return null;
                }
            }

            private void Commence()
            {
                lock (this)
                {
                    if (!Commenced)
                    {
                        Commenced = true;
                        _client._requestQueue.Remove(this);
                        _commencementTrigger.Dispose();
                        _commencementSource.SetResult(_stockCodes);
                    }
                }
            }
        }

        private readonly List<RequestPool> _requestQueue = new List<RequestPool>();

        public async Task<SparkLineData> GetSparkLinePooledAsync(string stockCode, CancellationToken cancellationToken)
        {
            if (_requestQueue.Count == 0 ||
                _requestQueue.Last().Commenced)
            {
                RequestPool pool = new RequestPool(this, stockCode);
                _requestQueue.Add(pool);
                return await pool.GetSparkLineAsync(stockCode, cancellationToken);
            }
            else
            {
                RequestPool pool = _requestQueue.Last();
                return await pool.GetSparkLineAsync(stockCode, cancellationToken);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
