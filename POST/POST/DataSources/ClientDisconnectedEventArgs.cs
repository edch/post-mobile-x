﻿using System.Net.Sockets;

namespace POST.DataSources
{
    public class ClientDisconnectedEventArgs
    {
        public SocketError SocketError { get; }
        public ClientDisconnectedEventArgs(SocketError socketError)
        {
            SocketError = socketError;
        }
    }
}
