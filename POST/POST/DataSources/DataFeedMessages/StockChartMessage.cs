﻿using System;

namespace POST.DataSources.DataFeedMessages
{

    public class StockChartMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.StockChart;

        public string StockCode { get; }
        public DateTime Time { get; }
        public decimal Open { get; }
        public decimal High { get; }
        public decimal Low { get; }
        public decimal Close { get; }
        public decimal Volume { get; }

        public StockChartMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            StockCode = Parser.ReadPOSTString();
            Time = Parser.ReadDateTime();
            Open = Parser.ReadInt32();
            High = Parser.ReadInt32();
            Low = Parser.ReadInt32();
            Close = Parser.ReadInt32();
            Volume = Parser.ReadInt64();

            Parser = null;
        }

    }
}
