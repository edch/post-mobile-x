﻿namespace POST.DataSources.DataFeedMessages
{

    public class IndexChartEndMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.IndexChartEnd;

        public string IndexCode { get; }

        public IndexChartEndMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            IndexCode = Parser.ReadPOSTString();

            Parser = null;
        }

    }
}
