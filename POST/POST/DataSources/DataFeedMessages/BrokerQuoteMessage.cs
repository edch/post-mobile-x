﻿using POST.Models;
using System;

namespace POST.DataSources.DataFeedMessages
{

    public class BrokerQuoteMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.BrokerQuote;

        public string BrokerCode { get; }
        public BrokerQuoteMarketType MarketType { get; }
        public decimal BuyFreq { get; }
        public decimal BuyVol { get; }
        public decimal BuyVal { get; }
        public decimal SellFreq { get; }
        public decimal SellVol { get; }
        public decimal SellVal { get; }

        public BrokerQuoteMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            BrokerCode = Parser.ReadPOSTString();
            string marketTypeString = Parser.ReadPOSTString();
            if (Enum.TryParse(marketTypeString, out BrokerQuoteMarketType marketType))
            {
                MarketType = marketType;
            }
            else
            {
                throw new ArgumentException($"{marketTypeString} is not a valid MarketType value.");
            }
            BuyFreq = Parser.ReadInt32();
            BuyVol = Parser.ReadInt64();
            BuyVal = Parser.ReadInt64();
            SellFreq = Parser.ReadInt32();
            SellVol = Parser.ReadInt64();
            SellVal = Parser.ReadInt64();

            Parser = null;
        }
    }
}
