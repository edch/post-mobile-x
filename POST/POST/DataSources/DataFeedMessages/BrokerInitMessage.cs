﻿using POST.Models;
using System;

namespace POST.DataSources.DataFeedMessages
{
    public class BrokerInitMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.Broker;

        public string BrokerCode { get; }
        public BrokerQuoteMarketType MarketType { get; set; }
        public string BrokerName { get; }
        public int BrokerType { get; }

        public BrokerInitMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            BrokerCode = Parser.ReadPOSTString();
            string marketTypeString = Parser.ReadPOSTString();

            if (Enum.TryParse(marketTypeString, out BrokerQuoteMarketType marketType))
            {
                MarketType = marketType;
            }
            else
            {
                throw new ArgumentException($"{marketTypeString} is not a valid MarketType value.");
            }
            BrokerName = Parser.ReadPOSTString();
            BrokerType = Parser.ReadInt32();

            Parser = null;
        }

        public BrokerInitMessage(string BrokerCode, string marketTypeString, string BrokerName, int BrokerType)
        {
            this.BrokerCode = BrokerCode;
            if (Enum.TryParse(marketTypeString, out BrokerQuoteMarketType marketType))
            {
                MarketType = marketType;
            }
            else
            {
                throw new ArgumentException($"{marketTypeString} is not a valid MarketType value.");
            }
            this.BrokerName = BrokerName;
            this.BrokerType = BrokerType;
        }
    }
}
