﻿using Newtonsoft.Json;
using POST.Models;
using System;

namespace POST.DataSources.DataFeedMessages
{
    public class StockInitMessage2
    {

        [JsonProperty("sectorId")]
        public int sectorId { get; set; }

        [JsonProperty("code")]
        public string code { get; set; }

        [JsonProperty("isLQ45")]
        public bool isLQ45 { get; set; }

        [JsonProperty("sharesPerLot")]
        public long sharesPerLot { get; set; }

        [JsonProperty("prev")]
        public decimal prev { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("remarks2")]
        public string remarks2 { get; set; }

        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("isSyariah")]
        public bool isSyariah { get; set; }

        [JsonProperty("board")]
        public string board { get; set; }

        [JsonProperty("totalShares")]
        public long totalShares { get; set; }

        [JsonIgnore]
        public MarketType MarketType
        {
            get
            {
                if (Enum.TryParse(board, out MarketType marketType))
                {
                    return marketType;
                }
                else
                {
                    return MarketType.RG;
                }
            }
        }

        [JsonIgnore]
        public decimal? PrevPrice
        {
            get
            {
                if (prev == 0)
                {
                    return null;
                }
                else
                {
                    return prev;
                }
            }
        }


    }
}
