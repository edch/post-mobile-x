﻿using POST.Models;
using System;

namespace POST.DataSources.DataFeedMessages
{

    public class OrderBookMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.OrderBook;

        public string StockCode { get; set; }
        public MarketType MarketType { get; set; }
        public OrderType OrderBookType { get; set; }
        public int SequenceNo { get; set; }
        public decimal Price { get; set; }
        public decimal Volume { get; set; }
        public decimal Frequency { get; set; }
        public string BrokerCode { get; set; }

        public OrderBookMessage()
        {
        }

        public OrderBookMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            StockCode = Parser.ReadPOSTString();
            string marketTypeString = Parser.ReadPOSTString();
            if (Enum.TryParse(marketTypeString, out MarketType marketType))
            {
                MarketType = marketType;
            }
            else
            {
                throw new ArgumentException($"{marketTypeString} is not a valid MarketType value.");
            }
            string orderBookType = Parser.ReadPOSTString();
            SequenceNo = Parser.ReadInt32();
            Price = Parser.ReadInt32();
            Volume = Parser.ReadInt64();
            Frequency = Parser.ReadInt32();
            switch (orderBookType)
            {
                case "OB":
                    OrderBookType = OrderType.Bid;
                    break;
                case "OO":
                    OrderBookType = OrderType.Offer;
                    break;
                case "":
                    OrderBookType = OrderType.None;
                    break;
                default:
                    throw new NotImplementedException();
            }

            Parser = null;
        }
    }
}
