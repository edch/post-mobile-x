﻿using POST.Models;
using System;

namespace POST.DataSources.DataFeedMessages
{

    public class RunningTradeMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.RunningTrade;

        public string Time { get; }
        public string StockCode { get; }
        public MarketType MarketType { get; }
        public decimal Price { get; }
        public decimal VolumeLot { get; }
        public decimal Previous { get; }
        public string BuyerCode { get; }
        public string BuyerType { get; }
        public string SellerCode { get; }
        public string SellerType { get; }
        public string TradeType { get; }
        public int? StockType { get; }

        public RunningTradeMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            Time = Parser.ReadPOSTString();
            StockCode = Parser.ReadPOSTString();
            string marketTypeString = Parser.ReadPOSTString();
            if (Enum.TryParse(marketTypeString, out MarketType marketType))
            {
                MarketType = marketType;
            }
            else
            {
                throw new ArgumentException($"{marketTypeString} is not a valid MarketType value.");
            }
            Price = Parser.ReadInt32();
            VolumeLot = Parser.ReadInt64();
            Previous = Parser.ReadInt32();
            BuyerCode = Parser.ReadPOSTString();
            BuyerType = Parser.ReadPOSTString();
            SellerCode = Parser.ReadPOSTString();
            SellerType = Parser.ReadPOSTString();
            TradeType = Parser.ReadPOSTString();
            StockType = Parser.ReadInt32();

            Parser = null;
        }
    }
}
