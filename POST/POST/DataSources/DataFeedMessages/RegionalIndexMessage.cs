﻿namespace POST.DataSources.DataFeedMessages
{

    public class RegionalIndexMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.RegionalIndex;

        public string Code { get; }
        public decimal Previous { get; }
        public decimal Open { get; }
        public decimal High { get; }
        public decimal Low { get; }
        public decimal Last { get; }
        public decimal Volume { get; }
        public string TypeOrRegion { get; }

        public RegionalIndexMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            Code = Parser.ReadPOSTString();
            Previous = (decimal)Parser.ReadDouble();
            Open = (decimal)Parser.ReadDouble();
            High = (decimal)Parser.ReadDouble();
            Low = (decimal)Parser.ReadDouble();
            Last = (decimal)Parser.ReadDouble();
            Volume = (decimal)Parser.ReadDouble();
            TypeOrRegion = Parser.ReadPOSTString();

            Parser = null;
        }
    }
}
