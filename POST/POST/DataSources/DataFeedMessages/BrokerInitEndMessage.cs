﻿namespace POST.DataSources.DataFeedMessages
{

    public class BrokerInitEndMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.BrokerEnd;

        public BrokerInitEndMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            Parser = null;
        }
    }
}
