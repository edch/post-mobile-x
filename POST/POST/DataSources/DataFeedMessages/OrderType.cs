﻿namespace POST.DataSources.DataFeedMessages
{
    public enum OrderType
    {
        None,
        Bid,
        Offer
    }
}
