﻿using System;

namespace POST.DataSources.DataFeedMessages
{

    public class IndexChartMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.StockChart;

        public string StockCode { get; }
        public DateTime Time { get; }
        public decimal Open { get; }
        public decimal High { get; }
        public decimal Low { get; }
        public decimal Close { get; }
        public decimal Volume { get; }

        public IndexChartMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            StockCode = Parser.ReadPOSTString();
            Time = Parser.ReadDateTime();
            Open = (decimal)Parser.ReadDouble();
            High = (decimal)Parser.ReadDouble();
            Low = (decimal)Parser.ReadDouble();
            Close = (decimal)Parser.ReadDouble();
            Volume = Parser.ReadInt64();

            Parser = null;
        }

    }
}
