﻿namespace POST.DataSources.DataFeedMessages
{

    public class IndexQuoteMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.IndexQuote;

        public string IndexCode { get; }
        public decimal Previous { get; }
        public decimal Open { get; }
        public decimal Last { get; }
        public decimal High { get; }
        public decimal Low { get; }
        public decimal Frequency { get; }
        public decimal Volume { get; }
        public decimal Value { get; }

        public IndexQuoteMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            IndexCode = Parser.ReadPOSTString();
            Previous = (decimal)Parser.ReadDouble();
            Open = (decimal)Parser.ReadDouble();
            Last = (decimal)Parser.ReadDouble();
            High = (decimal)Parser.ReadDouble();
            Low = (decimal)Parser.ReadDouble();
            Frequency = Parser.ReadInt32();
            Volume = Parser.ReadInt64();
            Value = Parser.ReadInt64();

            Parser = null;
        }

    }
}
