﻿using System;

namespace POST.DataSources.DataFeedMessages
{

    public class OrderTrackingMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.OrderTracking;

        public string OrderTrackingCode { get; }
        public decimal OrderNo { get; }
        public DateTime Time { get; }
        public string Status { get; }
        public decimal Volume { get; }
        public decimal Remaining { get; }

        public OrderTrackingMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            OrderTrackingCode = Parser.ReadPOSTString();
            OrderNo = Parser.ReadInt64();
            Time = Parser.ReadDateTime();
            Status = Parser.ReadPOSTString();
            Volume = Parser.ReadInt32();
            Remaining = Parser.ReadInt32();

            Parser = null;
        }
    }
}
