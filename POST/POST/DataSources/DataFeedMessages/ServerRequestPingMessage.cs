﻿namespace POST.DataSources.DataFeedMessages
{

    public class ServerRequestPingMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.ServerRequestPing;

        public ServerRequestPingMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            Parser = null;
        }
    }
}
