﻿namespace POST.DataSources.DataFeedMessages
{

    public class StockChartEndMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.StockChartEnd;

        public string StockCode { get; }

        public StockChartEndMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            StockCode = Parser.ReadPOSTString();

            Parser = null;
        }

    }
}
