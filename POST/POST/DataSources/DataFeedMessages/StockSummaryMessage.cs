﻿namespace POST.DataSources.DataFeedMessages
{

    public class StockSummaryMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.StockSummary;

        public string BrokerCode { get; }
        public decimal D_Buy_Value { get; }
        public decimal D_Buy_Volume { get; }
        public decimal D_Buy_Frequency { get; }
        public decimal D_Sell_Value { get; }
        public decimal D_Sell_Volume { get; }
        public decimal D_Sell_Frequency { get; }
        public decimal F_Buy_Value { get; }
        public decimal F_Buy_Volume { get; }
        public decimal F_Buy_Frequency { get; }
        public decimal F_Sell_Value { get; }
        public decimal F_Sell_Volume { get; }
        public decimal F_Sell_Frequency { get; }
        public string StockCode { get; }


        public StockSummaryMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            BrokerCode = Parser.ReadPOSTString();

            D_Buy_Value = Parser.ReadInt64();
            D_Buy_Volume = Parser.ReadInt64();
            D_Buy_Frequency = Parser.ReadInt64();

            D_Sell_Value = Parser.ReadInt64();
            D_Sell_Volume = Parser.ReadInt64();
            D_Sell_Frequency = Parser.ReadInt64();

            F_Buy_Value = Parser.ReadInt64();
            F_Buy_Volume = Parser.ReadInt64();
            F_Buy_Frequency = Parser.ReadInt64();

            F_Sell_Value = Parser.ReadInt64();
            F_Sell_Volume = Parser.ReadInt64();
            F_Sell_Frequency = Parser.ReadInt64();

            StockCode = Parser.ReadPOSTString();

            Parser = null;
        }
    }
}
