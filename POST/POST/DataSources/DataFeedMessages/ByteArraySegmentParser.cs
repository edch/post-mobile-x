﻿using System;
using System.Text;

namespace POST.DataSources.DataFeedMessages
{

    public class ByteArraySegmentParser
    {

        private byte[] _buffer;
        private int _segmentOffset;
        private int _segmentSize;
        private int _pos;

        private static readonly ByteArraySegmentParser _instance = new ByteArraySegmentParser();

        public static ByteArraySegmentParser ReuseInstance(byte[] buffer, int segmentOffset, int segmentSize)
        {
            _instance._buffer = buffer;
            _instance._segmentOffset = segmentOffset;
            _instance._segmentSize = segmentSize;
            _instance._pos = segmentOffset;
            return _instance;
        }

        private ByteArraySegmentParser()
        {
            _buffer = null;
            _segmentOffset = 0;
            _segmentSize = 0;
            _pos = 0;
        }

        public byte ReadByte()
        {
            try
            {
                if (_pos > _segmentOffset + _segmentSize)
                {
                    throw new IndexOutOfRangeException();
                }
                return _buffer[_pos];
            }
            finally
            {
                _pos++;
            }
        }

        public bool ReadBoolean()
        {
            try
            {
                if (_pos > _segmentOffset + _segmentSize)
                {
                    throw new IndexOutOfRangeException();
                }
                return _buffer[_pos] != 0;
            }
            finally
            {
                _pos++;
            }
        }

        public short ReadInt16()
        {
            try
            {
                if (_pos + 2 > _segmentOffset + _segmentSize)
                {
                    throw new IndexOutOfRangeException();
                }

                // reverse
                byte temp = _buffer[_pos];
                _buffer[_pos] = _buffer[_pos + 1];
                _buffer[_pos + 1] = temp;

                return BitConverter.ToInt16(_buffer, _pos);
            }
            finally
            {
                _pos += 2;
            }
        }

        public int ReadInt32()
        {
            try
            {
                if (_pos + 4 > _segmentOffset + _segmentSize)
                {
                    throw new IndexOutOfRangeException();
                }

                // reverse
                byte temp = _buffer[_pos];
                _buffer[_pos] = _buffer[_pos + 3];
                _buffer[_pos + 3] = temp;
                temp = _buffer[_pos + 1];
                _buffer[_pos + 1] = _buffer[_pos + 2];
                _buffer[_pos + 2] = temp;

                return BitConverter.ToInt32(_buffer, _pos);
            }
            finally
            {
                _pos += 4;
            }
        }

        public long ReadInt64()
        {
            try
            {
                if (_pos + 8 > _segmentOffset + _segmentSize)
                {
                    throw new IndexOutOfRangeException();
                }

                // reverse
                byte temp = _buffer[_pos];
                _buffer[_pos] = _buffer[_pos + 7];
                _buffer[_pos + 7] = temp;
                temp = _buffer[_pos + 1];
                _buffer[_pos + 1] = _buffer[_pos + 6];
                _buffer[_pos + 6] = temp;
                temp = _buffer[_pos + 2];
                _buffer[_pos + 2] = _buffer[_pos + 5];
                _buffer[_pos + 5] = temp;
                temp = _buffer[_pos + 3];
                _buffer[_pos + 3] = _buffer[_pos + 4];
                _buffer[_pos + 4] = temp;

                return BitConverter.ToInt64(_buffer, _pos);
            }
            finally
            {
                _pos += 8;
            }
        }

        public double ReadDouble()
        {
            try
            {
                if (_pos + 8 > _segmentOffset + _segmentSize)
                {
                    throw new IndexOutOfRangeException();
                }

                // reverse
                byte temp = _buffer[_pos];
                _buffer[_pos] = _buffer[_pos + 7];
                _buffer[_pos + 7] = temp;
                temp = _buffer[_pos + 1];
                _buffer[_pos + 1] = _buffer[_pos + 6];
                _buffer[_pos + 6] = temp;
                temp = _buffer[_pos + 2];
                _buffer[_pos + 2] = _buffer[_pos + 5];
                _buffer[_pos + 5] = temp;
                temp = _buffer[_pos + 3];
                _buffer[_pos + 3] = _buffer[_pos + 4];
                _buffer[_pos + 4] = temp;

                return BitConverter.ToDouble(_buffer, _pos);
            }
            finally
            {
                _pos += 8;
            }
        }

        public string ReadPOSTString()
        {
            int length = ReadInt16();
            try
            {
                if (_pos + length > _segmentOffset + _segmentSize)
                {
                    throw new IndexOutOfRangeException();
                }
                return Encoding.UTF8.GetString(_buffer, _pos, length);
            }
            finally
            {
                _pos += length;
            }
        }

        public DateTime ReadDateTime()
        {
            string s = ReadPOSTString();
            return DateTime.Parse(s);
        }
    }
}
