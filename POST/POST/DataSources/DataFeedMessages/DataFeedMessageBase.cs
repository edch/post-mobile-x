﻿using Redux;
using System;

namespace POST.DataSources.DataFeedMessages
{

    public abstract class DataFeedMessageBase : IAction
    {

        public int Size { get; }
        public abstract MessageType MessageType { get; }

        protected ByteArraySegmentParser Parser;

        /// <summary>
        /// This constructor enforces all deriving classes to take ArraySegment&lt;byte&gt; as one of its parameters.
        /// </summary>
        /// 

        protected DataFeedMessageBase()
        {
        }

        protected DataFeedMessageBase(byte[] buffer, int bufferOffset, int bufferSize)
        {
            Parser = ByteArraySegmentParser.ReuseInstance(buffer, bufferOffset, bufferSize);
            Size = bufferSize;

            MessageType header = (MessageType)Parser.ReadByte();
            if (header != MessageType)
            {
                throw new ArgumentException($"Binary does not contain {MessageType} message");
            }
        }

        public override string ToString()
        {
            return MessageType.ToString();
        }

        //protected static decimal? NullIfZero(decimal d) {
        //	if (d == 0) return null;
        //	else return d;
        //}

        //protected static decimal? NullIfZero(short? s) {
        //	if (s == null || s == 0) return null;
        //	else return s;
        //}

        //protected static decimal? NullIfZero(int? i) {
        //	if (i == null || i == 0) return null;
        //	else return i;
        //}

        //protected static decimal? NullIfZero(long? l) {
        //	if (l == null || l == 0) return null;
        //	else return l;
        //}
    }
}
