﻿namespace POST.DataSources.DataFeedMessages
{

    public class StockInitEndMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.StockInitEnd;

        public StockInitEndMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            Parser = null;
        }
    }
}
