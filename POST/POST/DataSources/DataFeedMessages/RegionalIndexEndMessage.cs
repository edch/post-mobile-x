﻿namespace POST.DataSources.DataFeedMessages
{

    public class RegionalIndexEndMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.RegionalIndexEnd;

        public RegionalIndexEndMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            Parser = null;
        }
    }
}
