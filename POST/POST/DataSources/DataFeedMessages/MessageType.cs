﻿namespace POST.DataSources.DataFeedMessages
{

    public enum MessageType
    {
        StockInit = 0,
        StockInitEnd = 100,
        StockChart = 1,
        StockChartEnd = 101,
        RegionalIndex = 2,
        RegionalIndexEnd = 102,
        Currency = 3,
        CurrencyEnd = 103,
        NewsList = 4,
        NewsListEnd = 104,
        RunningTrade = 5,
        Broker = 6,
        BrokerEnd = 106,
        BrokerQuote = 7,
        StockQuote = 8,
        IndexQuote = 9,
        NewsDetails = 10,
        OrderBook = 11,
        TradeBook = 12,
        Fundamental = 13,
        String = 14,
        ServerRequestPing = 15,
        ServerResponsePing = 16,
        Transaction = 17,
        StockSummary = 18,
        StockAdvertisement = 19,
        BrokerSummary = 20,
        BrokerDetail = 21,
        OrderTracking = 22,
        IndexChart = 23,
        IndexChartEnd = 123
    }
}
