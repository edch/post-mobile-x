﻿using POST.Models;
using System;

namespace POST.DataSources.DataFeedMessages
{
    public class StockInitMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.StockInit;

        public string StockCode { get; set; }
        public MarketType MarketType { get; set; }
        public string StockName { get; set; }
        public decimal? PrevPrice { get; set; }
        public int SectorId { get; set; }
        public string SubSector { get; set; }
        public bool IsSyariah { get; set; }
        public bool IsLQ45 { get; set; }
        public int SharesPerLot { get; set; }
        public long TotalShare { get; set; }
        public string Remarks { get; set; }

        public StockInitMessage()
        {

        }

        public StockInitMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            StockCode = Parser.ReadPOSTString();
            string marketTypeString = Parser.ReadPOSTString();
            if (Enum.TryParse(marketTypeString, out MarketType marketType))
            {
                MarketType = marketType;
            }
            else
            {
                throw new ArgumentException($"{marketTypeString} is not a valid MarketType value.");
            }
            StockName = Parser.ReadPOSTString();
            PrevPrice = Parser.ReadInt32();
            if (PrevPrice == 0)
            {
                PrevPrice = null;
            }

            SectorId = Parser.ReadInt16();
            SubSector = Parser.ReadPOSTString();
            IsSyariah = Parser.ReadBoolean();
            IsLQ45 = Parser.ReadBoolean();
            SharesPerLot = Parser.ReadInt16();
            TotalShare = Parser.ReadInt64();
            Remarks = Parser.ReadPOSTString();

            Parser = null;
        }

        public StockInitMessage(string StockCode, string marketTypeString, string StockName, decimal PrevPrice, int SectorId, string SubSector, string IsSyariah, string IsLQ45, int SharesPerLot, long TotalShare, string Remarks)
        {
            this.StockCode = StockCode;
            if (Enum.TryParse(marketTypeString, out MarketType marketType))
            {
                MarketType = marketType;
            }
            else
            {
                throw new ArgumentException($"{marketTypeString} is not a valid MarketType value.");
            }
            this.StockName = StockName;
            if (PrevPrice == 0)
            {
                this.PrevPrice = null;
            }
            else
            {
                this.PrevPrice = PrevPrice;
            }
            this.SectorId = SectorId;
            this.SubSector = SubSector;
            if (bool.TryParse(IsSyariah, out bool isSyariah))
            {
                this.IsSyariah = isSyariah;
            }
            else
            {
                this.IsSyariah = false;
            }
            if (bool.TryParse(IsLQ45, out bool isLQ45))
            {
                this.IsLQ45 = isLQ45;
            }
            else
            {
                this.IsLQ45 = false;
            }
            this.SharesPerLot = SharesPerLot;
            this.TotalShare = TotalShare;
            this.Remarks = Remarks;
        }
    }
}
