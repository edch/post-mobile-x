﻿namespace POST.DataSources.DataFeedMessages
{

    public class CurrencyMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.Currency;

        public string Code { get; }
        public string Type { get; }
        public decimal Bid { get; }
        public decimal Ask { get; }
        public decimal Last { get; }
        public string Date { get; }

        public CurrencyMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            Code = Parser.ReadPOSTString();
            Bid = (decimal)Parser.ReadDouble();
            Ask = (decimal)Parser.ReadDouble();
            Last = (decimal)Parser.ReadDouble();
            Type = Parser.ReadPOSTString();
            Date = Parser.ReadPOSTString();

            Parser = null;
        }
    }
}
