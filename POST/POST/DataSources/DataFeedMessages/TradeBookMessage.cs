﻿using POST.Models;
using System;

namespace POST.DataSources.DataFeedMessages
{

    public class TradeBookMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.TradeBook;

        public string StockCode { get; }
        public MarketType MarketType { get; }
        public decimal Price { get; }
        public decimal Volume { get; }
        public decimal Frequency { get; }

        public TradeBookMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            StockCode = Parser.ReadPOSTString();
            string marketTypeString = Parser.ReadPOSTString();
            if (Enum.TryParse(marketTypeString, out MarketType marketType))
            {
                MarketType = marketType;
            }
            else
            {
                throw new ArgumentException($"{marketTypeString} is not a valid MarketType value.");
            }
            Price = Parser.ReadInt32();
            Volume = Parser.ReadInt64();
            Frequency = Parser.ReadInt32();

            Parser = null;
        }
    }
}
