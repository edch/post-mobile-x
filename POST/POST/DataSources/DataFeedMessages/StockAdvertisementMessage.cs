﻿using System;

namespace POST.DataSources.DataFeedMessages
{
    public class StockAdvertisementMessage : DataFeedMessageBase
    {
        public override MessageType MessageType => MessageType.StockAdvertisement;

        public string StockCode { get; }
        public string BrokerCode { get; }
        public OrderType OrderBookType { get; }
        public decimal Price { get; }
        public decimal Volume { get; }
        public string OrderId { get; }

        public StockAdvertisementMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            StockCode = Parser.ReadPOSTString();
            BrokerCode = Parser.ReadPOSTString();
            string orderBookType = Parser.ReadPOSTString();
            Price = Parser.ReadInt32();
            Volume = Parser.ReadInt64();
            OrderId = Parser.ReadPOSTString();
            switch (orderBookType)
            {
                case "B":
                    OrderBookType = OrderType.Bid;
                    break;
                case "O":
                    OrderBookType = OrderType.Offer;
                    break;
                case "":
                    OrderBookType = OrderType.None;
                    break;
                default:
                    throw new NotImplementedException();
            }

            Parser = null;
        }

    }
}
