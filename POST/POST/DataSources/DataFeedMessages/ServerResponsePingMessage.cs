﻿namespace POST.DataSources.DataFeedMessages
{

    public class ServerResponsePingMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.ServerResponsePing;

        public ServerResponsePingMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            Parser = null;
        }
    }
}
