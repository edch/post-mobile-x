﻿namespace POST.DataSources.DataFeedMessages
{

    public class CurrencyEndMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.CurrencyEnd;

        public CurrencyEndMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            Parser = null;
        }

    }
}
