﻿using POST.Models;
using System;

namespace POST.DataSources.DataFeedMessages
{

    public class StockQuoteMessage : DataFeedMessageBase
    {

        public override MessageType MessageType => MessageType.StockQuote;

        public string StockCode { get; }
        public MarketType MarketType { get; }
        public decimal? Previous { get; }
        public decimal? Open { get; }
        public decimal? High { get; }
        public decimal? Low { get; }
        public decimal? Last { get; }
        public decimal Volume { get; }
        public decimal Value { get; }
        public decimal Frequency { get; }
        public decimal TotalForeignSellVolume { get; }
        public decimal TotalForeignSellValue { get; }
        public decimal TotalForeignBuyVolume { get; }
        public decimal TotalForeignBuyValue { get; }
        public decimal? TotalForeignSellFrequency { get; }
        public decimal? TotalForeignBuyFrequency { get; }
        public decimal? BestBidPrice { get; }
        public decimal? BestBidVolume { get; }
        public decimal? BestOfferPrice { get; }
        public decimal? BestOfferVolume { get; }
        public decimal? LastTradeVolume { get; }
        public decimal? TotalDomesticBuyFrequency { get; }
        public decimal? TotalDomesticBuyValue { get; }
        public decimal? TotalDomesticBuyVolume { get; }
        public decimal? TotalDomesticSellFrequency { get; }
        public decimal? TotalDomesticSellValue { get; }
        public decimal? TotalDomesticSellVolume { get; }

        public StockQuoteMessage(byte[] buffer, int bufferOffset, int bufferSize) : base(buffer, bufferOffset, bufferSize)
        {
            StockCode = Parser.ReadPOSTString();
            string marketTypeString = Parser.ReadPOSTString();
            if (Enum.TryParse(marketTypeString, out MarketType marketType))
            {
                MarketType = marketType;
            }
            else
            {
                throw new ArgumentException($"{marketTypeString} is not a valid MarketType value.");
            }
            Previous = NullIfZero(Parser.ReadInt32());
            Open = NullIfZero(Parser.ReadInt32());
            High = NullIfZero(Parser.ReadInt32());
            Low = NullIfZero(Parser.ReadInt32());
            Last = NullIfZero(Parser.ReadInt32());
            Volume = Parser.ReadInt64();
            Value = Parser.ReadInt64();
            Frequency = Parser.ReadInt32();
            TotalForeignSellVolume = Parser.ReadInt64();
            TotalForeignSellValue = Parser.ReadInt64();
            TotalForeignBuyVolume = Parser.ReadInt64();
            TotalForeignBuyValue = Parser.ReadInt64();
            try
            {
                TotalForeignSellFrequency = Parser.ReadInt64();
                TotalForeignBuyFrequency = Parser.ReadInt64();
                BestBidPrice = NullIfZero(Parser.ReadInt32());
                BestBidVolume = NullIfZero(Parser.ReadInt64());
                BestOfferPrice = NullIfZero(Parser.ReadInt32());
                BestOfferVolume = NullIfZero(Parser.ReadInt64());
                LastTradeVolume = NullIfZero(Parser.ReadInt64());
                TotalDomesticBuyFrequency = Parser.ReadInt64();
                TotalDomesticBuyValue = Parser.ReadInt64();
                TotalDomesticBuyVolume = Parser.ReadInt64();
                TotalDomesticSellFrequency = Parser.ReadInt64();
                TotalDomesticSellValue = Parser.ReadInt64();
                TotalDomesticSellVolume = Parser.ReadInt64();
            }
            catch
            {
                TotalForeignSellFrequency = null;
                TotalForeignBuyFrequency = null;
                BestBidPrice = null;
                BestBidVolume = null;
                BestOfferPrice = null;
                BestOfferVolume = null;
                LastTradeVolume = null;
                TotalDomesticBuyFrequency = null;
                TotalDomesticBuyValue = null;
                TotalDomesticBuyVolume = null;
                TotalDomesticSellFrequency = null;
                TotalDomesticSellValue = null;
                TotalDomesticSellVolume = null;
            }

            Parser = null;
        }

        private static decimal? NullIfZero(int i)
        {
            if (i == 0)
            {
                return null;
            }
            else
            {
                return i;
            }
        }

        private static decimal? NullIfZero(long l)
        {
            if (l == 0)
            {
                return null;
            }
            else
            {
                return l;
            }
        }
    }
}
