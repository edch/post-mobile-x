﻿using POST.DataSources.DataFeedMessages;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class BrokerInitClient : IDisposable
    {
        private const string ATHENA_URL_MI_BROKERINIT = "http://athena.post-pro.co.id/mi2/marketInfoData?request=brokerInit";

        private static readonly Lazy<BrokerInitClient> _singletonFactory = new Lazy<BrokerInitClient>(() => new BrokerInitClient());
        public static BrokerInitClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        public BrokerInitClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<List<BrokerInitMessage>> GetBrokerInitAsync(CancellationToken cancellationToken)
        {
            //{"data":["AD","OSO Sekuritas Indonesia",1],"id":"AD.RG"}
            string regex = @"\{""data"":\[""(?<BrokerCode>.*?)"",""(?<BrokerName>.*?)"",(?<Type>.*?)],""id"":""(?<BrokerId>.*?).(?<marketTypeString>.*?)""}";

            List<BrokerInitMessage> lstBrokerInitMessage = new List<BrokerInitMessage>();

            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, ATHENA_URL_MI_BROKERINIT))
            {
                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string strResult = await httpResponse.Content.ReadAsStringAsync();

                    MatchCollection matchCollection = Regex.Matches(strResult, regex);

                    foreach (Match match in matchCollection)
                    {
                        lstBrokerInitMessage.Add(new BrokerInitMessage(match.Groups["BrokerCode"].Value, match.Groups["marketTypeString"].Value, match.Groups["BrokerName"].Value, int.Parse(match.Groups["Type"].Value)));
                    }
                }
            }

            return lstBrokerInitMessage;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

    }
}
