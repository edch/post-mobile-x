﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace POST.DataSources.AlertMessages.Responses
{
    public class Notice
    {

        [JsonProperty("regTime")]
        public string regTime { get; set; }

        [JsonProperty("deleted")]
        public bool deleted { get; set; }

        [JsonProperty("code")]
        public string code { get; set; }

        [JsonProperty("noticetype")]
        public string noticetype { get; set; }

        [JsonProperty("price")]
        public int price { get; set; }

        [JsonProperty("imported")]
        public bool imported { get; set; }

        [JsonProperty("stockid")]
        public string stockid { get; set; }

        [JsonProperty("userid")]
        public string userid { get; set; }

        [JsonProperty("sent")]
        public bool sent { get; set; }

        [JsonProperty("noticeid")]
        public string noticeid { get; set; }

        public string ConditionLabel
        {
            get
            {
                if (noticetype == "HPRICE")
                {
                    return ">=";
                }
                else
                {
                    return "<=";
                }
            }
        }
    }

    public class AlertResponse
    {

        [JsonProperty("notices")]
        public IList<Notice> notices { get; set; }

        [JsonProperty("msg")]
        public string msg { get; set; }
    }
}
