﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace POST.DataSources.AlertMessages.Requests
{
    public class AddNoticeRequest : Dictionary<string, string>
    {
        public string ToContent { get; set; }

        public AddNoticeRequest(
            string userid,
            string stockid,
            string noticetype,
            string price
        )
        {
            Add("userid", userid);
            Add("stockid", stockid);
            Add("noticetype", noticetype);
            Add("price", price);

            ToContent = JsonConvert.SerializeObject(this);
        }
    }
}
