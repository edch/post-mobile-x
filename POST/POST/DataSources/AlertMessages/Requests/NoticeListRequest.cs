﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace POST.DataSources.AlertMessages.Requests
{
    public class NoticeListRequest : Dictionary<string, string>
    {
        public string ToContent { get; set; }

        public NoticeListRequest(
            string userid
        )
        {
            Add("userid", userid);

            ToContent = JsonConvert.SerializeObject(this);
        }
    }
}
