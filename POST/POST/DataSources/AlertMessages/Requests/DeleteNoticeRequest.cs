﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace POST.DataSources.AlertMessages.Requests
{
    public class DeleteNoticeRequest : Dictionary<string, string>
    {
        public string ToContent { get; set; }

        public DeleteNoticeRequest(
            string userid,
            string noticeid
        )
        {
            Add("userid", userid);
            Add("noticeid", noticeid);

            ToContent = JsonConvert.SerializeObject(this);
        }
    }
}
