﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace POST.DataSources.AlertMessages.Requests
{
    public class RegDeviceRequest : Dictionary<string, string>
    {
        public string ToContent { get; set; }

        public RegDeviceRequest(
            string userid,
            string firebaseid,
            string bundleid
        )
        {
            Add("userid", userid);
            Add("firebaseid", firebaseid);
            Add("bundleid", bundleid);

            ToContent = JsonConvert.SerializeObject(this);
        }



    }
}
