﻿using POST.DataSources.DataFeedRequests;
using System;
using System.Threading.Tasks;

namespace POST.DataSources
{
    public class DataFeedClientStateManager<TState> where TState : class
    {

        private readonly Func<TState, DataFeedClient> _clientFactory;
        private DataFeedClient _client;
        public TState State { get; }

        public bool IsConnected => _client?.IsConnected ?? false;

        public DataFeedClientStateManager(TState initialState, Func<TState, DataFeedClient> clientFactory)
        {
            State = initialState;
            _clientFactory = clientFactory ?? throw new ArgumentNullException(nameof(clientFactory));
        }

        public bool StartClient()
        {
            if (_client != null && !_client.IsConnected)
            {
                _client.Dispose();
                _client = null;
                //GC.Collect();
            }
            if (_client == null)
            {
                try
                {
                    _client = _clientFactory(State);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public void StopClient()
        {
            if (_client != null)
            {
                _client.Dispose();
                _client = null;
                //GC.Collect();
            }
        }

        public async Task<bool> SendAsync<TRequest>(TRequest request)
            where TRequest : DataFeedRequestBase
        {
            if (_client != null && _client.IsConnected)
            {
                return await _client.SendAsync(request);
            }
            else
            {
                return false;
            }
        }
    }
}
