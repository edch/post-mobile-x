﻿using Newtonsoft.Json;
using POST.DataSources.ApiResponses;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class FinancialsClient : IDisposable
    {

        public const string URL = "https://chart.post-pro.co.id/api/v1/finance/newFR";

        private static readonly Lazy<FinancialsClient> _singletonFactory = new Lazy<FinancialsClient>(() => new FinancialsClient());
        public static FinancialsClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        public FinancialsClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<Financials> GetFinancialsAsync(string stockCode, FinancialsComparison comparison, CancellationToken cancellationToken)
        {
            if (stockCode == null)
            {
                throw new ArgumentNullException(nameof(stockCode));
            }

            string comparisonCode;
            switch (comparison)
            {
                case FinancialsComparison.Q1:
                    comparisonCode = "3m";
                    break;
                case FinancialsComparison.Q2:
                    comparisonCode = "6m";
                    break;
                case FinancialsComparison.Q3:
                    comparisonCode = "9m";
                    break;
                case FinancialsComparison.Q4:
                    comparisonCode = "12m";
                    break;
                case FinancialsComparison.MRQ:
                    comparisonCode = "auto";
                    break;
                case FinancialsComparison.Quarterly:
                    comparisonCode = "every";
                    break;
                default:
                    throw new ArgumentException($"Unsupported comparison: {comparison}.", nameof(comparison));
            }
            string requestUrl = $"{URL}/{comparisonCode}/{stockCode}";
            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, requestUrl))
            {
                try
                {
                    using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                    {
                        string json = await httpResponse.Content.ReadAsStringAsync();
                        Financials financials = JsonConvert.DeserializeObject<Financials>(json);
                        return financials;
                    }
                }
                catch (OperationCanceledException)
                {
                    throw;
                }
                catch (ObjectDisposedException)
                {
                    throw new OperationCanceledException();
                }
                catch (JsonReaderException)
                {
                    // This will happen until JamesNK released this fix to NuGet: https://github.com/JamesNK/Newtonsoft.Json/pull/1714/commits/d5051cfa5a5ca9e5b141bda4321dd904b72aaecd
                    // TODO: implement custom deserializer to reduce our dependency to James.
                    throw;
                }
                catch (Exception)
                {
                    // Exception handling policy: suppress to null
                    return null;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

    }
}
