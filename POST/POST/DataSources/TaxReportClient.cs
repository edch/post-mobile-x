﻿using Newtonsoft.Json;
using POST.DataSources.TaxReportMessages;
using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{
    public class TaxReportClient : IDisposable
    {
        public const string URL = "https://api.post-pro.co.id/api/v1/TaxReport";

        private static readonly Lazy<TaxReportClient> _singletonFactory = new Lazy<TaxReportClient>(() => new TaxReportClient());
        public static TaxReportClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        public TaxReportClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<string> RequestTaxReportAsync(TaxReportMessageReq taxReportMessageReq, CancellationToken cancellationToken)
        {
            string strJsonReq = JsonConvert.SerializeObject(taxReportMessageReq);
            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Post, URL))
            {
                httpRequest.Content = new StringContent(strJsonReq, Encoding.UTF8, "application/json");
                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string result = await httpResponse.Content.ReadAsStringAsync();

                    return result;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}

