﻿using Newtonsoft.Json;
using POST.DataSources.ApiResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class NewsClient : IDisposable
    {

        public const string URL = "http://chart.post-pro.co.id:8080/api/v1/news";

        private static readonly Lazy<NewsClient> _singletonFactory = new Lazy<NewsClient>(() => new NewsClient());
        public static NewsClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        public NewsClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<ICollection<InternalNews>> GetInternalNewsFeedAsync(InternalNewsCategory category, CancellationToken cancellationToken)
        {
            string requestUrl = $"{URL}/{category}";
            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, requestUrl))
            {
                try
                {
                    using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                    {
                        string json = await httpResponse.Content.ReadAsStringAsync();
                        ApiResult<InternalNews[]> apiResult = JsonConvert.DeserializeObject<ApiResult<InternalNews[]>>(json);
                        return apiResult.Result;
                    }
                }
                catch (OperationCanceledException)
                {
                    throw;
                }
                catch (ObjectDisposedException)
                {
                    throw new OperationCanceledException();
                }
                catch
                {
                    // Exception handling policy: suppress to empty array
                    // TODO: change this to null
                    return new InternalNews[0];
                }
            }
        }

        public async Task<ICollection<ExternalNewsSnippet>> GetExternalNewsSnippetsAsync(CancellationToken cancellationToken)
        {
            string requestUrl = $"{URL}/newslist";
            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, requestUrl))
            {
                try
                {
                    using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                    {
                        string json = await httpResponse.Content.ReadAsStringAsync();
                        ApiResult<ExternalNewsSnippet[]> apiResult = JsonConvert.DeserializeObject<ApiResult<ExternalNewsSnippet[]>>(json);
                        return apiResult.Result;
                    }
                }
                catch (OperationCanceledException)
                {
                    throw;
                }
                catch (ObjectDisposedException)
                {
                    throw new OperationCanceledException();
                }
                catch
                {
                    // Exception handling policy: suppress to empty array
                    // TODO: change this to null
                    return new ExternalNewsSnippet[0];
                }
            }
        }

        public async Task<ExternalNews> GetExternalNewsAsync(int messageNo, CancellationToken cancellationToken)
        {
            string requestUrl = $"http://chart.post-pro.co.id:8080/api/v1/news/newsdetail/{messageNo}";
            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, requestUrl))
            {
                try
                {
                    using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                    {
                        string json = await httpResponse.Content.ReadAsStringAsync();
                        ApiResult<ExternalNews[]> apiResult = JsonConvert.DeserializeObject<ApiResult<ExternalNews[]>>(json);
                        return apiResult.Result.SingleOrDefault();
                    }
                }
                catch (OperationCanceledException)
                {
                    throw;
                }
                catch (ObjectDisposedException)
                {
                    throw new OperationCanceledException();
                }
                catch
                {
                    // Exception handling policy: suppress to null
                    return null;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
