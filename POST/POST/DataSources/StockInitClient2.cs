﻿using Newtonsoft.Json;
using POST.DataSources.DataFeedMessages;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class StockInitClient2 : IDisposable
    {
        //private const string ATHENA_URL_MI_STOCKINIT = "https://172.31.166.131/mi2/marketInfoData?request=stockInit2";
        private const string ATHENA_URL_MI_STOCKINIT = "http://trx.lc.pansolt.co.id/mi2/marketInfoData?request=stockInit2";

        private static readonly Lazy<StockInitClient2> _singletonFactory = new Lazy<StockInitClient2>(() => new StockInitClient2());
        public static StockInitClient2 Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        private readonly CultureInfo usProvider = new CultureInfo("en-US");

        public StockInitClient2()
        {
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
        }

        public async Task<List<StockInitMessage2>> GetStockInitAsync(CancellationToken cancellationToken)
        {
            List<StockInitMessage2> lstStockInitMessage = new List<StockInitMessage2>();

            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, ATHENA_URL_MI_STOCKINIT))
            {
                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string strResult = "";

                    if (httpResponse.Content.Headers.ContentEncoding.Contains("gzip"))
                    {
                        using (Stream s = await httpResponse.Content.ReadAsStreamAsync())
                        {
                            using (GZipStream decompressed = new GZipStream(s, CompressionMode.Decompress))
                            {
                                using (StreamReader rdr = new StreamReader(decompressed))
                                {
                                    strResult = await rdr.ReadToEndAsync();
                                }
                            }
                        }
                    }
                    else
                    {   // Use standard implementation if not compressed
                        strResult = await httpResponse.Content.ReadAsStringAsync();
                    }

                    lstStockInitMessage = JsonConvert.DeserializeObject<List<StockInitMessage2>>(strResult);
                }
            }

            return lstStockInitMessage;
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

    }
}
