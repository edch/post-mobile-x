﻿namespace POST.DataSources
{

    public enum FinancialsComparison
    {
        Q1,
        Q2,
        Q3,
        Q4,
        MRQ,
        Quarterly
    }
}
