﻿using Newtonsoft.Json;
using Redux;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POST.DataSources.DataFeedRequests
{

    [JsonObject(MemberSerialization.OptIn)]
    public abstract class DataFeedRequestBase : IAction
    {

        [JsonProperty("r"), JsonRequired]
        public RequestType RequestType { get; }

        [JsonProperty("d", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public object[] Parameters { get; }

        protected DataFeedRequestBase(RequestType requestType, params object[] parameters)
        {
            RequestType = requestType;
            if (parameters?.Length > 0)
            {
                Parameters = parameters;
            }
        }

        public byte[] ToBinary()
        {

            // Convert all parameters to binaries
            List<byte[]> binaries = Parameters?.Select(ToBinary).ToList() ?? new List<byte[]>();

            // Size = 1 (request type) + binaries
            byte size = (byte)(1 + binaries.Sum(bin => bin.Length));

            // Allocate buffer
            byte[] result = new byte[size + 1];

            // Put size and request type
            result[0] = size;
            result[1] = (byte)RequestType;

            // Put parameters
            int i = 2;
            foreach (byte[] bin in binaries)
            {
                Array.Copy(bin, 0, result, i, bin.Length);
                i += bin.Length;
            }

            return result;
        }

        private byte[] ToBinary(object obj)
        {
            switch (obj)
            {
                case byte b:
                    return new[] { b };
                case short i:
                    return new[] { (byte)i };
                case int i:
                    return new[] { (byte)i };
                case long i:
                    return new[] { (byte)i };
                case string s:
                    byte[] buffer = new byte[2 + s.Length];
                    buffer[0] = (byte)((s.Length & 0xff00) >> 8);
                    buffer[1] = (byte)(s.Length & 0xff);
                    Encoding.UTF8.GetBytes(s, 0, s.Length, buffer, 2);
                    return buffer;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
