﻿namespace POST.DataSources.DataFeedRequests
{

    public class SubscribeOrderTrackingRequest : DataFeedRequestBase
    {
        public SubscribeOrderTrackingRequest(string OrderTrackCode) : base(
            RequestType.OrderTracking,
            1,
            OrderTrackCode
        )
        { }
    }

    public class UnsubscribeOrderTrackingRequest : DataFeedRequestBase
    {
        public UnsubscribeOrderTrackingRequest(string OrderTrackCode) : base(
            RequestType.OrderTracking,
            0,
            OrderTrackCode
        )
        { }
    }
}
