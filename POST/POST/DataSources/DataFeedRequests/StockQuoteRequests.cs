﻿using POST.Models;

namespace POST.DataSources.DataFeedRequests
{

    public class SubscribeStockQuoteRequest : DataFeedRequestBase
    {
        public SubscribeStockQuoteRequest(StockId stockId) : base(
            RequestType.StockQuote,
            1,
            stockId.Code,
            (int)stockId.MarketType
        )
        { }
    }

    public class UnsubscribeStockQuoteRequest : DataFeedRequestBase
    {
        public UnsubscribeStockQuoteRequest(StockId stockId) : base(
            RequestType.StockQuote,
            0,
            stockId.Code,
            (int)stockId.MarketType
        )
        { }
    }

    public class SubscribeAllStockQuotesRequest : DataFeedRequestBase
    {
        public SubscribeAllStockQuotesRequest() : base(
            RequestType.StockQuote,
            1
        )
        { }
    }

    public class UnsubscribeAllStockQuotesRequest : DataFeedRequestBase
    {
        public UnsubscribeAllStockQuotesRequest() : base(
            RequestType.StockQuote,
            0
        )
        { }
    }
}
