﻿namespace POST.DataSources.DataFeedRequests
{

    public class StockInitRequest : DataFeedRequestBase
    {

        public StockInitRequest() : base(RequestType.StockInit) { }
    }
}
