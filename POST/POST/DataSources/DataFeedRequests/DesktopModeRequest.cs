﻿namespace POST.DataSources.DataFeedRequests
{

    public class DesktopModeRequest : DataFeedRequestBase
    {

        public DesktopModeRequest() : base(RequestType.DesktopMode) { }
    }
}
