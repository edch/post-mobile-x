﻿namespace POST.DataSources.DataFeedRequests
{

    public class SubscribeAllBrokerQuotesRequest : DataFeedRequestBase
    {
        public SubscribeAllBrokerQuotesRequest() : base(
            RequestType.BrokerQuote,
            1
        )
        { }
    }

    public class UnsubscribeAllBrokerQuoteRequest : DataFeedRequestBase
    {
        public UnsubscribeAllBrokerQuoteRequest() : base(
            RequestType.BrokerQuote,
            0
        )
        { }
    }
}
