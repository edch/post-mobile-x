﻿namespace POST.DataSources.DataFeedRequests
{

    public class BrokerInitRequest : DataFeedRequestBase
    {

        public BrokerInitRequest() : base(RequestType.BrokerInit) { }
    }
}
