﻿namespace POST.DataSources.DataFeedRequests
{

    public class SubscribeRunningTradeRequest : DataFeedRequestBase
    {
        public SubscribeRunningTradeRequest() : base(
            RequestType.RunningTrade,
            1
        )
        { }
    }

    public class UnsubscribeRunningTradeRequest : DataFeedRequestBase
    {
        public UnsubscribeRunningTradeRequest() : base(
            RequestType.RunningTrade,
            0
        )
        { }
    }
}
