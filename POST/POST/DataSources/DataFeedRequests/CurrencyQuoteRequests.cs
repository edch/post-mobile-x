﻿namespace POST.DataSources.DataFeedRequests
{

    public class SubscribeAllCurrencyQuotesRequest : DataFeedRequestBase
    {
        public SubscribeAllCurrencyQuotesRequest() : base(
            RequestType.CurrencyQuote,
            1
        )
        { }
    }

    public class UnsubscribeAllCurrencyQuotesRequest : DataFeedRequestBase
    {
        public UnsubscribeAllCurrencyQuotesRequest() : base(
            RequestType.CurrencyQuote,
            0
        )
        { }
    }
}
