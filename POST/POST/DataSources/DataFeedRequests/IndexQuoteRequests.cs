﻿namespace POST.DataSources.DataFeedRequests
{

    public class SubscribeAllIndexQuotesRequest : DataFeedRequestBase
    {
        public SubscribeAllIndexQuotesRequest() : base(
            RequestType.IndexQuote,
            1
        )
        { }
    }

    public class UnsubscribeAllIndexQuotesRequest : DataFeedRequestBase
    {
        public UnsubscribeAllIndexQuotesRequest() : base(
            RequestType.IndexQuote,
            0
        )
        { }
    }
}
