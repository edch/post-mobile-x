﻿namespace POST.DataSources.DataFeedRequests
{

    public class RegionalIndexInitRequest : DataFeedRequestBase
    {

        public RegionalIndexInitRequest() : base(RequestType.RegionalIndexInit) { }
    }
}
