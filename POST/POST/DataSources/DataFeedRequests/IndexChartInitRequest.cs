﻿using POST.Models;

namespace POST.DataSources.DataFeedRequests
{
    public class IndexChartInitRequest : DataFeedRequestBase
    {
        public IndexChartInitRequest(string indexCode, IndexChartTimeUnit timeUnit) : base(
            RequestType.StockChartInit,
            (int)timeUnit,
            indexCode
        )
        { }
    }
}
