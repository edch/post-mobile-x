﻿using POST.Models;

namespace POST.DataSources.DataFeedRequests
{

    public class SubscribeStockSummaryRequest : DataFeedRequestBase
    {
        public SubscribeStockSummaryRequest(StockId stockId) : base(
            RequestType.StockSummary,
            1,
            stockId.Code
        )
        { }
    }

    public class UnsubscribeSubscribeStockSummaryRequest : DataFeedRequestBase
    {
        public UnsubscribeSubscribeStockSummaryRequest(StockId stockId) : base(
            RequestType.StockSummary,
            0,
            stockId.Code
        )
        { }
    }
}
