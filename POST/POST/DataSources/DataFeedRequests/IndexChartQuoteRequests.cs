﻿namespace POST.DataSources.DataFeedRequests
{
    public class SubscribeIndexChartQuoteRequest : DataFeedRequestBase
    {
        public SubscribeIndexChartQuoteRequest(string indexCode) : base(
            requestType: RequestType.IndexChartQuote,
            1,
            indexCode
        )
        { }
    }

    public class UnsubscribeIndexChartQuoteRequest : DataFeedRequestBase
    {
        public UnsubscribeIndexChartQuoteRequest(string indexCode) : base(
            requestType: RequestType.IndexChartQuote,
            0,
            indexCode
        )
        { }
    }
}
