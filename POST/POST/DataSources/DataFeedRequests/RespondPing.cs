﻿namespace POST.DataSources.DataFeedRequests
{

    public class RespondPing : DataFeedRequestBase
    {

        public RespondPing() : base(RequestType.RespondPing) { }
    }
}
