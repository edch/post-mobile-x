﻿using POST.Models;

namespace POST.DataSources.DataFeedRequests
{

    public class SubscribeTradeBookRequest : DataFeedRequestBase
    {
        public SubscribeTradeBookRequest(StockId stockId) : base(
            RequestType.TradeBook,
            1,
            stockId.Code,
            (int)stockId.MarketType
        )
        { }
    }

    public class UnsubscribeTradeBookRequest : DataFeedRequestBase
    {
        public UnsubscribeTradeBookRequest(StockId stockId) : base(
            RequestType.TradeBook,
            0,
            stockId.Code,
            (int)stockId.MarketType
        )
        { }
    }
}
