﻿namespace POST.DataSources.DataFeedRequests
{

    public class SubscribeAllRegionalIndexQuotesRequest : DataFeedRequestBase
    {
        public SubscribeAllRegionalIndexQuotesRequest() : base(
            RequestType.RegionalIndexQuote,
            1
        )
        { }
    }

    public class UnsubscribeAllRegionalIndexQuotesRequest : DataFeedRequestBase
    {
        public UnsubscribeAllRegionalIndexQuotesRequest() : base(
            RequestType.RegionalIndexQuote,
            0
        )
        { }
    }
}
