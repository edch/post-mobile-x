﻿namespace POST.DataSources.DataFeedRequests
{

    public class CurrencyInitRequest : DataFeedRequestBase
    {

        public CurrencyInitRequest() : base(RequestType.CurrencyInit) { }
    }
}
