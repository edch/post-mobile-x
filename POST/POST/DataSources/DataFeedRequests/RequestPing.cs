﻿namespace POST.DataSources.DataFeedRequests
{

    public class RequestPing : DataFeedRequestBase
    {

        public RequestPing() : base(RequestType.RequestPing) { }
    }
}
