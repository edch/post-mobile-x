﻿using POST.Models;

namespace POST.DataSources.DataFeedRequests
{

    public class SubscribeOrderBookRequest : DataFeedRequestBase
    {
        public SubscribeOrderBookRequest(StockId stockId) : base(
            RequestType.OrderBook,
            1,
            stockId.Code,
            (int)stockId.MarketType
        )
        { }
    }

    public class UnsubscribeOrderBookRequest : DataFeedRequestBase
    {
        public UnsubscribeOrderBookRequest(StockId stockId) : base(
            RequestType.OrderBook,
            0,
            stockId.Code,
            (int)stockId.MarketType
        )
        { }
    }
}
