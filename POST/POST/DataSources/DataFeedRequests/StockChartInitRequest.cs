﻿using POST.Models;

namespace POST.DataSources.DataFeedRequests
{
    public class StockChartInitRequest : DataFeedRequestBase
    {
        public StockChartInitRequest(string indexCode, StockChartTimeUnit timeUnit) : base(
            RequestType.StockChartInit,
            (int)timeUnit,
            indexCode
        )
        { }
    }
}
