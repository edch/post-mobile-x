﻿namespace POST.DataSources.DataFeedRequests
{

    public enum RequestType
    {
        RequestPing = 0,
        RespondPing = 32,
        StockInit = 1,
        StockChartInit = 2,
        RegionalIndexInit = 3,
        CurrencyInit = 4,
        NewsList = 5,
        RunningTrade = 6,
        BrokerInit = 7,
        BrokerQuote = 8,
        StockQuote = 9,
        RegionalIndexQuote = 13,
        CurrencyQuote = 14,
        IndexQuote = 15,
        OrderBook = 18,
        TradeBook = 19,
        DesktopMode = 34,
        StockSummary = 37,
        OrderTracking = 41,
        IndexChartInit = 42,
        IndexChartQuote = 43
    }
}
