﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace POST.DataSources
{

    public class FeedbackClient : IDisposable
    {
        public const string URL = "https://chart.post-pro.co.id/mobilex/v1/util/feedback";

        private static readonly Lazy<FeedbackClient> _singletonFactory = new Lazy<FeedbackClient>(() => new FeedbackClient());
        public static FeedbackClient Default => _singletonFactory.Value;

        private readonly HttpClient _httpClient;

        public FeedbackClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<string> SendFeedbackAsync(string feedbackText, string appVersion, string os, string osVersion, string deviceType, string deviceId, string userId, string password, CancellationToken cancellationToken)
        {
            Dictionary<string, string> request = new Dictionary<string, string>
            {
                { "feedbackText", feedbackText },
                { "appVersion", appVersion },
                { "os", os },
                { "osVersion", osVersion },
                { "deviceType", deviceType },
                { "deviceID", deviceId },
                { "userID", userId },
                { "password", password }
            };

            using (HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Post, URL))
            {
                httpRequest.Content = new FormUrlEncodedContent(request);
                using (HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken))
                {
                    string result = await httpResponse.Content.ReadAsStringAsync();
                    return result;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _httpClient.CancelPendingRequests();
                    _httpClient.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

    }
}
