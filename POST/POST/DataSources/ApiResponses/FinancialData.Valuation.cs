﻿using Newtonsoft.Json;
using POST.JsonConverters;

namespace POST.DataSources.ApiResponses
{

    public partial class FinancialData
    {

        [JsonProperty("PE_RATIO"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal PriceToEarningsRatio { get; set; }

        [JsonProperty("PX_TO_BOOK_RATIO"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal PriceToBookValueRatio { get; set; }
    }
}
