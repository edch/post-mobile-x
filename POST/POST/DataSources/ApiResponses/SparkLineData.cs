﻿using Newtonsoft.Json;

namespace POST.DataSources.ApiResponses
{

    [JsonObject(MemberSerialization.OptIn)]
    public class SparkLineData
    {

        [JsonProperty("code"), JsonRequired]
        public string StockCode { get; set; }

        [JsonProperty("previous")]
        public decimal? Previous { get; set; }

        [JsonProperty("last")]
        public decimal? Last { get; set; }

        [JsonProperty("sl", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public decimal[] Closes { get; set; } = new decimal[0];
    }
}
