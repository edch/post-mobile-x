﻿using Newtonsoft.Json;

namespace POST.DataSources.ApiResponses
{

    [JsonObject(MemberSerialization.OptIn)]
    public class ApiResult<TResult>
    {

        [JsonProperty("result")]
        public TResult Result { get; set; }
    }
}
