﻿using Newtonsoft.Json;
using System;

namespace POST.DataSources.ApiResponses
{
    public class Msg
    {

        [JsonProperty("RUPS Date")]
        public DateTime RUPSDate { get; set; }

        [JsonProperty("Venue")]
        public string Venue { get; set; }

        [JsonProperty("Perihal")]
        public string Perihal { get; set; }

        [JsonProperty("Remarks")]
        public string Remarks { get; set; }

        [JsonProperty("Cum Date")]
        public string CumDate { get; set; }

        [JsonProperty("Ex Date")]
        public string ExDate { get; set; }

        [JsonProperty("Payment Date")]
        public string PaymentDate { get; set; }

        [JsonProperty("Ratio")]
        public string Ratio { get; set; }

        [JsonProperty("Dist Date")]
        public string DistDate { get; set; }

        [JsonProperty("Allot Date")]
        public string AllotDate { get; set; }

        [JsonProperty("Trading Period")]
        public string TradingPeriod { get; set; }

        [JsonProperty("Good Period")]
        public string GoodPeriod { get; set; }

        [JsonProperty("Distribution Period")]
        public string DistributionPeriod { get; set; }

        [JsonProperty("Stock to Rights Ratio")]
        public string StocktoRightsRatio { get; set; }

        [JsonProperty("Price")]
        public int? Price { get; set; }

        [JsonProperty("Ex Price")]
        public int? ExPrice { get; set; }

        [JsonProperty("Ex Period")]
        public string ExPeriod { get; set; }

        [JsonProperty("Listing Date")]
        public string ListingDate { get; set; }

        [JsonProperty("Maturity Date")]
        public string MaturityDate { get; set; }

        [JsonProperty("Expiry Date")]
        public string ExpiryDate { get; set; }

        [JsonProperty("Old Price")]
        public int? OldPrice { get; set; }

        [JsonProperty("New Price")]
        public int? NewPrice { get; set; }

        [JsonProperty("Exercise Date")]
        public string ExerciseDate { get; set; }

        [JsonProperty("Recording Date")]
        public string RecordingDate { get; set; }

        [JsonProperty("Issued Date")]
        public string IssuedDate { get; set; }

        [JsonProperty("Dividend Stock Price")]
        public int? DividendStockPrice { get; set; }

        public string CashDividend_View => Ratio.Substring(Ratio.IndexOf(":") + 1);

        public string RUPSDate_View => RUPSDate.ToString("dd MMM yyyy");

        public string RUPSDate_Compare => RUPSDate.ToString("yyyyMMdd");

        public string RUPSTime_View => RUPSDate.ToString("HH:mm");

        public string CumDate_View
        {
            get
            {
                string date = "";

                if (DateTime.TryParse(CumDate, out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string CumDate_Compare
        {
            get
            {
                string date = "";

                if (DateTime.TryParse(CumDate, out DateTime dt))
                {
                    date = dt.ToString("yyyyMMdd");
                }
                return date;
            }
        }

        public string ExDate_View
        {
            get
            {
                string date = "";

                if (DateTime.TryParse(ExDate, out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string ExerciseDate_View
        {
            get
            {
                string date = "";

                if (DateTime.TryParse(ExerciseDate, out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string RecordingDate_View
        {
            get
            {
                string date = "";

                if (DateTime.TryParse(RecordingDate, out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string IssuedDate_View
        {
            get
            {
                string date = "";

                if (DateTime.TryParse(IssuedDate, out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string IssuedDate_Compare
        {
            get
            {
                string date = "";

                if (DateTime.TryParse(IssuedDate, out DateTime dt))
                {
                    date = dt.ToString("yyyyMMdd");
                }
                return date;
            }
        }

        public string PaymentDate_View
        {
            get
            {
                string date = "";

                if (DateTime.TryParse(PaymentDate, out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string DistDate_View
        {
            get
            {
                string date = "";

                if (DateTime.TryParse(DistDate, out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string AllotDate_View
        {
            get
            {
                string date = "";

                if (DateTime.TryParse(AllotDate, out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string TradingStartDate_View
        {
            get
            {
                //2018-09-14>2018-09-20
                string date = "";

                if (DateTime.TryParse(TradingPeriod.Split(">")[0], out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string TradingEndDate_View
        {
            get
            {
                //2018-09-14>2018-09-20
                string date = "";

                if (DateTime.TryParse(TradingPeriod.Split(">")[1], out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string ListingDate_View
        {
            get
            {
                string date = "";

                if (DateTime.TryParse(ListingDate, out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string ListingDate_Compare
        {
            get
            {
                string date = "";

                if (DateTime.TryParse(ListingDate, out DateTime dt))
                {
                    date = dt.ToString("yyyyMMdd");
                }
                return date;
            }
        }

        public string StartPeriodDate_View
        {
            get
            {
                //2018-09-14>2018-09-20
                string date = "";

                if (DateTime.TryParse(ExPeriod.Split(">")[0], out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string EndPeriodDate_View
        {
            get
            {
                //2018-09-14>2018-09-20
                string date = "";

                if (DateTime.TryParse(ExPeriod.Split(">")[1], out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string ExpiryStartDate_View
        {
            get
            {
                //2018-09-14>2018-09-20
                string date = "";

                if (DateTime.TryParse(ExpiryDate.Split(">")[0], out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }

        public string ExpiryEndDate_View
        {
            get
            {
                //2018-09-14>2018-09-20
                string date = "";

                if (DateTime.TryParse(ExpiryDate.Split(">")[1], out DateTime dt))
                {
                    date = dt.ToString("dd MMM yyyy");
                }
                return date;
            }
        }
    }

    public class CorporateAction
    {

        [JsonProperty("actionType")]
        public string actionType { get; set; }

        [JsonProperty("msg")]
        public Msg msg { get; set; }
    }

}
