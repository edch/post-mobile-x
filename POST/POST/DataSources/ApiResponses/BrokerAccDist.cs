﻿namespace POST.DataSources.ApiResponses
{

    public class BrokerAccDist
    {

        public string BrokerCode { get; }

        public decimal SellTVol { get; }

        public decimal SellTVal { get; }

        public decimal BuyTVol { get; }

        public decimal BuyTVal { get; }

        public decimal NetTVol { get; }

        public BrokerAccDist(
            string brokerCode,
            decimal sellTVol,
            decimal sellTVal,
            decimal buyTVol,
            decimal buyTVal,
            decimal netTVol
        )
        {
            BrokerCode = brokerCode;
            SellTVol = sellTVol;
            SellTVal = sellTVal;
            BuyTVol = buyTVol;
            BuyTVal = buyTVal;
            NetTVol = netTVol;
        }
    }
}
