﻿using Newtonsoft.Json;
using POST.JsonConverters;

namespace POST.DataSources.ApiResponses
{

    public partial class FinancialData
    {


        [JsonProperty("CF_CASH_FROM_OPER"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal OperatingCashFlow_Million { get; set; }

        [JsonProperty("CF_CASH_FROM_OPER_C"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal CummulativeOperatingCashFlow_Million { get; set; }


        [JsonProperty("CF_FREE_CASH_FLOW"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal FreeCashFlow_Million { get; set; }

        [JsonProperty("CF_FREE_CASH_FLOW_C"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal CummulativeFreeCashFlow_Million { get; set; }

        [JsonProperty("BOOK_VAL_PER_SH"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal BookValuePerShare { get; set; }

        public decimal OperatingCashFlow => OperatingCashFlow_Million * 1_000_000m;
        public decimal FreeCashFlow => FreeCashFlow_Million * 1_000_000m;
        public decimal CummulativeOperatingCashFlow => CummulativeOperatingCashFlow_Million * 1_000_000m;
        public decimal CummulativeFreeCashFlow => CummulativeFreeCashFlow_Million * 1_000_000m;
    }
}
