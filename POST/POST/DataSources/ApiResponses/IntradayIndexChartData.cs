﻿using Newtonsoft.Json;
using System;

namespace POST.DataSources.ApiResponses
{

    [JsonObject(MemberSerialization.OptIn)]
    public class IntradayIndexChartData
    {

        [JsonProperty("Date")]
        public DateTime Date { get; set; }

        [JsonProperty("Open")]
        public decimal Open { get; set; }

        [JsonProperty("High")]
        public decimal High { get; set; }

        [JsonProperty("Low")]
        public decimal Low { get; set; }

        [JsonProperty("Close")]
        public decimal Close { get; set; }
    }
}
