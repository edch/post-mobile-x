﻿using Newtonsoft.Json;
using POST.JsonConverters;

namespace POST.DataSources.ApiResponses
{

    public partial class FinancialData
    {

        #region Finance

        /// <summary>
        /// Finance only
        /// </summary>
        [JsonProperty("LOAN_LOSS_COVERAGE", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate), JsonConverter(typeof(StringDecimalConverter))]
        public decimal? LoanLossCoverage_Percent { get; set; }

        /// <summary>
        /// Finance only
        /// </summary>
        [JsonProperty("ARDR_CAP_ADEQUACY_RATIO_MKT_RISK", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate), JsonConverter(typeof(StringDecimalConverter))]
        public decimal? CapitalAdequacyRatio_Percent { get; set; }

        /// <summary>
        /// Finance only
        /// </summary>
        public decimal? LoanLossCoverage => LoanLossCoverage_Percent.HasValue ? (decimal?)LoanLossCoverage_Percent.Value / 100m : null;

        /// <summary>
        /// Finance only
        /// </summary>
        public decimal? CapitalAdequacyRatio => CapitalAdequacyRatio_Percent.HasValue ? (decimal?)CapitalAdequacyRatio_Percent.Value / 100m : null;

        #endregion

        #region Non-Finance

        /// <summary>
        /// Non-Finance only
        /// </summary>
        [JsonProperty("EBITDA_TO_TOT_INT_EXP", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate), JsonConverter(typeof(StringDecimalConverter))]
        public decimal? InterestCoverageRatio_Percent { get; set; }

        /// <summary>
        /// Non-Finance only
        /// </summary>
        [JsonProperty("NET_DEBT_TO_EBITDA", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate), JsonConverter(typeof(StringDecimalConverter))]
        public decimal? NetDebtToEBITDA_Percent { get; set; }

        /// <summary>
        /// Non-Finance only
        /// </summary>
        public decimal? InterestCoverageRatio => InterestCoverageRatio_Percent.HasValue ? (decimal?)InterestCoverageRatio_Percent.Value / 100m : null;

        /// <summary>
        /// Non-Finance only
        /// </summary>
        public decimal? NetDebtToEBITDA => NetDebtToEBITDA_Percent.HasValue ? (decimal?)NetDebtToEBITDA_Percent.Value / 100m : null;

        #endregion
    }
}
