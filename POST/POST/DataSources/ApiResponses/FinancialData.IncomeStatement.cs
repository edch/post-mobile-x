﻿using Newtonsoft.Json;
using POST.JsonConverters;

namespace POST.DataSources.ApiResponses
{

    public partial class FinancialData
    {

        [JsonProperty("NET_INT_INC"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal NetInterestIncome_Million { get; set; }

        [JsonProperty("NET_REV_AFT_PROV"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal IncomeAfterProvision_Million { get; set; }

        [JsonProperty("IS_OPER_INC"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal OperatingProfit_Million { get; set; }

        [JsonProperty("PRETAX_INC"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal PretaxIncome_Million { get; set; }

        [JsonProperty("NET_INCOME"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal NetProfit_Million { get; set; }

        [JsonProperty("SALES_REV_TURN"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal SalesRevTurn_Million { get; set; }

        [JsonProperty("GROSS_PROFIT"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal GrossProfit_Million { get; set; }

        [JsonProperty("EBITDA"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal Ebitda_Million { get; set; }




        public decimal NetInterestIncome => NetInterestIncome_Million * 1_000_000m;
        public decimal IncomeAfterProvision => IncomeAfterProvision_Million * 1_000_000m;
        public decimal OperatingProfit => OperatingProfit_Million * 1_000_000m;
        public decimal PretaxIncome => PretaxIncome_Million * 1_000_000m;
        public decimal NetProfit => NetProfit_Million * 1_000_000m;
        public decimal SalesRevTurn => SalesRevTurn_Million * 1_000_000m;
        public decimal GrossProfit => GrossProfit_Million * 1_000_000m;
        public decimal Ebitda => Ebitda_Million * 1_000_000m;

        #region Cumulative

        [JsonProperty("NET_INT_INC_C"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal CumulativeNetInterestIncome_Million { get; set; }

        [JsonProperty("NET_REV_AFT_PROV_C"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal CumulativeIncomeAfterProvision_Million { get; set; }

        [JsonProperty("IS_OPER_INC_C"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal CumulativeOperatingProfit_Million { get; set; }

        [JsonProperty("PRETAX_INC_C"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal CumulativePretaxIncome_Million { get; set; }

        [JsonProperty("NET_INCOME_C"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal CumulativeNetProfit_Million { get; set; }

        [JsonProperty("SALES_REV_TURN_C"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal CumulativeSalesRevTurn_Million { get; set; }

        [JsonProperty("GROSS_PROFIT_C"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal CumulativeGrossProfit_Million { get; set; }

        [JsonProperty("EBITDA_C"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal CumulativEbitda_Million { get; set; }

        public decimal CumulativeNetInterestIncome => CumulativeNetInterestIncome_Million * 1_000_000m;
        public decimal CumulativeIncomeAfterProvision => CumulativeIncomeAfterProvision_Million * 1_000_000m;
        public decimal CumulativeOperatingProfit => CumulativeOperatingProfit_Million * 1_000_000m;
        public decimal CumulativePretaxIncome => CumulativePretaxIncome_Million * 1_000_000m;
        public decimal CumulativeNetProfit => CumulativeNetProfit_Million * 1_000_000m;
        public decimal CumulativeSalesRevTurn => CumulativeSalesRevTurn_Million * 1_000_000m;
        public decimal CumulativeGrossProfit => CumulativeGrossProfit_Million * 1_000_000m;
        public decimal CumulativEbitda => CumulativEbitda_Million * 1_000_000m;

        #endregion
    }
}
