﻿using Newtonsoft.Json;
using POST.JsonConverters;

namespace POST.DataSources.ApiResponses
{

    public partial class FinancialData
    {

        [JsonProperty("GROSS_MARGIN"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal GrossMargin_Percent { get; set; }

        [JsonProperty("OPER_MARGIN"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal OperatingMargin_Percent { get; set; }

        [JsonProperty("PROF_MARGIN"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal NetInterestMargin_Percent { get; set; }


        [JsonProperty("T12_NET_INT_MARGIN"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal T12NetInterestMargin_Percent { get; set; }


        public decimal GrossMargin => GrossMargin_Percent / 100m;
        public decimal OperatingMargin => OperatingMargin_Percent / 100m;
        public decimal NetInterestMargin => NetInterestMargin_Percent / 100m;
        public decimal T12NetInterestMargin => T12NetInterestMargin_Percent / 100m;
    }
}
