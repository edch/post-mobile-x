﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace POST.DataSources.ApiResponses
{
    public class Profile
    {

        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("BAE")]
        public string BAE { get; set; }

        [JsonProperty("Fax")]
        public string Fax { get; set; }

        [JsonProperty("Logo")]
        public string Logo { get; set; }

        [JsonProperty("NPKP")]
        public string NPKP { get; set; }

        [JsonProperty("NPWP")]
        public string NPWP { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Alamat")]
        public string Alamat { get; set; }

        [JsonProperty("DataID")]
        public int DataID { get; set; }

        [JsonProperty("Divisi")]
        public object Divisi { get; set; }

        [JsonProperty("Sektor")]
        public string Sektor { get; set; }

        [JsonProperty("Status")]
        public int Status { get; set; }

        [JsonProperty("Telepon")]
        public string Telepon { get; set; }

        [JsonProperty("Website")]
        public string Website { get; set; }

        [JsonProperty("SubSektor")]
        public string SubSektor { get; set; }

        [JsonProperty("KodeDivisi")]
        public object KodeDivisi { get; set; }

        [JsonProperty("KodeEmiten")]
        public string KodeEmiten { get; set; }

        [JsonProperty("NamaEmiten")]
        public string NamaEmiten { get; set; }

        [JsonProperty("JenisEmiten")]
        public object JenisEmiten { get; set; }

        [JsonProperty("EfekEmiten_EBA")]
        public bool EfekEmiten_EBA { get; set; }

        [JsonProperty("EfekEmiten_ETF")]
        public bool EfekEmiten_ETF { get; set; }

        [JsonProperty("EfekEmiten_SPEI")]
        public bool EfekEmiten_SPEI { get; set; }

        [JsonProperty("PapanPencatatan")]
        public string PapanPencatatan { get; set; }

        [JsonProperty("EfekEmiten_Saham")]
        public bool EfekEmiten_Saham { get; set; }

        [JsonProperty("TanggalPencatatan")]
        public DateTime TanggalPencatatan { get; set; }

        [JsonProperty("KegiatanUsahaUtama")]
        public string KegiatanUsahaUtama { get; set; }

        [JsonProperty("EfekEmiten_Obligasi")]
        public bool EfekEmiten_Obligasi { get; set; }
    }

    public class Pemegangsaham
    {

        [JsonProperty("Nama")]
        public string Nama { get; set; }

        [JsonProperty("Jumlah")]
        public object Jumlah { get; set; }

        [JsonProperty("Kategori")]
        public string Kategori { get; set; }

        [JsonProperty("Pengendali")]
        public bool Pengendali { get; set; }

        [JsonProperty("Persentase")]
        public double Persentase { get; set; }

        public string Deskripsi => Nama + " (" + Kategori + ")";
    }

    public class Komisari
    {

        [JsonProperty("Nama")]
        public string Nama { get; set; }

        [JsonProperty("Jabatan")]
        public string Jabatan { get; set; }

        [JsonProperty("Independen")]
        public bool Independen { get; set; }
    }

    public class Direktur
    {

        [JsonProperty("Nama")]
        public string Nama { get; set; }

        [JsonProperty("Jabatan")]
        public string Jabatan { get; set; }

        [JsonProperty("Afiliasi")]
        public bool Afiliasi { get; set; }
    }

    public class CompanyProfile
    {

        [JsonProperty("kodeemiten")]
        public string kodeemiten { get; set; }

        [JsonProperty("profiles")]
        public IList<Profile> profiles { get; set; }

        [JsonProperty("pemegangsaham")]
        public IList<Pemegangsaham> pemegangsaham { get; set; }

        [JsonProperty("komisaris")]
        public IList<Komisari> komisaris { get; set; }

        [JsonProperty("direktur")]
        public IList<Direktur> direktur { get; set; }
    }
}
