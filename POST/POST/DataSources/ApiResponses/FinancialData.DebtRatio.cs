﻿using Newtonsoft.Json;
using POST.JsonConverters;

namespace POST.DataSources.ApiResponses
{

    public partial class FinancialData
    {

        #region Finance

        /// <summary>
        /// Finance only
        /// </summary>
        [JsonProperty("TOT_LOAN_TO_TOT_DPST", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate), JsonConverter(typeof(StringDecimalConverter))]
        public decimal? LoanToDepositRatio_Percent { get; set; }

        /// <summary>
        /// Finance only
        /// </summary>
        [JsonProperty("NPLS_TO_TOTAL_LOANS", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate), JsonConverter(typeof(StringDecimalConverter))]
        public decimal? NonPerformingLoans_Percent { get; set; }

        /// <summary>
        /// Finance only
        /// </summary>
        public decimal? LoanToDepositRatio => LoanToDepositRatio_Percent.HasValue ? (decimal?)LoanToDepositRatio_Percent.Value / 100m : null;

        /// <summary>
        /// Finance only
        /// </summary>
        public decimal? NonPerformingLoans => NonPerformingLoans_Percent.HasValue ? (decimal?)NonPerformingLoans_Percent.Value / 100m : null;

        #endregion

        #region Non-Finance

        /// <summary>
        /// Non-Finance only
        /// </summary>
        [JsonProperty("TOT_DEBT_TO_TOT_EQY", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate), JsonConverter(typeof(StringDecimalConverter))]
        public decimal? DebtEquityRatio_Percent { get; set; }

        /// <summary>
        /// Non-Finance only
        /// </summary>
        [JsonProperty("NET_DEBT_TO_SHRHLDR_EQTY", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate), JsonConverter(typeof(StringDecimalConverter))]
        public decimal? GearingRatio_Percent { get; set; }

        /// <summary>
        /// Non-Finance only
        /// </summary>
        public decimal? DebtEquityRatio => DebtEquityRatio_Percent.HasValue ? (decimal?)DebtEquityRatio_Percent.Value / 100m : null;

        /// <summary>
        /// Non-Finance only
        /// </summary>
        public decimal? GearingRatio => GearingRatio_Percent.HasValue ? (decimal?)GearingRatio_Percent.Value / 100m : null;

        #endregion
    }
}
