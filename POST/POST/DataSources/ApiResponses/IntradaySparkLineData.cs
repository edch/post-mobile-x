﻿using Newtonsoft.Json;

namespace POST.DataSources.ApiResponses
{

    [JsonObject(MemberSerialization.OptIn)]
    public class IntradaySparkLineData
    {

        [JsonProperty("code")]
        public string StockCode { get; set; }

        [JsonProperty("sl")]
        public string[] Data { get; set; }
    }
}
