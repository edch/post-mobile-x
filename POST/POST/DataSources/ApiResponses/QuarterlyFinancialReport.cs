﻿using Newtonsoft.Json;

namespace POST.DataSources.ApiResponses
{

    [JsonObject(MemberSerialization.OptIn)]
    public class QuarterlyFinancialReport
    {

        [JsonProperty("period")]
        public string Period { get; set; }

        [JsonProperty("is_finance")]
        public bool IsFinanceCompany { get; set; }

        [JsonProperty("data")]
        public FinancialData Data { get; set; }
    }
}
