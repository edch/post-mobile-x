﻿using Newtonsoft.Json;
using POST.Models;
using System;
using System.Linq;

namespace POST.DataSources.ApiResponses
{

    [JsonObject(MemberSerialization.OptIn)]
    public class ExternalNewsSnippet : IUniqueIndexable<int>
    {

        [JsonProperty("message_no")]
        public int MessageNo { get; set; }

        public int Id => MessageNo;

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("news_date")]
        public DateTime NewsDate { get; set; }

        public override int GetHashCode()
        {
            return MessageNo.GetHashCode() ^
(Source?.GetHashCode() ?? 0) ^
(Title?.GetHashCode() ?? 0) ^
NewsDate.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is ExternalNewsSnippet other &&
MessageNo == other.MessageNo &&
Source == other.Source &&
Title == other.Title &&
NewsDate == other.NewsDate;
        }

        public void UpdateWith(object obj)
        {
            // Nothing to update.
        }

        #region Helper Properties
        [JsonIgnore]
        public string SourceName
        {
            get
            {
                switch (Source)
                {
                    case "IQP": return "IQ PLUS";
                    default: return Source;
                }
            }
        }

        [JsonIgnore]
        public string SourceIcon => "\uf1ea";

        [JsonIgnore]
        public bool Saved => Store.SavedNewsCollection.GetState().Contains(this);

        #endregion
    }
}
