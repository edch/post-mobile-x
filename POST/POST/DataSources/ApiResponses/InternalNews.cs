﻿using Newtonsoft.Json;
using POST.Models;
using System;
using System.Linq;
using Xamarin.Forms;

namespace POST.DataSources.ApiResponses
{

    [JsonObject(MemberSerialization.OptIn)]
    public class InternalNews : IUniqueIndexable<int>
    {

        [JsonProperty("id"), JsonRequired]
        public int Id { get; set; }

        [JsonProperty("categ")]
        public string Category { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonIgnore]
        public string CleanedNotes => Notes
            .Replace("<br/>", "\n")
            .Replace("<br />", "\n")
            .Replace("\n ", "\n")
            .Replace("\n ", "\n")
            .Replace("\n\n\n", "\n\n")
            .Replace("\n\n\n", "\n\n") + "...";

        [JsonProperty("filename")]
        public string FileName { get; set; }

        [JsonProperty("article_date")]
        public DateTime ArticleDate { get; set; }

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^
(Category?.GetHashCode() ?? 0) ^
(Title?.GetHashCode() ?? 0) ^
(Notes?.GetHashCode() ?? 0) ^
(FileName?.GetHashCode() ?? 0) ^
ArticleDate.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is InternalNews other &&
Id == other.Id &&
Category == other.Category &&
Title == other.Title &&
Notes == other.Notes &&
FileName == other.FileName &&
ArticleDate == other.ArticleDate;
        }

        public void UpdateWith(object obj)
        {
            // Nothing to update.
        }

        #region Helper Properties

        [JsonIgnore]
        public Color AccentColor
        {
            get
            {
                switch (Category)
                {
                    case "paninNews": return Color.FromUint(0xfffc0201);
                    case "dailyTechnical": return Color.FromUint(0xff1b79ce);
                    case "tradingIdea": return Color.FromUint(0xff4d4d4d);
                    case "inDepth": return Color.FromUint(0xff6ba83f);
                    default: return Color.FromUint(0xff000000);
                }
            }
        }

        [JsonIgnore]
        public string CategoryName
        {
            get
            {
                switch (Category)
                {
                    case "paninNews": return "PANIN NEWS";
                    case "dailyTechnical": return "DAILY TECHNICAL";
                    case "tradingIdea": return "TRADING IDEA";
                    case "inDepth": return "IN-DEPTH ANALYSIS";
                    default: return "NEWS";
                }
            }
        }

        [JsonIgnore]
        public string CategoryIcon
        {
            get
            {
                switch (Category)
                {
                    case "paninNews": return "\uf1ea";
                    case "dailyTechnical": return "\uf1fe";
                    case "tradingIdea": return "\uf0eb";
                    case "inDepth": return "";
                    default: return "";
                }
            }
        }

        [JsonIgnore]
        public bool Saved => Store.SavedNewsCollection.GetState().Contains(this);

        #endregion
    }
}
