﻿using Newtonsoft.Json;
using POST.JsonConverters;

namespace POST.DataSources.ApiResponses
{

    public partial class FinancialData
    {

        [JsonProperty("RETURN_ON_ASSET"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal ReturnOnAsset_Percent { get; set; }

        [JsonProperty("RETURN_COM_EQY"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal ReturnOnEquity_Percent { get; set; }

        public decimal ReturnOnAsset => ReturnOnAsset_Percent / 100m;
        public decimal ReturnOnEquity => ReturnOnEquity_Percent / 100m;
    }
}
