﻿using Newtonsoft.Json;
using POST.JsonConverters;

namespace POST.DataSources.ApiResponses
{

    public partial class FinancialData
    {

        [JsonProperty("BS_TOT_ASSET"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal TotalAsset_Million { get; set; }

        [JsonProperty("BS_TOT_LIAB2"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal TotalLiabilities_Million { get; set; }

        [JsonProperty("TOTAL_EQUITY"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal TotalEquity_Million { get; set; }

        [JsonProperty("NET_DEBT"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal NetDebt_Million { get; set; }

        public decimal TotalAsset => TotalAsset_Million * 1_000_000m;
        public decimal TotalLiabilities => TotalLiabilities_Million * 1_000_000m;
        public decimal TotalEquity => TotalEquity_Million * 1_000_000m;
        public decimal NetDebt => NetDebt_Million * 1_000_000m;
    }
}
