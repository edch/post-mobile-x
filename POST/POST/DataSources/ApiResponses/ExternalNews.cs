﻿using Newtonsoft.Json;

namespace POST.DataSources.ApiResponses
{

    [JsonObject(MemberSerialization.OptIn)]
    public class ExternalNews : ExternalNewsSnippet
    {

        /// <summary>
        /// This field is not included in snippet
        /// </summary>
        [JsonProperty("content", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Content { get; set; }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^
(Content?.GetHashCode() ?? 0);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as ExternalNewsSnippet) &&
obj is ExternalNews other &&
Content == other.Content;
        }
    }
}
