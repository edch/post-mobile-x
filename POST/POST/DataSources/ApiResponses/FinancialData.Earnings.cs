﻿using Newtonsoft.Json;
using POST.JsonConverters;

namespace POST.DataSources.ApiResponses
{

    public partial class FinancialData
    {

        [JsonProperty("IS_EPS"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal EarningsPerShare { get; set; }

        [JsonProperty("TRAIL_12M_EPS"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal EarningsPerShareAnnualized { get; set; }

        #region Cumulative

        [JsonProperty("IS_EPS_C"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal CumulativeEarningsPerShare { get; set; }

        [JsonProperty("TRAIL_12M_EPS_C"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal CumulativeEarningsPerShareAnnualized { get; set; }

        #endregion
    }
}
