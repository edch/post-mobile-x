﻿using Newtonsoft.Json;
using POST.JsonConverters;
using System.Collections.Generic;

namespace POST.DataSources.ApiResponses
{

    [JsonObject]
    public class Financials
    {

        [JsonProperty("lastPrice"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal LastPrice { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("finance")]
        public List<QuarterlyFinancialReport> Reports { get; set; }
    }
}
