﻿using Newtonsoft.Json;
using POST.JsonConverters;

namespace POST.DataSources.ApiResponses
{

    [JsonObject(MemberSerialization.OptIn)]
    public partial class FinancialData
    {

        [JsonProperty("PX_LAST"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal LastPrice { get; set; }

        [JsonProperty("EQY_SH_OUT"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal OutstandingShares_Million { get; set; }

        [JsonProperty("HISTORICAL_MARKET_CAP"), JsonConverter(typeof(StringDecimalConverter))]
        public decimal MarketCapitalization_Million { get; set; }

        public decimal OutstandingShares => OutstandingShares_Million * 1_000_000m;
        public decimal MarketCapitalization => MarketCapitalization_Million * 1_000_000m;
    }
}
