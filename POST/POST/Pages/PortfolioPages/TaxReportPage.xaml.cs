﻿using POST.Constants;
using POST.DataSources.AthenaMessages.Responses;
using POST.DataSources.TaxReportMessages;
using POST.Dialogs;
using POST.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.PortfolioPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TaxReportPage : ContentPage
    {
        private DateTime lastRefresh = DateTime.Now;
        private readonly List<int> lstYear;
        private readonly List<string> lstAccountId;
        private readonly List<string> lstReportType;

        private Picker pickerAccountId = null;

        private TradingAccount[] tradingAccounts = null;

        public TaxReportPage()
        {
            InitializeComponent();

            lstAccountId = new List<string>();

            int yearNow = DateTime.Now.Year;
            lstYear = new List<int>();
            for (int i = 1; i <= 4; i++)
            {
                lstYear.Add(yearNow - i);
            }

            lstReportType = new List<string>
            {
                "Transaction Report (Sell)",
                "Transaction Report (Buy)",
                "Transaction Report (All)",
                "Dividend Report"
            };
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            pickerRequestYear.ItemsSource = lstYear;

            pickerReportType.ItemsSource = lstReportType;

            Picker pickerAccountIdTmp = new Picker()
            {
                TextColor = Colors.FieldValueDefaultColor,
                Title = "Choose Account",
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 15
            };
            pickerAccountIdTmp.SelectedIndexChanged += PickerAccountIdTmp_SelectedIndexChanged;
            pickerAccountId = pickerAccountIdTmp;

            gridReport.Children.Add(pickerAccountIdTmp, 1, 0);

            RefreshPageAsync(false);
        }

        private void PickerAccountIdTmp_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateDetailAccountAsync();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            if (pickerAccountId != null)
            {
                gridReport.Children.Remove(pickerAccountId);
                pickerAccountId.SelectedIndexChanged -= PickerAccountIdTmp_SelectedIndexChanged;
                pickerAccountId = null;
            }
        }

        private async Task<int> UpdateAccountAsync()
        {
            try
            {
                tradingAccounts = await Store.AthenaSession.GetState().Client.GetTradingAccountsAsync(new CancellationTokenSource(TimeSpan.FromSeconds(10)).Token);
                if (tradingAccounts != null)
                {
                    lstAccountId.Clear();
                    Store.TradingAccounts.SetTradingAccounts(tradingAccounts.ToList());
                    for (int i = 0; i < tradingAccounts.Count(); i++)
                    {
                        lstAccountId.Add($"{tradingAccounts[i].BrokerAccountId}");
                    }
                    pickerAccountId.ItemsSource = lstAccountId;
                    if (lstAccountId.Count > 0)
                    {
                        pickerAccountId.SelectedIndex = 0;
                    }

                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            catch (OperationCanceledException)
            {
                return 3;
            }
            catch
            {
                return 4;
            }
            finally
            {
            }
        }

        private void UpdateDetailAccountAsync()
        {
            if (tradingAccounts != null && pickerAccountId.SelectedIndex >= 0 && tradingAccounts[pickerAccountId.SelectedIndex].Email is string strEmail && !string.IsNullOrEmpty(strEmail))
            {
                lblRegEmail.Text = strEmail;
            }
            else
            {
                lblRegEmail.Text = "";
            }

            if (tradingAccounts != null && pickerAccountId.SelectedIndex >= 0 && tradingAccounts[pickerAccountId.SelectedIndex].CustomerName is string CustomerName && !string.IsNullOrEmpty(CustomerName))
            {
                lblName.Text = CustomerName;
            }
            else
            {
                lblName.Text = "";
            }
        }

        private async void btnExecute_Clicked(object sender, EventArgs e)
        {
            if (pickerAccountId.SelectedIndex == -1)
            {
                DependencyService.Get<IToastMessage>().Show("Select Account ID!");
                return;
            }
            if (pickerReportType.SelectedIndex == -1)
            {
                DependencyService.Get<IToastMessage>().Show("Select Report Type!");
                return;
            }
            if (string.IsNullOrEmpty(lblRegEmail.Text))
            {
                DependencyService.Get<IToastMessage>().Show("No Email Registered for this account! Call Customer Care for further information.");
                return;
            }
            if (pickerRequestYear.SelectedIndex == -1)
            {
                DependencyService.Get<IToastMessage>().Show("Select Year!");
                return;
            }

            TaxReportMessageReq taxReportMessageReq = new TaxReportMessageReq
            {
                ReportYear = lstYear[pickerRequestYear.SelectedIndex]
            };

            // Trans (Sell)
            if (pickerReportType.SelectedIndex == 0)
            {
                taxReportMessageReq.ReportType = 0;
                taxReportMessageReq.OrderType = 'S';
            }
            // Trans (Buy)
            else if (pickerReportType.SelectedIndex == 1)
            {
                taxReportMessageReq.ReportType = 0;
                taxReportMessageReq.OrderType = 'B';
            }
            // Trans (Buy & Sell)
            else if (pickerReportType.SelectedIndex == 2)
            {
                taxReportMessageReq.ReportType = 0;
                taxReportMessageReq.OrderType = 'A';
            }
            // Dividend
            else if (pickerReportType.SelectedIndex == 3)
            {
                taxReportMessageReq.ReportType = 1;
            }

            if (int.TryParse(tradingAccounts[pickerAccountId.SelectedIndex].BrokerAccountId, out int custNo))
            {
                taxReportMessageReq.CustomerNo = custNo;
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Invalid Account Id!");
                return;
            }

            TaxReportRequestConfirmation taxReportRequestConfirmation = new TaxReportRequestConfirmation(taxReportMessageReq, this, tradingAccounts[pickerAccountId.SelectedIndex].Email, tradingAccounts[pickerAccountId.SelectedIndex].CustomerName);
            await PopupNavigation.Instance.PushAsync(taxReportRequestConfirmation);
        }

        public void ClearInput()
        {
            pickerRequestYear.SelectedIndex = -1;
            pickerReportType.SelectedIndex = -1;
        }

        public async void RefreshPageAsync(bool showMessage = true)
        {
            lastRefresh = DateTime.Now;
            int result = await UpdateAccountAsync();
            if (result == 1)
            {
                if (showMessage)
                {
                    DependencyService.Get<IToastMessage>().Show("Account ID Loaded.");
                }
            }
            else if (result == 2)
            {
                DependencyService.Get<IToastMessage>().Show("Null Data.");
            }
            else if (result == 3)
            {
                if (DateTime.Now.CompareTo(lastRefresh.AddSeconds(10)) >= 0)
                {
                    DependencyService.Get<IToastMessage>().Show("Request timeout.");
                }
            }
            else if (result == 4)
            {
                DependencyService.Get<IToastMessage>().Show("Request failed.");
            }

        }

        private void BtnRefresh_Clicked(object sender, EventArgs e)
        {
            RefreshPageAsync();
        }
    }
}