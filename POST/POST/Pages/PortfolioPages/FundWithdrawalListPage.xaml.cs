﻿using POST.Constants;

using POST.DataSources;

using POST.DataSources.WdMessages.Responses;

using POST.Dialogs;

using POST.Models;

using POST.Services;

using POST.Stores;

using Rg.Plugins.Popup.Services;

using System;

using System.Collections.Generic;

using System.Linq;

using System.Threading;

using System.Threading.Tasks;



using Xamarin.Forms;





namespace POST.Pages.PortfolioPages

{

    public partial class FundWithdrawalListPage : ContentPage

    {

        private readonly FundWithdrawalStatusFilterStore FundWithdrawalStatusFilterStore = new FundWithdrawalStatusFilterStore();

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));

        private List<FundWithdrawItem> lstFundWithdrawItem = null;

        private List<FundWithdrawItem> lstFundWithdrawItemTmp = null;

        private ColumnSort columnSort = ColumnSort.ASC;

        private int? lastSortedColumn;
        private DateTime lastRefresh = DateTime.Now;

        private class CustomGrid : Grid

        {

            public int XRow = 0;

            public int YColumn = 0;

        }

        private enum ColumnSort

        {

            ASC,

            DSC

        }

        private class GridSortSign : Grid

        {

            public ColumnSort columnSort = ColumnSort.ASC;
            private readonly Label lblSortForeSign;
            private readonly Label lblSortBackgroundSign;

            public GridSortSign()

            {

                lblSortBackgroundSign = new Label()

                {

                    Text = Glyphs.BackgroundSort,

                    TextColor = Colors.NetralSortColor,

                    FontSize = 12,

                    VerticalOptions = LayoutOptions.Center,

                    Margin = new Thickness(0, 0, 0, -2),

                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],

                    HorizontalOptions = LayoutOptions.Start

                };

                lblSortForeSign = new Label()

                {

                    Text = Glyphs.Unchanged,

                    TextColor = Colors.AscDescSortColor,

                    FontSize = 11,

                    VerticalOptions = LayoutOptions.Center,

                    Margin = new Thickness(0, 0, 0, -2),

                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],

                    HorizontalOptions = LayoutOptions.Start

                };

                Children.Add(lblSortBackgroundSign, 0, 0);

                Children.Add(lblSortForeSign, 0, 0);

            }



            public void SetAsc()

            {

                lblSortForeSign.Text = Glyphs.Gaining;

            }



            public void SetDesc()

            {

                lblSortForeSign.Text = Glyphs.Losing;

            }



            public void SetNetral()

            {

                lblSortForeSign.Text = Glyphs.Unchanged;

            }

        }

        private readonly List<GridSortSign> lstGridSortSign = new List<GridSortSign>();



        private CustomGrid ToHeader(string title, int yColumn, LayoutOptions layoutOptions)

        {

            Label lblTitle = new Label()

            {

                Text = title,

                Style = (Style)Resources["HeaderLabelStyle"]

            };

            GridSortSign gridSortSign = new GridSortSign();

            lstGridSortSign.Add(gridSortSign);



            CustomGrid customGrid = new CustomGrid()

            {

                HorizontalOptions = layoutOptions,

                YColumn = yColumn,

                ColumnSpacing = 1,

                ColumnDefinitions = new ColumnDefinitionCollection() { new ColumnDefinition { Width = GridLength.Auto }, new ColumnDefinition { Width = GridLength.Auto } },

                Children = {

                            lblTitle,

                           gridSortSign

                        }

            };



            Grid.SetRow(lblTitle, 0);

            Grid.SetColumn(lblTitle, 0);

            Grid.SetRow(gridSortSign, 0);

            Grid.SetColumn(gridSortSign, 1);



            customGrid.GestureRecognizers.Add(

                new TapGestureRecognizer

                {

                    Command = new Command(() =>

                    {

                        ResetColumnHeader();



                        if (columnSort == ColumnSort.ASC)

                        {

                            columnSort = ColumnSort.DSC;

                            gridSortSign.SetDesc();

                        }

                        else

                        {

                            columnSort = ColumnSort.ASC;

                            gridSortSign.SetAsc();

                        }



                        lastSortedColumn = yColumn;



                        LoadListAsync(FundWithdrawalStatusFilterStore.GetState(), lastSortedColumn);

                    })

                });



            return customGrid;

        }



        public FundWithdrawalListPage()

        {

            InitializeComponent();



            FundWithdrawalFilterPicker.ItemsSource = FundWithdrawalStatusDict.Items;

            FundWithdrawalFilterPicker.SelectedIndex = 0;



            CreateDataGrid();

        }



        private void CreateDataGrid()

        {

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(85) });

            headerGrid.Children.Add(ToHeader("Requested for", 1, LayoutOptions.Center), headerGrid.Children.Count, 0);


            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(86) });

            headerGrid.Children.Add(ToHeader("Amount", 2, LayoutOptions.End), headerGrid.Children.Count, 0);



            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(80) });

            headerGrid.Children.Add(ToHeader("Status", 3, LayoutOptions.Center), headerGrid.Children.Count, 0);



            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(150) });

            headerGrid.Children.Add(ToHeader("Destination Account", 4, LayoutOptions.Center), headerGrid.Children.Count, 0);




            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(120) });

            headerGrid.Children.Add(ToHeader("On Behalf of", 5, LayoutOptions.Center), headerGrid.Children.Count, 0);

        }



        private void ResetColumnHeader()

        {

            lastSortedColumn = null;

            lstGridSortSign.ForEach(grd => grd.SetNetral());

        }



        protected override void OnAppearing()

        {

            base.OnAppearing();



            _observers.Add(FundWithdrawalStatusFilterStore.Subscribe(fundWithdrawalStatusFilter =>

            {

                LoadListAsync(fundWithdrawalStatusFilter, lastSortedColumn);

            }));



            _observers.Add(Store.AthenaSession.Subscribe(athenaSession =>

            {

                RefreshPageAsync(false);

            }));

        }



        protected override void OnDisappearing()

        {

            base.OnDisappearing();

        }



        private async Task<int> LoadListAsync(string brokerAccountId, CancellationToken cancellationToken)

        {

            try

            {

                WdListResponse wdListResponse = await AthenaClient.Current.GetWdListAsync(brokerAccountId, cancellationToken);

                if (wdListResponse != null && wdListResponse.List != null && wdListResponse.List.Count > 0)

                {

                    lstFundWithdrawItem = wdListResponse.List;

                    lstFundWithdrawItemTmp = new List<FundWithdrawItem>(lstFundWithdrawItem);

                    FundWithdrawalFilterPicker.SelectedIndex = 0;

                    ResetColumnHeader();

                    FundWithdrawalListView.ItemsSource = lstFundWithdrawItemTmp;

                    return 1;

                }

                else

                {

                    ResetColumnHeader();

                    lstFundWithdrawItem = null;

                    FundWithdrawalListView.ItemsSource = null;

                    return 2;

                }

            }

            catch (OperationCanceledException)

            {

                return 3;

            }

            catch (Exception ex)

            {

                ResetColumnHeader();

                FundWithdrawalListView.ItemsSource = null;

                return 4;

            }

            finally

            {

                FundWithdrawalListView.IsRefreshing = false;

            }

        }



        private void LoadListAsync(string fundWithdrawalStatus, int? lastSortedColumn)

        {

            try

            {

                if (lstFundWithdrawItem != null && lstFundWithdrawItem.Count > 0)

                {

                    if (fundWithdrawalStatus.ToUpper() == FundWithdrawalStatusDict.Items[0].ToUpper())

                    {

                        lstFundWithdrawItemTmp = new List<FundWithdrawItem>(lstFundWithdrawItem);

                    }

                    else

                    {

                        lstFundWithdrawItemTmp = new List<FundWithdrawItem>(lstFundWithdrawItem.Where(item => item.Sdesc.ToUpper() == fundWithdrawalStatus.ToUpper()));

                    }



                    if (lastSortedColumn.HasValue)

                    {

                        switch (lastSortedColumn)

                        {

                            case 0:

                                break;



                            case 1:

                                if (columnSort == ColumnSort.ASC)

                                {

                                    lstFundWithdrawItemTmp = lstFundWithdrawItemTmp.OrderBy(ps => ps.RqDate).ToList();

                                }

                                else

                                {

                                    lstFundWithdrawItemTmp = lstFundWithdrawItemTmp.OrderByDescending(ps => ps.RqDate).ToList();

                                }

                                break;



                            case 2:

                                if (columnSort == ColumnSort.ASC)

                                {

                                    lstFundWithdrawItemTmp = lstFundWithdrawItemTmp.OrderBy(ps => ps.Amount).ToList();

                                }

                                else

                                {

                                    lstFundWithdrawItemTmp = lstFundWithdrawItemTmp.OrderByDescending(ps => ps.Amount).ToList();

                                }

                                break;



                            case 3:

                                if (columnSort == ColumnSort.ASC)

                                {

                                    lstFundWithdrawItemTmp = lstFundWithdrawItemTmp.OrderBy(ps => ps.Sdesc).ToList();

                                }

                                else

                                {

                                    lstFundWithdrawItemTmp = lstFundWithdrawItemTmp.OrderByDescending(ps => ps.Sdesc).ToList();

                                }

                                break;



                            case 4:

                                if (columnSort == ColumnSort.ASC)

                                {

                                    lstFundWithdrawItemTmp = lstFundWithdrawItemTmp.OrderBy(ps => ps.DestinationAccount).ToList();

                                }

                                else

                                {

                                    lstFundWithdrawItemTmp = lstFundWithdrawItemTmp.OrderByDescending(ps => ps.DestinationAccount).ToList();

                                }

                                break;


                            case 5:

                                if (columnSort == ColumnSort.ASC)

                                {

                                    lstFundWithdrawItemTmp = lstFundWithdrawItemTmp.OrderBy(ps => ps.BankAccName).ToList();

                                }

                                else

                                {

                                    lstFundWithdrawItemTmp = lstFundWithdrawItemTmp.OrderByDescending(ps => ps.BankAccName).ToList();

                                }

                                break;

                        }

                    }



                    FundWithdrawalListView.ItemsSource = lstFundWithdrawItemTmp;

                }

            }

            catch

            {

                ResetColumnHeader();

                FundWithdrawalListView.ItemsSource = null;

            }

            finally

            {

                FundWithdrawalListView.IsRefreshing = false;

            }

        }



        public async void RefreshPageAsync(bool showMessage = true)

        {

            if (Store.AthenaSession.GetState() != null &&

                    Store.AthenaSession.GetState().BrokerAccountId != null)

            {

                lastRefresh = DateTime.Now;

                _cancellationTokenSource.Cancel();

                _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));

                int result = await LoadListAsync(Store.AthenaSession.GetState().BrokerAccountId, _cancellationTokenSource.Token);

                if (result == 1)

                {

                    if (showMessage)
                    {
                        DependencyService.Get<IToastMessage>().Show("Fund Withdrawal List Loaded.");
                    }
                }

                else if (result == 2)

                {

                    DependencyService.Get<IToastMessage>().Show("Null Result.");

                }

                else if (result == 3)

                {

                    if (DateTime.Now.CompareTo(lastRefresh.AddSeconds(10)) >= 0)

                    {

                        DependencyService.Get<IToastMessage>().Show("Request timeout.");

                    }

                }

                else if (result == 4)

                {

                    DependencyService.Get<IToastMessage>().Show("Request failed.");

                }



            }

            else

            {

                ResetColumnHeader();

                lstFundWithdrawItem = null;

                lstFundWithdrawItemTmp = null;

                FundWithdrawalListView.ItemsSource = null;

                FundWithdrawalFilterPicker.SelectedIndex = 0;

            }

        }



        private void BtnRefresh_Clicked(object sender, EventArgs e)

        {

            RefreshPageAsync();

        }



        private async void SwitchAccountButton_Clicked(object sender, EventArgs e)

        {

            await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());

        }



        public void FundWithdrawalFilterPicker_SelectedIndexChanged(object sender, EventArgs e)

        {

            if (FundWithdrawalFilterPicker.SelectedItem is string selectedItem && selectedItem != null)

            {

                FundWithdrawalStatusFilterStore.Set(selectedItem);

            }

        }



    }

}

