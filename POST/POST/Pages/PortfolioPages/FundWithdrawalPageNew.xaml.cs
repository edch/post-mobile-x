﻿using POST.DataSources.AthenaMessages.Responses;
using POST.DataSources.WdMessages.Responses;
using POST.Dialogs;
using POST.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.PortfolioPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FundWithdrawalPageNew : ContentPage
    {
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();
        DateTime lastRefresh = DateTime.Now;
        Picker pickerRequestDate = null;
        List<String> lstDateString;
        BankAccount bankAccount;
        WdFundInfoResponse wdFundInfoResponse;

        public FundWithdrawalPageNew()
        {
            InitializeComponent();

            lstDateString = new List<string>();

            /*
            lblNotice.Text = "Permohonan penarikan dana ini akan diproses apabila saldo di Rekening Dana Nasabah mencukupi. " +
                            "Permohonan penarikan dana harus diterima selambat-lambatnya pukul 12:00 WIB pada satu hari kerja sebelumnya (T-1) dan akan dijalankan setelah Customer Service berhasil melakukan konfirmasi sebelum tanggal penarikan dana." +
                            "\nKonfirmasi via telepon akan dilakukan untuk jumlah penarikan dana di atas Rp.100.000.000,-(seratus juta rupiah).\n\n" +
                            "This cash withdrawal request will be processed if balance in Invetor Account is available. " +
                            "Fund withdrawal request must be received before 12:00 WIB, at least one working day before (T-1) and will proceed after Customer Service able to confirm before fund withdrawal date." +
                            "\nPhone confirmation will be made for the withdrawal amount above IDR 100,000,000 (one hundred million rupiahs).";
            */

            lblNotice.Text = "Permohonan penarikan dana ini akan diproses apabila saldo di Rekening Dana Nasabah mencukupi." +
                             "\nPermohonan penarikan dana yang diterima setelah pukul 12.00 WIB akan diproses pada hari kerja berikutnya." +
                             "\nTerhadap penarikan dana yang lebih besar dari 100.000.000 (seratus juta) akan dijalankan setelah berhasil dikonfirmasi oleh pihak Customer Service.";
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Picker pickerRequestDateTmp = new Picker()
            {
                Title = "Choose Date",
                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 15
            };
            pickerRequestDateTmp.SelectedIndexChanged += PickerRequestDate_SelectedIndexChanged;
            pickerRequestDate = pickerRequestDateTmp;

            grdFundWithdrawal.Children.Add(pickerRequestDate, 1, 2);

            _observers.Add(Store.AthenaSession.Subscribe(athenaSession =>
            {
                RefreshPageAsync(false);
            }));
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();

            if (pickerRequestDate != null)
            {
                grdFundWithdrawal.Children.Remove(pickerRequestDate);
                pickerRequestDate.SelectedIndexChanged -= PickerRequestDate_SelectedIndexChanged;
                pickerRequestDate = null;
            }

            base.OnDisappearing();
        }

        private void PickerRequestDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pickerRequestDate.SelectedIndex >= 0 && wdFundInfoResponse != null && wdFundInfoResponse.recs.Count >= 0)
            {
                lblWithdrawableAmt.Text = wdFundInfoResponse.recs[pickerRequestDate.SelectedIndex].value.ToString("N0");
            }
            else
            {
                lblWithdrawableAmt.Text = "";
            }
            txtAmount.Text = "";
            txtInWords.Text = "";
        }

        private async Task<int> UpdatePageAsync()
        {
            try
            {
                if (Store.AthenaSession.GetState() != null &&
                       Store.AthenaSession.GetState().BrokerAccountId != null && Store.AthenaSession.GetState().BrokerAccountId is String strBrokerAccountId)
                {
                    bankAccount = await Store.AthenaSession.GetState().Client.GetBankAccountAsync(strBrokerAccountId, new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);
                    wdFundInfoResponse = await Store.AthenaSession.GetState().Client.GetWdFundInfoAsync(strBrokerAccountId, new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);

                    if (bankAccount == null) return 2;
                    if (wdFundInfoResponse == null) return 2;

                    lstDateString.Clear();
                    if (wdFundInfoResponse.recs != null && wdFundInfoResponse.recs.Count >= 0)
                    {
                        foreach (Rec rec in wdFundInfoResponse.recs)
                        {
                            string strDateTmp = "";
                            if (rec.Date.HasValue)
                            {
                                strDateTmp = $"{rec.Date.Value.ToString("dddd, dd MMM yyyy")}";
                                lstDateString.Add(strDateTmp);
                            }
                        }
                    }

                    pickerRequestDate.ItemsSource = lstDateString;

                    //rdn
                    lblBankAccountHolderName.Text = bankAccount.RdnAccountHolderName;
                    lblBankAccountNumber.Text = bankAccount.RdnName + " " + bankAccount.RdnAccountNumber;

                    //bank
                    //lblBankName.Text = bankAccount.BankName;
                    lblAccountHolder.Text = bankAccount.BankAccountHolderName;
                    lblBankAccount.Text = bankAccount.BankName + " " + bankAccount.BankAccountNumber;

                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (OperationCanceledException)
            {
                return 3;
            }
            catch
            {
                return 4;
            }
            finally
            {

            }
        }

        public void ClearUi()
        {
            pickerRequestDate.SelectedIndex = -1;
            txtAmount.Text = "";
            txtInWords.Text = "";
            lblWithdrawableAmt.Text = "";

            if (Store.AthenaSession.GetState() == null || Store.AthenaSession.GetState().BrokerAccountId == null)
            {
                pickerRequestDate.ItemsSource = null;

                //rdn
                lblBankAccountHolderName.Text = "";
                lblBankAccountNumber.Text = "";

                //bank
                //lblBankName.Text = "";
                lblAccountHolder.Text = "";
                lblBankAccount.Text = "";
            }
        }

        public async void RefreshPageAsync(bool showMessage = true)
        {
            ClearUi();
            if (Store.AthenaSession.GetState() != null &&
                    Store.AthenaSession.GetState().BrokerAccountId != null)
            {
                lastRefresh = DateTime.Now;
                int result = await UpdatePageAsync();
                if (result == 1)
                {
                    if (showMessage)
                    {
                        DependencyService.Get<IToastMessage>().Show("Customer Info Loaded");
                        return;
                    }
                }
                else if (result == 2)
                {
                    DependencyService.Get<IToastMessage>().Show("Load Failed.");
                }
                else if (result == 3)
                {
                    if (DateTime.Now.CompareTo(lastRefresh.AddSeconds(10)) >= 0)
                    {
                        DependencyService.Get<IToastMessage>().Show("Request timeout.");
                    }
                }
                else if (result == 4)
                {
                    DependencyService.Get<IToastMessage>().Show("Request failed.");
                }

            }
        }

        private void BtnRefresh_Clicked(object sender, EventArgs e)
        {
            RefreshPageAsync();
        }

        private async void SwitchAccountButton_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
        }

        private void btnAllAmount_Clicked(object sender, EventArgs e)
        {
            if (pickerRequestDate.SelectedIndex >= 0 && wdFundInfoResponse != null && wdFundInfoResponse.recs.Count >= 0)
            {
                txtAmount.Text = wdFundInfoResponse.recs[pickerRequestDate.SelectedIndex].value.ToString();
            }
            else
            {
                txtAmount.Text = "";
            }
        }

        private async void btnExecute_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0) await PopupNavigation.Instance.PopAllAsync();
                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null) return;

            if (pickerRequestDate.SelectedIndex == -1)
            {
                DependencyService.Get<IToastMessage>().Show("Select Date!");
                return;
            }

            if (string.IsNullOrEmpty(txtAmount.Text))
            {
                DependencyService.Get<IToastMessage>().Show("Input Requested Amount!");
                return;
            }

            if (decimal.TryParse(txtAmount.Text, out decimal amount) && amount > 0)
            {
                if (amount > wdFundInfoResponse.recs[pickerRequestDate.SelectedIndex].value)
                {
                    DependencyService.Get<IToastMessage>().Show("Requested Amount cannot higher than Withdrawable Amount!");
                    return;
                }
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Invalid Requested Amount!");
                return;
            }

            if (string.IsNullOrEmpty(txtInWords.Text))
            {
                DependencyService.Get<IToastMessage>().Show("Input In Words Amount!");
                return;
            }

            DateTime selectedDateTime = wdFundInfoResponse.recs[pickerRequestDate.SelectedIndex].Date.Value;

            CultureInfo usProvider = new CultureInfo("en-US");
            FundWithdrawalConfirmationNew fundWithdrawalConfirmation = new FundWithdrawalConfirmationNew(bankAccount, Store.AthenaSession.GetState().BrokerAccountId, "", selectedDateTime, decimal.Parse(txtAmount.Text, System.Globalization.NumberStyles.Any, usProvider), txtInWords.Text, this);
            await PopupNavigation.Instance.PushAsync(fundWithdrawalConfirmation);
        }

    }
}