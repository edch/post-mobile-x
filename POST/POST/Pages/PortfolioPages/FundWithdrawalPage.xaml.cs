﻿using POST.DataSources.AthenaMessages.Responses;
using POST.Dialogs;
using POST.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.PortfolioPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FundWithdrawalPage : ContentPage
    {
        private DateTime lastRefresh = DateTime.Now;
        private readonly List<DateTime> lstDate;
        private readonly List<string> lstDateString;
        private readonly List<string> lstAccountId;
        private BankAccount bankAccount = null;
        private Picker pickerAccountId = null;

        public FundWithdrawalPage()
        {
            InitializeComponent();

            lstDate = new List<DateTime>();
            lstDateString = new List<string>();
            lstAccountId = new List<string>();
            short iDate = 1;
            while (iDate <= 14)
            {
                DateTime dateTmp = DateTime.Now.AddDays(iDate);
                lstDate.Add(dateTmp);
                lstDateString.Add(dateTmp.ToString("dddd, dd MMM yyyy"));
                iDate++;
            }

            lblNotice.Text = "Permohonan penarikan dana ini akan diproses apabila saldo di Rekening Dana Nasabah mencukupi. " +
                             "Permohonan penarikan dana harus diterima selambat-lambatnya pukul 12:00 WIB pada satu hari kerja sebelumnya (T-1) dan akan dijalankan setelah Customer Service berhasil melakukan konfirmasi sebelum tanggal penarikan dana." +
                             "\nKonfirmasi via telepon akan dilakukan untuk jumlah penarikan dana di atas Rp.100.000.000,-(seratus juta rupiah).\n\n" +
                             "This cash withdrawal request will be processed if balance in Invetor Account is available. " +
                             "Fund withdrawal request must be received before 12:00 WIB, at least one working day before (T-1) and will proceed after Customer Service able to confirm before fund withdrawal date." +
                             "\nPhone confirmation will be made for the withdrawal amount above IDR 100,000,000 (one hundred million rupiahs).";
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            pickerRequestDate.ItemsSource = lstDateString;

            Picker pickerAccountIdTmp = new Picker()
            {
                Title = "Choose Account",
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 15
            };
            pickerAccountIdTmp.SelectedIndexChanged += PickerAccountIdTmp_SelectedIndexChanged;
            pickerAccountId = pickerAccountIdTmp;

            grdFundWithdrawal.Children.Add(pickerAccountIdTmp, 1, 0);

            RefreshPageAsync(false);
        }

        private async void PickerAccountIdTmp_SelectedIndexChanged(object sender, EventArgs e)
        {
            await UpdateDetailAccountAsync();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            if (pickerAccountId != null)
            {
                grdFundWithdrawal.Children.Remove(pickerAccountId);
                pickerAccountId.SelectedIndexChanged -= PickerAccountIdTmp_SelectedIndexChanged;
                pickerAccountId = null;
            }
        }

        private async Task<int> UpdateAccountAsync()
        {
            try
            {
                TradingAccount[] response = await Store.AthenaSession.GetState().Client.GetTradingAccountsAsync(new CancellationTokenSource(TimeSpan.FromSeconds(10)).Token);
                if (response != null)
                {
                    lstAccountId.Clear();
                    for (int i = 0; i < response.Count(); i++)
                    {
                        lstAccountId.Add(response[i].BrokerAccountId);
                    }
                    pickerAccountId.ItemsSource = lstAccountId;
                    if (lstAccountId.Count > 0)
                    {
                        pickerAccountId.SelectedIndex = 0;
                    }

                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            catch (OperationCanceledException)
            {
                return 3;
            }
            catch
            {
                return 4;
            }
            finally
            {
            }
        }

        private async Task UpdateDetailAccountAsync()
        {
            try
            {
                bankAccount = await Store.AthenaSession.GetState().Client.GetBankAccountAsync(pickerAccountId.SelectedItem.ToString(), new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);

                if (bankAccount == null)
                {
                    DependencyService.Get<IToastMessage>().Show("Load Failed");
                }
                else
                {
                    //rdn
                    lblBankAccountHolderName.Text = bankAccount.RdnAccountHolderName;
                    lblBankAccountNumber.Text = bankAccount.RdnAccountNumber;

                    //bank
                    lblBankName.Text = bankAccount.BankName;
                    lblAccountHolder.Text = bankAccount.BankAccountHolderName;
                    lblBankAccount.Text = bankAccount.BankAccountNumber + " (" + bankAccount.BankName + ")";
                }
            }
            catch (Exception)
            {
                DependencyService.Get<IToastMessage>().Show("Load Failed");
                bankAccount = null;
            }
            finally
            {

            }
        }

        private async void btnExecute_Clicked(object sender, EventArgs e)
        {
            if (pickerAccountId.SelectedIndex == -1)
            {
                DependencyService.Get<IToastMessage>().Show("Select Account ID!");
                return;
            }
            if (string.IsNullOrEmpty(entryPin.Text))
            {
                DependencyService.Get<IToastMessage>().Show("Input PIN!");
                return;
            }
            if (pickerRequestDate.SelectedIndex == -1)
            {
                DependencyService.Get<IToastMessage>().Show("Select Date!");
                return;
            }
            if (string.IsNullOrEmpty(txtAmount.Text))
            {
                DependencyService.Get<IToastMessage>().Show("Input Amount!");
                return;
            }
            if (string.IsNullOrEmpty(txtInWords.Text))
            {
                DependencyService.Get<IToastMessage>().Show("Input In Words Amount!");
                return;
            }

            DateTime selectedDateTime = lstDate[pickerRequestDate.SelectedIndex];

            CultureInfo usProvider = new CultureInfo("en-US");
            FundWithdrawalConfirmation fundWithdrawalConfirmation = new FundWithdrawalConfirmation(bankAccount, pickerAccountId.SelectedItem.ToString(), entryPin.Text, selectedDateTime, decimal.Parse(txtAmount.Text, System.Globalization.NumberStyles.Any, usProvider), txtInWords.Text, this);
            await PopupNavigation.Instance.PushAsync(fundWithdrawalConfirmation);
        }

        public void ClearInput()
        {
            entryPin.Text = "";
            pickerRequestDate.SelectedIndex = -1;
            txtAmount.Text = "";
            txtInWords.Text = "";
        }

        public async void RefreshPageAsync(bool showMessage = true)
        {
            lastRefresh = DateTime.Now;
            int result = await UpdateAccountAsync();
            if (result == 1)
            {
                if (showMessage)
                {
                    DependencyService.Get<IToastMessage>().Show("Account ID Loaded.");
                }
            }
            else if (result == 2)
            {
                DependencyService.Get<IToastMessage>().Show("Null Data.");
            }
            else if (result == 3)
            {
                if (DateTime.Now.CompareTo(lastRefresh.AddSeconds(10)) >= 0)
                {
                    DependencyService.Get<IToastMessage>().Show("Request timeout.");
                }
            }
            else if (result == 4)
            {
                DependencyService.Get<IToastMessage>().Show("Request failed.");
            }

        }

        private void BtnRefresh_Clicked(object sender, EventArgs e)
        {
            RefreshPageAsync();
        }
    }
}