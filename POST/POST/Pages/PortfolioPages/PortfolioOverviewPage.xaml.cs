﻿using POST.Constants;
using POST.Converters;
using POST.DataSources.AthenaMessages.Responses;
using POST.Dialogs;
using POST.Models;
using POST.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.PortfolioPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PortfolioOverviewPage : ContentPage
    {
        private readonly OrderOfMagnitudeConverter orderOfMagnitudeConverter = new OrderOfMagnitudeConverter();

        private const int sizePerChar = 7;
        private const int nMinAvg = 30;
        private const int nMinLast = 40;
        private const int nMinNetLot = 60;
        private const int nMinBalanceLot = 80;
        private const int nMinshares = 60;
        private const int nMinGain = 30;
        private const int nMinPercentGain = 10;
        private const int nMinValue = 50;

        private class ViewModel
        {
            public string BrokerAccountId { get; set; }
            public string AccountName { get; set; }
            public string RDN { get; set; }
            public decimal StockValue { get; set; }
            public decimal TradingLimit { get; set; }
            public decimal PreviousBalance { get; set; }
            public decimal NetToday { get; set; }
            public decimal EndBalance { get; set; }
            public decimal IssuedPoin { get; set; }
            public decimal CallRatio { get; set; }
            public decimal CashOnT0 { get; set; }
            public decimal CashOnT1 { get; set; }
            public decimal CashOnT2 { get; set; }
            public decimal AssetValue { get; set; }

            public decimal GainLossTotal { get; set; }

            public Color GainLossTotalForeground
            {
                get
                {
                    if (GainLossTotal > 0m)
                    {
                        return Colors.GainForeground;
                    }
                    else if (GainLossTotal < 0m)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
            }

            public Color TradingLimitForeground
            {
                get
                {
                    if (TradingLimit > 0m)
                    {
                        return Colors.GainForeground;
                    }
                    else if (TradingLimit < 0m)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
            }

            public Color PreviousBalanceForeground
            {
                get
                {
                    if (PreviousBalance > 0m)
                    {
                        return Colors.GainForeground;
                    }
                    else if (PreviousBalance < 0m)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
            }

            public Color NetTodayForeground
            {
                get
                {
                    if (NetToday > 0m)
                    {
                        return Colors.GainForeground;
                    }
                    else if (NetToday < 0m)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
            }

            public Color EndBalanceForeground
            {
                get
                {
                    if (EndBalance > 0m)
                    {
                        return Colors.GainForeground;
                    }
                    else if (EndBalance < 0m)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
            }

            public Color CallRatioForeground
            {
                get
                {
                    if (CallRatio > 0m)
                    {
                        return Colors.GainForeground;
                    }
                    else if (CallRatio < 0m)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
            }

            public Color CashOnT0Foreground
            {
                get
                {
                    if (CashOnT0 > 0m)
                    {
                        return Colors.GainForeground;
                    }
                    else if (CashOnT0 < 0m)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
            }

            public Color CashOnT1Foreground
            {
                get
                {
                    if (CashOnT1 > 0m)
                    {
                        return Colors.GainForeground;
                    }
                    else if (CashOnT1 < 0m)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
            }

            public Color CashOnT2Foreground
            {
                get
                {
                    if (CashOnT2 > 0m)
                    {
                        return Colors.GainForeground;
                    }
                    else if (CashOnT2 < 0m)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
            }

            public Color AssetValueForeground
            {
                get
                {
                    if (AssetValue > 0m)
                    {
                        return Colors.GainForeground;
                    }
                    else if (AssetValue < 0m)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
            }

            public Color StockValueForeground
            {
                get
                {
                    if (StockValue > 0m)
                    {
                        return Colors.GainForeground;
                    }
                    else if (StockValue < 0m)
                    {
                        return Colors.LoseForeground;
                    }
                    else
                    {
                        return Colors.UnchangedForeground;
                    }
                }
            }

            public List<PortfolioStock> StockBalance { get; set; }
        }

        private enum ColumnSort
        {
            ASC,
            DSC
        }

        private ColumnSort columnSort = ColumnSort.ASC;
        private readonly View ViewProgressDisplay;

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public PortfolioOverviewPage()
        {
            InitializeComponent();

            ViewProgressDisplay = CreateProgressDisplay();
            GridMain.Children.Add(ViewProgressDisplay, 0, 1);

            CreateDataGrid();

            horizontalScrollView.Scrolled += async (sender, args) =>
            {
                await headerScrollView.ScrollToAsync(args.ScrollX, 0.0, false);
            };

            headerScrollView.Scrolled += async (sender, args) =>
            {
                await horizontalScrollView.ScrollToAsync(args.ScrollX, 0.0, false);
            };
        }

        private class GridSortSign : Grid
        {
            public ColumnSort columnSort = ColumnSort.ASC;
            private readonly Label lblSortForeSign;
            private readonly Label lblSortBackgroundSign;
            public GridSortSign()
            {
                lblSortBackgroundSign = new Label()
                {
                    Text = Glyphs.BackgroundSort,
                    TextColor = Colors.NetralSortColor,
                    FontSize = 12,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 0, -2),
                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                    HorizontalOptions = LayoutOptions.Start
                };
                lblSortForeSign = new Label()
                {
                    Text = Glyphs.Unchanged,
                    TextColor = Colors.AscDescSortColor,
                    FontSize = 11,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 0, -2),
                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                    HorizontalOptions = LayoutOptions.Start
                };
                Children.Add(lblSortBackgroundSign, 0, 0);
                Children.Add(lblSortForeSign, 0, 0);
            }

            public void SetAsc()
            {
                lblSortForeSign.Text = Glyphs.Gaining;
            }

            public void SetDesc()
            {
                lblSortForeSign.Text = Glyphs.Losing;
            }

            public void SetNetral()
            {
                lblSortForeSign.Text = Glyphs.Unchanged;
            }
        }

        private readonly List<GridSortSign> lstGridSortSign = new List<GridSortSign>();

        private void ResetColumnHeader()
        {
            lstGridSortSign.ForEach(grd => grd.SetNetral());
        }

        private CustomGrid ToFirstHeader(string title)
        {
            Label lblTitle = new Label()
            {
                Text = title,
                Style = (Style)Resources["HeaderLabelStyle"],
                HorizontalOptions = LayoutOptions.End
            };

            GridSortSign gridSortSign = new GridSortSign();
            lstGridSortSign.Add(gridSortSign);

            CustomGrid customGrid = new CustomGrid()
            {
                Margin = new Thickness(6, 0, 0, 0),
                YColumn = 0,
                WidthRequest = 60,
                HorizontalOptions = LayoutOptions.Start,
                ColumnSpacing = 1,
                ColumnDefinitions = new ColumnDefinitionCollection() { new ColumnDefinition { Width = GridLength.Auto }, new ColumnDefinition { Width = GridLength.Auto } },
                Children = {
                           lblTitle,
                           gridSortSign
                        }
            };

            Grid.SetRow(lblTitle, 0);
            Grid.SetColumn(lblTitle, 0);
            Grid.SetRow(gridSortSign, 0);
            Grid.SetColumn(gridSortSign, 1);

            customGrid.GestureRecognizers.Add(
                new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        if (BindingContext != null && (ViewModel)BindingContext is ViewModel vm)
                        {
                            ResetColumnHeader();

                            if (columnSort == ColumnSort.ASC)
                            {
                                columnSort = ColumnSort.DSC;
                                gridSortSign.SetDesc();
                            }
                            else
                            {
                                columnSort = ColumnSort.ASC;
                                gridSortSign.SetAsc();
                            }

                            List<PortfolioStock> lstPortfolioStock = vm.StockBalance;
                            if (columnSort == ColumnSort.ASC)
                            {
                                lstPortfolioStock = lstPortfolioStock.OrderBy(ps => ps.StockCode).ToList();
                            }
                            else
                            {
                                lstPortfolioStock = lstPortfolioStock.OrderByDescending(ps => ps.StockCode).ToList();
                            }
                            vm.StockBalance = lstPortfolioStock;
                            UpdateDataGrid(lstPortfolioStock);
                        }
                    })
                });

            return customGrid;
        }

        private CustomGrid ToHeader(string title, int yColumn)
        {
            Label lblTitle = new Label()
            {
                Text = title,
                Style = (Style)Resources["HeaderLabelStyle"]
            };
            GridSortSign gridSortSign = new GridSortSign();
            lstGridSortSign.Add(gridSortSign);

            CustomGrid customGrid = new CustomGrid()
            {
                HorizontalOptions = LayoutOptions.End,
                YColumn = yColumn,
                ColumnSpacing = 1,
                ColumnDefinitions = new ColumnDefinitionCollection() { new ColumnDefinition { Width = GridLength.Auto }, new ColumnDefinition { Width = GridLength.Auto } },
                Children = {
                            lblTitle,
                           gridSortSign
                        }
            };

            Grid.SetRow(lblTitle, 0);
            Grid.SetColumn(lblTitle, 0);
            Grid.SetRow(gridSortSign, 0);
            Grid.SetColumn(gridSortSign, 1);

            customGrid.GestureRecognizers.Add(
                new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        if (BindingContext != null && (ViewModel)BindingContext is ViewModel vm)
                        {
                            ResetColumnHeader();

                            if (columnSort == ColumnSort.ASC)
                            {
                                columnSort = ColumnSort.DSC;
                                gridSortSign.SetDesc();
                            }
                            else
                            {
                                columnSort = ColumnSort.ASC;
                                gridSortSign.SetAsc();
                            }

                            List<PortfolioStock> lstPortfolioStock = vm.StockBalance;
                            switch (yColumn)
                            {
                                case 0:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderBy(ps => ps.StockCode).ToList();
                                    }
                                    else
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderByDescending(ps => ps.StockCode).ToList();
                                    }
                                    break;

                                case 1:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderBy(ps => ps.Average).ToList();
                                    }
                                    else
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderByDescending(ps => ps.Average).ToList();
                                    }
                                    break;

                                case 2:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderBy(ps => ps.LastPrice).ToList();
                                    }
                                    else
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderByDescending(ps => ps.LastPrice).ToList();
                                    }
                                    break;

                                case 3:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderBy(ps => ps.AvailableVolumeLot).ToList();
                                    }
                                    else
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderByDescending(ps => ps.AvailableVolumeLot).ToList();
                                    }
                                    break;

                                case 4:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderBy(ps => ps.VolumeLot).ToList();
                                    }
                                    else
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderByDescending(ps => ps.VolumeLot).ToList();
                                    }
                                    break;

                                case 5:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderBy(ps => ps.Volume).ToList();
                                    }
                                    else
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderByDescending(ps => ps.Volume).ToList();
                                    }
                                    break;

                                case 6:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderBy(ps => ps.Gain).ToList();
                                    }
                                    else
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderByDescending(ps => ps.Gain).ToList();
                                    }
                                    break;

                                case 7:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderBy(ps => ps.GainPercent).ToList();
                                    }
                                    else
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderByDescending(ps => ps.GainPercent).ToList();
                                    }
                                    break;

                                case 8:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderBy(ps => ps.TotalValue).ToList();
                                    }
                                    else
                                    {
                                        lstPortfolioStock = lstPortfolioStock.OrderByDescending(ps => ps.TotalValue).ToList();
                                    }
                                    break;
                            }
                            vm.StockBalance = lstPortfolioStock;
                            UpdateDataGrid(lstPortfolioStock);
                        }
                    })
                });

            return customGrid;
        }

        private void CreateDataGrid()
        {
            rootGrid.Children.Add(ToFirstHeader("Stock"), 0, 0);

            //Avg
            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinAvg) });
            headerGrid.Children.Add(ToHeader("Avg", 1), headerGrid.Children.Count, 0);
            //Last
            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinLast) });
            headerGrid.Children.Add(ToHeader("Last", 2), headerGrid.Children.Count, 0);
            //Net(Lot)
            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinNetLot) });
            headerGrid.Children.Add(ToHeader("Net(Lot)", 3), headerGrid.Children.Count, 0);
            //Balance(Lot)
            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinBalanceLot) });
            headerGrid.Children.Add(ToHeader("Balance(Lot)", 4), headerGrid.Children.Count, 0);
            //Shares
            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinshares) });
            headerGrid.Children.Add(ToHeader("Shares", 5), headerGrid.Children.Count, 0);
            //+/-
            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinGain) });
            headerGrid.Children.Add(ToHeader("+/-", 6), headerGrid.Children.Count, 0);
            //%
            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinPercentGain) });
            headerGrid.Children.Add(ToHeader("%", 7), headerGrid.Children.Count, 0);
            //Value
            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinValue) });
            headerGrid.Children.Add(ToHeader("Value", 8), headerGrid.Children.Count, 0);

            //Stock
            RowHeaders.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60) });
            RowHeaders.Children.Add(new Grid(), RowHeaders.Children.Count, 0);

            //Avg
            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinAvg) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);
            //Last
            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinLast) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);
            //Net(Lot)
            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinNetLot) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);
            //Balance(Lot)
            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinBalanceLot) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);
            //Shares
            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinshares) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);
            //+/-
            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinGain) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);
            //%
            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinPercentGain) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);
            //Value
            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(nMinValue) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);

        }

        private class CustomGrid : Grid
        {
            public int XRow = 0;
            public int YColumn = 0;
        }

        private int UpdateColumn(Grid grid, string[] arrStr)
        {
            int nChar = 0;
            int iRow = 0;

            if (grid.RowDefinitions == null)
            {
                grid.RowDefinitions = new RowDefinitionCollection();
            }

            Label lblStockCode = null;

            foreach (string str in arrStr)
            {
                if (str.Length > nChar) nChar = str.Length;
                if (grid.RowDefinitions.Count <= iRow)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = 30 });

                    lblStockCode = new Label()
                    {
                        Text = str,
                        TextColor = Colors.FieldValueDefaultColor,
                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                        VerticalTextAlignment = TextAlignment.Center,
                        FontSize = 12,
                        HorizontalTextAlignment = TextAlignment.Start,
                        LineBreakMode = LineBreakMode.WordWrap
                    };
                    Grid.SetColumn(lblStockCode, 1);


                    CustomGrid customGrid = new CustomGrid()
                    {
                        ColumnSpacing = 1,
                        ColumnDefinitions = new ColumnDefinitionCollection() { new ColumnDefinition() { Width = 9 }, new ColumnDefinition() { Width = GridLength.Star } },
                        XRow = iRow,
                        Children = {
                            lblStockCode
                        }
                    };

                    customGrid.GestureRecognizers.Add(
                        new TapGestureRecognizer
                        {
                            Command = new Command(async () =>
                            {
                                if (BindingContext != null && (ViewModel)BindingContext is ViewModel vm)
                                {
                                    PortfolioStock portfolioStock = vm.StockBalance[customGrid.XRow];
                                    decimal price = portfolioStock.LastPrice;

                                    if (portfolioStock.Stock != null)
                                    {
                                        Store.RecentStockIds.AddStock(portfolioStock.Stock.Id);
                                        PortfolioStockActionDialog portfolioStockActionDialog = new PortfolioStockActionDialog(price, portfolioStock.AvailableVolumeLot);
                                        portfolioStockActionDialog.SetStock(portfolioStock.Stock.Id);

                                        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                        {
                                            await PopupNavigation.Instance.PopAllAsync();
                                        }

                                        await PopupNavigation.Instance.PushAsync(portfolioStockActionDialog);
                                    }
                                    else
                                    {
                                        DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
                                    }
                                }
                            })
                        });
                    grid.Children.Add(customGrid, 0, iRow);
                }
                else
                {
                    lblStockCode = ((Label)((CustomGrid)grid.Children[iRow]).Children[0]);
                    lblStockCode.Text = str;
                }

                //Syariah & SpecialNotation
                if (Store.StockById.GetState().TryGetValue(new StockId(str, MarketType.RG), out Stock stock2))
                {
                    lblStockCode.TextDecorations = stock2.TextDecoration;

                    if (string.IsNullOrEmpty(stock2.SpecialNotation2Long))
                    {
                        if (((CustomGrid)grid.Children[iRow]).Children.Count > 1)
                        {
                            ((CustomGrid)grid.Children[iRow]).Children.RemoveAt(1);
                        }
                    }
                    else
                    {
                        if (((CustomGrid)grid.Children[iRow]).Children.Count <= 1)
                        {
                            Grid gridSpecialNotationMark = new Grid()
                            {
                                WidthRequest = 9,
                                Padding = new Thickness(0),
                                VerticalOptions = LayoutOptions.Center,
                                HorizontalOptions = LayoutOptions.Start,
                                Margin = new Thickness(0, 7, 0, 7),
                                Children =
                                {
                                    new Frame()
                                    {
                                        Padding = new Thickness(0),
                                        Margin = new Thickness(0),
                                        BorderColor = Colors.SpecialNotationForegroundColor,
                                        BackgroundColor = Colors.SpecialNotationBackgroundColor
                                    },
                                    new Label()
                                    {
                                        Padding = new Thickness(0),
                                        Margin = new Thickness(0),
                                        Text = "!",
                                        TextColor = Colors.SpecialNotationForegroundColor,
                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProBold"],
                                        VerticalTextAlignment = TextAlignment.Center,
                                        FontSize = 12,
                                        HorizontalTextAlignment= TextAlignment.Center,
                                    }
                                }
                            };
                            Grid.SetColumn(gridSpecialNotationMark, 0);
                            ((CustomGrid)grid.Children[iRow]).Children.Insert(1, gridSpecialNotationMark);
                        }
                    }
                }
                iRow++;
            }

            int nRest = grid.RowDefinitions.Count - iRow;
            for (int i = 0; i < nRest; i++)
            {
                int n = grid.RowDefinitions.Count - 1;
                grid.Children.RemoveAt(n);
                grid.RowDefinitions.RemoveAt(n);
            }

            return nChar;
        }

        private int UpdateColumn(Grid grid, string[] arrStr, Color[] arrTextColor)
        {
            int nChar = 0;
            int iRow = 0;

            if (grid.RowDefinitions == null)
            {
                grid.RowDefinitions = new RowDefinitionCollection();
            }

            foreach (string str in arrStr)
            {
                if (str.Length > nChar) nChar = str.Length;
                if (grid.RowDefinitions.Count <= iRow)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = 30 });

                    CustomGrid customGrid = new CustomGrid()
                    {
                        XRow = iRow,
                        Children = {
                           new Label
                    {
                        Text = str,
                        TextColor = arrTextColor[iRow],
                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                        VerticalOptions = LayoutOptions.Center,
                        FontSize = 12,
                        HorizontalOptions = LayoutOptions.End,
                        LineBreakMode = LineBreakMode.TailTruncation,
                    }
                        }
                    };
                    customGrid.GestureRecognizers.Add(
                        new TapGestureRecognizer
                        {
                            Command = new Command(async () =>
                            {
                                if (BindingContext != null && (ViewModel)BindingContext is ViewModel vm)
                                {
                                    PortfolioStock portfolioStock = vm.StockBalance[customGrid.XRow];
                                    decimal price = portfolioStock.LastPrice;

                                    if (portfolioStock.Stock != null)
                                    {
                                        Store.RecentStockIds.AddStock(portfolioStock.Stock.Id);
                                        PortfolioStockActionDialog portfolioStockActionDialog = new PortfolioStockActionDialog(price, portfolioStock.AvailableVolumeLot);
                                        portfolioStockActionDialog.SetStock(portfolioStock.Stock.Id);

                                        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                        {
                                            await PopupNavigation.Instance.PopAllAsync();
                                        }

                                        await PopupNavigation.Instance.PushAsync(portfolioStockActionDialog);
                                    }

                                    else
                                    {
                                        DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
                                    }
                                }
                            })
                        });
                    grid.Children.Add(customGrid, 0, iRow);
                }
                else
                {
                    ((Label)((CustomGrid)grid.Children[iRow]).Children[0]).Text = str;
                    ((Label)((CustomGrid)grid.Children[iRow]).Children[0]).TextColor = arrTextColor[iRow];
                }
                iRow++;
            }

            int nRest = grid.RowDefinitions.Count - iRow;
            for (int i = 0; i < nRest; i++)
            {
                int n = grid.RowDefinitions.Count - 1;
                grid.Children.RemoveAt(n);
                grid.RowDefinitions.RemoveAt(n);
            }

            return nChar;
        }

        private int UpdateColumn(Grid grid, decimal[] arrValue, string strFormat, Color[] arrTextColor)
        {
            int nChar = 0;
            int iRow = 0;

            if (grid.RowDefinitions == null)
            {
                grid.RowDefinitions = new RowDefinitionCollection();
            }

            foreach (decimal val in arrValue)
            {
                if (val.ToString(strFormat).Length > nChar) nChar = val.ToString(strFormat).Length;
                if (grid.RowDefinitions.Count <= iRow)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = 30 });
                    CustomGrid customGrid = new CustomGrid()
                    {
                        XRow = iRow,
                        Children = {
                           new Label
                    {
                        Text = val.ToString(strFormat),
                        TextColor = arrTextColor[iRow],
                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                        VerticalOptions = LayoutOptions.Center,
                        FontSize = 12,
                        HorizontalOptions = LayoutOptions.End,
                        LineBreakMode = LineBreakMode.TailTruncation,
                    }
                        }
                    };

                    customGrid.GestureRecognizers.Add(
                        new TapGestureRecognizer
                        {
                            Command = new Command(async () =>
                            {
                                if (BindingContext != null && (ViewModel)BindingContext is ViewModel vm)
                                {
                                    PortfolioStock portfolioStock = vm.StockBalance[customGrid.XRow];
                                    decimal price = portfolioStock.LastPrice;

                                    if (portfolioStock.Stock != null)
                                    {
                                        Store.RecentStockIds.AddStock(portfolioStock.Stock.Id);
                                        PortfolioStockActionDialog portfolioStockActionDialog = new PortfolioStockActionDialog(price, portfolioStock.AvailableVolumeLot);
                                        portfolioStockActionDialog.SetStock(portfolioStock.Stock.Id);

                                        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                        {
                                            await PopupNavigation.Instance.PopAllAsync();
                                        }

                                        await PopupNavigation.Instance.PushAsync(portfolioStockActionDialog);
                                    }

                                    else
                                    {
                                        DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
                                    }
                                }
                            })
                        });
                    grid.Children.Add(customGrid, 0, iRow);
                }
                else
                {
                    ((Label)((CustomGrid)grid.Children[iRow]).Children[0]).Text = val.ToString(strFormat);
                    ((Label)((CustomGrid)grid.Children[iRow]).Children[0]).TextColor = arrTextColor[iRow];
                }
                iRow++;
            }

            int nRest = grid.RowDefinitions.Count - iRow;
            for (int i = 0; i < nRest; i++)
            {
                int n = grid.RowDefinitions.Count - 1;
                grid.Children.RemoveAt(n);
                grid.RowDefinitions.RemoveAt(n);
            }

            return nChar;
        }

        private int UpdateColumn(Grid grid, decimal[] arrValue, string strFormat)
        {
            int nChar = 0;
            int iRow = 0;

            if (grid.RowDefinitions == null)
            {
                grid.RowDefinitions = new RowDefinitionCollection();
            }

            foreach (decimal val in arrValue)
            {
                if (val.ToString(strFormat).Length > nChar) nChar = val.ToString(strFormat).Length;
                if (grid.RowDefinitions.Count <= iRow)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = 30 });
                    CustomGrid customGrid = new CustomGrid()
                    {
                        XRow = iRow,
                        Children = {
                           new Label
                    {
                        Text = val.ToString(strFormat),
                        TextColor = Colors.FieldValueDefaultColor,
                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                        VerticalOptions = LayoutOptions.Center,
                        FontSize = 12,
                        HorizontalOptions = LayoutOptions.End,
                        LineBreakMode = LineBreakMode.TailTruncation,
                    }
                        }
                    };

                    customGrid.GestureRecognizers.Add(
                        new TapGestureRecognizer
                        {
                            Command = new Command(async () =>
                            {
                                if (BindingContext != null && (ViewModel)BindingContext is ViewModel vm)
                                {
                                    PortfolioStock portfolioStock = vm.StockBalance[customGrid.XRow];
                                    decimal price = portfolioStock.LastPrice;

                                    if (portfolioStock.Stock != null)
                                    {
                                        Store.RecentStockIds.AddStock(portfolioStock.Stock.Id);
                                        PortfolioStockActionDialog portfolioStockActionDialog = new PortfolioStockActionDialog(price, portfolioStock.AvailableVolumeLot);
                                        portfolioStockActionDialog.SetStock(portfolioStock.Stock.Id);

                                        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                        {
                                            await PopupNavigation.Instance.PopAllAsync();
                                        }

                                        await PopupNavigation.Instance.PushAsync(portfolioStockActionDialog);
                                    }

                                    else
                                    {
                                        DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
                                    }
                                }
                            })
                        });
                    grid.Children.Add(customGrid, 0, iRow);
                }
                else
                {
                    ((Label)((CustomGrid)grid.Children[iRow]).Children[0]).Text = val.ToString(strFormat);
                }
                iRow++;
            }

            int nRest = grid.RowDefinitions.Count - iRow;
            for (int i = 0; i < nRest; i++)
            {
                int n = grid.RowDefinitions.Count - 1;
                grid.Children.RemoveAt(n);
                grid.RowDefinitions.RemoveAt(n);
            }

            return nChar;
        }

        private void UpdateDataGrid(List<PortfolioStock> StockBalance)
        {
            try
            {
                //Stock    
                UpdateColumn((Grid)RowHeaders.Children[0], StockBalance.Select(sb => sb.StockCode).ToArray());

                ////Avg
                int nAvg = UpdateColumn((Grid)columnsGrid.Children[0], StockBalance.Select(sb => sb.Average).ToArray(), "N0", StockBalance.Select(sb => sb.AvgForeground).ToArray());

                ////Last
                int nLast = UpdateColumn((Grid)columnsGrid.Children[1], StockBalance.Select(sb => sb.LastPrice).ToArray(), "N0", StockBalance.Select(sb => sb.LastPriceForeground).ToArray());

                ////Net(Lot)
                int nNetLot = UpdateColumn((Grid)columnsGrid.Children[2], StockBalance.Select(sb => sb.AvailableVolumeLot).ToArray(), "N0");

                ////Balance(Lot)
                int nBalanceLot = UpdateColumn((Grid)columnsGrid.Children[3], StockBalance.Select(sb => sb.VolumeLot).ToArray(), "N0");

                ////Shares
                int nShares = UpdateColumn((Grid)columnsGrid.Children[4], StockBalance.Select(sb => sb.Volume).ToArray(), "N0");

                decimal[] arrDecGain = StockBalance.Select(sb => sb.Gain).ToArray();
                string[] arrStrGain = arrDecGain.Select(g => orderOfMagnitudeConverter.Convert_NoZeroFraction(g, typeof(string), null, System.Globalization.CultureInfo.CurrentCulture).ToString()).ToArray();

                ////+/-
                int nGain = UpdateColumn((Grid)columnsGrid.Children[5], arrStrGain, StockBalance.Select(sb => sb.GainForeground).ToArray());

                ////%
                int nPercentGain = UpdateColumn((Grid)columnsGrid.Children[6], StockBalance.Select(sb => sb.GainPercent).ToArray(), "N2", StockBalance.Select(sb => sb.GainForeground).ToArray());

                ////Value
                int nValue = UpdateColumn((Grid)columnsGrid.Children[7], StockBalance.Select(sb => sb.TotalValue).ToArray(), "N0");

                //Avg
                nAvg = nAvg * sizePerChar;
                if (nAvg < nMinAvg) nAvg = nMinAvg;
                headerGrid.ColumnDefinitions[0].Width = nAvg;
                //Last
                nLast = nLast * sizePerChar;
                if (nLast < nMinLast) nLast = nMinLast;
                headerGrid.ColumnDefinitions[1].Width = nLast;
                //Net(Lot)
                nNetLot = nNetLot * sizePerChar;
                if (nNetLot < nMinNetLot) nNetLot = nMinNetLot;
                headerGrid.ColumnDefinitions[2].Width = nNetLot;
                //Balance(Lot)
                nBalanceLot = nBalanceLot * sizePerChar;
                if (nBalanceLot < nMinBalanceLot) nBalanceLot = nMinBalanceLot;
                headerGrid.ColumnDefinitions[3].Width = nBalanceLot;
                //Shares
                nShares = nShares * sizePerChar;
                if (nShares < nMinshares) nShares = nMinshares;
                headerGrid.ColumnDefinitions[4].Width = nShares;
                //+/-
                nGain = nGain * sizePerChar;
                if (nGain < nMinGain) nGain = nMinGain;
                headerGrid.ColumnDefinitions[5].Width = nGain;
                //%
                nPercentGain = nPercentGain * sizePerChar;
                if (nPercentGain < nMinPercentGain) nPercentGain = nMinPercentGain;
                headerGrid.ColumnDefinitions[6].Width = nPercentGain;
                //Value
                nValue = nValue * sizePerChar;
                if (nValue < nMinValue) nValue = nMinValue;
                headerGrid.ColumnDefinitions[7].Width = nValue;

                //Avg
                columnsGrid.ColumnDefinitions[0].Width = nAvg;
                //Last
                columnsGrid.ColumnDefinitions[1].Width = nLast;
                //Net(Lot)
                columnsGrid.ColumnDefinitions[2].Width = nNetLot;
                //Balance(Lot)
                columnsGrid.ColumnDefinitions[3].Width = nBalanceLot;
                //Shares
                columnsGrid.ColumnDefinitions[4].Width = nShares;
                //+/-
                columnsGrid.ColumnDefinitions[5].Width = nGain;
                //%
                columnsGrid.ColumnDefinitions[6].Width = nPercentGain;
                //Value
                columnsGrid.ColumnDefinitions[7].Width = nValue;

            }
            catch
            {

            }
        }

        public View CreateProgressDisplay()
        {
            Grid gridProgress = new Grid { BackgroundColor = (Color)App.Current.Resources["PageBackgroundColor"], Padding = new Thickness(50) };
            gridProgress.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            gridProgress.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            gridProgress.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //gridProgress.SetBinding(VisualElement.IsVisibleProperty, "IsWorking");
            ActivityIndicator activity = new ActivityIndicator
            {
                IsEnabled = true,
                IsVisible = true,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                IsRunning = true
            };
            gridProgress.Children.Add(activity, 0, 1);

            return gridProgress;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            ViewProgressDisplay.IsVisible = true;

            _observers.Add(Store.AthenaSession.Subscribe(async athenaSession =>
            {
                await UpdatePageAsync(athenaSession);
            }));
        }

        protected override void OnDisappearing()
        {
            ResetColumnHeader();
            columnSort = ColumnSort.ASC;

            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
            base.OnDisappearing();
        }

        private async Task<int> UpdatePageAsync(AthenaSession athenaSession)
        {
            ViewModel viewModel = BindingContext as ViewModel;
            if (athenaSession == null ||
                athenaSession.UseAppWithoutBrokerAccount || athenaSession.BrokerAccountId == null)
            {
                viewModel = null;
                BindingContext = null;
                ResetColumnHeader();
                UpdateDataGrid(new List<PortfolioStock>());
                ViewProgressDisplay.IsVisible = false;
                return 0;
            }
            else
            {
                try
                {
                    string isConsolidateResult = await Store.AthenaSession.GetState().Client.IsConsolidateAsync(new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);
                    if (isConsolidateResult == "1")
                    {
                        ViewProgressDisplay.IsVisible = false;
                        DependencyService.Get<IToastMessage>().Show("System currently on daily consolidation. Please try again later.");
                        return 5;
                    };

                    PortfolioAI1Response portfolioAI1Response = await Store.AthenaSession.GetState().Client.GetPortfolioAI1Async(athenaSession.BrokerAccountId, athenaSession.UserId, new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);

                    //CashBalance[] cashBalanceResponse = await Store.AthenaSession.GetState().Client.GetCashBalanceAsync(athenaSession.BrokerAccountId, new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);
                    //string cashFlowResponse = await Store.AthenaSession.GetState().Client.GetCashFlowAsync(athenaSession.BrokerAccountId, new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);
                    //List<CashFlow> lstCashFlows = await Store.AthenaSession.GetState().Client.GetPayableReceivableAsync(athenaSession.BrokerAccountId, new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);

                    CashBalance cashBalanceResponse = null;
                    cashBalanceResponse = new CashBalance
                        (
                         id: 0,
                         oltTl: portfolioAI1Response.OLT_TL,
                         name: portfolioAI1Response.Name,
                         currentCash: portfolioAI1Response.CurrentCash,
                         currentRatio: portfolioAI1Response.CurrentRatio,
                         sumStockValue: 0,
                         cashT3: portfolioAI1Response.cashT3,
                         todayTransaction: portfolioAI1Response.todayTrx,
                         points: 0
                        );

                    List<CashFlow> lstCashFlows = null;
                    lstCashFlows = new List<CashFlow>
                    {
                        new CashFlow
                            (
                             date: portfolioAI1Response.ARAP.T0Date,
                             amount: portfolioAI1Response.ARAP.T0AP + portfolioAI1Response.ARAP.T0AR,
                             due: 0
                            ){ },
                        new CashFlow
                            (
                             date: portfolioAI1Response.ARAP.T1Date,
                             amount: portfolioAI1Response.ARAP.T1AP + portfolioAI1Response.ARAP.T1AR,
                             due: 1
                            ){ },
                        new CashFlow
                            (
                             date: portfolioAI1Response.ARAP.T2Date,
                             amount: portfolioAI1Response.ARAP.T2AP + portfolioAI1Response.ARAP.T2AR,
                             due: 2
                            ){ },
                    };

                    if (cashBalanceResponse == null)
                    {
                        viewModel = null;
                        ViewProgressDisplay.IsVisible = false;
                        DependencyService.Get<IToastMessage>().Show("No Balance Data.");
                        return 2;
                    }
                    else
                    {
                        //TradingAccount tradingAccount = Store.TradingAccounts.GetState().Single(account => account.BrokerAccountId == athenaSession.BrokerAccountId);
                        //PortfolioStock[] stockBalanceResponse = await Store.AthenaSession.GetState().Client.GetStockBalanceAsync(athenaSession.BrokerAccountId, new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);

                        List<PortfolioStock> stockBalanceResponse = new List<PortfolioStock>();
                        if (portfolioAI1Response.portfStock != null)
                        {
                            foreach (PortfStock i in portfolioAI1Response.portfStock)
                            {
                                stockBalanceResponse.Add
                                    (
                                    new PortfolioStock
                                        (
                                        id: i.id,
                                        stockCode: i.stockCode,
                                        lastPrice: i.lastPrice,
                                        volume: i.volume,
                                        average: i.avg,
                                        availVolume: i.availVolume,
                                        gainLoss: i.gainLoss,
                                        haircut: i.haircutVal
                                        )
                                    );
                            }
                        }

                        cashBalanceResponse.SumStockValue = stockBalanceResponse.ToList().Sum(portfolioStock => portfolioStock.TotalValue);
                        cashBalanceResponse.CashOnT3 = cashBalanceResponse.CurrentCash + cashBalanceResponse.TodayTransaction;

                        viewModel = new ViewModel
                        {
                            BrokerAccountId = athenaSession.BrokerAccountId,
                            AccountName = cashBalanceResponse.Name,
                            RDN = "RDN: PANIN",
                            StockValue = cashBalanceResponse.SumStockValue,
                            TradingLimit = cashBalanceResponse.OltTl,
                            PreviousBalance = cashBalanceResponse.CurrentCash,
                            NetToday = cashBalanceResponse.TodayTransaction,
                            EndBalance = cashBalanceResponse.CashOnT3,
                            IssuedPoin = cashBalanceResponse.Points,
                            CallRatio = cashBalanceResponse.CurrentRatio,
                            CashOnT0 = lstCashFlows[0].Amount,
                            CashOnT1 = lstCashFlows[1].Amount,
                            CashOnT2 = lstCashFlows[2].Amount,
                            StockBalance = stockBalanceResponse.ToList(),
                            AssetValue = cashBalanceResponse.SumStockValue + cashBalanceResponse.CashOnT3,
                            GainLossTotal = stockBalanceResponse.ToList().Sum(portfolioStock => portfolioStock.Gain)
                            //CashOnT0 = cashBalanceResponse[0].CurrentCash + cashBalanceResponse.CashFlows.Where(cf => cf.Due <= 0).Sum(cf => cf.Amount),
                            //CashOnT1 = cashBalanceResponse[0].CurrentCash + cashBalanceResponse.CashFlows.Where(cf => cf.Due <= 1).Sum(cf => cf.Amount),
                            //CashOnT2 = cashBalanceResponse[0].CurrentCash + cashBalanceResponse.CashFlows.Where(cf => cf.Due <= 2).Sum(cf => cf.Amount)
                        };

                        BindingContext = viewModel;
                        ResetColumnHeader();
                        UpdateDataGrid(viewModel.StockBalance);
                        ViewProgressDisplay.IsVisible = false;
                        return 1;
                    }
                }
                catch (OperationCanceledException)
                {
                    ViewProgressDisplay.IsVisible = false;
                    DependencyService.Get<IToastMessage>().Show("Request timeout.");
                    return 3;
                }
                catch (Exception)
                {
                    viewModel = null;
                    ResetColumnHeader();
                    UpdateDataGrid(new List<PortfolioStock>());
                    ViewProgressDisplay.IsVisible = false;
                    DependencyService.Get<IToastMessage>().Show("Load Failed");
                    return 4;
                }
                finally
                {
                }
            }

        }

        private async void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is PortfolioStock portfolioStock)
            {
                decimal price = portfolioStock.LastPrice;

                if (portfolioStock.Stock != null)
                {
                    Store.RecentStockIds.AddStock(portfolioStock.Stock.Id);
                    PortfolioStockActionDialog portfolioStockActionDialog = new PortfolioStockActionDialog(price, portfolioStock.AvailableVolumeLot);
                    portfolioStockActionDialog.SetStock(portfolioStock.Stock.Id);

                    if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                    {
                        await PopupNavigation.Instance.PopAllAsync();
                    }

                    await PopupNavigation.Instance.PushAsync(portfolioStockActionDialog);
                }
                else
                {
                    DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
                }
            }
            if (sender is ListView listView)
            {
                listView.SelectedItem = null;
            }
        }

        public async void RefreshPageAsync()
        {
            if (Store.AthenaSession.GetState() != null &&
                    Store.AthenaSession.GetState().BrokerAccountId != null)
            {
                int result = await UpdatePageAsync(Store.AthenaSession.GetState());
                if (result == 1)
                {
                    DependencyService.Get<IToastMessage>().Show("Portfolio Loaded.");
                }
            }
            else
            {
                BindingContext = null;
            }
        }

        private async void SwitchAccountButton_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
        }

        private void BtnRefresh_Clicked(object sender, EventArgs e)
        {
            RefreshPageAsync();
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}