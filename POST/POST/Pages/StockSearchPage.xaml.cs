﻿using POST.Constants;
using POST.Dialogs;
using POST.Models;
using POST.Pages.AlertPages;
using POST.Pages.OrdersPages;
using POST.Stores;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    internal class StockSearchingTemplateSelector : DataTemplateSelector
    {
        public DataTemplate StockSearchingTemplate { get; set; }
        public DataTemplate StockRecentSearchingTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (item is Stock stock && stock.IsRecent)
            {
                return StockRecentSearchingTemplate;
            }

            return StockSearchingTemplate;
        }
    }

    public partial class StockSearchPage : ContentPage
    {
        private readonly StockSearchFilterStore StockSearchFilterStore = new StockSearchFilterStore();

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        //public ObservableSet<StockId, Stock> Stocks { get; }

        private readonly string senderType = null;
        private readonly List<Stock> lstStock = new List<Stock>();

        public StockSearchPage(string type)
        {
            InitializeComponent();

            senderType = type;

            Title = "Stocks - All";

            if (AppSettings.RecentStockIds.Count != 0)
            {
                lstStock.AddRange(Store.RecentStockIds.GetState().Reverse().Select(stockId =>
                {
                    // Transform stock codes into stocks
                    if (Store.StockById.GetState().TryGetValue(new StockId(stockId.Code, MarketType.RG), out Stock stock))
                    {
                        Stock stockTmp = new Stock(stock.Id.Code, stock.Id.MarketType, stock.Name);
                        stockTmp.UpdateWith(stock);
                        stockTmp.IsRecent = true;
                        return stockTmp;
                    }
                    else
                    {
                        return new Stock(
                        code: stockId.Code,
                        marketType: MarketType.RG,
                        name: StockNameFallback.For(stockId.Code)
                    );
                    }
                }).ToList());
            }

            lstStock.AddRange(
               Store.StockById.GetState().Values.ToList().Where(x => x.Id.MarketType == MarketType.RG)
               );

            lstViewStocks.ItemsSource = lstStock;

            StockSearchFilterPicker.ItemsSource = StockSearchFilterDict.Dict.Keys.ToList();
        }

        ~StockSearchPage()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
        }

        protected override void OnAppearing()
        {
            base.OnDisappearing();
        }


        protected override void OnDisappearing()
        {
            lstViewStocks.IsRefreshing = false;

            base.OnDisappearing();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            setUi();
        }

        private async void setUi()
        {
            lstViewStocks.IsRefreshing = true;
            string filter = txtSearch.Text;
            await Task.Run(() =>
            {
                Thread.Sleep(275);
                if (filter == txtSearch.Text)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        TextSearching(filter);
                    });
                }
            });
        }

        private void TextSearching(string strText)
        {
            Title = "Stocks";

            IEnumerable<Stock> lstStockTmp = lstStock.ToList();

            if (StockSearchFilterStore.GetState() == StockSearchFilter.ALL)
            {
                Title += " - All";
            }
            else
            if (StockSearchFilterStore.GetState() == StockSearchFilter.WATCHLIST)
            {
                lstStockTmp = lstStockTmp.Where(i => (i.IsWatchlistBoard == true));

                Title += " - IDX Watchlist";
            }

            if (string.IsNullOrWhiteSpace(strText))
            {
                lstViewStocks.ItemsSource = lstStockTmp;
            }
            else
            {
                strText = strText.ToLower();
                lstStockTmp = lstStockTmp.Where(i => (i.Name.ToLower().Contains(strText) || i.Id.Code.ToLower().Contains(strText)));
                lstViewStocks.ItemsSource = lstStockTmp.Select(s => s.Id.Code).Distinct().Select(code => lstStockTmp.First(s => s.Id.Code == code));
            }

            lstViewStocks.IsRefreshing = false;
        }

        private async void lstViewStocks_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is Stock stock)
            {
                if (senderType == "StockAlertPage")
                {
                    CreateStockAlertPage createStockAlertPage = new CreateStockAlertPage(stock);
                    await MainPage.NavigationPage.PushAsync(createStockAlertPage);
                }
                else if (senderType == "StockPage" || senderType == "BrokerAccDistPage")
                {
                    Store.RecentStockIds.AddStock(stock.Id);
                    await MainPage.NavigationPage.PopAsync();
                }
                else if (senderType == "ConditionalOrderPage")
                {
                    Store.RecentStockIds.AddStock(stock.Id);
                    CreateConditionalOrderPage createConditionalOrderPage = new CreateConditionalOrderPage(stock);
                    await MainPage.NavigationPage.PushAsync(createConditionalOrderPage);
                }
                else if (senderType == "BuyButton")
                {
                    Store.RecentStockIds.AddStock(stock.Id);
                    await MainPage.NavigationPage.PopAsync();

                    if ((stock.BestBidPrice ?? stock.LastOrPrevious).HasValue &&
                        stock.SharesPerLot.HasValue)
                    {
                        OrderDialog orderDialog = new OrderDialog(
                            action: OrderAction.Buy,
                            stockId: stock.Id,
                            openPrice: stock.OpenOrPrevious.Value,
                            price: stock.BestBidPrice ?? stock.LastOrPrevious.Value,
                            priceUnsure: stock.BestBidPrice == null,
                            sharesPerLot: stock.SharesPerLot.Value
                        );
                        await Navigation.PushPopupAsync(orderDialog);
                    }
                    else
                    {
                        OrderDialog orderDialog = new OrderDialog(
                             action: OrderAction.Buy,
                             stockId: stock.Id,
                             openPrice: 0,
                             price: 0,
                             priceUnsure: true,
                             sharesPerLot: stock.SharesPerLot.Value
                         );
                        await Navigation.PushPopupAsync(orderDialog);
                    }
                }
                else if (senderType == "SellButton")
                {
                    Store.RecentStockIds.AddStock(stock.Id);
                    await MainPage.NavigationPage.PopAsync();

                    if ((stock.BestBidPrice ?? stock.LastOrPrevious).HasValue &&
                        stock.SharesPerLot.HasValue)
                    {
                        OrderDialog orderDialog = new OrderDialog(
                            action: OrderAction.Sell,
                            stockId: stock.Id,
                            openPrice: stock.OpenOrPrevious.Value,
                            price: stock.BestBidPrice ?? stock.LastOrPrevious.Value,
                            priceUnsure: stock.BestBidPrice == null,
                            sharesPerLot: stock.SharesPerLot.Value
                        );
                        await Navigation.PushPopupAsync(orderDialog);
                    }
                    else
                    {
                        OrderDialog orderDialog = new OrderDialog(
                            action: OrderAction.Sell,
                            stockId: stock.Id,
                            openPrice: 0,
                            price: 0,
                            priceUnsure: true,
                            sharesPerLot: stock.SharesPerLot.Value
                        );
                        await Navigation.PushPopupAsync(orderDialog);
                    }
                }
            }
        }

        private void StockSearchFilterToolBar_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                StockSearchFilterPicker.Focus();
            });
        }

        public void StockSearchFilterPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StockSearchFilterPicker.SelectedItem is string selectedItem && selectedItem != null)
            {
                StockSearchFilterStore.Set(StockSearchFilterDict.Dict[selectedItem]);
            }
            setUi();
        }
    }
}
