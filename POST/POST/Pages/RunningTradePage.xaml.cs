﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Dialogs;
using POST.Models;
using POST.Pages.RunningTradeFilterPages;
using POST.ViewCells;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Index = POST.Models.Index;

namespace POST.Pages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RunningTradePage : ContentPage
    {

        private ListView TradesListView = null;

        private static bool _timerRunning = false;

        private class ClientState { }

        private readonly DataFeedClientStateManager<ClientState> _clientStateManager;

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();
        private Picker pickerFilterGroup = null;

        public RunningTradePage()
        {
            InitializeComponent();

            Store.RunningTrade.Clear();

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState(),
                clientFactory: state =>
                {
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        await sender.SendAsync(new SubscribeRunningTradeRequest());
                    };
                    client.DataReceived += (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case RunningTradeMessage runningTradeMessage:
                                if (Store.RunningTradeFilterGroupSelectedStore.GetState() == "ALL STOCKS" || Store.CustomRunningTradeFilterStockIdsStore.GetState()[Store.RunningTradeFilterGroupSelectedStore.GetState()].Any(stockCode => stockCode == runningTradeMessage.StockCode))
                                {
                                    Store.RunningTrade.AddTrade(runningTradeMessage);
                                }
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
            );
        }

        private void UpdateTitleView(Index index)
        {
            if (index != null)
            {
                FlooredLastLabel.Text = $"{index.FlooredLast:N0}";
                LastDecimalLabel.Text = "." + $"{index.LastDecimals:000}";
                FlooredLastLabel.TextColor = index.ChangeForeground;
                LastDecimalLabel.TextColor = index.ChangeForeground;
                StockChangeGlyphLabel.Text = index.ChangeGlyph;
                StockChangeGlyphLabel.TextColor = index.ChangeForeground;
                if (index.ChangePercent.HasValue)
                {
                    StockChangePercentLabel.Text = $"{index.ChangePercent.Value:N2}%";
                    StockChangePercentLabel.TextColor = index.ChangeForeground;
                }
                else
                {
                    StockChangePercentLabel.Text = string.Empty;
                }
                StockChangePointLabel.Text = index.Change.ToString("#,##0.00;#,##0.00;#,##0.00");
                StockChangePointLabel.TextColor = index.ChangeForeground;
            }
            else
            {
                FlooredLastLabel.Text = string.Empty;
                LastDecimalLabel.Text = string.Empty;
                StockChangeGlyphLabel.Text = string.Empty;
                StockChangePercentLabel.Text = string.Empty;
                StockChangePointLabel.Text = string.Empty;
            }
        }

        private void CreateTradesListView()
        {
            TradesListView = new ListView(ListViewCachingStrategy.RecycleElement)
            {
                BackgroundColor = Colors.PageBackgroundColor,
                RowHeight = 22,
                SeparatorVisibility = SeparatorVisibility.None
            };
            TradesListView.SizeChanged += TradesListView_SizeChanged;
            TradesListView.ItemSelected += TradesListView_ItemSelected;
            TradesListView.ItemTemplate = new DataTemplate(() => new RunningTradeCell());

            grdRunningTrade.Children.Add(TradesListView, 0, 2);
        }

        private void RemoveTradesListView()
        {
            if (TradesListView != null)
            {
                grdRunningTrade.Children.Remove(TradesListView);
                TradesListView.SizeChanged -= TradesListView_SizeChanged;
                TradesListView.ItemSelected -= TradesListView_ItemSelected;
                TradesListView = null;
            }
        }

        protected override async void OnAppearing()
        {
            IsVisible = true;

            base.OnAppearing();

            Picker pickerFilterGroupTmp = new Picker()
            {
                Title = "Choose Filter Group",
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 15
            };
            List<string> lstKey = new List<string>();
            lstKey.AddRange(Store.DefaultRunningTradeFilterStockIdsStore.GetState().Keys.ToList());
            lstKey.AddRange(Store.CustomRunningTradeFilterStockIdsStore.GetState().Keys.ToList());
            pickerFilterGroupTmp.ItemsSource = lstKey;
            pickerFilterGroupTmp.SelectedIndexChanged += pickerFilterGroup_SelectedIndexChanged;
            if (lstKey.Any(key => key == Store.RunningTradeFilterGroupSelectedStore.GetState()))
            {
                pickerFilterGroupTmp.SelectedItem = Store.RunningTradeFilterGroupSelectedStore.GetState();
            }
            else
            {
                pickerFilterGroupTmp.SelectedIndex = 0;
                Store.RunningTradeFilterGroupSelectedStore.Set("ALL STOCKS");
            }

            pickerFilterGroup = pickerFilterGroupTmp;

            grdPicker.Children.Add(pickerFilterGroupTmp);

            CreateTradesListView();

            //_observers.Add(Store.RunningTrade
            //    .DistinctUntilChanged()
            //    .ObserveOn(SynchronizationContext.Current)
            //    .SubscribeOn(SynchronizationContext.Current)
            //    .Subscribe(runningTrade =>
            //    {
            //        //BindingContext = runningTrade;
            //        TradesListView.ItemsSource = runningTrade;
            //    })
            //);

            _observers.Add(Store.IndexByCode
                .Select(indexCode => indexCode.TryGetValue("COMPOSITE", out Index index) ? index : null)
                .Subscribe(UpdateTitleView)
            );

            _clientStateManager.StartClient();

            if (Store.AthenaSession.GetState() is AthenaSession athenaSession &&
                athenaSession.BrokerAccountId == null &&
                !athenaSession.UseAppWithoutBrokerAccount)
            {
                if (PopupNavigation.Instance.PopupStack.Count == 0)
                {
                    await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
                }
            }

            RenderRunningTradeItemSource();
            if (!_timerRunning)
            {
                Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                {
                    if (IsVisible)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                       {
                           RenderRunningTradeItemSource();
                       });
                    }
                    if (!IsVisible)
                    {
                        _timerRunning = false;
                    }
                    return IsVisible;
                });
                _timerRunning = true;
            }
        }

        protected override void OnDisappearing()
        {
            IsVisible = false;

            _clientStateManager.StopClient();

            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();

            if (pickerFilterGroup != null)
            {
                grdPicker.Children.Remove(pickerFilterGroup);
                pickerFilterGroup.SelectedIndexChanged -= pickerFilterGroup_SelectedIndexChanged;
                pickerFilterGroup = null;
            }

            RemoveTradesListView();

            base.OnDisappearing();
        }

        private void RenderRunningTradeItemSource()
        {
            if (TradesListView != null)
            {
                Store.RunningTrade.RenderCollection();
                TradesListView.ItemsSource = Store.RunningTrade.GetState();
            }
        }

        // Get ListView height and determine page size
        private void TradesListView_SizeChanged(object sender, EventArgs e)
        {

            double height = TradesListView.Height;
            double width = TradesListView.Width;

            int rowHeight = 22;
            //if (height < 600) rowHeight = 40;
            //else if (height < 1000) rowHeight = 50;
            //else rowHeight = 60;

            if (rowHeight != TradesListView.RowHeight)
            {
                TradesListView.RowHeight = rowHeight;
            }

            int visibleRows = (int)Math.Floor(height / rowHeight);
            Store.RunningTrade.SetPageSize(visibleRows);

            Column0.Width = width < 320 ? new GridLength(0) : (width < 560 ? new GridLength(60) : new GridLength(1.8, GridUnitType.Star));
            Column3.Width = width < 500 ? new GridLength(3, GridUnitType.Star) : new GridLength(80);
            Column4.Width = width < 500 ? new GridLength(3, GridUnitType.Star) : new GridLength(80);
            Column6.Width = width < 380 ? new GridLength(0) : (width < 560 ? new GridLength(30) : new GridLength(0.9, GridUnitType.Star));
            Column7.Width = width < 380 ? new GridLength(0) : (width < 560 ? new GridLength(30) : new GridLength(0.9, GridUnitType.Star));
            Header0.IsVisible = width >= 320;
            Header6.IsVisible = width >= 380;
            Header7.IsVisible = width >= 380;
        }

        // Disable selection
        private void TradesListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (TradesListView.SelectedItem != null)
            {
                TradesListView.SelectedItem = null;
            }
        }

        private void pickerFilterGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is Picker pickerFilterGroup && Store.RunningTradeFilterGroupSelectedStore.GetState() != (string)pickerFilterGroup.SelectedItem)
            {
                Store.RunningTrade.Clear();
                RemoveTradesListView();
                CreateTradesListView();
                Store.RunningTradeFilterGroupSelectedStore.Set((string)pickerFilterGroup.SelectedItem);
            }
        }

        private async void EditFilterGroupButton_Clicked(object sender, EventArgs e)
        {
            FilterGroupPage filterGroupPage = new FilterGroupPage
            {
                Title = "Filter Group"
            };
            await MainPage.NavigationPage.PushAsync(filterGroupPage);
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}