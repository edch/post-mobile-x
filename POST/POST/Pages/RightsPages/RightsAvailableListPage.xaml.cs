﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.RightsMessages.Responses;
using POST.Models;
using POST.Services;
using POST.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.RightsPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RightsAvailableListPage : ContentPage
    {
        private readonly RightAvailableStatusFilterStore RightsAvailableStatusFilterStore = new RightAvailableStatusFilterStore();
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
        private List<RightsAvailableItem> lstRightsAvailableItem = null;
        private List<RightsAvailableItem> lstRightsAvailableItemTmp = null;
        private ColumnSort columnSort = ColumnSort.ASC;
        private int? lastSortedColumn;
        private DateTime lastRefresh = DateTime.Now;

        private class CustomGrid : Grid
        {
            public int XRow = 0;
            public int YColumn = 0;
        }

        private enum ColumnSort
        {
            ASC,
            DSC
        }

        private class GridSortSign : Grid
        {
            public ColumnSort columnSort = ColumnSort.ASC;
            private readonly Label lblSortForeSign;
            private readonly Label lblSortBackgroundSign;
            public GridSortSign()
            {
                lblSortBackgroundSign = new Label()
                {
                    Text = Glyphs.BackgroundSort,
                    TextColor = Colors.NetralSortColor,
                    FontSize = 12,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 0, -2),
                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                    HorizontalOptions = LayoutOptions.Start
                };
                lblSortForeSign = new Label()
                {
                    Text = Glyphs.Unchanged,
                    TextColor = Colors.AscDescSortColor,
                    FontSize = 11,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 0, -2),
                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                    HorizontalOptions = LayoutOptions.Start
                };
                Children.Add(lblSortBackgroundSign, 0, 0);
                Children.Add(lblSortForeSign, 0, 0);
            }

            public void SetAsc()
            {
                lblSortForeSign.Text = Glyphs.Gaining;
            }

            public void SetDesc()
            {
                lblSortForeSign.Text = Glyphs.Losing;
            }

            public void SetNetral()
            {
                lblSortForeSign.Text = Glyphs.Unchanged;
            }
        }

        private readonly List<GridSortSign> lstGridSortSign = new List<GridSortSign>();

        private CustomGrid ToHeader(string title, int yColumn, LayoutOptions layoutOptions)
        {
            Label lblTitle = new Label()
            {
                Text = title,
                Style = (Style)Resources["HeaderLabelStyle"]
            };
            GridSortSign gridSortSign = new GridSortSign();
            lstGridSortSign.Add(gridSortSign);

            CustomGrid customGrid = new CustomGrid()
            {
                HorizontalOptions = layoutOptions,
                YColumn = yColumn,
                ColumnSpacing = 1,
                ColumnDefinitions = new ColumnDefinitionCollection() { new ColumnDefinition { Width = GridLength.Auto }, new ColumnDefinition { Width = GridLength.Auto } },
                Children = {
                            lblTitle,
                           gridSortSign
                        }
            };

            Grid.SetRow(lblTitle, 0);
            Grid.SetColumn(lblTitle, 0);
            Grid.SetRow(gridSortSign, 0);
            Grid.SetColumn(gridSortSign, 1);

            customGrid.GestureRecognizers.Add(
                new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        ResetColumnHeader();

                        if (columnSort == ColumnSort.ASC)
                        {
                            columnSort = ColumnSort.DSC;
                            gridSortSign.SetDesc();
                        }
                        else
                        {
                            columnSort = ColumnSort.ASC;
                            gridSortSign.SetAsc();
                        }

                        lastSortedColumn = yColumn;

                        LoadListAsync_Status(RightsAvailableStatusFilterStore.GetState(), lastSortedColumn);
                    })
                });

            return customGrid;
        }

        public RightsAvailableListPage()
        {
            InitializeComponent();

            StatusFilterPicker.ItemsSource = RightsAvailableStatusDict.Items;
            StatusFilterPicker.SelectedIndex = 1;

            CreateDataGrid();
        }

        private void CreateDataGrid()
        {
            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60) });
            headerGrid.Children.Add(ToHeader("Stock", 1, LayoutOptions.Center), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(90) });
            headerGrid.Children.Add(ToHeader("Start Date", 2, LayoutOptions.Center), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(90) });
            headerGrid.Children.Add(ToHeader("End Date", 3, LayoutOptions.Center), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(70) });
            headerGrid.Children.Add(ToHeader("Price", 4, LayoutOptions.End), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(80) });
            headerGrid.Children.Add(ToHeader("Action", 5, LayoutOptions.Center), headerGrid.Children.Count, 0);


            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(80) });
            headerGrid.Children.Add(ToHeader("Status", 6, LayoutOptions.Center), headerGrid.Children.Count, 0);
        }

        private void ResetColumnHeader()
        {
            lastSortedColumn = null;
            lstGridSortSign.ForEach(grd => grd.SetNetral());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(RightsAvailableStatusFilterStore.Subscribe(rightAvailableStatusFilter =>
            {
                LoadListAsync_Status(rightAvailableStatusFilter, lastSortedColumn);
            }));

            _observers.Add(Store.AthenaSession.Subscribe(athenaSession =>
            {
                RefreshPageAsync(false);
            }));
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async Task<int> LoadListAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            try
            {
                RightsAvailableResponse rightsAvailableResponse = await AthenaClient.Current.GetRightsAvailableAsync(brokerAccountId, cancellationToken);
                if (rightsAvailableResponse != null && rightsAvailableResponse.actions != null && rightsAvailableResponse.actions.Length > 0)
                {
                    lstRightsAvailableItem = rightsAvailableResponse.actions.ToList();
                    lstRightsAvailableItemTmp = new List<RightsAvailableItem>(lstRightsAvailableItem);
                    ResetColumnHeader();
                    StatusFilterPicker.SelectedIndex = 1;
                    RightsAvailableStatusFilterStore.Set(RightsAvailableStatusDict.Items[1]);
                    return 1;
                }
                else
                {
                    ResetColumnHeader();
                    lstRightsAvailableItem = null;
                    RightsAvailableListView.ItemsSource = null;
                    return 2;
                }
            }
            catch (OperationCanceledException)
            {
                return 3;
            }
            catch
            {
                ResetColumnHeader();
                RightsAvailableListView.ItemsSource = null;
                return 4;
            }
            finally
            {
                RightsAvailableListView.IsRefreshing = false;
            }
        }

        private void LoadListAsync_Status(string rightAvailableStatus, int? lastSortedColumn)
        {
            try
            {
                if (lstRightsAvailableItem != null && lstRightsAvailableItem.Count > 0)
                {
                    if (rightAvailableStatus == RightsAvailableStatusDict.Items[0])
                    {
                        lstRightsAvailableItemTmp = new List<RightsAvailableItem>(lstRightsAvailableItem);
                    }
                    else
                    {
                        lstRightsAvailableItemTmp = new List<RightsAvailableItem>(lstRightsAvailableItem.Where(item => item.sstatus == rightAvailableStatus));
                    }

                    if (lastSortedColumn.HasValue)
                    {
                        switch (lastSortedColumn)
                        {
                            case 0:
                                break;

                            case 1:
                                if (columnSort == ColumnSort.ASC)
                                {
                                    lstRightsAvailableItemTmp = lstRightsAvailableItemTmp.OrderBy(ps => ps.rightId).ToList();
                                }
                                else
                                {
                                    lstRightsAvailableItemTmp = lstRightsAvailableItemTmp.OrderByDescending(ps => ps.rightId).ToList();
                                }
                                break;

                            case 2:
                                if (columnSort == ColumnSort.ASC)
                                {
                                    lstRightsAvailableItemTmp = lstRightsAvailableItemTmp.OrderBy(ps => ps.startdate).ToList();
                                }
                                else
                                {
                                    lstRightsAvailableItemTmp = lstRightsAvailableItemTmp.OrderByDescending(ps => ps.startdate).ToList();
                                }
                                break;

                            case 3:
                                if (columnSort == ColumnSort.ASC)
                                {
                                    lstRightsAvailableItemTmp = lstRightsAvailableItemTmp.OrderBy(ps => ps.enddate).ToList();
                                }
                                else
                                {
                                    lstRightsAvailableItemTmp = lstRightsAvailableItemTmp.OrderByDescending(ps => ps.enddate).ToList();
                                }
                                break;

                            case 4:
                                if (columnSort == ColumnSort.ASC)
                                {
                                    lstRightsAvailableItemTmp = lstRightsAvailableItemTmp.OrderBy(ps => ps.exPrice).ToList();
                                }
                                else
                                {
                                    lstRightsAvailableItemTmp = lstRightsAvailableItemTmp.OrderByDescending(ps => ps.exPrice).ToList();
                                }
                                break;

                            case 5:
                                if (columnSort == ColumnSort.ASC)
                                {
                                    lstRightsAvailableItemTmp = lstRightsAvailableItemTmp.OrderBy(ps => ps.action).ToList();
                                }
                                else
                                {
                                    lstRightsAvailableItemTmp = lstRightsAvailableItemTmp.OrderByDescending(ps => ps.action).ToList();
                                }
                                break;

                            case 6:
                                if (columnSort == ColumnSort.ASC)
                                {
                                    lstRightsAvailableItemTmp = lstRightsAvailableItemTmp.OrderBy(ps => ps.sstatus).ToList();
                                }
                                else
                                {
                                    lstRightsAvailableItemTmp = lstRightsAvailableItemTmp.OrderByDescending(ps => ps.sstatus).ToList();
                                }
                                break;
                        }
                    }

                    RightsAvailableListView.ItemsSource = lstRightsAvailableItemTmp;
                }
            }
            catch
            {
                ResetColumnHeader();
                RightsAvailableListView.ItemsSource = null;
            }
            finally
            {
                RightsAvailableListView.IsRefreshing = false;
            }
        }

        public async void RefreshPageAsync(bool showMessage = true)
        {
            if (Store.AthenaSession.GetState() != null &&
                    Store.AthenaSession.GetState().BrokerAccountId != null)
            {
                lastRefresh = DateTime.Now;
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
                int result = await LoadListAsync(Store.AthenaSession.GetState().BrokerAccountId, _cancellationTokenSource.Token);
                if (result == 1)
                {
                    if (showMessage)
                    {
                        DependencyService.Get<IToastMessage>().Show("Right/Warrant List Loaded.");
                    }
                }
                else if (result == 2)
                {
                    DependencyService.Get<IToastMessage>().Show("Right/Warrant List Loaded.");
                }
                else if (result == 3)
                {
                    if (DateTime.Now.CompareTo(lastRefresh.AddSeconds(10)) >= 0)
                    {
                        DependencyService.Get<IToastMessage>().Show("Request timeout.");
                    }
                }
                else if (result == 4)
                {
                    DependencyService.Get<IToastMessage>().Show("Request failed.");
                }

            }
            else
            {
                ResetColumnHeader();
                lstRightsAvailableItem = null;
                lstRightsAvailableItemTmp = null;
                RightsAvailableListView.ItemsSource = null;
                StatusFilterPicker.SelectedIndex = 1;
            }
        }

        public void StatusFilterPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StatusFilterPicker.SelectedItem is string selectedItem && selectedItem != null)
            {
                RightsAvailableStatusFilterStore.Set(selectedItem);
            }
        }
    }
}