﻿using POST.Constants;
using POST.Models;
using POST.ViewCells.RunningTradeFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.RunningTradeFilterPages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StockFilterPage : ContentPage
    {
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        private readonly string strGroupName = "";

        public StockFilterPage(string strGroupName)
        {
            InitializeComponent();
            this.strGroupName = strGroupName;
            StocksListView.ItemTemplate = new DataTemplate(() => new RunningTradeStockFilterCell(strGroupName));
        }

        ~StockFilterPage()
        {

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.CustomRunningTradeFilterStockIdsStore
                .Subscribe(groupFilter =>
                {
                    BindingContext = groupFilter[strGroupName].Select(stockCode =>
                    {
                        // Transform stock codes into stocks
                        if (Store.StockById.GetState().TryGetValue(new StockId(stockCode, MarketType.RG), out Stock stock))
                        {
                            return stock;
                        }
                        else
                        {
                            return new Stock(
                                code: stockCode,
                                marketType: MarketType.RG,
                                name: StockNameFallback.For(stockCode)
                            );
                        }
                    }).ToList();
                })
            );
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();

            base.OnDisappearing();
        }

        private async void AddButton_Clicked(object sender, EventArgs e)
        {
            AddRunningTradeStockFilterPage addRunningTradeStockFilterPage = new AddRunningTradeStockFilterPage(strGroupName)
            {
                Title = "Add Stocks"
            };
            await MainPage.NavigationPage.PushAsync(addRunningTradeStockFilterPage);
        }

        private async void ClearButton_Clicked(object sender, EventArgs e)
        {
            if (Store.CustomRunningTradeFilterStockIdsStore.GetState()[strGroupName].Count > 0)
            {
                bool AnswerYes = await Application.Current.MainPage.DisplayAlert("Clear Confirmation", "Are you sure want to clear the Stock Filter?", "Ok", "Cancel");
                if (!AnswerYes)
                {
                    return;
                }
                Store.CustomRunningTradeFilterStockIdsStore.ClearStock(strGroupName);
            }
        }

    }
}
