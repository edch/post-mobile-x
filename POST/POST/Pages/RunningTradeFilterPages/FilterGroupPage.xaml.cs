﻿using POST.Dialogs;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.RunningTradeFilterPages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FilterGroupPage : ContentPage
    {

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public FilterGroupPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.CustomRunningTradeFilterStockIdsStore
                .Subscribe(rt =>
                {
                    BindingContext = rt.Select(key =>
                    {
                        return key.Key;
                    }).ToList();
                })
            );
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();

            base.OnDisappearing();
        }

        private async void AddButton_Clicked(object sender, EventArgs e)
        {
            if (Store.CustomRunningTradeFilterStockIdsStore.GetState().Keys.Count < 5)
            {
                CreateFilterGroupDialog createFilterGroupDialog = new CreateFilterGroupDialog();
                await PopupNavigation.Instance.PushAsync(createFilterGroupDialog);
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Maximum is 5 Groups.", "Ok");
            }
        }

        private async void ClearButton_Clicked(object sender, EventArgs e)
        {
            if (Store.CustomRunningTradeFilterStockIdsStore.GetState().Keys.Count > 0)
            {
                bool AnswerYes = await Application.Current.MainPage.DisplayAlert("Clear Confirmation", "Are you sure want to clear the Filter Group?", "Ok", "Cancel");
                if (!AnswerYes)
                {
                    return;
                }
                Store.CustomRunningTradeFilterStockIdsStore.ClearGroup();
            }
        }
    }
}