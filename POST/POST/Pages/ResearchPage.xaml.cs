﻿using POST.Pages.NewsPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Index = POST.Models.Index;

namespace POST.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResearchPage : TabbedPage
    {
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public ResearchPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.IndexByCode
                .Select(indexCode => indexCode.TryGetValue("COMPOSITE", out Index index) ? index : null)
                .Subscribe(UpdateTitleView)
            );
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
        }

        private void UpdateTitleView(Index index)
        {
            if (index != null)
            {
                FlooredLastLabel.Text = $"{index.FlooredLast:N0}";
                LastDecimalLabel.Text = "." + $"{index.LastDecimals:000}";
                FlooredLastLabel.TextColor = index.ChangeForeground;
                LastDecimalLabel.TextColor = index.ChangeForeground;
                StockChangeGlyphLabel.Text = index.ChangeGlyph;
                StockChangeGlyphLabel.TextColor = index.ChangeForeground;
                if (index.ChangePercent.HasValue)
                {
                    StockChangePercentLabel.Text = $"{index.ChangePercent.Value:N2}%";
                    StockChangePercentLabel.TextColor = index.ChangeForeground;
                }
                else
                {
                    StockChangePercentLabel.Text = string.Empty;
                }
                StockChangePointLabel.Text = index.Change.ToString("#,##0.00;#,##0.00;#,##0.00");
                StockChangePointLabel.TextColor = index.ChangeForeground;
            }
            else
            {
                FlooredLastLabel.Text = string.Empty;
                LastDecimalLabel.Text = string.Empty;
                StockChangeGlyphLabel.Text = string.Empty;
                StockChangePercentLabel.Text = string.Empty;
                StockChangePointLabel.Text = string.Empty;
            }
        }

        private async void BookmarksButton_Clicked(object sender, EventArgs e)
        {
            SavedNewsCollectionPage page = (SavedNewsCollectionPage)Activator.CreateInstance(typeof(SavedNewsCollectionPage));
            await MainPage.NavigationPage.PushAsync(page);
        }
    }
}