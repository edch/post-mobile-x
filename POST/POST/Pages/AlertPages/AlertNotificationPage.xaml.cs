﻿using POST.DataSources;
using POST.DataSources.AlertMessages.Responses;
using POST.Dialogs;
using POST.Services;
using POST.ViewCells;
using Rg.Plugins.Popup.Services;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.AlertPages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlertNotificationPage : ContentPage
    {
        private DateTime lastRefresh = DateTime.Now;
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));

        public AlertNotificationPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            RefreshPageAsync(false);
        }

        protected override void OnDisappearing()
        {
            if (!_cancellationTokenSource.IsCancellationRequested)
            {
                _cancellationTokenSource.Cancel();
            }
            base.OnDisappearing();
        }

        private async Task<int> LoadAlertsAsync(CancellationToken cancellationToken)
        {
            try
            {
                AlertResponse alertResponse = await AlertClient.Default.GetNoticeListAsync(Store.LoginForm.GetState().UserId, cancellationToken);
                if (alertResponse != null)
                {
                    StocksAlertListView.ItemsSource = alertResponse.notices.Where(notice => notice.sent);
                    return 1;
                }
                else
                {
                    StocksAlertListView.ItemsSource = null;
                    return 2;
                }
            }
            catch (OperationCanceledException)
            {
                return 3;
            }
            catch (Exception)
            {
                StocksAlertListView.ItemsSource = null;
                return 4;
            }
            finally
            {
            }
        }

        public async void RefreshPageAsync(bool showMessage = true)
        {
            lastRefresh = DateTime.Now;
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
            int result = await LoadAlertsAsync(_cancellationTokenSource.Token);
            if (result == 1)
            {
                if (showMessage)
                {
                    DependencyService.Get<IToastMessage>().Show("Triggered Alert Loaded.");
                }
            }
            else if (result == 2)
            {
                DependencyService.Get<IToastMessage>().Show("Triggered Alert is empty.");
            }
            else if (result == 3)
            {
                if (DateTime.Now.CompareTo(lastRefresh.AddSeconds(10)) >= 0)
                {
                    DependencyService.Get<IToastMessage>().Show("Request timeout.");
                }
            }
            else if (result == 4)
            {
                DependencyService.Get<IToastMessage>().Show("Request failed.");
            }
        }

        private void StocksAlertListView_Refreshing(object sender, EventArgs e)
        {
            RefreshPageAsync();
        }

        private async void AlertStockViewCell_alertCellLongPress(object sender, ViewCells.AlertCellEventArgs e)
        {
            bool AnswerYes = await Application.Current.MainPage.DisplayAlert("Delete Confirmation", "Are you sure want to delete " + e.notice.stockid + "?", "Ok", "Cancel");
            if (AnswerYes)
            {
                try
                {
                    string strResponse = await AlertClient.Default.DeleteNoticeAsync(Store.LoginForm.GetState().UserId, e.notice.noticeid, new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);
                    DependencyService.Get<IToastMessage>().Show("Alert deleted.");
                    RefreshPageAsync(false);
                }
                catch
                {
                    DependencyService.Get<IToastMessage>().Show("Request failed.");
                }
            }
        }

        private async void AlertStockViewCell_alertCellSingleTap(object sender, ViewCells.AlertCellEventArgs e)
        {
            Store.RecentStockIds.AddStock(e.notice.stockid);
            StockPage page = new StockPage();
            await MainPage.NavigationPage.PushAsync(page);

            if (sender is AlertStockViewCell)
            {
                StocksAlertListView.SelectedItem = null;
            }
        }



        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }



        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}
