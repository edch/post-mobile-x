﻿using POST.DataSources;
using POST.Models;
using POST.Services;
using System;
using System.Collections.Generic;
using System.Threading;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.AlertPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateStockAlertPage : ContentPage
    {
        private readonly Stock stock;
        private readonly List<string> lstAlertCondition;

        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        public CreateStockAlertPage(Stock stock)
        {
            InitializeComponent();
            Title = "Create Stock Alert";
            lstAlertCondition = new List<string>() { "HIGHER THAN", "LOWER THAN" };
            this.stock = stock;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            pickerCondition.ItemsSource = lstAlertCondition;

            lblStockCode.Text = stock.Id.Code;
            lblStockName.Text = stock.Name;
            if (stock.LastOrPrevious.HasValue)
            {
                lblLastPrice.Text = "Last Price " + stock.LastOrPrevious.Value.ToString();
            }

            txtPriceCondition.Placeholder = "Enter Price";

            lblStockCode.TextDecorations = stock.TextDecoration;
            lblStockName.TextDecorations = stock.TextDecoration;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void btnCreate_Clicked(object sender, EventArgs e)
        {
            if (pickerCondition.SelectedIndex == -1)
            {
                DependencyService.Get<IToastMessage>().Show("Choose Condition!");
                return;
            }
            if (string.IsNullOrEmpty(txtPriceCondition.Text))
            {
                DependencyService.Get<IToastMessage>().Show("Enter Price!");
                return;
            }

            string noticeType = (pickerCondition.SelectedIndex == 0) ? "hprice" : "lprice";
            try
            {
                _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(20));
                string athenaResponse = await AlertClient.Default.AddNoticeAsync(Store.LoginForm.GetState().UserId, stock.Id.Code, noticeType, txtPriceCondition.Text, _cancellationTokenSource.Token);
                if (athenaResponse != null)
                {
                    DependencyService.Get<IToastMessage>().Show("Alert Created.");
                    await MainPage.NavigationPage.PopToRootAsync();
                }
                else
                {
                    DependencyService.Get<IToastMessage>().Show("Failed to create.");
                }
            }
            catch (OperationCanceledException)
            {
                DependencyService.Get<IToastMessage>().Show("Request timeout.");
            }
            catch
            {
                DependencyService.Get<IToastMessage>().Show("Request failed.");
            }
            finally
            {
            }
        }
    }
}