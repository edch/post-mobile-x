﻿using POST.Dialogs;
using Rg.Plugins.Popup.Services;
using System;
using System.IO;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HelpContactUs : ContentPage
    {
        private readonly string urlManifestContactUsResources = "";

        public HelpContactUs()
        {
            InitializeComponent();

            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    urlManifestContactUsResources = "POST.Droid.Resources.ContactUsResources.txt";
                    break;
                case Device.iOS:
                    urlManifestContactUsResources = "POST.iOS.Resources.ContactUsResources.txt";
                    break;
                case Device.UWP:
                    urlManifestContactUsResources = "POST.UWP.Resources.ContactUsResources.txt";
                    break;
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadTextFromResource();
        }

        private async void LoadTextFromResource()
        {
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(urlManifestContactUsResources))
            {
                if (stream != null)
                {
                    string fileText = "";
                    using (StreamReader reader = new System.IO.StreamReader(stream))
                    {
                        fileText = await reader.ReadToEndAsync();
                    }
                    lblContent.Text = fileText;
                }
            }
        }

        private async void btnFeedback_Clicked(object sender, EventArgs e)
        {
            FeedbackDialog popup = new FeedbackDialog();
            await PopupNavigation.Instance.PushAsync(popup);
        }
    }
}