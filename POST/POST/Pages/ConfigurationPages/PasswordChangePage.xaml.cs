﻿using POST.DataSources;
using POST.DataSources.AthenaMessages.Responses;
using POST.Models;
using POST.Services;
using System;
using System.Net;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.ConfigurationPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PasswordChangePage : ContentPage
    {
        public PasswordChangePage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void btnExecute_Clicked(object sender, EventArgs e)
        {
            string strOldPwd = entryOldPassword.Text;
            string strNewPwd = entryNewPassword.Text;
            string strConfPwd = entryConfPassword.Text;

            if (string.IsNullOrWhiteSpace(strOldPwd))
            {
                DependencyService.Get<IToastMessage>().Show("Please Insert Old Password!");
                return;
            }
            if (string.IsNullOrWhiteSpace(strNewPwd))
            {
                DependencyService.Get<IToastMessage>().Show("Please Insert New Password!");
                return;
            }
            if (string.IsNullOrWhiteSpace(strConfPwd))
            {
                DependencyService.Get<IToastMessage>().Show("Please Insert Confirm Password!");
                return;
            }
            if (strNewPwd != strConfPwd)
            {
                DependencyService.Get<IToastMessage>().Show("Confirm and New Password is Different!");
                return;
            }

            if (!Password.IsValid(strNewPwd))
            {
                DependencyService.Get<IToastMessage>().Show("Password must be 8 Alphanumeric Characters include Capital!");
                return;
            }

            try
            {
                AthenaResponse athenaResponse = await AthenaClient.Current.ChangePasswordAsync(strOldPwd, strNewPwd, cancellationToken: new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);

                if (athenaResponse.Message == "true")
                {
                    await DisplayAlert("Success", "Change Password Success!", "Ok");
                }
                else
                if (athenaResponse.Message == "false-used")
                {
                    await DisplayAlert("Failed", "This Password had been used. Try another Password!", "Ok");
                }
                else
                {
                    await DisplayAlert("Failed", "Change Password Failed!", "Ok");
                }
            }
            catch (OperationCanceledException)
            {
                DependencyService.Get<IToastMessage>().Show("Connection Timeout");
            }
            catch (WebException)
            {
                DependencyService.Get<IToastMessage>().Show("Connection Error");
            }
            catch (Exception)
            {
                DependencyService.Get<IToastMessage>().Show("Connection Timeout.");
            }
            finally
            {
                entryOldPassword.Text = "";
                entryNewPassword.Text = "";
                entryConfPassword.Text = "";
            }

        }

        private void btnCancel_Clicked(object sender, EventArgs e)
        {
            entryOldPassword.Text = "";
            entryNewPassword.Text = "";
            entryConfPassword.Text = "";
        }
    }
}