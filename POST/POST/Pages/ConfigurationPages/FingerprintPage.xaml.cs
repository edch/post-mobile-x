﻿using Plugin.Fingerprint;
using POST.Constants;
using POST.Dialogs;
using POST.Stores;
using Rg.Plugins.Popup.Services;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.ConfigurationPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FingerprintPage : ContentPage
    {
        public FingerprintPage()
        {
            InitializeComponent();

            stackLayoutFingerprint.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    switchUseFingerprint.IsToggled = !switchUseFingerprint.IsToggled;
                })
            });

            lblInfoIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    PrivacyNoticeFingerprintDialog popup = new PrivacyNoticeFingerprintDialog();
                    await PopupNavigation.Instance.PushAsync(popup);
                })
            });
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            Plugin.Fingerprint.Abstractions.FingerprintAvailability isAvailableFingerprint = await CrossFingerprint.Current.GetAvailabilityAsync();
            if (isAvailableFingerprint != Plugin.Fingerprint.Abstractions.FingerprintAvailability.Available)
            {
                stackLayoutFingerprint.IsEnabled = false;
                switchUseFingerprint.IsEnabled = false;
                lblTitle.TextColor = Colors.FieldNameColor;
                switchUseFingerprint.IsToggled = false;
            }
            else
            {
                stackLayoutFingerprint.IsEnabled = true;
                switchUseFingerprint.IsEnabled = true;
                lblTitle.TextColor = Colors.FieldValueDefaultColor;
                if (AppSettings.Fingerprint_Use)
                {
                    switchUseFingerprint.IsToggled = true;
                }
                else
                {
                    switchUseFingerprint.IsToggled = false;
                }
            }
        }

        private void SwitchUseFingerprint_Toggled(object sender, ToggledEventArgs e)
        {
            if (switchUseFingerprint.IsToggled)
            {
                AppSettings.Fingerprint_Use = true;
            }
            else
            {
                AppSettings.Fingerprint_Use = false;
            }
        }
    }
}