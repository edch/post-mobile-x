﻿using POST.DataSources;
using POST.DataSources.AthenaMessages.Responses;
using POST.Models;
using POST.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.ConfigurationPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PinChangePage : ContentPage
    {
        private readonly List<string> lstAccountId;
        private Picker pickerAccountId = null;

        public PinChangePage()
        {
            InitializeComponent();

            lstAccountId = new List<string>();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            Picker pickerAccountIdTmp = new Picker()
            {
                Title = "Choose Account",
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 15
            };
            pickerAccountId = pickerAccountIdTmp;
            grdPinChange.Children.Add(pickerAccountIdTmp, 1, 0);

            await UpdatePageAsync();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (pickerAccountId != null)
            {
                grdPinChange.Children.Remove(pickerAccountId);
                pickerAccountId = null;
            }
        }

        private async Task UpdatePageAsync()
        {
            TradingAccount[] response = await Store.AthenaSession.GetState().Client.GetTradingAccountsAsync(new CancellationTokenSource(TimeSpan.FromSeconds(10)).Token);
            if (response != null)
            {
                lstAccountId.Clear();
                for (int i = 0; i < response.Count(); i++)
                {
                    lstAccountId.Add(response[i].BrokerAccountId);
                }
                pickerAccountId.ItemsSource = lstAccountId;
                if (lstAccountId.Count > 0)
                {
                    pickerAccountId.SelectedIndex = 0;
                }
            }
        }

        private async void btnExecute_Clicked(object sender, EventArgs e)
        {
            string strOldPin = entryOldPin.Text;
            string strNewPin = entryNewPin.Text;
            string strConfPin = entryConfPin.Text;

            if (pickerAccountId.SelectedIndex == -1)
            {
                DependencyService.Get<IToastMessage>().Show("Select Account ID!");
                return;
            }
            if (string.IsNullOrEmpty(strOldPin))
            {
                DependencyService.Get<IToastMessage>().Show("Please Insert Old PIN!");
                return;
            }
            if (string.IsNullOrEmpty(strNewPin))
            {
                DependencyService.Get<IToastMessage>().Show("Please Insert New PIN!");
                return;
            }
            if (string.IsNullOrEmpty(strConfPin))
            {
                DependencyService.Get<IToastMessage>().Show("Please Insert Confirm PIN!");
                return;
            }
            if (strNewPin != strConfPin)
            {
                DependencyService.Get<IToastMessage>().Show("Confirm and New PIN is Different!");
                return;
            }

            if (!PIN.IsValid(strNewPin))
            {
                DependencyService.Get<IToastMessage>().Show("PIN must be 6 Digits!");
                return;
            }

            try
            {
                AthenaResponse athenaResponse = await AthenaClient.Current.ChangePinAsync(pickerAccountId.SelectedItem.ToString(), strOldPin, strNewPin, cancellationToken: new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);
                if (athenaResponse.Message == "true")
                {
                    await DisplayAlert("Success", "Change PIN Success!", "Ok");
                }
                else
                {
                    await DisplayAlert("Failed", "Change PIN Failed!", "Ok");
                }
            }
            catch (OperationCanceledException)
            {
                DependencyService.Get<IToastMessage>().Show("Connection Timeout");
            }
            catch (WebException)
            {
                DependencyService.Get<IToastMessage>().Show("Connection Error");
            }
            catch (Exception)
            {
                DependencyService.Get<IToastMessage>().Show("Connection Timeout.");
            }
            finally
            {
                entryOldPin.Text = "";
                entryNewPin.Text = "";
                entryConfPin.Text = "";
            }
        }

        private void btnCancel_Clicked(object sender, EventArgs e)
        {
            entryOldPin.Text = "";
            entryNewPin.Text = "";
            entryConfPin.Text = "";
        }

        public async void RefreshPageAsync()
        {
            await UpdatePageAsync();
            DependencyService.Get<IToastMessage>().Show("Account ID Loaded.");
        }

        private void BtnRefresh_Clicked(object sender, EventArgs e)
        {
            RefreshPageAsync();
        }
    }
}