﻿using POST.DataSources.AthenaMessages.Responses;
using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PortfolioPage : TabbedPage
    {

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public PortfolioPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.AthenaSession.Subscribe(athenaSession =>
            {
                if (athenaSession != null &&
                    athenaSession.BrokerAccountId != null)
                {
                    if (Store.TradingAccounts.GetState()?.FirstOrDefault(account => account.BrokerAccountId == athenaSession.BrokerAccountId) is TradingAccount tradingAccount)
                    {
                        AccountIdLabel.Text = $"{tradingAccount.BrokerAccountId} ({tradingAccount.CustomerType})";
                    }
                    else
                    {
                        AccountIdLabel.Text = athenaSession.BrokerAccountId;
                    }
                }
                else
                {
                    AccountIdLabel.Text = "No Account Selected";
                }
            }));
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
            base.OnDisappearing();
        }

    }
}