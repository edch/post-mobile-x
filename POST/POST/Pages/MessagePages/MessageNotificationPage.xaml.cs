﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.MessagePages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MessageNotificationPage : ContentPage
    {
        public MessageNotificationPage()
        {
            InitializeComponent();
        }
    }
}