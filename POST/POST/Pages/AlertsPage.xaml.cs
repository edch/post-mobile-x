﻿using POST.Pages.AlertPages;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlertsPage : TabbedPage
    {
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public AlertsPage()
        {
            InitializeComponent();

            //_observers.Add(Store.IndexByCode
            //    .Select(indexCode => indexCode.TryGetValue("COMPOSITE", out Index index) ? index : null)
            //    .Subscribe(UpdateTitleView)
            //);
        }

        //private void UpdateTitleView(Index index)
        //{
        //    if (index != null)
        //    {
        //        FlooredLastLabel.Text = $"{index.FlooredLast:N0}";
        //        LastDecimalLabel.Text = "." + $"{index.LastDecimals:000}";
        //        FlooredLastLabel.TextColor = index.ChangeForeground;
        //        LastDecimalLabel.TextColor = index.ChangeForeground;
        //        StockChangeGlyphLabel.Text = index.ChangeGlyph;
        //        StockChangeGlyphLabel.TextColor = index.ChangeForeground;
        //        if (index.ChangePercent.HasValue)
        //        {
        //            StockChangePercentLabel.Text = $"{index.ChangePercent.Value:N2}%";
        //            StockChangePercentLabel.TextColor = index.ChangeForeground;
        //        }
        //        else
        //        {
        //            StockChangePercentLabel.Text = string.Empty;
        //        }
        //    }
        //    else
        //    {
        //        FlooredLastLabel.Text = string.Empty;
        //        LastDecimalLabel.Text = string.Empty;
        //        StockChangeGlyphLabel.Text = string.Empty;
        //        StockChangePercentLabel.Text = string.Empty;
        //    }
        //}

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();

            base.OnDisappearing();
        }

        private void BtnRefresh_Clicked(object sender, EventArgs e)
        {
            if (CurrentPage is StockAlertPage stockAlertPage)
            {
                stockAlertPage.RefreshPageAsync();
            }
            else if (CurrentPage is AlertNotificationPage alertNotificationPage)
            {
                alertNotificationPage.RefreshPageAsync();
            }
        }

        public void GotoAlertNotificationPage()
        {
            CurrentPage = AlertNotification_Page;
        }
    }
}