﻿using Plugin.Fingerprint;
using POST.DataSources;
using POST.DataSources.AthenaMessages.Requests;
using POST.DataSources.AthenaMessages.Responses;
using POST.DataSources.DataFeedMessages;
using POST.Dialogs;
using POST.Models;
using POST.Services;
using POST.Stores;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {

        private readonly string _hashedPassword;
        private readonly bool isForcedLogout;

        private int tappedCount;
        private DateTime lastTapped;
        private readonly double delayTapped;
        private readonly int validTapped;


        public LoginPage(bool isForcedLogout = false)
        {
            InitializeComponent();

            tappedCount = 0;
            lastTapped = DateTime.Now;
            delayTapped = 900.0;
            validTapped = 4;

            gridOpeningAccount.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    Device.OpenUri(new System.Uri("https://register.pans.co.id/ref/pm4"));
                })
            });

            gridUserManualLink.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    Device.OpenUri(new System.Uri("http://chart2.post-pro.co.id/manual/ManualMobile4.pdf"));
                })
            });

            gridEIpoLink.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    Device.OpenUri(new System.Uri("http://www.e-ipo.co.id"));
                })
            });

            this.isForcedLogout = isForcedLogout;

            imgFingerprint.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    if (!AppSettings.Fingerprint_Use)
                    {
                        await DisplayAlert("Error", "Fingerprint not set. Go to Configuration Menu after Login.", "Ok");
                    }
                    else
                    {
                        Plugin.Fingerprint.Abstractions.FingerprintAuthenticationResult result = await CrossFingerprint.Current.AuthenticateAsync("Tap fingerprint sensor to login!");
                        if (result.Authenticated)
                        {
                            LoginForm loginForm = Store.LoginForm.GetState();
                            if (loginForm != null)
                            {
                                if (AppSettings.Encrypted == "0")
                                {
                                    LoginAsync(loginForm.UserId, loginForm.Password);
                                }
                                else
                                {
                                    LoginAsync(loginForm.UserId, Password.decrypt(loginForm.Password));
                                }
                            }
                            else
                            {
                                await DisplayAlert("Error", "No Account Saved.", "Ok");
                            }
                        }
                        else
                        {
                            if (result.Status == Plugin.Fingerprint.Abstractions.FingerprintAuthenticationResultStatus.Canceled)
                            {
                                return;
                            }
                            else
                            {
                                await DisplayAlert("Error", result.ErrorMessage, "Ok");
                            }
                        }
                    }
                })
            });

            gridVersion.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    try
                    {
                        DateTime now = DateTime.Now;

                        if (now.Subtract(lastTapped) is TimeSpan timeSpan && timeSpan.TotalMilliseconds <= delayTapped)
                        {
                            if (++tappedCount >= validTapped)
                            {
                                tappedCount = 0;
                                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                {
                                    await PopupNavigation.Instance.PopAsync();
                                }

                                await PopupNavigation.Instance.PushAsync(new ServerSetting());
                            }
                        }
                        else
                        {
                            tappedCount = 1;
                        }

                        lastTapped = now;
                    }
                    catch (Exception)
                    {
                        lastTapped = DateTime.Now;
                        tappedCount = 0;
                    }
                })

            });
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                bool AnswerYes = await Application.Current.MainPage.DisplayAlert("Confirmation", "Exit Application?", "Yes", "No");
                if (AnswerYes)
                {
                    if (Device.RuntimePlatform == Device.Android)
                    {
                        Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
                    }
                    else if (Device.RuntimePlatform == Device.iOS)
                    {
                        Thread.CurrentThread.Abort();
                    }
                }
            }
            );
            return true;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            Plugin.Fingerprint.Abstractions.FingerprintAvailability isAvailableFingerprint = await CrossFingerprint.Current.GetAvailabilityAsync();
            if (isAvailableFingerprint != Plugin.Fingerprint.Abstractions.FingerprintAvailability.Available)
            {
                imgFingerprint.IsVisible = false;
            }

            LoginForm loginForm = Store.LoginForm.GetState();
            if (loginForm != null)
            {
                UserIdEntry.Text = loginForm.UserId;
            }
            VersionLabel.Text = $"Ver. {DependencyService.Get<IDeviceProperties>().VersionName}";

            if (isForcedLogout)
            {
                await PopupNavigation.Instance.PushAsync(new SessionExpiredDialog());
            }
        }

        private async void LoginAsync(string userId, string password, bool useButton = false)
        {
            UserIdEntry.IsEnabled = false;
            PasswordEntry.IsEnabled = false;
            LoginButton.IsEnabled = false;
            try
            {
                gridSpinner.IsVisible = true;
                using (CancellationTokenSource cancellationTokenSource = new CancellationTokenSource())
                {
                    cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(30));
                    LoginRequest loginRequest = new LoginRequest(
                        userId: userId,
                        password: password,
                        version: DependencyService.Get<IDeviceProperties>().VersionName,
                        hashedPassword: _hashedPassword
                    );

                    Store.AthenaSession.SetSession(new AthenaSession(
                          userId: userId,
                          brokerAccountId: null,
                          useAppWithoutBrokerAccount: false
                      ));

                    lblStatus.Text = "Login...";
                    LoginResponse loginResponse = await Store.AthenaSession.GetState().Client.LoginAsync(loginRequest, cancellationTokenSource.Token);

                    if (loginResponse.IsSuccessStatusMessage || loginResponse.IsPasswordExpiredMessage)
                    {
                        if (useButton)
                        {
                            LoginForm loginForm = Store.LoginForm.GetState();
                            if (loginForm != null && loginForm.UserId != userId)
                            {
                                AppSettings.Fingerprint_Use = false;
                            }
                        }

                        Store.LoginForm.Set(new LoginForm(
                            userId: userId,
                            password: Password.encrypt(password),
                            hashedPassword: loginRequest.HashedPassword
                            ));

                        AppSettings.Encrypted = "1";

                        DependencyService.Get<IToastMessage>().Show("Login Succeed.");
                        lblStatus.Text = "Initializing...";

                        List<StockInitMessage2> lstStockInitRequest = null;
                        lstStockInitRequest = await StockInitClient2.Default.GetStockInitAsync(new CancellationTokenSource(TimeSpan.FromSeconds(60)).Token);
                        Store.StockById.Init2(lstStockInitRequest);
                        lstStockInitRequest = null;

                        DependencyService.Get<IToastMessage>().Show("Initialized.");
                        IsEnabled = false;

                        if (loginResponse.IsPasswordExpiredMessage)
                        {
                            await DisplayAlert("Warning", "Password Expired. Please change Password on Configuration Menu.", "Ok");
                        }

                        Application.Current.MainPage = new MainPage();
                    }
                    else
                    {
                        DependencyService.Get<IToastMessage>().Show(loginResponse.Message);
                    }
                }
            }
            catch (OperationCanceledException)
            {
                DependencyService.Get<IToastMessage>().Show("Connection Timeout");
            }
            catch (WebException)
            {
                DependencyService.Get<IToastMessage>().Show("Connection Error");
            }
            catch (Exception)
            {
                DependencyService.Get<IToastMessage>().Show("Connection Timeout.");
            }
            finally
            {
                UserIdEntry.IsEnabled = true;
                PasswordEntry.IsEnabled = true;
                LoginButton.IsEnabled = true;
                gridSpinner.IsVisible = false;
            }
        }

        private void LoginButton_Clicked(object sender, EventArgs e)
        {
            LoginAsync(UserIdEntry.Text, PasswordEntry.Text, true);
        }

        private void Entries_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(UserIdEntry.Text) && !string.IsNullOrEmpty(PasswordEntry.Text))
            {
                LoginButton.IsEnabled = true;
                LoginButton.BackgroundColor = Color.FromRgb(0x88, 0x00, 0x00);
                LoginButton.BorderColor = Color.FromRgb(0x88, 0x00, 0x00);
            }
            else
            {
                LoginButton.IsEnabled = false;
                LoginButton.BackgroundColor = Color.Transparent;
                LoginButton.BorderColor = Color.FromRgb(0xaa, 0x74, 0x74);
            }
            if (string.IsNullOrWhiteSpace(UserIdEntry.Text))
            {
                Store.LoginForm.Clear();
            }
        }
    }
}