﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.AthenaMessages.Responses;
using POST.Dialogs;
using POST.Models;
using POST.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.OrdersPages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderListPage : ContentPage
    {
        private DateTime lastRefresh = DateTime.Now;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
        private readonly View ViewProgressDisplay;

        public OrderListPage()
        {
            InitializeComponent();

            ViewProgressDisplay = CreateProgressDisplay();
            ViewProgressDisplay.IsVisible = false;
            GridMain.Children.Add(ViewProgressDisplay, 0, 0);

            CreateDataGrid();

            horizontalScrollView.Scrolled += async (sender, args) =>
            {
                await headerScrollView.ScrollToAsync(args.ScrollX, 0.0, false);
            };

            headerScrollView.Scrolled += async (sender, args) =>
            {
                await horizontalScrollView.ScrollToAsync(args.ScrollX, 0.0, false);
            };
        }

        private class CustomGrid : Grid
        {
            public int XRow = 0;
            public int YColumn = 0;
        }

        private enum ColumnSort
        {
            ASC,
            DSC
        }

        private ColumnSort columnSort = ColumnSort.ASC;

        private class GridSortSign : Grid
        {
            public ColumnSort columnSort = ColumnSort.ASC;
            private readonly Label lblSortForeSign;
            private readonly Label lblSortBackgroundSign;
            public GridSortSign()
            {
                lblSortBackgroundSign = new Label()
                {
                    Text = Glyphs.BackgroundSort,
                    TextColor = Colors.NetralSortColor,
                    FontSize = 12,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 0, -2),
                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                    HorizontalOptions = LayoutOptions.Start
                };
                lblSortForeSign = new Label()
                {
                    Text = Glyphs.Unchanged,
                    TextColor = Colors.AscDescSortColor,
                    FontSize = 11,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 0, -2),
                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                    HorizontalOptions = LayoutOptions.Start
                };
                Children.Add(lblSortBackgroundSign, 0, 0);
                Children.Add(lblSortForeSign, 0, 0);
            }

            public void SetAsc()
            {
                lblSortForeSign.Text = Glyphs.Gaining;
            }

            public void SetDesc()
            {
                lblSortForeSign.Text = Glyphs.Losing;
            }

            public void SetNetral()
            {
                lblSortForeSign.Text = Glyphs.Unchanged;
            }
        }

        private readonly List<GridSortSign> lstGridSortSign = new List<GridSortSign>();

        private CustomGrid ToHeader(string title, int yColumn, LayoutOptions layoutOptions)
        {
            Label lblTitle = new Label()
            {
                Text = title,
                Style = (Style)Resources["HeaderLabelStyle"]
            };
            GridSortSign gridSortSign = new GridSortSign();
            lstGridSortSign.Add(gridSortSign);

            CustomGrid customGrid = new CustomGrid()
            {
                HorizontalOptions = layoutOptions,
                YColumn = yColumn,
                ColumnSpacing = 1,
                ColumnDefinitions = new ColumnDefinitionCollection() { new ColumnDefinition { Width = GridLength.Auto }, new ColumnDefinition { Width = GridLength.Auto } },
                Children = {
                            lblTitle,
                           gridSortSign
                        }
            };

            Grid.SetRow(lblTitle, 0);
            Grid.SetColumn(lblTitle, 0);
            Grid.SetRow(gridSortSign, 0);
            Grid.SetColumn(gridSortSign, 1);

            customGrid.GestureRecognizers.Add(
                new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        if (BindingContext != null && (List<Order>)BindingContext is List<Order> lstOrder && lstOrder.Count > 0)
                        {
                            ResetColumnHeader();

                            if (columnSort == ColumnSort.ASC)
                            {
                                columnSort = ColumnSort.DSC;
                                gridSortSign.SetDesc();
                            }
                            else
                            {
                                columnSort = ColumnSort.ASC;
                                gridSortSign.SetAsc();
                            }

                            switch (yColumn)
                            {
                                case 0:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.StockCode).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.StockCode).ToList();
                                    }
                                    break;

                                case 1:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.Action).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.Action).ToList();
                                    }
                                    break;

                                case 2:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.Price).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.Price).ToList();
                                    }
                                    break;

                                case 3:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.Volume).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.Volume).ToList();
                                    }
                                    break;

                                case 4:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.RemainingVolume).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.RemainingVolume).ToList();
                                    }
                                    break;

                                case 5:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.TradedVolume).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.TradedVolume).ToList();
                                    }
                                    break;

                                case 6:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.OrderStatus).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.OrderStatus).ToList();
                                    }
                                    break;

                                case 7:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.OrderId).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.OrderId).ToList();
                                    }
                                    break;

                                case 8:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.Placed).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.Placed).ToList();
                                    }
                                    break;
                            }
                            UpdateDataGrid(lstOrder);
                        }
                    })
                });

            return customGrid;
        }

        private void UpdateColumn(Grid grid, string[] arrStr, LayoutOptions layoutOptions)
        {
            int iRow = 0;

            if (grid.RowDefinitions == null)
            {
                grid.RowDefinitions = new RowDefinitionCollection();
            }

            Label lblStockCode = null;

            foreach (string str in arrStr)
            {
                if (grid.RowDefinitions.Count <= iRow)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = 30 });

                    lblStockCode = new Label()
                    {
                        HorizontalOptions = layoutOptions,
                        Text = str,
                        TextColor = Colors.FieldValueDefaultColor,
                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                        VerticalOptions = LayoutOptions.Center,
                        FontSize = 12,
                        LineBreakMode = LineBreakMode.TailTruncation
                    };

                    CustomGrid customGrid = new CustomGrid()
                    {
                        XRow = iRow,
                        Children = {
                            lblStockCode
                        }
                    };

                    customGrid.GestureRecognizers.Add(
                        new TapGestureRecognizer
                        {
                            Command = new Command(async () =>
                            {
                                if (BindingContext != null && (List<Order>)BindingContext is List<Order> lstOrder && lstOrder.Count > 0)
                                {
                                    Order order = lstOrder[customGrid.XRow];
                                    if (order.OrderStatus == OrderStatus.Open)
                                    {
                                        if (Store.StockById.GetState().TryGetValue(new StockId(order.StockCode, order.MarketType), out Stock stock))
                                        {
                                            Store.RecentStockIds.AddStock(stock.Id);

                                            if (stock.OpenOrPrevious.HasValue)
                                            {
                                                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                                {
                                                    await PopupNavigation.Instance.PopAllAsync();
                                                }

                                                AmendWithdrawDialog amendWithdrawDialog = new AmendWithdrawDialog(
                                                order: order,
                                                openPrice: stock.OpenOrPrevious.Value,
                                                sharesPerLot: stock.SharesPerLot.Value
                                                );
                                                await PopupNavigation.Instance.PushAsync(amendWithdrawDialog);
                                            }
                                            else
                                            {
                                                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                                {
                                                    await PopupNavigation.Instance.PopAllAsync();
                                                }

                                                AmendWithdrawDialog amendWithdrawDialog = new AmendWithdrawDialog(
                                                order: order,
                                                openPrice: 0,
                                                sharesPerLot: stock.SharesPerLot.Value
                                                );
                                                await PopupNavigation.Instance.PushAsync(amendWithdrawDialog);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Store.StockById.GetState().TryGetValue(new StockId(order.StockCode, order.MarketType), out Stock stock))
                                        {
                                            Store.RecentStockIds.AddStock(stock.Id);
                                            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                            {
                                                await PopupNavigation.Instance.PopAllAsync();
                                            }

                                            OrderInformationDialog orderInformationDialog = new OrderInformationDialog(
                                            order: order,
                                            sharesPerLot: stock.SharesPerLot.Value
                                        );
                                            await PopupNavigation.Instance.PushAsync(orderInformationDialog);
                                        }
                                    }
                                }
                            })
                        });
                    grid.Children.Add(customGrid, 0, iRow);
                }
                else
                {
                    grid.Children[iRow].HorizontalOptions = layoutOptions;
                    lblStockCode = ((Label)((CustomGrid)grid.Children[iRow]).Children[0]);
                    lblStockCode.Text = str;
                }

                //Syariah
                if (Store.StockById.GetState().TryGetValue(new StockId(str, MarketType.RG), out Stock stock2))
                {
                    lblStockCode.TextDecorations = stock2.TextDecoration;
                }

                iRow++;
            }

            int nRest = grid.RowDefinitions.Count - iRow;
            for (int i = 0; i < nRest; i++)
            {
                int n = grid.RowDefinitions.Count - 1;
                grid.Children.RemoveAt(n);
                grid.RowDefinitions.RemoveAt(n);
            }
        }

        private void UpdateColumn(Grid grid, string[] arrStr, Color[] arrTextColor, LayoutOptions layoutOptions)
        {
            int iRow = 0;

            if (grid.RowDefinitions == null)
            {
                grid.RowDefinitions = new RowDefinitionCollection();
            }

            foreach (string str in arrStr)
            {
                if (grid.RowDefinitions.Count <= iRow)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = 30 });

                    CustomGrid customGrid = new CustomGrid()
                    {
                        XRow = iRow,
                        Children = {
                           new Label
                    {
                        HorizontalOptions = layoutOptions,
                        Text = str,
                        TextColor = arrTextColor[iRow],
                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                        VerticalOptions = LayoutOptions.Center,
                        FontSize = 12,
                        LineBreakMode = LineBreakMode.TailTruncation,
                    }
                        }
                    };
                    customGrid.GestureRecognizers.Add(
                        new TapGestureRecognizer
                        {
                            Command = new Command(async () =>
                            {
                                if (BindingContext != null && (List<Order>)BindingContext is List<Order> lstOrder && lstOrder.Count > 0)
                                {
                                    Order order = lstOrder[customGrid.XRow];
                                    if (order.OrderStatus == OrderStatus.Open)
                                    {
                                        if (Store.StockById.GetState().TryGetValue(new StockId(order.StockCode, order.MarketType), out Stock stock))
                                        {
                                            Store.RecentStockIds.AddStock(stock.Id);

                                            if (stock.OpenOrPrevious.HasValue)
                                            {
                                                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                                {
                                                    await PopupNavigation.Instance.PopAllAsync();
                                                }

                                                AmendWithdrawDialog amendWithdrawDialog = new AmendWithdrawDialog(
                                                order: order,
                                                openPrice: stock.OpenOrPrevious.Value,
                                                sharesPerLot: stock.SharesPerLot.Value
                                                );
                                                await PopupNavigation.Instance.PushAsync(amendWithdrawDialog);
                                            }
                                            else
                                            {
                                                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                                {
                                                    await PopupNavigation.Instance.PopAllAsync();
                                                }

                                                AmendWithdrawDialog amendWithdrawDialog = new AmendWithdrawDialog(
                                                order: order,
                                                openPrice: 0,
                                                sharesPerLot: stock.SharesPerLot.Value
                                                );
                                                await PopupNavigation.Instance.PushAsync(amendWithdrawDialog);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Store.StockById.GetState().TryGetValue(new StockId(order.StockCode, order.MarketType), out Stock stock))
                                        {
                                            Store.RecentStockIds.AddStock(stock.Id);
                                            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                            {
                                                await PopupNavigation.Instance.PopAllAsync();
                                            }

                                            OrderInformationDialog orderInformationDialog = new OrderInformationDialog(
                                            order: order,
                                            sharesPerLot: stock.SharesPerLot.Value
                                        );
                                            await PopupNavigation.Instance.PushAsync(orderInformationDialog);
                                        }
                                    }
                                }
                            })
                        });
                    grid.Children.Add(customGrid, 0, iRow);
                }
                else
                {
                    grid.Children[iRow].HorizontalOptions = layoutOptions;
                    ((Label)((CustomGrid)grid.Children[iRow]).Children[0]).Text = str;
                    ((Label)((CustomGrid)grid.Children[iRow]).Children[0]).TextColor = arrTextColor[iRow];
                }
                iRow++;
            }

            int nRest = grid.RowDefinitions.Count - iRow;
            for (int i = 0; i < nRest; i++)
            {
                int n = grid.RowDefinitions.Count - 1;
                grid.Children.RemoveAt(n);
                grid.RowDefinitions.RemoveAt(n);
            }
        }

        private void UpdateColumn(Grid grid, decimal[] arrValue, string strFormat, LayoutOptions layoutOptions)
        {
            int iRow = 0;

            if (grid.RowDefinitions == null)
            {
                grid.RowDefinitions = new RowDefinitionCollection();
            }

            foreach (decimal val in arrValue)
            {
                if (grid.RowDefinitions.Count <= iRow)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = 30 });
                    CustomGrid customGrid = new CustomGrid()
                    {
                        XRow = iRow,
                        Children = {
                           new Label
                    {
                        HorizontalOptions = layoutOptions,
                        Text = val.ToString(strFormat),
                        TextColor = Colors.FieldValueDefaultColor,
                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                        VerticalOptions = LayoutOptions.Center,
                        FontSize = 12,
                        LineBreakMode = LineBreakMode.TailTruncation,
                    }
                        }
                    };

                    customGrid.GestureRecognizers.Add(
                        new TapGestureRecognizer
                        {
                            Command = new Command(async () =>
                            {
                                if (BindingContext != null && (List<Order>)BindingContext is List<Order> lstOrder && lstOrder.Count > 0)
                                {
                                    Order order = lstOrder[customGrid.XRow];
                                    if (order.OrderStatus == OrderStatus.Open)
                                    {
                                        if (Store.StockById.GetState().TryGetValue(new StockId(order.StockCode, order.MarketType), out Stock stock))
                                        {
                                            Store.RecentStockIds.AddStock(stock.Id);

                                            if (stock.OpenOrPrevious.HasValue)
                                            {
                                                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                                {
                                                    await PopupNavigation.Instance.PopAllAsync();
                                                }

                                                AmendWithdrawDialog amendWithdrawDialog = new AmendWithdrawDialog(
                                                order: order,
                                                openPrice: stock.OpenOrPrevious.Value,
                                                sharesPerLot: stock.SharesPerLot.Value
                                                );
                                                await PopupNavigation.Instance.PushAsync(amendWithdrawDialog);
                                            }
                                            else
                                            {
                                                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                                {
                                                    await PopupNavigation.Instance.PopAllAsync();
                                                }

                                                AmendWithdrawDialog amendWithdrawDialog = new AmendWithdrawDialog(
                                                order: order,
                                                openPrice: 0,
                                                sharesPerLot: stock.SharesPerLot.Value
                                                );
                                                await PopupNavigation.Instance.PushAsync(amendWithdrawDialog);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Store.StockById.GetState().TryGetValue(new StockId(order.StockCode, order.MarketType), out Stock stock))
                                        {
                                            Store.RecentStockIds.AddStock(stock.Id);
                                            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                                            {
                                                await PopupNavigation.Instance.PopAllAsync();
                                            }

                                            OrderInformationDialog orderInformationDialog = new OrderInformationDialog(
                                            order: order,
                                            sharesPerLot: stock.SharesPerLot.Value
                                        );
                                            await PopupNavigation.Instance.PushAsync(orderInformationDialog);
                                        }
                                    }
                                }
                            })
                        });
                    grid.Children.Add(customGrid, 0, iRow);
                }
                else
                {
                    ((Label)((CustomGrid)grid.Children[iRow]).Children[0]).Text = val.ToString(strFormat);
                }
                iRow++;
            }

            int nRest = grid.RowDefinitions.Count - iRow;
            for (int i = 0; i < nRest; i++)
            {
                int n = grid.RowDefinitions.Count - 1;
                grid.Children.RemoveAt(n);
                grid.RowDefinitions.RemoveAt(n);
            }
        }

        private void UpdateDataGrid(List<Order> lstOrder)
        {
            BindingContext = lstOrder;

            try
            {   //Code
                UpdateColumn((Grid)RowHeaders.Children[0], lstOrder.Select(o => o.StockCode).ToArray(), LayoutOptions.Center);

                //Order 
                UpdateColumn((Grid)RowHeaders.Children[1], lstOrder.Select(o => o.Action).ToArray(), lstOrder.Select(o => o.ActionForeground).ToArray(), LayoutOptions.Center);

                //Price
                UpdateColumn((Grid)columnsGrid.Children[0], lstOrder.Select(o => o.Price).ToArray(), "N0", LayoutOptions.End);

                //Volume
                UpdateColumn((Grid)columnsGrid.Children[1], lstOrder.Select(o => o.Volume).ToArray(), "N0", LayoutOptions.End);

                //Remaining
                UpdateColumn((Grid)columnsGrid.Children[2], lstOrder.Select(o => o.RemainingVolume).ToArray(), "N0", LayoutOptions.End);

                //Traded
                UpdateColumn((Grid)columnsGrid.Children[3], lstOrder.Select(o => o.TradedVolume).ToArray(), "N0", LayoutOptions.End);

                //Status
                UpdateColumn((Grid)columnsGrid.Children[4], lstOrder.Select(o => o.OrderStatusCode).ToArray(), lstOrder.Select(o => o.OrderStatusForeground).ToArray(), LayoutOptions.Center);

                //OrderID
                UpdateColumn((Grid)columnsGrid.Children[5], lstOrder.Select(o => o.OrderId).ToArray(), LayoutOptions.Center);

                //Time
                UpdateColumn((Grid)columnsGrid.Children[6], lstOrder.Select(o => o.Placed).ToArray(), LayoutOptions.Center);
            }
            catch
            {

            }
        }

        private void CreateDataGrid()
        {
            fixedGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(45) });
            fixedGrid.Children.Add(ToHeader("Code", 0, LayoutOptions.Center), fixedGrid.Children.Count, 0);

            fixedGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            fixedGrid.Children.Add(ToHeader("Order", 1, LayoutOptions.Center), fixedGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(45) });
            headerGrid.Children.Add(ToHeader("Price", 2, LayoutOptions.End), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(50) });
            headerGrid.Children.Add(ToHeader("Volume", 3, LayoutOptions.End), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(65) });
            headerGrid.Children.Add(ToHeader("Remaining", 4, LayoutOptions.End), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(50) });
            headerGrid.Children.Add(ToHeader("Traded", 5, LayoutOptions.End), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(50) });
            headerGrid.Children.Add(ToHeader("Status", 6, LayoutOptions.Center), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(80) });
            headerGrid.Children.Add(ToHeader("OrderID", 7, LayoutOptions.Center), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60) });
            headerGrid.Children.Add(ToHeader("Time", 8, LayoutOptions.Center), headerGrid.Children.Count, 0);

            //////////////////////////////////////////////////////////////////////

            RowHeaders.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(45) });
            RowHeaders.Children.Add(new Grid(), RowHeaders.Children.Count, 0);

            RowHeaders.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            RowHeaders.Children.Add(new Grid(), RowHeaders.Children.Count, 0);

            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(45) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);

            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(50) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);

            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(65) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);

            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(50) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);

            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(50) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);

            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(80) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);

            columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60) });
            columnsGrid.Children.Add(new Grid(), columnsGrid.Children.Count, 0);
        }

        private void ResetColumnHeader()
        {
            lstGridSortSign.ForEach(grd => grd.SetNetral());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.AthenaSession.Subscribe(athenaSession =>
            {
                if (athenaSession != null &&
                    athenaSession.BrokerAccountId != null)
                {
                    RefreshPageAsync(false);
                }
                else
                {
                    ViewProgressDisplay.IsVisible = false;
                    ResetColumnHeader();
                    ResetLblTotal();
                    UpdateDataGrid(new List<Order>());
                }
            }));
        }

        protected override void OnDisappearing()
        {
            ResetColumnHeader();
            columnSort = ColumnSort.ASC;

            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
            if (!_cancellationTokenSource.IsCancellationRequested)
            {
                _cancellationTokenSource.Cancel();
            }
            base.OnDisappearing();
        }

        private void CalculateTotalSummaryAndUpdateLabel(List<Order> lstOrder, decimal feeBuy, decimal feeSell)
        {
            decimal buy = 0m;
            decimal sell = 0m;
            decimal net = 0m;

            foreach (Order order in lstOrder)
            {
                if (order.OrderStatus == OrderStatus.Rejected)
                {
                    continue;
                }

                if (order.OrderStatus == OrderStatus.Canceled && order.TradedVolume == 0m)
                {
                    continue;
                }

                if (order.RemainingVolume > 0m || order.TradedVolume > 0m)
                {
                    if (order.OrderAction == Models.OrderAction.Buy)
                    {
                        if (Store.StockById.GetState().TryGetValue(new StockId(order.StockCode, order.MarketType), out Stock stock))
                        {
                            buy += (order.RemainingVolume * order.Price * stock.SharesPerLot.Value) + (order.TradedVolume * order.Price * stock.SharesPerLot.Value);
                        }
                    }
                    else
                    {
                        if (Store.StockById.GetState().TryGetValue(new StockId(order.StockCode, order.MarketType), out Stock stock))
                        {
                            sell += (order.RemainingVolume * order.Price * stock.SharesPerLot.Value) + (order.TradedVolume * order.Price * stock.SharesPerLot.Value);
                        }
                    }
                }
            }

            buy = Math.Floor(buy * (1 + Math.Round(feeBuy, 3)));
            sell = Math.Floor(sell * (1 - Math.Round(feeSell, 3)));

            net = sell - buy;

            ResetLblTotal(buy, sell, net);
        }

        private async Task<int> LoadOrdersAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            try
            {
                ViewProgressDisplay.IsVisible = true;

                TradingLimitResponse tradingLimitResponse = await AthenaClient.Current.GetTradingLimitAsync(brokerAccountId, cancellationToken);

                Order[] athenaResponse = await AthenaClient.Current.GetOrdersAsync(brokerAccountId, cancellationToken);
                if (athenaResponse != null)
                {
                    List<Order> lstOrder = athenaResponse.ToList();
                    ResetColumnHeader();
                    UpdateDataGrid(lstOrder);
                    CalculateTotalSummaryAndUpdateLabel(lstOrder, tradingLimitResponse.FeeBuy, tradingLimitResponse.FeeSell);

                    return 1;
                }
                else
                {
                    ResetColumnHeader();
                    ResetLblTotal();
                    UpdateDataGrid(new List<Order>());
                    return 2;
                }
            }
            catch (OperationCanceledException)
            {
                return 3;
            }
            catch
            {
                ResetColumnHeader();
                ResetLblTotal();
                UpdateDataGrid(new List<Order>());
                return 4;
            }
            finally
            {
                ViewProgressDisplay.IsVisible = false;
            }
        }

        private void ResetLblTotal(
            decimal buy = 0m,
            decimal sell = 0m,
            decimal net = 0m)
        {
            lblBuy.Text = buy.ToString("N0");
            lblSell.Text = sell.ToString("N0");
            lblNet.Text = net.ToString("N0");

            if (net > 0m)
            {
                lblNet.TextColor = Colors.GainForeground;
            }
            else if (net < 0m)
            {
                lblNet.TextColor = Colors.LoseForeground;
            }
            else
            {
                lblNet.TextColor = Colors.UnchangedForeground;
            }
        }

        public async void RefreshPageAsync(bool showMessage = true)
        {
            if (Store.AthenaSession.GetState() != null &&
                    Store.AthenaSession.GetState().BrokerAccountId != null)
            {
                if (ViewProgressDisplay.IsVisible)
                {
                    return;
                }

                lastRefresh = DateTime.Now;
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
                int result = await LoadOrdersAsync(Store.AthenaSession.GetState().BrokerAccountId, _cancellationTokenSource.Token);
                if (result == 1)
                {
                    if (showMessage)
                    {
                        DependencyService.Get<IToastMessage>().Show("Order List Loaded.");
                    }
                }
                else if (result == 2)
                {
                    DependencyService.Get<IToastMessage>().Show("Null Order.");
                }
                else if (result == 3)
                {
                    if (DateTime.Now.CompareTo(lastRefresh.AddSeconds(10)) >= 0)
                    {
                        DependencyService.Get<IToastMessage>().Show("Request timeout.");
                    }
                }
                else if (result == 4)
                {
                    DependencyService.Get<IToastMessage>().Show("Request failed.");
                }
            }
            else
            {
                ViewProgressDisplay.IsVisible = false;
                ResetColumnHeader();
                ResetLblTotal();
                UpdateDataGrid(new List<Order>());
            }
        }

        public View CreateProgressDisplay()
        {
            Grid gridProgress = new Grid { BackgroundColor = (Color)App.Current.Resources["PageBackgroundColor"], Padding = new Thickness(50) };
            gridProgress.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            gridProgress.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            gridProgress.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //gridProgress.SetBinding(VisualElement.IsVisibleProperty, "IsWorking");
            ActivityIndicator activity = new ActivityIndicator
            {
                IsEnabled = true,
                IsVisible = true,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                IsRunning = true
            };
            gridProgress.Children.Add(activity, 0, 1);

            return gridProgress;
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

    }
}
