﻿using POST.DataSources.AthenaMessages.Responses;
using POST.Dialogs;
using POST.Models;
using POST.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.OrdersPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateConditionalOrderPage : ContentPage
    {
        private readonly Stock stock;
        private readonly List<string> lstOrderType;
        private readonly List<string> lstOrderCondition;
        private readonly List<string> lstPriceCondition;
        private readonly List<string> lstVolumeCondition;
        private readonly List<string> lstHigherLowerThan;
        private readonly List<string> lstHH;
        private readonly List<string> lstMM;
        private readonly List<DateTime> lstDate;
        private readonly List<string> lstDateString;

        public CreateConditionalOrderPage(Stock stock)
        {
            InitializeComponent();
            Title = "Create Conditional Order";
            lstOrderType = new List<string>() { "BUY", "SELL" };
            lstOrderCondition = new List<string>() { "Date and Time", "Price and Volume" };
            lstPriceCondition = new List<string>() { "LAST DONE", "BEST BID", "BEST OFFER" };
            lstVolumeCondition = new List<string>() { "NONE", "BEST BID", "BEST OFFER" };
            lstHigherLowerThan = new List<string>() { ">=", "<=" };

            lstHH = new List<string>();
            for (int i = 0; i <= 23; i++)
            {
                lstHH.Add(i.ToString().PadLeft(2, '0'));
            }
            lstMM = new List<string>();
            for (int i = 0; i <= 59; i++)
            {
                lstMM.Add(i.ToString().PadLeft(2, '0'));
            }

            lstDate = new List<DateTime>();
            lstDateString = new List<string>();
            short iDate = 0, iCounter = 0;
            while (iDate < 7)
            {
                DateTime dateTmp = DateTime.Now.AddDays(iCounter);
                if (dateTmp.DayOfWeek != DayOfWeek.Sunday && dateTmp.DayOfWeek != DayOfWeek.Saturday)
                {
                    lstDate.Add(dateTmp);
                    lstDateString.Add(dateTmp.ToString("dddd, dd MMM yyyy"));
                    iDate++;
                }
                iCounter++;
            }

            if (stock.Id.Code.ToLower().Contains("-r"))
            {
                if (Store.StockById.GetState().TryGetValue(new StockId(stock.Id.Code, MarketType.TN), out Stock stockTmp))
                {
                    stock = stockTmp;
                }
            }

            this.stock = stock;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            lblStockCode.Text = stock.Id.Code;
            lblStockName.Text = stock.Name;
            txtPrice.Placeholder = "Enter Price";
            txtVolume.Placeholder = "Enter Volume";

            pickerOrder.ItemsSource = lstOrderType;
            pickerCondition.ItemsSource = lstOrderCondition;
            pickerPriceCondition.ItemsSource = lstPriceCondition;
            pickerPriceHigherLowerThan.ItemsSource = lstHigherLowerThan;
            pickerVolumeCondition.ItemsSource = lstVolumeCondition;
            pickerVolumeHigherLowerThan.ItemsSource = lstHigherLowerThan;
            pickerTimeHH.ItemsSource = lstHH;
            pickerTimeMM.ItemsSource = lstMM;
            pickerDateCondition.ItemsSource = lstDateString;

            pickerCondition.SelectedIndex = 0;
            pickerPriceCondition.SelectedIndex = 0;
            pickerPriceHigherLowerThan.SelectedIndex = 0;
            pickerVolumeCondition.SelectedIndex = 0;
            pickerVolumeHigherLowerThan.SelectedIndex = 0;
            pickerTimeHH.SelectedIndex = 0;
            pickerTimeMM.SelectedIndex = 0;

            lblStockCode.TextDecorations = stock.TextDecoration;
            lblStockName.TextDecorations = stock.TextDecoration;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void pickerCondition_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pickerCondition.SelectedIndex == 0) //Date and Time
            {
                gridDateTimeCondition.IsVisible = true;
                gridPriceVolumeCondition.IsVisible = false;
            }
            else //Price and Volume
            {
                gridDateTimeCondition.IsVisible = false;
                gridPriceVolumeCondition.IsVisible = true;
            }
        }

        private async void btnCreate_Clicked(object sender, EventArgs e)
        {
            if (pickerOrder.SelectedIndex == -1)
            {
                DependencyService.Get<IToastMessage>().Show("Choose Order!");
                return;
            }
            if (string.IsNullOrEmpty(txtPrice.Text))
            {
                DependencyService.Get<IToastMessage>().Show("Enter Price!");
                return;
            }
            if (string.IsNullOrEmpty(txtVolume.Text))
            {
                DependencyService.Get<IToastMessage>().Show("Enter Volume!");
                return;
            }
            if (pickerCondition.SelectedIndex == 0)
            {
                if (pickerDateCondition.SelectedIndex == -1)
                {
                    DependencyService.Get<IToastMessage>().Show("Select Date!");
                    return;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(txtPriceCondition.Text))
                {
                    DependencyService.Get<IToastMessage>().Show("Enter Price Condition!");
                    return;
                }
                if (pickerVolumeCondition.SelectedIndex != 0 && string.IsNullOrEmpty(txtVolumeCondition.Text))
                {
                    DependencyService.Get<IToastMessage>().Show("Enter Volume Condition!");
                    return;
                }
            }

            string strCondition = "";
            string strPriceCondition = "0";
            string strVolCondition = "0";
            string strDateTimeCondition = "";

            if (pickerCondition.SelectedIndex == 0) // time
            {
                strCondition = "10";
                DateTime selectedDateTime = lstDate[pickerDateCondition.SelectedIndex];
                strDateTimeCondition = selectedDateTime.ToString("MM-dd-yyyy") + " " + pickerTimeHH.SelectedItem.ToString() + ":" + pickerTimeMM.SelectedItem.ToString() + ":00";
            }
            else
            {
                if (pickerPriceCondition.SelectedIndex == 0) // last done
                {
                    strCondition = "00";
                }
                else if (pickerPriceCondition.SelectedIndex == 1) // best bid
                {
                    strCondition = "01";
                }
                else if (pickerPriceCondition.SelectedIndex == 2) // best offer
                {
                    strCondition = "02";
                }

                if (pickerPriceHigherLowerThan.SelectedIndex == 0) // >=
                {
                    strCondition += "0";
                }
                else // <=
                {
                    strCondition += "1";
                }

                strPriceCondition = txtPriceCondition.Text;

                if (pickerVolumeCondition.SelectedIndex == 0)
                {
                    strCondition += "0";
                }
                else
                {
                    strCondition += "1";

                    if (pickerVolumeCondition.SelectedIndex == 1) //best bid vol
                    {
                        strCondition += "0";
                    }
                    else if (pickerVolumeCondition.SelectedIndex == 2) // best offer vol
                    {
                        strCondition += "1";
                    }

                    if (pickerVolumeHigherLowerThan.SelectedIndex == 0) // >=
                    {
                        strCondition += "0";
                    }
                    else // <=
                    {
                        strCondition += "1";
                    }

                    strVolCondition = txtVolumeCondition.Text;
                }
            }

            strCondition = strCondition.PadRight(6, '0');

            string BuyOrSell = (pickerOrder.SelectedIndex == 0) ? "0" : "1";

            CultureInfo usProvider = new CultureInfo("en-US");
            ConditionalOrder conditionalOrder = new ConditionalOrder("", "", "", "", stock.Id.Code, stock.Id.MarketType.ToString(), decimal.Parse(txtVolume.Text, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(txtPrice.Text, System.Globalization.NumberStyles.Any, usProvider), BuyOrSell, strCondition, decimal.Parse(strPriceCondition, System.Globalization.NumberStyles.Any, usProvider), decimal.Parse(strVolCondition, System.Globalization.NumberStyles.Any, usProvider), ConditionalOrderStatus.NotExecuted.ToString(), "", "1", strDateTimeCondition);
            ConditionalOrderConfirmationDialog conditionalOrderConfirmationDialog = new ConditionalOrderConfirmationDialog(conditionalOrder, stock.SharesPerLot.Value);
            await PopupNavigation.Instance.PushAsync(conditionalOrderConfirmationDialog);
        }

    }
}