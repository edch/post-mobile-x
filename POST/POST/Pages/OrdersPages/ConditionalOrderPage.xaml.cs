﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.AthenaMessages.Responses;
using POST.Dialogs;
using POST.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.OrdersPages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConditionalOrderPage : ContentPage
    {
        private DateTime lastRefresh = DateTime.Now;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));

        public ConditionalOrderPage()
        {
            InitializeComponent();

            CreateDataGrid();
        }

        private class CustomGrid : Grid
        {
            public int XRow = 0;
            public int YColumn = 0;
        }

        private enum ColumnSort
        {
            ASC,
            DSC
        }

        private ColumnSort columnSort = ColumnSort.ASC;

        private class GridSortSign : Grid
        {
            public ColumnSort columnSort = ColumnSort.ASC;
            private readonly Label lblSortForeSign;
            private readonly Label lblSortBackgroundSign;
            public GridSortSign()
            {
                lblSortBackgroundSign = new Label()
                {
                    Text = Glyphs.BackgroundSort,
                    TextColor = Colors.NetralSortColor,
                    FontSize = 12,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 0, -2),
                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                    HorizontalOptions = LayoutOptions.Start
                };
                lblSortForeSign = new Label()
                {
                    Text = Glyphs.Unchanged,
                    TextColor = Colors.AscDescSortColor,
                    FontSize = 11,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 0, -2),
                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                    HorizontalOptions = LayoutOptions.Start
                };
                Children.Add(lblSortBackgroundSign, 0, 0);
                Children.Add(lblSortForeSign, 0, 0);
            }

            public void SetAsc()
            {
                lblSortForeSign.Text = Glyphs.Gaining;
            }

            public void SetDesc()
            {
                lblSortForeSign.Text = Glyphs.Losing;
            }

            public void SetNetral()
            {
                lblSortForeSign.Text = Glyphs.Unchanged;
            }
        }

        private readonly List<GridSortSign> lstGridSortSign = new List<GridSortSign>();

        private CustomGrid ToHeader(string title, int yColumn, LayoutOptions layoutOptions)
        {
            Label lblTitle = new Label()
            {
                Text = title,
                Style = (Style)Resources["HeaderLabelStyle"]
            };
            GridSortSign gridSortSign = new GridSortSign();
            lstGridSortSign.Add(gridSortSign);

            CustomGrid customGrid = new CustomGrid()
            {
                HorizontalOptions = layoutOptions,
                YColumn = yColumn,
                ColumnSpacing = 1,
                ColumnDefinitions = new ColumnDefinitionCollection() { new ColumnDefinition { Width = GridLength.Auto }, new ColumnDefinition { Width = GridLength.Auto } },
                Children = {
                            lblTitle,
                           gridSortSign
                        }
            };

            Grid.SetRow(lblTitle, 0);
            Grid.SetColumn(lblTitle, 0);
            Grid.SetRow(gridSortSign, 0);
            Grid.SetColumn(gridSortSign, 1);

            customGrid.GestureRecognizers.Add(
                new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        if (BindingContext != null && (List<ConditionalOrder>)BindingContext is List<ConditionalOrder> lstOrder && lstOrder.Count > 0)
                        {
                            ResetColumnHeader();

                            if (columnSort == ColumnSort.ASC)
                            {
                                columnSort = ColumnSort.DSC;
                                gridSortSign.SetDesc();
                            }
                            else
                            {
                                columnSort = ColumnSort.ASC;
                                gridSortSign.SetAsc();
                            }

                            switch (yColumn)
                            {
                                case 0:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.StockCode).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.StockCode).ToList();
                                    }
                                    break;

                                case 1:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.Action).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.Action).ToList();
                                    }
                                    break;

                                case 2:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.O_Price).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.O_Price).ToList();
                                    }
                                    break;

                                case 3:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.O_Volume).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.O_Volume).ToList();
                                    }
                                    break;

                                case 4:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.ActiveLabelText).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.ActiveLabelText).ToList();
                                    }
                                    break;

                                case 5:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstOrder = lstOrder.OrderBy(ps => ps.ExecutedLabelText).ToList();
                                    }
                                    else
                                    {
                                        lstOrder = lstOrder.OrderByDescending(ps => ps.ExecutedLabelText).ToList();
                                    }
                                    break;
                            }
                            BindingContext = lstOrder;
                            ConditionalOrderListView.ItemsSource = lstOrder;
                        }
                    })
                });

            return customGrid;
        }

        private void CreateDataGrid()
        {
            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(50) });
            headerGrid.Children.Add(ToHeader("Code", 0, LayoutOptions.Center), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(35) });
            headerGrid.Children.Add(ToHeader("Order", 1, LayoutOptions.Center), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            headerGrid.Children.Add(ToHeader("Price", 2, LayoutOptions.End), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            headerGrid.Children.Add(ToHeader("Volume", 3, LayoutOptions.End), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(45) });
            headerGrid.Children.Add(ToHeader("Active", 4, LayoutOptions.Center), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(50) });
            headerGrid.Children.Add(ToHeader("Executed", 5, LayoutOptions.Center), headerGrid.Children.Count, 0);
        }

        private void ResetColumnHeader()
        {
            lstGridSortSign.ForEach(grd => grd.SetNetral());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.AthenaSession.Subscribe(athenaSession =>
            {
                if (athenaSession != null &&
                    athenaSession.BrokerAccountId != null)
                {
                    RefreshPageAsync(false);
                }
                else
                {
                    ResetColumnHeader();
                    ConditionalOrderListView.ItemsSource = null;
                }
            }));
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
            if (!_cancellationTokenSource.IsCancellationRequested)
            {
                _cancellationTokenSource.Cancel();
            }
            base.OnDisappearing();
        }

        private async Task<int> LoadOrdersAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            try
            {
                ConditionalOrder[] athenaResponse = await AthenaClient.Current.GetConditionalOrdersAsync(brokerAccountId, cancellationToken);
                if (athenaResponse != null)
                {
                    List<ConditionalOrder> lstConditionalOrder = athenaResponse.ToList();
                    ResetColumnHeader();
                    BindingContext = lstConditionalOrder;
                    ConditionalOrderListView.ItemsSource = athenaResponse;
                    return 1;
                }
                else
                {
                    ResetColumnHeader();
                    ConditionalOrderListView.ItemsSource = null;
                    return 2;
                }
            }
            catch (OperationCanceledException)
            {
                return 3;
            }
            catch
            {
                ResetColumnHeader();
                ConditionalOrderListView.ItemsSource = null;
                return 4;
            }
            finally
            {
                ConditionalOrderListView.IsRefreshing = false;
            }
        }

        public async void RefreshPageAsync(bool showMessage = true)
        {
            if (Store.AthenaSession.GetState() != null &&
                    Store.AthenaSession.GetState().BrokerAccountId != null)
            {
                lastRefresh = DateTime.Now;
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
                int result = await LoadOrdersAsync(Store.AthenaSession.GetState().BrokerAccountId, _cancellationTokenSource.Token);
                if (result == 1)
                {
                    if (showMessage)
                    {
                        DependencyService.Get<IToastMessage>().Show("Conditional Order Loaded.");
                    }
                }
                else if (result == 2)
                {
                    DependencyService.Get<IToastMessage>().Show("Null Order.");
                }
                else if (result == 3)
                {
                    if (DateTime.Now.CompareTo(lastRefresh.AddSeconds(10)) >= 0)
                    {
                        DependencyService.Get<IToastMessage>().Show("Request timeout.");
                    }
                }
                else if (result == 4)
                {
                    DependencyService.Get<IToastMessage>().Show("Request failed.");
                }
            }
            else
            {
                ResetColumnHeader();
                ConditionalOrderListView.ItemsSource = null;
            }
        }

        private async void AddButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            StockSearchPage stockSearchPage = new StockSearchPage("ConditionalOrderPage");
            await MainPage.NavigationPage.PushAsync(stockSearchPage);
        }
    }
}
