﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.AthenaMessages.Responses;
using POST.Dialogs;
using POST.Models;
using POST.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.OrdersPages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TradeDonePage : ContentPage
    {
        private DateTime lastRefresh = DateTime.Now;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));

        public TradeDonePage()
        {
            InitializeComponent();

            CreateDataGrid();
        }

        private class CustomGrid : Grid
        {
            public int XRow = 0;
            public int YColumn = 0;
        }

        private enum ColumnSort
        {
            ASC,
            DSC
        }

        private ColumnSort columnSort = ColumnSort.ASC;

        private class GridSortSign : Grid
        {
            public ColumnSort columnSort = ColumnSort.ASC;
            private readonly Label lblSortForeSign;
            private readonly Label lblSortBackgroundSign;
            public GridSortSign()
            {
                lblSortBackgroundSign = new Label()
                {
                    Text = Glyphs.BackgroundSort,
                    TextColor = Colors.NetralSortColor,
                    FontSize = 12,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 0, -2),
                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                    HorizontalOptions = LayoutOptions.Start
                };
                lblSortForeSign = new Label()
                {
                    Text = Glyphs.Unchanged,
                    TextColor = Colors.AscDescSortColor,
                    FontSize = 11,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 0, -2),
                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                    HorizontalOptions = LayoutOptions.Start
                };
                Children.Add(lblSortBackgroundSign, 0, 0);
                Children.Add(lblSortForeSign, 0, 0);
            }

            public void SetAsc()
            {
                lblSortForeSign.Text = Glyphs.Gaining;
            }

            public void SetDesc()
            {
                lblSortForeSign.Text = Glyphs.Losing;
            }

            public void SetNetral()
            {
                lblSortForeSign.Text = Glyphs.Unchanged;
            }
        }

        private readonly List<GridSortSign> lstGridSortSign = new List<GridSortSign>();

        private CustomGrid ToHeader(string title, int yColumn, LayoutOptions layoutOptions)
        {
            Label lblTitle = new Label()
            {
                Text = title,
                Style = (Style)Resources["HeaderLabelStyle"]
            };
            GridSortSign gridSortSign = new GridSortSign();
            lstGridSortSign.Add(gridSortSign);

            CustomGrid customGrid = new CustomGrid()
            {
                HorizontalOptions = layoutOptions,
                YColumn = yColumn,
                ColumnSpacing = 1,
                ColumnDefinitions = new ColumnDefinitionCollection() { new ColumnDefinition { Width = GridLength.Auto }, new ColumnDefinition { Width = GridLength.Auto } },
                Children = {
                            lblTitle,
                           gridSortSign
                        }
            };

            Grid.SetRow(lblTitle, 0);
            Grid.SetColumn(lblTitle, 0);
            Grid.SetRow(gridSortSign, 0);
            Grid.SetColumn(gridSortSign, 1);

            customGrid.GestureRecognizers.Add(
                new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        if (BindingContext != null && (List<TradeDone>)BindingContext is List<TradeDone> lstTradeDone && lstTradeDone.Count > 0)
                        {
                            ResetColumnHeader();

                            if (columnSort == ColumnSort.ASC)
                            {
                                columnSort = ColumnSort.DSC;
                                gridSortSign.SetDesc();
                            }
                            else
                            {
                                columnSort = ColumnSort.ASC;
                                gridSortSign.SetAsc();
                            }

                            switch (yColumn)
                            {
                                case 0:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstTradeDone = lstTradeDone.OrderBy(ps => ps.TradeTime).ToList();
                                    }
                                    else
                                    {
                                        lstTradeDone = lstTradeDone.OrderByDescending(ps => ps.TradeTime).ToList();
                                    }
                                    break;

                                case 1:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstTradeDone = lstTradeDone.OrderBy(ps => ps.TradeId).ToList();
                                    }
                                    else
                                    {
                                        lstTradeDone = lstTradeDone.OrderByDescending(ps => ps.TradeId).ToList();
                                    }
                                    break;

                                case 2:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstTradeDone = lstTradeDone.OrderBy(ps => ps.StockCode).ToList();
                                    }
                                    else
                                    {
                                        lstTradeDone = lstTradeDone.OrderByDescending(ps => ps.StockCode).ToList();
                                    }
                                    break;

                                case 3:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstTradeDone = lstTradeDone.OrderBy(ps => ps.Action).ToList();
                                    }
                                    else
                                    {
                                        lstTradeDone = lstTradeDone.OrderByDescending(ps => ps.Action).ToList();
                                    }
                                    break;

                                case 4:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstTradeDone = lstTradeDone.OrderBy(ps => ps.Price).ToList();
                                    }
                                    else
                                    {
                                        lstTradeDone = lstTradeDone.OrderByDescending(ps => ps.Price).ToList();
                                    }
                                    break;

                                case 5:
                                    if (columnSort == ColumnSort.ASC)
                                    {
                                        lstTradeDone = lstTradeDone.OrderBy(ps => ps.Volume).ToList();
                                    }
                                    else
                                    {
                                        lstTradeDone = lstTradeDone.OrderByDescending(ps => ps.Volume).ToList();
                                    }
                                    break;
                            }
                            BindingContext = lstTradeDone;
                            TradeDoneListView.ItemsSource = lstTradeDone;
                        }
                    })
                });

            return customGrid;
        }

        private void CreateDataGrid()
        {
            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            headerGrid.Children.Add(ToHeader("Time", 0, LayoutOptions.Center), headerGrid.Children.Count, 0); ;

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(80) });
            headerGrid.Children.Add(ToHeader("Trade ID", 1, LayoutOptions.Center), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(50) });
            headerGrid.Children.Add(ToHeader("Code", 2, LayoutOptions.Center), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(35) });
            headerGrid.Children.Add(ToHeader("Order", 3, LayoutOptions.Center), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            headerGrid.Children.Add(ToHeader("Price", 4, LayoutOptions.End), headerGrid.Children.Count, 0);

            headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            headerGrid.Children.Add(ToHeader("Volume", 5, LayoutOptions.End), headerGrid.Children.Count, 0);
        }

        private void ResetColumnHeader()
        {
            lstGridSortSign.ForEach(grd => grd.SetNetral());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.AthenaSession.Subscribe(athenaSession =>
            {
                if (athenaSession != null &&
                    athenaSession.BrokerAccountId != null)
                {
                    RefreshPageAsync(false);
                }
                else
                {
                    ResetColumnHeader();
                    ResetLblTotal();
                    TradeDoneListView.ItemsSource = null;
                }
            }));
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
            if (!_cancellationTokenSource.IsCancellationRequested)
            {
                _cancellationTokenSource.Cancel();
            }
            base.OnDisappearing();
        }

        private void CalculateTotalSummaryAndUpdateLabel(List<TradeDone> lstTradeDone, decimal feeBuy, decimal feeSell)
        {
            decimal buy = 0m;
            decimal sell = 0m;
            decimal net = 0m;

            foreach (TradeDone tradeDone in lstTradeDone)
            {
                if (tradeDone.Volume > 0m)
                {
                    if (tradeDone.OrderAction == Models.OrderAction.Buy)
                    {
                        if (Store.StockById.GetState().TryGetValue(new StockId(tradeDone.StockCode, MarketType.RG), out Stock stock))
                        {
                            buy += (tradeDone.Volume * tradeDone.Price * stock.SharesPerLot.Value);
                        }
                    }
                    else
                    {
                        if (Store.StockById.GetState().TryGetValue(new StockId(tradeDone.StockCode, MarketType.RG), out Stock stock))
                        {
                            sell += (tradeDone.Volume * tradeDone.Price * stock.SharesPerLot.Value);
                        }
                    }
                }
            }

            buy = Math.Floor(buy * (1 + Math.Round(feeBuy, 3)));
            sell = Math.Floor(sell * (1 - Math.Round(feeSell, 3)));

            net = sell - buy;

            ResetLblTotal(buy, sell, net);
        }

        private async Task<int> LoadOrdersAsync(string brokerAccountId, CancellationToken cancellationToken)
        {
            try
            {
                TradingLimitResponse tradingLimitResponse = await AthenaClient.Current.GetTradingLimitAsync(brokerAccountId, cancellationToken);

                TradeDone[] tradeDoneResponse = await AthenaClient.Current.GetTradeDoneAsync(brokerAccountId, cancellationToken);
                if (tradeDoneResponse != null)
                {
                    List<TradeDone> lstTradeDone = tradeDoneResponse.ToList();
                    BindingContext = lstTradeDone;
                    ResetColumnHeader();
                    TradeDoneListView.ItemsSource = tradeDoneResponse;
                    CalculateTotalSummaryAndUpdateLabel(lstTradeDone, tradingLimitResponse.FeeBuy, tradingLimitResponse.FeeSell);

                    return 1;
                }
                else
                {
                    ResetLblTotal();
                    ResetColumnHeader();
                    TradeDoneListView.ItemsSource = null;
                    return 2;
                }
            }
            catch (OperationCanceledException)
            {
                return 3;
            }
            catch
            {
                ResetLblTotal();
                ResetColumnHeader();
                TradeDoneListView.ItemsSource = null;
                return 4;
            }
            finally
            {
                TradeDoneListView.IsRefreshing = false;
            }
        }

        private void ResetLblTotal(
           decimal buy = 0m,
           decimal sell = 0m,
           decimal net = 0m)
        {
            lblBuy.Text = buy.ToString("N0");
            lblSell.Text = sell.ToString("N0");
            lblNet.Text = net.ToString("N0");

            if (net > 0m)
            {
                lblNet.TextColor = Colors.GainForeground;
            }
            else if (net < 0m)
            {
                lblNet.TextColor = Colors.LoseForeground;
            }
            else
            {
                lblNet.TextColor = Colors.UnchangedForeground;
            }
        }


        public async void RefreshPageAsync(bool showMessage = true)
        {
            if (Store.AthenaSession.GetState() != null &&
                    Store.AthenaSession.GetState().BrokerAccountId != null)
            {
                lastRefresh = DateTime.Now;
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
                int result = await LoadOrdersAsync(Store.AthenaSession.GetState().BrokerAccountId, _cancellationTokenSource.Token);
                if (result == 1)
                {
                    if (showMessage)
                    {
                        DependencyService.Get<IToastMessage>().Show("Trade List Loaded.");
                    }
                }
                else if (result == 2)
                {
                    DependencyService.Get<IToastMessage>().Show("Null Order.");
                }
                else if (result == 3)
                {
                    if (DateTime.Now.CompareTo(lastRefresh.AddSeconds(10)) >= 0)
                    {
                        DependencyService.Get<IToastMessage>().Show("Request timeout.");
                    }
                }
                else if (result == 4)
                {
                    DependencyService.Get<IToastMessage>().Show("Request failed.");
                }
            }
            else
            {
                ResetLblTotal();
                ResetColumnHeader();
                TradeDoneListView.ItemsSource = null;
            }
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}
