﻿using POST.DataSources.ApiResponses;
using POST.Dialogs;
using POST.Models;
using POST.Models.Collections;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.NewsPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SavedNewsCollectionPage : ContentPage
    {

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public ObservableSet<int, IUniqueIndexable<int>> NewsFeed { get; }

        public SavedNewsCollectionPage()
        {
            InitializeComponent();
            NewsFeed = new ObservableSet<int, IUniqueIndexable<int>>();
            NewsFeedListView.ItemsSource = NewsFeed;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.SavedNewsCollection
                .DistinctUntilChanged()
                .Subscribe(newsFeed =>
                {
                    if (newsFeed != null)
                    {
                        NewsFeed.SetItems(newsFeed.OfType<IUniqueIndexable<int>>().ToList());
                    }
                    else
                    {
                        NewsFeed.Clear();
                    }
                })
            );
        }

        protected override void OnDisappearing()
        {

            // Dispose all observers
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();

            base.OnDisappearing();
        }

        private async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is ExternalNewsSnippet externalNewsSnippet)
            {
                ExternalNewsPage page = (ExternalNewsPage)Activator.CreateInstance(typeof(ExternalNewsPage));
                page.MessageNo = externalNewsSnippet.MessageNo;
                page.Title = externalNewsSnippet.Title;
                await MainPage.NavigationPage.PushAsync(page);
            }
            else if (e.Item is ExternalNews externalNews)
            {
                ExternalNewsPage page = (ExternalNewsPage)Activator.CreateInstance(typeof(ExternalNewsPage));
                page.MessageNo = externalNews.MessageNo;
                page.Title = externalNews.Title;
                await MainPage.NavigationPage.PushAsync(page);
            }
            else if (e.Item is InternalNews internalNews)
            {
                InternalNewsPage page = (InternalNewsPage)Activator.CreateInstance(typeof(InternalNewsPage));
                page.Uri = $"http://chart.post-pro.co.id:8080{internalNews.FileName}";
                page.Title = internalNews.Title;
                await MainPage.NavigationPage.PushAsync(page);
            }
            if (sender is ListView listView)
            {
                listView.SelectedItem = null;
            }
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}