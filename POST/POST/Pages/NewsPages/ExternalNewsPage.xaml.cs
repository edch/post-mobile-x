﻿using POST.DataSources;
using POST.DataSources.ApiResponses;
using POST.Dialogs;
using Rg.Plugins.Popup.Services;
using System;
using System.Linq;
using System.Threading;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.NewsPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExternalNewsPage : ContentPage
    {

        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        public int? MessageNo { get; set; }

        public ExternalNewsPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (MessageNo.HasValue)
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                _cancellationTokenSource = cts;
                LoadNews(MessageNo.Value, cts.Token);
            }
        }

        protected override void OnDisappearing()
        {
            _cancellationTokenSource.Cancel();
            MessageNo = null;
            base.OnDisappearing();
        }



        private async void LoadNews(int messageNo, CancellationToken cancellationToken)
        {
            try
            {
                ExternalNews externalNews;
                if (Store.ExternalNewsFeed.GetState().FirstOrDefault(snippet => snippet.MessageNo == messageNo) is ExternalNews news)
                {
                    externalNews = news;
                }
                else if (Store.SavedNewsCollection.GetState().FirstOrDefault(savedNews => savedNews is ExternalNews) is ExternalNews savedExternalNews)
                {
                    externalNews = savedExternalNews;
                }
                else if (Store.SavedNewsCollection.GetState().FirstOrDefault(savedNews => savedNews is ExternalNewsSnippet) is ExternalNewsSnippet savedExternalNewsSnippet)
                {
                    externalNews = await NewsClient.Default.GetExternalNewsAsync(messageNo, cancellationToken);
                    Store.SavedNewsCollection.SetContent(externalNews);
                }
                else
                {
                    externalNews = await NewsClient.Default.GetExternalNewsAsync(messageNo, cancellationToken);
                    Store.ExternalNewsFeed.SetContent(externalNews);
                }
                WebView.Source = ConvertToWebViewSource(externalNews);
            }
            catch { }
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private static HtmlWebViewSource ConvertToWebViewSource(ExternalNews externalNews)
        {
            return new HtmlWebViewSource
            {
                Html = $@"
<html>
	<head>
		<title>{externalNews.Title}</title>
		<style>{STYLESHEET}</style>
	</head>
	<body>
		<div class=""container"">
			<h1>{externalNews.Title}</h1>
			<div class=""article"">
				{externalNews.Content}
			</div>
		</div>
	</body>
</html>"
            };
        }

        private const string STYLESHEET = @"
html {
	padding: 0;
	margin: 0;
}
.container {
	padding: 6px;
}
h1 {
	font-family: 'Merriweather', Georgia, serif;
	font-size: 25px;
}
.article {
	font-family: 'Lato', sans-serif;
	font-size: 17px;
}
";
    }
}