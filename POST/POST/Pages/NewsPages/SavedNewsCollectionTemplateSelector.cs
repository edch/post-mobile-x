﻿using POST.DataSources.ApiResponses;
using System;
using Xamarin.Forms;

namespace POST.Pages.NewsPages
{
    public class SavedNewsCollectionTemplateSelector : DataTemplateSelector
    {

        public DataTemplate InternalNewsTemplate { get; set; }
        public DataTemplate ExternalNewsTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            switch (item)
            {
                case InternalNews internalNews:
                    return InternalNewsTemplate;
                case ExternalNews externalNews:
                    return ExternalNewsTemplate;
                case ExternalNewsSnippet externalNewsSnippet:
                    return ExternalNewsTemplate;
                default:
                    throw new InvalidOperationException($"{typeof(SavedNewsCollectionTemplateSelector)} does not support item of type {item.GetType()}.");
            }
        }
    }
}
