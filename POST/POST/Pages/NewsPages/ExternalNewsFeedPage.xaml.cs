﻿using POST.DataSources;
using POST.DataSources.ApiResponses;
using POST.Dialogs;
using POST.Models.Collections;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.NewsPages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExternalNewsFeedPage : ContentPage
    {

        private CancellationTokenSource cancellationTokenSource;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public ObservableSet<int, ExternalNewsSnippet> NewsFeed { get; }

        public ExternalNewsFeedPage()
        {
            InitializeComponent();
            NewsFeed = new ObservableSet<int, ExternalNewsSnippet>();
            NewsFeedListView.ItemsSource = NewsFeed;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.ExternalNewsFeed
                .DistinctUntilChanged()
                .Subscribe(newsFeed =>
                {
                    if (newsFeed != null)
                    {
                        NewsFeed.SetItems(newsFeed.ToList());
                    }
                    else
                    {
                        NewsFeed.Clear();
                    }
                })
            );

            if (cancellationTokenSource != null)
            {
                cancellationTokenSource.Cancel();
            }
            cancellationTokenSource = new CancellationTokenSource();
            await LoadNewsFeedAsync(cancellationTokenSource.Token);
        }

        protected override void OnDisappearing()
        {

            if (cancellationTokenSource != null)
            {
                cancellationTokenSource.Cancel();
                cancellationTokenSource = null;
            }

            // Dispose all observers
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();

            base.OnDisappearing();
        }

        private async Task LoadNewsFeedAsync(CancellationToken cancellationToken)
        {
            try
            {
                ICollection<ExternalNewsSnippet> newsFeed = await NewsClient.Default.GetExternalNewsSnippetsAsync(cancellationToken);
                Store.ExternalNewsFeed.UpdateNewsFeed(newsFeed);
            }
            catch { }
        }

        private async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is ExternalNewsSnippet selectedNews)
            {
                ExternalNewsPage page = (ExternalNewsPage)Activator.CreateInstance(typeof(ExternalNewsPage));
                page.MessageNo = selectedNews.MessageNo;
                page.Title = selectedNews.Title;
                await MainPage.NavigationPage.PushAsync(page);
            }
            if (sender is ListView listView)
            {
                listView.SelectedItem = null;
            }
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}