﻿using POST.Dialogs;
using Rg.Plugins.Popup.Services;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.NewsPages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InternalNewsPage : ContentPage
    {

        private static readonly HttpClient _httpClient = new HttpClient();
        public string Uri { get; set; }
        private byte[] buffer = null;

        public InternalNewsPage()
        {
            InitializeComponent();
            PdfViewer.Toolbar.Enabled = false;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            IsVisible = true;
            try
            {
                buffer = await _httpClient.GetByteArrayAsync(Uri);
            }
            catch { buffer = null; }
            LoadPdf(Uri);
        }

        protected override void OnDisappearing()
        {
            IsVisible = false;
            Uri = null;
            base.OnDisappearing();
        }

        private async void LoadPdf(string uri)
        {
            try
            {
                if (buffer == null)
                {
                    return;
                }

                await Task.Delay(1000);
                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    if (Uri == uri && IsVisible)
                    {
                        PdfViewer.LoadDocument(memoryStream);
                    }
                }
            }
            catch { }
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}