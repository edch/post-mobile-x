﻿using POST.Dialogs;
using POST.Models;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.StockPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TechnicalChartPage : ContentPage
    {

        public StockId StockId { get; set; }


        public TechnicalChartPage()
        {
            InitializeComponent();

            StockId = new StockId("COMPOSITE", MarketType.RG);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            WebView.Source = new UrlWebViewSource
            {
                Url = "https://chart.post-pro.co.id/chartIQ/stx-phone-https.html?1"
            };
        }

        private async void WebView_Navigated(object sender, WebNavigatedEventArgs e)
        {
            WebView.Eval($"(function(){{document.getElementById(\"symbol\").value=\"{StockId.Code}\";stxx.newChart({{symbol:\"{StockId.Code}\",type:0}},null,null,finishedLoadingNewChart(\"{StockId.Code}\",\"{StockId.Code}\"));}})();");
            await Task.Delay(1000);
            WebView.Eval($"document.getElementById(\"symbol\").value=\"{StockId.Code}\";stxx.changeOccurred(\"layout\");");

            //if (StockId != null)
            //{
            //    WebView.Eval($"(function(){{document.getElementById(\"symbol\").value=\"{StockId.Code}\";stxx.newChart({{symbol:\"{StockId.Code}\",type:0}},null,null,finishedLoadingNewChart(\"{StockId.Code}\",\"{StockId.Code}\"));}})();");
            //    await Task.Delay(1000);
            //    WebView.Eval($"document.getElementById(\"symbol\").value=\"{StockId.Code}\";stxx.changeOccurred(\"layout\");");
            //}
            //else
            //{
            //    WebView.Eval($"(function(){{document.getElementById(\"symbol\").value=\"COMPOSITE\";stxx.newChart({{symbol:\"COMPOSITE\",type:0}},null,null,finishedLoadingNewChart(\"COMPOSITE\",\"COMPOSITE\"));}})();");
            //    await Task.Delay(1000);
            //    WebView.Eval($"document.getElementById(\"symbol\").value=\"COMPOSITE\";stxx.changeOccurred(\"layout\");");
            //}

        }


        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (StockId == null)
            {
                return;
            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                stockById.TryGetValue(StockId, out Stock stock) &&
                (stock.BestBidPrice ?? stock.LastOrPrevious).HasValue &&
                stock.SharesPerLot.HasValue)
            {
                OrderDialog orderDialog = new OrderDialog(
                    action: OrderAction.Buy,
                    stockId: StockId,
                    openPrice: stock.OpenOrPrevious.Value,
                    price: stock.BestBidPrice ?? stock.LastOrPrevious.Value,
                    priceUnsure: stock.BestBidPrice == null,
                    sharesPerLot: stock.SharesPerLot.Value
                );
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await Navigation.PushPopupAsync(orderDialog);
            }
            else
            {
                IsEnabled = false;
                StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await MainPage.NavigationPage.PushAsync(stockSearchPage);
                IsEnabled = true;
            }
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (StockId == null)
            {
                return;
            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                stockById.TryGetValue(StockId, out Stock stock) &&
                (stock.BestOfferPrice ?? stock.LastOrPrevious).HasValue &&
                stock.SharesPerLot.HasValue)
            {
                OrderDialog orderDialog = new OrderDialog(
                    action: OrderAction.Sell,
                    stockId: StockId,
                    openPrice: stock.OpenOrPrevious.Value,
                    price: stock.BestOfferPrice ?? stock.LastOrPrevious.Value,
                    priceUnsure: stock.BestOfferPrice == null,
                    sharesPerLot: stock.SharesPerLot.Value
                );
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await Navigation.PushPopupAsync(orderDialog);
            }
            else
            {
                IsEnabled = false;
                StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await MainPage.NavigationPage.PushAsync(stockSearchPage);
                IsEnabled = true;
            }
        }
    }
}