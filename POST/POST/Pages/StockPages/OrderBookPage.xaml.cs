﻿using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Models;
using POST.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.StockPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderBookPage : ContentPage
    {

        public static readonly BindableProperty StockIdProperty = BindableProperty.Create("StockId", typeof(StockId?), typeof(OrderBookPage), defaultValue: null);
        public StockId? StockId
        {
            get => (StockId?)GetValue(StockIdProperty);
            set => SetValue(StockIdProperty, value);
        }

        private class ClientState
        {
            public ThrottledDispatcher<OrderBook, OrderBookMessage> ThrottledDispatcher { get; set; }
        }

        private readonly DataFeedClientStateManager<ClientState> _clientStateManager;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public OrderBookPage()
        {
            InitializeComponent();

            OrderBookListView.IsRefreshing = true;

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState
                {
                    ThrottledDispatcher = new ThrottledDispatcher<OrderBook, OrderBookMessage>(
                        store: Store.OrderBook,
                        actionDispatcher: messages =>
                        {
                            if (!Store.OrderBook.GetState().Any())
                            {
                                Device.BeginInvokeOnMainThread(async () =>
                                {
                                    await Task.Delay(500);
                                    OrderBook orderBook = Store.OrderBook.GetState();
                                    OrderBookListView.IsRefreshing = false;
                                    if (orderBook.BestBid != null)
                                    {
                                        OrderBookListView.ScrollTo(orderBook.Single(level => level.Price == orderBook.BestBid), ScrollToPosition.Center, true);
                                    }
                                    else if (orderBook.BestAsk != null)
                                    {
                                        OrderBookListView.ScrollTo(orderBook.Single(level => level.Price == orderBook.BestAsk), ScrollToPosition.Center, true);
                                    }
                                });
                            }
                            return Store.OrderBook.BatchUpdate(messages);
                        },
                        coalesceInterval: TimeSpan.FromSeconds(1),
                        flushInterval: TimeSpan.FromSeconds(2)
                    )
                },
                clientFactory: state =>
                {
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        if (StockId.HasValue)
                        {
                            await sender.SendAsync(new SubscribeOrderBookRequest(StockId.Value));
                        }
                    };
                    client.DataReceived += (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case OrderBookMessage orderBookMessage:
                                state.ThrottledDispatcher.Dispatch(orderBookMessage);
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
            );

            _observers.Add(Store.OrderBook
                .Subscribe(orderBook => BindingContext = orderBook)
            );

            _observers.Add(Store.StockById
                .Select(stockById =>
                {
                    if (StockId.HasValue && stockById.TryGetValue(StockId.Value, out Stock stock))
                    {
                        return stock.Previous;
                    }
                    else
                    {
                        return null;
                    }
                })
                .DistinctUntilChanged()
                .Subscribe(previous =>
                {
                    if (previous.HasValue)
                    {
                        Store.OrderBook.SetPrevious(previous.Value);
                    }
                })
            );
        }

        ~OrderBookPage()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (StockId.HasValue && Store.OrderBook.GetState().StockId != StockId)
            {
                Store.OrderBook.Clear(StockId.Value);
                if (Store.StockById.GetState().TryGetValue(StockId.Value, out Stock stock) && stock.Previous.HasValue)
                {
                    Store.OrderBook.SetPrevious(stock.Previous.Value);
                }
            }
            _clientStateManager.StartClient();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            _clientStateManager.StopClient();
        }

        private async void OrderBookLevelCell_BuyClicked(object sender, ViewCells.OrderButtonEventArgs e)
        {
            //if (Store.MercurySession.GetState()?.BrokerAccountId == null) {
            //	await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            //}
            //if (Store.MercurySession.GetState()?.BrokerAccountId == null) return;
            //if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
            //	StockId.HasValue &&
            //	stockById.TryGetValue(StockId.Value, out Stock stock) &&
            //	BindingContext is OrderBookLevel orderBookLevel) {
            //	OrderDialog orderDialog = new OrderDialog(
            //		action: OrderAction.Buy,
            //		stockId: StockId.Value,
            //		price: e.Price,
            //		priceUnsure: false,
            //		sharesPerLot: stock.SharesPerLot.Value
            //	);
            //	await Navigation.PushPopupAsync(orderDialog);
            //} else {
            //	DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
            //}
        }

        private async void OrderBookLevelCell_SellClicked(object sender, ViewCells.OrderButtonEventArgs e)
        {
            //if (Store.MercurySession.GetState()?.BrokerAccountId == null) {
            //	await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            //}
            //if (Store.MercurySession.GetState()?.BrokerAccountId == null) return;
            //if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
            //	StockId.HasValue &&
            //	stockById.TryGetValue(StockId.Value, out Stock stock) &&
            //	BindingContext is OrderBookLevel orderBookLevel) {
            //	OrderDialog orderDialog = new OrderDialog(
            //		action: OrderAction.Sell,
            //		stockId: StockId.Value,
            //		price: e.Price,
            //		priceUnsure: false,
            //		sharesPerLot: stock.SharesPerLot.Value
            //	);
            //	await Navigation.PushPopupAsync(orderDialog);
            //} else {
            //	DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
            //}
        }
    }
}