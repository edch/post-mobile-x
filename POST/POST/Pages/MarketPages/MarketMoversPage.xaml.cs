﻿using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Models;
using POST.Models.Collections;
using POST.Stores;
using POST.ViewCells;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.MarketPages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MarketMoversPage : ContentPage
    {

        private const string TOP_GAINERS = "TOP GAINERS";
        private const string TOP_LOSERS = "TOP LOSERS";
        private const string TOP_VOLUME = "MOST TRADED BY VOLUME";
        private const string TOP_FREQUENCY = "MOST TRADED BY FREQUENCY";
        private const string TOP_VALUE = "MOST TRADED BY VALUE";

        private class ClientState
        {
            public List<StockInitMessage> StockInitMessages { get; } = new List<StockInitMessage>();
            public ThrottledDispatcher<IReadOnlyDictionary<StockId, Stock>, StockQuoteMessage> ThrottledDispatcher { get; set; }
        }

        private readonly DataFeedClientStateManager<ClientState> _clientStateManager;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        private readonly ObservableGroup<StockId, Stock> TopGainers = new ObservableGroup<StockId, Stock>(TOP_GAINERS);
        private readonly ObservableGroup<StockId, Stock> TopLosers = new ObservableGroup<StockId, Stock>(TOP_LOSERS);
        private readonly ObservableGroup<StockId, Stock> TopVolume = new ObservableGroup<StockId, Stock>(TOP_VOLUME);
        private readonly ObservableGroup<StockId, Stock> TopFrequency = new ObservableGroup<StockId, Stock>(TOP_FREQUENCY);
        private readonly ObservableGroup<StockId, Stock> TopValue = new ObservableGroup<StockId, Stock>(TOP_VALUE);

        public ObservableCollection<ObservableGroup<StockId, Stock>> StockCodeGroups { get; set; }

        public MarketMoversPage()
        {
            InitializeComponent();
            StockCodeGroups = new ObservableCollection<ObservableGroup<StockId, Stock>>();
            MarketMoversListView.ItemsSource = StockCodeGroups;

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState(),
                clientFactory: state =>
                {
                    state.StockInitMessages.Clear();
                    state.ThrottledDispatcher = new ThrottledDispatcher<IReadOnlyDictionary<StockId, Stock>, StockQuoteMessage>(
                        store: Store.StockById,
                        actionDispatcher: messages => Store.StockById.UpdateQuotes(messages.ToList()),
                        coalesceInterval: TimeSpan.FromSeconds(0.5),
                        flushInterval: TimeSpan.FromSeconds(1)
                    );
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        await sender.SendAsync(new StockInitRequest());
                    };
                    client.DataReceived += async (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case StockInitMessage stockInitMessage:
                                if (Store.StockById.GetState().Any())
                                {
                                    await sender.SendAsync(new SubscribeAllStockQuotesRequest());
                                }
                                else
                                {
                                    state.StockInitMessages.Add(stockInitMessage);
                                }
                                break;
                            case StockInitEndMessage stockInitEndMessage:
                                if (!Store.StockById.GetState().Any())
                                {
                                    Store.StockById.Init(state.StockInitMessages);
                                }
                                state.StockInitMessages.Clear();
                                MarketMoversListView.IsRefreshing = false;

                                // HACK: We need to resend SubscribeAllStockQuotesRequest to refresh all quotes.
                                // This should be fixed on server side.
                                await Task.Delay(2_000);
                                if (_clientStateManager.IsConnected)
                                {
                                    await sender.SendAsync(new SubscribeAllStockQuotesRequest());
                                }
                                break;
                            case StockQuoteMessage stockQuoteMessage:
                                state.ThrottledDispatcher.Dispatch(stockQuoteMessage);
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
            );

            // Observe top gainers
            _observers.Add(Store.StockById
                .Select(stockById => (from stock in stockById.Values
                                      where stock.IsInitialized
                                      where stock.Id.MarketType == MarketType.RG
                                      where stock.ChangePercent.HasValue
                                      where stock.ChangePercent.Value > 0
                                      orderby stock.ChangePercent.Value descending
                                      select stock).Take(5).ToList())
                .Subscribe(stocks =>
                {
                    TopGainers.SetItems(stocks);
                    RenderGroups();
                })
            );

            // Observe top losers
            _observers.Add(Store.StockById
                .Select(stockById => (from stock in stockById.Values
                                      where stock.IsInitialized
                                      where stock.Id.MarketType == MarketType.RG
                                      where stock.ChangePercent.HasValue
                                      where stock.ChangePercent.Value < 0
                                      orderby stock.ChangePercent.Value ascending
                                      select stock).Take(5).ToList())
                .Subscribe(stocks =>
                {
                    TopLosers.SetItems(stocks);
                    RenderGroups();
                })
            );

            // Observe most traded by volume
            _observers.Add(Store.StockById
                .Select(stockById => (from stock in stockById.Values
                                      where stock.IsInitialized
                                      where stock.Id.MarketType == MarketType.RG
                                      where stock.Magnitudes != null
                                      where stock.Magnitudes.Volume > 0
                                      orderby stock.Magnitudes.Volume descending
                                      select stock).Take(5).ToList())
                .Subscribe(stocks =>
                {
                    TopVolume.SetItems(stocks);
                    RenderGroups();
                })
            );

            // Observe most traded by frequency
            _observers.Add(Store.StockById
                .Select(stockById => (from stock in stockById.Values
                                      where stock.IsInitialized
                                      where stock.Id.MarketType == MarketType.RG
                                      where stock.Magnitudes != null
                                      where stock.Magnitudes.Frequency > 0
                                      orderby stock.Magnitudes.Frequency descending
                                      select stock).Take(5).ToList())
                .Subscribe(stocks =>
                {
                    TopFrequency.SetItems(stocks);
                    RenderGroups();
                })
            );

            // Observe most traded by value
            _observers.Add(Store.StockById
                .Select(stockById => (from stock in stockById.Values
                                      where stock.IsInitialized
                                      where stock.Id.MarketType == MarketType.RG
                                      where stock.Magnitudes != null
                                      where stock.Magnitudes.Value > 0
                                      orderby stock.Magnitudes.Value descending
                                      select stock).Take(5).ToList())
                .Subscribe(stocks =>
                {
                    TopValue.SetItems(stocks);
                    RenderGroups();
                })
            );

            _observers.Add(Store.MarketMoversViewMode
                .Subscribe(viewMode =>
                {
                    switch (viewMode)
                    {
                        case ViewMode.Simple:
                            MarketMoversListView.ItemTemplate = new DataTemplate(typeof(CompactStockCell));
                            break;
                        case ViewMode.Advanced:
                            MarketMoversListView.ItemTemplate = new DataTemplate(typeof(StockCell));
                            break;
                    }
                })
            );
        }

        ~MarketMoversPage()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _clientStateManager.StartClient();
        }

        private void RenderGroups()
        {
            int i = 0;
            foreach (ObservableGroup<StockId, Stock> group in new[] {
                TopGainers,
                TopLosers,
                TopVolume,
                TopFrequency,
                TopValue
            })
            {
                if (group.Any())
                {
                    if (!StockCodeGroups.Contains(group))
                    {
                        StockCodeGroups.Insert(i, group);
                    }
                    i++;
                }
                else
                {
                    StockCodeGroups.Remove(group);
                }
            }
        }

        protected override void OnDisappearing()
        {

            // Stop client
            _clientStateManager.StopClient();

            base.OnDisappearing();
        }

        private async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is string stockCode)
            {
                StockPage page = (StockPage)Activator.CreateInstance(typeof(StockPage));
                page.StockId = new StockId(stockCode, MarketType.RG);
                page.Title = stockCode;
                await MainPage.NavigationPage.PushAsync(page);
            }
            else if (e.Item is Stock stock)
            {
                StockPage page = (StockPage)Activator.CreateInstance(typeof(StockPage));
                page.StockId = stock.Id;
                page.Title = stock.Id.Code;
                await MainPage.NavigationPage.PushAsync(page);
            }
            if (sender is ListView listView)
            {
                listView.SelectedItem = null;
            }
        }

        private void SimpleViewButton_Clicked(object sender, EventArgs e)
        {
            Store.MarketMoversViewMode.SetViewMode(ViewMode.Simple);
        }

        private void AdvancedViewButton_Clicked(object sender, EventArgs e)
        {
            Store.MarketMoversViewMode.SetViewMode(ViewMode.Advanced);
        }
    }
}
