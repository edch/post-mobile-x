﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Models;
using POST.Models.Collections;
using POST.Stores;
using POST.ViewCells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.MarketPages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WatchListPage : ContentPage
    {

        private class ClientState
        {
            public List<StockInitMessage> StockInitMessages { get; } = new List<StockInitMessage>();
            public ThrottledDispatcher<IReadOnlyDictionary<StockId, Stock>, StockQuoteMessage> ThrottledDispatcher { get; set; }
            public HashSet<string> Subscribed { get; } = new HashSet<string>();
        }

        private readonly DataFeedClientStateManager<ClientState> _clientStateManager;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public ObservableSet<StockId, Stock> Stocks { get; }

        private static readonly IReadOnlyDictionary<string, SortKey> SortKeys = new Dictionary<string, SortKey> {
            { "Stock Code A-Z", SortKey.CodeAsc },
            { "Stock Code Z-A", SortKey.CodeDesc },
            { "Top Gainer (%)", SortKey.ChangePercentDesc },
            { "Top Loser (%)", SortKey.ChangePercentAsc }
        };

        public WatchListPage()
        {
            InitializeComponent();
            Stocks = new ObservableSet<StockId, Stock>();
            StocksListView.ItemsSource = Stocks;

            foreach (string sortKey in SortKeys.Keys)
            {
                SortKeyPicker.Items.Add(sortKey);
            }

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState(),
                clientFactory: state =>
                {
                    state.StockInitMessages.Clear();
                    state.Subscribed.Clear();
                    state.ThrottledDispatcher = new ThrottledDispatcher<IReadOnlyDictionary<StockId, Stock>, StockQuoteMessage>(
                        store: Store.StockById,
                        actionDispatcher: messages => Store.StockById.UpdateQuotes(messages.ToList()),
                        coalesceInterval: TimeSpan.FromMilliseconds(500),
                        flushInterval: TimeSpan.FromSeconds(1)
                    );
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        await sender.SendAsync(new DesktopModeRequest());
                        await sender.SendAsync(new StockInitRequest());
                    };
                    client.DataReceived += async (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case StockInitMessage stockInitMessage:
                                if (!Store.StockById.GetState().ContainsKey(new StockId(stockInitMessage.StockCode, stockInitMessage.MarketType)))
                                {
                                    state.StockInitMessages.Add(stockInitMessage);
                                }
                                break;
                            case StockInitEndMessage stockInitEndMessage:
                                if (!Store.StockById.GetState().Any())
                                {
                                    Store.StockById.Init(state.StockInitMessages);
                                }
                                state.StockInitMessages.Clear();
                                StocksListView.IsRefreshing = false;
                                //foreach (StockId stockId in Store.WatchedStockIds.GetState()) {
                                //	if (!state.Subscribed.Contains(stockId.Code)) {
                                //		await sender.SendAsync(new SubscribeStockQuoteRequest(stockId));
                                //		state.Subscribed.Add(stockId.Code);
                                //	}
                                //}
                                await sender.SendAsync(new SubscribeAllStockQuotesRequest());
                                break;
                            case StockQuoteMessage stockQuoteMessage:
                                state.ThrottledDispatcher.Dispatch(stockQuoteMessage);
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
            );

            // Observe WatchedStockIds, StockById, WatchListSortKey together
            _observers.Add(
                Observable.Merge(
                    Store.WatchedStockIds.Select(stockIds => new
                    {
                        StockIds = stockIds,
                        StockById = Store.StockById.GetState(),
                        SortKey = Store.WatchListSortKey.GetState()
                    }),
                    Store.StockById.Select(stockById => new
                    {
                        StockIds = Store.WatchedStockIds.GetState(),
                        StockById = stockById,
                        SortKey = Store.WatchListSortKey.GetState()
                    }),
                    Store.WatchListSortKey.Select(sortKey => new
                    {
                        StockIds = Store.WatchedStockIds.GetState(),
                        StockById = Store.StockById.GetState(),
                        SortKey = sortKey
                    })
                )
                .Select(merged =>
                {
                    // Sort stock codes by selected sort key
                    switch (merged.SortKey)
                    {
                        default:
                        case SortKey.CodeAsc:
                            return (from stockId in merged.StockIds
                                    orderby stockId.Code ascending
                                    select stockId.Code).ToList();
                        case SortKey.CodeDesc:
                            return (from stockId in merged.StockIds
                                    orderby stockId.Code descending
                                    select stockId.Code).ToList();
                        case SortKey.ChangePercentAsc:
                            return (from stockId in merged.StockIds
                                    where merged.StockById.ContainsKey(stockId)
                                    let changePercent = merged.StockById[stockId].ChangePercent
                                    where changePercent.HasValue
                                    orderby changePercent.Value ascending
                                    select stockId.Code).Union(
                                        from stockId in merged.StockIds
                                        where merged.StockById.ContainsKey(stockId)
                                        where !merged.StockById[stockId].ChangePercent.HasValue
                                        orderby stockId.Code ascending
                                        select stockId.Code).Union(
                                            from stockId in merged.StockIds
                                            where !merged.StockById.ContainsKey(stockId)
                                            orderby stockId.Code ascending
                                            select stockId.Code).ToList();
                        case SortKey.ChangePercentDesc:
                            return (from stockId in merged.StockIds
                                    where merged.StockById.ContainsKey(stockId)
                                    let changePercent = merged.StockById[stockId].ChangePercent
                                    where changePercent.HasValue
                                    orderby changePercent.Value descending
                                    select stockId.Code).Union(
                                        from stockId in merged.StockIds
                                        where merged.StockById.ContainsKey(stockId)
                                        where !merged.StockById[stockId].ChangePercent.HasValue
                                        orderby stockId.Code descending
                                        select stockId.Code).Union(
                                            from stockId in merged.StockIds
                                            where !merged.StockById.ContainsKey(stockId)
                                            orderby stockId.Code descending
                                            select stockId.Code).ToList();
                    }
                })
                .Select(stockCodes =>
                {
                    // Transform stock codes into stocks
                    return stockCodes.Select(stockCode =>
                    {
                        if (Store.StockById.GetState().TryGetValue(new StockId(stockCode, MarketType.RG), out Stock stock))
                        {
                            return stock;
                        }
                        else
                        {
                            return new Stock(
                                code: stockCode,
                                marketType: MarketType.RG,
                                name: StockNameFallback.For(stockCode)
                            );
                        }
                    }).ToList();
                })
                .ObserveOn(SynchronizationContext.Current)
                .Subscribe(stocks =>
                {
                    // Update items source
                    Stocks.SetItems(stocks);
                })
            );

            // Observer for updating Sort icon in toolbar
            _observers.Add(Store.WatchListSortKey
                .ObserveOn(SynchronizationContext.Current)
                .Subscribe(sortKey =>
                {
                    SortButton.Text = Glyphs.ForSortKey(sortKey);
                })
            );

            _observers.Add(Store.WatchListViewMode
                .Subscribe(viewMode =>
                {
                    switch (viewMode)
                    {
                        case ViewMode.Simple:
                            StocksListView.ItemTemplate = new DataTemplate(typeof(CompactStockCell));
                            break;
                        case ViewMode.Advanced:
                            StocksListView.ItemTemplate = new DataTemplate(typeof(StockCell));
                            break;
                    }
                })
            );
        }

        ~WatchListPage()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _clientStateManager.StartClient();
        }

        protected override void OnDisappearing()
        {

            // Stop client
            _clientStateManager.StopClient();

            // Cancel refreshing
            StocksListView.IsRefreshing = false;

            base.OnDisappearing();
        }

        private async void Handle_PullToRefresh(object sender, EventArgs e)
        {
            await _clientStateManager.SendAsync(new StockInitRequest());
        }

        /*
		private async void Handle_ItemTapped(object sender, ItemTappedEventArgs e) {
			if (e.Item is Stock stock) {
				var page = new StockPage();
				page.StockId = stock.Id;
				page.Title = stock.Id.Code;
				await MainPage.NavigationPage.PushAsync(page);
			}
			if (sender is ListView listView) {
				listView.SelectedItem = null;
			}
		}
        */

        private void SortButton_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                SortKeyPicker.Focus();
            });
        }

        private void SortKeyPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SortKeyPicker.SelectedItem is string selectedItem &&
                SortKeys.TryGetValue(selectedItem, out SortKey sortKey))
            {
                Store.WatchListSortKey.SortBy(sortKey);
            }
        }

        private async void AddButton_Clicked(object sender, EventArgs e)
        {
            AddWatchlistPage addWatchlistPage = new AddWatchlistPage();
            await MainPage.NavigationPage.PushAsync(addWatchlistPage);
        }

        private void SimpleViewButton_Clicked(object sender, EventArgs e)
        {
            Store.WatchListViewMode.SetViewMode(ViewMode.Simple);
        }

        private void AdvancedViewButton_Clicked(object sender, EventArgs e)
        {
            Store.WatchListViewMode.SetViewMode(ViewMode.Advanced);
        }

    }
}
