﻿using POST.Models;
using POST.Models.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.MarketPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeleteWatchlistPage : ContentPage
    {
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();
        public ObservableSet<StockId, Stock> Stocks { get; }

        private readonly List<Stock> lstStockDeleted;

        public DeleteWatchlistPage()
        {
            InitializeComponent();
            lstStockDeleted = new List<Stock>();
            Stocks = new ObservableSet<StockId, Stock>();
            lstViewStocks.ItemsSource = Stocks;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Stocks.SetItems(Store.StockById.GetState().Values.ToList().Where(x => Store.WatchedStockIds.GetState().Contains(x.Id)).ToList());
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void StockDeletingCell_StockCellTapped(object sender, ViewCells.StockCellEventArgs e)
        {
            Stock stock = e.stock;

            lstStockDeleted.Add(stock);
            Stocks.Remove(stock);

            if (!string.IsNullOrWhiteSpace(txtSearch.Text))
            {
                txtSearch_TextChanged(this, new TextChangedEventArgs(txtSearch.Text, txtSearch.Text));
            }
        }

        private async void SaveToolbarButton_Clicked(object sender, EventArgs e)
        {
            foreach (Stock stock in lstStockDeleted)
            {
                Store.WatchedStockIds.RemoveStock(stock.Id);
            }

            await MainPage.NavigationPage.PopToRootAsync();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            lstViewStocks.IsRefreshing = true;

            if (string.IsNullOrWhiteSpace(e.NewTextValue))
            {
                lstViewStocks.ItemsSource = Stocks;
            }
            else
            {
                lstViewStocks.ItemsSource = Stocks.Where(i => (i.Name.ToLower().Contains(e.NewTextValue.ToLower()) || i.Id.Code.ToLower().Contains(e.NewTextValue.ToLower())));
            }

            lstViewStocks.IsRefreshing = false;
        }
    }
}