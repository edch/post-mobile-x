﻿using POST.Models;
using POST.Models.Collections;
using POST.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.MarketPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class AddWatchlistPage : ContentPage
    {
        private readonly StockSearchFilterStore StockSearchFilterStore = new StockSearchFilterStore();

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public ObservableSet<StockId, Stock> Stocks { get; }

        public AddWatchlistPage()
        {
            InitializeComponent();
            Stocks = new ObservableSet<StockId, Stock>();
            lstViewStocks.ItemsSource = Stocks;

            Title = "Stocks - All";

            StockSearchFilterPicker.ItemsSource = StockSearchFilterDict.Dict.Keys.ToList();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(
               Store.StockById
               .Select(stockCode => stockCode.Values.ToList().Where(x => x.Id.MarketType == MarketType.RG))
               .Subscribe(stocks =>
               {
                   // Update items source                   
                   Stocks.SetItems(stocks.ToList());
               })
            );
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }

            lstViewStocks.IsRefreshing = false;

            base.OnDisappearing();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            setUi();
        }

        private void setUi()
        {
            Title = "Stocks";

            IEnumerable<Stock> lstStockTmp = Stocks.ToList();

            if (StockSearchFilterStore.GetState() == StockSearchFilter.ALL)
            {
                Title += " - All";
            }
            else
            if (StockSearchFilterStore.GetState() == StockSearchFilter.WATCHLIST)
            {
                lstStockTmp = lstStockTmp.Where(i => (i.IsWatchlistBoard == true));

                Title += " - IDX Watchlist";
            }

            lstViewStocks.IsRefreshing = true;

            string strFilter = txtSearch.Text;

            if (string.IsNullOrWhiteSpace(strFilter))
            {
                lstViewStocks.ItemsSource = lstStockTmp;
            }
            else
            {
                strFilter = strFilter.ToLower();
                lstViewStocks.ItemsSource = lstStockTmp.Where(i => (i.Name.ToLower().Contains(strFilter) || i.Id.Code.ToLower().Contains(strFilter)));
            }

            lstViewStocks.IsRefreshing = false;
        }

        private void StockSearchFilterToolBar_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                StockSearchFilterPicker.Focus();
            });
        }

        public void StockSearchFilterPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StockSearchFilterPicker.SelectedItem is string selectedItem && selectedItem != null)
            {
                StockSearchFilterStore.Set(StockSearchFilterDict.Dict[selectedItem]);
            }
            setUi();
        }
    }
}
