﻿using POST.DataSources;
using POST.DataSources.ApiResponses;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Dialogs;
using POST.Models;
using POST.Services;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BrokerAccDistPage : ContentPage
    {
        private class ClientState { }

        private readonly DataFeedClientStateManager<ClientState> _clientStateManager;

        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
        private readonly ICollection<IDisposable> _observersStockQuote = new List<IDisposable>();
        private DateTime lastRefresh = DateTime.Now;
        private readonly List<string> lstMarketType;
        private readonly List<string> lstDomesticForeign;

        public static BindableProperty StockIdProperty = BindableProperty.Create("StockId", typeof(StockId?), typeof(BrokerAccDistPage), defaultValue: null, propertyChanged: OnStockIdChanged);

        public StockId? StockId
        {
            get => (StockId?)GetValue(StockIdProperty);
            set
            {
                StockId stockId = (StockId)value;
                SetValue(StockIdProperty, stockId);

                _clientStateManager.StopClient();

                foreach (IDisposable observer in _observersStockQuote)
                {
                    observer.Dispose();
                }
                _observersStockQuote.Clear();

                _clientStateManager.StartClient();

                _observersStockQuote.Add(Store.StockById
                    .Select(stockById => StockId.HasValue && stockById.TryGetValue(StockId.Value, out Stock stock) ? stock : null)
                    .Subscribe(stock => UpdateTitleView(stock)
                    )
                );
            }
        }

        private void UpdateTitleView(Stock stock)
        {
            if (stock != null)
            {
                StockCodeLabel.TextColor = stock.StockCodeForeground;
                StockPriceLabel.Text = $"{stock.LastOrPrevious:N0}";
                StockPriceLabel.TextColor = stock.ChangeForeground;
                StockChangeGlyphLabel.Text = stock.ChangeGlyph;
                StockChangeGlyphLabel.TextColor = stock.ChangeForeground;
                if (stock.ChangePercent.HasValue)
                {
                    StockChangePercentLabel.Text = $"{stock.ChangePercent.Value:N2}%";
                    StockChangePercentLabel.TextColor = stock.ChangeForeground;
                }
                else
                {
                    StockChangePercentLabel.Text = string.Empty;
                }
                if (stock.Change.HasValue)
                {
                    StockChangePointLabel.Text = stock.Change.Value.ToString("#,##0;0");
                    StockChangePointLabel.TextColor = stock.ChangeForeground;
                }
                else
                {
                    StockChangePointLabel.Text = string.Empty;
                }
            }
            else
            {
                StockPriceLabel.Text = string.Empty;
                StockChangeGlyphLabel.Text = string.Empty;
                StockChangePercentLabel.Text = string.Empty;
                StockChangePointLabel.Text = string.Empty;
            }
        }

        private static void OnStockIdChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is BrokerAccDistPage brokerAccDistPage)
            {
                if (newValue is StockId stockId)
                {
                    brokerAccDistPage.StockCodeLabel.Text = stockId.Code;

                    //Syariah
                    if (Store.StockById.GetState().TryGetValue(stockId, out Stock stock))
                    {
                        brokerAccDistPage.StockCodeLabel.TextDecorations = stock.TextDecoration;
                    }
                }
                else
                {
                    brokerAccDistPage.StockCodeLabel.Text = string.Empty;
                    brokerAccDistPage.StockCodeLabel.TextDecorations = TextDecorations.None;
                }
            }
        }

        public BrokerAccDistPage()
        {
            InitializeComponent();

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState(),
                clientFactory: state =>
                {
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        if (StockId.HasValue)
                        {
                            await sender.SendAsync(new SubscribeStockQuoteRequest(StockId.Value));
                        }
                    };
                    client.DataReceived += (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case StockQuoteMessage stockQuoteMessage:
                                //Debug.WriteLine("StockPage " + stockQuoteMessage.StockCode + " " + stockQuoteMessage.MarketType.ToString() + " " + stockQuoteMessage.Volume);
                                Store.StockById.UpdateQuotes(new List<StockQuoteMessage> { stockQuoteMessage });
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
            );

            lstMarketType = new List<string>() { "ALL", "RG", "NG", "TN" };
            lstDomesticForeign = new List<string>() { "ALL", "Domestic", "Foreign" };

            pickerMarketType.ItemsSource = lstMarketType;
            pickerMarketType.SelectedIndex = 0;
            pickerMarketType.SelectedIndexChanged += PickerMarketType_SelectedIndexChanged;

            pickerDomesticForeign.ItemsSource = lstDomesticForeign;
            pickerDomesticForeign.SelectedIndex = 0;
            pickerDomesticForeign.SelectedIndexChanged += PickerDomesticForeign_SelectedIndexChanged;

            dtpStart.Format = "MMM/dd/yyyy";
            dtpEnd.Format = "MMM/dd/yyyy";
            dtpStart.MinimumDate = DateTime.Now.AddYears(-5);
            dtpStart.MaximumDate = DateTime.Now;
            dtpEnd.MinimumDate = DateTime.Now.AddYears(-5);
            dtpEnd.MaximumDate = DateTime.Now;
            dtpStart.DateSelected += DatetimePicker_DateSelected;
            dtpEnd.DateSelected += DatetimePicker_DateSelected;
        }

        private void DatetimePicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            RefreshChartAsync();
        }

        private void PickerDomesticForeign_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshChartAsync();
        }

        private void PickerMarketType_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshChartAsync();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Store.CurrentDisplayStockStore.SetStock(Store.RecentStockIds.GetState().ToList().Last());

            StockId = Store.CurrentDisplayStockStore.GetState();

            RefreshChartAsync(false);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            if (!_cancellationTokenSource.IsCancellationRequested)
            {
                _cancellationTokenSource.Cancel();
            }

            foreach (IDisposable observer in _observersStockQuote)
            {
                observer.Dispose();
            }
            _observersStockQuote.Clear();
        }

        public async void RefreshChartAsync(bool showMessage = true)
        {
            if (dtpEnd.Date < dtpStart.Date)
            {
                DependencyService.Get<IToastMessage>().Show("Start Date can not be greater than End Date.");
                ClearTheChart();
                return;
            }

            lastRefresh = DateTime.Now;
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
            int result = await LoadBrokerAccDistAsync(_cancellationTokenSource.Token);
            if (result == 1)
            {
                if (showMessage)
                {
                    DependencyService.Get<IToastMessage>().Show("Chart Loaded.");
                }
            }
            else if (result == 2)
            {
                if (showMessage)
                {
                    DependencyService.Get<IToastMessage>().Show("Chart Loaded.");
                }
            }
            else if (result == 3)
            {
                if (DateTime.Now.CompareTo(lastRefresh.AddSeconds(10)) >= 0)
                {
                    DependencyService.Get<IToastMessage>().Show("Request timeout.");
                }
            }
            else if (result == 4)
            {
                DependencyService.Get<IToastMessage>().Show("Request failed.");
            }
        }

        private void ClearTheChart()
        {
            plotViewTopBuyers.Model = BrokerAccDistChartModel.CreateTopBuyers_Null();
            plotViewTopSellers.Model = BrokerAccDistChartModel.CreateTopSellers_Null();
            plotViewTopNetBuyers.Model = BrokerAccDistChartModel.CreateTopNetBuyers_Null();
            plotViewTopNetSellers.Model = BrokerAccDistChartModel.CreateTopNetSellers_Null();
        }

        private async Task<int> LoadBrokerAccDistAsync(CancellationToken cancellationToken)
        {
            try
            {
                string strMarketType = null;
                string strDomesticForeign = null;

                if (pickerMarketType.SelectedIndex != 0)
                {
                    strMarketType = pickerMarketType.SelectedItem.ToString();
                }

                if (pickerDomesticForeign.SelectedIndex != 0)
                {
                    if (pickerDomesticForeign.SelectedIndex == 1)
                    {
                        strDomesticForeign = "D";
                    }
                    else
                    {
                        strDomesticForeign = "F";
                    }
                }

                List<BrokerAccDist> brokerAccDistResponse = await BrokerAccDistClient.Default.GetBrokerAccDistAsync(StockId.Value.Code, dtpStart.Date.ToString("yyyy-MM-dd"), dtpEnd.Date.ToString("yyyy-MM-dd"), cancellationToken, strDomesticForeign, strMarketType);
                if (brokerAccDistResponse.Count > 0)
                {
                    plotViewTopBuyers.Model = BrokerAccDistChartModel.CreateTopBuyers(brokerAccDistResponse);
                    plotViewTopSellers.Model = BrokerAccDistChartModel.CreateTopSellers(brokerAccDistResponse);
                    plotViewTopNetBuyers.Model = BrokerAccDistChartModel.CreateTopNetBuyers(brokerAccDistResponse);
                    plotViewTopNetSellers.Model = BrokerAccDistChartModel.CreateTopNetSellers(brokerAccDistResponse);
                    return 1;
                }
                else
                {
                    ClearTheChart();
                    return 2;
                }
            }
            catch (OperationCanceledException)
            {
                return 3;
            }
            catch
            {
                ClearTheChart();
                return 4;
            }
            finally
            {

            }
        }

        private async void StockSearchButton_Clicked(object sender, EventArgs e)
        {
            StockSearchPage stockSearchPage = new StockSearchPage("BrokerAccDistPage");
            await MainPage.NavigationPage.PushAsync(stockSearchPage);
        }

        private void BtnRefresh_Clicked(object sender, EventArgs e)
        {
            RefreshChartAsync();
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (StockId == null)
            {
                return;
            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                stockById.TryGetValue(StockId.Value, out Stock stock) &&
                (stock.BestBidPrice ?? stock.LastOrPrevious).HasValue &&
                stock.SharesPerLot.HasValue)
            {
                OrderDialog orderDialog = new OrderDialog(
                    action: OrderAction.Buy,
                    stockId: StockId.Value,
                    openPrice: stock.OpenOrPrevious.Value,
                    price: stock.BestBidPrice ?? stock.LastOrPrevious.Value,
                    priceUnsure: stock.BestBidPrice == null,
                    sharesPerLot: stock.SharesPerLot.Value
                );
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await Navigation.PushPopupAsync(orderDialog);
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
            }
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (StockId == null)
            {
                return;
            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                stockById.TryGetValue(StockId.Value, out Stock stock) &&
                (stock.BestOfferPrice ?? stock.LastOrPrevious).HasValue &&
                stock.SharesPerLot.HasValue)
            {
                OrderDialog orderDialog = new OrderDialog(
                    action: OrderAction.Sell,
                    stockId: StockId.Value,
                    openPrice: stock.OpenOrPrevious.Value,
                    price: stock.BestOfferPrice ?? stock.LastOrPrevious.Value,
                    priceUnsure: stock.BestOfferPrice == null,
                    sharesPerLot: stock.SharesPerLot.Value
                );
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await Navigation.PushPopupAsync(orderDialog);
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
            }
        }
    }
}