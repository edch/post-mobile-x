﻿using POST.Dialogs;
using POST.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public AboutPage()
        {
            InitializeComponent();
        }

        private void SetLogo()
        {
            if (!Store.TradingAccounts.IsSyariahAcc)
            {
                imgLogo.Source = ImageSource.FromFile("logo.png");
            }
            else
            {
                imgLogo.Source = ImageSource.FromFile("icon_syariah.png");
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.TradingAccounts.Subscribe(tradingAccounts =>
            {
                SetLogo();
            }));

            AssemblyProductAttribute productAttribute = ((AssemblyProductAttribute)Attribute.GetCustomAttribute(
            Assembly.GetExecutingAssembly(), typeof(AssemblyProductAttribute), false));
            if (productAttribute != null)
            {
                lblTitle.Text = productAttribute.Product;
            }

            AssemblyCopyrightAttribute copyrightAttribute = ((AssemblyCopyrightAttribute)Attribute.GetCustomAttribute(
            Assembly.GetExecutingAssembly(), typeof(AssemblyCopyrightAttribute), false));
            if (copyrightAttribute != null)
            {
                lblCopyright.Text = copyrightAttribute.Copyright;
            }

            lblVersion.Text = $"Ver. {DependencyService.Get<IDeviceProperties>().VersionName}";

            //try
            //{
            //    string token = FirebaseInstanceId.Instance.Token;
            //    if (token != null) txtFirebaseId.Text = token;
            //}
            //catch
            //{
            //    DependencyService.Get<IToastMessage>().Show("Null IFirebaseMessage");
            //}

        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
            base.OnDisappearing();
        }

        private async void btnDisclaimer_Clicked(object sender, EventArgs e)
        {
            DisclaimerDialog popup = new DisclaimerDialog();
            await PopupNavigation.Instance.PushAsync(popup);
        }
    }
}