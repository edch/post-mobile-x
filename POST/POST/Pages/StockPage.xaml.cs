﻿using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Models;
using POST.Views;
using POST.Views.StockViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StockPage : TabbedPage
    {
        private class ClientState { }

        private readonly DataFeedClientStateManager<ClientState> _clientStateManager;
        private readonly ICollection<IDisposable> _observersMarketType = new List<IDisposable>();
        private readonly ICollection<IDisposable> _observersStockQuote = new List<IDisposable>();

        public static BindableProperty StockIdProperty = BindableProperty.Create("StockId", typeof(StockId?), typeof(StockPage), defaultValue: null, propertyChanged: OnStockIdChanged);
        public StockId? StockId
        {
            get => (StockId?)GetValue(StockIdProperty);
            set
            {
                StockId stockId = (StockId)value;
                SetValue(StockIdProperty, stockId);

                _clientStateManager.StopClient();

                foreach (IDisposable observer in _observersStockQuote)
                {
                    observer.Dispose();
                }
                _observersStockQuote.Clear();

                _clientStateManager.StartClient();

                _observersStockQuote.Add(Store.StockById
                    .Select(stockById => StockId.HasValue && stockById.TryGetValue(StockId.Value, out Stock stock) ? stock : null)
                    .Subscribe(stock => UpdateTitleView(stock)
                    )
                );
            }
        }

        private static void OnStockIdChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is StockPage stockPage)
            {
                if (newValue is StockId stockId)
                {
                    stockPage.StockCodeLabel.Text = stockId.Code;

                    //Syariah
                    if (Store.StockById.GetState().TryGetValue(stockId, out Stock stock))
                    {
                        stockPage.StockCodeLabel.TextDecorations = stock.TextDecoration;
                    }
                }
                else
                {
                    stockPage.StockCodeLabel.Text = string.Empty;
                    stockPage.StockCodeLabel.TextDecorations = TextDecorations.None;
                }
            }
        }

        public StockPage()
        {
            InitializeComponent();

            CurrentPage = null;

            CurrentPageChanged += new EventHandler(TabbedPage_CurrentPageChanged);

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState(),
                clientFactory: state =>
                {
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        if (StockId.HasValue)
                        {
                            await sender.SendAsync(new SubscribeStockQuoteRequest(StockId.Value));
                        }
                    };
                    client.DataReceived += (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case StockQuoteMessage stockQuoteMessage:
                                //Debug.WriteLine("StockPage " + stockQuoteMessage.StockCode + " " + stockQuoteMessage.MarketType.ToString() + " " + stockQuoteMessage.Volume);
                                Store.StockById.UpdateQuotes(new List<StockQuoteMessage> { stockQuoteMessage });
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
            );
        }

        private void UpdateTitleView(Stock stock)
        {
            if (stock != null)
            {
                StockCodeLabel.TextColor = stock.StockCodeForeground;
                StockPriceLabel.Text = $"{stock.LastOrPrevious:N0}";
                StockPriceLabel.TextColor = stock.ChangeForeground;
                StockChangeGlyphLabel.Text = stock.ChangeGlyph;
                StockChangeGlyphLabel.TextColor = stock.ChangeForeground;
                if (stock.ChangePercent.HasValue)
                {
                    StockChangePercentLabel.Text = $"{stock.ChangePercent.Value:N2}%";
                    StockChangePercentLabel.TextColor = stock.ChangeForeground;
                }
                else
                {
                    StockChangePercentLabel.Text = string.Empty;
                }
                if (stock.Change.HasValue)
                {
                    StockChangePointLabel.Text = stock.Change.Value.ToString("#,##0;0");
                    StockChangePointLabel.TextColor = stock.ChangeForeground;
                }
                else
                {
                    StockChangePointLabel.Text = string.Empty;
                }
            }
            else
            {
                StockPriceLabel.Text = string.Empty;
                StockChangeGlyphLabel.Text = string.Empty;
                StockChangePercentLabel.Text = string.Empty;
                StockChangePointLabel.Text = string.Empty;
            }
        }

        private readonly int iPosPriceChange = 0;

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Store.CurrentDisplayStockStore.SetStock(Store.RecentStockIds.GetState().ToList().Last());

            StockId = Store.CurrentDisplayStockStore.GetState();

            if (PreviousView != null)
            {
                PreviousView.SetStock(StockId);
                PreviousView.OnAppearing();
            }
            else
            {
                CurrentPage = StockOverview;
            }

            _observersMarketType.Add(Store.CurrentDisplayStockStore.Subscribe(stockId =>
                    {
                        if (StockId != stockId)
                        {
                            StockId = stockId;
                        }
                    }
                )
            );
        }

        protected override void OnDisappearing()
        {

            base.OnDisappearing();

            _clientStateManager.StopClient();

            foreach (IDisposable observer in _observersMarketType)
            {
                observer.Dispose();
            }
            _observersMarketType.Clear();

            foreach (IDisposable observer in _observersStockQuote)
            {
                observer.Dispose();
            }
            _observersStockQuote.Clear();

            if (PreviousView != null)
            {
                PreviousView.OnDisappearing();
            }
        }

        private IContentView_Stock PreviousView = null;

        private void TabbedPage_CurrentPageChanged(object sender, System.EventArgs e)
        {
            if (CurrentPage == StockOverview)
            {
                LoadContentView<StockOverviewView>(StockOverview);
            }
            else if (CurrentPage == OrderBook)
            {
                LoadContentView<OrderBookViewType2>(OrderBook);
            }
            else if (CurrentPage == CompleteBook)
            {
                LoadContentView<CompleteBookView>(CompleteBook);
            }
            else if (CurrentPage == StockFinancials)
            {
                LoadContentView<StockFinancialsView>(StockFinancials);
            }
            else if (CurrentPage == CompanyProfile)
            {
                LoadContentView<CompanyProfileView>(CompanyProfile);
            }
            else if (CurrentPage == CorporateAction)
            {
                LoadContentView<CorporateActionView>(CorporateAction);
            }
        }

        private void LoadContentView<TView>(ContentPage contentPage) where TView : ContentView, IContentView_Stock
        {
            if (CurrentPage == contentPage)
            {
                if (PreviousView != null)
                {
                    PreviousView.OnDisappearing();
                    PreviousView = null;
                }
                if (!(contentPage.Content is TView))
                {
                    contentPage.Content = (TView)Activator.CreateInstance(typeof(TView));
                }
                PreviousView = contentPage.Content as IContentView_Stock;
                PreviousView.SetStock(StockId);
                PreviousView.OnAppearing();
            }
        }

        private void StockOverviewChartModeButton_Clicked(object sender, EventArgs e)
        {
            if (StockOverview.Content is StockOverviewView stockOverviewView)
            {
                stockOverviewView.ChartModeButton_Clicked(sender, e);
            }
        }

        private void StockOverviewTechnicalChartButton_Clicked(object sender, EventArgs e)
        {
            if (StockOverview.Content is StockOverviewView stockOverviewView)
            {
                stockOverviewView.TechnicalChartButton_Clicked(sender, e);
            }
        }

        private void StockFinancialsQuarterSelectorItem_Clicked(object sender, EventArgs e)
        {
            if (StockFinancials.Content is StockFinancialsView stockFinancialsView)
            {
                stockFinancialsView.QuarterSelectorItem_Clicked(sender, e);
            }
        }

        private async void StockSearchButton_Clicked(object sender, EventArgs e)
        {
            StockSearchPage stockSearchPage = new StockSearchPage("StockPage");
            await MainPage.NavigationPage.PushAsync(stockSearchPage);
        }
    }
}