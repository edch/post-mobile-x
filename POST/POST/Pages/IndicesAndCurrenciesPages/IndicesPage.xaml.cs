﻿using POST.Constants;
using POST.Dialogs;
using POST.Models;
using POST.Models.Collections;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Index = POST.Models.Index;

namespace POST.Pages.IndicesAndCurrenciesPages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IndicesPage : ContentPage
    {
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public ObservableSet<string, Index> Indices { get; }

        private static readonly IReadOnlyDictionary<string, SortKey> SortKeys = new Dictionary<string, SortKey> {
            { "Index Code A-Z", SortKey.CodeAsc },
            { "Index Code Z-A", SortKey.CodeDesc },
            { "Top Gainer (%)", SortKey.ChangePercentDesc },
            { "Top Loser (%)", SortKey.ChangePercentAsc }
        };

        public IndicesPage()
        {
            InitializeComponent();
            Indices = new ObservableSet<string, Index>();
            IndicesListView.ItemsSource = Indices;

            foreach (string sortKey in SortKeys.Keys)
            {
                SortKeyPicker.Items.Add(sortKey);
            }

            // Observe IndexByCode, IndicesSortKey
            _observers.Add(
                Observable.Merge(
                    Store.IndexByCode.Select(indexByCode => new
                    {
                        IndexByCode = indexByCode,
                        SortKey = Store.IndicesSortKey.GetState()
                    }),
                    Store.IndicesSortKey.Select(sortKey => new
                    {
                        IndexByCode = Store.IndexByCode.GetState(),
                        SortKey = sortKey
                    })
                )
                .Select(merged =>
                {
                    // Sort indices by selected sort key
                    switch (merged.SortKey)
                    {
                        default:
                        case SortKey.CodeAsc:
                            return (from index in merged.IndexByCode.Values
                                    orderby index.Code ascending
                                    select index).ToList();
                        case SortKey.CodeDesc:
                            return (from index in merged.IndexByCode.Values
                                    orderby index.Code descending
                                    select index).ToList();
                        case SortKey.ChangePercentAsc:
                            return (from index in merged.IndexByCode.Values
                                    where index.OHLC.Close > 0
                                    orderby index.ChangePercent ascending
                                    select index).Union(
                                        from index in merged.IndexByCode.Values
                                        where index.OHLC.Close <= 0
                                        orderby index.Code ascending
                                        select index).ToList();
                        case SortKey.ChangePercentDesc:
                            return (from index in merged.IndexByCode.Values
                                    where index.OHLC.Close > 0
                                    orderby index.ChangePercent descending
                                    select index).Union(
                                        from index in merged.IndexByCode.Values
                                        where index.OHLC.Close <= 0
                                        orderby index.Code descending
                                        select index).ToList();
                    }
                })
                .ObserveOn(SynchronizationContext.Current)
                .Subscribe(indices =>
                {
                    // Update items source
                    Indices.SetItems(indices);
                })
            );

            // Observer for updating Sort icon in toolbar
            _observers.Add(Store.IndicesSortKey
                .ObserveOn(SynchronizationContext.Current)
                .Subscribe(sortKey =>
                {
                    SortButton.Text = Glyphs.ForSortKey(sortKey);
                })
            );
        }

        ~IndicesPage()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is Index index)
            {
                // TODO
            }
            if (sender is ListView listView)
            {
                listView.SelectedItem = null;
            }
        }

        private void SortButton_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                SortKeyPicker.Focus();
            });
        }

        private void SortKeyPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SortKeyPicker.SelectedItem is string selectedItem &&
                SortKeys.TryGetValue(selectedItem, out SortKey sortKey))
            {
                Store.IndicesSortKey.SortBy(sortKey);
            }
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}
