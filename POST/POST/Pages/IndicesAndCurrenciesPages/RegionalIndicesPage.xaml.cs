﻿using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Dialogs;
using POST.Models;
using POST.Models.Collections;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages.IndicesAndCurrenciesPages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegionalIndicesPage : ContentPage
    {

        private class ClientState
        {
            public List<RegionalIndexMessage> RegionalIndexMessages { get; } = new List<RegionalIndexMessage>();
            public bool Subscribed { get; set; }
        }

        private readonly DataFeedClientStateManager<ClientState> _clientStateManager;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public ObservableSet<string, RegionalIndex> RegionalIndices { get; }

        private static readonly IReadOnlyDictionary<string, SortKey> SortKeys = new Dictionary<string, SortKey> {
            { "Index Code A-Z", SortKey.CodeAsc },
            { "Index Code Z-A", SortKey.CodeDesc },
            { "Top Gainer (%)", SortKey.ChangePercentDesc },
            { "Top Loser (%)", SortKey.ChangePercentAsc }
        };

        public RegionalIndicesPage()
        {
            InitializeComponent();
            RegionalIndices = new ObservableSet<string, RegionalIndex>();
            RegionalIndicesListView.ItemsSource = RegionalIndices;

            foreach (string sortKey in SortKeys.Keys)
            {
                SortKeyPicker.Items.Add(sortKey);
            }

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState { Subscribed = false },
                clientFactory: state =>
                {
                    state.RegionalIndexMessages.Clear();
                    state.Subscribed = false;
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        await sender.SendAsync(new RegionalIndexInitRequest());
                    };
                    client.DataReceived += async (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case RegionalIndexMessage regionalIndexMessage:
                                state.RegionalIndexMessages.Add(regionalIndexMessage);
                                break;
                            case RegionalIndexEndMessage regionalIndexEndMessage:
                                Store.RegionalIndexByCode.UpdateQuotes(state.RegionalIndexMessages);
                                state.RegionalIndexMessages.Clear();
                                RegionalIndicesListView.IsRefreshing = false;

                                // HACK: We need to resend SubscribeAllRegionalIndexQuotesRequest to refresh all quotes.
                                // This should be fixed on server side.
                                await Task.Delay(60_000);
                                if (_clientStateManager.IsConnected)
                                {
                                    await sender.SendAsync(new SubscribeAllRegionalIndexQuotesRequest());
                                    state.Subscribed = true;
                                }
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
            );

            // Observe RegionalIndexByCode and RegionalIndicesSortKey together
            _observers.Add(
                Observable.Merge(
                    Store.RegionalIndexByCode.Select(regionalIndexByCode => new
                    {
                        RegionalIndexByCode = regionalIndexByCode,
                        SortKey = Store.RegionalIndicesSortKey.GetState()
                    }),
                    Store.RegionalIndicesSortKey.Select(sortKey => new
                    {
                        RegionalIndexByCode = Store.RegionalIndexByCode.GetState(),
                        SortKey = sortKey
                    })
                )
                .Select(merged =>
                {
                    switch (merged.SortKey)
                    {
                        default:
                        case SortKey.CodeAsc:
                            return (from regionalIndex in merged.RegionalIndexByCode.Values
                                    orderby regionalIndex.Code ascending
                                    select regionalIndex).ToList();
                        case SortKey.CodeDesc:
                            return (from regionalIndex in merged.RegionalIndexByCode.Values
                                    orderby regionalIndex.Code descending
                                    select regionalIndex).ToList();
                        case SortKey.ChangePercentAsc:
                            return (from regionalIndex in merged.RegionalIndexByCode.Values
                                    let changePercent = regionalIndex.ChangePercent
                                    where changePercent.HasValue
                                    orderby changePercent.Value ascending
                                    select regionalIndex).Union(
                                        from regionalIndex in merged.RegionalIndexByCode.Values
                                        let changePercent = regionalIndex.ChangePercent
                                        where !changePercent.HasValue
                                        orderby regionalIndex.Code ascending
                                        select regionalIndex).ToList();
                        case SortKey.ChangePercentDesc:
                            return (from regionalIndex in merged.RegionalIndexByCode.Values
                                    let changePercent = regionalIndex.ChangePercent
                                    where changePercent.HasValue
                                    orderby changePercent.Value descending
                                    select regionalIndex).Union(
                                        from regionalIndex in merged.RegionalIndexByCode.Values
                                        let changePercent = regionalIndex.ChangePercent
                                        where !changePercent.HasValue
                                        orderby regionalIndex.Code descending
                                        select regionalIndex).ToList();
                    }
                })
                .Subscribe(regionalIndices =>
                {
                    RegionalIndices.SetItems(regionalIndices);
                })
            );
        }

        ~RegionalIndicesPage()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _clientStateManager.StartClient();
        }

        protected override void OnDisappearing()
        {

            // Stop client
            _clientStateManager.StopClient();

            base.OnDisappearing();
        }

        private async void Handle_PullToRefresh(object sender, EventArgs e)
        {
            await _clientStateManager.SendAsync(new RegionalIndexInitRequest());
        }

        private void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (sender is ListView listView)
            {
                listView.SelectedItem = null;
            }
        }

        private void SortButton_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                SortKeyPicker.Focus();
            });
        }

        private void SortKeyPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SortKeyPicker.SelectedItem is string selectedItem &&
                SortKeys.TryGetValue(selectedItem, out SortKey sortKey))
            {
                Store.RegionalIndicesSortKey.SortBy(sortKey);
            }
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}
