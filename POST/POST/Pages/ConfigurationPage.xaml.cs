﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Pages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConfigurationPage : TabbedPage
    {
        public ConfigurationPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

    }
}