﻿using POST.Constants;
using POST.DataSources;
using POST.Views;
using POST.Views.MarketViews;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Index = POST.Models.Index;

namespace POST.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MarketPage : TabbedPage
    {

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();
        private CompositeChartView compositeChartView = null;
        private WatchListView watchListView = null;
        private MarketMoversView marketMoversView = null;
        private readonly BrokerQuoteView brokerQuoteView = null;

        public MarketPage()
        {
            InitializeComponent();
            CurrentPage = null;
            CurrentPageChanged += new EventHandler(TabbedPage_CurrentPageChanged);

            compositeChartView = (CompositeChartView)Activator.CreateInstance(typeof(CompositeChartView));
            watchListView = (WatchListView)Activator.CreateInstance(typeof(WatchListView));
            marketMoversView = (MarketMoversView)Activator.CreateInstance(typeof(MarketMoversView));
            brokerQuoteView = (BrokerQuoteView)Activator.CreateInstance(typeof(BrokerQuoteView));
        }

        ~MarketPage()
        {
            compositeChartView = null;
            watchListView = null;
            marketMoversView = null;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await DataFeedClient.StateManager.Current.SubscribeToAllStocksAsync(true);

            _observers.Add(Store.IndexByCode
              .Select(indexCode => indexCode.TryGetValue("COMPOSITE", out Index index) ? index : null)
              .Subscribe(UpdateTitleView)
            );

            MainPage.NavigationPage.BarBackgroundColor = Colors.Primary;

            if (PreviousView != null)
            {
                PreviousView.OnAppearing();
            }
            else
            {
                CurrentPage = CompositeChart;
            }
        }

        protected override async void OnDisappearing()
        {
            await DataFeedClient.StateManager.Current.SubscribeToAllStocksAsync(false);

            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();

            if (PreviousView != null)
            {
                PreviousView.OnDisappearing();
            }
            base.OnDisappearing();
        }

        private void UpdateTitleView(Index index)
        {
            if (index != null)
            {
                FlooredLastLabel.Text = $"{index.FlooredLast:N0}";
                LastDecimalLabel.Text = "." + $"{index.LastDecimals:000}";
                FlooredLastLabel.TextColor = index.ChangeForeground;
                LastDecimalLabel.TextColor = index.ChangeForeground;
                StockChangeGlyphLabel.Text = index.ChangeGlyph;
                StockChangeGlyphLabel.TextColor = index.ChangeForeground;
                if (index.ChangePercent.HasValue)
                {
                    StockChangePercentLabel.Text = $"{index.ChangePercent.Value:N2}%";
                    StockChangePercentLabel.TextColor = index.ChangeForeground;
                }
                else
                {
                    StockChangePercentLabel.Text = string.Empty;
                }

                StockChangePointLabel.Text = index.Change.ToString("#,##0.00;#,##0.00;#,##0.00");
                StockChangePointLabel.TextColor = index.ChangeForeground;
            }
            else
            {
                FlooredLastLabel.Text = string.Empty;
                LastDecimalLabel.Text = string.Empty;
                StockChangeGlyphLabel.Text = string.Empty;
                StockChangePercentLabel.Text = string.Empty;
                StockChangePointLabel.Text = string.Empty;
            }
        }

        private ControlTemplate GetControlTemplate(string name)
        {
            if (App.Current.Resources.TryGetValue(name, out object obj) &&
                obj is ControlTemplate controlTemplate)
            {
                return controlTemplate;
            }
            else
            {
                return null;
            }
        }

        private IContentView PreviousView = null;

        private void TabbedPage_CurrentPageChanged(object sender, System.EventArgs e)
        {
            if (CurrentPage == CompositeChart)
            {
                LoadContentView<CompositeChartView>(CompositeChart);
            }
            else if (CurrentPage == WatchList)
            {
                LoadContentView<WatchListView>(WatchList);
            }
            else if (CurrentPage == MarketMovers)
            {
                LoadContentView<MarketMoversView>(MarketMovers);
            }
            else if (CurrentPage == BrokerQuotes)
            {
                LoadContentView<BrokerQuoteView>(BrokerQuotes);
            }
        }

        private void LoadContentView<TView>(ContentPage contentPage) where TView : ContentView, IContentView
        {
            if (CurrentPage == contentPage)
            {
                if (PreviousView != null)
                {
                    PreviousView.OnDisappearing();
                    PreviousView = null;
                }

                if (!(contentPage.Content is TView))
                {
                    if (CurrentPage == CompositeChart)
                    {
                        contentPage.Content = compositeChartView;
                    }
                    else if (CurrentPage == WatchList)
                    {
                        contentPage.Content = watchListView;
                    }
                    else if (CurrentPage == MarketMovers)
                    {
                        contentPage.Content = marketMoversView;
                    }
                    else if (CurrentPage == BrokerQuotes)
                    {
                        contentPage.Content = brokerQuoteView;
                    }
                }

                PreviousView = contentPage.Content as IContentView;
                PreviousView.OnAppearing();
            }
        }

        private void WatchListAddButton_Clicked(object sender, EventArgs e)
        {
            if (WatchList.Content is WatchListView watchListView)
            {
                watchListView.AddButton_Clicked(sender, e);
            }
        }
        private void WatchListSortButton_Clicked(object sender, EventArgs e)
        {
            if (WatchList.Content is WatchListView watchListView)
            {
                watchListView.SortButton_Clicked(sender, e);
            }
        }

        private void WatchListSimpleViewButton_Clicked(object sender, EventArgs e)
        {
            if (WatchList.Content is WatchListView watchListView)
            {
                watchListView.SimpleViewButton_Clicked(sender, e);
            }
        }

        private void WatchListAdvancedViewButton_Clicked(object sender, EventArgs e)
        {
            if (WatchList.Content is WatchListView watchListView)
            {
                watchListView.AdvancedViewButton_Clicked(sender, e);
            }
        }

        //private void MarketMoversSimpleViewButton_Clicked(object sender, EventArgs e)
        //{
        //    if (MarketMovers.Content is MarketMoversView marketMoversView)
        //    {
        //        marketMoversView.SimpleViewButton_Clicked(sender, e);
        //    }
        //}

        //private void MarketMoversAdvancedViewButton_Clicked(object sender, EventArgs e)
        //{
        //    if (MarketMovers.Content is MarketMoversView marketMoversView)
        //    {
        //        marketMoversView.AdvancedViewButton_Clicked(sender, e);
        //    }
        //}

        private void WatchListDeleteStocksButton_Clicked(object sender, EventArgs e)
        {
            if (WatchList.Content is WatchListView watchListView)
            {
                watchListView.WatchListDeleteStocksButton_Clicked(sender, e);
            }
        }

        private void CompositeChartModeButton_Clicked(object sender, EventArgs e)
        {
            if (CompositeChart.Content is CompositeChartView compositeChartView)
            {
                compositeChartView.ChartModeButton_Clicked(sender, e);
            }
        }

        private void CompositeChartTechnicalChartButton_Clicked(object sender, EventArgs e)
        {
            if (CompositeChart.Content is CompositeChartView compositeChartView)
            {
                compositeChartView.TechnicalChartButton_Clicked(sender, e);
            }
        }

        private void BrokerQuotesSortButton_Clicked(object sender, EventArgs e)
        {
            if (BrokerQuotes.Content is BrokerQuoteView brokerQuoteView)
            {
                brokerQuoteView.SortButton_Clicked(sender, e);
            }
        }
    }
}