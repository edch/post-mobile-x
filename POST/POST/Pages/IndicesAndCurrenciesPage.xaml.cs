﻿using POST.Constants;
using POST.Views;
using POST.Views.IndicesAndCurrenciesViews;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Index = POST.Models.Index;

namespace POST.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IndicesAndCurrenciesPage : TabbedPage
    {

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public IndicesAndCurrenciesPage()
        {
            InitializeComponent();
        }

        private void UpdateTitleView(Index index)
        {
            if (index != null)
            {
                FlooredLastLabel.Text = $"{index.FlooredLast:N0}";
                LastDecimalLabel.Text = "." + $"{index.LastDecimals:000}";
                FlooredLastLabel.TextColor = index.ChangeForeground;
                LastDecimalLabel.TextColor = index.ChangeForeground;
                StockChangeGlyphLabel.Text = index.ChangeGlyph;
                StockChangeGlyphLabel.TextColor = index.ChangeForeground;
                if (index.ChangePercent.HasValue)
                {
                    StockChangePercentLabel.Text = $"{index.ChangePercent.Value:N2}%";
                    StockChangePercentLabel.TextColor = index.ChangeForeground;
                }
                else
                {
                    StockChangePercentLabel.Text = string.Empty;
                }
                StockChangePointLabel.Text = index.Change.ToString("#,##0.00;#,##0.00;#,##0.00");
                StockChangePointLabel.TextColor = index.ChangeForeground;
            }
            else
            {
                FlooredLastLabel.Text = string.Empty;
                LastDecimalLabel.Text = string.Empty;
                StockChangeGlyphLabel.Text = string.Empty;
                StockChangePercentLabel.Text = string.Empty;
                StockChangePointLabel.Text = string.Empty;
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.IndexByCode
                .Select(indexCode => indexCode.TryGetValue("COMPOSITE", out Index index) ? index : null)
                .Subscribe(UpdateTitleView)
            );

            MainPage.NavigationPage.BarBackgroundColor = Colors.Primary;

            if (PreviousView != null)
            {
                PreviousView.OnAppearing();
            }
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();

            if (PreviousView != null)
            {
                PreviousView.OnDisappearing();
            }
            base.OnDisappearing();
        }

        private ControlTemplate GetControlTemplate(string name)
        {
            if (App.Current.Resources.TryGetValue(name, out object obj) &&
                obj is ControlTemplate controlTemplate)
            {
                return controlTemplate;
            }
            else
            {
                return null;
            }
        }

        private const int AppearingDelay = 2000;
        private IContentView PreviousView = null;

        private void TabbedPage_CurrentPageChanged(object sender, System.EventArgs e)
        {
            if (CurrentPage == Indices)
            {
                LoadContentView<IndicesView>(Indices);
            }
            else if (CurrentPage == RegionalIndices)
            {
                LoadContentView<RegionalIndicesView>(RegionalIndices);
            }
            else if (CurrentPage == Currencies)
            {
                LoadContentView<CurrenciesView>(Currencies);
            }
        }

        private void LoadContentView<TView>(ContentPage contentPage) where TView : ContentView, IContentView
        {
            if (CurrentPage == contentPage)
            {
                if (PreviousView != null)
                {
                    PreviousView.OnDisappearing();
                    PreviousView = null;
                }
                if (!(contentPage.Content is TView))
                {
                    contentPage.Content = (TView)Activator.CreateInstance(typeof(TView));
                }
                PreviousView = contentPage.Content as IContentView;
                PreviousView.OnAppearing();
            }
        }

        private void IndicesSortButton_Clicked(object sender, EventArgs e)
        {
            if (Indices.Content is IndicesView indicesView)
            {
                indicesView.SortButton_Clicked(sender, e);
            }
        }

        private void RegionalIndicesSortButton_Clicked(object sender, EventArgs e)
        {
            if (RegionalIndices.Content is RegionalIndicesView regionalIndicesView)
            {
                regionalIndicesView.SortButton_Clicked(sender, e);
            }
        }

        private void CurrenciesBtnCalc_Clicked(object sender, EventArgs e)
        {
            if (Currencies.Content is CurrenciesView currenciesView)
            {
                currenciesView.BtnCalc_Clicked(sender, e);
            }
        }

    }
}