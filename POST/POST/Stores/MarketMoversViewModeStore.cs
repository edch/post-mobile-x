﻿using POST.Models;
using Redux;
using System;

namespace POST.Stores
{

    public class MarketMoversViewModeStore : Store<ViewMode>
    {

        public MarketMoversViewModeStore() : base(
            reducer: Reduce,
            initialState: AppSettings.MarketMoversViewMode)
        { }

        private static ViewMode Reduce(ViewMode state, IAction action)
        {
            switch (action)
            {
                case Actions.SetViewMode setViewModeAction:
                    return AppSettings.MarketMoversViewMode = setViewModeAction.ViewMode;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(MarketMoversViewModeStore)}.");
            }
        }

        private static class Actions
        {
            public class SetViewMode : IAction
            {
                public ViewMode ViewMode { get; }
                public SetViewMode(ViewMode viewMode)
                {
                    ViewMode = viewMode;
                }
            }
        }

        public IAction SetViewMode(ViewMode viewMode)
        {
            return Dispatch(new Actions.SetViewMode(viewMode));
        }
    }
}
