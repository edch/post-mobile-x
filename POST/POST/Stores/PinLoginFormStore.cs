﻿using POST.Models;
using Redux;
using System;

namespace POST.Stores
{

    public class PinLoginFormStore : Store<PinLoginForm>
    {

        public PinLoginFormStore() : base(
            reducer: Reduce,
            initialState: new PinLoginForm(
                brokerAccountId: AppSettings.BrokerAccountId
            ))
        { }

        private static PinLoginForm Reduce(PinLoginForm state, IAction action)
        {
            switch (action)
            {
                case Actions.SetBrokerAccountId setBrokerAccountIdAction:
                    AppSettings.BrokerAccountId = setBrokerAccountIdAction.BrokerAccountId;
                    return new PinLoginForm(
                        brokerAccountId: setBrokerAccountIdAction.BrokerAccountId
                    );
                case Actions.Clear clearAction:
                    AppSettings.BrokerAccountId = null;
                    return new PinLoginForm(
                        brokerAccountId: null
                    );
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(PinLoginFormStore)}.");
            }
        }

        private static class Actions
        {
            public class SetBrokerAccountId : IAction
            {
                public string BrokerAccountId { get; }
                public SetBrokerAccountId(string brokerAccountId)
                {
                    BrokerAccountId = brokerAccountId ?? throw new ArgumentNullException(nameof(brokerAccountId));
                }
            }
            public class Clear : IAction { }
        }

        public IAction SetBrokerAccountId(string brokerAccountId)
        {
            return Dispatch(new Actions.SetBrokerAccountId(brokerAccountId));
        }

        public IAction Clear()
        {
            return Dispatch(new Actions.Clear());
        }
    }
}
