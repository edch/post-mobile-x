﻿using POST.DataSources.DataFeedMessages;
using POST.Models;
using Redux;
using System;
using System.Diagnostics;

namespace POST.Stores
{

    public class StockChartStore : Store<(StockChartTimeUnit TimeUnit, StockChart StockChart)>
    {

        public StockChartStore() : base(
            reducer: Reduce,
            initialState: (
                TimeUnit: StockChartTimeUnit.Minute,
                StockChart: null
            ))
        { }

        private static (StockChartTimeUnit TimeUnit, StockChart StockChart) Reduce((StockChartTimeUnit TimeUnit, StockChart StockChart) state, IAction action)
        {
            switch (action)
            {
                case StockChartMessage stockChartMessage:
                    StockChartTuple stockChartTuple = new StockChartTuple(
                        time: stockChartMessage.Time,
                        open: stockChartMessage.Open,
                        high: stockChartMessage.High,
                        low: stockChartMessage.Low,
                        close: stockChartMessage.Close,
                        volume: stockChartMessage.Volume
                    );
                    if (state.StockChart != null)
                    {
                        if (state.StockChart.StockCode == stockChartMessage.StockCode)
                        {
                            state.StockChart.UpdateWithChartTuple(stockChartTuple);
                        }
                        else
                        {
                            Debug.WriteLine($"BAD: Received Stock Chart of {stockChartMessage.StockCode}. Expected {state.StockChart.StockCode}.");
                        }
                        return state;
                    }
                    else
                    {
                        return (
                            state.TimeUnit,
                            StockChart: new StockChart(
                                stockCode: stockChartMessage.StockCode,
                                timeUnit: state.TimeUnit
                            ) {
                                stockChartTuple
                            }
                        );
                    }
                case Actions.ClearChart clearChartAction:
                    return (
                        TimeUnit: StockChartTimeUnit.Minute,
                        StockChart: null
                    );
                case Actions.SetTimeUnit setTimeUnitAction:
                    if (state.TimeUnit == setTimeUnitAction.TimeUnit)
                    {
                        return state;
                    }
                    else
                    {
                        return (
                            setTimeUnitAction.TimeUnit,
                            StockChart: null
                        );
                    }
                case Actions.ClearChartIfStockCodeDoesntMatch clearChartIfStockCodeDoesntMatchAction:
                    if (state.StockChart?.StockCode != clearChartIfStockCodeDoesntMatchAction.StockCode)
                    {
                        return (
                            state.TimeUnit,
                            StockChart: null
                        );
                    }
                    else
                    {
                        return state;
                    }
                case Actions.SetTimeUnitAndClearChartIfStockCodeDoesntMatch setTimeUnitAndClearChartIfStockCodeDoesntMatchAction:
                    if (state.TimeUnit != setTimeUnitAndClearChartIfStockCodeDoesntMatchAction.TimeUnit ||
                        state.StockChart?.StockCode != setTimeUnitAndClearChartIfStockCodeDoesntMatchAction.StockCode)
                    {
                        return (
                            setTimeUnitAndClearChartIfStockCodeDoesntMatchAction.TimeUnit,
                            StockChart: null
                        );
                    }
                    else
                    {
                        return state;
                    }
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(StockChartStore)}.");
            }
        }

        private class Actions
        {

            public class ClearChart : IAction { }

            public class SetTimeUnit : IAction
            {
                public StockChartTimeUnit TimeUnit { get; }
                public SetTimeUnit(StockChartTimeUnit timeUnit)
                {
                    TimeUnit = timeUnit;
                }
            }

            public class ClearChartIfStockCodeDoesntMatch : IAction
            {
                public string StockCode { get; }
                public ClearChartIfStockCodeDoesntMatch(string stockCode)
                {
                    StockCode = stockCode ?? throw new ArgumentNullException(nameof(stockCode));
                }
            }

            public class SetTimeUnitAndClearChartIfStockCodeDoesntMatch : IAction
            {
                public StockChartTimeUnit TimeUnit { get; }
                public string StockCode { get; }
                public SetTimeUnitAndClearChartIfStockCodeDoesntMatch(StockChartTimeUnit timeUnit, string stockCode)
                {
                    TimeUnit = timeUnit;
                    StockCode = stockCode ?? throw new ArgumentNullException(nameof(stockCode));
                }
            }
        }

        public IAction ClearChart()
        {
            return Dispatch(new Actions.ClearChart());
        }

        public IAction SetTimeUnit(StockChartTimeUnit timeUnit)
        {
            return Dispatch(new Actions.SetTimeUnit(timeUnit));
        }

        public IAction ClearChartIfStockCodeDoesntMatch(string stockCode)
        {
            return Dispatch(new Actions.ClearChartIfStockCodeDoesntMatch(stockCode));
        }

        public IAction SetTimeUnitAndClearChartIfStockCodeDoesntMatch(StockChartTimeUnit timeUnit, string stockCode)
        {
            return Dispatch(new Actions.SetTimeUnitAndClearChartIfStockCodeDoesntMatch(timeUnit, stockCode));
        }
    }
}
