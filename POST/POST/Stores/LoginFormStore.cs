﻿using POST.Models;
using Redux;
using System;

namespace POST.Stores
{
    public class LoginFormStore : Store<LoginForm>
    {
        public LoginFormStore() : base(
            reducer: Reduce,
            initialState: AppSettings.LoginForm)
        { }

        private static LoginForm Reduce(LoginForm state, IAction action)
        {
            switch (action)
            {
                case Actions.Set setAction:
                    return AppSettings.LoginForm = setAction.Form;
                case Actions.ClearPassword clearPasswordAction:
                    return AppSettings.LoginForm = new LoginForm(
                        userId: state.UserId,
                         password: null,
                        hashedPassword: null
                    );
                case Actions.Clear clearAction:
                    return AppSettings.LoginForm = null;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(LoginFormStore)}.");
            }
        }

        private static class Actions
        {
            public class Set : IAction
            {
                public LoginForm Form { get; }
                public Set(LoginForm form)
                {
                    Form = form ?? throw new ArgumentNullException(nameof(form));
                }
            }
            public class ClearPassword : IAction { }
            public class Clear : IAction { }
        }

        public IAction Set(LoginForm loginForm)
        {
            return Dispatch(new Actions.Set(loginForm));
        }

        public IAction ClearPassword()
        {
            return Dispatch(new Actions.ClearPassword());
        }

        public IAction Clear()
        {
            return Dispatch(new Actions.Clear());
        }
    }
}
