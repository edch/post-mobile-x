﻿using POST.DataSources.DataFeedMessages;
using POST.Models;
using POST.Models.Collections;
using Redux;
using System;

namespace POST.Stores
{

    public class RunningTradeStore : Store<RunningTrade>
    {

        public RunningTradeStore() : base(
            reducer: Reduce,
            initialState: new RunningTrade())
        { }

        private static RunningTrade Reduce(RunningTrade state, IAction action)
        {
            switch (action)
            {
                case Actions.AddTrade addTradeAction:
                    state.AddTrade(addTradeAction.Trade);
                    return state;
                case Actions.AddRunningTradeMessage addRunningTradeMessageAction:
                    state.AddTrade(new Trade(
                        stockCode: addRunningTradeMessageAction.RunningTradeMessage.StockCode,
                        stockType: addRunningTradeMessageAction.RunningTradeMessage.StockType,
                        time: DateTime.Parse(addRunningTradeMessageAction.RunningTradeMessage.Time),
                        marketType: addRunningTradeMessageAction.RunningTradeMessage.MarketType,
                        previous: addRunningTradeMessageAction.RunningTradeMessage.Previous,
                        price: addRunningTradeMessageAction.RunningTradeMessage.Price,
                        lot: addRunningTradeMessageAction.RunningTradeMessage.VolumeLot,
                        buyerCode: addRunningTradeMessageAction.RunningTradeMessage.BuyerCode,
                        buyerType: addRunningTradeMessageAction.RunningTradeMessage.BuyerType,
                        sellerCode: addRunningTradeMessageAction.RunningTradeMessage.SellerCode,
                        sellerType: addRunningTradeMessageAction.RunningTradeMessage.SellerType,
                        tradeType: addRunningTradeMessageAction.RunningTradeMessage.TradeType
                    ));
                    return state;
                case Actions.Clear clearAction:
                    return new RunningTrade()
                    {
                        PageSize = state.PageSize
                    };
                case Actions.SetPageSize setPageSizeAction:
                    state.PageSize = setPageSizeAction.PageSize;
                    return state;
                case Actions.RenderCollection renderCollection:
                    {
                        state.RenderCollection();
                        return state;
                    }

                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(RunningTradeStore)}.");
            }
        }

        private static class Actions
        {
            public class AddTrade : IAction
            {
                public Trade Trade { get; }
                public AddTrade(Trade trade)
                {
                    Trade = trade ?? throw new ArgumentNullException(nameof(trade));
                }
            }

            public class AddRunningTradeMessage : IAction
            {
                public RunningTradeMessage RunningTradeMessage { get; }
                public AddRunningTradeMessage(RunningTradeMessage runningTradeMessage)
                {
                    RunningTradeMessage = runningTradeMessage ?? throw new ArgumentNullException(nameof(runningTradeMessage));
                }
            }

            public class Clear : IAction { }

            public class SetPageSize : IAction
            {
                public int PageSize { get; }
                public SetPageSize(int pageSize)
                {
                    PageSize = pageSize;
                }
            }

            public class RenderCollection : IAction { }
        }

        public IAction AddTrade(Trade trade)
        {
            return Dispatch(new Actions.AddTrade(trade));
        }

        public IAction AddTrade(RunningTradeMessage runningTradeMessage)
        {
            return Dispatch(new Actions.AddRunningTradeMessage(runningTradeMessage));
        }

        public IAction Clear()
        {
            return Dispatch(new Actions.Clear());
        }

        public IAction SetPageSize(int pageSize)
        {
            return Dispatch(new Actions.SetPageSize(pageSize));
        }

        public IAction RenderCollection()
        {
            return Dispatch(new Actions.RenderCollection());
        }
    }
}
