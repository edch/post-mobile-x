﻿using POST.DataSources;
using Redux;
using System;

namespace POST.Stores
{

    public class DataFeedStatusStore : Store<DataFeedStatus>
    {

        public DataFeedStatusStore() : base(
            reducer: Reduce,
            initialState: DataFeedStatus.Unconnected)
        { }

        private static DataFeedStatus Reduce(DataFeedStatus state, IAction action)
        {
            switch (action)
            {
                case Actions.SetStatus setStatusAction:
                    return setStatusAction.Status;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(DataFeedStatusStore)}.");
            }
        }

        private static class Actions
        {
            public class SetStatus : IAction
            {
                public DataFeedStatus Status { get; }
                public SetStatus(DataFeedStatus status)
                {
                    Status = status;
                }
            }
        }

        public IAction SetStatus(DataFeedStatus status)
        {
            return Dispatch(new Actions.SetStatus(status));
        }
    }
}
