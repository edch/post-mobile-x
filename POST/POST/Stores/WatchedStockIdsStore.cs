﻿using POST.Models;
using Redux;
using System;
using System.Collections.Generic;
using System.Linq;

namespace POST.Stores
{

    /// <summary>
    /// Note: StockId is a tuple of (StockCode, MarketType)
    /// </summary>
    public class WatchedStockIdsStore : Store<IReadOnlyList<StockId>>
    {

        public WatchedStockIdsStore() : base(
            reducer: Reduce,
            initialState: AppSettings.WatchedStockIds
        )
        { }

        private static IReadOnlyList<StockId> Reduce(IReadOnlyList<StockId> state, IAction action)
        {
            StockId stockId;

            switch (action)
            {
                case Actions.AddStock addStockAction:
                    stockId = addStockAction.StockId;
                    if (state.Contains(stockId))
                    {
                        return state;
                    }
                    else
                    {
                        List<StockId> stockIds = new List<StockId>(state)
                        {
                            stockId
                        };
                        AppSettings.WatchedStockIds = stockIds;
                        return stockIds;
                    }
                case Actions.RemoveStock removeStockAction:
                    stockId = removeStockAction.StockId;
                    if (state.Contains(stockId))
                    {
                        List<StockId> stockIds = new List<StockId>(state);
                        stockIds.Remove(stockId);
                        AppSettings.WatchedStockIds = stockIds;
                        return stockIds;
                    }
                    else
                    {
                        return state;
                    }
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(WatchedStockIdsStore)}.");
            }
        }

        private static class Actions
        {
            public class AddStock : IAction
            {
                public StockId StockId { get; }
                public AddStock(StockId stockId)
                {
                    StockId = stockId;
                }
            }
            public class RemoveStock : IAction
            {
                public StockId StockId { get; }
                public RemoveStock(StockId stockId)
                {
                    StockId = stockId;
                }
            }
        }

        public IAction AddStock(StockId stockId)
        {
            return Dispatch(new Actions.AddStock(stockId));
        }

        public IAction AddStock(string stockCode)
        {
            return Dispatch(new Actions.AddStock(new StockId(stockCode, MarketType.RG)));
        }

        public IAction RemoveStock(StockId stockId)
        {
            return Dispatch(new Actions.RemoveStock(stockId));
        }

        public IAction RemoveStock(string stockCode)
        {
            return Dispatch(new Actions.RemoveStock(new StockId(stockCode, MarketType.RG)));
        }
    }
}
