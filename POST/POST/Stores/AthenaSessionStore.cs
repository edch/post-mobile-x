﻿using POST.Models;
using Redux;
using System;

namespace POST.Stores
{
    public class AthenaSessionStore : Store<AthenaSession>
    {
        public AthenaSessionStore() : base(
            reducer: Reduce,
            initialState: null)
        { }

        private static AthenaSession Reduce(AthenaSession state, IAction action)
        {
            switch (action)
            {
                case Actions.SetSession setSessionAction:
                    return setSessionAction.Session;
                case Actions.UseBrokerAccount useBrokerAccountAction:
                    return state != null ? new AthenaSession(
                        userId: state.UserId,
                        brokerAccountId: useBrokerAccountAction.BrokerAccountId,
                        useAppWithoutBrokerAccount: false,
                        state.Client
                    ) : null;
                case Actions.UseAppWithoutBrokerAccount useAppWithoutBrokerAccountAction:
                    return state != null ? new AthenaSession(
                        userId: state.UserId,
                        brokerAccountId: null,
                        useAppWithoutBrokerAccount: true,
                        state.Client
                    ) : null;
                case Actions.Clear clearAction:
                    return null;
                case Actions.Refresh refreshAction:
                    return state;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(AthenaSessionStore)}.");
            }
        }

        private static class Actions
        {
            public class SetSession : IAction
            {
                public AthenaSession Session { get; }
                public SetSession(AthenaSession session)
                {
                    Session = session ?? throw new ArgumentNullException(nameof(session));
                }
            }
            public class UseBrokerAccount : IAction
            {
                public string BrokerAccountId { get; }
                public UseBrokerAccount(string brokerAccountId)
                {
                    BrokerAccountId = brokerAccountId ?? throw new ArgumentNullException(nameof(brokerAccountId));
                }
            }
            public class UseAppWithoutBrokerAccount : IAction { }
            public class Clear : IAction { }
            public class Refresh : IAction { }
        }

        public IAction SetSession(AthenaSession session)
        {
            return Dispatch(new Actions.SetSession(session));
        }

        public IAction UseBrokerAccount(string brokerAccountId)
        {
            return Dispatch(new Actions.UseBrokerAccount(brokerAccountId));
        }

        public IAction UseAppWithoutBrokerAccount()
        {
            return Dispatch(new Actions.UseAppWithoutBrokerAccount());
        }

        public IAction ClearSession()
        {
            return Dispatch(new Actions.Clear());
        }

        public IAction RefreshSession()
        {
            return Dispatch(new Actions.Refresh());
        }
    }
}
