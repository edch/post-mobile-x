﻿using POST.Models;
using Redux;
using System;

namespace POST.Stores
{

    public class WatchListViewModeStore : Store<ViewMode>
    {

        public WatchListViewModeStore() : base(
            reducer: Reduce,
            initialState: AppSettings.WatchListViewMode)
        { }

        private static ViewMode Reduce(ViewMode state, IAction action)
        {
            switch (action)
            {
                case Actions.SetViewMode setViewModeAction:
                    return AppSettings.WatchListViewMode = setViewModeAction.ViewMode;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(WatchListViewModeStore)}.");
            }
        }

        private static class Actions
        {
            public class SetViewMode : IAction
            {
                public ViewMode ViewMode { get; }
                public SetViewMode(ViewMode viewMode)
                {
                    ViewMode = viewMode;
                }
            }
        }

        public IAction SetViewMode(ViewMode viewMode)
        {
            return Dispatch(new Actions.SetViewMode(viewMode));
        }
    }
}
