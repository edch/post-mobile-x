﻿using POST.Models;
using Redux;
using System;

namespace POST.Stores
{
    public class RightAvailableStatusFilterStore : Store<string>
    {

        public RightAvailableStatusFilterStore() : base(
            reducer: Reduce,
            initialState: RightsAvailableStatusDict.Items[1])
        { }

        private static string Reduce(string state, IAction action)
        {
            switch (action)
            {
                case Actions.Set set:
                    return set.RightAvailableStatus;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(RightAvailableStatusFilterStore)}.");
            }
        }

        private static class Actions
        {
            public class Set : IAction
            {
                public string RightAvailableStatus { get; }
                public Set(string fundWithdrawalStatus)
                {
                    RightAvailableStatus = fundWithdrawalStatus;
                }
            }
        }

        public IAction Set(string fundWithdrawalStatus)
        {
            return Dispatch(new Actions.Set(fundWithdrawalStatus));
        }
    }
}
