﻿using POST.Models;
using Redux;
using System;



namespace POST.Stores

{

    public class FundWithdrawalStatusFilterStore : Store<string>

    {



        public FundWithdrawalStatusFilterStore() : base(

            reducer: Reduce,

            initialState: FundWithdrawalStatusDict.Items[0])

        { }



        private static string Reduce(string state, IAction action)

        {

            switch (action)

            {

                case Actions.Set set:

                    return set.FundWithdrawalStatus;

                default:

                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(FundWithdrawalStatusFilterStore)}.");

            }

        }



        private static class Actions

        {

            public class Set : IAction

            {

                public string FundWithdrawalStatus { get; }

                public Set(string fundWithdrawalStatus)

                {

                    FundWithdrawalStatus = fundWithdrawalStatus;

                }

            }

        }



        public IAction Set(string fundWithdrawalStatus)
        {
            return Dispatch(new Actions.Set(fundWithdrawalStatus));
        }
    }

}

