﻿using POST.DataSources.ApiResponses;
using Redux;
using System;
using System.Collections.Generic;
using System.Linq;

namespace POST.Stores
{

    public class ExternalNewsFeedStore : Store<IReadOnlyList<ExternalNewsSnippet>>
    {

        public ExternalNewsFeedStore() : base(
            reducer: Reduce,
            initialState: new List<ExternalNewsSnippet>())
        { }

        private static IReadOnlyList<ExternalNewsSnippet> Reduce(IReadOnlyList<ExternalNewsSnippet> state, IAction action)
        {
            switch (action)
            {
                case Actions.UpdateNewsFeed updateNewsFeedAction:
                    return updateNewsFeedAction.NewsFeed.ToList();
                case Actions.SetContent setContentAction:
                    for (int i = 0; i < state.Count; i++)
                    {
                        if (state[i].MessageNo == setContentAction.ExternalNews.MessageNo)
                        {
                            return state
                                .Take(i)
                                .Union(new List<ExternalNews> { setContentAction.ExternalNews })
                                .Union(state.Skip(i + 1))
                                .ToList();
                        }
                    }
                    return state;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(ExternalNewsFeedStore)}.");
            }
        }

        private static class Actions
        {
            public class UpdateNewsFeed : IAction
            {
                public ICollection<ExternalNewsSnippet> NewsFeed { get; }
                public UpdateNewsFeed(ICollection<ExternalNewsSnippet> newsFeed)
                {
                    NewsFeed = newsFeed ?? throw new ArgumentNullException(nameof(newsFeed));
                }
            }
            public class SetContent : IAction
            {
                public ExternalNews ExternalNews { get; }
                public SetContent(ExternalNews externalNews)
                {
                    ExternalNews = externalNews ?? throw new ArgumentNullException(nameof(externalNews));
                }
            }
        }

        public IAction UpdateNewsFeed(ICollection<ExternalNewsSnippet> newsFeed)
        {
            return Dispatch(new Actions.UpdateNewsFeed(newsFeed));
        }

        public IAction SetContent(ExternalNews news)
        {
            return Dispatch(new Actions.SetContent(news));
        }
    }
}
