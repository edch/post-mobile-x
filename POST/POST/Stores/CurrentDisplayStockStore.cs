﻿using POST.Models;
using Redux;
using System;

namespace POST.Stores
{

    /// <summary>
    /// Note: StockId is a tuple of (StockCode, MarketType)
    /// </summary>
    public class CurrentDisplayStockStore : Store<StockId>
    {

        public CurrentDisplayStockStore() : base(
            reducer: Reduce,
            initialState: new StockId("AALI", MarketType.RG)
        )
        { }

        private static StockId Reduce(StockId state, IAction action)
        {
            switch (action)
            {
                case Actions.SetStock setStock:
                    StockId stockId = setStock.StockId;
                    if (setStock.StockId.Code.ToLower().Contains("-r"))
                    {
                        stockId.MarketType = MarketType.TN;
                    }
                    return stockId;
                case Actions.SetMarketTypeRG setMarketTypeRG:
                    return new StockId(state.Code, MarketType.RG);
                case Actions.SetMarketTypeTN setMarketTypeTN:
                    return new StockId(state.Code, MarketType.TN);
                case Actions.SetMarketTypeNG setMarketTypeNG:
                    return new StockId(state.Code, MarketType.NG);
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(RecentStockIdsStore)}.");
            }
        }

        private static class Actions
        {
            public class SetStock : IAction
            {
                public StockId StockId { get; }
                public SetStock(StockId stockId)
                {
                    StockId = stockId;
                }
            }

            public class SetMarketTypeRG : IAction
            {
                public SetMarketTypeRG()
                {
                }
            }

            public class SetMarketTypeTN : IAction
            {
                public SetMarketTypeTN()
                {
                }
            }

            public class SetMarketTypeNG : IAction
            {
                public SetMarketTypeNG()
                {
                }
            }
        }

        public IAction SetStock(StockId stockId)
        {
            return Dispatch(new Actions.SetStock(stockId));
        }

        public IAction SetMarketTypeRG()
        {
            return Dispatch(new Actions.SetMarketTypeRG());
        }

        public IAction SetMarketTypeTN()
        {
            return Dispatch(new Actions.SetMarketTypeTN());
        }

        public IAction SetMarketTypeNG()
        {
            return Dispatch(new Actions.SetMarketTypeNG());
        }
    }
}
