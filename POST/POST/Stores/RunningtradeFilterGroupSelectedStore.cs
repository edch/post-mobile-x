﻿using Redux;
using System;

namespace POST.Stores
{

    public class RunningTradeFilterGroupSelectedStore : Store<string>
    {

        public RunningTradeFilterGroupSelectedStore() : base(
            reducer: Reduce,
            initialState: AppSettings.RunningtradeFilterGroupSelected)
        { }

        private static string Reduce(string state, IAction action)
        {
            switch (action)
            {
                case Actions.Set setAction:
                    return AppSettings.RunningtradeFilterGroupSelected = setAction.ItemSeleted;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(WatchListSortKeyStore)}.");
            }
        }

        private static class Actions
        {
            public class Set : IAction
            {
                public string ItemSeleted { get; }
                public Set(string itemSeleted)
                {
                    ItemSeleted = itemSeleted;
                }
            }
        }

        public IAction Set(string itemSeleted)
        {
            return Dispatch(new Actions.Set(itemSeleted));
        }
    }
}
