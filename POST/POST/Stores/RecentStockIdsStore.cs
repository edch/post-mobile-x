﻿using POST.Models;
using Redux;
using System;
using System.Collections.Generic;
using System.Linq;

namespace POST.Stores
{

    /// <summary>
    /// Note: StockId is a tuple of (StockCode, MarketType)
    /// </summary>
    public class RecentStockIdsStore : Store<IReadOnlyList<StockId>>
    {

        public RecentStockIdsStore() : base(
            reducer: Reduce,
            initialState: AppSettings.RecentStockIdsForStoreInitalState
        )
        {

        }

        private static IReadOnlyList<StockId> Reduce(IReadOnlyList<StockId> state, IAction action)
        {
            switch (action)
            {
                case Actions.AddStock addStockAction:
                    if (AppSettings.RecentStockIds.Count == 0)
                    {
                        state = new List<StockId>();
                    }

                    if (state.Contains(addStockAction.StockId))
                    {
                        List<StockId> stockIds = new List<StockId>(state);
                        stockIds.Remove(addStockAction.StockId);
                        stockIds.Add(addStockAction.StockId);
                        if (stockIds.Count > 5)
                        {
                            stockIds.RemoveAt(0);
                        }

                        AppSettings.RecentStockIds = stockIds;
                        return stockIds;
                    }
                    else
                    {
                        List<StockId> stockIds = new List<StockId>(state)
                        {
                            addStockAction.StockId
                        };
                        if (stockIds.Count > 5)
                        {
                            stockIds.RemoveAt(0);
                        }

                        AppSettings.RecentStockIds = stockIds;
                        return stockIds;
                    }
                case Actions.ClearStock clearStock:
                    StockId stockId;
                    if (AppSettings.RecentStockIds.Count != 0)
                    {
                        stockId = AppSettings.RecentStockIds.Last();
                    }
                    else
                    {
                        stockId = new StockId("AALI", MarketType.RG);
                    }
                    AppSettings.RecentStockIds = new List<StockId>();
                    return new List<StockId>() { stockId };
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(RecentStockIdsStore)}.");
            }
        }

        private static class Actions
        {
            public class AddStock : IAction
            {
                public StockId StockId { get; }
                public AddStock(StockId stockId)
                {
                    stockId.MarketType = MarketType.RG;
                    StockId = stockId;
                }
            }

            public class ClearStock : IAction
            {
                public ClearStock()
                {
                }
            }
        }

        public IAction AddStock(StockId stockId)
        {
            return Dispatch(new Actions.AddStock(stockId));
        }

        public IAction AddStock(string stockCode)
        {
            return Dispatch(new Actions.AddStock(new StockId(stockCode, MarketType.RG)));
        }

        public IAction ClearStock()
        {
            return Dispatch(new Actions.ClearStock());
        }
    }
}
