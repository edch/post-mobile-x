﻿using POST.DataSources.DataFeedMessages;
using Redux;
using System;
using System.Collections.Generic;
using Index = POST.Models.Index;

namespace POST.Stores
{

    public class IndexByCodeStore : Store<IReadOnlyDictionary<string, Index>>
    {

        public IndexByCodeStore() : base(
            reducer: Reduce,
            initialState: new Dictionary<string, Index>())
        { }

        private static IReadOnlyDictionary<string, Index> Reduce(IReadOnlyDictionary<string, Index> state, IAction action)
        {
            switch (action)
            {
                case IndexQuoteMessage indexQuoteMessage:
                    Dictionary<string, Index> clone = new Dictionary<string, Index>();
                    foreach (string indexCode in state.Keys)
                    {
                        clone.Add(indexCode, state[indexCode]);
                    }
                    if (clone.TryGetValue(indexQuoteMessage.IndexCode, out Index existingIndex))
                    {
                        existingIndex.UpdateWith(
                            previous: indexQuoteMessage.Previous,
                            open: indexQuoteMessage.Open,
                            high: indexQuoteMessage.High,
                            low: indexQuoteMessage.Low,
                            last: indexQuoteMessage.Last,
                            volume: indexQuoteMessage.Volume,
                            value: indexQuoteMessage.Value,
                            frequency: indexQuoteMessage.Frequency
                        );
                    }
                    else
                    {
                        clone.Add(indexQuoteMessage.IndexCode, new Index(
                            code: indexQuoteMessage.IndexCode,
                            previous: indexQuoteMessage.Previous,
                            open: indexQuoteMessage.Open,
                            high: indexQuoteMessage.High,
                            low: indexQuoteMessage.Low,
                            last: indexQuoteMessage.Last,
                            volume: indexQuoteMessage.Volume,
                            value: indexQuoteMessage.Value,
                            frequency: indexQuoteMessage.Frequency
                        ));
                    }
                    return clone;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(IndexByCodeStore)}.");
            }
        }
    }
}
