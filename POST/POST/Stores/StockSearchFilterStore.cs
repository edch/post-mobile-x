﻿using POST.Models;
using Redux;
using System;

namespace POST.Stores
{
    public class StockSearchFilterStore : Store<StockSearchFilter>
    {

        public StockSearchFilterStore() : base(
            reducer: Reduce,
            initialState: StockSearchFilter.ALL)
        { }

        private static StockSearchFilter Reduce(StockSearchFilter state, IAction action)
        {
            switch (action)
            {
                case Actions.Set set:
                    return set.StockSearchFilter;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(StockSearchFilter)}.");
            }
        }

        private static class Actions
        {
            public class Set : IAction
            {
                public StockSearchFilter StockSearchFilter { get; }
                public Set(StockSearchFilter stockSearchFilter)
                {
                    StockSearchFilter = stockSearchFilter;
                }
            }
        }

        public IAction Set(StockSearchFilter stockSearchFilter)
        {
            return Dispatch(new Actions.Set(stockSearchFilter));
        }
    }
}
