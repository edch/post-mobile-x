﻿using POST.DataSources.DataFeedMessages;
using POST.Extensions;
using POST.Models;
using Redux;
using System;
using System.Collections.Generic;

namespace POST.Stores
{

    public class CurrencyByCodeStore : Store<IReadOnlyDictionary<string, Currency>>
    {

        public CurrencyByCodeStore() : base(
            reducer: Reduce,
            initialState: new Dictionary<string, Currency>())
        { }

        private static IReadOnlyDictionary<string, Currency> Reduce(IReadOnlyDictionary<string, Currency> state, IAction action)
        {
            switch (action)
            {
                case CurrencyMessage currencyMessage:
                    return state.CloneThenAddOrUpdate(
                        key: currencyMessage.Code,
                        addIfNotExist: new Currency(
                            code: currencyMessage.Code,
                            type: currencyMessage.Type,
                            bid: currencyMessage.Bid,
                            ask: currencyMessage.Ask,
                            last: currencyMessage.Last,
                            date: DateTime.Parse(currencyMessage.Date)
                        ),
                        updateIfExist: existingCurrency => existingCurrency.UpdateWith(
                            bid: existingCurrency.Bid,
                            ask: existingCurrency.Ask,
                            last: existingCurrency.Last,
                            date: existingCurrency.Date
                        )
                    );
                case Actions.UpdateQuotes updateQuotesAction:
                    Dictionary<string, Currency> clone = new Dictionary<string, Currency>(state);
                    foreach (CurrencyMessage currencyMessage in updateQuotesAction.CurrencyMessages)
                    {
                        if (clone.TryGetValue(currencyMessage.Code, out Currency existingCurrency))
                        {
                            existingCurrency.UpdateWith(
                                bid: currencyMessage.Bid,
                                ask: currencyMessage.Ask,
                                last: currencyMessage.Last,
                                date: DateTime.Parse(currencyMessage.Date)
                            );
                        }
                        else
                        {
                            clone.Add(currencyMessage.Code, new Currency(
                                code: currencyMessage.Code,
                                type: currencyMessage.Type,
                                bid: currencyMessage.Bid,
                                ask: currencyMessage.Ask,
                                last: currencyMessage.Last,
                                date: DateTime.Parse(currencyMessage.Date)
                            ));
                        }
                    }
                    return clone;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(CurrencyByCodeStore)}.");
            }
        }

        private static class Actions
        {
            public class UpdateQuotes : IAction
            {
                public IReadOnlyCollection<CurrencyMessage> CurrencyMessages { get; }
                public UpdateQuotes(IReadOnlyCollection<CurrencyMessage> currencyMessages)
                {
                    CurrencyMessages = currencyMessages ?? throw new ArgumentNullException(nameof(currencyMessages));
                }
            }
        }

        public IAction UpdateQuotes(IReadOnlyCollection<CurrencyMessage> currencyMessages)
        {
            return Dispatch(new Actions.UpdateQuotes(currencyMessages));
        }
    }
}
