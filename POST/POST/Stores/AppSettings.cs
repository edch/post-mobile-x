﻿using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using POST.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace POST.Stores
{

    public static class AppSettings
    {

        private const string LOGIN_FORM = "LOGIN_FORM";
        private const string FINGERPRINT_USE = "FINGERPRINT_USE";
        private const string BROKER_ACCOUNT_ID = "BROKER_ACCOUNT_ID";
        private const string WATCHED_STOCK_CODES = "WATCHED_STOCK_CODES";
        private const string DEFAULTRUNNINGTRADE_FILTER_STOCK_CODES = "DEFAULTRUNNINGTRADE_FILTER_STOCK_CODES";
        private const string CUSTOMRUNNINGTRADE_FILTER_STOCK_CODES = "CUSTOMRUNNINGTRADE_FILTER_STOCK_CODES";
        private const string RUNNINGTRADE_FILTER_GROUP_SELECTED = "RUNNINGTRADE_FILTER_GROUP_SELECTED";
        private const string WATCH_LIST_SORT_KEY = "WATCH_LIST_SORT_KEY";
        private const string WATCH_LIST_VIEW_MODE = "WATCH_LIST_VIEW_MODE";
        private const string MARKET_MOVERS_VIEW_MODE = "MARKET_MOVERS_VIEW_MODE";
        private const string REGIONAL_INDICES_SORT_KEY = "REGIONAL_INDICES_SORT_KEY";
        private const string SAVED_NEWS_COLLECTION = "SAVED_NEWS_COLLECTION";
        private const string CHART_MODE = "CHART_MODE";
        private const string MARKET_MOVER_CATEGORY = "MARKET_MOVER_CATEGORY";
        private const string MARKET_MOVER_MARKET_TYPE = "MARKET_MOVER_MARKET_TYPE";
        private const string BROKER_QUOTE_SORT_KEY = "BROKER_QUOTE_SORT_KEY";
        private const string BROKER_QUOTE_CATEGORY = "BROKER_QUOTE_CATEGORY";
        private const string BROKER_QUOTE_MARKET_TYPE = "BROKER_QUOTE_MARKET_TYPE";
        private const string CORPORATE_ACTION_TYPE = "CORPORATE_ACTION_TYPE";
        private const string RECENT_STOCK_CODES = "RECENT_STOCK_CODES";

        private const string ENCRYPTED = "ENCRYPTED";

        private const string ALERT_ADDR_1 = "ALERT_ADDR_1";
        private const string ATHENA_STOCKINIT_ADDR_1 = "ATHENA_STOCKINIT_ADDR_1";
        private const string COMPANYPROFILE_ADDR_1 = "COMPANYPROFILE_ADDR_1";
        private const string FEEDBACK_ADDR_1 = "FEEDBACK_ADDR_1";
        private const string FINANCIAL_ADDR_1 = "FINANCIAL_ADDR_1";
        private const string INTRADAYCOMPOSITECHART_ADDR_1 = "INTRADAYCOMPOSITECHART_ADDR_1";
        private const string INTRADAYSPARKLINE_ADDR_1 = "INTRADAYSPARKLINE_ADDR_1";
        private const string NEWS_ADDR_1 = "NEWS_ADDR_1";
        private const string SPARKLINE_ADDR_1 = "SPARKLINE_ADDR_1";

        private const string LAYOUTHELP_MARKETMOVERS = "LAYOUTHELP_MARKETMOVERS";
        private const string LAYOUTHELP_ORDERBOOK = "LAYOUTHELP_ORDERBOOK";
        private const string LAYOUTHELP_COMPLETEBOOK = "LAYOUTHELP_COMPLETEBOOK";

        private const string SHOWADDEDCONFIRMATIONDIALOG = "SHOWADDEDCONFIRMATIONDIALOG";

        //private const string DATAFEED_SERVER_LIVE = "DATAFEED_SERVER_LIVE";
        //private const string DATAFEED_PORT_LIVE = "DATAFEED_PORT_LIVE";

        //private const string DATAFEED_SERVER_MOCK = "DATAFEED_SERVER_MOCK";
        //private const string DATAFEED_PORT_MOCK = "DATAFEED_PORT_MOCK";

        //private const string TRADING_SERVER_MAIN = "TRADING_SERVER_MAIN";

        //private const string TRADING_SERVER_BACKUP = "TRADING_SERVER_BACKUP";

        private const string DATAFEED_SERVER_SELECTED = "DATAFEED_SERVER_SELECTED";
        private const string TRADING_SERVER_SELECTED = "TRADING_SERVER_SELECTED";


#if __IOS__
		public const string SourceId = "13";
#elif __ANDROID__
        public const string SourceId = "12";
#else
        public const string SourceId { get; set; }
#endif

        public static Color[] SectorColor => new Color[]
        {
            Color.FromArgb(0xFF,0xFF,0xFF), //0
            Color.FromArgb(0XFA,0XFE,0xFB), //1
            Color.FromArgb(0xB7,0xB7,0xB7), //2
            Color.FromArgb(0xB7,0xB7,0xFC), //3
            Color.FromArgb(0xFA,0xB7,0xFC), //4
            Color.FromArgb(0x9B,0xFA,0xFD), //5
            Color.FromArgb(0xC7,0xC8,0xC8), //6
            Color.FromArgb(0x82,0xFB,0x8D), //7
            Color.FromArgb(0xFC,0xAE,0xA3), //8
            Color.FromArgb(0x33,0x99,0xFF)  //9
        };

        public static Color[] BrokerTypeColor => new Color[]
        {
            Color.FromArgb(0xFF,0xFF,0xFF), //0
            Color.FromArgb(0X00,0XFF,0x7F), //1
            Color.FromArgb(0x00,0xFF,0x7F), //2
            Color.FromArgb(0xFF,0x00,0xFF), //3            
        };

        private static List<StockId> DefaultWatchedStockIds => new List<StockId> {
            new StockId("ADHI", MarketType.RG),
            new StockId("ADRO", MarketType.RG),
            new StockId("ASII", MarketType.RG),
            new StockId("BBCA", MarketType.RG),
            new StockId("BMRI", MarketType.RG),
            new StockId("BBNI", MarketType.RG),
            new StockId("BBRI", MarketType.RG),
            new StockId("GGRM", MarketType.RG),
            new StockId("CTRA", MarketType.RG)
        };

        private static readonly Lazy<string> SerializedDefaultWatchedStockIds = new Lazy<string>(() => JsonConvert.SerializeObject(DefaultWatchedStockIds
            .Select(stockId => (stockId.Code, stockId.MarketType))
            .ToList())
        );

        private static readonly Lazy<string> SerializedRunningTradeFilterStockIds = new Lazy<string>(() => JsonConvert.SerializeObject(
                new List<(string, string)>()
                {
                }
            )
        );

        private static readonly Lazy<string> SerializedDefaultRunningTradeFilterStockIds = new Lazy<string>(() => JsonConvert.SerializeObject(
               new List<(string, string)>() {
                    ("ALL STOCKS", "")
               }
           )
       );

        private static ISettings Settings => CrossSettings.Current;

        public static LoginForm LoginForm
        {
            get
            {
                string json = Settings.GetValueOrDefault(LOGIN_FORM, null);
                if (json == null)
                {
                    return null;
                }

                return JsonConvert.DeserializeObject<LoginForm>(json);
            }
            set
            {
                if (value != null)
                {
                    string json = JsonConvert.SerializeObject(value);
                    Settings.AddOrUpdateValue(LOGIN_FORM, json);
                }
                else
                {
                    Settings.Remove(LOGIN_FORM);
                }
            }
        }

        public static string Encrypted
        {
            get => Settings.GetValueOrDefault(ENCRYPTED, "0");
            set => Settings.AddOrUpdateValue(ENCRYPTED, value);
        }

        public static bool Fingerprint_Use
        {
            get => Settings.GetValueOrDefault(FINGERPRINT_USE, false);
            set => Settings.AddOrUpdateValue(FINGERPRINT_USE, value);
        }

        public static string BrokerAccountId
        {
            get => Settings.GetValueOrDefault(BROKER_ACCOUNT_ID, null);
            set
            {
                if (value != null)
                {
                    Settings.AddOrUpdateValue(BROKER_ACCOUNT_ID, value);
                }
                else
                {
                    Settings.Remove(BROKER_ACCOUNT_ID);
                }
            }
        }

        public static List<StockId> WatchedStockIds
        {
            get
            {
                string json = Settings.GetValueOrDefault(WATCHED_STOCK_CODES, SerializedDefaultWatchedStockIds.Value);
                return JsonConvert.DeserializeObject<List<(string Code, string MarketType)>>(json)
                    .Select(tuple => new StockId(tuple.Code, (MarketType)Enum.Parse(typeof(MarketType), tuple.MarketType)))
                    .ToList();
            }
            set
            {
                string json = JsonConvert.SerializeObject(value
                    .Select(stockId => (stockId.Code, stockId.MarketType))
                    .ToList());
                Settings.AddOrUpdateValue(WATCHED_STOCK_CODES, json);
            }
        }

        public static List<StockId> RecentStockIdsForStoreInitalState
        {
            get
            {
                if (RecentStockIds.Count != 0)
                {
                    return RecentStockIds;
                }
                else
                {
                    return new List<StockId>() { new StockId("AALI", MarketType.RG) };
                }
            }
        }

        public static List<StockId> RecentStockIds
        {
            get
            {
                string json = Settings.GetValueOrDefault(RECENT_STOCK_CODES, "[]");
                return JsonConvert.DeserializeObject<List<(string Code, string MarketType)>>(json)
                    .Select(tuple => new StockId(tuple.Code, (MarketType)Enum.Parse(typeof(MarketType), tuple.MarketType)))
                    .ToList();
            }
            set
            {
                string json = JsonConvert.SerializeObject(value
                    .Select(stockId => (stockId.Code, stockId.MarketType))
                    .ToList());
                Settings.AddOrUpdateValue(RECENT_STOCK_CODES, json);
            }
        }

        public static RunningTradeFilterStockIds DefaultRunningTradeFilterStockIds
        {
            get
            {
                RunningTradeFilterStockIds runningTradeFilterStockIds = new RunningTradeFilterStockIds();
                string json = Settings.GetValueOrDefault(DEFAULTRUNNINGTRADE_FILTER_STOCK_CODES, SerializedDefaultRunningTradeFilterStockIds.Value);

                List<(string, string)> lstRunningTradeStockId = JsonConvert.DeserializeObject<List<(string key, string stockId)>>(json)
                    .ToList();

                foreach ((string key, string stock) in lstRunningTradeStockId)
                {
                    if (!runningTradeFilterStockIds.isAnyGroup(key))
                    {
                        runningTradeFilterStockIds.AddGroup(key);
                    }
                    if (!string.IsNullOrEmpty(stock))
                    {
                        runningTradeFilterStockIds.AddStock(key, stock);
                    }
                }

                return runningTradeFilterStockIds;
            }
            set
            {
                List<(string, string)> lstRunningTrade = new List<(string, string)>();
                foreach (string key in value.Keys)
                {
                    if (value[key].Count > 0)
                    {
                        foreach (string stock in value[key])
                        {
                            lstRunningTrade.Add((key, stock));
                        }
                    }
                    else
                    {
                        lstRunningTrade.Add((key, ""));
                    }
                }

                string json = JsonConvert.SerializeObject(lstRunningTrade);
                Settings.AddOrUpdateValue(DEFAULTRUNNINGTRADE_FILTER_STOCK_CODES, json);
            }
        }

        public static RunningTradeFilterStockIds CustomRunningTradeFilterStockIds
        {
            get
            {
                RunningTradeFilterStockIds runningTradeFilterStockIds = new RunningTradeFilterStockIds();
                string json = Settings.GetValueOrDefault(CUSTOMRUNNINGTRADE_FILTER_STOCK_CODES, SerializedRunningTradeFilterStockIds.Value);

                List<(string, string)> lstRunningTradeStockId = JsonConvert.DeserializeObject<List<(string key, string stockId)>>(json)
                    .ToList();

                foreach ((string key, string stock) in lstRunningTradeStockId)
                {
                    if (!runningTradeFilterStockIds.isAnyGroup(key))
                    {
                        runningTradeFilterStockIds.AddGroup(key);
                    }
                    if (!string.IsNullOrEmpty(stock))
                    {
                        runningTradeFilterStockIds.AddStock(key, stock);
                    }
                }

                return runningTradeFilterStockIds;
            }
            set
            {
                List<(string, string)> lstRunningTrade = new List<(string, string)>();
                foreach (string key in value.Keys)
                {
                    if (value[key].Count > 0)
                    {
                        foreach (string stock in value[key])
                        {
                            lstRunningTrade.Add((key, stock));
                        }
                    }
                    else
                    {
                        lstRunningTrade.Add((key, ""));
                    }
                }

                string json = JsonConvert.SerializeObject(lstRunningTrade);
                Settings.AddOrUpdateValue(CUSTOMRUNNINGTRADE_FILTER_STOCK_CODES, json);
            }
        }

        public static string RunningtradeFilterGroupSelected
        {
            get => Settings.GetValueOrDefault(RUNNINGTRADE_FILTER_GROUP_SELECTED, "ALL STOCKS");
            set => Settings.AddOrUpdateValue(RUNNINGTRADE_FILTER_GROUP_SELECTED, value);
        }

        public static SortKey WatchListSortKey
        {
            get => (SortKey)Settings.GetValueOrDefault(WATCH_LIST_SORT_KEY, (int)SortKey.CodeAsc);
            set => Settings.AddOrUpdateValue(WATCH_LIST_SORT_KEY, (int)value);
        }

        public static ViewMode WatchListViewMode
        {
            get => (ViewMode)Settings.GetValueOrDefault(WATCH_LIST_VIEW_MODE, (int)ViewMode.Advanced);
            set => Settings.AddOrUpdateValue(WATCH_LIST_VIEW_MODE, (int)value);
        }

        public static ViewMode MarketMoversViewMode
        {
            get => (ViewMode)Settings.GetValueOrDefault(MARKET_MOVERS_VIEW_MODE, (int)ViewMode.Simple);
            set => Settings.AddOrUpdateValue(MARKET_MOVERS_VIEW_MODE, (int)value);
        }

        public static SortKey RegionalIndicesSortKey
        {
            get => (SortKey)Settings.GetValueOrDefault(REGIONAL_INDICES_SORT_KEY, (int)SortKey.CodeAsc);
            set => Settings.AddOrUpdateValue(REGIONAL_INDICES_SORT_KEY, (int)value);
        }

        public static ChartMode ChartMode
        {
            get => (ChartMode)Settings.GetValueOrDefault(CHART_MODE, (int)ChartMode.Line);
            set => Settings.AddOrUpdateValue(CHART_MODE, (int)value);
        }

        public static IReadOnlyList<object> SavedNewsCollection
        {
            get
            {
                string json = Settings.GetValueOrDefault(SAVED_NEWS_COLLECTION, "[]");
                return JsonConvert.DeserializeObject<List<object>>(json, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                });
            }
            set
            {
                string json = JsonConvert.SerializeObject(value, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                });
                Settings.AddOrUpdateValue(SAVED_NEWS_COLLECTION, json);
            }
        }

        public static MarketMoversCategory MarketMoversCategory
        {
            get => (MarketMoversCategory)Settings.GetValueOrDefault(MARKET_MOVER_CATEGORY, (int)MarketMoversCategory.TopGainers);
            set => Settings.AddOrUpdateValue(MARKET_MOVER_CATEGORY, (int)value);
        }

        public static MarketType MarketMoversMarketType
        {
            get => (MarketType)Settings.GetValueOrDefault(MARKET_MOVER_MARKET_TYPE, (int)MarketType.RG);
            set => Settings.AddOrUpdateValue(MARKET_MOVER_MARKET_TYPE, (int)value);
        }

        public static BrokerQuoteSortKey BrokerQuoteSortKey
        {
            get => (BrokerQuoteSortKey)Settings.GetValueOrDefault(BROKER_QUOTE_SORT_KEY, (int)BrokerQuoteSortKey.ValDesc);
            set => Settings.AddOrUpdateValue(BROKER_QUOTE_SORT_KEY, (int)value);
        }

        public static BrokerQuoteCategory BrokerQuoteCategory
        {
            get => (BrokerQuoteCategory)Settings.GetValueOrDefault(BROKER_QUOTE_CATEGORY, (int)BrokerQuoteCategory.Total);
            set => Settings.AddOrUpdateValue(BROKER_QUOTE_CATEGORY, (int)value);
        }

        public static BrokerQuoteMarketType BrokerQuoteMarketType
        {
            get => (BrokerQuoteMarketType)Settings.GetValueOrDefault(BROKER_QUOTE_MARKET_TYPE, (int)BrokerQuoteMarketType.ALL);
            set => Settings.AddOrUpdateValue(BROKER_QUOTE_MARKET_TYPE, (int)value);
        }

        public static CorporateActionType CorporateActionType
        {
            get => (CorporateActionType)Settings.GetValueOrDefault(CORPORATE_ACTION_TYPE, (int)CorporateActionType.CASHDIVIDEND);
            set => Settings.AddOrUpdateValue(CORPORATE_ACTION_TYPE, (int)value);
        }

        public static string AlertAddress_1
        {
            get => Settings.GetValueOrDefault(ALERT_ADDR_1, "http://172.31.2.92:8081/p4");
            set => Settings.AddOrUpdateValue(ALERT_ADDR_1, value);
        }

        public static string AthenaStockInitAddress_1
        {
            get => Settings.GetValueOrDefault(ATHENA_STOCKINIT_ADDR_1, "http://trx.lc.pansolt.co.id/mi2/marketInfoData?request=stockInit");
            set => Settings.AddOrUpdateValue(ATHENA_STOCKINIT_ADDR_1, value);
        }

        public static string CompanyProfileAddress_1
        {
            get => Settings.GetValueOrDefault(COMPANYPROFILE_ADDR_1, "https://chart.post-pro.co.id/companyprofile");
            set => Settings.AddOrUpdateValue(COMPANYPROFILE_ADDR_1, value);
        }

        public static string FeedbackAddress_1
        {
            get => Settings.GetValueOrDefault(FEEDBACK_ADDR_1, "https://chart.post-pro.co.id/mobilex/v1/util/feedback");
            set => Settings.AddOrUpdateValue(FEEDBACK_ADDR_1, value);
        }

        public static string FinancialAddress_1
        {
            get => Settings.GetValueOrDefault(FINANCIAL_ADDR_1, "https://chart.post-pro.co.id/api/v1/finance/newFR");
            set => Settings.AddOrUpdateValue(FINANCIAL_ADDR_1, value);
        }

        public static string IntradayCompositeChartAddress_1
        {
            get => Settings.GetValueOrDefault(INTRADAYCOMPOSITECHART_ADDR_1, "https://chart.post-pro.co.id/api/v1/chart/mobile/1min/COMPOSITE");
            set => Settings.AddOrUpdateValue(INTRADAYCOMPOSITECHART_ADDR_1, value);
        }

        public static string IntradaySparklineAddress_1
        {
            get => Settings.GetValueOrDefault(INTRADAYSPARKLINE_ADDR_1, "https://chart.post-pro.co.id/mobilex/v1/chart/spark10min");
            set => Settings.AddOrUpdateValue(INTRADAYSPARKLINE_ADDR_1, value);
        }

        public static string NewsAddress_1
        {
            get => Settings.GetValueOrDefault(NEWS_ADDR_1, "http://chart.post-pro.co.id:8080/api/v1/news");
            set => Settings.AddOrUpdateValue(NEWS_ADDR_1, value);
        }

        public static string SparklineAddress_1
        {
            get => Settings.GetValueOrDefault(SPARKLINE_ADDR_1, "http://202.78.206.115:8080/chart/sparkline");
            set => Settings.AddOrUpdateValue(SPARKLINE_ADDR_1, value);
        }

        public static string LayoutHelp_MarketMovers
        {
            get => Settings.GetValueOrDefault(LAYOUTHELP_MARKETMOVERS, "1");
            set
            {
                if (value != null)
                {
                    Settings.AddOrUpdateValue(LAYOUTHELP_MARKETMOVERS, value);
                }
                else
                {
                    Settings.Remove(LAYOUTHELP_MARKETMOVERS);
                }
            }
        }

        public static string LayoutHelp_OrderBook
        {
            get => Settings.GetValueOrDefault(LAYOUTHELP_ORDERBOOK, "1");
            set
            {
                if (value != null)
                {
                    Settings.AddOrUpdateValue(LAYOUTHELP_ORDERBOOK, value);
                }
                else
                {
                    Settings.Remove(LAYOUTHELP_ORDERBOOK);
                }
            }
        }

        public static string LayoutHelp_CompleteBook
        {
            get => Settings.GetValueOrDefault(LAYOUTHELP_COMPLETEBOOK, "0");
            set
            {
                if (value != null)
                {
                    Settings.AddOrUpdateValue(LAYOUTHELP_COMPLETEBOOK, value);
                }
                else
                {
                    Settings.Remove(LAYOUTHELP_COMPLETEBOOK);
                }
            }
        }

        public static string ShowAddedConfirmationDialog
        {
            get => Settings.GetValueOrDefault(SHOWADDEDCONFIRMATIONDIALOG, "1");
            set
            {
                if (value != null)
                {
                    Settings.AddOrUpdateValue(SHOWADDEDCONFIRMATIONDIALOG, value);
                }
                else
                {
                    Settings.Remove(SHOWADDEDCONFIRMATIONDIALOG);
                }
            }
        }

        //public static string DatafeedServer_Live
        //{
        //    get => Settings.GetValueOrDefault(DATAFEED_SERVER_LIVE, "df.lc.pansolt.co.id");
        //    set => Settings.AddOrUpdateValue(DATAFEED_SERVER_LIVE, value);
        //}

        //public static int DatafeedPort_Live
        //{
        //    get => Settings.GetValueOrDefault(DATAFEED_PORT_LIVE, 1235);
        //    set => Settings.AddOrUpdateValue(DATAFEED_PORT_LIVE, value);
        //}

        //public static string DatafeedServer_Mock
        //{
        //    get => Settings.GetValueOrDefault(DATAFEED_SERVER_MOCK, "orca.post-pro.co.id");
        //    set => Settings.AddOrUpdateValue(DATAFEED_SERVER_MOCK, value);
        //}

        //public static int DatafeedPort_Mock
        //{
        //    get => Settings.GetValueOrDefault(DATAFEED_PORT_MOCK, 1255);
        //    set => Settings.AddOrUpdateValue(DATAFEED_PORT_MOCK, value);
        //}

        //public static string TradingServer_Main
        //{
        //    get => Settings.GetValueOrDefault(TRADING_SERVER_MAIN, "http://trx.lc.pansolt.co.id/mi2");
        //    set => Settings.AddOrUpdateValue(TRADING_SERVER_MAIN, value);
        //}
        //public static string TradingServer_Backup
        //{
        //    get => Settings.GetValueOrDefault(TRADING_SERVER_BACKUP, "http://athena2.post-pro.co.id/mi2");
        //    set => Settings.AddOrUpdateValue(TRADING_SERVER_BACKUP, value);
        //}

        public static int DatafeedServer_Selected
        {
            get => Settings.GetValueOrDefault(DATAFEED_SERVER_SELECTED, (int)DatafeedServerType.Live);
            set => Settings.AddOrUpdateValue(DATAFEED_SERVER_SELECTED, value);
        }

        public static int TradingServer_Selected
        {
            get => Settings.GetValueOrDefault(TRADING_SERVER_SELECTED, (int)TradingServerType.Main);
            set => Settings.AddOrUpdateValue(TRADING_SERVER_SELECTED, value);
        }

    }
}
