﻿using POST.DataSources.DataFeedMessages;
using POST.Models;
using Redux;
using System;
using System.Linq;

namespace POST.Stores
{

    /// <summary>
    /// Note: StockId is a tuple of (StockCode, MarketType)
    /// </summary>
    public class OrderTrackingStore : Store<OrderTrackingList>
    {

        public OrderTrackingStore() : base(
            reducer: Reduce,
            initialState: new OrderTrackingList("", "", 0)
        )
        { }

        private static OrderTrackingList Reduce(OrderTrackingList state, IAction action)
        {
            switch (action)
            {
                case OrderTrackingMessage _orderTrackingMessage:
                    if (state.OrderTrackingCode == _orderTrackingMessage.OrderTrackingCode)
                    {
                        if (state.Any(ot => ot.OrderNo == _orderTrackingMessage.OrderNo))
                        {
                            state.Remove(state.Single(ot => ot.OrderNo == _orderTrackingMessage.OrderNo));
                            state.Add(new OrderTracking(_orderTrackingMessage.OrderTrackingCode, _orderTrackingMessage.OrderNo, _orderTrackingMessage.Time, _orderTrackingMessage.Status, _orderTrackingMessage.Volume, _orderTrackingMessage.Remaining));
                        }
                        else
                        {
                            state.Add(new OrderTracking(_orderTrackingMessage.OrderTrackingCode, _orderTrackingMessage.OrderNo, _orderTrackingMessage.Time, _orderTrackingMessage.Status, _orderTrackingMessage.Volume, _orderTrackingMessage.Remaining));
                        }
                    }
                    return state;
                case Actions.ClearOrderTracking ClearOrderTracking:
                    return new OrderTrackingList(ClearOrderTracking.OrderTrackingCode, ClearOrderTracking.JsxId, ClearOrderTracking.SharesPerlot);
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(WatchedStockIdsStore)}.");
            }
        }

        private static class Actions
        {
            public class AddOrderTracking : IAction
            {
                public OrderTracking OrderTracking { get; }
                public AddOrderTracking(OrderTracking orderTracking)
                {
                    OrderTracking = orderTracking;
                }
            }
            public class RemoveOrderTracking : IAction
            {
                public OrderTracking OrderTracking { get; }
                public RemoveOrderTracking(OrderTracking orderTracking)
                {
                    OrderTracking = orderTracking;
                }
            }
            public class ClearOrderTracking : IAction
            {
                public string OrderTrackingCode;
                public string JsxId;
                public long SharesPerlot;

                public ClearOrderTracking(string orderTrackingCode, string jsxId, long sharesPerlot)
                {
                    OrderTrackingCode = orderTrackingCode;
                    JsxId = jsxId;
                    SharesPerlot = sharesPerlot;
                }
            }
        }

        public IAction Clear(string orderTrackingCode, string jsxId, long sharesPerlot)
        {
            return Dispatch(new Actions.ClearOrderTracking(orderTrackingCode, jsxId, sharesPerlot));
        }
    }
}
