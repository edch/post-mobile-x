﻿using Redux;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace POST.Stores
{

    public class ThrottledDispatcher<TState, TAction>
        where TAction : IAction
    {

        public IStore<TState> Store { get; }
        private readonly Func<IList<TAction>, IAction> _actionDispatcher;
        private readonly IList<TAction> _buffer = new List<TAction>();
        private DateTime? _lastFlush;
        private DateTime? _lastAppend;

        public TimeSpan CoalesceInterval { get; }
        public TimeSpan FlushInterval { get; }
        private readonly TimeSpan _coalesceTolerance;
        private Task DelayTask;

        public ThrottledDispatcher(
            IStore<TState> store,
            Func<IList<TAction>, IAction> actionDispatcher,
            TimeSpan coalesceInterval,
            TimeSpan flushInterval)
        {
            Store = store ?? throw new ArgumentNullException(nameof(store));
            _actionDispatcher = actionDispatcher ?? throw new ArgumentNullException(nameof(actionDispatcher));
            CoalesceInterval = coalesceInterval;
            FlushInterval = flushInterval;
            _coalesceTolerance = new TimeSpan(coalesceInterval.Ticks / 2);
        }

        public async void Dispatch(TAction action)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            DateTime now = DateTime.Now;
            if (!_lastAppend.HasValue)
            {
                _buffer.Add(action);
                _lastAppend = now;
                _lastFlush = now;
                // proceed to delayed flush
            }
            else if (now.Subtract(_lastAppend.Value) < CoalesceInterval && now.Subtract(_lastFlush.Value) < FlushInterval)
            {
                _buffer.Add(action);
                _lastAppend = now;
                return;
            }
            else
            {
                _buffer.Add(action);
                IAction dispatchedAction = _actionDispatcher(_buffer) ?? throw new ArgumentNullException(nameof(dispatchedAction));
                _lastAppend = null;
                _lastFlush = now;
                return;
            }
            // delayed flush
            if (DelayTask == null && _lastAppend.HasValue && (now == _lastAppend.Value || now.Subtract(_lastAppend.Value) >= _coalesceTolerance))
            {
                DelayTask = Task.Delay(FlushInterval);
                await DelayTask;
                DelayTask = null;
                if (_lastAppend.HasValue && DateTime.Now.Subtract(_lastAppend.Value).Add(_coalesceTolerance) >= CoalesceInterval)
                {
                    IAction dispatchedAction = _actionDispatcher(_buffer) ?? throw new ArgumentNullException(nameof(dispatchedAction));
                    _lastAppend = null;
                    _lastFlush = now;
                }
            }
        }
    }
}
