﻿using POST.DataSources.DataFeedMessages;
using POST.Models;
using Redux;
using System;
using System.Collections.Generic;

namespace POST.Stores
{

    public class StockSummaryStore : Store<StockSummary>
    {

        public StockSummaryStore() : base(
            reducer: Reduce,
            initialState: new StockSummary(new StockId("", MarketType.RG)))
        { }

        private static StockSummary Reduce(StockSummary state, IAction action)
        {
            switch (action)
            {
                case Actions.BatchUpdate batchUpdateAction:
                    foreach (StockSummaryMessage stockSummaryMessage in batchUpdateAction.Messages)
                    {
                        if (stockSummaryMessage.StockCode != state.StockId.Code)
                        {
                            continue;
                        }

                        state.UpdateLevel(
                            stockSummaryMessage.BrokerCode,
                            stockSummaryMessage.D_Buy_Value,
                            stockSummaryMessage.D_Buy_Volume,
                            stockSummaryMessage.D_Buy_Frequency,
                            stockSummaryMessage.D_Sell_Value,
                            stockSummaryMessage.D_Sell_Volume,
                            stockSummaryMessage.D_Sell_Frequency,
                            stockSummaryMessage.F_Buy_Value,
                            stockSummaryMessage.F_Buy_Volume,
                            stockSummaryMessage.F_Buy_Frequency,
                            stockSummaryMessage.F_Sell_Value,
                            stockSummaryMessage.F_Sell_Volume,
                            stockSummaryMessage.F_Sell_Frequency
                            );
                    }
                    return state;
                case Actions.Clear clearAction:
                    return new StockSummary(clearAction.StockId);
                case StockSummaryMessage stockSummaryMessage:
                    if (stockSummaryMessage.StockCode == state.StockId.Code)
                    {
                        state.UpdateLevel(
                             stockSummaryMessage.BrokerCode,
                             stockSummaryMessage.D_Buy_Value,
                             stockSummaryMessage.D_Buy_Volume,
                             stockSummaryMessage.D_Buy_Frequency,
                             stockSummaryMessage.D_Sell_Value,
                             stockSummaryMessage.D_Sell_Volume,
                             stockSummaryMessage.D_Sell_Frequency,
                             stockSummaryMessage.F_Buy_Value,
                             stockSummaryMessage.F_Buy_Volume,
                             stockSummaryMessage.F_Buy_Frequency,
                             stockSummaryMessage.F_Sell_Value,
                             stockSummaryMessage.F_Sell_Volume,
                             stockSummaryMessage.F_Sell_Frequency
                             );
                    }
                    return state;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(OrderBookStore)}.");
            }
        }

        private static class Actions
        {
            public class BatchUpdate : IAction
            {
                public IList<StockSummaryMessage> Messages { get; }
                public BatchUpdate(IList<StockSummaryMessage> messages)
                {
                    Messages = messages ?? throw new ArgumentNullException(nameof(messages));
                }
            }

            public class SetPrevious : IAction
            {
                public decimal Previous { get; }
                public SetPrevious(decimal previous)
                {
                    Previous = previous;
                }
            }
            public class Clear : IAction
            {
                public StockId StockId { get; }
                public Clear(StockId stockId)
                {
                    StockId = stockId;
                }
            }
        }

        public IAction BatchUpdate(IList<StockSummaryMessage> stockSummaryMessage)
        {
            return Dispatch(new Actions.BatchUpdate(stockSummaryMessage));
        }

        public IAction SetPrevious(decimal previous)
        {
            return Dispatch(new Actions.SetPrevious(previous));
        }

        public IAction Clear(StockId stockId)
        {
            return Dispatch(new Actions.Clear(stockId));
        }
    }
}
