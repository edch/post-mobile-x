﻿using POST.DataSources.AthenaMessages.Responses;
using Redux;
using System;
using System.Collections.Generic;

namespace POST.Stores
{
    public class TradingAccountsStore : Store<IReadOnlyList<TradingAccount>>
    {
        public bool IsSyariahAcc { get; set; }

        public TradingAccountsStore() : base(
            reducer: Reduce,
            initialState: null
        )
        { }

        private static IReadOnlyList<TradingAccount> Reduce(IReadOnlyList<TradingAccount> state, IAction action)
        {
            switch (action)
            {
                case Actions.SetTradingAccounts setTradingAccountsAction:
                    {
                        return setTradingAccountsAction.TradingAccounts;
                    }
                case Actions.Clear clearAction:
                    return null;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(TradingAccountsStore)}.");
            }
        }

        private static class Actions
        {
            public class SetTradingAccounts : IAction
            {
                public IReadOnlyList<TradingAccount> TradingAccounts { get; }
                public SetTradingAccounts(IReadOnlyList<TradingAccount> tradingAccounts)
                {
                    TradingAccounts = tradingAccounts ?? throw new ArgumentNullException(nameof(tradingAccounts));
                }
            }

            public class Clear : IAction { }
        }

        public IAction SetTradingAccounts(IReadOnlyList<TradingAccount> tradingAccounts)
        {
            return Dispatch(new Actions.SetTradingAccounts(tradingAccounts));
        }

        public IAction Clear()
        {
            return Dispatch(new Actions.Clear());
        }
    }
}
