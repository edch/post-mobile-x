﻿using POST.DataSources.ApiResponses;
using POST.Models;
using Redux;
using System;

namespace POST.Stores
{
    public class CompositeChartStore : Store<IndexChart>
    {
        public CompositeChartStore() : base(
            reducer: Reduce,
            initialState: null)
        { }

        private static IndexChart Reduce(IndexChart state, IAction action)
        {
            switch (action)
            {
                case Actions.Set setAction:
                    return setAction.IndexChart;
                case Actions.UpdateWithIntradayIndexChartData updateWithIntradayIndexChartDataAction:
                    if (state == null)
                    {
                        state = new IndexChart("COMPOSITE", IndexChartTimeUnit.Minute);
                    }
                    state.UpdateWithIntradayIndexChartData(updateWithIntradayIndexChartDataAction.Data);
                    return state;
                case Actions.Clear clearAction:
                    return null;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(CompositeChartStore)}.");
            }
        }

        private static class Actions
        {
            public class Set : IAction
            {
                public IndexChart IndexChart { get; }
                public Set(IndexChart indexChart)
                {
                    IndexChart = indexChart ?? throw new ArgumentNullException(nameof(indexChart));
                }
            }
            public class UpdateWithIntradayIndexChartData : IAction
            {
                public IntradayIndexChartData[] Data { get; }
                public UpdateWithIntradayIndexChartData(IntradayIndexChartData[] data)
                {
                    Data = data ?? throw new ArgumentNullException(nameof(data));
                }
            }
            public class Clear : IAction { }
        }

        public IAction Set(IndexChart indexChart)
        {
            return Dispatch(new Actions.Set(indexChart));
        }

        public IAction UpdateWithIntradayIndexChartData(IntradayIndexChartData[] data)
        {
            return Dispatch(new Actions.UpdateWithIntradayIndexChartData(data));
        }

        public IAction Clear()
        {
            return Dispatch(new Actions.Clear());
        }
    }
}
