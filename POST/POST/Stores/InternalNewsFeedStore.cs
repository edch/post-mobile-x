﻿using POST.DataSources;
using POST.DataSources.ApiResponses;
using POST.Extensions;
using Redux;
using System;
using System.Collections.Generic;
using System.Linq;

namespace POST.Stores
{

    public class InternalNewsFeedStore : Store<IReadOnlyDictionary<InternalNewsCategory, IReadOnlyList<InternalNews>>>
    {

        public InternalNewsFeedStore() : base(
            reducer: Reduce,
            initialState: new Dictionary<InternalNewsCategory, IReadOnlyList<InternalNews>>())
        { }

        private static IReadOnlyDictionary<InternalNewsCategory, IReadOnlyList<InternalNews>> Reduce(IReadOnlyDictionary<InternalNewsCategory, IReadOnlyList<InternalNews>> state, IAction action)
        {
            switch (action)
            {
                case Actions.UpdateNewsFeed updateNewsFeedAction:
                    return state.CloneThenSet(updateNewsFeedAction.NewsCategory, updateNewsFeedAction.NewsFeed.ToList());
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(InternalNewsFeedStore)}.");
            }
        }

        private static class Actions
        {
            public class UpdateNewsFeed : IAction
            {
                public InternalNewsCategory NewsCategory { get; }
                public ICollection<InternalNews> NewsFeed { get; }
                public UpdateNewsFeed(InternalNewsCategory newsCategory, ICollection<InternalNews> newsFeed)
                {
                    NewsCategory = newsCategory;
                    NewsFeed = newsFeed ?? throw new ArgumentNullException(nameof(newsFeed));
                }
            }
        }

        public IAction UpdateNewsFeed(InternalNewsCategory newsCategory, ICollection<InternalNews> newsFeed)
        {
            return Dispatch(new Actions.UpdateNewsFeed(newsCategory, newsFeed));
        }
    }
}
