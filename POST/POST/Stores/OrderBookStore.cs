﻿using POST.DataSources.DataFeedMessages;
using POST.Models;
using Redux;
using System;
using System.Collections.Generic;

namespace POST.Stores
{

    public class OrderBookStore : Store<OrderBook>
    {

        public OrderBookStore() : base(
            reducer: Reduce,
            initialState: new OrderBook(new StockId("", MarketType.RG)))
        { }

        private static OrderBook Reduce(OrderBook state, IAction action)
        {
            switch (action)
            {
                case Actions.BatchUpdate batchUpdateAction:
                    {
                        foreach (OrderBookMessage orderBookMessage in batchUpdateAction.OrderBookMessages)
                        {
                            try
                            {
                                if (orderBookMessage.StockCode != state.StockId.Code)
                                {
                                    continue;
                                }

                                if (orderBookMessage.MarketType != state.StockId.MarketType)
                                {
                                    continue;
                                }

                                if (orderBookMessage.Price <= 0)
                                {
                                    continue;
                                }

                                if (orderBookMessage.Volume < 0)
                                {
                                    continue;
                                }

                                if (orderBookMessage.Frequency < 0)
                                {
                                    continue;
                                }

                                switch (orderBookMessage.OrderBookType)
                                {
                                    case OrderType.Bid:
                                        state.UpdateBidLevel(orderBookMessage.Price, orderBookMessage.Volume, orderBookMessage.Frequency, orderBookMessage.BrokerCode);
                                        break;
                                    case OrderType.Offer:
                                        state.UpdateAskLevel(orderBookMessage.Price, orderBookMessage.Volume, orderBookMessage.Frequency, orderBookMessage.BrokerCode);
                                        break;
                                }
                            }
                            catch
                            {

                            }
                        }
                        return state;
                    }
                case Actions.SetPrevious setPreviousAction:
                    state.Previous = setPreviousAction.Previous;
                    return state;
                case Actions.SetLastPrice setLastPriceAction:
                    state.LastPrice = setLastPriceAction.LastPrice;
                    return state;
                case Actions.Clear clearAction:
                    return new OrderBook(clearAction.StockId);
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(OrderBookStore)}.");
            }
        }

        private static class Actions
        {
            public class BatchUpdate : IAction
            {
                public IList<OrderBookMessage> OrderBookMessages { get; }
                public BatchUpdate(IList<OrderBookMessage> messages)
                {
                    OrderBookMessages = messages;
                }
            }

            public class SetLastPrice : IAction
            {
                public decimal LastPrice { get; }
                public SetLastPrice(decimal lastPrice)
                {
                    LastPrice = lastPrice;
                }
            }
            public class SetPrevious : IAction
            {
                public decimal Previous { get; }
                public SetPrevious(decimal previous)
                {
                    Previous = previous;
                }
            }
            public class Clear : IAction
            {
                public StockId StockId { get; }
                public Clear(StockId stockId)
                {
                    StockId = stockId;
                }
            }
        }

        public IAction BatchUpdate(IList<OrderBookMessage> orderBookMessages)
        {
            return Dispatch(new Actions.BatchUpdate(orderBookMessages));
        }

        public IAction SetPrevious(decimal previous)
        {
            return Dispatch(new Actions.SetPrevious(previous));
        }

        public IAction SetLastPrice(decimal lastPrice)
        {
            return Dispatch(new Actions.SetLastPrice(lastPrice));
        }

        public IAction Clear(StockId stockId)
        {
            return Dispatch(new Actions.Clear(stockId));
        }
    }
}
