﻿using POST.DataSources.DataFeedMessages;
using POST.Models;
using Redux;
using System;
using System.Collections.Generic;

namespace POST.Stores
{

    public class TradeBookStore : Store<TradeBook>
    {

        public TradeBookStore() : base(
            reducer: Reduce,
            initialState: new TradeBook(new StockId("", MarketType.RG)))
        { }

        private static TradeBook Reduce(TradeBook state, IAction action)
        {
            switch (action)
            {
                case Actions.BatchUpdate batchUpdateAction:
                    foreach (TradeBookMessage tradeBookMessage in batchUpdateAction.Messages)
                    {
                        if (tradeBookMessage.StockCode != state.StockId.Code)
                        {
                            continue;
                        }

                        state.UpdateLevel(tradeBookMessage.Price, tradeBookMessage.Volume, tradeBookMessage.Frequency);
                    }
                    return state;
                case Actions.SetPrevious setPreviousAction:
                    state.Previous = setPreviousAction.Previous;
                    return state;
                case Actions.Clear clearAction:
                    return new TradeBook(clearAction.StockId);
                case TradeBookMessage tradeBookMessage:
                    if (tradeBookMessage.StockCode == state.StockId.Code)
                    {
                        state.UpdateLevel(tradeBookMessage.Price, tradeBookMessage.Volume, tradeBookMessage.Frequency);
                    }
                    return state;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(OrderBookStore)}.");
            }
        }

        private static class Actions
        {
            public class BatchUpdate : IAction
            {
                public IList<TradeBookMessage> Messages { get; }
                public BatchUpdate(IList<TradeBookMessage> messages)
                {
                    Messages = messages ?? throw new ArgumentNullException(nameof(messages));
                }
            }

            public class SetPrevious : IAction
            {
                public decimal Previous { get; }
                public SetPrevious(decimal previous)
                {
                    Previous = previous;
                }
            }
            public class Clear : IAction
            {
                public StockId StockId { get; }
                public Clear(StockId stockId)
                {
                    StockId = stockId;
                }
            }
        }

        public IAction BatchUpdate(IList<TradeBookMessage> tradeBookMessages)
        {
            return Dispatch(new Actions.BatchUpdate(tradeBookMessages));
        }

        public IAction SetPrevious(decimal previous)
        {
            return Dispatch(new Actions.SetPrevious(previous));
        }

        public IAction Clear(StockId stockId)
        {
            return Dispatch(new Actions.Clear(stockId));
        }
    }
}
