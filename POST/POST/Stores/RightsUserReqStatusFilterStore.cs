﻿using POST.Models;
using Redux;
using System;

namespace POST.Stores
{
    public class RightsUserReqStatusFilterStore : Store<string>
    {

        public RightsUserReqStatusFilterStore() : base(
            reducer: Reduce,
            initialState: RightsUserReqStatusDict.Items[0])
        { }

        private static string Reduce(string state, IAction action)
        {
            switch (action)
            {
                case Actions.Set set:
                    return set.RightsUserReqStatus;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(RightsUserReqStatusFilterStore)}.");
            }
        }

        private static class Actions
        {
            public class Set : IAction
            {
                public string RightsUserReqStatus { get; }
                public Set(string rightsUserReqStatus)
                {
                    RightsUserReqStatus = rightsUserReqStatus;
                }
            }
        }

        public IAction Set(string rightsUserReqStatus)
        {
            return Dispatch(new Actions.Set(rightsUserReqStatus));
        }
    }
}
