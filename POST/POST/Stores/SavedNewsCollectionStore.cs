﻿using POST.DataSources.ApiResponses;
using POST.Extensions;
using POST.Models;
using Redux;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms.Internals;

namespace POST.Stores
{

    public class SavedNewsCollectionStore : Store<IReadOnlyList<object>>
    {

        public SavedNewsCollectionStore() : base(
            reducer: Reduce,
            initialState: AppSettings.SavedNewsCollection)
        { }

        private static IReadOnlyList<object> Reduce(IReadOnlyList<object> state, IAction action)
        {
            switch (action)
            {
                case Actions.AddNews addNewsAction:
                    if (state.Contains(addNewsAction.News))
                    {
                        return state;
                    }
                    else
                    {
                        return AppSettings.SavedNewsCollection = state.CloneThenAdd(addNewsAction.News);
                    }
                case Actions.SetContent setContentAction:
                    if (state.FirstOrDefault(news => news is ExternalNewsSnippet snippet && snippet.Id == setContentAction.News.Id) is ExternalNewsSnippet externalNewsSnippet &&
                        state.IndexOf(externalNewsSnippet) is int i &&
                        i != -1)
                    {
                        return AppSettings.SavedNewsCollection = state.CloneThenSet(i, setContentAction.News);
                    }
                    else
                    {
                        return state;
                    }
                case Actions.RemoveNews removeNewsAction:
                    return AppSettings.SavedNewsCollection = state.CloneThenRemove(removeNewsAction.News);
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(SavedNewsCollectionStore)}.");
            }
        }

        private static class Actions
        {
            public class AddNews : IAction
            {
                public IUniqueIndexable<int> News { get; }
                public AddNews(IUniqueIndexable<int> news)
                {
                    if (news is InternalNews ||
                        news is ExternalNews ||
                        news is ExternalNewsSnippet)
                    {
                        News = news;
                    }
                    else
                    {
                        throw new ArgumentException("Unrecognized News Type", nameof(news));
                    }
                }
            }
            public class SetContent : IAction
            {
                public ExternalNews News { get; }
                public SetContent(ExternalNews news)
                {
                    News = news ?? throw new ArgumentNullException(nameof(news));
                }
            }
            public class RemoveNews : IAction
            {
                public IUniqueIndexable<int> News { get; }
                public RemoveNews(IUniqueIndexable<int> news)
                {
                    if (news is InternalNews ||
                        news is ExternalNews ||
                        news is ExternalNewsSnippet)
                    {
                        News = news;
                    }
                    else
                    {
                        throw new ArgumentException("Unrecognized News Type", nameof(news));
                    }
                }
            }
        }

        public IAction AddNews(InternalNews news)
        {
            return Dispatch(new Actions.AddNews(news));
        }

        public IAction AddNews(ExternalNews news)
        {
            return Dispatch(new Actions.AddNews(news));
        }

        public IAction AddNews(ExternalNewsSnippet news)
        {
            return Dispatch(new Actions.AddNews(news));
        }

        public IAction SetContent(ExternalNews news)
        {
            return Dispatch(new Actions.SetContent(news));
        }

        public IAction RemoveNews(InternalNews news)
        {
            return Dispatch(new Actions.RemoveNews(news));
        }

        public IAction RemoveNews(ExternalNews news)
        {
            return Dispatch(new Actions.RemoveNews(news));
        }

        public IAction RemoveNews(ExternalNewsSnippet news)
        {
            return Dispatch(new Actions.RemoveNews(news));
        }
    }
}
