﻿using POST.DataSources.DataFeedMessages;
using POST.Extensions;
using POST.Models;
using Redux;
using System;
using System.Collections.Generic;
using System.Linq;

namespace POST.Stores
{

    /// <summary>
    /// Note: BrokerId is a tuple of (BrokerCode, MarketType)
    /// </summary>
    public class BrokerByIdStore : Store<IReadOnlyDictionary<BrokerId, Broker>>
    {

        public BrokerByIdStore() : base(
            reducer: Reduce,
            initialState: new Dictionary<BrokerId, Broker>())
        { }

        private static IReadOnlyDictionary<BrokerId, Broker> Reduce(IReadOnlyDictionary<BrokerId, Broker> state, IAction action)
        {
            switch (action)
            {
                case Actions.Init initAction:
                    {
                        return initAction.BrokerInitMessages.TrySelect(
                            selector: brokerInitMessage => new Broker(
                                code: brokerInitMessage.BrokerCode,
                                marketType: brokerInitMessage.MarketType,
                                name: brokerInitMessage.BrokerName,
                                type: brokerInitMessage.BrokerType
                            )
                        ).GroupBy(
                            keySelector: broker => broker.Id
                        ).ToDictionary(
                            keySelector: group => group.Key,
                            elementSelector: brokers => brokers.Last()
                        );
                    }

                case Actions.UpdateQuotes updateQuotesAction:
                    {
                        foreach (BrokerQuoteMessage brokerQuoteMessage in updateQuotesAction.BrokerQuoteMessages)
                        {
                            BrokerId brokerId = new BrokerId(brokerQuoteMessage.BrokerCode, brokerQuoteMessage.MarketType);
                            //Debug.WriteLine("MarketType: " + brokerQuoteMessage.BrokerCode + "," + brokerQuoteMessage.MarketType.ToString() + "," + brokerQuoteMessage.BuyFreq + "," + brokerQuoteMessage.BuyVol + "," + brokerQuoteMessage.BuyVal + "," + brokerQuoteMessage.SellFreq + "," + brokerQuoteMessage.SellVol + "," + brokerQuoteMessage.SellVal);
                            if (state.TryGetValue(brokerId, out Broker existingBroker))
                            {
                                //Debug.WriteLine("MarketType: " + brokerId.Code + "," + existingBroker.Id.MarketType.ToString() + "," + brokerQuoteMessage.BuyFreq + "," + brokerQuoteMessage.BuyVol + "," + brokerQuoteMessage.BuyVal + "," + brokerQuoteMessage.SellFreq + "," + brokerQuoteMessage.SellVol + "," + brokerQuoteMessage.SellVal);
                                existingBroker.UpdateWithBrokerQuote(
                                    buyFreq: brokerQuoteMessage.BuyFreq,
                                    buyVol: brokerQuoteMessage.BuyVol,
                                    buyVal: brokerQuoteMessage.BuyVal,
                                    sellFreq: brokerQuoteMessage.SellFreq,
                                    sellVol: brokerQuoteMessage.SellVol,
                                    sellVal: brokerQuoteMessage.SellVal
                                );
                            }
                        }
                        return state;
                    }

                case BrokerQuoteMessage brokerQuoteMessage:
                    {
                        BrokerId brokerId2 = new BrokerId(brokerQuoteMessage.BrokerCode, brokerQuoteMessage.MarketType);
                        //Debug.WriteLine("MarketType: " + brokerQuoteMessage.BrokerCode + "," + brokerQuoteMessage.MarketType.ToString() + "," + brokerQuoteMessage.BuyFreq + "," + brokerQuoteMessage.BuyVol + "," + brokerQuoteMessage.BuyVal + "," + brokerQuoteMessage.SellFreq + "," + brokerQuoteMessage.SellVol + "," + brokerQuoteMessage.SellVal);
                        if (state.TryGetValue(brokerId2, out Broker existingBroker2))
                        {
                            //Debug.WriteLine("MarketType: " + brokerId.Code + "," + existingBroker.Id.MarketType.ToString() + "," + brokerQuoteMessage.BuyFreq + "," + brokerQuoteMessage.BuyVol + "," + brokerQuoteMessage.BuyVal + "," + brokerQuoteMessage.SellFreq + "," + brokerQuoteMessage.SellVol + "," + brokerQuoteMessage.SellVal);
                            existingBroker2.UpdateWithBrokerQuote(
                                buyFreq: brokerQuoteMessage.BuyFreq,
                                buyVol: brokerQuoteMessage.BuyVol,
                                buyVal: brokerQuoteMessage.BuyVal,
                                sellFreq: brokerQuoteMessage.SellFreq,
                                sellVol: brokerQuoteMessage.SellVol,
                                sellVal: brokerQuoteMessage.SellVal
                            );
                        }
                        return state;
                    }

                case BrokerInitMessage brokerInitMessage:
                    {
                        BrokerId brokerIdRG = new BrokerId(brokerInitMessage.BrokerCode, BrokerQuoteMarketType.RG);

                        if (!state.TryGetValue(brokerIdRG, out Broker existingBroker2))
                        {
                            Broker brokerRG = new Broker(
                            code: brokerInitMessage.BrokerCode,
                            marketType: BrokerQuoteMarketType.RG,
                            name: brokerInitMessage.BrokerName,
                            type: brokerInitMessage.BrokerType);

                            Broker brokerNG = new Broker(
                            code: brokerInitMessage.BrokerCode,
                            marketType: BrokerQuoteMarketType.NG,
                            name: brokerInitMessage.BrokerName,
                            type: brokerInitMessage.BrokerType);
                            BrokerId brokerIdNG = new BrokerId(brokerInitMessage.BrokerCode, BrokerQuoteMarketType.NG);


                            Broker brokerTN = new Broker(
                            code: brokerInitMessage.BrokerCode,
                            marketType: BrokerQuoteMarketType.TN,
                            name: brokerInitMessage.BrokerName,
                            type: brokerInitMessage.BrokerType);
                            BrokerId brokerIdTN = new BrokerId(brokerInitMessage.BrokerCode, BrokerQuoteMarketType.TN);

                            Broker brokerAll = new Broker(
                            code: brokerInitMessage.BrokerCode,
                            marketType: BrokerQuoteMarketType.ALL,
                            name: brokerInitMessage.BrokerName,
                            type: brokerInitMessage.BrokerType);
                            BrokerId brokerIdAll = new BrokerId(brokerInitMessage.BrokerCode, BrokerQuoteMarketType.ALL);

                            state = state.CloneThenAdd(brokerIdRG, brokerRG);
                            state = state.CloneThenAdd(brokerIdNG, brokerNG);
                            state = state.CloneThenAdd(brokerIdTN, brokerTN);
                            state = state.CloneThenAdd(brokerIdAll, brokerAll);
                        }
                        return state;
                    }

                case Actions.Clear clear:
                    {
                        return new Dictionary<BrokerId, Broker>();
                    }

                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(BrokerByIdStore)}.");
            }
        }

        private static class Actions
        {

            public class Init : IAction
            {
                public IReadOnlyCollection<BrokerInitMessage> BrokerInitMessages { get; }
                public Init(IReadOnlyCollection<BrokerInitMessage> brokerInitMessages)
                {
                    BrokerInitMessages = brokerInitMessages ?? throw new ArgumentNullException(nameof(brokerInitMessages));
                }
            }

            public class UpdateQuotes : IAction
            {
                public IReadOnlyCollection<BrokerQuoteMessage> BrokerQuoteMessages { get; }

                public UpdateQuotes(IReadOnlyCollection<BrokerQuoteMessage> brokerQuoteMessages)
                {
                    BrokerQuoteMessages = brokerQuoteMessages ?? throw new ArgumentNullException(nameof(brokerQuoteMessages));
                }
            }

            public class Clear : IAction { }
        }

        public IAction Init(IReadOnlyCollection<BrokerInitMessage> brokerInitMessages)
        {
            return Dispatch(new Actions.Init(brokerInitMessages));
        }

        public IAction UpdateQuotes(IReadOnlyCollection<BrokerQuoteMessage> brokerQuoteMessages)
        {
            return Dispatch(new Actions.UpdateQuotes(brokerQuoteMessages));
        }

        public IAction Clear()
        {
            return Dispatch(new Actions.Clear());
        }
    }
}
