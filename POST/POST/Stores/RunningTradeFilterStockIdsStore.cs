﻿using POST.Models;
using Redux;
using System;

namespace POST.Stores
{

    /// <summary>
    /// Note: StockId is a tuple of (StockCode, MarketType)
    /// </summary>
    public class RunningTradeFilterStockIdsStore : Store<RunningTradeFilterStockIds>
    {

        public RunningTradeFilterStockIdsStore(RunningTradeFilterStockIds runningTradeFilterStockIds) : base(
            reducer: Reduce,
            initialState: runningTradeFilterStockIds
        )
        { }

        private static RunningTradeFilterStockIds Reduce(RunningTradeFilterStockIds state, IAction action)
        {
            switch (action)
            {
                case Actions.AddGroup addGroupAction:
                    state.AddGroup(addGroupAction.Key);
                    AppSettings.CustomRunningTradeFilterStockIds = state;
                    return state;

                case Actions.RemoveGroup removeGroupAction:
                    state.RemoveGroup(removeGroupAction.Key);
                    if (Store.RunningTradeFilterGroupSelectedStore.GetState() == removeGroupAction.Key)
                    {
                        Store.RunningTrade.Clear();
                    }

                    AppSettings.CustomRunningTradeFilterStockIds = state;
                    return state;

                case Actions.ClearGroup clearGroupAction:
                    state.ClearGroup();
                    Store.RunningTrade.Clear();
                    AppSettings.CustomRunningTradeFilterStockIds = state;
                    return state;

                case Actions.AddStock addStockAction:
                    state.AddStock(addStockAction.Key, addStockAction.StockId);
                    if (Store.RunningTradeFilterGroupSelectedStore.GetState() == addStockAction.Key)
                    {
                        Store.RunningTrade.Clear();
                    }

                    AppSettings.CustomRunningTradeFilterStockIds = state;
                    return state;

                case Actions.RemoveStock removeStockAction:
                    state.RemoveStock(removeStockAction.Key, removeStockAction.StockId);
                    if (Store.RunningTradeFilterGroupSelectedStore.GetState() == removeStockAction.Key)
                    {
                        Store.RunningTrade.Clear();
                    }

                    AppSettings.CustomRunningTradeFilterStockIds = state;
                    return state;

                case Actions.ClearStock clearStockAction:
                    state.ClearStock(clearStockAction.Key);
                    if (Store.RunningTradeFilterGroupSelectedStore.GetState() == clearStockAction.Key)
                    {
                        Store.RunningTrade.Clear();
                    }

                    AppSettings.CustomRunningTradeFilterStockIds = state;
                    return state;

                case Actions.RenameGroup renameGroupAction:
                    state.RenameGroup(renameGroupAction.OldName, renameGroupAction.NewName);
                    if (Store.RunningTradeFilterGroupSelectedStore.GetState() == renameGroupAction.OldName)
                    {
                        Store.RunningTradeFilterGroupSelectedStore.Set(renameGroupAction.NewName);
                    }
                    AppSettings.CustomRunningTradeFilterStockIds = state;
                    return state;

                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(RunningTradeFilterStockIdsStore)}.");
            }
        }

        private static class Actions
        {
            public class AddStock : IAction
            {
                public string Key { get; }
                public string StockId { get; }
                public AddStock(string key, string stockId)
                {
                    Key = key;
                    StockId = stockId;
                }
            }

            public class RemoveStock : IAction
            {
                public string Key { get; }
                public string StockId { get; }
                public RemoveStock(string key, string stockId)
                {
                    Key = key;
                    StockId = stockId;
                }
            }

            public class ClearStock : IAction
            {
                public string Key { get; }
                public ClearStock(string key)
                {
                    Key = key;
                }
            }

            public class AddGroup : IAction
            {
                public string Key { get; }
                public AddGroup(string key)
                {
                    Key = key;
                }
            }

            public class RemoveGroup : IAction
            {
                public string Key { get; }
                public RemoveGroup(string key)
                {
                    Key = key;
                }
            }

            public class ClearGroup : IAction
            {
                public ClearGroup()
                {
                }
            }

            public class RenameGroup : IAction
            {
                public string OldName { get; }
                public string NewName { get; }
                public RenameGroup(string oldName, string newName)
                {
                    OldName = oldName;
                    NewName = newName;
                }
            }
        }

        public IAction AddStock(string key, string stockId)
        {
            return Dispatch(new Actions.AddStock(key, stockId));
        }

        public IAction RemoveStock(string key, string stockId)
        {
            return Dispatch(new Actions.RemoveStock(key, stockId));
        }

        public IAction ClearStock(string key)
        {
            return Dispatch(new Actions.ClearStock(key));
        }

        public IAction AddGroup(string key)
        {
            return Dispatch(new Actions.AddGroup(key));
        }

        public IAction RemoveGroup(string key)
        {
            return Dispatch(new Actions.RemoveGroup(key));
        }

        public IAction ClearGroup()
        {
            return Dispatch(new Actions.ClearGroup());
        }

        public IAction RenameGroup(string oldName, string newName)
        {
            return Dispatch(new Actions.RenameGroup(oldName, newName));
        }
    }
}
