﻿using POST.Models;
using Redux;
using System;

namespace POST.Stores
{

    public class RegionalIndicesSortKeyStore : Store<SortKey>
    {

        public RegionalIndicesSortKeyStore() : base(
            reducer: Reduce,
            initialState: AppSettings.RegionalIndicesSortKey)
        { }

        private static SortKey Reduce(SortKey state, IAction action)
        {
            switch (action)
            {
                case Actions.SortBy sortByAction:
                    return AppSettings.RegionalIndicesSortKey = sortByAction.SortKey;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(RegionalIndicesSortKeyStore)}.");
            }
        }

        private static class Actions
        {
            public class SortBy : IAction
            {
                public SortKey SortKey { get; }
                public SortBy(SortKey sortKey)
                {
                    SortKey = sortKey;
                }
            }
        }

        public IAction SortBy(SortKey sortKey)
        {
            return Dispatch(new Actions.SortBy(sortKey));
        }
    }
}
