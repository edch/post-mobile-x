﻿using POST.Models;
using Redux;
using System;

namespace POST.Stores
{

    public class ChartModeStore : Store<ChartMode>
    {

        public ChartModeStore() : base(
            reducer: Reduce,
            initialState: AppSettings.ChartMode)
        { }

        private static ChartMode Reduce(ChartMode state, IAction action)
        {
            switch (action)
            {
                case Actions.SetChartMode setChartModeAction:
                    return AppSettings.ChartMode = setChartModeAction.ChartMode;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(ChartModeStore)}.");
            }
        }

        private static class Actions
        {
            public class SetChartMode : IAction
            {
                public ChartMode ChartMode { get; }
                public SetChartMode(ChartMode chartMode)
                {
                    ChartMode = chartMode;
                }
            }
        }

        public IAction SetChartMode(ChartMode chartMode)
        {
            return Dispatch(new Actions.SetChartMode(chartMode));
        }
    }
}
