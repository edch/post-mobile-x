﻿using POST.DataSources.DataFeedMessages;
using POST.Extensions;
using POST.Models;
using Redux;
using System;
using System.Collections.Generic;

namespace POST.Stores
{

    public class RegionalIndexByCodeStore : Store<IReadOnlyDictionary<string, RegionalIndex>>
    {

        public RegionalIndexByCodeStore() : base(
            reducer: Reduce,
            initialState: new Dictionary<string, RegionalIndex>())
        { }

        private static IReadOnlyDictionary<string, RegionalIndex> Reduce(IReadOnlyDictionary<string, RegionalIndex> state, IAction action)
        {
            switch (action)
            {
                case RegionalIndexMessage regionalIndexMessage:
                    return state.CloneThenAddOrUpdate(
                        key: regionalIndexMessage.Code,
                        addIfNotExist: new RegionalIndex(
                            code: regionalIndexMessage.Code,
                            typeOrRegion: regionalIndexMessage.TypeOrRegion,
                            previous: regionalIndexMessage.Previous,
                            open: regionalIndexMessage.Open,
                            high: regionalIndexMessage.High,
                            low: regionalIndexMessage.Low,
                            last: regionalIndexMessage.Last,
                            volume: regionalIndexMessage.Volume
                        ),
                        updateIfExist: existingRegionalIndex => existingRegionalIndex.UpdateWith(
                            previous: regionalIndexMessage.Previous,
                            open: regionalIndexMessage.Open,
                            high: regionalIndexMessage.High,
                            low: regionalIndexMessage.Low,
                            last: regionalIndexMessage.Last,
                            volume: regionalIndexMessage.Volume
                        )
                    );
                case Actions.UpdateQuotes updateQuotesAction:
                    Dictionary<string, RegionalIndex> clone = new Dictionary<string, RegionalIndex>(state);
                    foreach (RegionalIndexMessage regionalIndexMessage in updateQuotesAction.RegionalIndexMessages)
                    {
                        if (clone.TryGetValue(regionalIndexMessage.Code, out RegionalIndex existingRegionalIndex))
                        {
                            existingRegionalIndex.UpdateWith(
                                previous: regionalIndexMessage.Previous,
                                open: regionalIndexMessage.Open,
                                high: regionalIndexMessage.High,
                                low: regionalIndexMessage.Low,
                                last: regionalIndexMessage.Last,
                                volume: regionalIndexMessage.Volume
                            );
                        }
                        else
                        {
                            clone.Add(regionalIndexMessage.Code, new RegionalIndex(
                                code: regionalIndexMessage.Code,
                                typeOrRegion: regionalIndexMessage.TypeOrRegion,
                                previous: regionalIndexMessage.Previous,
                                open: regionalIndexMessage.Open,
                                high: regionalIndexMessage.High,
                                low: regionalIndexMessage.Low,
                                last: regionalIndexMessage.Last,
                                volume: regionalIndexMessage.Volume
                            ));
                        }
                    }
                    return clone;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(RegionalIndexByCodeStore)}.");
            }
        }

        public static class Actions
        {
            public class UpdateQuotes : IAction
            {
                public IReadOnlyCollection<RegionalIndexMessage> RegionalIndexMessages { get; }
                public UpdateQuotes(IReadOnlyCollection<RegionalIndexMessage> regionalIndexMessages)
                {
                    RegionalIndexMessages = regionalIndexMessages;
                }
            }
        }

        public IAction UpdateQuotes(IReadOnlyCollection<RegionalIndexMessage> regionalIndexMessages)
        {
            return Dispatch(new Actions.UpdateQuotes(regionalIndexMessages));
        }
    }
}
