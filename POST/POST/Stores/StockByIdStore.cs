﻿using POST.DataSources.DataFeedMessages;
using POST.Extensions;
using POST.Models;
using Redux;
using System;
using System.Collections.Generic;
using System.Linq;

namespace POST.Stores
{

    /// <summary>
    /// Note: StockId is a tuple of (StockCode, MarketType)
    /// </summary>
    public class StockByIdStore : Store<IReadOnlyDictionary<StockId, Stock>>
    {

        public StockByIdStore() : base(
            reducer: Reduce,
            initialState: new Dictionary<StockId, Stock>())
        { }

        private static IReadOnlyDictionary<StockId, Stock> Reduce(IReadOnlyDictionary<StockId, Stock> state, IAction action)
        {
            switch (action)
            {
                case Actions.Init initAction:
                    return initAction.StockInitMessages.TrySelect(
                        selector: stockInitMessage => new Stock(
                            code: stockInitMessage.StockCode,
                            marketType: stockInitMessage.MarketType,
                            name: stockInitMessage.StockName,
                            sharesPerLot: stockInitMessage.SharesPerLot,
                            totalShares: stockInitMessage.TotalShare,
                            remarks: stockInitMessage.Remarks,
                            previous: stockInitMessage.PrevPrice
                        )
                    ).GroupBy(
                        keySelector: stock => stock.Id
                    ).ToDictionary(
                        keySelector: group => group.Key,
                        elementSelector: stocks => stocks.Last()
                    );

                case Actions.Init2 initAction2:
                    {
                        return initAction2.StockInitMessages.TrySelect(
                            selector: stockInitMessage => new Stock(
                                code: stockInitMessage.code,
                                marketType: stockInitMessage.MarketType,
                                name: stockInitMessage.name,
                                sharesPerLot: stockInitMessage.sharesPerLot,
                                totalShares: stockInitMessage.totalShares,
                                remarks: stockInitMessage.remarks2,
                                previous: stockInitMessage.PrevPrice
                            )
                        ).GroupBy(
                            keySelector: stock => stock.Id
                        ).ToDictionary(
                            keySelector: group => group.Key,
                            elementSelector: stocks => stocks.Last()
                        );
                    }

                case Actions.UpdateQuotes updateQuotesAction:
                    foreach (StockQuoteMessage stockQuoteMessage in updateQuotesAction.StockQuoteMessages)
                    {
                        StockId stockId = new StockId(stockQuoteMessage.StockCode, stockQuoteMessage.MarketType);
                        if (state.TryGetValue(stockId, out Stock existingStock))
                        {
                            existingStock.UpdateWithStockQuote(
                                previous: stockQuoteMessage.Previous,
                                open: stockQuoteMessage.Open,
                                high: stockQuoteMessage.High,
                                low: stockQuoteMessage.Low,
                                last: stockQuoteMessage.Last,
                                volume: stockQuoteMessage.Volume,
                                value: stockQuoteMessage.Value,
                                frequency: stockQuoteMessage.Frequency,
                                bestBidPrice: stockQuoteMessage.BestBidPrice,
                                bestBidVolume: stockQuoteMessage.BestBidVolume,
                                bestOfferPrice: stockQuoteMessage.BestOfferPrice,
                                bestOfferVolume: stockQuoteMessage.BestOfferVolume,
                                lastTradeVolume: stockQuoteMessage.LastTradeVolume,
                                totalForeignSellVolume: stockQuoteMessage.TotalForeignSellVolume,
                                totalForeignSellValue: stockQuoteMessage.TotalForeignSellValue,
                                totalForeignSellFrequency: stockQuoteMessage.TotalForeignSellFrequency,
                                totalForeignBuyVolume: stockQuoteMessage.TotalForeignBuyVolume,
                                totalForeignBuyValue: stockQuoteMessage.TotalForeignBuyValue,
                                totalForeignBuyFrequency: stockQuoteMessage.TotalForeignBuyFrequency,
                                totalDomesticSellVolume: stockQuoteMessage.TotalDomesticSellVolume,
                                totalDomesticSellValue: stockQuoteMessage.TotalDomesticSellValue,
                                totalDomesticSellFrequency: stockQuoteMessage.TotalDomesticSellFrequency,
                                totalDomesticBuyVolume: stockQuoteMessage.TotalDomesticBuyVolume,
                                totalDomesticBuyValue: stockQuoteMessage.TotalDomesticBuyValue,
                                totalDomesticBuyFrequency: stockQuoteMessage.TotalDomesticBuyFrequency
                            );
                        }
                    }
                    return state;
                default:
                    throw new NotImplementedException($"Action {action.GetType()} is not supported in {nameof(StockByIdStore)}.");
            }
        }

        private static class Actions
        {

            public class Init : IAction
            {
                public IReadOnlyCollection<StockInitMessage> StockInitMessages { get; }
                public Init(IReadOnlyCollection<StockInitMessage> stockInitMessages)
                {
                    StockInitMessages = stockInitMessages ?? throw new ArgumentNullException(nameof(stockInitMessages));
                }
            }

            public class Init2 : IAction
            {
                public IReadOnlyCollection<StockInitMessage2> StockInitMessages { get; }
                public Init2(IReadOnlyCollection<StockInitMessage2> stockInitMessages)
                {
                    StockInitMessages = stockInitMessages ?? throw new ArgumentNullException(nameof(stockInitMessages));
                }
            }

            public class UpdateQuotes : IAction
            {
                public IReadOnlyCollection<StockQuoteMessage> StockQuoteMessages { get; }
                public UpdateQuotes(IReadOnlyCollection<StockQuoteMessage> stockQuoteMessages)
                {
                    StockQuoteMessages = stockQuoteMessages ?? throw new ArgumentNullException(nameof(stockQuoteMessages));
                }
            }
        }

        public IAction Init(IReadOnlyCollection<StockInitMessage> stockInitMessages)
        {
            return Dispatch(new Actions.Init(stockInitMessages));
        }

        public IAction Init2(IReadOnlyCollection<StockInitMessage2> stockInitMessages)
        {
            return Dispatch(new Actions.Init2(stockInitMessages));
        }

        public IAction UpdateQuotes(IReadOnlyCollection<StockQuoteMessage> stockQuoteMessages)
        {
            return Dispatch(new Actions.UpdateQuotes(stockQuoteMessages));
        }
    }
}
