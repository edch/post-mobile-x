﻿using POST.Constants;
using POST.DataSources.AthenaMessages.Responses;
using POST.Dialogs;
using POST.Pages;
using POST.Pages.StockPages;
using POST.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SidebarMenu : ContentPage
    {

        public readonly ListView ListView;
        private readonly SidebarMenuViewModel ViewModel;

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public SidebarMenu()
        {
            InitializeComponent();

            ViewModel = new SidebarMenuViewModel();
            BindingContext = ViewModel;
            ListView = MenuItemsListView;

            AccountGrid.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
                })
            });
        }

        private void SetLogo()
        {
            if (!Store.TradingAccounts.IsSyariahAcc)
            {
                imgLogo.Source = ImageSource.FromFile("logo_post.png");
            }
            else
            {
                imgLogo.Source = ImageSource.FromFile("logo_syariah.png");
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            ListView.SelectedItem = ViewModel.MenuItems.First();

            _observers.Add(Store.AthenaSession.Subscribe(athenaSession =>
            {
                if (athenaSession != null &&
                    athenaSession.BrokerAccountId != null)
                {
                    if (Store.TradingAccounts.GetState()?.FirstOrDefault(account => account.BrokerAccountId == athenaSession.BrokerAccountId) is TradingAccount tradingAccount)
                    {
                        AccountLabel.Text = $"{tradingAccount.BrokerAccountId} ({tradingAccount.CustomerType}) - {tradingAccount.CustomerName}";
                    }
                    else
                    {
                        AccountLabel.Text = $"{athenaSession.BrokerAccountId}";
                    }
                    AccountLabel.TextColor = Color.White;
                    AccountGrid.BackgroundColor = Color.FromRgb(0x66, 0x00, 0x00);
                }
                else
                {
                    AccountLabel.Text = "No Account Selected";
                    AccountLabel.TextColor = Color.FromRgba(0xff, 0xff, 0xff, 0xcc);
                    AccountGrid.BackgroundColor = Color.FromRgba(0xff, 0xff, 0xff, 0x11);
                }
            }));

            _observers.Add(Store.TradingAccounts.Subscribe(tradingAccounts =>
            {
                SetLogo();
            }));
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
            base.OnDisappearing();
        }

        private class SidebarMenuViewModel
        {

            public ObservableCollection<Models.MenuItem> MenuItems { get; }

            public SidebarMenuViewModel()
            {
                MenuItems = new ObservableCollection<Models.MenuItem>(new[] {
                    new Models.MenuItem(id: 0, icon: Glyphs.RunningTrade, title: "Running Trade", targetType: typeof(RunningTradePage)),
                    new Models.MenuItem(id: 1, icon: Glyphs.Market, title: "Market", targetType: typeof(MarketPage)),
                    new Models.MenuItem(id: 2, icon: Glyphs.Stock, title: "Stock", targetType: typeof(StockPage)),
                    new Models.MenuItem(id: 3, icon: Glyphs.Chart, title: "Chart", targetType: typeof(TechnicalChartPage)),
                    new Models.MenuItem(id: 4, icon: Glyphs.BrokerAccDist, title: "Accum & Dist", targetType: typeof(BrokerAccDistPage)),
                    new Models.MenuItem(id: 5, icon: Glyphs.IndicesAndCurrencies, title: "Indices & Currencies", targetType: typeof(IndicesAndCurrenciesPage)),
                    new Models.MenuItem(id: 6, icon: Glyphs.News, title: "News & Research", targetType: typeof(ResearchPage)),
                    new Models.MenuItem(id: 7, icon: Glyphs.Orders, title: "Orders", targetType: typeof(OrdersPage)),
                    new Models.MenuItem(id: 8, icon: Glyphs.Rights, title: "Right/Warrant", targetType: typeof(RightsOrderPages)),
                    new Models.MenuItem(id: 9, icon: Glyphs.Portfolio, title: "Portfolio & Account", targetType: typeof(PortfolioPage)),
                    new Models.MenuItem(id: 10, icon: Glyphs.Alerts, title: "Alerts", targetType: typeof(AlertsPage)),
                    new Models.MenuItem(id: 11, icon: Glyphs.Gear, title: "Configuration", targetType: typeof(ConfigurationPage)),
                    new Models.MenuItem(id: 12, icon: Glyphs.Support, title: "Help/Contact Us", targetType: typeof(HelpContactUs)),
                    new Models.MenuItem(id: 13, icon: Glyphs.TellAFriend, title: "Tell a Friend", targetType: typeof(IShareDialog)),
                    new Models.MenuItem(id: 14, icon: Glyphs.About, title: "About", targetType: typeof(AboutPage)),
                    new Models.MenuItem(id: 15, icon: Glyphs.Logout, title: "Logout", targetType: typeof(Models.MenuItem.LogoutMenu))
                });
            }
        }
    }
}