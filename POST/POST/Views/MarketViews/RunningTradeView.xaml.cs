﻿using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Views.MarketViews
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RunningTradeView : ContentView, IContentView
    {

        private class ClientState { }

        private readonly DataFeedClientStateManager<ClientState> _clientStateManager;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public RunningTradeView()
        {
            InitializeComponent();

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState(),
                clientFactory: state =>
                {
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        await sender.SendAsync(new SubscribeRunningTradeRequest());
                    };
                    client.DataReceived += (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case RunningTradeMessage runningTradeMessage:
                                Store.RunningTrade.AddTrade(runningTradeMessage);
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
            );

            _observers.Add(Store.RunningTrade
                .DistinctUntilChanged()
                .ObserveOn(SynchronizationContext.Current)
                .SubscribeOn(SynchronizationContext.Current)
                .Subscribe(runningTrade =>
                {
                    BindingContext = runningTrade;
                })
            );
        }

        ~RunningTradeView()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
        }

        public void OnAppearing()
        {
            _clientStateManager.StartClient();
        }

        public void OnDisappearing()
        {
            _clientStateManager.StopClient();
        }

        // Get ListView height and determine page size
        private void TradesListView_SizeChanged(object sender, EventArgs e)
        {

            double height = TradesListView.Height;
            double width = TradesListView.Width;

            int rowHeight;
            if (height < 600)
            {
                rowHeight = 40;
            }
            else if (height < 1000)
            {
                rowHeight = 50;
            }
            else
            {
                rowHeight = 60;
            }

            if (rowHeight != TradesListView.RowHeight)
            {
                TradesListView.RowHeight = rowHeight;
            }

            int visibleRows = (int)Math.Floor(height / rowHeight);
            Store.RunningTrade.SetPageSize(visibleRows);

            Column0.Width = width < 320 ? new GridLength(0) : (width < 560 ? new GridLength(60) : new GridLength(1.8, GridUnitType.Star));
            Column3.Width = width < 500 ? new GridLength(3, GridUnitType.Star) : new GridLength(80);
            Column4.Width = width < 500 ? new GridLength(3, GridUnitType.Star) : new GridLength(80);
            Column6.Width = width < 380 ? new GridLength(0) : (width < 560 ? new GridLength(30) : new GridLength(0.9, GridUnitType.Star));
            Column7.Width = width < 380 ? new GridLength(0) : (width < 560 ? new GridLength(30) : new GridLength(0.9, GridUnitType.Star));
            Header0.IsVisible = width >= 320;
            Header6.IsVisible = width >= 380;
            Header7.IsVisible = width >= 380;
        }

        // Disable selection
        private void TradesListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (TradesListView.SelectedItem != null)
            {
                TradesListView.SelectedItem = null;
            }
        }
    }
}