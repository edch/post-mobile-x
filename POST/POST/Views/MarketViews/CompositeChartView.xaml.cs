﻿using POST.DataSources;
using POST.DataSources.ApiResponses;
using POST.Dialogs;
using POST.Models;
using POST.Pages;
using POST.Pages.MarketPages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Views.MarketViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompositeChartView : ContentView, IContentView
    {

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();
        private static bool _timerRunning = false;

        private static readonly IReadOnlyDictionary<string, ChartMode> ChartModes = new Dictionary<string, ChartMode> {
            { "Line", ChartMode.Line },
            { "Candlestick", ChartMode.Candlestick }
        };

        public CompositeChartView()
        {
            InitializeComponent();
            foreach (string chartModeKey in ChartModes.Keys)
            {
                ChartModePicker.Items.Add(chartModeKey);
            }

            _observers.Add(Store.CompositeChart
                .ObserveOn(SynchronizationContext.Current)
                .Subscribe(compositeChart =>
                {
                    if (compositeChart != null && compositeChart.Count > 0)
                    {
                        compositeChart.RemoveAt(0);
                        CompositeChart.IndexChart = compositeChart;
                        CompositeChart.InvalidateSurface();
                    }
                })
            );

            _observers.Add(Store.IndexByCode
                .Select(indexByCode => indexByCode.TryGetValue("COMPOSITE", out Models.Index index) ? index : null)
                .DistinctUntilChanged()
                .ObserveOn(SynchronizationContext.Current)
                .Subscribe(composite =>
                {
                    CompositeChart.Index = composite;
                    BindingContext = composite;
                })
            );
        }

        ~CompositeChartView()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
        }

        public async void OnAppearing()
        {
            IsVisible = true;
            await RefreshChartAsync();
            if (!_timerRunning)
            {
                Device.StartTimer(TimeSpan.FromMinutes(1), () =>
                {
                    if (IsVisible)
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            await RefreshChartAsync();
                        });
                    }
                    if (!IsVisible)
                    {
                        _timerRunning = false;
                    }
                    return IsVisible;
                });
                _timerRunning = true;
            }
        }

        private async Task RefreshChartAsync()
        {
            try
            {
                IntradayIndexChartData[] data = await IntradayCompositeChartClient.Default.GetIntradayIndexChartAsync(CancellationToken.None);
                Store.CompositeChart.UpdateWithIntradayIndexChartData(data);
            }
            catch { }
        }

        public void OnDisappearing()
        {
            IsVisible = false;
        }

        public void ChartModeButton_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                ChartModePicker.Focus();
            });
        }

        public void ChartModePicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ChartModePicker.SelectedItem is string selectedItem &&
                ChartModes.TryGetValue(selectedItem, out ChartMode chartMode))
            {
                Store.ChartMode.SetChartMode(chartMode);
            }
        }

        public async void TechnicalChartButton_Clicked(object sender, EventArgs e)
        {
            TechnicalChartPage page = (TechnicalChartPage)Activator.CreateInstance(typeof(TechnicalChartPage));
            page.Title = "COMPOSITE";
            await MainPage.NavigationPage.PushAsync(page);
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}