﻿using POST.Constants;
using POST.DataSources;
using POST.Dialogs;
using POST.Models;
using POST.Models.Collections;
using POST.Pages;
using POST.Pages.MarketPages;
using POST.ViewCells;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Views.MarketViews
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WatchListView : ContentView, IContentView
    {

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public ObservableSet<StockId, Stock> Stocks { get; }

        private static bool _timerRunning = false;
        private bool isVisible = false;
        private List<Stock> lstStocks = new List<Stock>();
        private Picker SortKeyPicker = null;

        private static readonly IReadOnlyDictionary<string, SortKey> SortKeys = new Dictionary<string, SortKey> {
            { "Stock Code A-Z", SortKey.CodeAsc },
            { "Stock Code Z-A", SortKey.CodeDesc },
            { "Top Gainer (%)", SortKey.ChangePercentDesc },
            { "Top Loser (%)", SortKey.ChangePercentAsc }
        };

        public WatchListView()
        {
            InitializeComponent();
            //Stocks = new ObservableSet<StockId, Stock>();
            //StocksListView.ItemsSource = Stocks;

            // Observer for updating Sort icon in toolbar
            _observers.Add(Store.WatchListSortKey
                .ObserveOn(SynchronizationContext.Current)
                .Subscribe(sortKey =>
                {
                    //SortButton.Text = Glyphs.ForSortKey(sortKey);
                })
            );

            _observers.Add(Store.WatchListViewMode
                .Subscribe(viewMode =>
                {
                    switch (viewMode)
                    {
                        case ViewMode.Simple:
                            RowHeader.Height = 25;
                            GridRowHeader.IsVisible = true;
                            StocksListView.ItemTemplate = new DataTemplate(() => new CompactStockCell(WatchListView_StockCellSingleTap, WatchListView_StockCellLongPress));
                            StocksListView.RowHeight = 35;
                            break;
                        case ViewMode.Advanced:
                            RowHeader.Height = 0;
                            GridRowHeader.IsVisible = false;
                            StocksListView.ItemTemplate = new DataTemplate(() => new StockCell(WatchListView_StockCellSingleTap, WatchListView_StockCellLongPress));
                            StocksListView.RowHeight = 71;
                            break;
                    }
                })
            );

            // Observe WatchedStockIds, StockById, WatchListSortKey together           
            _observers.Add(
                Observable.Merge(
                    Store.WatchedStockIds.Select(stockIds => new
                    {
                        StockIds = stockIds,
                        StockById = Store.StockById.GetState(),
                        SortKey = Store.WatchListSortKey.GetState()
                    }),
                    Store.StockById.Select(stockById => new
                    {
                        StockIds = Store.WatchedStockIds.GetState(),
                        StockById = stockById,
                        SortKey = Store.WatchListSortKey.GetState()
                    }),
                    Store.WatchListSortKey.Select(sortKey => new
                    {
                        StockIds = Store.WatchedStockIds.GetState(),
                        StockById = Store.StockById.GetState(),
                        SortKey = sortKey
                    })
                )
                .Select(merged =>
                {
                    // Sort stock codes by selected sort key
                    switch (merged.SortKey)
                    {
                        default:
                        case SortKey.CodeAsc:
                            return (from stockId in merged.StockIds
                                    orderby stockId.Code ascending
                                    select stockId.Code).ToList();
                        case SortKey.CodeDesc:
                            return (from stockId in merged.StockIds
                                    orderby stockId.Code descending
                                    select stockId.Code).ToList();
                        case SortKey.ChangePercentAsc:
                            return (from stockId in merged.StockIds
                                    where merged.StockById.ContainsKey(stockId)
                                    let changePercent = merged.StockById[stockId].ChangePercent
                                    where changePercent.HasValue
                                    orderby changePercent.Value ascending
                                    select stockId.Code).Union(
                                        from stockId in merged.StockIds
                                        where merged.StockById.ContainsKey(stockId)
                                        where !merged.StockById[stockId].ChangePercent.HasValue
                                        orderby stockId.Code ascending
                                        select stockId.Code).Union(
                                            from stockId in merged.StockIds
                                            where !merged.StockById.ContainsKey(stockId)
                                            orderby stockId.Code ascending
                                            select stockId.Code).ToList();
                        case SortKey.ChangePercentDesc:
                            return (from stockId in merged.StockIds
                                    where merged.StockById.ContainsKey(stockId)
                                    let changePercent = merged.StockById[stockId].ChangePercent
                                    where changePercent.HasValue
                                    orderby changePercent.Value descending
                                    select stockId.Code).Union(
                                        from stockId in merged.StockIds
                                        where merged.StockById.ContainsKey(stockId)
                                        where !merged.StockById[stockId].ChangePercent.HasValue
                                        orderby stockId.Code descending
                                        select stockId.Code).Union(
                                            from stockId in merged.StockIds
                                            where !merged.StockById.ContainsKey(stockId)
                                            orderby stockId.Code descending
                                            select stockId.Code).ToList();
                    }
                })
                .Select(stockCodes =>
                {
                    // Transform stock codes into stocks
                    return stockCodes.Select(stockCode =>
                    {
                        if (Store.StockById.GetState().TryGetValue(new StockId(stockCode, MarketType.RG), out Stock stock))
                        {
                            return stock;
                        }
                        else
                        {
                            return new Stock(
                                code: stockCode,
                                marketType: MarketType.RG,
                                name: StockNameFallback.For(stockCode)
                            );
                        }
                    }).ToList();
                })
                .ObserveOn(SynchronizationContext.Current)
                .Subscribe(stocks =>
                {
                    // Update items source                    
                    lstStocks = stocks;
                })
            );
        }

        ~WatchListView()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
        }


        public async void OnAppearing()
        {
            //await DataFeedClient.StateManager.Current.RequestStockInitAsync();
            //await DataFeedClient.StateManager.Current.SetSubscribedStocksAsync(
            //    stockCodes: Store.WatchedStockIds.GetState()
            //        .Select(stockId => stockId.Code)
            //        .Distinct()
            //        .ToHashSet()
            //);            

            isVisible = true;

            Picker SortKeyPickerTmp = new Picker()
            {
                Title = "Sort by",
                IsVisible = false,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 15
            };

            SortKeyPickerTmp.ItemsSource = SortKeys.Keys.ToList();
            SortKeyPickerTmp.SelectedIndexChanged += SortKeyPicker_SelectedIndexChanged;

            SortKeyPicker = SortKeyPickerTmp;

            grdWatchlist.Children.Add(SortKeyPicker);

            UpdateView();

            if (!_timerRunning)
            {
                Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                {
                    if (isVisible)
                    {
                        UpdateView();
                    }
                    if (!isVisible)
                    {
                        _timerRunning = false;
                    }
                    return isVisible;
                });
                _timerRunning = true;
            }
        }

        public void OnDisappearing()
        {
            isVisible = false;

            if (SortKeyPicker != null)
            {
                grdWatchlist.Children.Remove(SortKeyPicker);
                SortKeyPicker.SelectedIndexChanged -= SortKeyPicker_SelectedIndexChanged;
                SortKeyPicker = null;
            }
        }

        private void UpdateView()
        {
            StocksListView.ItemsSource = lstStocks;
            //Stocks.SetItems(stocks);
        }

        private async void WatchListView_StockCellSingleTap(object sender, StockCellEventArgs e)
        {
            Stock stock = e.stock;
            Store.RecentStockIds.AddStock(stock.Id);
            StockPage page = new StockPage();
            await MainPage.NavigationPage.PushAsync(page);

            if (sender is CompactStockCell || sender is StockCell)
            {
                StocksListView.SelectedItem = null;
            }
        }

        private async void WatchListView_StockCellLongPress(object sender, StockCellEventArgs e)
        {
            Stock stock = e.stock;
            bool AnswerYes = await Application.Current.MainPage.DisplayAlert("Remove Confirmation", "Are you sure want to remove " + stock.Id.Code + "?", "Ok", "Cancel");
            if (AnswerYes)
            {
                Store.WatchedStockIds.RemoveStock(stock.Id.Code);
            }

            if (sender is CompactStockCell || sender is StockCell)
            {
                StocksListView.SelectedItem = null;
            }
        }

        private async void Handle_PullToRefresh(object sender, EventArgs e)
        {
            //await DataFeedClient.StateManager.Current.RequestStockInitAsync();
            //await DataFeedClient.StateManager.Current.SetSubscribedStocksAsync(
            //	stockCodes: Store.WatchedStockIds.GetState()
            //		.Select(stockId => stockId.Code)
            //		.Distinct()
            //		.ToHashSet()
            //);

            await DataFeedClient.StateManager.Current.SubscribeToAllStocksAsync(false);
            await DataFeedClient.StateManager.Current.SubscribeToAllStocksAsync(true);
            StocksListView.IsRefreshing = false;
        }

        public void SortButton_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                SortKeyPicker.Focus();
            });
        }

        private void SortKeyPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SortKeyPicker.SelectedItem is string selectedItem &&
                SortKeys.TryGetValue(selectedItem, out SortKey sortKey))
            {
                Store.WatchListSortKey.SortBy(sortKey);
                UpdateView();
            }
        }

        public async void AddButton_Clicked(object sender, EventArgs e)
        {
            AddWatchlistPage addWatchlistPage = new AddWatchlistPage();
            await MainPage.NavigationPage.PushAsync(addWatchlistPage);
            UpdateView();
        }

        public void SimpleViewButton_Clicked(object sender, EventArgs e)
        {
            Store.WatchListViewMode.SetViewMode(ViewMode.Simple);
            UpdateView();
        }

        public void AdvancedViewButton_Clicked(object sender, EventArgs e)
        {
            Store.WatchListViewMode.SetViewMode(ViewMode.Advanced);
            UpdateView();
        }

        public async void WatchListDeleteStocksButton_Clicked(object sender, EventArgs e)
        {
            DeleteWatchlistPage deleteWatchlistPage = new DeleteWatchlistPage
            {
                Title = "Delete Stocks"
            };
            await MainPage.NavigationPage.PushAsync(deleteWatchlistPage);
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}