﻿using POST.DataSources;
using POST.Dialogs;
using POST.Models;
using POST.Pages;
using POST.Stores;
using POST.ViewCells;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Views.MarketViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MarketMoversView : ContentView, IContentView
    {
        private readonly List<string> lstMarketMoverCategory;

        //private readonly DataFeedClientStateManager<ClientState> _clientStateManager;
        private static bool _timerRunning = false;
        private bool isVisible = false;
        private List<Stock> lstStocks = null;
        private Picker pickerCategory = null;
        private Picker pickerCategoryHelp = null;

        public MarketMoversView()
        {
            InitializeComponent();

            lstMarketMoverCategory = new List<string>() { "Top Gainers", "Top Losers", "Most Traded By Volume", "Most Traded By Frequency", "Most Traded By Value" };

            //_observers.Add(Store.MarketMoversViewMode
            //    .Subscribe(viewMode =>
            //    {
            //        switch (viewMode)
            //        {
            //            case ViewMode.Simple:
            //                RowHeader.Height = 25;
            //                GridRowHeader.IsVisible = true;
            //                MarketMoversListView.ItemTemplate = new DataTemplate(() => new CompactStockCell(MarketMoversListView_StockCellSingleTap, null));
            //                MarketMoversListView.RowHeight = 35;
            //                if (layoutHelp.IsVisible)
            //                {                                
            //                        grdFrameStockSimple.IsVisible = true;
            //                        imgPointFingerStockSimple.IsVisible = true;
            //                        imgTapToSeeDetailsSimple.IsVisible = true;

            //                        grdFrameStockAdvance.IsVisible = false;
            //                        imgPointFingerStockAdvance.IsVisible = false;
            //                        imgTapToSeeDetailsAdvance.IsVisible = false;                                
            //                }
            //                    break;
            //            case ViewMode.Advanced:
            //                RowHeader.Height = 0;
            //                GridRowHeader.IsVisible = false;
            //                MarketMoversListView.ItemTemplate = new DataTemplate(() => new StockCell(MarketMoversListView_StockCellSingleTap, null));
            //                MarketMoversListView.RowHeight = 71;
            //                if (layoutHelp.IsVisible)
            //                {
            //                    grdFrameStockSimple.IsVisible = false;
            //                    imgPointFingerStockSimple.IsVisible = false;
            //                    imgTapToSeeDetailsSimple.IsVisible = false;

            //                    grdFrameStockAdvance.IsVisible = true;
            //                    imgPointFingerStockAdvance.IsVisible = true;
            //                    imgTapToSeeDetailsAdvance.IsVisible = true;
            //                }
            //                break;
            //        }
            //    })
            //);

            if (AppSettings.LayoutHelp_MarketMovers == "1")
            {
                layoutHelp.IsVisible = true;

                imgTopOverlay.GestureRecognizers.Add(new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        layoutHelp.IsVisible = false;
                        if (switchNeverShowAgain.IsToggled)
                        {
                            AppSettings.LayoutHelp_MarketMovers = "0";
                        }
                    })
                });

                imgNeverShowAgain.GestureRecognizers.Add(new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        switchNeverShowAgain.IsToggled = !switchNeverShowAgain.IsToggled;
                    })
                });
            }
        }

        ~MarketMoversView()
        {

        }

        public void OnAppearing()
        {
            isVisible = true;

            Picker pickerCategoryTmp = new Picker()
            {
                Title = "Choose Category",
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 15
            };

            pickerCategoryTmp.ItemsSource = lstMarketMoverCategory;
            pickerCategoryTmp.SelectedIndex = Convert.ToInt32(AppSettings.MarketMoversCategory);
            pickerCategoryTmp.SelectedIndexChanged += pickerCategory_SelectedIndexChanged;

            pickerCategory = pickerCategoryTmp;

            grdPicker.Children.Add(pickerCategoryTmp);

            Picker pickerCategoryHelpTmp = new Picker()
            {
                Title = "Choose Category",
                IsTabStop = false,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 15
            };

            pickerCategoryHelpTmp.ItemsSource = lstMarketMoverCategory;
            pickerCategoryHelpTmp.SelectedIndex = Convert.ToInt32(AppSettings.MarketMoversCategory);

            pickerCategoryHelp = pickerCategoryHelpTmp;

            grdPickerHelp.Children.Add(pickerCategoryHelpTmp);

            UpdateItemListSource();
            UpdateView();

            if (!_timerRunning)
            {
                Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                {
                    if (isVisible)
                    {
                        UpdateItemListSource();
                        UpdateView();
                    }
                    if (!isVisible)
                    {
                        _timerRunning = false;
                    }
                    return isVisible;
                });
                _timerRunning = true;
            }
        }

        public void OnDisappearing()
        {
            isVisible = false;

            if (pickerCategory != null)
            {
                grdPicker.Children.Remove(pickerCategory);
                pickerCategory.SelectedIndexChanged -= pickerCategory_SelectedIndexChanged;
                pickerCategory = null;
            }

            if (pickerCategoryHelp != null)
            {
                grdPickerHelp.Children.Remove(pickerCategoryHelp);
                pickerCategoryHelp = null;
            }
        }

        private void UpdateItemListSource()
        {
            List<Stock> lstStocksTmp = null;

            if (AppSettings.MarketMoversCategory == MarketMoversCategory.TopGainers)
            {
                lstStocksTmp = Store.StockById.GetState().Values.Where(stock =>
                    stock.IsInitialized &&
                    stock.Id.MarketType == MarketType.RG &&
                    !stock.Id.Code.Contains("-") &&
                    stock.ChangePercent.HasValue &&
                    stock.ChangePercent.Value > 0
                ).OrderByDescending(stock => stock.ChangePercent.Value).ToList();

                lstStocks = lstStocksTmp;
            }
            else if (AppSettings.MarketMoversCategory == MarketMoversCategory.TopLosers)
            {
                lstStocksTmp = Store.StockById.GetState().Values.Where(stock =>
                    stock.IsInitialized &&
                    stock.Id.MarketType == MarketType.RG &&
                    !stock.Id.Code.Contains("-") &&
                    stock.ChangePercent.HasValue &&
                    stock.ChangePercent.Value < 0
                ).OrderBy(stock => stock.ChangePercent.Value).ToList();

                lstStocks = lstStocksTmp;
            }
            else if (AppSettings.MarketMoversCategory == MarketMoversCategory.MostTradedByVolume)
            {
                lstStocksTmp = Store.StockById.GetState().Values.Where(stock =>
                       stock.IsInitialized &&
                       stock.Id.MarketType == MarketType.RG &&
                       !stock.Id.Code.Contains("-") &&
                       stock.Magnitudes != null &&
                       stock.Magnitudes.Volume > 0
                   ).OrderByDescending(stock => stock.Magnitudes.Volume).ToList();

                lstStocks = lstStocksTmp;
            }
            else if (AppSettings.MarketMoversCategory == MarketMoversCategory.MostTradedByFrequency)
            {
                lstStocksTmp = Store.StockById.GetState().Values.Where(stock =>
                           stock.IsInitialized &&
                           stock.Id.MarketType == MarketType.RG &&
                           !stock.Id.Code.Contains("-") &&
                           stock.Magnitudes != null &&
                           stock.Magnitudes.Frequency > 0
                       ).OrderByDescending(stock => stock.Magnitudes.Frequency).ToList();

                lstStocks = lstStocksTmp;
            }
            else if (AppSettings.MarketMoversCategory == MarketMoversCategory.MostTradedByValue)
            {
                lstStocksTmp = Store.StockById.GetState().Values.Where(stock =>
                              stock.IsInitialized &&
                              stock.Id.MarketType == MarketType.RG &&
                              !stock.Id.Code.Contains("-") &&
                              stock.Magnitudes != null &&
                              stock.Magnitudes.Value > 0
                          ).OrderByDescending(stock => stock.Magnitudes.Value).ToList();

                lstStocks = lstStocksTmp;
            }
        }

        private void UpdateView()
        {
            if (lstStocks != null && lstStocks.Any())
            {
                MarketMoversListView.ItemsSource = lstStocks;
            }
        }

        private async void MarketMoversListView_StockCellSingleTap(object sender, StockCellEventArgs e)
        {
            Stock stock = e.stock;
            Store.RecentStockIds.AddStock(stock.Id);
            StockPage page = new StockPage();
            await MainPage.NavigationPage.PushAsync(page);

            if (sender is CompactStockCell || sender is StockCell)
            {
                MarketMoversListView.SelectedItem = null;
            }
        }

        public void SimpleViewButton_Clicked(object sender, EventArgs e)
        {
            Store.MarketMoversViewMode.SetViewMode(ViewMode.Simple);
        }

        public void AdvancedViewButton_Clicked(object sender, EventArgs e)
        {
            Store.MarketMoversViewMode.SetViewMode(ViewMode.Advanced);
        }

        private async void MarketMoversListView_Refreshing(object sender, EventArgs e)
        {
            await DataFeedClient.StateManager.Current.SubscribeToAllStocksAsync(false);
            await DataFeedClient.StateManager.Current.SubscribeToAllStocksAsync(true);
            MarketMoversListView.IsRefreshing = false;
        }

        private void pickerCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is Picker pickerCategory && AppSettings.MarketMoversCategory != (MarketMoversCategory)pickerCategory.SelectedIndex)
            {
                AppSettings.MarketMoversCategory = (MarketMoversCategory)pickerCategory.SelectedIndex;
                UpdateItemListSource();
                UpdateView();
            }
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}