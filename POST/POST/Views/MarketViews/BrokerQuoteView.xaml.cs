﻿using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Dialogs;
using POST.Models;
using POST.Pages;
using POST.Stores;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Views.MarketViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BrokerQuoteView : ContentView, IContentView
    {
        private class ClientState
        {
            public List<BrokerInitMessage> BrokerInitMessages { get; } = new List<BrokerInitMessage>();
            public ThrottledDispatcher<IReadOnlyDictionary<BrokerId, Broker>, BrokerQuoteMessage> ThrottledDispatcher { get; set; }
        }

        private readonly DataFeedClientStateManager<ClientState> _clientStateManager;

        private readonly List<string> lstBrokerQuoteCategory;
        private readonly List<string> lstBrokerQuoteType;
        private static bool _timerRunning = false;
        private bool isVisible = false;
        private List<Broker> lstBroker = null;
        private Picker pickerCategory = null;
        private Picker pickerType = null;
        private Picker SortKeyPicker = null;

        private static readonly IReadOnlyDictionary<string, BrokerQuoteSortKey> SortKeys = new Dictionary<string, BrokerQuoteSortKey> {
            { "Code A-Z", BrokerQuoteSortKey.CodeAsc},
            { "Code Z-A", BrokerQuoteSortKey.CodeDesc },
            { "Value 0-9", BrokerQuoteSortKey.ValAsc },
            { "Value 9-0", BrokerQuoteSortKey.ValDesc},
            { "Volume 0-9", BrokerQuoteSortKey.VolAsc },
            { "Volume 9-0", BrokerQuoteSortKey.VolDesc },
            { "Frequency 0-9", BrokerQuoteSortKey.FreqAsc },
            { "Frequency 9-0", BrokerQuoteSortKey.FreqDesc }
        };

        public BrokerQuoteView()
        {
            InitializeComponent();
            lstBrokerQuoteCategory = new List<string>() { "Total (Buy & Sell)", "Buy", "Sell", "Net" };
            lstBrokerQuoteType = new List<string>() { "All Market", "RG", "NG", "TN" };

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState(),
                clientFactory: state =>
                {
                    state.BrokerInitMessages.Clear();
                    state.ThrottledDispatcher = new ThrottledDispatcher<IReadOnlyDictionary<BrokerId, Broker>, BrokerQuoteMessage>(
                        store: Store.BrokerById,
                        actionDispatcher: messages => Store.BrokerById.UpdateQuotes(messages.ToList()),
                        coalesceInterval: TimeSpan.FromSeconds(0.5),
                        flushInterval: TimeSpan.FromSeconds(1)
                    );
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        await sender.SendAsync(new BrokerInitRequest());
                    };
                    client.DataReceived += async (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case BrokerInitMessage brokerInitMessage:
                                Store.BrokerById.Dispatch(brokerInitMessage);
                                break;
                            case BrokerInitEndMessage brokerInitEndMessage:
                                // HACK: We need to resend SubscribeAllStockQuotesRequest to refresh all quotes.
                                // This should be fixed on server side.
                                await Task.Delay(2_000);
                                if (_clientStateManager.IsConnected)
                                {
                                    await sender.SendAsync(new SubscribeAllBrokerQuotesRequest());
                                }
                                break;
                            case BrokerQuoteMessage brokerQuoteMessage:
                                //Debug.WriteLine("brokerQuoteMessage " + brokerQuoteMessage.BrokerCode + "," + brokerQuoteMessage.MarketType.ToString());
                                //state.ThrottledDispatcher.Dispatch(brokerQuoteMessage);
                                Store.BrokerById.Dispatch(brokerQuoteMessage);
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
            );
        }

        ~BrokerQuoteView()
        {

        }

        public void OnAppearing()
        {
            isVisible = true;

            //< Picker Grid.Column = "0" IsTabStop = "False" x: Name = "pickerType" Title = "Choose Market Type" FontFamily = "{StaticResource DINProMedium}" FontSize = "15"  TextColor = "White" />
            //< Picker Grid.Column = "1" IsTabStop = "False" x: Name = "pickerCategory" Title = "Choose Category" FontFamily = "{StaticResource DINProMedium}" FontSize = "15"  TextColor = "White" />

            //< Picker
            //  x: Name = "SortKeyPicker"
            //  Title = "Sort by"
            //  IsVisible = "False"
            //  FontFamily = "{StaticResource ProximaNovaReg}"
            //  FontSize = "15"
            //  BackgroundColor = "Black"
            //  SelectedIndexChanged = "SortKeyPicker_SelectedIndexChanged" />

            Picker SortKeyPickerTmp = new Picker()
            {
                Title = "Sort by",
                IsVisible = false,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 15
            };
            SortKeyPickerTmp.ItemsSource = SortKeys.Keys.ToList();
            SortKeyPickerTmp.SelectedIndex = Convert.ToInt32(AppSettings.BrokerQuoteSortKey);
            SortKeyPickerTmp.SelectedIndexChanged += SortKeyPicker_SelectedIndexChanged;
            SortKeyPicker = SortKeyPickerTmp;
            grdBrokerQuote.Children.Add(SortKeyPickerTmp);

            Picker pickerTypeTmp = new Picker()
            {
                Title = "Choose Market Type",
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 15
            };
            pickerTypeTmp.ItemsSource = lstBrokerQuoteType;
            pickerTypeTmp.SelectedIndex = Convert.ToInt32(AppSettings.BrokerQuoteMarketType);
            pickerTypeTmp.SelectedIndexChanged += pickerType_SelectedIndexChanged;
            pickerType = pickerTypeTmp;

            Picker pickerCategoryTmp = new Picker()
            {
                Title = "Choose Category",
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 15
            };
            pickerCategoryTmp.ItemsSource = lstBrokerQuoteCategory;
            pickerCategoryTmp.SelectedIndex = Convert.ToInt32(AppSettings.BrokerQuoteCategory);
            pickerCategoryTmp.SelectedIndexChanged += pickerCategory_SelectedIndexChanged;
            pickerCategory = pickerCategoryTmp;

            grdPicker.Children.Add(pickerTypeTmp, 0, 0);
            grdPicker.Children.Add(pickerCategoryTmp, 1, 0);

            _clientStateManager.StartClient();

            if (lstBroker == null || !lstBroker.Any())
            {
                BrokerListView.IsRefreshing = true;
            }
            else
            {
                UpdateItemListSource();
                UpdateView();
            }

            if (!_timerRunning)
            {
                Device.StartTimer(TimeSpan.FromSeconds(2), () =>
                {
                    if (isVisible)
                    {
                        UpdateItemListSource();
                        UpdateView();
                    }
                    if (!isVisible)
                    {
                        _timerRunning = false;
                    }
                    return isVisible;
                });
                _timerRunning = true;
            }
        }

        public void OnDisappearing()
        {
            isVisible = false;

            _clientStateManager.StopClient();

            if (SortKeyPicker != null)
            {
                grdBrokerQuote.Children.Remove(SortKeyPicker);
                SortKeyPicker.SelectedIndexChanged -= SortKeyPicker_SelectedIndexChanged;
                SortKeyPicker = null;
            }

            if (pickerType != null)
            {
                grdPicker.Children.Remove(pickerType);
                pickerType.SelectedIndexChanged -= pickerType_SelectedIndexChanged;
                pickerType = null;
            }

            if (pickerCategory != null)
            {
                grdPicker.Children.Remove(pickerCategory);
                pickerCategory.SelectedIndexChanged -= pickerCategory_SelectedIndexChanged;
                pickerCategory = null;
            }
        }

        private void UpdateItemListSource()
        {
            List<Broker> lstBrokerTmp = Store.BrokerById.GetState().Values.Where(broker => broker.Id.MarketType == AppSettings.BrokerQuoteMarketType).ToList();

            switch (AppSettings.BrokerQuoteSortKey)
            {
                default:
                case BrokerQuoteSortKey.CodeAsc:
                    lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.Id.Code).ToList();
                    break;
                case BrokerQuoteSortKey.CodeDesc:
                    lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.Id.Code).ToList();
                    break;
                case BrokerQuoteSortKey.ValAsc:
                    switch (AppSettings.BrokerQuoteCategory)
                    {
                        default:
                        case BrokerQuoteCategory.Total:
                            lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.TotalVal).ToList();
                            break;
                        case BrokerQuoteCategory.Buy:
                            lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.BuyVal).ToList();
                            break;
                        case BrokerQuoteCategory.Sell:
                            lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.SellVal).ToList();
                            break;
                        case BrokerQuoteCategory.Net:
                            lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.NetVal).ToList();
                            break;
                    }
                    break;
                case BrokerQuoteSortKey.ValDesc:
                    switch (AppSettings.BrokerQuoteCategory)
                    {
                        default:
                        case BrokerQuoteCategory.Total:
                            lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.TotalVal).ToList();
                            break;
                        case BrokerQuoteCategory.Buy:
                            lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.BuyVal).ToList();
                            break;
                        case BrokerQuoteCategory.Sell:
                            lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.SellVal).ToList();
                            break;
                        case BrokerQuoteCategory.Net:
                            lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.NetVal).ToList();
                            break;
                    }
                    break;
                case BrokerQuoteSortKey.VolAsc:
                    switch (AppSettings.BrokerQuoteCategory)
                    {
                        default:
                        case BrokerQuoteCategory.Total:
                            lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.TotalVol).ToList();
                            break;
                        case BrokerQuoteCategory.Buy:
                            lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.BuyVol).ToList();
                            break;
                        case BrokerQuoteCategory.Sell:
                            lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.SellVol).ToList();
                            break;
                        case BrokerQuoteCategory.Net:
                            lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.NetVol).ToList();
                            break;
                    }
                    break;
                case BrokerQuoteSortKey.VolDesc:
                    switch (AppSettings.BrokerQuoteCategory)
                    {
                        default:
                        case BrokerQuoteCategory.Total:
                            lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.TotalVol).ToList();
                            break;
                        case BrokerQuoteCategory.Buy:
                            lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.BuyVol).ToList();
                            break;
                        case BrokerQuoteCategory.Sell:
                            lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.SellVol).ToList();
                            break;
                        case BrokerQuoteCategory.Net:
                            lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.NetVol).ToList();
                            break;
                    }
                    break;
                case BrokerQuoteSortKey.FreqAsc:
                    switch (AppSettings.BrokerQuoteCategory)
                    {
                        default:
                        case BrokerQuoteCategory.Total:
                            lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.TotalFreq).ToList();
                            break;
                        case BrokerQuoteCategory.Buy:
                            lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.BuyFreq).ToList();
                            break;
                        case BrokerQuoteCategory.Sell:
                            lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.SellFreq).ToList();
                            break;
                        case BrokerQuoteCategory.Net:
                            lstBrokerTmp = lstBrokerTmp.OrderBy(broker => broker.NetFreq).ToList();
                            break;
                    }
                    break;
                case BrokerQuoteSortKey.FreqDesc:
                    switch (AppSettings.BrokerQuoteCategory)
                    {
                        default:
                        case BrokerQuoteCategory.Total:
                            lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.TotalFreq).ToList();
                            break;
                        case BrokerQuoteCategory.Buy:
                            lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.BuyFreq).ToList();
                            break;
                        case BrokerQuoteCategory.Sell:
                            lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.SellFreq).ToList();
                            break;
                        case BrokerQuoteCategory.Net:
                            lstBrokerTmp = lstBrokerTmp.OrderByDescending(broker => broker.NetFreq).ToList();
                            break;
                    }
                    break;
            }

            foreach (Broker broker in lstBrokerTmp)
            {
                broker.RowNumber = lstBrokerTmp.IndexOf(broker) + 1;
            }

            lstBroker = lstBrokerTmp;
        }

        private void UpdateView()
        {
            if (lstBroker != null && lstBroker.Any())
            {
                BrokerListView.IsRefreshing = false;
                BrokerListView.ItemsSource = lstBroker;
            }
        }

        private void BrokerListView_Refreshing(object sender, EventArgs e)
        {
            _clientStateManager.StopClient();
            _clientStateManager.StartClient();
            BrokerListView.IsRefreshing = false;
        }

        private void pickerCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsVisible && AppSettings.BrokerQuoteCategory != (BrokerQuoteCategory)pickerCategory.SelectedIndex)
            {
                AppSettings.BrokerQuoteCategory = (BrokerQuoteCategory)pickerCategory.SelectedIndex;
                UpdateItemListSource();
                UpdateView();
            }
        }

        private void pickerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsVisible && AppSettings.BrokerQuoteMarketType != (BrokerQuoteMarketType)pickerType.SelectedIndex)
            {
                AppSettings.BrokerQuoteMarketType = (BrokerQuoteMarketType)pickerType.SelectedIndex;
                UpdateItemListSource();
                UpdateView();
            }
        }

        public void SortButton_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                SortKeyPicker.Focus();
            });
        }

        private void SortKeyPicker_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (SortKeyPicker.SelectedItem is string selectedItem &&
                SortKeys.TryGetValue(selectedItem, out BrokerQuoteSortKey brokerQuoteSortKey))
            {
                if (AppSettings.BrokerQuoteSortKey != brokerQuoteSortKey)
                {
                    AppSettings.BrokerQuoteSortKey = brokerQuoteSortKey;
                    UpdateItemListSource();
                    UpdateView();
                }
            }
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}