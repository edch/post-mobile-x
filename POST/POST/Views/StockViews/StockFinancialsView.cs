﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.ApiResponses;
using POST.Dialogs;
using POST.Models;
using POST.Services;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace POST.Views.StockViews
{
    public class StockFinancialsView : ContentView, IContentView_Stock
    {

        public static BindableProperty StockIdProperty = BindableProperty.Create("StockId", typeof(StockId?), typeof(StockFinancialsView), defaultValue: null);
        public StockId? StockId
        {
            get => (StockId?)GetValue(StockIdProperty);
            set => SetValue(StockIdProperty, value);
        }

        #region Field Definitions
        private static readonly IReadOnlyList<FieldDefinition> FieldDefinitions = new List<FieldDefinition> {
            new FieldDefinition(" ", report => {
                string[] dateparts = report.Period.Split('|');
                int year = int.Parse(dateparts[0]);
                int quarter = int.Parse(dateparts[1]) / 3;
                return year * 10 + quarter;
            }, FieldFormat.Quarter, isSectionHeader: true),
            new FieldDefinition("DESCRIPTIVE"),
            new FieldDefinition("Last Price", report => report.Data.LastPrice, FieldFormat.IDR),
            new FieldDefinition("Outstanding Shares", report => report.Data.OutstandingShares_Million, FieldFormat.MillionShares),
            new FieldDefinition("Market Capitalization", report => report.Data.MarketCapitalization_Million, FieldFormat.MillionIDR),

            new FieldDefinition("BALANCE SHEET"),
            new FieldDefinition("Total Asset", report => report.Data.TotalAsset_Million, FieldFormat.MillionIDR),
            new FieldDefinition("Total Liabilities", report => report.Data.TotalLiabilities_Million, FieldFormat.MillionIDR),
            new FieldDefinition("Total Equity", report => report.Data.TotalEquity_Million, FieldFormat.MillionIDR),
            new FieldDefinition("Net Debt", report => report.Data.NetDebt_Million, FieldFormat.MillionIDR),

            new FieldDefinition("INCOME STATEMENT"),
            new FieldDefinition("Net Interest Income", report => report.Data.NetInterestIncome_Million, FieldFormat.MillionIDR, false,FieldRestriction.FinanceOnly),
            new FieldDefinition("Income After Provision", report => report.Data.IncomeAfterProvision_Million, FieldFormat.MillionIDR, false,FieldRestriction.FinanceOnly),
            new FieldDefinition("Sales", report => report.Data.SalesRevTurn_Million, FieldFormat.MillionIDR, false,FieldRestriction.NonFinanceOnly),
            new FieldDefinition("Gross Profit", report => report.Data.GrossProfit_Million, FieldFormat.MillionIDR, false,FieldRestriction.NonFinanceOnly),
            new FieldDefinition("Operating Profit", report => report.Data.OperatingProfit_Million, FieldFormat.MillionIDR),
            new FieldDefinition("Pretax Income", report => report.Data.PretaxIncome_Million, FieldFormat.MillionIDR, false,FieldRestriction.FinanceOnly),
            new FieldDefinition("EBITDA", report => report.Data.Ebitda_Million, FieldFormat.MillionIDR, false,FieldRestriction.NonFinanceOnly),
            new FieldDefinition("Net Profit", report => report.Data.NetProfit_Million, FieldFormat.MillionIDR),

            new FieldDefinition("CASH FLOW"),
            new FieldDefinition("Operating Cash Flow", report => report.Data.OperatingCashFlow_Million, FieldFormat.MillionIDR),
            new FieldDefinition("Free Cash Flow", report => report.Data.FreeCashFlow_Million, FieldFormat.MillionIDR),
            new FieldDefinition("Book Value per Share", report => report.Data.BookValuePerShare, FieldFormat.IDRPerShare),

            new FieldDefinition("EARNINGS"),
            new FieldDefinition("Earnings per Share", report => report.Data.EarningsPerShare, FieldFormat.IDRPerShare),
            new FieldDefinition("Annualized", report => report.Data.EarningsPerShareAnnualized, FieldFormat.IDRPerShare),

            new FieldDefinition("DEBT RATIO"),
            new FieldDefinition("Loan to Deposit Ratio", report => report.Data.LoanToDepositRatio_Percent, FieldFormat.Percentage, fieldRestriction: FieldRestriction.FinanceOnly),
            new FieldDefinition("Non-performing Loans", report => report.Data.NonPerformingLoans_Percent, FieldFormat.Percentage, fieldRestriction: FieldRestriction.FinanceOnly),
            new FieldDefinition("Debt to Equity", report => report.Data.DebtEquityRatio_Percent, FieldFormat.Percentage, fieldRestriction: FieldRestriction.NonFinanceOnly),
            new FieldDefinition("Gearing Ratio", report => report.Data.GearingRatio_Percent, FieldFormat.Percentage, fieldRestriction: FieldRestriction.NonFinanceOnly),

            new FieldDefinition("VALUATION"),
            new FieldDefinition("Price to Earnings Ratio", report => report.Data.PriceToEarningsRatio, FieldFormat.Ratio),
            new FieldDefinition("Price to Book Value Ratio", report => report.Data.PriceToBookValueRatio, FieldFormat.Ratio),

            new FieldDefinition("MARGIN"),
            new FieldDefinition("Gross Margin", report => report.Data.GrossMargin_Percent, FieldFormat.Percentage),
            new FieldDefinition("Operating Margin", report => report.Data.OperatingMargin_Percent, FieldFormat.Percentage),
            new FieldDefinition("Net Interest Margin", report => report.Data.NetInterestMargin_Percent, FieldFormat.Percentage,false,FieldRestriction.NonFinanceOnly),
            new FieldDefinition("Net Interest Margin", report => report.Data.T12NetInterestMargin_Percent, FieldFormat.Percentage, false,FieldRestriction.FinanceOnly),

            new FieldDefinition("PROFITABILITY"),
            new FieldDefinition("Return on Asset", report => report.Data.ReturnOnAsset_Percent, FieldFormat.Percentage),
            new FieldDefinition("Return on Equity", report => report.Data.ReturnOnEquity_Percent, FieldFormat.Percentage),

            new FieldDefinition("LEVERAGE"),
            new FieldDefinition("Loan Loss Coverage", report => report.Data.LoanLossCoverage_Percent, FieldFormat.Percentage, fieldRestriction: FieldRestriction.FinanceOnly),
            new FieldDefinition("Capital Adequacy Ratio", report => report.Data.CapitalAdequacyRatio_Percent, FieldFormat.Percentage, fieldRestriction: FieldRestriction.FinanceOnly),
            new FieldDefinition("Interest Coverage Ratio", report => report.Data.InterestCoverageRatio_Percent, FieldFormat.Percentage, fieldRestriction: FieldRestriction.NonFinanceOnly),
            new FieldDefinition("Net Debt to EBITDA", report => report.Data.NetDebtToEBITDA_Percent, FieldFormat.Percentage, fieldRestriction: FieldRestriction.NonFinanceOnly),
        };

        #endregion

        private FinancialsComparison CurrentFinancialsComparison;
        private Financials CurrentFinancials;
        private Grid RowHeaders;
        private readonly Picker QuarterPicker;

        private Grid ToHeader(QuarterlyFinancialReport report)
        {
            Grid grid = new Grid
            {
                Margin = new Thickness(0, 3, 0, 0)
            };
            FieldDefinition fieldDefinition = FieldDefinitions.First();
            grid.Children.Add(new Label
            {
                Text = fieldDefinition.GetText(report),
                TextColor = fieldDefinition.GetTextColor(report),
                FontFamily = (OnPlatform<string>)App.Current.Resources[fieldDefinition.IsSectionHeader ? "DINProBold" : "DINProMedium"],
                FontSize = 11,
                HorizontalTextAlignment = TextAlignment.End,
                VerticalTextAlignment = TextAlignment.Center,
                LineBreakMode = LineBreakMode.TailTruncation,
                Margin = fieldDefinition.IsSectionHeader ? new Thickness(0, 3, 5, -3) : new Thickness(0, 0, 5, 0)
            });
            return grid;
        }

        private Grid ToColumn(QuarterlyFinancialReport report, FieldRestriction? fieldRestriction)
        {
            Grid grid = new Grid
            {
                Margin = new Thickness(0, 3, 0, 0)
            };
            foreach (FieldDefinition fieldDefinition in FieldDefinitions)
            {
                if ((fieldRestriction == FieldRestriction.FinanceOnly && fieldDefinition.IsForFinanceCompany) ||
                    (fieldRestriction == FieldRestriction.NonFinanceOnly && fieldDefinition.IsForNonFinanceCompany) ||
                    (fieldRestriction == null && fieldDefinition.IsCompanyNeutral))
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = fieldDefinition.IsSectionHeader ? 27 : 22 });
                    grid.Children.Add(new Label
                    {
                        Text = fieldDefinition.GetText(report),
                        TextColor = FieldDefinitions.First() == fieldDefinition ? Color.Transparent : fieldDefinition.GetTextColor(report),
                        FontFamily = (OnPlatform<string>)App.Current.Resources[fieldDefinition.IsSectionHeader ? "DINProBold" : "DINProMedium"],
                        FontSize = 11,
                        HorizontalTextAlignment = TextAlignment.End,
                        VerticalTextAlignment = TextAlignment.Center,
                        LineBreakMode = LineBreakMode.TailTruncation,
                        Margin = fieldDefinition.IsSectionHeader ? new Thickness(0, 3, 5, -3) : new Thickness(0, 0, 5, 0)
                    }, 0, grid.Children.Count);
                }
            }
            return grid;
        }

        private void SetupRowHeaders(FieldRestriction? fieldRestriction)
        {
            IEnumerable<FieldDefinition> fieldDefinitions;
            if (fieldRestriction == FieldRestriction.FinanceOnly)
            {
                fieldDefinitions = from f in FieldDefinitions
                                   where f.IsForFinanceCompany
                                   select f;
            }
            else if (fieldRestriction == FieldRestriction.NonFinanceOnly)
            {
                fieldDefinitions = from f in FieldDefinitions
                                   where f.IsForNonFinanceCompany
                                   select f;
            }
            else
            {
                fieldDefinitions = from f in FieldDefinitions
                                   where f.IsCompanyNeutral
                                   select f;
            }
            foreach (FieldDefinition fieldDefinition in fieldDefinitions)
            {
                // Add the row definition before adding any child into that particular row index
                RowHeaders.RowDefinitions.Add(new RowDefinition { Height = fieldDefinition.IsSectionHeader ? 27 : 22 });
                // Add a new label to the last row of RowHeaders Grid
                string strRowHeader = fieldDefinition.HeaderText;
                if (strRowHeader == "Annualized")
                {
                    strRowHeader = "Earnings per Share T12M";
                }

                RowHeaders.Children.Add(new Label
                {
                    Text = strRowHeader,
                    FontFamily = (OnPlatform<string>)App.Current.Resources[fieldDefinition.IsSectionHeader ? "DINProBold" : "DINProLight"],
                    FontSize = 11,
                    HorizontalTextAlignment = TextAlignment.Start,
                    VerticalTextAlignment = TextAlignment.Center,
                    LineBreakMode = LineBreakMode.TailTruncation,
                    Margin = fieldDefinition.IsSectionHeader ? new Thickness(5, 3, 0, -3) : new Thickness(5, 0, 0, 0),
                    TextColor = fieldDefinition.IsSectionHeader ? Color.Accent : Colors.FieldValueDefaultColor
                }, 0, RowHeaders.Children.Count);
            }
        }

        private View CreateView(Picker quarterPicker, Financials stockFinancials)
        {

            /*  VerticalScrollView (VSV)
			 *  ______________________________________________________
			 *  |                                                    |
			 *  | :<------------ HorizontalScrollView (HSV) ------>: |
			 *  | :              (ColSpan = 2)                     : |
			 *  | : RowHeaders                                     : |
			 *  | :________________________________________________: |
			 *  | /___________/|                                   | |
			 *  | |           || ColumnsGrid                       | |
			 *  | | (child    || _________________________________ | |
			 *  | |  of VSV   || |                               | | |
			 *  | |  instead  || | Column       Column       ... | | |
			 *  | |  of HSV)  || | ___________  ___________      | | |
			 *  | |           || | |         |  |         |      | | |
			 *  | | [Label  ] || | | [Label] |  | [Label] |      | | |
			 *  | | [Label  ] || | | [Label] |  | [Label] |      | | |
			 *  | |           || | |         |  |         |      | | |
			 *  | |  ...      || | |  ...    |  |  ...    |      | | |
			 *  | |           || | |_________|  |_________|      | | |
			 *  | |           || |_______________________________| | |
			 *  | |           ||___________________________________| |
			 *  | |___________|/                                     |
			 *  |____________________________________________________|
			 */


            Grid headerGrid = new Grid();

            // ColumnsGrid will be placed inside HorizontalScrollView, which spans across 2 columns: 140px + Auto.
            // Therefore, the left 140px portion of this Grid will initially be behind ColumnHeaders
            // and 140px left margin is required to make the content initially visible
            Grid columnsGrid = new Grid
            {
                Margin = new Thickness(140, 0, 0, 0)
            };

            // This ScrollView handles horizontal scrolling
            ScrollView horizontalScrollView = new ScrollView
            {
                HorizontalOptions = LayoutOptions.Fill,
                HorizontalScrollBarVisibility = ScrollBarVisibility.Never,
                VerticalOptions = LayoutOptions.Fill,
                VerticalScrollBarVisibility = ScrollBarVisibility.Never,
                Orientation = ScrollOrientation.Horizontal,
                Content = columnsGrid
            };

            // The frozen, left-most column containing field name labels
            RowHeaders = new Grid
            {
                BackgroundColor = (Color)App.Current.Resources["PageBackgroundColor"]
            };
            if (stockFinancials != null)
            {
                FieldRestriction? fieldRestriction;
                bool? isFinanceCompany = stockFinancials?.Reports?.FirstOrDefault()?.IsFinanceCompany;
                if (isFinanceCompany == true)
                {
                    fieldRestriction = FieldRestriction.FinanceOnly;
                }
                else if (isFinanceCompany == false)
                {
                    fieldRestriction = FieldRestriction.NonFinanceOnly;
                }
                else
                {
                    fieldRestriction = null;
                }
                SetupRowHeaders(fieldRestriction);

                if (stockFinancials.Reports != null)
                {
                    foreach (QuarterlyFinancialReport report in stockFinancials.Reports)
                    {
                        headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(80) });
                        headerGrid.Children.Add(ToHeader(report), headerGrid.Children.Count, 0);
                        columnsGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(80) });
                        columnsGrid.Children.Add(ToColumn(report, fieldRestriction), columnsGrid.Children.Count, 0);
                    }
                }
            }

            // This is the direct descendant to root ScrollView
            // It is split into two columns:
            // - Left column (140px wide) for frozen RowHeaders
            // - Right column (takes up all remaining horizontal space) for scrollable part of the table
            Grid tableGrid = new Grid
            {
                ColumnDefinitions = {
                    new ColumnDefinition { Width = 140 },
                    new ColumnDefinition { Width = GridLength.Star }
                }
            };

            // Add HorizontalScrollView to TableGrid spanning across 2 columns
            tableGrid.Children.Add(horizontalScrollView, 0, 0);
            Grid.SetColumnSpan(horizontalScrollView, 2);

            // Add RowHeaders to the first column of TableGrid
            tableGrid.Children.Add(RowHeaders, 0, 0);

            // Root element of the layout, responsible for handling vertical scroll
            ScrollView verticalScrollView = new ScrollView
            {
                HorizontalOptions = LayoutOptions.Fill,
                HorizontalScrollBarVisibility = ScrollBarVisibility.Never,
                VerticalOptions = LayoutOptions.FillAndExpand,
                VerticalScrollBarVisibility = ScrollBarVisibility.Default,
                Orientation = ScrollOrientation.Vertical,
                BackgroundColor = (Color)App.Current.Resources["PageBackgroundColor"],
                Content = tableGrid
            };

            Grid rootGrid = new Grid
            {
                BackgroundColor = (Color)App.Current.Resources["PageBackgroundColor"],

                RowDefinitions = {

                    new RowDefinition { Height = 22 },

                    new RowDefinition { Height = GridLength.Star },

                    new RowDefinition { Height = 40}

                }
            };

            ScrollView headerScrollView = new ScrollView
            {
                HorizontalOptions = LayoutOptions.Fill,
                HorizontalScrollBarVisibility = ScrollBarVisibility.Never,
                VerticalOptions = LayoutOptions.Fill,
                VerticalScrollBarVisibility = ScrollBarVisibility.Never,
                Orientation = ScrollOrientation.Horizontal,
                Content = headerGrid,
                Margin = new Thickness(140, 0, 0, 0),
                InputTransparent = true
            };

            rootGrid.Children.Add(verticalScrollView, 0, 0);
            Grid.SetRowSpan(verticalScrollView, 2);
            rootGrid.Children.Add(headerScrollView, 0, 0);

            rootGrid.Children.Add(quarterPicker);

            // Setting RowHeaders.InputTransparent to true allow touch input to go through RowHeaders,
            // enabling user to start swiping right anywhere on the screen even if the initial
            // touch position was over the RowHeaders grid.
            // Setting RowHeaders.InputTransparent to false, in other hand, will block the scroll touch
            // gesture from reaching the HorizontalScrollView, thus preventing conflicting gesture
            // recognition between horizontal scrolling and paging
            horizontalScrollView.Scrolled += async (sender, args) =>
            {
                if (args.ScrollX != 0)
                {
                    // When grid is scrolled away from its leftmost edge, enable RowHeaders'
                    // input transparency
                    RowHeaders.InputTransparent = true;
                    RowHeaders.BackgroundColor = Color.FromRgba(0x0f, 0x12, 0x19, 0xee);
                }
                else
                {
                    // When grid is scrolled to its leftmost edge, disable RowHeaders' input
                    // transparency
                    RowHeaders.InputTransparent = false;
                    RowHeaders.BackgroundColor = (Color)App.Current.Resources["PageBackgroundColor"];
                }
                await headerScrollView.ScrollToAsync(args.ScrollX, 0.0, false);
            };

            verticalScrollView.Scrolled += (sender, args) =>
            {
                if (args.ScrollY != 0)
                {
                    headerGrid.BackgroundColor = (Color)App.Current.Resources["PageBackgroundColor"];
                }
                else
                {
                    headerGrid.BackgroundColor = Color.Transparent;
                }
            };

            Grid buySellGrid = new Grid
            {
                Padding = new Thickness(4, 0, 4, 4),
                ColumnDefinitions = {
                    new ColumnDefinition(),
                    new ColumnDefinition()
                },
                BackgroundColor = (Color)App.Current.Resources["PageBackgroundColor"]
            };
            Grid.SetRow(buySellGrid, 2);

            Button BuyButton = new Button()
            {
                Text = "BUY",
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProBold"],
                BackgroundColor = Colors.DangerColor,
                TextColor = Colors.FieldValueDefaultColor
            };
            BuyButton.Clicked += BuyButton_Clicked;
            buySellGrid.Children.Add(BuyButton, 0, 0);

            Button SellButton = new Button()
            {
                Text = "SELL",
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProBold"],
                BackgroundColor = Colors.SuccessColor,
                TextColor = Colors.FieldValueDefaultColor
            };
            SellButton.Clicked += SellButton_Clicked;
            buySellGrid.Children.Add(SellButton, 1, 0);
            rootGrid.Children.Add(buySellGrid);

            return rootGrid;
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)

        {

            if (StockId == null)
            {
                return;
            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)

            {

                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());

            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&

                stockById.TryGetValue(StockId.Value, out Stock stock) &&

                (stock.BestBidPrice ?? stock.LastOrPrevious).HasValue &&

                stock.SharesPerLot.HasValue)

            {

                OrderDialog orderDialog = new OrderDialog(

                    action: OrderAction.Buy,

                    stockId: StockId.Value,

                    openPrice: stock.OpenOrPrevious.Value,

                    price: stock.BestBidPrice ?? stock.LastOrPrevious.Value,

                    priceUnsure: stock.BestBidPrice == null,

                    sharesPerLot: stock.SharesPerLot.Value

                );

                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await Navigation.PushPopupAsync(orderDialog);

            }

            else

            {

                DependencyService.Get<IToastMessage>().Show("Can't place order right now.");

            }

        }

        private async void SellButton_Clicked(object sender, EventArgs e)

        {

            if (StockId == null)
            {
                return;
            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)

            {

                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());

            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&

                stockById.TryGetValue(StockId.Value, out Stock stock) &&

                (stock.BestOfferPrice ?? stock.LastOrPrevious).HasValue &&

                stock.SharesPerLot.HasValue)

            {

                OrderDialog orderDialog = new OrderDialog(

                    action: OrderAction.Sell,

                    stockId: StockId.Value,

                    openPrice: stock.OpenOrPrevious.Value,

                    price: stock.BestOfferPrice ?? stock.LastOrPrevious.Value,

                    priceUnsure: stock.BestOfferPrice == null,

                    sharesPerLot: stock.SharesPerLot.Value

                );

                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await Navigation.PushPopupAsync(orderDialog);

            }

            else

            {

                DependencyService.Get<IToastMessage>().Show("Can't place order right now.");

            }

        }

        private Picker CreateQuarterPicker()
        {
            Picker quarterPicker = new Picker
            {
                Title = "Compare",
                IsVisible = false,
                FontFamily = (OnPlatform<string>)App.Current.Resources["ProximaNovaReg"],
                FontSize = 15,
                BackgroundColor = Color.Black,
                Items = {
                    "Q1",
                    "Q2",
                    "Q3",
                    "Q4",
                    "Every Quarter"
                }
            };
            quarterPicker.SelectedIndexChanged += async (sender, args) =>
            {
                if (!quarterPicker.IsEnabled)
                {
                    return;
                }

                try
                {
                    quarterPicker.IsEnabled = false;
                    switch ((string)quarterPicker.SelectedItem)
                    {
                        case "Q1":
                            await LoadDataAsync(FinancialsComparison.Q1);
                            break;
                        case "Q2":
                            await LoadDataAsync(FinancialsComparison.Q2);
                            break;
                        case "Q3":
                            await LoadDataAsync(FinancialsComparison.Q3);
                            break;
                        case "Q4":
                            await LoadDataAsync(FinancialsComparison.Q4);
                            break;
                        case "Every Quarter":
                            await LoadDataAsync(FinancialsComparison.Quarterly);
                            break;
                        default:
                            return;
                    }
                }
                finally
                {
                    quarterPicker.IsEnabled = true;
                }
            };
            return quarterPicker;
        }

        public StockFinancialsView()
        {
            QuarterPicker = CreateQuarterPicker();
            CurrentFinancialsComparison = FinancialsComparison.Quarterly;
            //Content = CreateView(QuarterPicker, null);
            //ToolbarItem quarterSelectorItem = new ToolbarItem { Text = "\uf073" };
            //quarterSelectorItem.Clicked += (sender, args) => {
            //    QuarterPicker.Focus();
            //};
            //ToolbarItems.Add(quarterSelectorItem);
        }

        public View CreateProgressDisplay()
        {
            Grid gridProgress = new Grid { BackgroundColor = (Color)App.Current.Resources["PageBackgroundColor"], Padding = new Thickness(50) };
            gridProgress.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            gridProgress.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            gridProgress.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //gridProgress.SetBinding(VisualElement.IsVisibleProperty, "IsWorking");
            ActivityIndicator activity = new ActivityIndicator
            {
                IsEnabled = true,
                IsVisible = true,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                IsRunning = true
            };
            gridProgress.Children.Add(activity, 0, 1);

            return gridProgress;
        }

        public void QuarterSelectorItem_Clicked(object sender, EventArgs e)
        {
            QuarterPicker.Focus();
        }

        private async Task LoadDataAsync(FinancialsComparison financialsComparison)
        {
            //if (financialsComparison == CurrentFinancialsComparison && CurrentFinancials != null) return;
            try
            {
                Content = CreateProgressDisplay();
                Financials stockFinancials = await FinancialsClient.Default.GetFinancialsAsync(StockId.Value.Code, financialsComparison, CancellationToken.None);

                Thread th = new Thread(new ParameterizedThreadStart(CreateViewThread))
                {
                    IsBackground = true
                };
                th.Start(stockFinancials);

                CurrentFinancialsComparison = financialsComparison;
                CurrentFinancials = stockFinancials;
            }
            catch
            {
                // TODO: show error
                return;
            }
        }

        private void CreateViewThread(object obj)
        {
            Financials stockFinancials = (Financials)obj;
            View viewTmp = CreateView(QuarterPicker, stockFinancials);
            Device.BeginInvokeOnMainThread(() => Content = viewTmp);

        }

        public async void OnAppearing()
        {
            await LoadDataAsync(CurrentFinancialsComparison);
        }

        public void OnDisappearing()
        {
            return;
        }
        public void SetStock(StockId? stockId)
        {
            StockId = stockId;
        }

        private enum FieldFormat
        {
            Blank,
            Quarter,
            IDR,
            Shares,
            MillionIDR,
            MillionShares,
            IDRPerShare,
            Ratio,
            Percentage
        }

        private enum FieldRestriction
        {
            FinanceOnly,
            NonFinanceOnly
        }

        private class FieldDefinition
        {
            public string HeaderText { get; }
            public bool IsSectionHeader { get; }
            private readonly Func<QuarterlyFinancialReport, decimal?> _valueSelector;
            private readonly FieldFormat _fieldFormat;
            private readonly FieldRestriction? _fieldRestriction;

            public FieldDefinition(string headerText)
            {
                HeaderText = headerText ?? throw new ArgumentNullException(nameof(headerText));
                IsSectionHeader = true;
                _valueSelector = report => 0;
                _fieldFormat = FieldFormat.Blank;
                _fieldRestriction = null;
            }

            public FieldDefinition(string headerText, Func<QuarterlyFinancialReport, decimal> valueSelector, FieldFormat fieldFormat, bool isSectionHeader = false, FieldRestriction? fieldRestriction = null)
            {
                HeaderText = headerText ?? throw new ArgumentNullException(nameof(headerText));
                IsSectionHeader = isSectionHeader;
                if (valueSelector == null)
                {
                    throw new ArgumentNullException(nameof(valueSelector));
                }

                _valueSelector = report => valueSelector(report);
                _fieldFormat = fieldFormat;
                _fieldRestriction = fieldRestriction;
            }

            public FieldDefinition(string headerText, Func<QuarterlyFinancialReport, decimal?> valueSelector, FieldFormat fieldFormat, bool isSectionHeader = false, FieldRestriction? fieldRestriction = null)
            {
                HeaderText = headerText ?? throw new ArgumentNullException(nameof(headerText));
                IsSectionHeader = isSectionHeader;
                _valueSelector = valueSelector ?? throw new ArgumentNullException(nameof(valueSelector));
                _fieldFormat = fieldFormat;
                _fieldRestriction = fieldRestriction;
            }

            public bool IsCompanyNeutral => _fieldRestriction == null;
            public bool IsForFinanceCompany => _fieldRestriction == null || _fieldRestriction.Value == FieldRestriction.FinanceOnly;
            public bool IsForNonFinanceCompany => _fieldRestriction == null || _fieldRestriction.Value == FieldRestriction.NonFinanceOnly;

            public string GetText(QuarterlyFinancialReport report)
            {
                decimal? value = _valueSelector(report);
                switch (_fieldFormat)
                {
                    case FieldFormat.Blank:
                        return " ";
                    case FieldFormat.Quarter:
                        return $"Q{(int)value.Value % 10} {(int)value.Value / 10}";
                    case FieldFormat.IDR:
                        return value.ToFinancialText();
                    case FieldFormat.Shares:
                        //return $"{value.ToFinancialText()} Shares";
                        return value.ToFinancialText();
                    case FieldFormat.MillionIDR:
                        return value.ToFinancialMillionText();
                    case FieldFormat.MillionShares:
                        //return $"{value.ToFinancialMillionText()} Shares";
                        return value.ToFinancialMillionText();
                    case FieldFormat.IDRPerShare:
                        return value.ToFinancialTextWithDecimals();
                    case FieldFormat.Ratio:
                        return value.ToFinancialRatioText();
                    case FieldFormat.Percentage:
                        return value.ToFinancialPercentageText();
                    default:
                        throw new NotImplementedException();
                }
            }

            public Color GetTextColor(QuarterlyFinancialReport report)
            {
                if (IsSectionHeader)
                {
                    return Colors.Accent;
                }

                decimal? value = _valueSelector(report);
                return value.ToFinancialTextColor();
            }
        }
    }

    public static class StockFinancialsDecimalExtensions
    {

        public static string ToFinancialMillionText(this decimal? valueInMillions)
        {
            return valueInMillions.HasValue ? (
valueInMillions.Value < 0 ? $"({valueInMillions.Value:N0} M)" :
$"{valueInMillions.Value:N0} M"
) : "-";
        }

        public static string ToFinancialText(this decimal? value)
        {
            return value.HasValue ? (
value.Value < 0 ? $"({value.Value:N0})" :
$"{value.Value:N0}"
) : "-";
        }

        public static string ToFinancialTextWithDecimals(this decimal? value)
        {
            return value.HasValue ? (
value.Value < 0 ? $"({value.Value:N})" :
$"{value.Value:N}"
) : "-";
        }

        public static string ToFinancialRatioText(this decimal? value)
        {
            return value.HasValue ? (
value.Value < 0 ? $"({value.Value:N2}) x" :
$"{value.Value:N2} x"
) : "-";
        }

        public static string ToFinancialPercentageText(this decimal? valueInPercent)
        {
            return valueInPercent.HasValue ? (
valueInPercent.Value < 0 ? $"({valueInPercent.Value:N2}) %" :
$"{valueInPercent.Value:N2} %"
) : "-";
        }

        public static Color ToFinancialTextColor(this decimal? value)
        {
            return value.HasValue ? (
value < 0 ? Colors.LoseForeground : (
value > 0 ? Colors.FieldValueDefaultColor :
Color.Gray)) :
Color.Gray;
        }
    }
}