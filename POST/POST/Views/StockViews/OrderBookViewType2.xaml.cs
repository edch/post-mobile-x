﻿using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Dialogs;
using POST.Models;
using POST.Stores;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Views.StockViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderBookViewType2 : ContentView, IContentView_Stock
    {
        public static readonly BindableProperty StockIdProperty = BindableProperty.Create("StockId", typeof(StockId?), typeof(StockOverviewView), defaultValue: null);
        public StockId? StockId
        {
            get => (StockId?)GetValue(StockIdProperty);
            set => SetValue(StockIdProperty, value);
        }

        private class ClientState
        {
            public ThrottledDispatcher<OrderBook, OrderBookMessage> ThrottledDispatcher { get; set; }
        }

        private DataFeedClientStateManager<ClientState> _clientStateManager;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public OrderBookViewType2()
        {
            InitializeComponent();

            if (AppSettings.LayoutHelp_OrderBook == "1")
            {
                layoutHelp.IsVisible = true;

                imgTopOverlay.GestureRecognizers.Add(new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        layoutHelp.IsVisible = false;
                        if (switchNeverShowAgain.IsToggled)
                        {
                            AppSettings.LayoutHelp_OrderBook = "0";
                        }
                    })
                });

                imgNeverShowAgain.GestureRecognizers.Add(new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        switchNeverShowAgain.IsToggled = !switchNeverShowAgain.IsToggled;
                    })
                });
            }
        }

        public void OnAppearing()
        {
            IsVisible = true;

            imgPointFinger.TranslationX = FrameBidPriceHelp.X;
            imgPointFinger.TranslationY = FrameBidPriceHelp.Y + 48;

            imgTapToBuySell.TranslationX = imgPointFinger.X + 100;
            imgTapToBuySell.TranslationY = imgPointFinger.Y + 80;

            Store.OrderBook.Clear(StockId.Value);
            if (Store.StockById.GetState().TryGetValue(StockId.Value, out Stock stock) && stock.Previous.HasValue)
            {
                Store.OrderBook.SetPrevious(stock.Previous.Value);
            }

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState
                {
                    ThrottledDispatcher = new ThrottledDispatcher<OrderBook, OrderBookMessage>(
                        store: Store.OrderBook,
                        actionDispatcher: messages =>
                        {
                            //if (!Store.OrderBook.GetState().Any())
                            //{
                            //    Device.BeginInvokeOnMainThread(async () =>
                            //    {
                            //        await Task.Delay(500);
                            //        OrderBook orderBook = Store.OrderBook.GetState();

                            //        if (orderBook.BestBid != null)
                            //        {
                            //            OrderBookListView.ScrollTo(orderBook.Single(level => level.Price == orderBook.BestBid), ScrollToPosition.Center, true);
                            //        }
                            //        else if (orderBook.BestAsk != null)
                            //        {
                            //            OrderBookListView.ScrollTo(orderBook.Single(level => level.Price == orderBook.BestAsk), ScrollToPosition.Center, true);
                            //        }
                            //    });
                            //}
                            OrderBookListViewType2.IsRefreshing = false;
                            return Store.OrderBook.BatchUpdate(messages);
                        },
                        coalesceInterval: TimeSpan.FromSeconds(1),
                        flushInterval: TimeSpan.FromSeconds(2)
                    )
                },
                clientFactory: state =>
                {
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        if (StockId.HasValue)
                        {
                            OrderBookListViewType2.IsRefreshing = true;
                            await sender.SendAsync(new SubscribeOrderBookRequest(StockId.Value));
                        }
                    };
                    client.DataReceived += (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case OrderBookMessage orderBookMessage:
                                state.ThrottledDispatcher.Dispatch(orderBookMessage);
                                break;
                            default:
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
                );

            _clientStateManager.StartClient();

            Device.StartTimer(TimeSpan.FromSeconds(2), () =>
            {
                if (OrderBookListViewType2.IsRefreshing)
                {
                    OrderBookListViewType2.IsRefreshing = false;
                }
                return false;
            }
            );

            _observers.Add(Store.StockById
               .Select(stockById => StockId.HasValue && stockById.TryGetValue(StockId.Value, out Stock stock2) ? stock2 : null)
               .Subscribe(stock2 =>
                   {
                       Store.OrderBook.SetLastPrice(stock2.LastOrPrevious.Value);
                       Store.OrderBook.SetPrevious(stock2.Previous.Value);
                   }
               )
           );

            _observers.Add(Store.OrderBook
                .Subscribe(orderBook =>
                {
                    OrderBookListViewType2.ItemsSource = orderBook.GetListOrderBookLevelType2();
                    if (orderBook.BidVolSum.HasValue)
                    {
                        lblBidVolSum.Text = orderBook.BidVolSum.Value.ToString("N0");
                    }
                    if (orderBook.AskVolSum.HasValue)
                    {
                        lblAskVolSum.Text = orderBook.AskVolSum.Value.ToString("N0");
                    }
                    if (orderBook.BidFreqSum.HasValue)
                    {
                        lblBidFreqSum.Text = orderBook.BidFreqSum.Value.ToString("N0");
                    }
                    if (orderBook.AskFreqSum.HasValue)
                    {
                        lblAskFreqSum.Text = orderBook.AskFreqSum.Value.ToString("N0");
                    }
                }
                )
            );
        }

        public void OnDisappearing()
        {
            IsVisible = false;

            _clientStateManager.StopClient();

            _clientStateManager = null;

            Store.OrderBook.Clear(StockId.Value);

            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
        }

        public void SetStock(StockId? stockId)
        {
            if (StockId.HasValue)
            {
                Store.OrderBook.Clear(StockId.Value);
            }
            StockId = stockId;
        }

        private async void OrderBookLevelCellType2_BidPriceClicked(object sender, ViewCells.OrderButtonEventArgs e)
        {
            decimal price = e.Price.Value;
            OrderBookActionDialog orderBookActionDialog = new OrderBookActionDialog(price);
            orderBookActionDialog.SetStock(StockId);
            await PopupNavigation.Instance.PushAsync(orderBookActionDialog);
        }

        private async void OrderBookLevelCellType2_AskPriceClicked(object sender, ViewCells.OrderButtonEventArgs e)
        {
            decimal price = e.Price.Value;
            OrderBookActionDialog orderBookActionDialog = new OrderBookActionDialog(price);
            orderBookActionDialog.SetStock(StockId);
            await PopupNavigation.Instance.PushAsync(orderBookActionDialog);
        }
    }
}