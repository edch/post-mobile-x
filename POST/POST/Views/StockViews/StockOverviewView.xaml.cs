﻿using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Dialogs;
using POST.Models;
using POST.Pages.StockPages;
using POST.Services;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Views.StockViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StockOverviewView : ContentView, IContentView_Stock
    {
        public static BindableProperty StockIdProperty = BindableProperty.Create("StockId", typeof(StockId?), typeof(StockOverviewView), defaultValue: null);
        public StockId? StockId
        {
            get => (StockId?)GetValue(StockIdProperty);
            set => SetValue(StockIdProperty, value);
        }

        private class ClientState
        {
            public List<StockChartMessage> StockChartMessages { get; } = new List<StockChartMessage>();
        }

        private readonly DataFeedClientStateManager<ClientState> _clientStateManager;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        private StockChart StockChart;

        private static readonly IReadOnlyDictionary<string, ChartMode> ChartModes = new Dictionary<string, ChartMode> {
            { "Line", ChartMode.Line },
            { "Candlestick", ChartMode.Candlestick }
        };

        public StockOverviewView()
        {
            InitializeComponent();
            foreach (string chartModeKey in ChartModes.Keys)
            {
                ChartModePicker.Items.Add(chartModeKey);
            }

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState(),
                clientFactory: state =>
                {
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        if (StockId.HasValue)
                        {
                            state.StockChartMessages.Clear();
                            await sender.SendAsync(new StockChartInitRequest(StockId.Value.Code, StockChartTimeUnit.FiveMinutes));
                        }
                    };
                    client.DataReceived += (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case StockChartMessage stockChartMessage:
                                state.StockChartMessages.Add(stockChartMessage);
                                break;
                            case StockChartEndMessage stockChartEndMessage:
                                StockChart = new StockChart(StockId.Value.Code, StockChartTimeUnit.FiveMinutes);
                                foreach (StockChartMessage stockChartMessage in from message in state.StockChartMessages
                                                                                where message.Time.Date == state.StockChartMessages.Last().Time.Date
                                                                                select message)
                                {
                                    StockChart.Add(new StockChartTuple(
                                        time: stockChartMessage.Time,
                                        open: stockChartMessage.Open,
                                        high: stockChartMessage.High,
                                        low: stockChartMessage.Low,
                                        close: stockChartMessage.Close,
                                        volume: stockChartMessage.Volume
                                    ));
                                }
                                if (StockChart.Count > 0)
                                {
                                    CandlestickChart.IsVisible = true;
                                }

                                CandlestickChart.StockChart = StockChart;
                                CandlestickChart.InvalidateSurface();
                                state.StockChartMessages.Clear();
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
            );
        }

        public void OnAppearing()
        {
            //if (StockChart != null)
            //{
            //    CandlestickChart.StockChart = StockChart;
            //    //CandlestickChart.InvalidateSurface();
            //}

            CandlestickChart.IsVisible = false;

            _clientStateManager.StartClient();

            _observers.Add(Store.StockById
                .Select(stockById => StockId.HasValue && stockById.TryGetValue(StockId.Value, out Stock stock) ? stock : null)
                .Subscribe(stock =>
                {
                    //Debug.WriteLine("StockOverviewView: " + stock.Id.Code + " " + stock.Id.MarketType);                                        
                    UpdateView(stock);
                })
            );
        }

        public void OnDisappearing()
        {
            _clientStateManager.StopClient();

            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
        }

        private void UpdateView(Stock stock)
        {
            lblPrev.Text = stock.Previous.HasValue ? stock.Previous.Value.ToString("N0") : "";
            if (stock.OHLC.HasValue)
            {
                lblHigh.Text = stock.OHLC.Value.High.ToString("N0");
                lblLow.Text = stock.OHLC.Value.Low.ToString("N0");
            }
            else
            {
                lblHigh.Text = "";
                lblLow.Text = "";
            }
            if (stock.HasMagnitudes)
            {
                lblVol.Text = stock.Magnitudes.Volume.Value.ToString("N0");
                lblVal.Text = stock.Magnitudes.Value.ToString("N0");
                lblFreq.Text = stock.Magnitudes.Frequency.ToString("N0");
            }
            else
            {
                lblVol.Text = "";
                lblVal.Text = "";
                lblFreq.Text = "";
            }
        }

        public async void TechnicalChartButton_Clicked(object sender, EventArgs e)
        {
            if (StockId.HasValue)
            {
                TechnicalChartPage page = (TechnicalChartPage)Activator.CreateInstance(typeof(TechnicalChartPage));
                page.StockId = StockId.Value;
                page.Title = StockId.Value.Code;
                await MainPage.NavigationPage.PushAsync(page);
            }
        }

        public void ChartModeButton_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                ChartModePicker.Focus();
            });
        }

        public void ChartModePicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ChartModePicker.SelectedItem is string selectedItem &&
                ChartModes.TryGetValue(selectedItem, out ChartMode chartMode))
            {
                Store.ChartMode.SetChartMode(chartMode);
            }
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (StockId == null)
            {
                return;
            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                stockById.TryGetValue(StockId.Value, out Stock stock) &&
                (stock.BestBidPrice ?? stock.LastOrPrevious).HasValue &&
                stock.SharesPerLot.HasValue)
            {
                OrderDialog orderDialog = new OrderDialog(
                    action: OrderAction.Buy,
                    stockId: StockId.Value,
                    openPrice: stock.OpenOrPrevious.Value,
                    price: stock.BestBidPrice ?? stock.LastOrPrevious.Value,
                    priceUnsure: stock.BestBidPrice == null,
                    sharesPerLot: stock.SharesPerLot.Value
                );
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await Navigation.PushPopupAsync(orderDialog);
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
            }
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (StockId == null)
            {
                return;
            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                stockById.TryGetValue(StockId.Value, out Stock stock) &&
                (stock.BestOfferPrice ?? stock.LastOrPrevious).HasValue &&
                stock.SharesPerLot.HasValue)
            {
                OrderDialog orderDialog = new OrderDialog(
                    action: OrderAction.Sell,
                    stockId: StockId.Value,
                    openPrice: stock.OpenOrPrevious.Value,
                    price: stock.BestOfferPrice ?? stock.LastOrPrevious.Value,
                    priceUnsure: stock.BestOfferPrice == null,
                    sharesPerLot: stock.SharesPerLot.Value
                );
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await Navigation.PushPopupAsync(orderDialog);
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
            }
        }

        public void SetStock(StockId? stockId)
        {
            StockId = stockId;

            if (Store.StockById.GetState().TryGetValue(StockId.Value, out Stock stock))
            {
                CandlestickChart.Stock2 = stock;
            }

        }
    }
}