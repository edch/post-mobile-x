﻿using POST.DataSources;
using POST.DataSources.ApiResponses;
using POST.Dialogs;
using POST.Models;
using POST.Services;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Views.StockViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompanyProfileView : ContentView, IContentView_Stock
    {
        private CancellationTokenSource _cancellationTokenSource;

        public static BindableProperty StockIdProperty = BindableProperty.Create("StockId", typeof(StockId?), typeof(StockOverviewView), defaultValue: null);
        public StockId? StockId
        {
            get => (StockId?)GetValue(StockIdProperty);
            set => SetValue(StockIdProperty, value);
        }

        public CompanyProfileView()
        {
            InitializeComponent();
        }

        public void SetStock(StockId? stockId)
        {
            StockId = stockId;
        }

        public void OnAppearing()
        {
            RefreshPageAsync();
        }

        public void OnDisappearing()
        {
            if (!_cancellationTokenSource.IsCancellationRequested)
            {
                _cancellationTokenSource.Cancel();
            }
        }

        public void RefreshPageAsync()
        {
            _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
            LoadDataAsync(_cancellationTokenSource.Token);
        }

        private async void LoadDataAsync(CancellationToken cancellationToken)
        {
            CompanyProfile companyProfile = null;
            try
            {
                companyProfile = await CompanyProfileClient.Default.GetCompanyProfileAsync(StockId.Value.Code, cancellationToken);
            }
            catch (OperationCanceledException)
            {
                //DependencyService.Get<IToastMessage>().Show("Request timeout.");
            }
            catch
            {
                DependencyService.Get<IToastMessage>().Show("Request failed.");
            }
            finally
            {
                updateView(companyProfile);
            }
        }

        private void updateView(CompanyProfile companyProfile)
        {
            if (companyProfile != null && companyProfile.profiles != null)
            {
                lblMainBusiness.Text = companyProfile.profiles[0].KegiatanUsahaUtama;
                lblAddress.Text = companyProfile.profiles[0].Alamat;
                lblAddress.Text += "\n" + "Telp: " + companyProfile.profiles[0].Telepon + " Fax: " + companyProfile.profiles[0].Fax;
                lblAddress.Text += "\n" + "Email: " + companyProfile.profiles[0].Email + " Website: " + companyProfile.profiles[0].Website;
                lblSector.Text = companyProfile.profiles[0].Sektor;
                lblSubSector.Text = companyProfile.profiles[0].SubSektor;

                int heightRowList = 30;
                int i;

                ShareholderListView.ItemsSource = companyProfile.pemegangsaham.Where(pemegangsaham => pemegangsaham.Nama != "-");
                i = companyProfile.pemegangsaham.Where(pemegangsaham => pemegangsaham.Nama != "-").Count();
                ShareholderListView.HeightRequest = i * heightRowList;

                BocListView.ItemsSource = companyProfile.komisaris.Where(komisaris => komisaris.Nama != "-");
                i = companyProfile.komisaris.Where(komisaris => komisaris.Nama != "-").Count();
                BocListView.HeightRequest = i * heightRowList;

                BodListView.ItemsSource = companyProfile.direktur.Where(direktur => direktur.Nama != "-");
                i = companyProfile.direktur.Where(direktur => direktur.Nama != "-").Count();
                BodListView.HeightRequest = i * heightRowList;
            }
            else
            {
                lblMainBusiness.Text = "";
                lblAddress.Text = "";
                lblSector.Text = "";
                lblSubSector.Text = "";

                ShareholderListView.ItemsSource = null;
                ShareholderListView.HeightRequest = 60;

                BocListView.ItemsSource = null;
                BocListView.HeightRequest = 60;

                BodListView.ItemsSource = null;
                BodListView.HeightRequest = 60;

                DependencyService.Get<IToastMessage>().Show("No Data.");
            }
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (StockId == null)
            {
                return;
            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                stockById.TryGetValue(StockId.Value, out Stock stock) &&
                (stock.BestBidPrice ?? stock.LastOrPrevious).HasValue &&
                stock.SharesPerLot.HasValue)
            {
                OrderDialog orderDialog = new OrderDialog(
                    action: OrderAction.Buy,
                    stockId: StockId.Value,
                    openPrice: stock.OpenOrPrevious.Value,
                    price: stock.BestBidPrice ?? stock.LastOrPrevious.Value,
                    priceUnsure: stock.BestBidPrice == null,
                    sharesPerLot: stock.SharesPerLot.Value
                );
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await Navigation.PushPopupAsync(orderDialog);
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
            }
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (StockId == null)
            {
                return;
            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                stockById.TryGetValue(StockId.Value, out Stock stock) &&
                (stock.BestOfferPrice ?? stock.LastOrPrevious).HasValue &&
                stock.SharesPerLot.HasValue)
            {
                OrderDialog orderDialog = new OrderDialog(
                    action: OrderAction.Sell,
                    stockId: StockId.Value,
                    openPrice: stock.OpenOrPrevious.Value,
                    price: stock.BestOfferPrice ?? stock.LastOrPrevious.Value,
                    priceUnsure: stock.BestOfferPrice == null,
                    sharesPerLot: stock.SharesPerLot.Value
                );
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await Navigation.PushPopupAsync(orderDialog);
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
            }
        }
    }
}