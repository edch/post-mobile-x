﻿using POST.DataSources;
using POST.DataSources.ApiResponses;
using POST.Dialogs;
using POST.Models;
using POST.Services;
using POST.Stores;
using POST.ViewCells.CorporateAction;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Views.StockViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CorporateActionView : ContentView, IContentView_Stock
    {
        private CancellationTokenSource _cancellationTokenSource;

        public static BindableProperty StockIdProperty = BindableProperty.Create("StockId", typeof(StockId?), typeof(StockOverviewView), defaultValue: null);
        public StockId? StockId
        {
            get => (StockId?)GetValue(StockIdProperty);
            set => SetValue(StockIdProperty, value);
        }

        private static readonly IReadOnlyDictionary<string, CorporateActionType> CorporateActionTypeList = new Dictionary<string, CorporateActionType> {
            { "Cash Dividend", CorporateActionType.CASHDIVIDEND},
            { "RUPS", CorporateActionType.RUPS},
            { "Rights Issue", CorporateActionType.RIGHTSISSUE},
            { "Warrants", CorporateActionType.WARRANTS},
            { "Stock Split", CorporateActionType.SPLIT},
            { "Stock Reverse", CorporateActionType.REVERSE},
            { "Stock Dividend", CorporateActionType.STOCKDIVIDEND}
        };

        public CorporateActionView()
        {
            InitializeComponent();

            pickerAction.ItemsSource = CorporateActionTypeList.Keys.ToList();
        }

        public void SetStock(StockId? stockId)
        {
            StockId = stockId;
        }

        public void OnAppearing()
        {
            pickerAction.SelectedIndex = Convert.ToInt32(AppSettings.CorporateActionType);
            RefreshPageAsync();
        }

        public void OnDisappearing()
        {
            if (!_cancellationTokenSource.IsCancellationRequested)
            {
                _cancellationTokenSource.Cancel();
            }
        }

        public void RefreshPageAsync()
        {
            _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
            LoadDataAsync(_cancellationTokenSource.Token);
        }

        private async void LoadDataAsync(CancellationToken cancellationToken)
        {
            CorporateAction[] corporateAction = null;
            try
            {
                corporateAction = await CorporateActionClient.Default.GetCorporateActionAsync(StockId.Value.Code, cancellationToken);
            }
            catch (OperationCanceledException)
            {
                //DependencyService.Get<IToastMessage>().Show("Request timeout.");
            }
            catch
            {
                DependencyService.Get<IToastMessage>().Show("Request failed.");
            }
            finally
            {
                updateView(corporateAction);
            }
        }

        private void updateView(CorporateAction[] corporateAction)
        {
            if (corporateAction != null)
            {
                List<CorporateAction> lstCorporateAction;
                switch (AppSettings.CorporateActionType)
                {
                    case CorporateActionType.RUPS:
                        CorporateActionListView.ItemTemplate = new DataTemplate(() => new RupsViewCell());
                        CorporateActionListView.RowHeight = 180;
                        lstCorporateAction = corporateAction.Where(corpAct => corpAct.actionType == pickerAction.SelectedItem.ToString()).OrderByDescending(corpAct => corpAct.msg.RUPSDate_Compare).ToList();
                        CorporateActionListView.ItemsSource = lstCorporateAction;
                        CorporateActionListView.HeightRequest = lstCorporateAction.Count * CorporateActionListView.RowHeight;
                        break;
                    case CorporateActionType.CASHDIVIDEND:
                        CorporateActionListView.ItemTemplate = new DataTemplate(() => new CashDividendViewCell());
                        CorporateActionListView.RowHeight = 210;
                        lstCorporateAction = corporateAction.Where(corpAct => corpAct.actionType == pickerAction.SelectedItem.ToString()).OrderByDescending(corpAct => corpAct.msg.CumDate_Compare).ToList();
                        CorporateActionListView.ItemsSource = lstCorporateAction;
                        CorporateActionListView.HeightRequest = lstCorporateAction.Count * CorporateActionListView.RowHeight;
                        break;
                    case CorporateActionType.RIGHTSISSUE:
                        CorporateActionListView.ItemTemplate = new DataTemplate(() => new RightIssueViewCell());
                        CorporateActionListView.RowHeight = 240;
                        lstCorporateAction = corporateAction.Where(corpAct => corpAct.actionType == pickerAction.SelectedItem.ToString()).OrderByDescending(corpAct => corpAct.msg.CumDate_Compare).ToList();
                        CorporateActionListView.ItemsSource = lstCorporateAction;
                        CorporateActionListView.HeightRequest = lstCorporateAction.Count * CorporateActionListView.RowHeight;
                        break;
                    case CorporateActionType.WARRANTS:
                        CorporateActionListView.ItemTemplate = new DataTemplate(() => new WarrantsViewCell());
                        CorporateActionListView.RowHeight = 240;
                        lstCorporateAction = corporateAction.Where(corpAct => corpAct.actionType == pickerAction.SelectedItem.ToString()).OrderByDescending(corpAct => corpAct.msg.ListingDate_Compare).ToList();
                        CorporateActionListView.ItemsSource = lstCorporateAction;
                        CorporateActionListView.HeightRequest = lstCorporateAction.Count * CorporateActionListView.RowHeight;
                        break;
                    case CorporateActionType.SPLIT:
                        CorporateActionListView.ItemTemplate = new DataTemplate(() => new StockSplitViewCell());
                        CorporateActionListView.RowHeight = 180;
                        lstCorporateAction = corporateAction.Where(corpAct => corpAct.actionType == "Split").OrderByDescending(corpAct => corpAct.msg.IssuedDate_Compare).ToList();
                        CorporateActionListView.ItemsSource = lstCorporateAction;
                        CorporateActionListView.HeightRequest = lstCorporateAction.Count * CorporateActionListView.RowHeight;
                        break;
                    case CorporateActionType.REVERSE:
                        CorporateActionListView.ItemTemplate = new DataTemplate(() => new StockReverseViewCell());
                        CorporateActionListView.RowHeight = 180;
                        lstCorporateAction = corporateAction.Where(corpAct => corpAct.actionType == "Reverse").OrderByDescending(corpAct => corpAct.msg.IssuedDate_Compare).ToList();
                        CorporateActionListView.ItemsSource = lstCorporateAction;
                        CorporateActionListView.HeightRequest = lstCorporateAction.Count * CorporateActionListView.RowHeight;
                        break;
                    case CorporateActionType.STOCKDIVIDEND:
                        CorporateActionListView.ItemTemplate = new DataTemplate(() => new StockDividendViewCell());
                        CorporateActionListView.RowHeight = 240;
                        lstCorporateAction = corporateAction.Where(corpAct => corpAct.actionType == "Stock Dividend").OrderByDescending(corpAct => corpAct.msg.CumDate_Compare).ToList();
                        CorporateActionListView.ItemsSource = lstCorporateAction;
                        CorporateActionListView.HeightRequest = lstCorporateAction.Count * CorporateActionListView.RowHeight;
                        break;
                }
            }
            else
            {
                CorporateActionListView.ItemsSource = null;

                DependencyService.Get<IToastMessage>().Show("No Data.");
            }
        }

        private void pickerAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            AppSettings.CorporateActionType = CorporateActionTypeList[pickerAction.SelectedItem.ToString()];
            RefreshPageAsync();
        }


        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (StockId == null)
            {
                return;
            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                stockById.TryGetValue(StockId.Value, out Stock stock) &&
                (stock.BestBidPrice ?? stock.LastOrPrevious).HasValue &&
                stock.SharesPerLot.HasValue)
            {
                OrderDialog orderDialog = new OrderDialog(
                    action: OrderAction.Buy,
                    stockId: StockId.Value,
                    openPrice: stock.OpenOrPrevious.Value,
                    price: stock.BestBidPrice ?? stock.LastOrPrevious.Value,
                    priceUnsure: stock.BestBidPrice == null,
                    sharesPerLot: stock.SharesPerLot.Value
                );
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await Navigation.PushPopupAsync(orderDialog);
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
            }
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (StockId == null)
            {
                return;
            }

            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                stockById.TryGetValue(StockId.Value, out Stock stock) &&
                (stock.BestOfferPrice ?? stock.LastOrPrevious).HasValue &&
                stock.SharesPerLot.HasValue)
            {
                OrderDialog orderDialog = new OrderDialog(
                    action: OrderAction.Sell,
                    stockId: StockId.Value,
                    openPrice: stock.OpenOrPrevious.Value,
                    price: stock.BestOfferPrice ?? stock.LastOrPrevious.Value,
                    priceUnsure: stock.BestOfferPrice == null,
                    sharesPerLot: stock.SharesPerLot.Value
                );
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await Navigation.PushPopupAsync(orderDialog);
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
            }
        }
    }
}