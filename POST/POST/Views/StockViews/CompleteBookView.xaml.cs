﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Dialogs;
using POST.Models;
using POST.Stores;
using POST.ViewCells;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Views.StockViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompleteBookView : ContentView, IContentView_Stock
    {
        public static readonly BindableProperty StockIdProperty = BindableProperty.Create("StockId", typeof(StockId?), typeof(StockOverviewView), defaultValue: null);
        public StockId? StockId
        {
            get => (StockId?)GetValue(StockIdProperty);
            set => SetValue(StockIdProperty, value);
        }

        private class ClientState
        {
            public ThrottledDispatcher<OrderBook, OrderBookMessage> ThrottledDispatcher { get; set; }
        }

        private DataFeedClientStateManager<ClientState> _clientStateManager;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        private List<TradeBookLevel> lstTradeBook = null;

        private List<StockSummaryLevel> lstStockSummary = null;

        private readonly List<OrderBookLevelType2> lstAdvOrderBookLevelType2 = null;

        private static bool _timerRunning = false;

        private int counterOrderBook = 0;

        public CompleteBookView()
        {
            InitializeComponent();

            lstTradeBook = new List<TradeBookLevel>();
            lstStockSummary = new List<StockSummaryLevel>();
            lstAdvOrderBookLevelType2 = new List<OrderBookLevelType2>();

            if (AppSettings.LayoutHelp_CompleteBook == "1")
            {
                layoutHelp.IsVisible = true;

                imgTopOverlay.GestureRecognizers.Add(new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        layoutHelp.IsVisible = false;
                        if (switchNeverShowAgain.IsToggled)
                        {
                            AppSettings.LayoutHelp_CompleteBook = "0";
                        }
                    })
                });

                imgNeverShowAgain.GestureRecognizers.Add(new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        switchNeverShowAgain.IsToggled = !switchNeverShowAgain.IsToggled;
                    })
                });
            }

            grdRGMarketType.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    if (Store.CurrentDisplayStockStore.GetState().MarketType == MarketType.RG)
                    {
                        return;
                    }

                    Store.CurrentDisplayStockStore.SetMarketTypeRG();
                    OnDisappearing2();
                    SetStock(Store.CurrentDisplayStockStore.GetState());
                    OnAppearing2();
                })
            });

            grdTNMarketType.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    if (Store.CurrentDisplayStockStore.GetState().MarketType == MarketType.TN)
                    {
                        return;
                    }

                    Store.CurrentDisplayStockStore.SetMarketTypeTN();
                    OnDisappearing2();
                    SetStock(Store.CurrentDisplayStockStore.GetState());
                    OnAppearing2();
                })
            });


            grdNGMarketType.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    if (Store.CurrentDisplayStockStore.GetState().MarketType == MarketType.NG)
                    {
                        return;
                    }

                    Store.CurrentDisplayStockStore.SetMarketTypeNG();
                    OnDisappearing2();
                    SetStock(Store.CurrentDisplayStockStore.GetState());
                    OnAppearing2();
                })
            });

            lblRemarks1.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    if (lblRemarks1.Text != "-" && lblRemarks1.Text != "----")
                    {
                        if (Store.StockById.GetState().TryGetValue(StockId.Value, out Stock stock))
                        {
                            SpecialNotationsDialog specialNotationsDialog = new SpecialNotationsDialog(stock.SpecialNotation1Long);
                            await PopupNavigation.Instance.PushAsync(specialNotationsDialog);
                        }
                    }
                })
            });

            lblRemarks2.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    if (Store.StockById.GetState().TryGetValue(StockId.Value, out Stock stock) && !string.IsNullOrEmpty(stock.SpecialNotation2Long))
                    {
                        SpecialNotationsDialog specialNotationsDialog = new SpecialNotationsDialog(stock.SpecialNotation2Long);
                        await PopupNavigation.Instance.PushAsync(specialNotationsDialog);
                    }

                })
            });

            grdRow1.TranslationY -= 25;
            grdRow2.TranslationY -= 25;
            grdRow3.TranslationY -= 10;
        }

        private ListView OrderBookListViewType2 = null;
        private MarketType MarketTypeListView_ = MarketType.RG;

        private void RenewStockListView(MarketType marketType)
        {
            if (OrderBookListViewType2 != null && grdOrderBook.Children.Any(view => view == OrderBookListViewType2))
            {
                if (MarketTypeListView_ == marketType)
                {
                    return;
                }
                grdOrderBook.Children.Remove(OrderBookListViewType2);
                OrderBookListViewType2 = null;
            }

            OrderBookListViewType2 = new ListView(ListViewCachingStrategy.RecycleElement)
            {
                SeparatorColor = Colors.ListViewSeparatorColor,
                BackgroundColor = Colors.PageBackgroundColor,
                RefreshControlColor = Colors.Accent,
                RowHeight = 22
            };

            if (marketType == MarketType.NG)
            {
                OrderBookListViewType2.ItemTemplate = new DataTemplate(() => new StockAdvViewCell());
            }
            else
            {
                OrderBookListViewType2.ItemTemplate = new DataTemplate(() => new CompleteBookViewCell(OrderBookLevelCellType2_BidPriceClicked, OrderBookLevelCellType2_AskPriceClicked));
            }

            MarketTypeListView_ = marketType;

            grdOrderBook.Children.Add(OrderBookListViewType2);

        }

        public async void OnAppearing()
        {
            IsVisible = true;

            counterOrderBook = 0;

            imgPointFinger.TranslationX = FrameBidPriceHelp.X;
            imgPointFinger.TranslationY = FrameBidPriceHelp.Y + 48 + gridPriceHelp.Height;

            imgTapToBuySell.TranslationX = imgPointFinger.X + 100;
            imgTapToBuySell.TranslationY = imgPointFinger.Y + 80 + gridPriceHelp.Height;

            Store.OrderBook.Clear(StockId.Value);
            Store.TradeBook.Clear(StockId.Value);
            Store.StockSummary.Clear(StockId.Value);


            if (Store.StockById.GetState().TryGetValue(StockId.Value, out Stock stock) && stock.Previous.HasValue)
            {
                Store.OrderBook.SetPrevious(stock.Previous.Value);
                Store.TradeBook.SetPrevious(stock.Previous.Value);
            }

            if (StockId.HasValue)
            {
                RenewStockListView(StockId.Value.MarketType);
            }

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState
                {
                    ThrottledDispatcher = new ThrottledDispatcher<OrderBook, OrderBookMessage>(
                        store: Store.OrderBook,
                        actionDispatcher: messages =>
                        {
                            //if (!Store.OrderBook.GetState().Any())
                            //{
                            //    Device.BeginInvokeOnMainThread(async () =>
                            //    {
                            //        await Task.Delay(500);
                            //        OrderBook orderBook = Store.OrderBook.GetState();

                            //        if (orderBook.BestBid != null)
                            //        {
                            //            OrderBookListView.ScrollTo(orderBook.Single(level => level.Price == orderBook.BestBid), ScrollToPosition.Center, true);
                            //        }
                            //        else if (orderBook.BestAsk != null)
                            //        {
                            //            OrderBookListView.ScrollTo(orderBook.Single(level => level.Price == orderBook.BestAsk), ScrollToPosition.Center, true);
                            //        }
                            //    });
                            //}
                            OrderBookListViewType2.IsRefreshing = false;
                            return Store.OrderBook.BatchUpdate(messages);
                        },
                        coalesceInterval: TimeSpan.FromSeconds(1),
                        flushInterval: TimeSpan.FromSeconds(2)
                    )
                },
                clientFactory: state =>
                {
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        if (StockId.HasValue)
                        {
                            OrderBookListViewType2.IsRefreshing = true;
                            await sender.SendAsync(new SubscribeOrderBookRequest(StockId.Value));
                            await sender.SendAsync(new SubscribeTradeBookRequest(StockId.Value));
                            await sender.SendAsync(new SubscribeStockSummaryRequest(StockId.Value));
                        }
                    };
                    client.DataReceived += (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case OrderBookMessage orderBookMessage:
                                {
                                    state.ThrottledDispatcher.Dispatch(orderBookMessage);
                                    break;
                                }
                            case TradeBookMessage tradeBookMessage:
                                Store.TradeBook.Dispatch(tradeBookMessage);
                                break;
                            case StockSummaryMessage stockSummaryMessage:
                                Store.StockSummary.Dispatch(stockSummaryMessage);
                                break;
                            case StockAdvertisementMessage stockAdvertisementMessage:
                                {
                                    OrderBookMessage orderBookMessage = new OrderBookMessage
                                    {
                                        StockCode = stockAdvertisementMessage.StockCode,
                                        BrokerCode = stockAdvertisementMessage.BrokerCode,
                                        OrderBookType = stockAdvertisementMessage.OrderBookType,
                                        Price = stockAdvertisementMessage.Price,
                                        Volume = stockAdvertisementMessage.Volume
                                    };
                                    state.ThrottledDispatcher.Dispatch(orderBookMessage);
                                    break;
                                }
                            default:
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
                );

            _clientStateManager.StartClient();

            UpdateItemListSource();
            UpdateView();
            UpdateGrdMarketType();

            if (!_timerRunning)
            {
                Device.StartTimer(TimeSpan.FromSeconds(2), () =>
                {
                    if (IsVisible)
                    {
                        UpdateItemListSource();
                        UpdateView();
                    }
                    if (!IsVisible)
                    {
                        _timerRunning = false;
                    }

                    if (counterOrderBook < 1)
                    {
                        counterOrderBook++;
                    }
                    else if (OrderBookListViewType2.IsRefreshing)
                    {
                        OrderBookListViewType2.IsRefreshing = false;
                    }

                    return IsVisible;
                });
                _timerRunning = true;
            }

            _observers.Add(Store.StockById
               .Select(stockById => StockId.HasValue && stockById.TryGetValue(StockId.Value, out Stock stock2) ? stock2 : null)
               .Subscribe(stock2 =>
               {
                   Store.OrderBook.SetLastPrice(stock2.LastOrPrevious.Value);
                   UpdateViewStockPrice(stock2);
               }
               )
           );

            _observers.Add(Store.OrderBook
                .Subscribe(orderBook =>
                {
                    if (StockId.HasValue && StockId == orderBook.StockId)
                    {
                        OrderBookListViewType2.ItemsSource = orderBook.GetListOrderBookLevelType2();
                        if (orderBook.BidVolSum.HasValue)
                        {
                            lblBidVolSum.Text = orderBook.BidVolSum.Value.ToString("N0");
                        }
                        if (orderBook.AskVolSum.HasValue)
                        {
                            lblAskVolSum.Text = orderBook.AskVolSum.Value.ToString("N0");
                        }
                        if (orderBook.StockId.MarketType == MarketType.NG)
                        {
                            lblBidFreqSum.Text = "";
                            lblAskFreqSum.Text = "";
                        }
                        else
                        {
                            if (orderBook.BidFreqSum.HasValue)
                            {
                                lblBidFreqSum.Text = orderBook.BidFreqSum.Value.ToString("N0");
                            }
                            if (orderBook.AskFreqSum.HasValue)
                            {
                                lblAskFreqSum.Text = orderBook.AskFreqSum.Value.ToString("N0");
                            }
                        }
                    }
                }
                )
            );

            try
            {
                if (stock != null)
                {
                    lblRemarks1.Text = stock.SpecialNotation1Short;
                    lblRemarks2.Text = stock.SpecialNotation2Short;
                }
            }
            catch (OperationCanceledException)
            {
                lblRemarks1.Text = "-";
                lblRemarks2.Text = "-";
            }
            catch (Exception)
            {
                lblRemarks1.Text = "----";
                lblRemarks2.Text = "----";
            }
        }

        public void OnAppearing2()
        {
            counterOrderBook = 0;

            Store.OrderBook.Clear(StockId.Value);
            Store.TradeBook.Clear(StockId.Value);
            Store.StockSummary.Clear(StockId.Value);

            if (Store.StockById.GetState().TryGetValue(StockId.Value, out Stock stock) && stock.Previous.HasValue)
            {
                Store.OrderBook.SetPrevious(stock.Previous.Value);
                Store.TradeBook.SetPrevious(stock.Previous.Value);
            }

            if (StockId.HasValue)
            {
                RenewStockListView(StockId.Value.MarketType);
            }

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState
                {
                    ThrottledDispatcher = new ThrottledDispatcher<OrderBook, OrderBookMessage>(
                        store: Store.OrderBook,
                        actionDispatcher: messages =>
                        {
                            //if (!Store.OrderBook.GetState().Any())
                            //{
                            //    Device.BeginInvokeOnMainThread(async () =>
                            //    {
                            //        await Task.Delay(500);
                            //        OrderBook orderBook = Store.OrderBook.GetState();

                            //        if (orderBook.BestBid != null)
                            //        {
                            //            OrderBookListView.ScrollTo(orderBook.Single(level => level.Price == orderBook.BestBid), ScrollToPosition.Center, true);
                            //        }
                            //        else if (orderBook.BestAsk != null)
                            //        {
                            //            OrderBookListView.ScrollTo(orderBook.Single(level => level.Price == orderBook.BestAsk), ScrollToPosition.Center, true);
                            //        }
                            //    });
                            //}
                            OrderBookListViewType2.IsRefreshing = false;
                            return Store.OrderBook.BatchUpdate(messages);
                        },
                        coalesceInterval: TimeSpan.FromSeconds(1),
                        flushInterval: TimeSpan.FromSeconds(2)
                    )
                },
                clientFactory: state =>
                {
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        if (StockId.HasValue)
                        {
                            OrderBookListViewType2.IsRefreshing = true;
                            await sender.SendAsync(new SubscribeOrderBookRequest(StockId.Value));
                            await sender.SendAsync(new SubscribeTradeBookRequest(StockId.Value));
                            await sender.SendAsync(new SubscribeStockSummaryRequest(StockId.Value));
                        }
                    };
                    client.DataReceived += (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case OrderBookMessage orderBookMessage:
                                {
                                    state.ThrottledDispatcher.Dispatch(orderBookMessage);
                                    break;
                                }
                            case TradeBookMessage tradeBookMessage:
                                Store.TradeBook.Dispatch(tradeBookMessage);
                                break;
                            case StockSummaryMessage stockSummaryMessage:
                                Store.StockSummary.Dispatch(stockSummaryMessage);
                                break;
                            case StockAdvertisementMessage stockAdvertisementMessage:
                                {
                                    OrderBookMessage orderBookMessage = new OrderBookMessage
                                    {
                                        StockCode = stockAdvertisementMessage.StockCode,
                                        MarketType = MarketType.NG,
                                        BrokerCode = stockAdvertisementMessage.BrokerCode,
                                        OrderBookType = stockAdvertisementMessage.OrderBookType,
                                        Price = stockAdvertisementMessage.Price,
                                        Volume = stockAdvertisementMessage.Volume
                                    };
                                    state.ThrottledDispatcher.Dispatch(orderBookMessage);
                                    break;
                                }
                            default:
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
                );

            _clientStateManager.StartClient();

            UpdateItemListSource();
            UpdateView();
            UpdateGrdMarketType();

            _observers.Add(Store.StockById
               .Select(stockById => StockId.HasValue && stockById.TryGetValue(StockId.Value, out Stock stock2) ? stock2 : null)
               .Subscribe(stock2 =>
               {
                   Store.OrderBook.SetLastPrice(stock2.LastOrPrevious.Value);
                   UpdateViewStockPrice(stock2);
               }
               )
           );

            _observers.Add(Store.OrderBook
                .Subscribe(orderBook =>
                {
                    if (StockId.HasValue && StockId == orderBook.StockId)
                    {
                        OrderBookListViewType2.ItemsSource = orderBook.GetListOrderBookLevelType2();

                        if (orderBook.BidVolSum.HasValue)
                        {
                            lblBidVolSum.Text = orderBook.BidVolSum.Value.ToString("N0");
                        }
                        if (orderBook.AskVolSum.HasValue)
                        {
                            lblAskVolSum.Text = orderBook.AskVolSum.Value.ToString("N0");
                        }
                        if (orderBook.StockId.MarketType == MarketType.NG)
                        {
                            lblBidFreqSum.Text = "";
                            lblAskFreqSum.Text = "";
                        }
                        else
                        {
                            if (orderBook.BidFreqSum.HasValue)
                            {
                                lblBidFreqSum.Text = orderBook.BidFreqSum.Value.ToString("N0");
                            }
                            if (orderBook.AskFreqSum.HasValue)
                            {
                                lblAskFreqSum.Text = orderBook.AskFreqSum.Value.ToString("N0");
                            }
                        }
                    }
                }
                )
            );
        }

        public void OnDisappearing()
        {
            IsVisible = false;

            if (_clientStateManager != null)
            {
                _clientStateManager.StopClient();
                _clientStateManager = null;
            }

            Store.OrderBook.Clear(StockId.Value);
            Store.TradeBook.Clear(StockId.Value);
            Store.StockSummary.Clear(StockId.Value);

            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();

            if (!Store.CurrentDisplayStockStore.GetState().Code.ToLower().Contains("-r"))
            {
                Store.CurrentDisplayStockStore.SetMarketTypeRG();
            }
        }

        public void OnDisappearing2()
        {
            if (_clientStateManager != null)
            {
                _clientStateManager.StopClient();
                _clientStateManager = null;
            }

            Store.OrderBook.Clear(StockId.Value);
            Store.TradeBook.Clear(StockId.Value);
            Store.StockSummary.Clear(StockId.Value);

            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
        }

        public void SetStock(StockId? stockId)
        {
            if (StockId.HasValue)
            {
                Store.OrderBook.Clear(StockId.Value);
                Store.TradeBook.Clear(StockId.Value);
                Store.StockSummary.Clear(StockId.Value);
            }
            StockId = stockId;
        }

        private void UpdateItemListSource()
        {
            List<TradeBookLevel> lstTradeBookLevelTmp = Store.TradeBook.GetState().Values.OrderByDescending(tradeBook => tradeBook.Price).ToList();
            lstTradeBook = lstTradeBookLevelTmp;

            List<StockSummaryLevel> lstStockSummaryLevelTmp = Store.StockSummary.GetState().Values.OrderBy(stockSummary => stockSummary.BrokerCode).ToList();
            lstStockSummary = lstStockSummaryLevelTmp;
        }

        private void UpdateViewStockPrice(Stock stock)
        {
            lblPrev.Text = stock.Previous.HasValue ? stock.Previous.Value.ToString("N0") : "";

            Store.OrderBook.SetPrevious(stock.Previous.Value);
            Store.TradeBook.SetPrevious(stock.Previous.Value);

            if (stock.OHLC.HasValue)
            {
                lblHigh.Text = stock.OHLC.Value.High.ToString("N0");
                lblLow.Text = stock.OHLC.Value.Low.ToString("N0");
            }
            else
            {
                lblHigh.Text = "";
                lblLow.Text = "";
            }
            if (stock.HasMagnitudes)
            {
                lblVol.Text = stock.Magnitudes.Volume.Value.ToString("N0");
                lblVal.Text = stock.Magnitudes.Value.ToString("N0");
                lblFreq.Text = stock.Magnitudes.Frequency.ToString("N0");
            }
            else
            {
                lblVol.Text = "";
                lblVal.Text = "";
                lblFreq.Text = "";
            }
        }

        private void UpdateView()
        {
            if (lstTradeBook != null)
            {
                TradeBookListView.ItemsSource = lstTradeBook;
            }

            if (lstStockSummary != null)
            {
                StockSummaryListView.ItemsSource = lstStockSummary;
            }
        }

        private void UpdateGrdMarketType()
        {
            if (Store.CurrentDisplayStockStore.GetState().MarketType == MarketType.RG)
            {
                FrameRGMarketType.BorderColor = Colors.Accent;
                lblRGMarketType.BackgroundColor = Colors.Accent;
                lblRGMarketType.TextColor = Colors.FieldValueDefaultColor;

                FrameTNMarketType.BorderColor = Colors.FieldNameColor;
                lblTNMarketType.BackgroundColor = Colors.Transparent;
                lblTNMarketType.TextColor = Colors.FieldNameColor;

                FrameNGMarketType.BorderColor = Colors.FieldNameColor;
                lblNGMarketType.BackgroundColor = Colors.Transparent;
                lblNGMarketType.TextColor = Colors.FieldNameColor;
            }
            else if (Store.CurrentDisplayStockStore.GetState().MarketType == MarketType.TN)
            {
                FrameRGMarketType.BorderColor = Colors.FieldNameColor;
                lblRGMarketType.BackgroundColor = Colors.Transparent;
                lblRGMarketType.TextColor = Colors.FieldNameColor;

                FrameTNMarketType.BorderColor = Colors.Accent;
                lblTNMarketType.BackgroundColor = Colors.Accent;
                lblTNMarketType.TextColor = Colors.FieldValueDefaultColor;

                FrameNGMarketType.BorderColor = Colors.FieldNameColor;
                lblNGMarketType.BackgroundColor = Colors.Transparent;
                lblNGMarketType.TextColor = Colors.FieldNameColor;
            }
            else if (Store.CurrentDisplayStockStore.GetState().MarketType == MarketType.NG)
            {
                FrameRGMarketType.BorderColor = Colors.FieldNameColor;
                lblRGMarketType.BackgroundColor = Colors.Transparent;
                lblRGMarketType.TextColor = Colors.FieldNameColor;

                FrameTNMarketType.BorderColor = Colors.FieldNameColor;
                lblTNMarketType.BackgroundColor = Colors.Transparent;
                lblTNMarketType.TextColor = Colors.FieldNameColor;

                FrameNGMarketType.BorderColor = Colors.Accent;
                lblNGMarketType.BackgroundColor = Colors.Accent;
                lblNGMarketType.TextColor = Colors.FieldValueDefaultColor;
            }

            if (Store.CurrentDisplayStockStore.GetState().Code.ToLower().Contains("-r") && Store.CurrentDisplayStockStore.GetState().MarketType == MarketType.TN)
            {
                grdRGMarketType.IsEnabled = false;
                grdTNMarketType.IsEnabled = false;
                grdNGMarketType.IsEnabled = false;
            }
            else
            {
                grdRGMarketType.IsEnabled = true;
                grdTNMarketType.IsEnabled = true;
                grdNGMarketType.IsEnabled = true;
            }

            if (Store.CurrentDisplayStockStore.GetState().MarketType == MarketType.NG)
            {
                lblBidFrqBrokerColHeader.Text = "BROKER";
                lblAskFrqBrokerColHeader.Text = "BROKER";
            }
            else
            {
                lblBidFrqBrokerColHeader.Text = "FRQ";
                lblAskFrqBrokerColHeader.Text = "FRQ";
            }
        }

        private async void OrderBookLevelCellType2_BidPriceClicked(object sender, ViewCells.OrderButtonEventArgs e)
        {
            if (StockId.HasValue && StockId.Value.MarketType == MarketType.NG)
            {
                return;
            }
            decimal price = e.Price.Value;
            OrderBookActionDialog orderBookActionDialog = new OrderBookActionDialog(price);
            orderBookActionDialog.SetStock(StockId);
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await PopupNavigation.Instance.PushAsync(orderBookActionDialog);
        }

        private async void OrderBookLevelCellType2_AskPriceClicked(object sender, ViewCells.OrderButtonEventArgs e)
        {
            if (StockId.HasValue && StockId.Value.MarketType == MarketType.NG)
            {
                return;
            }
            decimal price = e.Price.Value;
            OrderBookActionDialog orderBookActionDialog = new OrderBookActionDialog(price);
            orderBookActionDialog.SetStock(StockId);
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await PopupNavigation.Instance.PushAsync(orderBookActionDialog);
        }
    }
}