﻿using POST.Models;

namespace POST.Views
{
    public interface IContentView
    {
        void OnAppearing();
        void OnDisappearing();
    }

    public interface IContentView_Stock : IContentView
    {
        void SetStock(StockId? stockId);
    }
}
