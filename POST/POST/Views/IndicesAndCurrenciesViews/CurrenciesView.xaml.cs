﻿using POST.DataSources;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Dialogs;
using POST.Models;
using POST.Models.Collections;
using POST.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Views.IndicesAndCurrenciesViews
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CurrenciesView : ContentView, IContentView
    {

        private class ClientState
        {
            public List<CurrencyMessage> CurrencyMessages { get; } = new List<CurrencyMessage>();
            public bool Subscribed { get; set; }
        }

        private readonly DataFeedClientStateManager<ClientState> _clientStateManager;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public ObservableSet<string, Currency> Currencies { get; }

        public CurrenciesView()
        {
            InitializeComponent();
            Currencies = new ObservableSet<string, Currency>();
            CurrenciesListView.ItemsSource = Currencies;

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState { Subscribed = false },
                clientFactory: state =>
                {
                    state.CurrencyMessages.Clear();
                    state.Subscribed = false;
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        await sender.SendAsync(new CurrencyInitRequest());
                    };
                    client.DataReceived += async (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case CurrencyMessage currencyMessage:
                                state.CurrencyMessages.Add(currencyMessage);
                                break;
                            case CurrencyEndMessage currencyEndMessage:
                                Store.CurrencyByCode.UpdateQuotes(state.CurrencyMessages);
                                state.CurrencyMessages.Clear();
                                CurrenciesListView.IsRefreshing = false;

                                // HACK: We need to resend SubscribeAllCurrencyQuotesRequest to refresh all quotes.
                                // This should be fixed on server side.
                                await Task.Delay(60_000);
                                if (_clientStateManager.IsConnected)
                                {
                                    await sender.SendAsync(new SubscribeAllCurrencyQuotesRequest());
                                    state.Subscribed = true;
                                }
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
            );
        }

        public void OnAppearing()
        {
            _observers.Add(Store.CurrencyByCode
                .Select(currencyByCode => currencyByCode.Values.OrderBy(currency =>
                {
                    if (currency.Code.StartsWith("USD"))
                    {
                        return currency.Code;
                    }
                    else if (currency.Code.Length > 3)
                    {
                        return $"{currency.Code.Substring(3)}{currency.Code.Substring(0, 3)}";
                    }
                    else
                    {
                        return currency.Code;
                    }
                }).ToList())
                .Subscribe(currencies =>
                {
                    Currencies.SetItems(currencies);
                })
            );

            _clientStateManager.StartClient();
            IsVisible = true;
        }

        public void OnDisappearing()
        {

            // Stop client
            _clientStateManager.StopClient();

            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();

            // Cancel refreshing
            CurrenciesListView.IsRefreshing = false;

            IsVisible = false;
        }

        private async void Handle_PullToRefresh(object sender, EventArgs e)
        {
            await _clientStateManager.SendAsync(new CurrencyInitRequest());
        }

        private async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is Currency currency)
            {
                CurrCalculatorDialog popup = new CurrCalculatorDialog(currency.NumeratorCode, currency.DenominatorCode);
                await PopupNavigation.Instance.PushAsync(popup);
            }
            if (sender is ListView listView)
            {
                listView.SelectedItem = null;
            }
        }

        public async void BtnCalc_Clicked(object sender, EventArgs e)
        {
            CurrCalculatorDialog popup = new CurrCalculatorDialog("", "");
            await PopupNavigation.Instance.PushAsync(popup);
        }

        private async void BuyButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("BuyButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }

        private async void SellButton_Clicked(object sender, EventArgs e)
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            IsEnabled = false;
            StockSearchPage stockSearchPage = new StockSearchPage("SellButton");
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }

            await MainPage.NavigationPage.PushAsync(stockSearchPage);
            IsEnabled = true;
        }
    }
}