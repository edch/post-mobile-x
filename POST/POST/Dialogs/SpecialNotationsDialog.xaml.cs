﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SpecialNotationsDialog : PopupPage
    {
        private readonly string specialNotation;

        public SpecialNotationsDialog(string specialNotation)
        {
            InitializeComponent();

            this.specialNotation = specialNotation;

            CloseWhenBackgroundIsClicked = true;
        }

        protected virtual Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.5);
        }

        protected virtual Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(1);
        }

        private async void CloseAllPopup()
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lblNotations.Text = specialNotation;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void CancelFilterGroupButton_Clicked(object sender, EventArgs e)
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        private async void btnOk_Clicked(object sender, EventArgs e)
        {
            CloseAllPopup();
        }

    }
}