﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.AthenaMessages.Responses;
using POST.Extensions;
using POST.Pages.PortfolioPages;
using POST.Services;
using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace POST.Dialogs
{
    public class FundWithdrawalConfirmation : PopupPage
    {
        private readonly BankAccount bankAccount;
        private readonly string brokerAccountId;
        private readonly string pin;
        private readonly DateTime dateRequested;
        private readonly decimal amount;
        private readonly string amountWords;
        private readonly FundWithdrawalPage fundWithdrawalPage = null;

        private string GetTitle()
        {
            return "Fund Withdrawal Confirmation";
        }

        private Color GetTitleBackgroundColor()
        {
            return Colors.FieldNameColor;
        }

        public FundWithdrawalConfirmation(
            BankAccount bankAccount,
            string brokerAccountId,
            string pin,
            DateTime dateRequested,
            decimal amount,
            string amountWords,
            FundWithdrawalPage fundWithdrawalPage
        )
        {
            this.bankAccount = bankAccount;
            this.brokerAccountId = brokerAccountId;
            this.pin = pin;
            this.dateRequested = dateRequested;
            this.amount = amount;
            this.amountWords = amountWords;
            this.fundWithdrawalPage = fundWithdrawalPage;

            CloseWhenBackgroundIsClicked = false;
            Animation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Center,
                PositionOut = MoveAnimationOptions.Center,
                ScaleIn = 1.2,
                ScaleOut = 0.8,
                DurationIn = 400,
                DurationOut = 300,
                EasingIn = Easing.SinOut,
                EasingOut = Easing.SinIn,
                HasBackgroundAnimation = true
            };

            Content = GetFormView(0);
        }

        private View GetFormView(double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 300,
                Spacing = 1,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Account",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = brokerAccountId,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Amount",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{amount:N0}",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "In Words",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = amountWords,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Request Date",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = dateRequested.ToString("ddd, dd MMM yyyy"),
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Bank Name",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = bankAccount.BankName,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Account No",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = bankAccount.BankAccountNumber,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Account Name",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                Text = bankAccount.BankAccountHolderName,
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(5, 20, 5, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "Execute",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.White,
                                BackgroundColor = Colors.Accent,
                                Command = new Command(async () => await OnConfirmCreateAsync())
                            }.Apply(button => Grid.SetColumn(button, 0)),
                            new Button {
                                Text = "Cancel",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () =>  {if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAllAsync(); } })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private View GetErrorView(string errorMessage, double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = "Fund Withdrawal Request Status",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Label {
                        TextColor = Colors.PageTextDialogColor,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                        Margin = new Thickness(20, 10, 20, 20),
                        Text = errorMessage
                    },
                    new Grid {
                        Padding = new Thickness(10, 20, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () => {
                                    if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAllAsync(); } await MainPage.NavigationPage.PopToRootAsync(true);
                                })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private async Task OnConfirmCreateAsync()
        {
            try
            {
                IsEnabled = false;

                AthenaResponse athenaResponse = await AthenaClient.Current.PlaceCashWithdrawAsync(brokerAccountId, pin, dateRequested.ToString("dd-MM-yyyy"), amount, amountWords, bankAccount.BankAccountNumber, bankAccount.RdnAccountNumber, bankAccount.BankAccountHolderName, "1", cancellationToken: new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);

                string[] arrStr = athenaResponse.Message.Split("\n");

                if (arrStr.Count() > 0)
                {
                    if (arrStr[0] == "true")
                    {
                        DependencyService.Get<IToastMessage>().Show("Fund Withdrawal Request sent.");

                        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                        {
                            await PopupNavigation.Instance.PopAllAsync();
                        }

                        await MainPage.NavigationPage.PopToRootAsync(true);
                    }
                    else
                    {
                        Content = GetErrorView("Fund Withdrawal Request Failed. " + arrStr[1] + "!", 0);
                    }
                }
            }
            catch
            {
                Content = GetErrorView("Fund Withdrawal Request Failed", 0);
            }
            finally
            {
                IsEnabled = true;
            }

            fundWithdrawalPage.ClearInput();
        }
    }
}
