﻿using POST.Services;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateFilterGroupDialog : PopupPage
    {
        public CreateFilterGroupDialog()
        {
            InitializeComponent();

            CloseWhenBackgroundIsClicked = true;
        }

        protected virtual Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.5);
        }

        protected virtual Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(1);
        }

        private async void CloseAllPopup()
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void CancelFilterGroupButton_Clicked(object sender, EventArgs e)
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        private async void CreateFilterGroupButton_Clicked(object sender, EventArgs e)
        {
            string name = entryFilterGroupName.Text;
            if (string.IsNullOrWhiteSpace(name))
            {
                DependencyService.Get<IToastMessage>().Show("Group name is empty.");
            }
            else if (Store.CustomRunningTradeFilterStockIdsStore.GetState().isAnyGroup(name) || Store.DefaultRunningTradeFilterStockIdsStore.GetState().isAnyGroup(name))
            {
                DependencyService.Get<IToastMessage>().Show("Group name already exist.");
            }
            else
            {
                Store.CustomRunningTradeFilterStockIdsStore.AddGroup(name);
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }
            }
        }

    }
}