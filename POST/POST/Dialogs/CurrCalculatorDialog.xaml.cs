﻿using POST.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CurrCalculatorDialog : PopupPage
    {
        private readonly Dictionary<string, decimal> dictBaseUsd;
        private readonly Dictionary<string, decimal> dictBaseIdr;
        private readonly ObservableCollection<string> lstCurrCode;

        private string strBaseCode, strTargetCode;

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        public CurrCalculatorDialog(string strBaseCode, string strTargetCode)
        {
            InitializeComponent();

            lstCurrCode = new ObservableCollection<string>() { "IDR", "USD" };
            dictBaseIdr = new Dictionary<string, decimal>
            {
                { "IDR", 1 }
            };
            dictBaseUsd = new Dictionary<string, decimal>
            {
                { "USD", 1 }
            };

            PickerBase.ItemsSource = lstCurrCode;
            PickerTarget.ItemsSource = lstCurrCode;

            this.strBaseCode = strBaseCode;
            this.strTargetCode = strTargetCode;
        }

        protected virtual Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.5);
        }

        protected virtual Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(1);
        }

        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();
        }

        protected override bool OnBackgroundClicked()
        {
            CloseAllPopup();
            return false;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _observers.Add(Store.CurrencyByCode
                .Select(currencyByCode => currencyByCode.Values.OrderBy(currency =>
                {
                    if (currency.Code.StartsWith("USD"))
                    {
                        return currency.Code;
                    }
                    else if (currency.Code.Length > 3)
                    {
                        return $"{currency.Code.Substring(3)}{currency.Code.Substring(0, 3)}";
                    }
                    else
                    {
                        return currency.Code;
                    }
                }).ToList())
                .Subscribe(currencies =>
                {
                    UpdateDictBase(currencies);
                })
            );

            PickerBase.SelectedItem = strBaseCode;
            PickerTarget.SelectedItem = strTargetCode;

            TxtBase.Text = "1";
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();

            base.OnDisappearing();
        }

        private void UpdateDictBase(List<Currency> lstCurr)
        {
            string strTarget = "";
            decimal rate = 0;

            foreach (Currency curr in lstCurr)
            {
                if (curr.Code.Contains("USD"))
                {
                    if (curr.DenominatorCode == "USD")
                    {
                        strTarget = curr.NumeratorCode;
                        rate = 1 / curr.Last;
                    }
                    else
                    {
                        strTarget = curr.DenominatorCode;
                        rate = curr.Last;
                    }

                    if (!dictBaseUsd.ContainsKey(strTarget))
                    {
                        dictBaseUsd.Add(strTarget, rate);
                    }
                    else
                    {
                        dictBaseUsd[strTarget] = rate;
                    }
                }
                else
                {
                    if (curr.DenominatorCode == "IDR")
                    {
                        strTarget = curr.NumeratorCode;
                        rate = 1 / curr.Last;
                    }
                    else
                    {
                        strTarget = curr.DenominatorCode;
                        rate = curr.Last;
                    }

                    if (!dictBaseIdr.ContainsKey(strTarget))
                    {
                        dictBaseIdr.Add(strTarget, rate);
                    }
                    else
                    {
                        dictBaseIdr[strTarget] = rate;
                    }
                }

                if (!lstCurrCode.Contains(strTarget))
                {
                    lstCurrCode.Add(strTarget);
                }
            }

            CalculateRate();
        }

        private void TxtBase_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue.Length > 13)
            {
                TxtBase.Text = TxtBase.Text.Remove(13);
            }

            CalculateRate();
        }

        private void PickerBase_SelectedIndexChanged(object sender, EventArgs e)
        {
            strBaseCode = PickerBase.SelectedItem.ToString();
            CalculateRate();
        }

        private void PickerTarget_SelectedIndexChanged(object sender, EventArgs e)
        {
            strTargetCode = PickerTarget.SelectedItem.ToString();
            CalculateRate();
        }

        private async void CloseAllPopup()
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        private void BtnClose_Clicked(object sender, EventArgs e)
        {
            CloseAllPopup();
        }

        private void BtnChange_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(strBaseCode) || string.IsNullOrEmpty(strTargetCode))
            {
                return;
            }

            string strTmp = PickerBase.SelectedItem.ToString();
            PickerBase.SelectedItem = PickerTarget.SelectedItem.ToString();
            PickerTarget.SelectedItem = strTmp;
        }

        private void CalculateRate()
        {
            if (string.IsNullOrEmpty(TxtBase.Text) || string.IsNullOrEmpty(strBaseCode) || string.IsNullOrEmpty(strTargetCode))
            {
                return;
            }

            decimal result = 0;

            if (dictBaseIdr.ContainsKey(strBaseCode) && dictBaseIdr.ContainsKey(strTargetCode))
            {
                result = dictBaseIdr[strTargetCode] / dictBaseIdr[strBaseCode];
            }
            else if (dictBaseUsd.ContainsKey(strBaseCode) && dictBaseUsd.ContainsKey(strTargetCode))
            {
                result = dictBaseUsd[strTargetCode] / dictBaseUsd[strBaseCode];
            }
            else
            {
                decimal tmpBase = 0;
                if (dictBaseUsd.ContainsKey(strTargetCode))
                {
                    if (!dictBaseIdr.TryGetValue("USD", out decimal tmpIdrUsd))
                    {
                        if (dictBaseUsd.TryGetValue("USD", out decimal tmpUsd) && dictBaseUsd.TryGetValue("IDR", out decimal tmpIdr))
                        {
                            tmpIdrUsd = tmpUsd / tmpIdr;
                        }
                        else
                        {
                            TxtTarget.Text = "NaN";
                            return;
                        }
                    }
                    tmpBase = dictBaseIdr[strBaseCode] / tmpIdrUsd;
                    result = dictBaseUsd[strTargetCode] / tmpBase;
                }
                else
                {
                    if (!dictBaseUsd.TryGetValue("IDR", out decimal tmpUsdIdr))
                    {
                        if (dictBaseIdr.TryGetValue("IDR", out decimal tmpIdr) && dictBaseIdr.TryGetValue("USD", out decimal tmpUsd))
                        {
                            tmpUsdIdr = tmpIdr / tmpUsd;
                        }
                        else
                        {
                            TxtTarget.Text = "NaN";
                            return;
                        }
                    }
                    tmpBase = dictBaseUsd[strBaseCode] / tmpUsdIdr;
                    result = dictBaseIdr[strTargetCode] / tmpBase;
                }
            }

            decimal.TryParse(TxtBase.Text, out decimal d);
            try
            {
                result = d * result;
                TxtTarget.Text = result.ToString("N4").TrimEnd('0').TrimEnd('.');
            }
            catch
            {
                TxtTarget.Text = "NaN";
            }
        }
    }
}