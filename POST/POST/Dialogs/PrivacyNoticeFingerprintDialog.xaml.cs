﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PrivacyNoticeFingerprintDialog : PopupPage
    {
        private readonly string urlManifestDisclaimerResourcesPrivacyNotice = "";
        private readonly string urlManifestDisclaimerResourcesCautions = "";

        public PrivacyNoticeFingerprintDialog()
        {
            InitializeComponent();

            CloseWhenBackgroundIsClicked = true;

            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    urlManifestDisclaimerResourcesPrivacyNotice = "POST.Droid.Resources.FingerprintPrivacyNotice.txt";
                    urlManifestDisclaimerResourcesCautions = "POST.Droid.Resources.FingerprintCautions.txt";
                    break;
                case Device.iOS:
                    urlManifestDisclaimerResourcesPrivacyNotice = "POST.iOS.Resources.FingerprintPrivacyNotice.txt";
                    urlManifestDisclaimerResourcesCautions = "POST.iOS.Resources.FingerprintCautions.txt";
                    break;
                case Device.UWP:
                    urlManifestDisclaimerResourcesPrivacyNotice = "POST.UWP.Resources.FingerprintPrivacyNotice.txt";
                    urlManifestDisclaimerResourcesCautions = "POST.UWP.Resources.FingerprintCautions.txt";
                    break;
            }
        }

        private async void LoadTextFromResource()
        {
            Assembly assembly = IntrospectionExtensions.GetTypeInfo(typeof(DisclaimerDialog)).Assembly;
            using (Stream stream = assembly.GetManifestResourceStream(urlManifestDisclaimerResourcesPrivacyNotice))
            {
                if (stream != null)
                {
                    string fileText = "";
                    using (StreamReader reader = new System.IO.StreamReader(stream))
                    {
                        fileText = await reader.ReadToEndAsync();
                    }
                    lblContentPrivacyNotice.Text = fileText;
                }
            }
            using (Stream stream = assembly.GetManifestResourceStream(urlManifestDisclaimerResourcesCautions))
            {
                if (stream != null)
                {
                    string fileText = "";
                    using (StreamReader reader = new System.IO.StreamReader(stream))
                    {
                        fileText = await reader.ReadToEndAsync();
                    }
                    lblContentCaution.Text = fileText;
                }
            }
        }

        protected virtual Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.5);
        }

        protected virtual Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(1);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            LoadTextFromResource();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void CloseAllPopup()
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        private async void btnOk_Clicked(object sender, EventArgs e)
        {
            CloseAllPopup();
        }

    }
}