﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.WdMessages.Responses;
using POST.Extensions;
using POST.Services;
using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace POST.Dialogs
{

    public class FundWithdrawalDetailDialog : PopupPage
    {
        FundWithdrawItem fundWithdrawItem = null;

        private string GetTitle()
        {
            return "Fund Withdrawal Details";
        }

        private Color GetTitleBackgroundColor()
        {
            return Colors.FieldNameColor;
        }

        public FundWithdrawalDetailDialog(FundWithdrawItem fundWithdrawItem)
        {
            this.fundWithdrawItem = fundWithdrawItem;

            CloseWhenBackgroundIsClicked = false;
            Animation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Center,
                PositionOut = MoveAnimationOptions.Center,
                ScaleIn = 1.2,
                ScaleOut = 0.8,
                DurationIn = 400,
                DurationOut = 300,
                EasingIn = Easing.SinOut,
                EasingOut = Easing.SinIn,
                HasBackgroundAnimation = true
            };

            Content = GetFormView(0);
        }

        private View GetFormView(double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 300,
                Spacing = 1,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Requested for",
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = fundWithdrawItem.RqDate,
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    /*
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Transfer Date",
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = fundWithdrawItem.TxDate,
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    */
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Amount",
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{fundWithdrawItem.Amount:N0}",
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Status",
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {                                
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }.Apply(lbl =>
                                {
                                    if(fundWithdrawItem.Sdesc == "OPEN")
                                    {
                                        lbl.Text = fundWithdrawItem.Sdesc + $" (available for cancel)";
                                    }
                                    else
                                    {
                                        lbl.Text = fundWithdrawItem.Sdesc;
                                    }
                                })
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Destination Account",
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = fundWithdrawItem.BankName + " " + fundWithdrawItem.BankAccount,
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "On Behalf of",
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = fundWithdrawItem.BankAccName,
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(5, 20, 5, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "Cancel Withdrawal",
                                FontSize = 11,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.White,
                                BackgroundColor = Colors.Accent,
                                Command = new Command(async () => await OnConfirmCancelAsync())
                            }.Apply(button =>
                                {
                                    Grid.SetColumn(button, 0);
                                    {
                                        if(fundWithdrawItem.Sdesc != "OPEN")
                                        {
                                            button.BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2);
                                            button.TextColor = Color.DarkGray;
                                        }
                                    }
                                }),
                            new Button {
                                Text = "Close",
                                FontSize = 11,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () =>  {if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) await PopupNavigation.Instance.PopAllAsync(); })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private View GetErrorView(string errorMessage, double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = "Fund Withdrawal Canceling Request Status",
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Label {
                        TextColor = Colors.PageTextDialogColor,
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProRegular"],
                        Margin = new Thickness(20, 10, 20, 20),
                        Text = errorMessage
                    },
                    new Grid {
                        Padding = new Thickness(10, 20, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () => {
                                    if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)await PopupNavigation.Instance.PopAllAsync();
                                    await MainPage.NavigationPage.PopToRootAsync(true);
                                    Store.AthenaSession.RefreshSession();
                                })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private View GetResponseView(string message, double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = "Fund Withdrawal Canceling Response",
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Label {
                        TextColor = Colors.PageTextDialogColor,
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProRegular"],
                        Margin = new Thickness(20, 10, 20, 20),
                        Text = message
                    },
                    new Grid {
                        Padding = new Thickness(10, 20, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () => {
                                    if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)await PopupNavigation.Instance.PopAllAsync();
                                    await MainPage.NavigationPage.PopToRootAsync(true);
                                    Store.AthenaSession.RefreshSession();
                                })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private async Task OnConfirmCancelAsync()
        {
            try
            {
                IsEnabled = false;

                if (fundWithdrawItem.Sdesc != "OPEN")
                {
                    DependencyService.Get<IToastMessage>().Show("Fund Withdrawal Status is not Open. Cannot Cancel Fund Withdrawal!");
                    return;
                }

                var AnswerYes = await Application.Current.MainPage.DisplayAlert("Cancel Confirmation", "Are you sure want to cancel the withdrawal?", "Ok", "Cancel");
                if (!AnswerYes)
                {
                    return;
                }

                WdCancelResponse wdCancelResponse = await AthenaClient.Current.WdCancelAsync(
                    custId: fundWithdrawItem.CustId,
                    brokerAccountId: fundWithdrawItem.CustId,
                    id: fundWithdrawItem.Id.ToString(),
                    cancellationToken: new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token
                    );

                if (wdCancelResponse != null)
                {
                    Content = GetResponseView(wdCancelResponse.msg, 0);
                }
                else
                {
                    Content = GetErrorView("Cancel Fund Withdrawal Request Failed", 0);
                }
            }
            catch
            {
                Content = GetErrorView("Cancel Fund Withdrawal Request Failed", 0);
            }
            finally
            {
                IsEnabled = true;
            }
        }
    }
}
