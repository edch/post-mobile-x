﻿using POST.DataSources;
using POST.Services;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FeedbackDialog : PopupPage
    {
        public FeedbackDialog()
        {
            InitializeComponent();
        }

        private async void ClosePopup()
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAsync();
            }
        }

        private async void btnCancel_Clicked(object sender, EventArgs e)
        {
            ClosePopup();
        }

        private async void btnSubmit_Clicked(object sender, EventArgs e)
        {
            try
            {
                string result = await FeedbackClient.Default.SendFeedbackAsync(FeedbackEditor.Text, DependencyService.Get<IDeviceProperties>().VersionName, DependencyService.Get<IDeviceProperties>().OS, DependencyService.Get<IDeviceProperties>().OSVersion, DependencyService.Get<IDeviceProperties>().DeviceType, DependencyService.Get<IDeviceProperties>().DeviceId, Store.LoginForm.GetState().UserId, Store.LoginForm.GetState().Password, new CancellationTokenSource(TimeSpan.FromSeconds(10)).Token);
                ClosePopup();
                DependencyService.Get<IToastMessage>().Show("Feedback Submitted.");
            }
            catch (OperationCanceledException)
            {
                DependencyService.Get<IToastMessage>().Show("Connection Timeout.");
            }
            catch (ObjectDisposedException)
            {
                DependencyService.Get<IToastMessage>().Show("Connection Timeout.");
            }
            catch (Exception)
            {
                // Exception handling policy: suppress to null
                DependencyService.Get<IToastMessage>().Show("Connection Timeout.");
            }
        }
    }
}