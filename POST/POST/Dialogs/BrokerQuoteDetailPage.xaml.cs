﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BrokerQuoteDetailPage : PopupPage
    {
        public string BrokerName { get; }

        public BrokerQuoteDetailPage(string brokerName)
        {
            InitializeComponent();
            CloseWhenBackgroundIsClicked = true;

            BrokerName = brokerName;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lblBrokerName.Text = BrokerName;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void CloseAllPopup()
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        private async void btnOk_Clicked(object sender, EventArgs e)
        {
            CloseAllPopup();
        }
    }
}