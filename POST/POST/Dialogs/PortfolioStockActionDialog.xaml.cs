﻿using POST.Constants;
using POST.Models;
using POST.Services;
using POST.Views.StockViews;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PortfolioStockActionDialog : PopupPage
    {
        private readonly decimal price, volNet;
        private OrderAction orderAction;

        public static readonly BindableProperty StockIdProperty = BindableProperty.Create("StockId", typeof(StockId?), typeof(StockOverviewView), defaultValue: null);
        public StockId? StockId
        {
            get => (StockId?)GetValue(StockIdProperty);
            set => SetValue(StockIdProperty, value);
        }

        public PortfolioStockActionDialog(decimal price, decimal volNet)
        {
            InitializeComponent();
            CloseWhenBackgroundIsClicked = true;
            this.price = price;
            this.volNet = volNet;
        }

        public void SetStock(StockId? stockId)
        {
            StockId = stockId;
        }

        protected virtual Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.5);
        }

        protected virtual Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(1);
        }

        private async void CloseAllPopup()
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lblTitle.Text = StockId.Value.Code;

            if (Store.StockById.GetState().TryGetValue(StockId.Value, out Stock stock2))
            {
                lblTitle.TextDecorations = stock2.TextDecoration;

                if (!string.IsNullOrEmpty(stock2.SpecialNotation2Long))
                {
                    Grid gridSpecialNotationMark = new Grid()
                    {
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Margin = new Thickness(5, 0, 5, 0),
                        Children =
                        {
                            new Frame()
                            {
                                BorderColor = Colors.Accent,
                                BackgroundColor = Colors.Transparent
                            },
                            new StackLayout()
                            {
                                Orientation = StackOrientation.Vertical,
                                Margin = new Thickness(5),
                                Children =
                                {
                                    new StackLayout()
                                    {
                                        Orientation = StackOrientation.Horizontal,
                                        Children =
                                        {
                                            new Grid()
                                            {
                                                HeightRequest = 15,
                                                Children =
                                                {
                                                    new Frame()
                                                    {
                                                        BorderColor = Colors.SpecialNotationForegroundColor,
                                                        BackgroundColor = Colors.SpecialNotationBackgroundColor
                                                    },
                                                    new Label()
                                                    {
                                                        Text = "! Special Notation",
                                                        TextColor = Colors.SpecialNotationForegroundColor,
                                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProBold"],
                                                        VerticalTextAlignment = TextAlignment.Center,
                                                        FontSize = 10.5,
                                                        Padding = new Thickness(2.5,0,2.5,0)
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    new Label()
                                    {
                                        Text = stock2.SpecialNotation2Long,
                                        TextColor = Colors.FieldValueDefaultColor,
                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                        VerticalTextAlignment = TextAlignment.Center,
                                        HorizontalTextAlignment = TextAlignment.Start,
                                        FontSize = 11
                                    }
                                }
                            }

                        }
                    };

                    slRoot.Children.Insert(1, gridSpecialNotationMark);
                }
            }

            lblPrice.Text = price.ToString();
            lblVolNet.Text = volNet.ToString();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void BuyButton_Clicked(object sender, EventArgs e)
        {
            orderAction = OrderAction.Buy;
            Action();
        }

        private void SellButton_Clicked(object sender, EventArgs e)
        {
            orderAction = OrderAction.Sell;
            Action();
        }

        private async void Action()
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                StockId.HasValue &&
                stockById.TryGetValue(StockId.Value, out Stock stock)
                //&& BindingContext is OrderBookLevel orderBookLevel
                )
            {
                OrderDialog orderDialog = null;
                if (orderAction == OrderAction.Buy)
                {
                    if (stock.OpenOrPrevious.HasValue)
                    {
                        orderDialog = new OrderDialog(
                        action: orderAction,
                        stockId: StockId.Value,
                        openPrice: stock.OpenOrPrevious.Value,
                        price: price,
                        priceUnsure: false,
                        sharesPerLot: stock.SharesPerLot.Value
                        );
                        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                        {
                            await PopupNavigation.Instance.PopAsync(true);
                        }

                        await PopupNavigation.Instance.PushAsync(orderDialog);
                    }
                    else
                    {
                        orderDialog = new OrderDialog(
                       action: orderAction,
                       stockId: StockId.Value,
                       openPrice: 0,
                       price: price,
                       priceUnsure: true,
                       sharesPerLot: stock.SharesPerLot.Value
                       );
                        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                        {
                            await PopupNavigation.Instance.PopAsync(true);
                        }

                        await PopupNavigation.Instance.PushAsync(orderDialog);
                    }
                }
                else
                {
                    if (stock.OpenOrPrevious.HasValue)
                    {
                        orderDialog = new OrderDialog(
                        action: orderAction,
                        stockId: StockId.Value,
                        openPrice: stock.OpenOrPrevious.Value,
                        price: price,
                        priceUnsure: false,
                        sharesPerLot: stock.SharesPerLot.Value,
                        volNet: volNet
                        );
                        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                        {
                            await PopupNavigation.Instance.PopAsync(true);
                        }

                        await PopupNavigation.Instance.PushAsync(orderDialog);
                    }
                    else
                    {
                        orderDialog = new OrderDialog(
                          action: orderAction,
                          stockId: StockId.Value,
                          openPrice: 0,
                          price: price,
                          priceUnsure: true,
                          sharesPerLot: stock.SharesPerLot.Value,
                          volNet: volNet
                          );
                        if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                        {
                            await PopupNavigation.Instance.PopAsync(true);
                        }

                        await PopupNavigation.Instance.PushAsync(orderDialog);
                    }
                }
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
            }
        }

    }
}