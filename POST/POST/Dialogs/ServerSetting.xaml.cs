﻿using POST.DataSources;
using POST.Models;
using POST.Services;
using POST.Stores;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ServerSetting : PopupPage
    {
        public ServerSetting()
        {
            InitializeComponent();

            pickerDatafeedServer.ItemsSource = DatafeedServerTypeDict.Dict.Keys.ToList();
            pickerTradingServer.ItemsSource = TradingServerTypeDict.Dict.Keys.ToList();
        }

        private async void ClosePopup()
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAsync();
            }
        }

        private void btnOk_Clicked(object sender, EventArgs e)
        {
            int prevDatafeedServerSelected = AppSettings.DatafeedServer_Selected;

            if (pickerDatafeedServer.SelectedIndex >= 0)
            {
                AppSettings.DatafeedServer_Selected = pickerDatafeedServer.SelectedIndex;
            }
            else
            {
                AppSettings.DatafeedServer_Selected = 0;
            }

            if (pickerTradingServer.SelectedIndex >= 0)
            {
                AppSettings.TradingServer_Selected = pickerTradingServer.SelectedIndex;
            }
            else
            {
                AppSettings.TradingServer_Selected = 0;
            }

            if (prevDatafeedServerSelected != AppSettings.DatafeedServer_Selected)
            {
                DataFeedClient.StateManager.Current.StopClient();
                DataFeedClient.StateManager.Current.StartClient();
            }

            ClosePopup();
            DependencyService.Get<IToastMessage>().Show("Server Settings Saved.");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            pickerDatafeedServer.SelectedIndex = AppSettings.DatafeedServer_Selected;
            pickerTradingServer.SelectedIndex = AppSettings.TradingServer_Selected;
        }
    }
}