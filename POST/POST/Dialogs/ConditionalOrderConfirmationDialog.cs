﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.AthenaMessages.Responses;
using POST.Extensions;
using POST.Models;
using POST.Models.Primitives;
using POST.Services;
using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace POST.Dialogs
{
    public class ConditionalOrderConfirmationDialog : PopupPage
    {
        public OrderAction Action { get; }

        public ConditionalOrder ConditionalOrder { get; private set; }

        private string Condition { get; set; }

        public Lot Lot { get; private set; }

        private readonly Label lblStockCode;

        private string GetTitle()
        {
            switch (Action)
            {
                case OrderAction.Buy: return "BUY";
                case OrderAction.Sell: return "SELL";
                default: throw new NotImplementedException();
            }
        }

        private Color GetTitleBackgroundColor()
        {
            switch (Action)
            {
                case OrderAction.Buy: return Colors.LoseBackground;
                case OrderAction.Sell: return Colors.GainBackground;
                default: throw new NotImplementedException();
            }
        }

        public ConditionalOrderConfirmationDialog(
            ConditionalOrder conditionalOrder,
            long sharesPerLot
        )
        {
            switch (conditionalOrder.OrderAction)
            {
                case OrderAction.Buy:
                case OrderAction.Sell:
                    Action = conditionalOrder.OrderAction;
                    break;
                default:
                    throw new NotImplementedException();
            }
            ConditionalOrder = conditionalOrder;

            Lot = new Lot(ConditionalOrder.O_Volume, sharesPerLot);

            CloseWhenBackgroundIsClicked = false;
            Animation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Center,
                PositionOut = MoveAnimationOptions.Center,
                ScaleIn = 1.2,
                ScaleOut = 0.8,
                DurationIn = 400,
                DurationOut = 300,
                EasingIn = Easing.SinOut,
                EasingOut = Easing.SinIn,
                HasBackgroundAnimation = true
            };

            if (ConditionalOrder.Condition.Substring(0, 2) == "10")
            {

                System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
                string format = "MM-dd-yyyy HH:mm:ss";
                if (DateTime.TryParseExact(ConditionalOrder.TimeCondition, format, provider, System.Globalization.DateTimeStyles.None, out DateTime dateTimeTmp))
                {
                    Condition = dateTimeTmp.ToString("ddd dd MMM yyyy HH:mm:ss");
                }
                else
                {
                    // If TryParseExact Failed
                    Condition = "";
                }
            }
            else
            {
                if (ConditionalOrder.Condition.Substring(0, 2) == "00")
                {
                    Condition = "Last Done Price ";
                }
                else if (ConditionalOrder.Condition.Substring(0, 2) == "01")
                {
                    Condition = "Best Bid Price ";
                }
                else if (ConditionalOrder.Condition.Substring(0, 2) == "02")
                {
                    Condition = "Best Offer Price ";
                }

                if (ConditionalOrder.Condition.Substring(2, 1) == "0")
                {
                    Condition += ">= ";
                }
                else if (ConditionalOrder.Condition.Substring(2, 1) == "1")
                {
                    Condition += "<= ";
                }

                Condition += ConditionalOrder.C_Price.ToString("N0");

                if (ConditionalOrder.Condition.Substring(3, 1) == "1")
                {
                    Condition += " ";

                    if (ConditionalOrder.Condition.Substring(4, 1) == "0")
                    {
                        Condition += "\n AND Best Bid Volume ";
                    }
                    else if (ConditionalOrder.Condition.Substring(4, 1) == "1")
                    {
                        Condition += "\n AND Best Offer Volume ";
                    }

                    if (ConditionalOrder.Condition.Substring(5, 1) == "0")
                    {
                        Condition += ">= ";
                    }
                    else if (ConditionalOrder.Condition.Substring(5, 1) == "1")
                    {
                        Condition += "<= ";
                    }

                    Condition += ConditionalOrder.C_Volume.ToString("N0");
                }
            }

            lblStockCode = new Label
            {
                Text = ConditionalOrder.StockCode,
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                TextColor = Colors.PageTextDialogColor,
                HorizontalTextAlignment = TextAlignment.End,
                Margin = new Thickness(0, 0, 10, 0)
            };

            if (Store.StockById.GetState().TryGetValue(new StockId(ConditionalOrder.StockCode, MarketType.RG), out Stock stock) && stock.Previous.HasValue)
            {
                lblStockCode.TextDecorations = stock.TextDecoration;
            }

            Content = GetFormView(0);
        }

        private View GetFormView(double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 300,
                Spacing = 1,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            },
                            new Label {
                                Text = "Conditional Order Confirmation",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Code",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            lblStockCode
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Price",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{ConditionalOrder.O_Price:N0}",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0),
                            },
                            new Label {
                                Text = "IDR",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Vol",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{ConditionalOrder.O_Volume:N0}",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0)
                            },
                            new Label {
                                Text = "Lot",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Total",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{ConditionalOrder.O_Price * Lot.Shares.Value:N0}",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0)
                            },
                            new Label {
                                Text = "IDR",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Condition",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = Condition,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(5, 20, 5, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "Cancel",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () =>  {if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAllAsync(); } })
                            }.Apply(button => Grid.SetColumn(button, 0)),
                            new Button {
                                Text = "Create",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.White,
                                BackgroundColor = GetTitleBackgroundColor(),
                                Command = new Command(async () => await OnConfirmCreateAsync())
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private async Task OnConfirmCreateAsync()
        {
            try
            {
                IsEnabled = false;

                string brokerAccountId = Store.AthenaSession.GetState().BrokerAccountId;

                if (ConditionalOrder.OrderAction == OrderAction.Buy)
                {
                    AthenaResponse athenaResponse = await AthenaClient.Current.PlaceBuyConditionalOrderAsync(
                    brokerAccountId: brokerAccountId,
                    stockCode: ConditionalOrder.StockCode,
                    boardCode: ConditionalOrder.MarketCode,
                    price: ConditionalOrder.O_Price.ToString(),
                    volume: ConditionalOrder.O_Volume.ToString(),
                    condition: ConditionalOrder.Condition,
                    priceCondition: ConditionalOrder.C_Price.ToString(),
                    volCondition: ConditionalOrder.C_Volume.ToString(),
                    timeCondition: ConditionalOrder.TimeCondition,
                    expDate: DateTime.Now.ToString("MM-dd-yyyy") + " 00:00:00",
                    cancellationToken: new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);
                }
                else
                {
                    AthenaResponse athenaResponse = await AthenaClient.Current.PlaceSellConditionalOrderAsync(
                    brokerAccountId: brokerAccountId,
                    stockCode: ConditionalOrder.StockCode,
                    boardCode: ConditionalOrder.MarketCode,
                    price: ConditionalOrder.O_Price.ToString(),
                    volume: ConditionalOrder.O_Volume.ToString(),
                    condition: ConditionalOrder.Condition,
                    priceCondition: ConditionalOrder.C_Price.ToString(),
                    volCondition: ConditionalOrder.C_Volume.ToString(),
                    timeCondition: ConditionalOrder.TimeCondition,
                    expDate: DateTime.Now.ToString("MM-dd-yyyy") + " 00:00:00",
                    cancellationToken: new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);
                }

                DependencyService.Get<IToastMessage>().Show("Conditional Order sent.");

                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                await MainPage.NavigationPage.PopToRootAsync(true);
            }
            catch (OperationCanceledException)
            {
                Content = GetExceptionErrorView("Please Refresh and Check your Conditional Order List.", 0);
            }
            catch
            {
                Content = GetExceptionErrorView("Please Refresh and Check your Conditional Order List.", 0);
            }
            finally
            {
                IsEnabled = true;
            }
        }

        private View GetExceptionErrorView(string errorMessage, double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                Spacing = 1,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            },
                            new Label {
                                Text = "Order Status Unknown",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Label {
                        TextColor = Colors.PageTextDialogColor,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                        Margin = new Thickness(20, 10, 20, 20),
                        Text = errorMessage
                    },
                    new Grid {
                        Padding = new Thickness(10, 20, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () => { if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAsync(true); } })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

    }
}
