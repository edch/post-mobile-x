﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.RightsMessages.Responses;
using POST.Extensions;
using POST.Services;
using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace POST.Dialogs
{

    public class RightsWthdrawalDetailDialog : PopupPage
    {
        private readonly RightsUserReqItem rightsUserReqItem = null;

        private long SharesPerLot { get; }

        private string GetTitle()
        {
            return "Exercise Details";
        }

        private Color GetTitleBackgroundColor()
        {
            return Colors.FieldNameColor;
        }

        public RightsWthdrawalDetailDialog(
            RightsUserReqItem rightsUserReqItem,
            long sharesPerLot)
        {
            this.rightsUserReqItem = rightsUserReqItem;
            SharesPerLot = sharesPerLot;
            CloseWhenBackgroundIsClicked = false;
            Animation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Center,
                PositionOut = MoveAnimationOptions.Center,
                ScaleIn = 1.2,
                ScaleOut = 0.8,
                DurationIn = 400,
                DurationOut = 300,
                EasingIn = Easing.SinOut,
                EasingOut = Easing.SinIn,
                HasBackgroundAnimation = true
            };

            Content = GetFormView(0);
        }

        private View GetFormView(double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 300,
                Spacing = 1,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Stock",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = rightsUserReqItem.rightid,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    //new Grid {
                    //    Padding = new Thickness(10),
                    //    Children = {
                    //        new Label {
                    //            Text = "Request Time",
                    //            FontSize = 13,
                    //            FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                    //            TextColor = Colors.PageTextDialogColor
                    //        },
                    //        new Label {
                    //            Text = rightsUserReqItem.requesttime,
                    //            FontSize = 13,
                    //            FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                    //            TextColor = Colors.PageTextDialogColor,
                    //            HorizontalTextAlignment = TextAlignment.End,
                    //            Margin = new Thickness(0, 0, 10, 0)
                    //        }
                    //    }
                    //},
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Shares",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text =$"{rightsUserReqItem.share:N0}",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Value",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text =  $"{rightsUserReqItem.value:N0}",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Status",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = rightsUserReqItem.sstatus,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(5, 20, 5, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "Cancel Exercise",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.White,
                                BackgroundColor = Colors.Accent,
                                Command = new Command(async () => await OnConfirmCancelAsync())
                            }.Apply(button =>
                                {
                                    Grid.SetColumn(button, 0);
                                    {
                                        if(rightsUserReqItem.sstatus != "OPEN")
                                        {
                                            button.BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2);
                                            button.TextColor = Color.DarkGray;
                                        }
                                    }
                                }),
                            new Button {
                                Text = "Close",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () =>  {if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAllAsync(); } })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private View GetErrorView(string errorMessage, double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = "Exercise Canceling Request Status",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Label {
                        TextColor = Colors.PageTextDialogColor,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                        Margin = new Thickness(20, 10, 20, 20),
                        Text = errorMessage
                    },
                    new Grid {
                        Padding = new Thickness(10, 20, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () => {
                                    if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAllAsync(); } await MainPage.NavigationPage.PopToRootAsync(true);
                                    Store.AthenaSession.RefreshSession();
                                })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private View GetResponseView(string message, double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = "Exercise Canceling Response",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Label {
                        TextColor = Colors.PageTextDialogColor,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                        Margin = new Thickness(20, 10, 20, 20),
                        Text = message
                    },
                    new Grid {
                        Padding = new Thickness(10, 20, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () => {
                                    if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAllAsync(); } await MainPage.NavigationPage.PopToRootAsync(true);
                                    Store.AthenaSession.RefreshSession();
                                })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private async Task OnConfirmCancelAsync()
        {
            try
            {
                IsEnabled = false;

                if (rightsUserReqItem.sstatus != "OPEN")
                {
                    DependencyService.Get<IToastMessage>().Show("Exercise Status is not Open. Cannot Cancel Exercise!");
                    return;
                }

                bool AnswerYes = await Application.Current.MainPage.DisplayAlert("Cancel Confirmation", "Are you sure want to cancel the Exercise?", "Ok", "Cancel");
                if (!AnswerYes)
                {
                    return;
                }

                RightsCancelResponse rightsCancelResponse = await AthenaClient.Current.CancelRightsAsync(
                    brokerAccountId: rightsUserReqItem.custId,
                    id: rightsUserReqItem.id.ToString(),
                    cancellationToken: new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token
                    );

                if (rightsCancelResponse != null)
                {
                    Content = GetResponseView(rightsCancelResponse.msg, 0);
                }
                else
                {
                    Content = GetErrorView("Cancel Exercise Request Failed", 0);
                }
            }
            catch
            {
                Content = GetErrorView("Cancel Exercise Request Failed", 0);
            }
            finally
            {
                IsEnabled = true;
            }
        }
    }
}
