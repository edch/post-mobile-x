﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SessionExpiredDialog : PopupPage
    {

        public SessionExpiredDialog()
        {
            InitializeComponent();

            CloseWhenBackgroundIsClicked = true;

        }

        protected virtual Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.5);
        }

        protected virtual Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(1);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void CloseAllPopup()
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        private async void btnOk_Clicked(object sender, EventArgs e)
        {
            CloseAllPopup();
        }

    }
}