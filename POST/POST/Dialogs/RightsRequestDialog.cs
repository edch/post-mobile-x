﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.AthenaMessages.Responses;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.RightsMessages.Responses;
using POST.Extensions;
using POST.Models;
using POST.Services;
using POST.Stores;
using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace POST.Dialogs
{
    public class RightsRequestDialog : PopupPage
    {
        public RightsAvailableItem rightsAvailableItem { get; }
        public StockId StockId { get; }
        //public Lot Lot { get; private set; }
        public decimal Shares { get; private set; }
        public decimal Price { get; private set; }
        public long SharesPerLot { get; }

        private readonly Entry entryPrice;

        private readonly Entry entryTotal;

        private readonly Entry entryVolume;

        private readonly Label lblPrice;

        private readonly Label lblTotal;

        private readonly Label lblVolume;

        private readonly Label lblStockCode;

        public MarketType marketType { get; set; }
        public OrderExpiry orderExpiry { get; set; }

        private readonly bool isChecked = false;

        private class ClientState
        {
            public ThrottledDispatcher<OrderBook, OrderBookMessage> ThrottledDispatcher { get; set; }
        }

        private string GetTitle()
        {
            return "Exercise";
        }

        private Color GetTitleBackgroundColor()
        {
            return Colors.LoseBackground;
        }

        public RightsRequestDialog(
           RightsAvailableItem rightsAvailableItem,
           StockId stockId,
            long sharesPerLot,
            decimal volNet = 0
       )
        {
            this.rightsAvailableItem = rightsAvailableItem;
            StockId = stockId;
            Shares = 0;
            //Lot = new Lot(volNet, sharesPerLot);
            SharesPerLot = sharesPerLot;
            Price = rightsAvailableItem.exPrice;

            entryPrice = new Entry
            {
                Placeholder = "Price",
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                TextColor = Colors.PageTextDialogColor,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                PlaceholderColor = Colors.FieldNameColor,
                Keyboard = Keyboard.Numeric,
                FontSize = 13,
                HorizontalTextAlignment = TextAlignment.End,
                VerticalOptions = LayoutOptions.Center
            };
            entryPrice.TextChanged += EntryPrice_TextChanged;

            entryTotal = new Entry
            {
                Placeholder = "Total",
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                Keyboard = Keyboard.Numeric,
                TextColor = Colors.PageTextDialogColor,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                PlaceholderColor = Colors.FieldNameColor,
                FontSize = 13,
                ReturnType = ReturnType.Go,
                VerticalOptions = LayoutOptions.Center,
                ReturnCommand = new Command(arg =>
                {
                    if (arg is Entry entry)
                    {
                        if (string.IsNullOrWhiteSpace(entry.Text))
                        {
                            DependencyService.Get<IToastMessage>().Show("Please enter total.");
                        }
                        else if (decimal.TryParse(entry.Text, out decimal total))
                        {
                            if (total >= decimal.One && decimal.Remainder(total, decimal.One) == decimal.Zero)
                            {
                                //Lot = new Lot(Math.Floor(total / SharesPerLot / Price), SharesPerLot);
                                Shares = Math.Floor(total / Price);
                                Content = GetFormView(0);
                            }
                            else
                            {
                                DependencyService.Get<IToastMessage>().Show("Invalid total.");
                            }
                        }
                        else
                        {
                            DependencyService.Get<IToastMessage>().Show("Invalid total.");
                        }
                    }
                })
            }.Apply(it =>
            {
                it.TextChanged += EntryPrice_TextChanged;
                it.ReturnCommandParameter = it;
            });

            entryVolume = new Entry
            {
                Placeholder = "1",
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                PlaceholderColor = Colors.FieldNameColor,
                Keyboard = Keyboard.Numeric,
                TextColor = Colors.PageTextDialogColor,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                FontSize = 13,
                VerticalOptions = LayoutOptions.Center,
                ReturnType = ReturnType.Go,
                ReturnCommand = new Command(arg =>
                {
                    if (arg is Entry entry)
                    {
                        if (string.IsNullOrWhiteSpace(entry.Text))
                        {
                            //Lot = new Lot(1, SharesPerLot);
                            Shares = sharesPerLot;
                            Content = GetFormView(0);
                        }
                        else if (decimal.TryParse(entry.Text, out decimal lot))
                        {
                            if (lot >= decimal.One && decimal.Remainder(lot, decimal.One) == decimal.Zero)
                            {
                                //Lot = new Lot(lot, SharesPerLot);
                                Shares = lot;
                                Content = GetFormView(0);
                            }
                            else
                            {
                                DependencyService.Get<IToastMessage>().Show("Invalid volume.");
                            }
                        }
                        else
                        {
                            DependencyService.Get<IToastMessage>().Show("Invalid volume.");
                        }
                    }
                })
            }.Apply(it =>
            {
                it.TextChanged += EntryPrice_TextChanged;
                it.ReturnCommandParameter = it;
            });

            lblPrice = new Label
            {
                TextColor = Colors.PageTextDialogColor,
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                HorizontalTextAlignment = TextAlignment.End,
                Margin = new Thickness(0, 0, 30, 0),
                HorizontalOptions = LayoutOptions.End
            };

            lblVolume = new Label
            {
                TextColor = Colors.PageTextDialogColor,
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                HorizontalTextAlignment = TextAlignment.End,
                Margin = new Thickness(0, 0, 56, 0),
                HorizontalOptions = LayoutOptions.End
            };

            lblTotal = new Label
            {
                TextColor = Colors.PageTextDialogColor,
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                HorizontalTextAlignment = TextAlignment.End,
                Margin = new Thickness(0, 0, 36, 0),
                HorizontalOptions = LayoutOptions.End
            };

            CloseWhenBackgroundIsClicked = false;
            Animation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Center,
                PositionOut = MoveAnimationOptions.Center,
                ScaleIn = 1.2,
                ScaleOut = 0.8,
                DurationIn = 400,
                DurationOut = 300,
                EasingIn = Easing.SinOut,
                EasingOut = Easing.SinIn,
                HasBackgroundAnimation = true
            };

            lblStockCode = new Label
            {
                Text = rightsAvailableItem.rightId,
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                TextColor = Colors.PageTextDialogColor,
                HorizontalTextAlignment = TextAlignment.End,
                Margin = new Thickness(0, 0, 10, 0)
            };

            Content = GetFormView(0);
        }

        protected override void OnAppearing()
        {
        }

        protected override void OnDisappearing()
        {
        }

        private void EntryPrice_TextChanged(object sender, TextChangedEventArgs e)
        {
            Entry entry = (Entry)sender;
            if (decimal.TryParse(entry.Text, out decimal parsedPrice))
            {
                entry.Text = $"{parsedPrice:N0}";
            }
        }

        private async Task<View> GetVolumeEntryView(double translationX)
        {
            decimal netSharesPorto = 0;
            decimal netSharesCash = 0;
            decimal T0Net = 0;

            try
            {
                PortfolioAI1Response portfolioAI1Response = await Store.AthenaSession.GetState().Client.GetPortfolioAI1Async(Store.AthenaSession.GetState().BrokerAccountId, Store.AthenaSession.GetState().UserId, new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);
                List<PortfolioStock> stockBalanceResponse = new List<PortfolioStock>();
                foreach (PortfStock i in portfolioAI1Response.portfStock)
                {
                    stockBalanceResponse.Add
                        (
                        new PortfolioStock
                            (
                            id: i.id,
                            stockCode: i.stockCode,
                            lastPrice: i.lastPrice,
                            volume: i.volume,
                            average: i.avg,
                            availVolume: i.availVolume,
                            gainLoss: i.gainLoss,
                            haircut: i.haircutVal
                            )
                        );
                }

                if (stockBalanceResponse != null)
                {
                    List<PortfolioStock> lstStockBalanceResponse = stockBalanceResponse.Where(s => (s.StockCode == rightsAvailableItem.rightId && s.AvailableVolume > 0m)).ToList();
                    if (lstStockBalanceResponse != null && lstStockBalanceResponse.Count > 0)
                    {
                        netSharesPorto = lstStockBalanceResponse[0].AvailableVolume;
                    }

                    if (portfolioAI1Response.ARAP.T0Net >= decimal.One && decimal.Remainder(portfolioAI1Response.ARAP.T0Net, decimal.One) == decimal.Zero)
                    {
                        netSharesCash = Math.Floor(portfolioAI1Response.ARAP.T0Net / Price);
                        T0Net = portfolioAI1Response.ARAP.T0Net;
                    }
                }
                else
                {
                    DependencyService.Get<IToastMessage>().Show("Failed to Retreive Account Details.");
                }
            }
            catch
            {
                DependencyService.Get<IToastMessage>().Show("Failed to Retreive Account Details.");
            }

            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 280,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            },
                            new Label {
                                Text = "Set Volume (Shares)",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Grid {
                        Padding = 10,
                        Margin = new Thickness(0),
                        ColumnSpacing = 5,
                        ColumnDefinitions = {
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = GridLength.Star),
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = 60)
                            ,new ColumnDefinition().Apply(gridColumn => gridColumn.Width = 60)
                        },
                        Children = {
                           entryVolume.Apply(entryVolume =>
                                {
                                    entryVolume.Text = "";
                                    Grid.SetColumn(entryVolume, 0);
                                     Device.BeginInvokeOnMainThread(() =>
                                        {
                                            entryVolume.Focus();
                                        });
                                }
                           ),

                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                Margin = new Thickness(0),
                                Padding = new Thickness(0),
                                TextColor = Color.Black,
                                VerticalOptions = LayoutOptions.Center,
                                BackgroundColor = Colors.ButtonBackgroundOnWhiteColor,
                                Command = new Command(() =>
                                    {
                                            if (string.IsNullOrWhiteSpace(entryVolume.Text))
                                            {
                                                DependencyService.Get<IToastMessage>().Show("Please enter volume.");
                                            }
                                            else if (decimal.TryParse(entryVolume.Text, out decimal lot))
                                            {
                                                if (lot >= decimal.One && decimal.Remainder(lot, decimal.One) == decimal.Zero)
                                                {
                                                    //Lot = new Lot(lot, SharesPerLot);
                                                    Shares = lot;
                                                    Content = GetFormView(0);
                                                }
                                                else
                                                {
                                                    DependencyService.Get<IToastMessage>().Show("Invalid volume.");
                                                }
                                            }
                                            else
                                            {
                                                DependencyService.Get<IToastMessage>().Show("Invalid volume.");
                                            }
                                    }
                                )
                            }.Apply(button => Grid.SetColumn(button, 1))
                            ,new Button {
                                Text = "MAX",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                Margin = new Thickness(0),
                                Padding = new Thickness(0),
                                TextColor = Color.Black,
                                VerticalOptions = LayoutOptions.Center,
                                BackgroundColor = Colors.ButtonBackgroundOnWhiteColor,
                                Command = new Command( () =>
                                    {
                                            Shares = Math.Min(netSharesCash,netSharesPorto);
                                            Content = GetFormView(0);
                                    }
                                )
                            }.Apply(button => Grid.SetColumn(button, 2))
                        }
                    },
                    new Label {
                                Text = $"Net Vol: {netSharesPorto:N0} Shares",
                                FontSize = 11,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor =Colors.PageTextDialogColor,
                                Padding = new Thickness(10,0,10,0)
                            },

                    new Label {
                                Text = $"Net T0: {T0Net:N0} IDR",
                                FontSize = 11,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor =Colors.PageTextDialogColor,
                                Padding = new Thickness(10,0,10,10)
                            }
                }
            };
        }

        private async Task<View> GetTotalEntryView(double translationX)
        {
            decimal netSharesPorto = 0;
            decimal netSharesCash = 0;
            decimal T0Net = 0;

            try
            {
                PortfolioAI1Response portfolioAI1Response = await Store.AthenaSession.GetState().Client.GetPortfolioAI1Async(Store.AthenaSession.GetState().BrokerAccountId, Store.AthenaSession.GetState().UserId, new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);
                List<PortfolioStock> stockBalanceResponse = new List<PortfolioStock>();
                foreach (PortfStock i in portfolioAI1Response.portfStock)
                {
                    stockBalanceResponse.Add
                        (
                        new PortfolioStock
                            (
                            id: i.id,
                            stockCode: i.stockCode,
                            lastPrice: i.lastPrice,
                            volume: i.volume,
                            average: i.avg,
                            availVolume: i.availVolume,
                            gainLoss: i.gainLoss,
                            haircut: i.haircutVal
                            )
                        );
                }

                if (stockBalanceResponse != null)
                {
                    List<PortfolioStock> lstStockBalanceResponse = stockBalanceResponse.Where(s => (s.StockCode == rightsAvailableItem.rightId && s.AvailableVolume > 0m)).ToList();
                    if (lstStockBalanceResponse != null && lstStockBalanceResponse.Count > 0)
                    {
                        netSharesPorto = lstStockBalanceResponse[0].AvailableVolume;
                    }

                    if (portfolioAI1Response.ARAP.T0Net >= decimal.One && decimal.Remainder(portfolioAI1Response.ARAP.T0Net, decimal.One) == decimal.Zero)
                    {
                        netSharesCash = Math.Floor(portfolioAI1Response.ARAP.T0Net / Price);
                        T0Net = portfolioAI1Response.ARAP.T0Net;
                    }
                }
                else
                {
                    DependencyService.Get<IToastMessage>().Show("Failed to Retreive Account Details.");
                }
            }
            catch
            {
                DependencyService.Get<IToastMessage>().Show("Failed to Retreive Account Details.");
            }

            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 280,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            },
                            new Label {
                                Text = "Set Total (IDR)",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Grid {
                        Padding = 10,
                        Margin = new Thickness(0),
                        ColumnSpacing = 5,
                        ColumnDefinitions = {
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = GridLength.Star),
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = 60)
                            ,new ColumnDefinition().Apply(gridColumn => gridColumn.Width = 60)
                        },
                        Children = {
                           entryTotal.Apply(entryTotal =>
                                {
                                    entryTotal.Text = "";
                                    Grid.SetColumn(entryTotal, 0);
                                     Device.BeginInvokeOnMainThread(() =>
                                        {
                                            entryTotal.Focus();
                                        });
                                }
                           ),

                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                Margin = new Thickness(0),
                                Padding = new Thickness(0),
                                TextColor = Color.Black,
                                VerticalOptions = LayoutOptions.Center,
                                BackgroundColor = Colors.ButtonBackgroundOnWhiteColor,
                                Command = new Command(() =>
                                    {
                                       if (string.IsNullOrWhiteSpace(entryTotal.Text))
                        {
                            DependencyService.Get<IToastMessage>().Show("Please enter total.");
                        }
                        else if (decimal.TryParse(entryTotal.Text, out decimal total))
                        {
                            if (total >= decimal.One && decimal.Remainder(total, decimal.One) == decimal.Zero)
                            {
                                //Lot = new Lot(Math.Floor(total / SharesPerLot / Price), SharesPerLot);
                                Shares = Math.Floor(total / Price);
                                Content = GetFormView(0);
                            }
                            else
                            {
                                DependencyService.Get<IToastMessage>().Show("Invalid total.");
                            }
                        }
                        else
                        {
                            DependencyService.Get<IToastMessage>().Show("Invalid total.");
                        }
                                    }
                                )
                            }.Apply(button => Grid.SetColumn(button, 1))
                            ,new Button {
                                Text = "MAX",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                Margin = new Thickness(0),
                                Padding = new Thickness(0),
                                TextColor = Color.Black,
                                VerticalOptions = LayoutOptions.Center,
                                BackgroundColor = Colors.ButtonBackgroundOnWhiteColor,
                                Command = new Command( () =>
                                    {
                                        Shares = Math.Min(netSharesCash, netSharesPorto);
                                        Content = GetFormView(0);
                                    }
                                )
                            }.Apply(button => Grid.SetColumn(button, 2))
                        }
                    },
                    new Label {
                                Text = $"Net Vol: {netSharesPorto:N0} Shares",
                                FontSize = 11,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor =Colors.PageTextDialogColor,
                                Padding = new Thickness(10,0,10,0)
                            },

                    new Label {
                                Text = $"Net T0: {T0Net:N0} IDR",
                                FontSize = 11,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor =Colors.PageTextDialogColor,
                                Padding = new Thickness(10,0,10,10)
                            }
                }
            };
        }

        private View GetFormView(double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                WidthRequest = 320,
                Spacing = 0,
                TranslationX = translationX,
                Children = {
                     new StackLayout {
                        BackgroundColor = GetTitleBackgroundColor(),
                        Orientation  = StackOrientation.Horizontal,
                        Spacing = 10,
                        Children =
                        {
                             new StackLayout {
                                Padding = new Thickness(10),
                                Children = {
                                    new Label {
                                        Text = GetTitle(),
                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                        FontSize = 15,
                                        TextColor = Color.White,
                                    }
                                }
                             }
                        }
                     },
                    new StackLayout {
                        Orientation  = StackOrientation.Horizontal,
                        Spacing = 20,
                        Children =
                        {
                            new StackLayout
                            {
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                Orientation = StackOrientation.Vertical,
                                Children =
                                {
                                    new Grid {
                                        Padding = new Thickness(10),
                                        Children = {
                                            new Label {
                                                Text = "Code",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor
                                            },
                                            lblStockCode
                                        }
                                    },
                                    new Grid {
                                        HeightRequest = 20,
                                        Padding = new Thickness(10),
                                        Children = {
                                            new Label {
                                                Text = "Price",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor
                                            },
                                            lblPrice.Apply(lbl => lbl.Text = $"{Price:N0}"),
                                            new Label {
                                                Text = "IDR",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End,
                                                Margin = new Thickness(0, 0, 0, 0)
                                            }
                                        }
                                    },
                                    new Grid {
                                        Padding = new Thickness(10),
                                        Children = {
                                            new Label {
                                                Text = "Vol",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor
                                            },
                                            lblVolume.Apply(lbl => lbl.Text = $"{Shares:N0}"),
                                            new Label {
                                                Text = "Shares",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End,
                                                Margin = new Thickness(0, 0, 10, 0)
                                            },
                                            new Label {
                                                Text = "\u203a",
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End
                                            }
                                        }
                                    }.Apply(grid => {
                                        grid.GestureRecognizers.Add(new TapGestureRecognizer {
                                            Command = new Command(async () => {
                                                grid.BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2);
                                                Content = await GetVolumeEntryView(0);
                                            })
                                        });
                                    }),
                                    new Grid {
                                        HeightRequest = 20,
                                        Padding = new Thickness(10),
                                        Children = {
                                            new Label {
                                                Text = "Total",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor
                                            },
                                            lblTotal.Apply(lbl => lbl.Text = $"{Price * Shares:N0}"),
                                            new Label {
                                                Text = "IDR",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End,
                                                Margin = new Thickness(0, 0, 10, 0)
                                            },
                                            new Label {
                                                Text = "\u203a",
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End
                                            }
                                        }
                                    }.Apply(grid => {
                                        grid.GestureRecognizers.Add(new TapGestureRecognizer {
                                            Command = new Command(async () => {
                                                grid.BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2);
                                                Content = await GetTotalEntryView(0);
                                            })
                                        });
                                    })
                                }
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10, 10, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "Cancel",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Colors.ButtonBackgroundOnWhiteColor,
                                Command = new Command(async () => {if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAsync(true); } })
                            }.Apply(button => Grid.SetColumn(button, 0)),
                            new Button {
                                Text = "CONFIRM",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                BackgroundColor = GetTitleBackgroundColor(),
                                Command = new Command(async () =>
                                {
                                    if (CheckOrderValid())
                                    {
                                            Content = GetFormConfirmationView(0);
                                    };
                                })
                            }.Apply(button =>
                                {
                                    Grid.SetColumn(button, 1);
                                })
                        }
                    }
                }
            };
        }

        private View GetFormConfirmationView(double translationX)
        {
            Label lblStockCode = new Label
            {
                Text = rightsAvailableItem.rightId,
                FontSize = 15,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                TextColor = Colors.PageTextDialogColor,
                HorizontalTextAlignment = TextAlignment.End,
                Margin = new Thickness(0, 0, 10, 0)
            };


            if (Store.StockById.GetState().TryGetValue(StockId, out Stock stock))
            {
                lblStockCode.TextDecorations = stock.TextDecoration;
            }

            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 300,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Code",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor =Colors.PageTextDialogColor
                            },
                            lblStockCode
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Price",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{Price:N0}",
                                FontSize = 15,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0),
                            },
                            new Label {
                                Text = "IDR",
                                FontSize = 13,
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Vol",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{Shares:N0}",
                                FontSize = 15,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 56, 0)
                            },
                            new Label {
                                Text = "Shares",
                                FontSize = 13,
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Total",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{Price * Shares:N0}",
                                FontSize = 15,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0)
                            },
                            new Label {
                                Text = "IDR",
                                FontSize = 13,
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10, 10, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "Cancel",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Colors.ButtonBackgroundOnWhiteColor,
                                Command = new Command(() => Content = GetFormView(0))
                            }.Apply(button => Grid.SetColumn(button, 0)),
                            new Button {
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                BackgroundColor = GetTitleBackgroundColor(),
                                Command = new Command(async () =>
                                    {
                                        AppSettings.ShowAddedConfirmationDialog = (isChecked)? "0":"1";
                                        await OnConfirmOrderAsync();
                                    })
                            }.Apply(button =>
                            {
                                Grid.SetColumn(button, 1);
                               button.Text = "Exercise";
                            })
                        }
                    }
                }
            };
        }

        private View GetErrorView(string errorMessage, double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            },
                            new Label {
                                Text = "Error",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Label {
                        TextColor = Colors.PageTextDialogColor,
                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                        FontSize = 13,
                        Margin = new Thickness(20, 10, 20, 20),
                        Text = errorMessage
                    },
                    new Grid {
                        Padding = new Thickness(10, 20, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(() => {
                                    Content = GetFormView(0);
                                })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private View GetExceptionalErrorView(string errorMessage, double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            },
                            new Label {
                                Text = "Order Status Unknown",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Label {
                        TextColor = Colors.PageTextDialogColor,
                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                        FontSize = 13,
                        Margin = new Thickness(20, 10, 20, 20),
                        Text = errorMessage
                    },
                    new Grid {
                        Padding = new Thickness(10, 20, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () => {if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAsync(true); } })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private View GetResponseView(string message, double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = "Exercise Request Response",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Label {
                        TextColor = Colors.PageTextDialogColor,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                        Margin = new Thickness(20, 10, 20, 20),
                        Text = message
                    },
                    new Grid {
                        Padding = new Thickness(10, 20, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () => {
                                    if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAllAsync(); } await MainPage.NavigationPage.PopToRootAsync(true);
                                })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private bool CheckOrderValid()
        {
            if (Shares <= 0m)
            {
                DependencyService.Get<IToastMessage>().Show("Volume can not null.");
                return false;
            }
            else
            {
                return true;
            }
        }

        private async Task OnConfirmOrderAsync()
        {
            try
            {
                IsEnabled = false;
                PlaceRightsResponse placeRightsResponse = await AthenaClient.Current.PlaceRightsAsync(
                    brokerAccountId: Store.AthenaSession.GetState().BrokerAccountId,
                    userid: Store.AthenaSession.GetState().UserId,
                    custid: Store.AthenaSession.GetState().BrokerAccountId,
                    rightid: rightsAvailableItem.rightId,
                    share: Shares,
                    cancellationToken: new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token
                );
                if (placeRightsResponse != null)
                {
                    Content = GetResponseView(placeRightsResponse.msg, 0);
                }
                else
                {
                    Content = GetErrorView("Rights Request Failed", 0);
                }
            }
            catch (OperationCanceledException)
            {
                Content = GetExceptionalErrorView("Please Refresh and Check your Rights Request List.", 0);
            }
            catch
            {
                Content = GetExceptionalErrorView("Please Refresh and Check your Rights Request List.", 0);
            }
            finally
            {
                IsEnabled = true;
            }
        }
    }
}
