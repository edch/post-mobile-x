﻿using POST.Constants;
using POST.DataSources.AthenaMessages.Responses;
using POST.Extensions;
using POST.Models;
using POST.Models.Primitives;
using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using Xamarin.Forms;

namespace POST.Dialogs
{
    public class OrderInformationDialog : PopupPage
    {

        private const int PRICE_SPREAD_STEPS = 20;
        private const int PRICE_SELECTOR_HEIGHT = 280;

        public OrderAction Action { get; }
        public decimal Price { get; private set; }
        public Lot Lot { get; private set; }
        public long SharesPerLot { get; }
        public Order Order { get; private set; }

        public MarketType marketType { get; set; }
        public OrderExpiry orderExpiry { get; set; }

        private readonly Label lblRG;
        private readonly Label lblTN;
        private readonly Label lblDay;
        private readonly Label lblSession;
        private readonly Label lblStockCode;

        private string GetTitle()
        {
            switch (Action)
            {
                case OrderAction.Buy: return "BUY";
                case OrderAction.Sell: return "SELL";
                default: throw new NotImplementedException();
            }
        }

        private Color GetTitleBackgroundColor()
        {
            switch (Action)
            {
                case OrderAction.Buy: return Colors.LoseBackground;
                case OrderAction.Sell: return Colors.GainBackground;
                default: throw new NotImplementedException();
            }
        }

        public OrderInformationDialog(
            Order order,
            long sharesPerLot
        )
        {
            switch (order.OrderAction)
            {
                case OrderAction.Buy:
                case OrderAction.Sell:
                    Action = order.OrderAction;
                    break;
                default:
                    throw new NotImplementedException();
            }
            Order = order;
            Price = order.Price;
            Lot = new Lot(order.Volume, sharesPerLot);
            SharesPerLot = sharesPerLot;
            marketType = order.MarketType;
            orderExpiry = order.OrderExpiry;

            lblRG = new Label
            {
                Text = "RG",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                WidthRequest = 58,
                HorizontalTextAlignment = TextAlignment.Center
            };

            lblRG.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    marketType = MarketType.RG;
                    setLblMarket();
                })
            });

            lblTN = new Label
            {
                Text = "TN",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                WidthRequest = 58,
                HorizontalTextAlignment = TextAlignment.Center,
            };

            lblTN.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    marketType = MarketType.TN;
                    setLblMarket();
                })
            });

            lblDay = new Label
            {
                Text = "Day",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                WidthRequest = 58,
                HorizontalTextAlignment = TextAlignment.Center
            };

            lblDay.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    orderExpiry = OrderExpiry.DAY;
                    setLblMarket();
                })
            });

            lblSession = new Label
            {
                Text = "Session",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                WidthRequest = 58,
                HorizontalTextAlignment = TextAlignment.Center
            };

            lblSession.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    orderExpiry = OrderExpiry.SESSION;
                    setLblMarket();
                })
            });

            setLblMarket();

            lblStockCode = new Label
            {
                Text = Order.StockCode,
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                TextColor = Colors.PageTextDialogColor,
                HorizontalTextAlignment = TextAlignment.End,
                Margin = new Thickness(0, 0, 10, 0)
            };

            if (Store.StockById.GetState().TryGetValue(new StockId(Order.StockCode, MarketType.RG), out Stock stock))
            {
                lblStockCode.TextDecorations = stock.TextDecoration;
            }

            CloseWhenBackgroundIsClicked = false;
            Animation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Center,
                PositionOut = MoveAnimationOptions.Center,
                ScaleIn = 1.2,
                ScaleOut = 0.8,
                DurationIn = 400,
                DurationOut = 300,
                EasingIn = Easing.SinOut,
                EasingOut = Easing.SinIn,
                HasBackgroundAnimation = true
            };

            Content = GetFormView(0);
        }

        private View GetFormView(double translationX)
        {
            return
                new ScrollView
                {
                    Orientation = ScrollOrientation.Vertical,
                    VerticalOptions = LayoutOptions.Fill,
                    Content = new StackLayout
                    {
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        VerticalOptions = LayoutOptions.CenterAndExpand,
                        WidthRequest = 300,
                        Spacing = 0,
                        BackgroundColor = Colors.PageBackgroundDialogColor,
                        TranslationX = translationX,
                        Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            },
                            new Label {
                                Text = "Order Information",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Code",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            lblStockCode
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Order ID",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = Order.OrderId,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Time",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = Order.Placed,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Price",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{Price:N0}",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0),
                            },
                            new Label {
                                Text = "IDR",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Vol",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{Order.Volume:N0}",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0)
                            },
                            new Label {
                                Text = "Lot",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Remaining Vol",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{Order.RemainingVolume:N0}",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0)
                            },
                            new Label {
                                Text = "Lot",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Traded Vol",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{Order.TradedVolume:N0}",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0)
                            },
                            new Label {
                                Text = "Lot",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Total",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{Price * Lot.Shares.Value:N0}",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0)
                            },
                            new Label {
                                Text = "IDR",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Status",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = Order.OrderStatusCode,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        IsEnabled = false,
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Board",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Grid {
                                HorizontalOptions = LayoutOptions.End
                            }.Apply
                            (
                                grid =>
                                {
                                    if (marketType == MarketType.RG)
                                    {
                                        grid.Children.Add(lblRG);
                                    }
                                    else
                                    {
                                        grid.Children.Add(lblTN);
                                    }
                                }
                            )
                        }
                    },
                    new Grid {
                        IsEnabled = false,
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Expiry",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Grid {
                                HorizontalOptions = LayoutOptions.End
                            }.Apply
                            (
                                grid =>
                                {
                                    if (orderExpiry == OrderExpiry.DAY)
                                    {
                                        grid.Children.Add(lblDay);
                                    }
                                    else
                                    {
                                        grid.Children.Add(lblSession);
                                    }
                                }
                            )
                        }
                    },
                    new Grid {
                        RowDefinitions = new RowDefinitionCollection(){ new RowDefinition { Height = new GridLength(1, GridUnitType.Star) }, new RowDefinition { Height = new GridLength(1, GridUnitType.Auto)}},
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Reject Note",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            }.Apply(label => Grid.SetRow(label, 0)),
                            new Label {
                                Text = Order.RejectNote,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                VerticalTextAlignment = TextAlignment.Center,
                                HorizontalTextAlignment = TextAlignment.Start
                            }.Apply(label => Grid.SetRow(label, 1))
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10, 20, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProBold"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () =>  {if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAllAsync(); } })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
                    }
                };

        }

        private void setLblMarket()
        {
            if (marketType == MarketType.TN)
            {
                lblRG.TextColor = Colors.FieldNameColor;
                lblRG.BackgroundColor = Colors.ButtonBackgroundOnWhiteColor;

                lblTN.TextColor = Colors.FieldValueDefaultColor;
                lblTN.BackgroundColor = Colors.Accent;
            }
            else
            {
                lblTN.TextColor = Colors.FieldNameColor;
                lblTN.BackgroundColor = Colors.ButtonBackgroundOnWhiteColor;

                lblRG.TextColor = Colors.FieldValueDefaultColor;
                lblRG.BackgroundColor = Colors.Accent;
            }

            if (orderExpiry == OrderExpiry.SESSION)
            {
                lblDay.TextColor = Colors.FieldNameColor;
                lblDay.BackgroundColor = Colors.ButtonBackgroundOnWhiteColor;

                lblSession.TextColor = Colors.FieldValueDefaultColor;
                lblSession.BackgroundColor = Colors.Accent;
            }
            else
            {
                lblSession.TextColor = Colors.FieldNameColor;
                lblSession.BackgroundColor = Colors.ButtonBackgroundOnWhiteColor;

                lblDay.TextColor = Colors.FieldValueDefaultColor;
                lblDay.BackgroundColor = Colors.Accent;
            }
        }


    }
}
