﻿using POST.Services;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RenameFilterGroupDialog : PopupPage
    {
        private readonly string OldName = "";

        public RenameFilterGroupDialog(string oldName)
        {
            InitializeComponent();

            CloseWhenBackgroundIsClicked = true;

            OldName = oldName;
        }

        protected virtual Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.5);
        }

        protected virtual Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(1);
        }

        private async void CloseAllPopup()
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lblTitle.Text = "Rename Filter Group - " + OldName;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void CancelFilterGroupButton_Clicked(object sender, EventArgs e)
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        private async void RenameFilterGroupButton_Clicked(object sender, EventArgs e)
        {
            string newName = entryFilterGroupName.Text;

            if (string.IsNullOrWhiteSpace(newName))
            {
                DependencyService.Get<IToastMessage>().Show("Group name is empty.");
            }
            else if (string.Compare(OldName, newName, true) == 0)
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }
            }
            else if (Store.CustomRunningTradeFilterStockIdsStore.GetState().isAnyGroup(newName) || Store.DefaultRunningTradeFilterStockIdsStore.GetState().isAnyGroup(newName))
            {
                DependencyService.Get<IToastMessage>().Show("Group name already exist.");
            }
            else
            {
                Store.CustomRunningTradeFilterStockIdsStore.RenameGroup(OldName, newName);
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }
            }
        }

    }
}