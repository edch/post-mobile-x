﻿using POST.Models;
using POST.Services;
using POST.Views.StockViews;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderBookActionDialog : PopupPage
    {
        private readonly decimal price;
        private OrderAction orderAction;

        public static readonly BindableProperty StockIdProperty = BindableProperty.Create("StockId", typeof(StockId?), typeof(StockOverviewView), defaultValue: null);
        public StockId? StockId
        {
            get => (StockId?)GetValue(StockIdProperty);
            set => SetValue(StockIdProperty, value);
        }

        public OrderBookActionDialog(decimal price)
        {
            InitializeComponent();
            CloseWhenBackgroundIsClicked = true;
            this.price = price;
        }

        public void SetStock(StockId? stockId)
        {
            StockId = stockId;
        }

        protected virtual Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.5);
        }

        protected virtual Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(1);
        }

        private async void CloseAllPopup()
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lblTitle.Text = StockId.Value.Code;
            lblPrice.Text = price.ToString();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void BuyButton_Clicked(object sender, EventArgs e)
        {
            orderAction = OrderAction.Buy;
            Action();
        }

        private void SellButton_Clicked(object sender, EventArgs e)
        {
            orderAction = OrderAction.Sell;
            Action();
        }

        private async void Action()
        {
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                await PopupNavigation.Instance.PushAsync(new TradingAccountSelector());
            }
            if (Store.AthenaSession.GetState()?.BrokerAccountId == null)
            {
                return;
            }

            if (Store.StockById.GetState() is IReadOnlyDictionary<StockId, Stock> stockById &&
                StockId.HasValue &&
                stockById.TryGetValue(StockId.Value, out Stock stock)
                //&& BindingContext is OrderBookLevel orderBookLevel
                )
            {
                OrderDialog orderDialog = new OrderDialog(
                    action: orderAction,
                    stockId: StockId.Value,
                    openPrice: stock.OpenOrPrevious.Value,
                    price: price,
                    priceUnsure: false,
                    sharesPerLot: stock.SharesPerLot.Value
                );
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAsync(true);
                }

                await PopupNavigation.Instance.PushAsync(orderDialog);
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Can't place order right now.");
            }
        }

    }
}