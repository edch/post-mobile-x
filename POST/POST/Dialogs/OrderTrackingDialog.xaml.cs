﻿using POST.DataSources;
using POST.DataSources.AthenaMessages.Responses;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.Models;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderTrackingDialog : PopupPage
    {
        public StockId StockId;

        public Order Order;

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        private IList<OrderTracking> lstOrderTracking = null;

        private bool _timerRunning = false;

        private bool hasScrolled = false;

        private bool isVisible = false;

        private class ClientState
        {
        }

        private readonly DataFeedClientStateManager<ClientState> _clientStateManager;

        public OrderTrackingDialog(StockId stockId, Order order)
        {
            InitializeComponent();
            CloseWhenBackgroundIsClicked = true;
            Order = order;
            StockId = stockId;

            string BuyorSell = (order.OrderAction == OrderAction.Buy) ? "B" : "O";
            string OrderTrackingCode = order.StockCode + "." + order.MarketCode.ToString() + "." + BuyorSell + "." + order.Price.ToString();

            Store.StockById.GetState().TryGetValue(stockId, out Stock stockTmp);
            Store.OrderTrackingStore.Clear(OrderTrackingCode, order.JsxId, stockTmp.SharesPerLot.Value);

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
              initialState: new ClientState(),
              clientFactory: state =>
              {
                  DataFeedClient client = new DataFeedClient();
                  client.Connected += async sender =>
                  {
                      OrderTrackingListView.IsRefreshing = true;
                      await sender.SendAsync(new SubscribeStockQuoteRequest(stockId));
                      await sender.SendAsync(new SubscribeOrderTrackingRequest(OrderTrackingCode));
                  };
                  client.DataReceived += (sender, arg) =>
                  {
                      switch (arg.Message)
                      {
                          case OrderTrackingMessage orderTrackingMessage:
                              Store.OrderTrackingStore.Dispatch(orderTrackingMessage);
                              break;
                          case StockQuoteMessage stockQuoteMessage:
                              Store.StockById.UpdateQuotes(new List<StockQuoteMessage> { stockQuoteMessage });
                              break;
                          default:
                              break;
                      }
                  };
                  Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                  return client;
              }
              );
        }

        protected override void OnAppearing()
        {
            isVisible = true;

            base.OnAppearing();

            lblTitle.Text = StockId.Code;

            if (Store.StockById.GetState().TryGetValue(new StockId(Order.StockCode, MarketType.RG), out Stock stock2))
            {
                lblTitle.TextDecorations = stock2.TextDecoration;
            }

            _clientStateManager.StartClient();

            _observers.Add(Store.StockById
                .Select(stockById => stockById.TryGetValue(StockId, out Stock stock) ? stock : null)
                .Subscribe(stock =>
                {
                    lblPrice.Text = $"{stock.LastOrPrevious:N0}";
                    lblPrice.TextColor = stock.ChangeForeground;
                })
            );

            lblJsxId.Text = Order.JsxId;
            lblPriceBuySell.Text = $"{Order.Price:N0}";
            lblVol.Text = $"{Order.Volume:N0}";
            lblBuySell.Text = Order.OrderAction.ToString();
            lblQueue.Text = "";
            lblStatus.Text = "";

            if (!_timerRunning)
            {
                Device.StartTimer(TimeSpan.FromSeconds(3), () =>
                {
                    if (isVisible)
                    {
                        OrderTrackingListView.IsRefreshing = false;
                        UpdateListSourceAndView();
                    }

                    if (!isVisible)
                    {
                        _timerRunning = false;
                    }

                    return isVisible;
                });
                _timerRunning = true;
            }
        }

        protected override void OnDisappearing()
        {
            isVisible = false;

            base.OnDisappearing();

            Store.OrderTrackingStore.Clear("", "", 0);

            _clientStateManager.StopClient();

            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
        }

        private async void UpdateListSourceAndView()
        {
            lstOrderTracking = Store.OrderTrackingStore.GetState().OrderBy(ot => ot.OrderNo).ToList();

            if (lstOrderTracking != null)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    OrderTrackingListView.ItemsSource = lstOrderTracking;
                });

                OrderTracking orderTracking = null;

                try
                {
                    orderTracking = lstOrderTracking.Single(ot => ot.OrderNo.ToString() == Order.JsxId);
                }
                catch (Exception)
                {

                }

                if (orderTracking != null)
                {
                    lblStatus.Text = orderTracking.Status;

                    decimal queue = 0;
                    if (orderTracking.Status != "D")
                    {
                        IEnumerable<OrderTracking> lstOrderTrackingTmp = lstOrderTracking.Where(ot => ot.Status == "Q");

                        foreach (OrderTracking ot in lstOrderTrackingTmp)
                        {
                            if (ot.OrderNoString == Order.JsxId)
                            {
                                break;
                            }
                            if (ot.Remaining > 0)
                            {
                                queue += (ot.Volume - ot.Remaining);
                            }
                            else
                            {
                                queue += ot.Volume;
                            }
                        }
                    }
                    lblQueue.Text = $"{Math.Floor(queue / Store.OrderTrackingStore.GetState().SharesPerLot):N0}";

                    if (!hasScrolled)
                    {
                        hasScrolled = true;
                        int delay = (int)Math.Ceiling(lstOrderTracking.Count * 1.5);
                        await Task.Delay(delay);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            OrderTrackingListView.ScrollTo(orderTracking, ScrollToPosition.Center, true);
                        });
                    }
                }
            }
        }

    }
}