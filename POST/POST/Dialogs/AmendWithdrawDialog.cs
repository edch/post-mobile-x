﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.AthenaMessages.Responses;
using POST.DataSources.DataFeedMessages;
using POST.DataSources.DataFeedRequests;
using POST.DataSources.RemarksMessages.Responses;
using POST.Extensions;
using POST.Models;
using POST.Models.Primitives;
using POST.Services;
using POST.Stores;
using POST.ViewCells;
using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace POST.Dialogs
{
    public class AmendWithdrawDialog : PopupPage
    {

        private const int PRICE_SPREAD_STEPS = 20;
        private const int PRICE_SELECTOR_HEIGHT = 280;

        public OrderAction Action { get; }
        public StockId StockId { get; }
        public decimal OpenPrice { get; private set; }
        public decimal Price { get; private set; }
        public Lot Lot { get; private set; }
        public long SharesPerLot { get; }
        public Order Order { get; private set; }

        private RemarksResponse remarksResponse = null;

        private readonly Entry entryPrice;

        private readonly Entry entryTotal;

        private readonly Entry entryVolume;

        private readonly Label lblPrice;

        private readonly Label lblTotal;

        private readonly Label lblVolume;

        private readonly ListView OrderBookListView;

        private bool _timerRunning = false;

        private bool hasScrolled = false;

        private bool isVisible = false;

        private bool isFirst = true;

        private IList<OrderBookLevelTwoColumn> lstOrderBookLevelTwoColumn = null;

        public MarketType marketType { get; set; }
        public OrderExpiry orderExpiry { get; set; }

        private readonly Label lblRG;
        private readonly Label lblTN;
        private readonly Label lblDay;
        private readonly Label lblSession;
        private readonly Label lblCheckedBoxBlankBlack;
        private readonly Label lblCheckedBoxBlankWhite;
        private readonly Label lblCheckedBoxChecked;
        private readonly Label lblStockCode;
        private readonly Button BtnPlus5Vol;
        private readonly Button BtnPlus10Vol;
        private readonly Button BtnPlus25Vol;
        private readonly Button BtnPlus50Vol;
        private readonly Button BtnPlus100Vol;
        private readonly Button BtnPlus500Vol;

        private bool isChecked = false;

        private class ClientState
        {
            public ThrottledDispatcher<OrderBook, OrderBookMessage> ThrottledDispatcher { get; set; }
        }

        private DataFeedClientStateManager<ClientState> _clientStateManager;
        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();

        private string GetTitle()
        {
            switch (Action)
            {
                case OrderAction.Buy: return "BUY";
                case OrderAction.Sell: return "SELL";
                default: throw new NotImplementedException();
            }
        }

        private Color GetTitleBackgroundColor()
        {
            switch (Action)
            {
                case OrderAction.Buy: return Colors.LoseBackground;
                case OrderAction.Sell: return Colors.GainBackground;
                default: throw new NotImplementedException();
            }
        }

        private Color GetPriceBackgroundColor()
        {
            switch (Action)
            {
                case OrderAction.Buy: return Colors.BidBackground_WhiteTheme;
                case OrderAction.Sell: return Colors.AskBackground_WhiteTheme;
                default: throw new NotImplementedException();
            }
        }

        private readonly Label StockPriceLabel;
        private readonly Label StockChangeGlyphLabel;
        private readonly Label StockChangePercentLabel;
        private readonly Label StockChangePointLabel;

        public AmendWithdrawDialog(
            Order order,
            decimal openPrice,
            long sharesPerLot
        )
        {
            switch (order.OrderAction)
            {
                case OrderAction.Buy:
                case OrderAction.Sell:
                    Action = order.OrderAction;
                    break;
                default:
                    throw new NotImplementedException();
            }
            Order = order;
            OpenPrice = openPrice;
            Price = order.Price;
            Lot = new Lot(order.RemainingVolume, sharesPerLot);
            SharesPerLot = sharesPerLot;
            marketType = order.MarketType;
            orderExpiry = order.OrderExpiry;
            StockId = new StockId(order.StockCode, order.MarketType);

            lblRG = new Label
            {
                Text = "RG",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                WidthRequest = 58,
                HorizontalTextAlignment = TextAlignment.Center
            };

            lblRG.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    marketType = MarketType.RG;
                    setLblMarket();
                })
            });

            lblTN = new Label
            {
                Text = "TN",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                WidthRequest = 58,
                HorizontalTextAlignment = TextAlignment.Center,
            };

            lblTN.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    marketType = MarketType.TN;
                    setLblMarket();
                })
            });

            lblDay = new Label
            {
                Text = "Day",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                WidthRequest = 58,
                HorizontalTextAlignment = TextAlignment.Center
            };

            lblDay.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    orderExpiry = OrderExpiry.DAY;
                    setLblMarket();
                })
            });

            lblSession = new Label
            {
                Text = "Session",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                WidthRequest = 58,
                HorizontalTextAlignment = TextAlignment.Center
            };

            lblSession.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    orderExpiry = OrderExpiry.SESSION;
                    setLblMarket();
                })
            });

            lblCheckedBoxBlankBlack = new Label
            {
                Text = "\uf0c8",
                FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                FontSize = 15,
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };

            lblCheckedBoxBlankWhite = new Label
            {
                Text = "\uf0c8",
                FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                FontSize = 12,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Margin = new Thickness(0.2, 0.2, 0, 0.2)
            };

            lblCheckedBoxChecked = new Label
            {
                Text = "\uf14a",
                FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                FontSize = 15,
                TextColor = Colors.Accent,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };

            SetLabelCheckedDontShow();

            setLblMarket();

            entryPrice = new Entry
            {
                Placeholder = "Price",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                TextColor = Colors.PageTextDialogColor,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                PlaceholderColor = Colors.FieldNameColor,
                Keyboard = Keyboard.Numeric,
                HorizontalTextAlignment = TextAlignment.End,
                VerticalOptions = LayoutOptions.Center
            };
            entryPrice.TextChanged += EntryPrice_TextChanged;

            entryTotal = new Entry
            {
                Placeholder = "Total",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                Keyboard = Keyboard.Numeric,
                TextColor = Colors.PageTextDialogColor,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                PlaceholderColor = Colors.FieldNameColor,
                ReturnType = ReturnType.Go,
                VerticalOptions = LayoutOptions.Center,
                ReturnCommand = new Command(arg =>
                {
                    if (arg is Entry entry)
                    {
                        if (string.IsNullOrWhiteSpace(entry.Text))
                        {
                            DependencyService.Get<IToastMessage>().Show("Please enter total.");
                        }
                        else if (decimal.TryParse(entry.Text, out decimal total))
                        {
                            if (total >= decimal.One && decimal.Remainder(total, decimal.One) == decimal.Zero)
                            {
                                Lot = new Lot(Math.Floor(total / SharesPerLot / Price), SharesPerLot);
                                Content = GetFormView(0);
                            }
                            else
                            {
                                DependencyService.Get<IToastMessage>().Show("Invalid total.");
                            }
                        }
                        else
                        {
                            DependencyService.Get<IToastMessage>().Show("Invalid total.");
                        }
                    }
                })
            }.Apply(it =>
            {
                it.TextChanged += EntryPrice_TextChanged;
                it.ReturnCommandParameter = it;
            });

            entryVolume = new Entry
            {
                Placeholder = "1",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                PlaceholderColor = Colors.FieldNameColor,
                Keyboard = Keyboard.Numeric,
                TextColor = Colors.PageTextDialogColor,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                VerticalOptions = LayoutOptions.Center,
                ReturnType = ReturnType.Go,
                ReturnCommand = new Command(arg =>
                {
                    if (arg is Entry entry)
                    {
                        if (string.IsNullOrWhiteSpace(entry.Text))
                        {
                            Lot = new Lot(1, SharesPerLot);
                            Content = GetFormView(0);
                        }
                        else if (decimal.TryParse(entry.Text, out decimal lot))
                        {
                            if (lot >= decimal.One && decimal.Remainder(lot, decimal.One) == decimal.Zero)
                            {
                                Lot = new Lot(lot, SharesPerLot);
                                Content = GetFormView(0);
                            }
                            else
                            {
                                DependencyService.Get<IToastMessage>().Show("Invalid volume.");
                            }
                        }
                        else
                        {
                            DependencyService.Get<IToastMessage>().Show("Invalid volume.");
                        }
                    }
                })
            }.Apply(it =>
            {
                it.TextChanged += EntryPrice_TextChanged;
                it.ReturnCommandParameter = it;
            });

            lblPrice = new Label
            {
                TextColor = Colors.PageTextDialogColor,
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                HorizontalTextAlignment = TextAlignment.End,
                Margin = new Thickness(0, 0, 36, 0),
                HorizontalOptions = LayoutOptions.End
            };

            lblVolume = new Label
            {
                TextColor = Colors.PageTextDialogColor,
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                HorizontalTextAlignment = TextAlignment.End,
                Margin = new Thickness(0, 0, 36, 0),
                HorizontalOptions = LayoutOptions.End
            };

            lblTotal = new Label
            {
                TextColor = Colors.PageTextDialogColor,
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                HorizontalTextAlignment = TextAlignment.End,
                Margin = new Thickness(0, 0, 36, 0),
                HorizontalOptions = LayoutOptions.End
            };

            StockPriceLabel = new Label
            {
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 15,
                VerticalOptions = LayoutOptions.Center
            };
            StockChangeGlyphLabel = new Label
            {
                FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                FontSize = 13,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center
            };
            StockChangePointLabel = new Label
            {
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 13,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.End
            };
            StockChangePercentLabel = new Label
            {
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                FontSize = 13,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Start
            };

            CloseWhenBackgroundIsClicked = false;
            Animation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Center,
                PositionOut = MoveAnimationOptions.Center,
                ScaleIn = 1.2,
                ScaleOut = 0.8,
                DurationIn = 400,
                DurationOut = 300,
                EasingIn = Easing.SinOut,
                EasingOut = Easing.SinIn,
                HasBackgroundAnimation = true
            };

            OrderBookListView = new ListView(ListViewCachingStrategy.RecycleElement)
            {
                RowHeight = 30,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                ItemTemplate = new DataTemplate(() => new OrderBookLevelTwoColumnCell() { })
            };
            OrderBookListView.ItemTapped += OrderBookListView_ItemTapped;
            OrderBookListView.IsRefreshing = true;

            lblStockCode = new Label
            {
                Text = Order.StockCode,
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                TextColor = Colors.PageTextDialogColor,
                HorizontalTextAlignment = TextAlignment.End,
                Margin = new Thickness(0, 0, 10, 0)
            };

            Store.OrderBookTwoColumn.Clear(StockId);
            if (Store.StockById.GetState().TryGetValue(StockId, out Stock stock) && stock.Previous.HasValue)
            {
                Store.OrderBookTwoColumn.SetPrevious(stock.Previous.Value);
            }
            if (stock != null)
            {
                lblStockCode.TextDecorations = stock.TextDecoration;
            }

            _clientStateManager = new DataFeedClientStateManager<ClientState>(
                initialState: new ClientState
                {
                    ThrottledDispatcher = new ThrottledDispatcher<OrderBook, OrderBookMessage>(
                        store: Store.OrderBookTwoColumn,
                        actionDispatcher: messages =>
                        {
                            return Store.OrderBookTwoColumn.BatchUpdate(messages);
                        },
                        coalesceInterval: TimeSpan.FromSeconds(1),
                        flushInterval: TimeSpan.FromSeconds(2)
                    )
                },
                clientFactory: state =>
                {
                    DataFeedClient client = new DataFeedClient();
                    client.Connected += async sender =>
                    {
                        await sender.SendAsync(new SubscribeStockQuoteRequest(StockId));
                        await sender.SendAsync(new SubscribeOrderBookRequest(StockId));
                    };
                    client.DataReceived += (sender, arg) =>
                    {
                        switch (arg.Message)
                        {
                            case StockQuoteMessage stockQuoteMessage:
                                if (isFirst)
                                {
                                    if (OpenPrice == 0)
                                    {
                                        OpenPrice = stockQuoteMessage.Previous.Value;
                                    }

                                    isFirst = false;
                                }
                                Store.StockById.UpdateQuotes(new List<StockQuoteMessage> { stockQuoteMessage });
                                break;
                            case OrderBookMessage orderBookMessage:
                                state.ThrottledDispatcher.Dispatch(orderBookMessage);
                                break;
                            default:
                                break;
                        }
                    };
                    Device.BeginInvokeOnMainThread(async () => await client.ConnectAsync());
                    return client;
                }
                );

            _clientStateManager.StartClient();

            _observers.Add(Store.StockById
              .Select(stockById => stockById.TryGetValue(StockId, out Stock stock2) ? stock2 : null)
              .Subscribe(stock2 =>
              {
                  if (stock2.LastOrPrevious.HasValue)
                  {
                      Store.OrderBookTwoColumn.SetLastPrice(stock2.LastOrPrevious.Value);
                  }
                  if (stock2.Previous.HasValue)
                  {
                      Store.OrderBookTwoColumn.SetPrevious(stock2.Previous.Value);
                  }
                  UpdateTitleView(stock2);
              }
              )
          );

            BtnPlus5Vol = new Button
            {
                Text = "+5",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                Margin = new Thickness(0),
                Padding = new Thickness(0),
                TextColor = Color.Black,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Colors.Add1Color,
                Command = new Command(() =>
                {
                    AddVolume(5);
                })
            };

            BtnPlus10Vol = new Button
            {
                Text = "+10",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                Margin = new Thickness(0),
                Padding = new Thickness(0),
                TextColor = Color.Black,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Colors.Add2Color,
                Command = new Command(() =>
                {
                    AddVolume(10);
                })
            };

            BtnPlus25Vol = new Button
            {
                Text = "+25",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                Margin = new Thickness(0),
                Padding = new Thickness(0),
                TextColor = Color.Black,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Colors.Add3Color,
                Command = new Command(() =>
                {
                    AddVolume(25);
                })
            };

            BtnPlus50Vol = new Button
            {
                Text = "+50",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                Margin = new Thickness(0),
                Padding = new Thickness(0),
                TextColor = Color.Black,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Colors.Add4Color,
                Command = new Command(() =>
                {
                    AddVolume(50);
                })
            };

            BtnPlus100Vol = new Button
            {
                Text = "+100",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                Margin = new Thickness(0),
                Padding = new Thickness(0),
                TextColor = Color.Black,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Colors.Add5Color,
                Command = new Command(() =>
                {
                    AddVolume(100);
                })
            };

            BtnPlus500Vol = new Button
            {
                Text = "+500",
                FontSize = 13,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                Margin = new Thickness(0),
                Padding = new Thickness(0),
                TextColor = Color.Black,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Colors.Add6Color,
                Command = new Command(() =>
                {
                    AddVolume(500);
                })
            };

            GetRemarks();

            Content = GetFormView(0);
        }

        private async void GetRemarks()
        {
            try
            {
                remarksResponse = await RemarksClient.Default.GetRemarksAsync(Order.StockCode, new CancellationTokenSource(TimeSpan.FromSeconds(10)).Token);
            }
            catch { }
        }

        private void AddVolume(decimal addVol)
        {
            if (string.IsNullOrWhiteSpace(entryVolume.Text))
            {
                Lot = new Lot(addVol, SharesPerLot);
            }
            else if (decimal.TryParse(entryVolume.Text, out decimal lot))
            {
                if (lot >= decimal.One && decimal.Remainder(lot, decimal.One) == decimal.Zero)
                {
                    Lot = new Lot((lot + addVol), SharesPerLot);
                }
                else
                {
                    Lot = new Lot(addVol, SharesPerLot);
                }
            }
            else
            {
                DependencyService.Get<IToastMessage>().Show("Invalid volume.");
            }
            entryVolume.Text = Lot.Value.ToString();
        }

        private void SetLabelCheckedDontShow()
        {
            if (isChecked)
            {
                lblCheckedBoxBlankBlack.IsVisible = false;
                lblCheckedBoxBlankWhite.IsVisible = false;
                lblCheckedBoxChecked.IsVisible = true;
            }
            else
            {
                lblCheckedBoxBlankBlack.IsVisible = true;
                lblCheckedBoxBlankWhite.IsVisible = true;
                lblCheckedBoxChecked.IsVisible = false;
            }
        }

        private void ChangeSettingDontShow()
        {
            isChecked = !isChecked;
        }

        protected override void OnAppearing()
        {
            isVisible = true;

            if (!_timerRunning)
            {
                Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                {
                    if (isVisible)
                    {
                        UpdateListSourceAndView();
                    }

                    if (!isVisible)
                    {
                        _timerRunning = false;
                    }

                    return isVisible;
                });
                _timerRunning = true;
            }
        }

        protected override void OnDisappearing()
        {
            isVisible = false;

            _clientStateManager.StopClient();

            _clientStateManager = null;

            Store.OrderBookTwoColumn.Clear(StockId);

            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
        }

        private void UpdateListSourceAndView()
        {
            if (OrderBookListView.IsRefreshing)
            {
                OrderBookListView.IsRefreshing = false;
            }

            lstOrderBookLevelTwoColumn = Store.OrderBookTwoColumn.GetState().GetListOrderBookLevelTwoColumn();

            if (lstOrderBookLevelTwoColumn != null)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    OrderBookListView.ItemsSource = lstOrderBookLevelTwoColumn;
                });

                if (!hasScrolled)
                {
                    hasScrolled = true;
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        OrderBookListView.ScrollTo(lstOrderBookLevelTwoColumn[9], ScrollToPosition.Center, false);
                    });
                }
            }
        }

        private void OrderBookListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is OrderBookLevelTwoColumn orderBookLevelTwoColumn && orderBookLevelTwoColumn.Price != 0)
            {
                Thread th = new Thread(new ThreadStart(BlinkPriceAndTotallLabel))
                {
                    IsBackground = true
                };
                th.Start();

                Price = orderBookLevelTwoColumn.Price;
                lblPrice.Apply(lbl => lbl.Text = $"{Price:N0}");
                lblTotal.Text = $"{Price * Lot.Shares.Value:N0}";
            }
            if (sender is ListView listView)
            {
                listView.SelectedItem = null;
            }
        }

        private void BlinkPriceAndTotallLabel()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    lblPrice.BackgroundColor = Colors.Accent;
                    lblTotal.BackgroundColor = Colors.Accent;

                    lblPrice.TextColor = Colors.FieldValueDefaultColor;
                    lblTotal.TextColor = Colors.FieldValueDefaultColor;

                    lblPrice.FontSize = 16;
                    lblTotal.FontSize = 16;
                }
                catch
                {

                }
            });

            Thread.Sleep(300);

            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    lblPrice.BackgroundColor = Colors.Transparent;
                    lblTotal.BackgroundColor = Colors.Transparent;

                    lblPrice.TextColor = Colors.PageTextDialogColor;
                    lblTotal.TextColor = Colors.PageTextDialogColor;

                    lblPrice.FontSize = 13;
                    lblTotal.FontSize = 13;
                }
                catch
                {

                }
            });
        }


        public bool IsValidPrice(decimal price)
        {
            if (remarksResponse == null || remarksResponse.data.StockType != StockType.ORDI)
            {
                if (price <= 0)
                {
                    return false;
                }
                else if (decimal.Remainder(price, 1m) > 0m)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                if (price <= 0)
                {
                    return false;
                }
                else if (decimal.Remainder(price, 1m) > 0m)
                {
                    return false;
                }
                else if (price < 200m)
                {
                    return decimal.Remainder(price, 1m) == 0m;
                }
                else if (price < 500m)
                {
                    return decimal.Remainder(price, 2m) == 0m;
                }
                else if (price < 2000m)
                {
                    return decimal.Remainder(price, 5m) == 0m;
                }
                else if (price < 5000m)
                {
                    return decimal.Remainder(price, 10m) == 0m;
                }
                else
                {
                    return decimal.Remainder(price, 25m) == 0m;
                }
            }
        }

        private void setLblMarket()
        {
            if (marketType == MarketType.TN)
            {
                lblRG.TextColor = Colors.FieldNameColor;
                lblRG.BackgroundColor = Colors.ButtonBackgroundOnWhiteColor;

                lblTN.TextColor = Colors.FieldValueDefaultColor;
                lblTN.BackgroundColor = Colors.Accent;
            }
            else
            {
                lblTN.TextColor = Colors.FieldNameColor;
                lblTN.BackgroundColor = Colors.ButtonBackgroundOnWhiteColor;

                lblRG.TextColor = Colors.FieldValueDefaultColor;
                lblRG.BackgroundColor = Colors.Accent;
            }

            if (orderExpiry == OrderExpiry.SESSION)
            {
                lblDay.TextColor = Colors.FieldNameColor;
                lblDay.BackgroundColor = Colors.ButtonBackgroundOnWhiteColor;

                lblSession.TextColor = Colors.FieldValueDefaultColor;
                lblSession.BackgroundColor = Colors.Accent;
            }
            else
            {
                lblSession.TextColor = Colors.FieldNameColor;
                lblSession.BackgroundColor = Colors.ButtonBackgroundOnWhiteColor;

                lblDay.TextColor = Colors.FieldValueDefaultColor;
                lblDay.BackgroundColor = Colors.Accent;
            }
        }

        private View GetPriceSelectorView(double translationX)
        {

            Price priceTmp = new Price(Price, OpenPrice, Order.StockCode, (remarksResponse != null) ? remarksResponse.data.StockType : StockType.ORDI);

            entryPrice.Text = Price.ToString();

            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                Spacing = 0,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        Margin = new Thickness(0),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            },
                            new Label {
                                Text = "Set Price",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Grid {
                        Padding = 10,
                        Margin = new Thickness(0),
                        HorizontalOptions = LayoutOptions.End,
                        ColumnSpacing = 15,
                        ColumnDefinitions = {
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = 50),
                            new ColumnDefinition(),
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = 60),
                        },
                        Children = {
                            new Label {
                               Text = "Price",
                               FontSize = 13,
                               FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                               TextColor = Colors.PageTextDialogColor,
                               VerticalOptions = LayoutOptions.Center
                            }.Apply(label => Grid.SetColumn(label, 0)),

                            entryPrice.Apply(entry => Grid.SetColumn(entry, 1)),

                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                Margin = new Thickness(5),
                                TextColor = Color.Black,
                                VerticalOptions = LayoutOptions.Center,
                                BackgroundColor = Colors.ButtonBackgroundOnWhiteColor,
                                Command = new Command(() =>
                                    {
                                        if(decimal.TryParse(entryPrice.Text, out decimal parsedPrice))
                                        {
                                            if (!IsValidPrice(parsedPrice))
                                            {
                                                DependencyService.Get<IToastMessage>().Show("Please enter a valid price.");
                                            }
                                            else
                                            {
                                                Price = parsedPrice;
                                                Content = GetFormView(0);
                                            }
                                        }
                                        else
                                        {
                                            DependencyService.Get<IToastMessage>().Show("Please enter a valid price.");
                                        }
                                    }
                                )
                            }.Apply(button => Grid.SetColumn(button, 2))
                        }
                    },
                    new Grid {
                        Margin = new Thickness(0),
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "L.Low",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = priceTmp.LLow_String,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 40, 0)
                            },
                            new Label {
                                Text = "IDR",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End
                            }
                        }
                    }.Apply(grid => {
                                    grid.GestureRecognizers.Add(new TapGestureRecognizer {
                                        Command = new Command(() => {
                                            if(priceTmp.LLow != 0)
                                            {
                                                Price = priceTmp.LLow;
                                                grid.BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2);
                                                Content = GetFormView(0);
                                            }
                                        })
                                    });
                            }),
                    new ScrollView {
                        Margin = new Thickness(0),
                        HeightRequest = PRICE_SELECTOR_HEIGHT,
                        BackgroundColor = Colors.ButtonBackgroundOnWhiteColor,
                        Content = new StackLayout {
                            Spacing = 0
                        }.Apply(stackLayout => {
                                priceTmp.Spread(PRICE_SPREAD_STEPS).Select(price => {
                                return new Grid {
                                    Padding = new Thickness(10),
                                    HeightRequest = 30,
                                    Children = {
                                        new Label {
                                            Text = price.Value == Price ? "\u2713" : "",
                                            TextColor = Colors.PageTextDialogColor
                                        },
                                        new Label {
                                            Text = $"{price.Value:N0}",
                                            FontSize = 13,
                                            FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                            TextColor = Colors.PageTextDialogColor,
                                            HorizontalTextAlignment = TextAlignment.End,
                                            Margin = new Thickness(0, 0, 40, 0)
                                        },
                                        new Label {
                                            Text = "IDR",
                                            FontSize = 13,
                                            FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                            TextColor = Colors.PageTextDialogColor,
                                            HorizontalTextAlignment = TextAlignment.End
                                        }
                                    }
                                }.Apply(grid => {
                                    grid.GestureRecognizers.Add(new TapGestureRecognizer {
                                        Command = new Command(() => {
                                            Price = price.Value;
                                            grid.BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2);
                                            Content = GetFormView(0);
                                        })
                                    });
                                });
                            }).ToList().ForEach(grid => stackLayout.Children.Add(grid));
                        })
                    }.Apply(scrollView => {
                        Device.BeginInvokeOnMainThread(async () => {
                            List<Price> prices = priceTmp.Spread(PRICE_SPREAD_STEPS).ToList();
                            int rows = prices.Count();
                            int totalHeight = rows * 50;
                            int priceOffset = prices.Count(p => p.Value < Price) * 50;
                            int scrollOffset = Math.Max(priceOffset - (PRICE_SELECTOR_HEIGHT - 50) / 2, 0);
                            await scrollView.ScrollToAsync(
                                x: 0,
                                y: scrollOffset,
                                animated: false
                            );
                        });
                    }),
                    new Grid {
                        Margin = new Thickness(0),
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "L.High",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = priceTmp.LHigh_String,
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 40, 0)
                            },
                            new Label {
                                Text = "IDR",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End
                            }
                        }
                    }.Apply(grid => {
                                    grid.GestureRecognizers.Add(new TapGestureRecognizer {
                                        Command = new Command(() => {
                                            if (priceTmp.LHigh != 0){
                                                Price = priceTmp.LHigh;
                                                grid.BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2);
                                                Content = GetFormView(0);
                                            }
                                        })
                                    });
                            })
                }
            };
        }

        private void EntryPrice_TextChanged(object sender, TextChangedEventArgs e)
        {
            Entry entry = (Entry)sender;
            if (decimal.TryParse(entry.Text, out decimal parsedPrice))
            {
                entry.Text = $"{parsedPrice:N0}";
            }
        }

        private View GetVolumeEntryView(double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            },
                            new Label {
                                Text = "Set Volume (Lot)",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Grid {
                        Padding = 10,
                        Margin = new Thickness(0),
                        ColumnSpacing = 15,
                        ColumnDefinitions = {
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = GridLength.Star),
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = 60),
                        },
                        Children = {
                           entryVolume.Apply(entryVolume =>
                                {
                                    entryVolume.Text = "";
                                    Grid.SetColumn(entryVolume, 0);
                                     Device.BeginInvokeOnMainThread(() =>
                                        {
                                            entryVolume.Focus();
                                        });
                                }
                           ),

                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                Margin = new Thickness(5),
                                TextColor = Color.Black,
                                VerticalOptions = LayoutOptions.Center,
                                BackgroundColor = Colors.ButtonBackgroundOnWhiteColor,
                                Command = new Command(() =>
                                    {
                                            if (string.IsNullOrWhiteSpace(entryVolume.Text))
                                            {
                                                Lot = new Lot(1, SharesPerLot);
                                                Content = GetFormView(0);
                                            }
                                            else if (decimal.TryParse(entryVolume.Text, out decimal lot))
                                            {
                                                if (lot >= decimal.One && decimal.Remainder(lot, decimal.One) == decimal.Zero)
                                                {
                                                    Lot = new Lot(lot, SharesPerLot);
                                                    Content = GetFormView(0);
                                                }
                                                else
                                                {
                                                    DependencyService.Get<IToastMessage>().Show("Invalid volume.");
                                                }
                                            }
                                            else
                                            {
                                                DependencyService.Get<IToastMessage>().Show("Invalid volume.");
                                            }
                                    }
                                )
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    },
                    new Grid {
                        Padding = 10,
                        Margin = new Thickness(0),
                        ColumnSpacing = 5,
                        RowDefinitions =
                        {
                            new RowDefinition().Apply(gridRow => gridRow.Height = GridLength.Auto),
                            new RowDefinition().Apply(gridRow => gridRow.Height = GridLength.Auto)
                        },
                        ColumnDefinitions = {
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = GridLength.Star),
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = GridLength.Star),
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = GridLength.Star),
                        },
                        Children = {
                            BtnPlus5Vol.Apply(button => {Grid.SetColumn(button, 0);  Grid.SetRow(button, 0); }),
                            BtnPlus10Vol.Apply(button => {Grid.SetColumn(button, 1);  Grid.SetRow(button, 0); }),
                            BtnPlus25Vol.Apply(button => {Grid.SetColumn(button, 2);  Grid.SetRow(button, 0); }),
                            BtnPlus50Vol.Apply(button => {Grid.SetColumn(button, 0);  Grid.SetRow(button, 1); }),
                            BtnPlus100Vol.Apply(button => {Grid.SetColumn(button, 1);  Grid.SetRow(button, 1); }),
                            BtnPlus500Vol.Apply(button => {Grid.SetColumn(button, 2);  Grid.SetRow(button, 1); }),
                        }
                    }
                }
            };
        }

        private View GetTotalEntryView(double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            },
                            new Label {
                                Text = "Set Total (IDR)",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Grid {
                        Padding = 10,
                        Margin = new Thickness(0),
                        ColumnSpacing = 15,
                        ColumnDefinitions = {
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = GridLength.Star),
                            new ColumnDefinition().Apply(gridColumn => gridColumn.Width = 60),
                        },
                        Children = {
                           entryTotal.Apply(entryTotal =>
                                {
                                    entryTotal.Text = "";
                                    Grid.SetColumn(entryTotal, 0);
                                     Device.BeginInvokeOnMainThread(() =>
                                        {
                                            entryTotal.Focus();
                                        });
                                }
                           ),

                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                Margin = new Thickness(5),
                                TextColor = Color.Black,
                                VerticalOptions = LayoutOptions.Center,
                                BackgroundColor = Colors.ButtonBackgroundOnWhiteColor,
                                Command = new Command(() =>
                                    {
                                       if (string.IsNullOrWhiteSpace(entryTotal.Text))
                        {
                            DependencyService.Get<IToastMessage>().Show("Please enter total.");
                        }
                        else if (decimal.TryParse(entryTotal.Text, out decimal total))
                        {
                            if (total >= decimal.One && decimal.Remainder(total, decimal.One) == decimal.Zero)
                            {
                                Lot = new Lot(Math.Floor(total / SharesPerLot / Price), SharesPerLot);
                                Content = GetFormView(0);
                            }
                            else
                            {
                                DependencyService.Get<IToastMessage>().Show("Invalid total.");
                            }
                        }
                        else
                        {
                            DependencyService.Get<IToastMessage>().Show("Invalid total.");
                        }
                                    }
                                )
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private void UpdateTitleView(Stock stock)
        {
            if (stock != null && stock.LastOrPrevious.HasValue)
            {
                StockPriceLabel.Text = $"{stock.LastOrPrevious:N0}";
                StockPriceLabel.TextColor = stock.ChangeForeground_WhiteTheme;
                StockChangeGlyphLabel.Text = stock.ChangeGlyph;
                StockChangeGlyphLabel.TextColor = stock.ChangeForeground_WhiteTheme;
                if (stock.ChangePercent.HasValue)
                {
                    StockChangePercentLabel.Text = $"{stock.ChangePercent.Value:N2}%";
                    StockChangePercentLabel.TextColor = stock.ChangeForeground_WhiteTheme;
                }
                else
                {
                    StockChangePercentLabel.Text = string.Empty;
                }
                if (stock.Change.HasValue)
                {
                    StockChangePointLabel.Text = stock.Change.Value.ToString("#,##0;0");
                    StockChangePointLabel.TextColor = stock.ChangeForeground_WhiteTheme;
                }
                else
                {
                    StockChangePointLabel.Text = string.Empty;
                }
            }
            else
            {
                StockPriceLabel.Text = string.Empty;
                StockChangeGlyphLabel.Text = string.Empty;
                StockChangePercentLabel.Text = string.Empty;
                StockChangePointLabel.Text = string.Empty;
            }
        }

        private View GetFormView(double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                WidthRequest = 320,
                Spacing = 0,
                TranslationX = translationX,
                Children = {
                     new StackLayout {
                        BackgroundColor = GetTitleBackgroundColor(),
                        Orientation  = StackOrientation.Horizontal,
                        Spacing = 10,
                        Children =
                        {
                             new StackLayout {
                                Padding = new Thickness(10),
                                Children = {
                                    new Label {
                                        Text = GetTitle(),
                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                        FontSize = 15,
                                        TextColor = Color.White,
                                    },
                                    new Label {
                                        Text = "Order Confirmation",
                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                        FontSize = 15,
                                        TextColor = Color.White,
                                    }
                                }
                             },
                             new Grid
                             {
                                 Margin = new Thickness (10),
                                 BackgroundColor = Color.White,
                                 HorizontalOptions = LayoutOptions.EndAndExpand,
                                 Children =
                                 {
                                     new StackLayout{
                                         Spacing = 10,
                                         Padding = new Thickness(4),
                                 BackgroundColor = GetPriceBackgroundColor(),
                                 Orientation = StackOrientation.Horizontal,
                                 Children =
                                 {
                                     StockPriceLabel,
                                     new Grid
                                     {
                                         RowSpacing = 0,
                                         VerticalOptions = LayoutOptions.Center,
                                         RowDefinitions = new RowDefinitionCollection{ new RowDefinition { Height = GridLength.Star}, new RowDefinition { Height = GridLength.Star } },
                                         Children =
                                         {
                                             new StackLayout
                                             {
                                                 Orientation = StackOrientation.Horizontal,
                                                 HorizontalOptions = LayoutOptions.End,
                                                 VerticalOptions = LayoutOptions.Center,
                                                 Children =
                                                 {
                                                     new Grid
                                                     {
                                                         VerticalOptions = LayoutOptions.Center,
                                                         Children =
                                                         {
                                                             new Label
                                                            {
                                                                    Text = Glyphs.BackgroundSort,
                                                                    FontFamily = (OnPlatform<string>)App.Current.Resources["FontAwesomeProSolid"],
                                                                    FontSize = 13,
                                                                    TextColor = Colors.NetralSortColor_WhiteTheme,
                                                                    HorizontalOptions = LayoutOptions.End,
                                                                    VerticalOptions = LayoutOptions.Center
                                                            },
                                                             StockChangeGlyphLabel
                                                         }
                                                     },
                                                    StockChangePointLabel
                                                 }
                                             }.Apply(sl => Grid.SetRow(sl,0)),

                                                StockChangePercentLabel.Apply(lbl => Grid.SetRow(lbl,1)),
                                         }
                                     }
                                 }
                             }
                                 }
                             }

                        }
                     },
                    new StackLayout {
                        Orientation  = StackOrientation.Horizontal,
                        Spacing = 20,
                        Children =
                        {
                            new Grid{
                                WidthRequest = 120,
                                Children =
                                {
                                    new Grid
                                    {
                                        BackgroundColor = Colors.PageTitleBackgroundDialogColor_WhiteTheme,
                                        HorizontalOptions = LayoutOptions.FillAndExpand,
                                        VerticalOptions = LayoutOptions.FillAndExpand
                                    },
                                    new Grid{
                                        BackgroundColor = Colors.PageBackgroundDialogColor,
                                        Margin = new Thickness(0,0,1,0),
                                        RowSpacing = 0,
                                        RowDefinitions = new RowDefinitionCollection{ new RowDefinition{ Height = 30 }, new RowDefinition { Height = 360} },
                                        Children =
                                        {
                                            new Grid
                                            {
                                                BackgroundColor = Colors.PageTitleBackgroundDialogColor_WhiteTheme,
                                                ColumnDefinitions = new ColumnDefinitionCollection{ new ColumnDefinition { Width = GridLength.Star}, new ColumnDefinition { Width = GridLength.Star } },
                                                Children =
                                                {
                                                   new Label()
                                                    {
                                                        Text = "Price",
                                                        TextColor = Colors.PageTextDialogColor,
                                                        FontSize = 13,
                                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                        VerticalOptions = LayoutOptions.Center,
                                                        Margin = new Thickness(0, 0, 0, -2),
                                                        HorizontalOptions = LayoutOptions.Center
                                                    }.Apply(lbl => Grid.SetColumn(lbl,0)),
                                                   new Label()
                                                    {
                                                        Text = "Vol",
                                                        TextColor = Colors.PageTextDialogColor,
                                                        FontSize = 13,
                                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                        VerticalOptions = LayoutOptions.Center,
                                                        Margin = new Thickness(0, 0, 0, -2),
                                                        HorizontalOptions = LayoutOptions.Center
                                                    }.Apply(lbl => Grid.SetColumn(lbl,1))
                                                }
                                            }.Apply(grid => Grid.SetRow(grid, 0)),
                                            OrderBookListView.Apply(lv =>
                                                {
                                                    Grid.SetRow(lv,1);
                                                    if (lstOrderBookLevelTwoColumn != null) { OrderBookListView.ScrollTo(lstOrderBookLevelTwoColumn[9], ScrollToPosition.Center, false); } }
                                            )
                                        }
                                    }
                                }
                            },
                            new StackLayout
                            {
                                WidthRequest = 200,
                                Orientation = StackOrientation.Vertical,
                                Children =
                                {
                                    new Grid {
                                        Padding = new Thickness(10),
                                        Children = {
                                            new Label {
                                                Text = "Code",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor
                                            },
                                            lblStockCode
                                        }
                                    },
                                    new Grid {
                                        Padding = new Thickness(10),
                                        Children = {
                                            new Label {
                                                Text = "Order ID",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor
                                            },
                                            new Label {
                                                Text = Order.OrderId,
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End,
                                                Margin = new Thickness(0, 0, 10, 0)
                                            }
                                        }
                                    },
                                    new Grid {
                                        Padding = new Thickness(10),
                                        Children = {
                                            new Label {
                                                Text = "Time",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor
                                            },
                                            new Label {
                                                Text = Order.Placed,
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End,
                                                Margin = new Thickness(0, 0, 10, 0)
                                            }
                                        }
                                    },
                                    new Grid {
                                        HeightRequest = 20,
                                        Padding = new Thickness(10),
                                        Children = {
                                            new Label {
                                                Text = "Price",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor
                                            },
                                            lblPrice.Apply(lbl => lbl.Text = $"{Price:N0}"),
                                            new Label {
                                                Text = "IDR",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End,
                                                Margin = new Thickness(0, 0, 10, 0)
                                            },
                                            new Label {
                                                Text = "\u203a",
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End
                                            }
                                        }
                                    }.Apply(grid => {
                                        grid.GestureRecognizers.Add(new TapGestureRecognizer {
                                            Command = new Command(() => {
                                                grid.BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2);
                                                Content = GetPriceSelectorView(0);
                                            })
                                        });
                                    }),
                                    new Grid {
                                        Padding = new Thickness(10),
                                        Children = {
                                            new Label {
                                                Text = "Vol",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor
                                            },
                                            lblVolume.Apply(lbl => lbl.Text = $"{Lot.Value:N0}"),
                                            new Label {
                                                Text = "Lot",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End,
                                                Margin = new Thickness(0, 0, 10, 0)
                                            },
                                            new Label {
                                                Text = "\u203a",
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End
                                            }
                                        }
                                    }.Apply(grid => {
                                        grid.GestureRecognizers.Add(new TapGestureRecognizer {
                                            Command = new Command(() => {
                                                grid.BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2);
                                                Content = GetVolumeEntryView(0);
                                            })
                                        });
                                    }),
                                    new Grid {
                                        HeightRequest = 20,
                                        Padding = new Thickness(10),
                                        Children = {
                                            new Label {
                                                Text = "Total",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor
                                            },
                                            lblTotal.Apply(lbl => lbl.Text = $"{Price * Lot.Shares.Value:N0}"),
                                            new Label {
                                                Text = "IDR",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End,
                                                Margin = new Thickness(0, 0, 10, 0)
                                            },
                                            new Label {
                                                Text = "\u203a",
                                                TextColor = Colors.PageTextDialogColor,
                                                HorizontalTextAlignment = TextAlignment.End
                                            }
                                        }
                                    }.Apply(grid => {
                                        grid.GestureRecognizers.Add(new TapGestureRecognizer {
                                            Command = new Command(() => {
                                                grid.BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2);
                                                Content = GetTotalEntryView(0);
                                            })
                                        });
                                    }),
                                    new Grid {
                                        IsEnabled = false,
                                        Padding = new Thickness(10),
                                        Children = {
                                            new Label {
                                                Text = "Board",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor
                                            },
                                            new Grid {
                                                HorizontalOptions = LayoutOptions.End,
                                                ColumnSpacing = 6,
                                                ColumnDefinitions = {
                                                    new ColumnDefinition(),
                                                    new ColumnDefinition()
                                                },
                                                Children = {
                                                    lblRG.Apply(label => Grid.SetColumn(label, 0)),
                                                    lblTN.Apply(button => Grid.SetColumn(button, 1))
                                                }
                                            }
                                        }
                                    },
                                    new Grid {
                                        IsEnabled = false,
                                        Padding = new Thickness(10),
                                        Children = {
                                            new Label {
                                                Text = "Expiry",
                                                FontSize = 13,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor
                                            },
                                            new Grid {
                                                HorizontalOptions = LayoutOptions.End,
                                                ColumnSpacing = 6,
                                                ColumnDefinitions = {
                                                    new ColumnDefinition(),
                                                    new ColumnDefinition()
                                                },
                                                Children = {
                                                    lblDay.Apply(label => Grid.SetColumn(label, 0)),
                                                    lblSession.Apply(button => Grid.SetColumn(button, 1))
                                                }
                                            }
                                        }
                                    },
                                    new ScrollView
                                    {
                                        Padding = new Thickness(10,0,10,0),
                                        HeightRequest = 100,
                                        VerticalScrollBarVisibility = ScrollBarVisibility.Always,
                                        Orientation = ScrollOrientation.Vertical,
                                        Content =
                                        new StackLayout()
                                        {
                                            Orientation = StackOrientation.Vertical,
                                            Children =
                                            {
                                                new Label {
                                                Text = "-",
                                                FontSize = 11,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor,
                                                VerticalTextAlignment = TextAlignment.Center,
                                                HorizontalTextAlignment = TextAlignment.Start,
                                                LineBreakMode = LineBreakMode.WordWrap
                                            }.Apply(label => {
                                                    if (Store.StockById.GetState().TryGetValue(StockId, out Stock stock))
                                                    {
                                                        label.Text = stock.SpecialNotation1LongNoNewLine;
                                                    }
                                                    else
                                                    {
                                                        label.Text = "";
                                                    }
                                                }),
                                            new Label {
                                                Text = "-",
                                                FontSize = 11,
                                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                                TextColor = Colors.PageTextDialogColor,
                                                VerticalTextAlignment = TextAlignment.Center,
                                                HorizontalTextAlignment = TextAlignment.Start,
                                                LineBreakMode = LineBreakMode.WordWrap
                                            }.Apply(label => {
                                                    if (Store.StockById.GetState().TryGetValue(StockId, out Stock stock))
                                                    {
                                                        label.Text = stock.SpecialNotation2Long;
                                                        label.TextColor = Colors.DangerColor;
                                                    }
                                                    else
                                                    {
                                                        label.Text = "";
                                                    }
                                                })

                                            }
                                        }
                                    }
                     }
                            }
                        }
                    },

                    new Grid {
                        Padding = new Thickness(10, 10, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "Cancel",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () =>  {if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAllAsync(); } })
                            }.Apply(button => Grid.SetColumn(button, 0)),
                            new Button {
                                Text = "Amend",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                BackgroundColor = GetTitleBackgroundColor(),
                                Command = new Command(async () =>
                                {
                                    if (CheckOrderValid())
                                    {
                                        if (AppSettings.ShowAddedConfirmationDialog == "1")
                                        {
                                            Content = GetFormConfirmationView(0);
                                        }
                                        else
                                        {
                                            await OnConfirmAmendAsync();
                                        }
                                    };
                                })
                            }.Apply(button => Grid.SetColumn(button, 1)),
                            new Button {
                                Text = "Withdraw",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                BackgroundColor = GetTitleBackgroundColor(),
                                Command = new Command(async () => await OnConfirmWithdrawAsync())
                            }.Apply(button => Grid.SetColumn(button, 2))
                        }
                    }
                }
            };
        }

        private View GetFormConfirmationView(double translationX)
        {
            Label lblStockCode = new Label
            {
                Text = StockId.Code,
                FontSize = 15,
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                TextColor = Colors.PageTextDialogColor,
                HorizontalTextAlignment = TextAlignment.End,
                Margin = new Thickness(0, 0, 10, 0)
            };

            if (Store.StockById.GetState().TryGetValue(StockId, out Stock stock))
            {
                lblStockCode.TextDecorations = stock.TextDecoration;
            }

            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 300,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Code",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor =Colors.PageTextDialogColor
                            },
                            lblStockCode
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Price",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{Price:N0}",
                                FontSize = 15,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0),
                            },
                            new Label {
                                Text = "IDR",
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Vol",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{Lot.Value:N0}",
                                FontSize = 15,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0)
                            },
                            new Label {
                                Text = "Lot",
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Total",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Label {
                                Text = $"{Price * Lot.Shares.Value:N0}",
                                FontSize = 15,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 50, 0)
                            },
                            new Label {
                                Text = "IDR",
                                TextColor = Colors.PageTextDialogColor,
                                HorizontalTextAlignment = TextAlignment.End,
                                Margin = new Thickness(0, 0, 10, 0)
                            }
                        }
                    },
                    new Grid {
                        IsEnabled = false,
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Board",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Grid {
                                HorizontalOptions = LayoutOptions.End
                            }.Apply
                            (
                                grid =>
                                {
                                    if (marketType == MarketType.RG)
                                    {
                                        grid.Children.Add(lblRG);
                                    }
                                    else
                                    {
                                        grid.Children.Add(lblTN);
                                    }
                                }
                            )
                        }
                    },
                    new Grid {
                        IsEnabled = false,
                        Padding = new Thickness(10),
                        Children = {
                            new Label {
                                Text = "Expiry",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                TextColor = Colors.PageTextDialogColor
                            },
                            new Grid {
                                HorizontalOptions = LayoutOptions.End
                            }.Apply
                            (
                                grid =>
                                {
                                    if (orderExpiry == OrderExpiry.DAY)
                                    {
                                        grid.Children.Add(lblDay);
                                    }
                                    else
                                    {
                                        grid.Children.Add(lblSession);
                                    }
                                }
                            )
                        }
                    },
                    new StackLayout
                    {
                        Margin = new Thickness(10,0,0,0),
                        Orientation  = StackOrientation.Horizontal,
                        Spacing = 2,
                        Children =
                        {
                            new Grid
                            {
                                WidthRequest = 20,
                                Children =
                                {
                                    lblCheckedBoxBlankBlack,
                                    lblCheckedBoxBlankWhite,
                                    lblCheckedBoxChecked
                                }
                            }.Apply(grid =>
                                {
                                    grid.GestureRecognizers.Add(new TapGestureRecognizer
                                        {
                                            Command = new Command(() =>
                                            {
                                                ChangeSettingDontShow();
                                                SetLabelCheckedDontShow();
                                            })
                                        });
                                }),
                            new Grid
                            {
                                Children = {
                                    new Label {
                                    Text = "Don't show this again",
                                    FontSize = 13,
                                    FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                    TextColor = Colors.PageTextDialogColor,
                                    VerticalOptions = LayoutOptions.Center
                                    }
                                }
                            }
                            .Apply(grd =>
                            {
                                grd.GestureRecognizers.Add(new TapGestureRecognizer
                                {
                                    Command = new Command(() =>
                                    {
                                        ChangeSettingDontShow();
                                        SetLabelCheckedDontShow();
                                    })
                                });
                            })
                        }
                    },
                    new Grid {
                        Padding = new Thickness(10, 10, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "Cancel",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Colors.ButtonBackgroundOnWhiteColor,
                                Command = new Command(() => Content = GetFormView(0))
                            }.Apply(button => Grid.SetColumn(button, 0)),
                            new Button {
                                Text = "CONFIRM AMEND",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                BackgroundColor = GetTitleBackgroundColor(),
                                Command = new Command(async () =>
                                    {
                                        AppSettings.ShowAddedConfirmationDialog = (isChecked)? "0":"1";
                                        await OnConfirmAmendAsync();
                                    })
                            }.Apply(button =>
                            {
                                Grid.SetColumn(button, 1);
                            })
                        }
                    }
                }
            };
        }

        private View GetErrorView(string errorMessage, double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            },
                            new Label {
                                Text = "Error",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Label {
                        TextColor = Colors.PageTextDialogColor,
                        FontSize = 13,
                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                        Margin = new Thickness(20, 10, 20, 20),
                        Text = errorMessage
                    },
                    new Grid {
                        Padding = new Thickness(10, 20, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(() => {
                                    Content = GetFormView(0);
                                })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private View GetExceptionErrorView(string errorMessage, double translationX)
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Colors.PageBackgroundDialogColor,
                TranslationX = translationX,
                Children = {
                    new StackLayout {
                        Padding = new Thickness(10),
                        BackgroundColor = GetTitleBackgroundColor(),
                        Children = {
                            new Label {
                                Text = GetTitle(),
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            },
                            new Label {
                                Text = "Order Status Unknown",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                FontSize = 15,
                                TextColor = Color.White,
                            }
                        }
                    },
                    new Label {
                        TextColor = Colors.PageTextDialogColor,
                        FontSize = 13,
                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                        Margin = new Thickness(20, 10, 20, 20),
                        Text = errorMessage
                    },
                    new Grid {
                        Padding = new Thickness(10, 20, 10, 10),
                        ColumnDefinitions = {
                            new ColumnDefinition(),
                            new ColumnDefinition()
                        },
                        Children = {
                            new Button {
                                Text = "OK",
                                FontSize = 13,
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                TextColor = Color.Black,
                                BackgroundColor = Color.FromRgb(0xf2, 0xf2, 0xf2),
                                Command = new Command(async () => {if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAsync(true); } })
                            }.Apply(button => Grid.SetColumn(button, 1))
                        }
                    }
                }
            };
        }

        private async void DisplayResponseMessage(string errorMessage, string brokerAccountId)
        {
            if (errorMessage == "8")
            {
                Content = GetErrorView("Not enough volume.", 0);
            }
            else if (errorMessage == "9")
            {
                Content = GetErrorView("Not enough trading limit.", 0);
            }
            else if (errorMessage == "10")
            {
                Content = GetErrorView("No share.", 0);
            }
            else
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                DependencyService.Get<IToastMessage>().Show("Amend sent.");
                Store.AthenaSession.UseAppWithoutBrokerAccount();
                Store.AthenaSession.UseBrokerAccount(brokerAccountId);
            }
            //DependencyService.Get<IToastMessage>().Show(errorMessage);
        }

        private bool CheckOrderValid()
        {
            if (Lot.Value <= 0m)
            {
                DependencyService.Get<IToastMessage>().Show("Volume can not null.");
                return false;
            }
            else
            {
                return true;
            }
        }

        private async Task OnConfirmAmendAsync()
        {
            try
            {
                IsEnabled = false;
                string brokerAccountId = Store.AthenaSession.GetState().BrokerAccountId;
                AthenaResponse athenaResponse = await AthenaClient.Current.PlaceAmendOrderAsync(
                    brokerAccountId: brokerAccountId,
                    stockCode: Order.StockCode,
                    boardCode: Order.MarketCode,
                    price: Price,
                    volume: Lot.Value,
                    expiry: Order.ExpireFlag,
                    orderId: Order.OrderId,
                    jsxId: Order.JsxId,
                    command: Order.BuyOrSell,
                    cancellationToken: new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token
                );
                if (athenaResponse != null)
                {
                    DisplayResponseMessage(athenaResponse.Message, brokerAccountId);
                }
            }
            catch (OperationCanceledException)
            {
                Content = GetExceptionErrorView("Please Refresh and Check your Order List.", 0);
            }
            catch
            {
                Content = GetExceptionErrorView("Please Refresh and Check your Order List.", 0);
            }
            finally
            {
                IsEnabled = true;
            }
        }

        private async Task OnConfirmWithdrawAsync()
        {
            if (Lot.Value <= 0m)
            {
                DependencyService.Get<IToastMessage>().Show("Volume can not null.");
                return;
            }

            bool AnswerYes = await Application.Current.MainPage.DisplayAlert("Withdraw Confirmation", "Are you sure want to withdraw the order?", "Ok", "Cancel");
            if (!AnswerYes)
            {
                return;
            }

            try
            {
                IsEnabled = false;
                string brokerAccountId = Store.AthenaSession.GetState().BrokerAccountId;
                AthenaResponse athenaResponse = await AthenaClient.Current.PlaceWithdrawOrderAsync(
                    brokerAccountId: brokerAccountId,
                    stockCode: Order.StockCode,
                    boardCode: Order.MarketCode,
                    price: Price,
                    orderId: Order.OrderId,
                    jsxId: Order.JsxId,
                    command: Order.BuyOrSell,
                    cancellationToken: new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token
                );
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                DependencyService.Get<IToastMessage>().Show("Withdraw sent.");
                Store.AthenaSession.RefreshSession();
            }
            catch (OperationCanceledException)
            {
                Content = GetExceptionErrorView("Please Refresh and Check your Order List.", 0);
            }
            catch
            {
                Content = GetExceptionErrorView("Please Refresh and Check your Order List.", 0);
            }
            finally
            {
                IsEnabled = true;
            }
        }
    }
}
