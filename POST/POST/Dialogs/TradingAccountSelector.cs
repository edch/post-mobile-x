﻿using POST.Constants;
using POST.DataSources.AthenaMessages.Responses;
using POST.Extensions;
using POST.Models;
using POST.Services;
using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace POST.Dialogs
{

    public class TradingAccountSelector : PopupPage
    {

        private readonly ICollection<IDisposable> _observers = new List<IDisposable>();
        private readonly ScrollView ScrollView_;
        private readonly StackLayout _stack;
        private readonly Grid Grid_;
        private CancellationTokenSource _cancellationTokenSource;

        private readonly Image imgLogo;

        public TradingAccountSelector()
        {

            //CloseWhenBackgroundIsClicked = Store.AthenaSession.GetState() is AthenaSession athenaSession &&
            //    (athenaSession.BrokerAccountId != null || athenaSession.UseAppWithoutBrokerAccount);                        

            CloseWhenBackgroundIsClicked = true;

            Animation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Center,
                PositionOut = MoveAnimationOptions.Center,
                ScaleIn = 1.2,
                ScaleOut = 0.8,
                DurationIn = 400,
                DurationOut = 300,
                EasingIn = Easing.SinOut,
                EasingOut = Easing.SinIn,
                HasBackgroundAnimation = true
            };

            _stack = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                BackgroundColor = Color.FromRgb(0x5c, 0x00, 0x00),
                Padding = new Thickness(5)
            };

            ScrollView_ = new ScrollView()
            {
                Orientation = ScrollOrientation.Vertical,
                VerticalOptions = LayoutOptions.Fill,
                Content = _stack,
                VerticalScrollBarVisibility = ScrollBarVisibility.Always
            };

            Grid_ = new Grid()
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.Center,
                WidthRequest = 250,
                HeightRequest = 394,
                Padding = new Thickness(10, 20, 10, 20),
                BackgroundColor = Color.FromRgb(0x66, 0x00, 0x00),
                RowDefinitions = {
                    new RowDefinition { Height = GridLength.Auto },
                    new RowDefinition { Height = GridLength.Auto },
                    new RowDefinition { Height = GridLength.Star },
                    new RowDefinition { Height = GridLength.Auto }
                }
            };

            imgLogo = new Image
            {
                Source = "logo.png",
                WidthRequest = 100,
                HorizontalOptions = LayoutOptions.Center,
                Margin = new Thickness(0, 20, 0, 10)
            };

            Label lblChoose = new Label
            {
                Text = "Choose a Trading Account",
                Margin = new Thickness(0, 0, 0, 10),
                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                FontSize = 15,
                TextColor = Colors.FieldValueDefaultColor,
                HorizontalOptions = LayoutOptions.Center,
                HorizontalTextAlignment = TextAlignment.Center
            };

            Grid GridUseWithoutAccount = new Grid
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Start,
                Children = {
                        new Label {
                            Text = "Continue without an account",
                            FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                            FontSize = 15,
                            Margin = new Thickness(20, 15, 20, 10)
                        },
                        new Label {
                            Text = "\u203a",
                            FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                            FontSize = 15,
                            Margin = new Thickness(20, 15, 20, 10),
                            HorizontalTextAlignment = TextAlignment.End
                        }
                    },
                GestureRecognizers = {
                        new TapGestureRecognizer {
                            Command = new Command(async () => {
                                if (IsEnabled) {
                                    Store.AthenaSession.UseAppWithoutBrokerAccount();
                                    if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAsync(true); } }
                            })
                        }
                    }
            };

            Grid_.Children.Add(imgLogo, 0, 0);
            Grid_.Children.Add(lblChoose, 0, 1);
            Grid_.Children.Add(ScrollView_, 0, 2);
            Grid_.Children.Add(GridUseWithoutAccount, 0, 3);

            Content = Grid_;

            //Content = new StackLayout
            //{
            //    HorizontalOptions = LayoutOptions.CenterAndExpand,
            //    VerticalOptions = LayoutOptions.CenterAndExpand,
            //    WidthRequest = 250,
            //    MinimumHeightRequest = 200,
            //    Padding = new Thickness(20),
            //    BackgroundColor = Color.FromRgb(0x66, 0x00, 0x00),
            //    Children = {
            //            new Image {
            //                Source = "logo.png",
            //                WidthRequest = 100,
            //                HorizontalOptions = LayoutOptions.Center,
            //                Margin = new Thickness(0, 20, 0, 10)
            //            },
            //            new Label {
            //                Text = "Choose a Trading Account",
            //                Margin = new Thickness(0, 0, 0, 10),
            //                FontFamily = (string)(OnPlatform<string>)App.Current.Resources["DINProRegular"],
            //                FontSize = 15,
            //                TextColor = Colors.FieldValueDefaultColor,
            //                HorizontalOptions = LayoutOptions.Center,
            //                HorizontalTextAlignment = TextAlignment.Center
            //            },
            //            ScrollView_
            //        }
            //};

        }

        private void SetLogo()
        {
            if (!Store.TradingAccounts.IsSyariahAcc)
            {
                imgLogo.Source = ImageSource.FromFile("logo.png");
            }
            else
            {
                imgLogo.Source = ImageSource.FromFile("icon_syariah.png");
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _cancellationTokenSource = new CancellationTokenSource();

            SetLogo();

            _observers.Add(
                Observable.Merge(
                    Store.TradingAccounts.Select(tradingAccounts => new
                    {
                        TradingAccounts = tradingAccounts,
                        PinLoginForm = Store.PinLoginForm.GetState(),
                        AthenaSession = Store.AthenaSession.GetState()
                    }),
                    Store.PinLoginForm.Select(pinLoginForm => new
                    {
                        TradingAccounts = Store.TradingAccounts.GetState(),
                        PinLoginForm = pinLoginForm,
                        AthenaSession = Store.AthenaSession.GetState()
                    }),
                    Store.AthenaSession.Select(athenaSession => new
                    {
                        TradingAccounts = Store.TradingAccounts.GetState(),
                        PinLoginForm = Store.PinLoginForm.GetState(),
                        AthenaSession = athenaSession
                    })
                )
                .DistinctUntilChanged()
                .Subscribe(async merged =>
                {
                    await UpdateStackAsync(merged.TradingAccounts, merged.PinLoginForm, merged.AthenaSession, _cancellationTokenSource.Token);
                })
            );
        }

        protected override void OnDisappearing()
        {
            foreach (IDisposable observer in _observers)
            {
                observer.Dispose();
            }
            _observers.Clear();
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource = null;
            base.OnDisappearing();
        }

        private async Task UpdateStackAsync(IReadOnlyList<TradingAccount> tradingAccounts, PinLoginForm pinLoginForm, AthenaSession athenaSession, CancellationToken cancellationToken)
        {
            _stack.Children.Clear();
            if (tradingAccounts != null)
            {
                foreach (TradingAccount tradingAccount in tradingAccounts)
                {
                    if (pinLoginForm?.BrokerAccountId == tradingAccount.BrokerAccountId)
                    {
                        if (athenaSession.BrokerAccountId == tradingAccount.BrokerAccountId)
                        {
                            // Selected, logged in
                            _stack.Children.Add(new StackLayout
                            {
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                VerticalOptions = LayoutOptions.Start,
                                BackgroundColor = Color.FromRgb(0x88, 0x00, 0x00),
                                Children = {
                                    new Label {
                                        Text = $"{tradingAccount.BrokerAccountId} - {tradingAccount.CustomerName}",
                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                        FontSize = 15,
                                        Margin = new Thickness(20, 20, 20, 0)
                                    },
                                    new Label {
                                        Text = "Logged in",
                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                        FontSize = 13,
                                        TextColor = Color.FromRgba(0xff, 0xff, 0xff, 0xcc),
                                        Margin = new Thickness(20, 10, 20, 20)
                                    }
                                },
                                GestureRecognizers = {
                                    new TapGestureRecognizer {
                                        Command = new Command(async () => {
                                            if (IsEnabled) {
                                                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0) { await PopupNavigation.Instance.PopAsync(true); } }
                                        })
                                    }
                                }
                            });
                        }
                        else
                        {
                            // Selected, not logged in
                            Entry entryPin = null;
                            Button btnEntryPin = null;

                            entryPin = new Entry
                            {
                                Placeholder = "PIN",
                                IsPassword = true,
                                Keyboard = Keyboard.Default,
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                FontSize = 13,
                                ReturnType = ReturnType.Go,
                                ReturnCommand = new Command(async arg =>
                                {
                                    if (IsEnabled && arg is Entry entry)
                                    {
                                        entry.IsEnabled = false;
                                        if (btnEntryPin != null)
                                        {
                                            btnEntryPin.IsEnabled = false;
                                        }

                                        CancellationTokenSource timeoutSource = new CancellationTokenSource(TimeSpan.FromSeconds(30));
                                        await PinLoginAsync(tradingAccount, entry.Text, CancellationTokenSource.CreateLinkedTokenSource(_cancellationTokenSource.Token, timeoutSource.Token).Token);
                                        entry.IsEnabled = true;
                                        if (btnEntryPin != null)
                                        {
                                            btnEntryPin.IsEnabled = true;
                                        }
                                    }
                                })
                            }.Apply(it =>
                            {
                                it.ReturnCommandParameter = it;
                            });

                            btnEntryPin = new Button
                            {
                                Text = "OK",
                                FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                FontSize = 15,
                                BackgroundColor = Color.Transparent,
                                TextColor = Colors.Accent,
                                VerticalOptions = LayoutOptions.Center,
                                HorizontalOptions = LayoutOptions.Center,
                                WidthRequest = 20,
                                Padding = new Thickness(0),
                                Margin = new Thickness(0)
                            };

                            btnEntryPin.Clicked += async (sender, args) =>
                            {
                                if (IsEnabled && sender is Button btnTmp)
                                {
                                    btnTmp.IsEnabled = false;
                                    if (entryPin != null)
                                    {
                                        entryPin.IsEnabled = false;
                                    }

                                    CancellationTokenSource timeoutSource = new CancellationTokenSource(TimeSpan.FromSeconds(30));
                                    await PinLoginAsync(tradingAccount, entryPin.Text, CancellationTokenSource.CreateLinkedTokenSource(_cancellationTokenSource.Token, timeoutSource.Token).Token);
                                    btnTmp.IsEnabled = true;
                                    if (entryPin != null)
                                    {
                                        entryPin.IsEnabled = true;
                                    }
                                }
                            };

                            _stack.Children.Add(new StackLayout
                            {
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                VerticalOptions = LayoutOptions.Start,
                                BackgroundColor = Color.FromRgb(0x88, 0x00, 0x00),
                                Children = {
                                    new Label {
                                        Text = $"{tradingAccount.BrokerAccountId} - {tradingAccount.CustomerName}",
                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                        FontSize = 15,
                                        Margin = new Thickness(20, 20, 20, 0)
                                    },
                                    new StackLayout
                                    {
                                        Orientation = StackOrientation.Horizontal,
                                        HorizontalOptions = LayoutOptions.FillAndExpand,
                                        VerticalOptions = LayoutOptions.Start,
                                        Margin = new Thickness(20, 10, 12, 20),
                                        Children = {
                                            entryPin,
                                            btnEntryPin
                                        }
                                    }

                                }
                            });
                        }
                    }
                    else
                    {
                        if (athenaSession.BrokerAccountId == tradingAccount.BrokerAccountId)
                        {
                            // Not selected, logged in
                            _stack.Children.Add(new StackLayout
                            {
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                VerticalOptions = LayoutOptions.Start,
                                BackgroundColor = Color.FromRgba(0xff, 0xff, 0xff, 0x11),
                                Children = {
                                    new Label {
                                        Text = $"{tradingAccount.BrokerAccountId} - {tradingAccount.CustomerName}",
                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                        FontSize = 15,
                                        Margin = new Thickness(20, 20, 20, 0)
                                    },
                                    new Label {
                                        Text = "Logged in",
                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProRegular"],
                                        FontSize = 13,
                                        TextColor = Color.FromRgba(0xff, 0xff, 0xff, 0xcc),
                                        Margin = new Thickness(20, 10, 20, 20)
                                    }
                                },
                                GestureRecognizers = {
                                    new TapGestureRecognizer {
                                        Command = new Command(() => {
                                            if (IsEnabled) {
                                                Store.PinLoginForm.SetBrokerAccountId(tradingAccount.BrokerAccountId);
                                            }
                                        })
                                    }
                                }
                            });
                        }
                        else
                        {
                            // Not selected, not logged in
                            _stack.Children.Add(new Grid
                            {
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                VerticalOptions = LayoutOptions.Start,
                                BackgroundColor = Color.FromRgba(0xff, 0xff, 0xff, 0x11),
                                Children = {
                                    new Label {
                                        Text = $"{tradingAccount.BrokerAccountId} - {tradingAccount.CustomerName}",
                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                        FontSize = 15,
                                        Margin = new Thickness(20)
                                    },
                                    new Label {
                                        Text = "\u203a",
                                        FontFamily = (OnPlatform<string>)App.Current.Resources["DINProMedium"],
                                        FontSize = 15,
                                        Margin = new Thickness(20),
                                        HorizontalTextAlignment = TextAlignment.End
                                    }
                                },
                                GestureRecognizers = {
                                    new TapGestureRecognizer {
                                        Command = new Command(() => {
                                            if (IsEnabled) {
                                                Store.PinLoginForm.SetBrokerAccountId(tradingAccount.BrokerAccountId);
                                            }
                                        })
                                    }
                                }
                            });
                        }
                    }
                }
            }
            else
            {
                _stack.Children.Add(new ActivityIndicator
                {
                    IsRunning = true,
                    Margin = new Thickness(20)
                });

                for (; ; )
                {
                    try
                    {
                        TradingAccount[] response = await Store.AthenaSession.GetState().Client.GetTradingAccountsAsync(cancellationToken);
                        if (response != null)
                        {
                            bool isSyariah = false;
                            if (response != null && response.Length > 0)
                            {
                                foreach (TradingAccount tradingAccount in response)
                                {
                                    if (tradingAccount.CustomerType == "S" || tradingAccount.CustomerType == "K")
                                    {
                                        isSyariah = true;
                                        break;
                                    }
                                }
                            }
                            Store.TradingAccounts.IsSyariahAcc = isSyariah;

                            Store.TradingAccounts.SetTradingAccounts(response.ToList());

                            SetLogo();
                        }
                        //                  else if (response.Message != null) {
                        //	DependencyService.Get<IToastMessage>().Show(response.Message);
                        //	await PopupNavigation.Instance.PopAsync(true);
                        //}
                        break;
                    }
                    catch
                    {
                        await Task.Delay(5000);
                        continue;
                    }
                }
            }


        }

        private void TradingAccountSelector_Clicked(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private async Task PinLoginAsync(TradingAccount tradingAccount, string pin, CancellationToken cancellationToken)
        {
            IsEnabled = false;
            try
            {
                if (string.IsNullOrEmpty(pin))
                {
                    DependencyService.Get<IToastMessage>().Show("PIN not Provided");
                }
                else
                {
                    PinLoginResponse pinLoginresponse = await Store.AthenaSession.GetState().Client.PinLoginAsync(tradingAccount.BrokerAccountId, pin, cancellationToken);
                    if (pinLoginresponse.IsSuccessStatusMessage)
                    {
                        PinValidateResponse pinValidateResponse = await Store.AthenaSession.GetState().Client.PinValidateAsync(tradingAccount.BrokerAccountId, _cancellationTokenSource.Token);
                        if (pinValidateResponse.IsSuccessStatusMessage)
                        {
                            Store.AthenaSession.UseBrokerAccount(tradingAccount.BrokerAccountId);
                            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                            {
                                await PopupNavigation.Instance.PopAsync(true);
                            }

                            DependencyService.Get<IToastMessage>().Show($"Using account {tradingAccount.BrokerAccountId} - {tradingAccount.CustomerName}");
                        }
                        else
                        {
                            DependencyService.Get<IToastMessage>().Show("Validate Pin Failed");
                        }
                    }
                    else
                    {
                        DependencyService.Get<IToastMessage>().Show("Pin Login Failed");
                    }
                }
            }
            catch (Exception)
            {
                DependencyService.Get<IToastMessage>().Show("Login Failed");
            }
            finally
            {
                IsEnabled = true;
            }
        }
    }
}
