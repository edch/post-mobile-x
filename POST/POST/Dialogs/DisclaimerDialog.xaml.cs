﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST.Dialogs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DisclaimerDialog : PopupPage
    {
        private readonly string urlManifestDisclaimerResources = "";

        public DisclaimerDialog()
        {
            InitializeComponent();

            CloseWhenBackgroundIsClicked = true;

            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    urlManifestDisclaimerResources = "POST.Droid.Resources.DisclaimerResources.txt";
                    break;
                case Device.iOS:
                    urlManifestDisclaimerResources = "POST.iOS.Resources.DisclaimerResources.txt";
                    break;
                case Device.UWP:
                    urlManifestDisclaimerResources = "POST.UWP.Resources.DisclaimerResources.txt";
                    break;
            }
        }

        private async void LoadTextFromResource()
        {
            Assembly assembly = IntrospectionExtensions.GetTypeInfo(typeof(DisclaimerDialog)).Assembly;
            using (Stream stream = assembly.GetManifestResourceStream(urlManifestDisclaimerResources))
            {
                if (stream != null)
                {
                    string fileText = "";
                    using (StreamReader reader = new System.IO.StreamReader(stream))
                    {
                        fileText = await reader.ReadToEndAsync();
                    }
                    lblContent.Text = fileText;
                }
            }
        }

        protected virtual Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.5);
        }

        protected virtual Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(1);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            LoadTextFromResource();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void CloseAllPopup()
        {
            if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
        }

        private async void btnOk_Clicked(object sender, EventArgs e)
        {
            CloseAllPopup();
        }

    }
}