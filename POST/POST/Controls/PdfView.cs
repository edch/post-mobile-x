﻿using Xamarin.Forms;

namespace POST.Controls
{

    public class PdfView : WebView
    {

        public static readonly BindableProperty UriProperty = BindableProperty.Create("Uri", typeof(string), typeof(PdfView), defaultValue: null);
        public string Uri
        {
            get => (string)GetValue(UriProperty);
            set => SetValue(UriProperty, value);
        }
    }
}
