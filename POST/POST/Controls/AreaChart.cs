﻿using POST.Constants;
using POST.Models;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Linq;
using Xamarin.Forms;

namespace POST.Controls
{

    public class AreaChart : SKCanvasView
    {

        private const int MIN_COLUMNS = 20;
        private const float STROKE_WIDTH = 2f;
        private const float CANDLE_WIDTH_RATIO = 0.6f;

        public static BindableProperty StockChartProperty = BindableProperty.Create("StockChart", typeof(StockChart), typeof(AreaChart));
        public StockChart StockChart
        {
            get => (StockChart)GetValue(StockChartProperty);
            set => SetValue(StockChartProperty, value);
        }

        public static BindableProperty TimeUnitProperty = BindableProperty.Create("TimeUnit", typeof(StockChartTimeUnit), typeof(AreaChart), defaultValue: StockChartTimeUnit.Day);
        public StockChartTimeUnit TimeUnit
        {
            get => (StockChartTimeUnit)GetValue(TimeUnitProperty);
            set => SetValue(TimeUnitProperty, value);
        }

        private float _scale;
        private int _columns;
        private decimal _minPrice;
        private decimal _maxPrice;

        protected override void OnPaintSurface(SKPaintSurfaceEventArgs e)
        {
            base.OnPaintSurface(e);

            if (Width == 0 ||
                Height == 0 ||
                StockChart == null ||
                StockChart.Count == 0)
            {
                return;
            }

            SKImageInfo imageInfo = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;

            _scale = imageInfo.Width / (float)Width;

            _minPrice = StockChart.Min(tuple => tuple.Low);
            _maxPrice = StockChart.Max(tuple => tuple.High);
            _columns = Math.Max(MIN_COLUMNS, StockChart.Count);

            canvas.Clear();

            DrawCandlesticks(canvas);
        }

        private void DrawCandlesticks(SKCanvas canvas)
        {
            for (int col = 0; col < StockChart.Count; col++)
            {
                (float left, float middle, float right) = ToX(col);
                (float openY, float highY, float lowY, float closeY) = ToY(StockChart[col]);

                if (StockChart[col].Close > StockChart[col].Open)
                {
                    // Green stick
                    using (SKPaint paint = new SKPaint
                    {
                        Style = SKPaintStyle.Stroke,
                        StrokeCap = SKStrokeCap.Square,
                        StrokeJoin = SKStrokeJoin.Miter,
                        StrokeWidth = STROKE_WIDTH,
                        Color = Colors.GainForeground.ToSKColor(),
                        IsAntialias = true
                    })
                    {
                        canvas.DrawRect(new SKRect(left, closeY, right, openY), paint);
                        canvas.DrawLine(middle, highY, middle, closeY, paint);
                        canvas.DrawLine(middle, openY, middle, lowY, paint);
                    }
                }
                else if (StockChart[col].Close < StockChart[col].Open)
                {
                    // Red stick
                    using (SKPaint paint = new SKPaint
                    {
                        Style = SKPaintStyle.StrokeAndFill,
                        StrokeCap = SKStrokeCap.Square,
                        StrokeJoin = SKStrokeJoin.Miter,
                        StrokeWidth = STROKE_WIDTH,
                        Color = Colors.LoseForeground.ToSKColor(),
                        IsAntialias = true
                    })
                    {
                        canvas.DrawRect(new SKRect(left, openY, right, closeY), paint);
                        canvas.DrawLine(middle, highY, middle, lowY, paint);
                    }
                }
                else
                {
                    // Yellow stick
                    using (SKPaint paint = new SKPaint
                    {
                        Style = SKPaintStyle.Stroke,
                        StrokeCap = SKStrokeCap.Square,
                        StrokeJoin = SKStrokeJoin.Miter,
                        StrokeWidth = STROKE_WIDTH,
                        Color = Colors.UnchangedForeground.ToSKColor(),
                        IsAntialias = true
                    })
                    {
                        canvas.DrawLine(left, openY, right, closeY, paint);
                        canvas.DrawLine(middle, highY, middle, lowY, paint);
                    }
                }
            }
        }

        private (float left, float middle, float right) ToX(int i)
        {
            return (
                left: (i + (1f - CANDLE_WIDTH_RATIO) / 2f) / _columns * (float)Width * _scale,
                middle: (i + 0.5f) / _columns * (float)Width * _scale,
                right: (i + 0.5f + CANDLE_WIDTH_RATIO / 2f) / _columns * (float)Width * _scale
            );
        }

        private (float openY, float highY, float lowY, float closeY) ToY(StockChartTuple tuple)
        {
            return (
                openY: ToY(tuple.Open),
                highY: ToY(tuple.High),
                lowY: ToY(tuple.Low),
                closeY: ToY(tuple.Close)
            );
        }

        private float ToY(decimal price)
        {
            return _minPrice == _maxPrice ? (float)Height / 2f * _scale : (float)(_maxPrice - price) / (float)(_maxPrice - _minPrice) * (float)Height * _scale;
        }
    }
}
