﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.ApiResponses;
using POST.Models;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Xamarin.Forms;

namespace POST.Controls
{

    public class IntradaySparkLine : SKCanvasView
    {

        private const int MIN_COUNT = 20;
        private const float STROKE_WIDTH = 2f;

        private static readonly Lazy<SKPaint> PREV_LINE_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = new SKColor(0x55, 0x55, 0x55),
            StrokeWidth = STROKE_WIDTH,
            IsAntialias = true,
            PathEffect = SKPathEffect.CreateDash(new[] { 1f, 9f }, 10f),
            StrokeCap = SKStrokeCap.Round
        });
        private static readonly Lazy<SKPaint> GAIN_STROKE_PAINT = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = Colors.GainForeground.ToSKColor(),
            StrokeWidth = STROKE_WIDTH,
            IsAntialias = false,
            StrokeCap = SKStrokeCap.Round,
            StrokeJoin = SKStrokeJoin.Round
        });
        private static readonly Lazy<SKPaint> LOSE_STROKE_PAINT = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = Colors.LoseForeground.ToSKColor(),
            StrokeWidth = STROKE_WIDTH,
            IsAntialias = false,
            StrokeCap = SKStrokeCap.Round,
            StrokeJoin = SKStrokeJoin.Round
        });
        private static readonly Lazy<SKPaint> UNCHANGED_STROKE_PAINT = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = Colors.UnchangedForeground.ToSKColor(),
            StrokeWidth = STROKE_WIDTH,
            IsAntialias = false,
            StrokeCap = SKStrokeCap.Round,
            StrokeJoin = SKStrokeJoin.Round
        });
        private static readonly Lazy<SKPaint> WHITE_STROKE_PAINT = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = SKColors.White,
            StrokeWidth = STROKE_WIDTH,
            IsAntialias = false,
            StrokeCap = SKStrokeCap.Round,
            StrokeJoin = SKStrokeJoin.Round
        });
        private static readonly Lazy<SKPaint> GAIN_FILL_PAINT = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Fill,
            Color = Colors.GainForeground.ToSKColor().WithAlpha(0x10),
            IsAntialias = false
        });
        private static readonly Lazy<SKPaint> LOSE_FILL_PAINT = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Fill,
            Color = Colors.LoseForeground.ToSKColor().WithAlpha(0x10),
            IsAntialias = false
        });
        private static readonly Lazy<SKPaint> UNCHANGED_FILL_PAINT = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Fill,
            Color = Colors.UnchangedForeground.ToSKColor().WithAlpha(0x10),
            IsAntialias = false
        });
        private static readonly Lazy<SKPaint> WHITE_FILL_PAINT = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Fill,
            Color = SKColors.White.WithAlpha(0x10),
            IsAntialias = false
        });

        private readonly SKPath _path = new SKPath();

        public static BindableProperty StockCodeProperty = BindableProperty.Create("StockCode", typeof(string), typeof(IntradaySparkLine), defaultValue: null, propertyChanged: OnStockCodeChanged);
        public string StockCode
        {
            get => (string)GetValue(StockCodeProperty);
            set => SetValue(StockCodeProperty, value);
        }

        private static readonly ConcurrentDictionary<string, decimal?[]> ClosesByStockCode = new ConcurrentDictionary<string, decimal?[]>();

        public static BindableProperty LabelTextProperty = BindableProperty.Create("LabelText", typeof(string), typeof(IntradaySparkLine), defaultValue: null, defaultBindingMode: BindingMode.TwoWay);
        public string LabelText
        {
            get => (string)GetValue(LabelTextProperty);
            set => SetValue(LabelTextProperty, value);
        }

        public static BindableProperty LabelTextColorProperty = BindableProperty.Create("LabelTextColor", typeof(Color), typeof(IntradaySparkLine), defaultValue: Colors.UndeterminedForeground, defaultBindingMode: BindingMode.TwoWay);
        public Color LabelTextColor
        {
            get => (Color)GetValue(LabelTextColorProperty);
            set => SetValue(LabelTextColorProperty, value);
        }

        public void Refresh()
        {
            OnStockCodeChanged(this, StockCode, StockCode);
        }

        private static async void OnStockCodeChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is IntradaySparkLine sparkLine)
            {
                if (newValue is string stockCode)
                {
                    try
                    {
                        if (ClosesByStockCode.ContainsKey(stockCode))
                        {
                            sparkLine.InvalidateSurface();
                        }
                        IntradaySparkLineData sparkLineData = await IntradaySparkLineClient.Default.GetSparkLinePooledAsync(stockCode, CancellationToken.None);
                        if (sparkLineData != null)
                        {
                            List<decimal?> closes = new List<decimal?>();
                            foreach (string d in sparkLineData.Data)
                            {
                                string tuple = d.TrimStart('[').TrimEnd(']');
                                string[] values = tuple.Split(',');
                                if (values.Length == 4 && decimal.TryParse(values[3], out decimal close))
                                {
                                    closes.Add(close);
                                }
                                else
                                {
                                    closes.Add(null);
                                }
                            }

                            ClosesByStockCode.AddOrUpdate(stockCode, closes.ToArray(), (sc, c) => closes.ToArray());

                            if (closes.Any(close => close.HasValue) &&
                                Store.StockById.GetState().TryGetValue(new StockId(stockCode, MarketType.RG), out Stock stock) &&
                                stock.Previous.HasValue)
                            {
                                decimal last = closes.Where(close => close.HasValue).Last().Value;
                                decimal previous = stock.Previous.Value;
                                if (last > previous)
                                {
                                    sparkLine.LabelText = $"+{(last - previous) / previous * 100m:0.00}%";
                                    sparkLine.LabelTextColor = Colors.GainForeground;
                                }
                                else if (last < previous)
                                {
                                    sparkLine.LabelText = $"-{(previous - last) / previous * 100m:0.00}%";
                                    sparkLine.LabelTextColor = Colors.LoseForeground;
                                }
                                else
                                {
                                    sparkLine.LabelText = $"+0.00%";
                                    sparkLine.LabelTextColor = Colors.UnchangedForeground;
                                }
                            }
                            else
                            {
                                //sparkLine.Closes = null;
                                sparkLine.LabelText = string.Empty;
                            }
                        }
                        else
                        {
                            //sparkLine.Closes = null;
                            sparkLine.LabelText = string.Empty;
                        }
                    }
                    catch
                    {
                        //sparkLine.Closes = null;
                        sparkLine.LabelText = string.Empty;
                    }
                }
                else
                {
                    //sparkLine.Closes = null;
                    sparkLine.LabelText = string.Empty;
                }
                sparkLine.InvalidateSurface();
            }
        }

        private float _scale;
        private int _count;
        private int _offset;
        private decimal _minPrice;
        private decimal _maxPrice;

        /// <summary>
        /// No allocation, boxing, unboxing, construction, deconstruction allowed within this method
        /// </summary>
        protected override void OnPaintSurface(SKPaintSurfaceEventArgs e)
        {
            base.OnPaintSurface(e);

            SKImageInfo imageInfo = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            float x, y;

            canvas.Clear();

            if (Width == 0 ||
                Height == 0 ||
                StockCode == null ||
                !ClosesByStockCode.TryGetValue(StockCode, out decimal?[] closes) ||
                closes.Length == 0 ||
                !closes.Any(close => close.HasValue))
            {
                return;
            }

            _scale = imageInfo.Width / (float)Width;

            decimal? previous = Store.StockById.GetState().TryGetValue(new StockId(StockCode, MarketType.RG), out Stock stock) ? stock.Previous : null;
            _minPrice = closes.Where(close => close.HasValue).Min(close => close.Value);
            _maxPrice = closes.Where(close => close.HasValue).Max(close => close.Value);
            if (previous.HasValue)
            {
                if (_maxPrice - previous.Value > previous.Value - _minPrice)
                {
                    _minPrice = previous.Value - (_maxPrice - previous.Value);
                }
                else
                {
                    _maxPrice = previous.Value + (previous.Value - _minPrice);
                }
                if (_maxPrice < previous.Value * 1.01m)
                {
                    _maxPrice = previous.Value * 1.01m;
                }
                if (_minPrice > previous.Value * 0.99m)
                {
                    _minPrice = previous.Value * 0.99m;
                }
            }

            _count = Math.Max(MIN_COUNT, closes.Length);
            _offset = _count - closes.Length;

            if (previous.HasValue)
            {
                _path.Rewind();
                ToCoordinate(0, previous.Value, out x, out y);
                _path.MoveTo(x, y);
                ToCoordinate(closes.Length - 1, previous.Value, out x, out y);
                _path.LineTo(x, y);
                canvas.DrawPath(_path, PREV_LINE_PAINT_LAZY.Value);
            }

            _path.Rewind();
            if (previous.HasValue)
            {
                ToCoordinate(0, previous.Value, out x, out y);
                _path.MoveTo(x, y);
            }
            for (int i = 1; i < closes.Length; i++)
            {
                if (closes[i].HasValue)
                {
                    ToCoordinate(i, closes[i].Value, out x, out y);
                    _path.LineTo(x, y);
                }
            }

            SKPaint strokePaint;
            SKPaint fillPaint;
            if (previous.HasValue)
            {
                decimal last = closes.Last(close => close.HasValue).Value;
                if (last > previous.Value)
                {
                    strokePaint = GAIN_STROKE_PAINT.Value;
                    fillPaint = GAIN_FILL_PAINT.Value;
                }
                else if (last < previous.Value)
                {
                    strokePaint = LOSE_STROKE_PAINT.Value;
                    fillPaint = LOSE_FILL_PAINT.Value;
                }
                else
                {
                    strokePaint = UNCHANGED_STROKE_PAINT.Value;
                    fillPaint = UNCHANGED_FILL_PAINT.Value;
                }
            }
            else
            {
                strokePaint = WHITE_STROKE_PAINT.Value;
                fillPaint = WHITE_FILL_PAINT.Value;
            }
            canvas.DrawPath(_path, strokePaint);

            if (previous.HasValue)
            {
                ToCoordinate(closes.Length - 1, previous.Value, out x, out y);
                _path.LineTo(x, y);
                canvas.DrawPath(_path, fillPaint);
            }
        }

        private void ToCoordinate(int i, decimal price, out float x, out float y)
        {
            x = ((i + _offset) / (float)_count * ((float)Width - STROKE_WIDTH) + STROKE_WIDTH / 2) * _scale;
            y = (_minPrice == _maxPrice ? (float)Height / 2f : (float)(_maxPrice - price) / (float)(_maxPrice - _minPrice) * ((float)Height - STROKE_WIDTH) + STROKE_WIDTH / 2) * _scale;
        }
    }
}
