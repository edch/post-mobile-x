﻿using POST.Constants;
using POST.DataSources;
using POST.DataSources.ApiResponses;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Linq;
using System.Threading;
using Xamarin.Forms;

namespace POST.Controls
{

    public class SparkLine : SKCanvasView
    {

        private const int MIN_COUNT = 20;
        private const float STROKE_WIDTH = 2f;

        public static BindableProperty StockCodeProperty = BindableProperty.Create("StockCode", typeof(string), typeof(SparkLine), defaultValue: null, propertyChanged: OnStockCodeChanged);
        public string StockCode
        {
            get => (string)GetValue(StockCodeProperty);
            set => SetValue(StockCodeProperty, value);
        }

        public static BindableProperty ClosesProperty = BindableProperty.Create("Closes", typeof(decimal[]), typeof(SparkLine), defaultValue: null, defaultBindingMode: BindingMode.TwoWay);
        public decimal[] Closes
        {
            get => (decimal[])GetValue(ClosesProperty);
            set => SetValue(ClosesProperty, value);
        }

        public static BindableProperty LabelTextProperty = BindableProperty.Create("LabelText", typeof(string), typeof(SparkLine), defaultValue: null, defaultBindingMode: BindingMode.TwoWay);
        public string LabelText
        {
            get => (string)GetValue(LabelTextProperty);
            set => SetValue(LabelTextProperty, value);
        }

        public static BindableProperty LabelTextColorProperty = BindableProperty.Create("LabelTextColor", typeof(Color), typeof(SparkLine), defaultValue: Colors.UndeterminedForeground, defaultBindingMode: BindingMode.TwoWay);
        public Color LabelTextColor
        {
            get => (Color)GetValue(LabelTextColorProperty);
            set => SetValue(LabelTextColorProperty, value);
        }

        private static async void OnStockCodeChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is SparkLine sparkLine)
            {
                if (newValue is string stockCode)
                {
                    try
                    {
                        if (sparkLine.Closes != null)
                        {
                            sparkLine.InvalidateSurface();
                        }
                        SparkLineData sparkLineData = await SparkLineClient.Default.GetSparkLinePooledAsync(stockCode, CancellationToken.None);
                        if (sparkLineData != null)
                        {
                            sparkLine.Closes = sparkLineData.Closes;

                            if (sparkLineData.Closes != null &&
                                sparkLineData.Closes.Any() &&
                                sparkLineData.Closes.First() != 0)
                            {
                                decimal first = sparkLineData.Closes.First();
                                decimal last = sparkLineData.Closes.Last();
                                if (last > first)
                                {
                                    sparkLine.LabelText = $"4 Wk: +{(last - first) / first * 100m:0.00}%";
                                    sparkLine.LabelTextColor = Colors.GainForeground;
                                }
                                else if (last < first)
                                {
                                    sparkLine.LabelText = $"4 Wk: -{(first - last) / first * 100m:0.00}%";
                                    sparkLine.LabelTextColor = Colors.LoseForeground;
                                }
                                else
                                {
                                    sparkLine.LabelText = $"4 Wk: +0.00%";
                                    sparkLine.LabelTextColor = Colors.UnchangedForeground;
                                }
                            }
                            else
                            {
                                sparkLine.Closes = null;
                                sparkLine.LabelText = string.Empty;
                            }
                        }
                        else
                        {
                            sparkLine.Closes = null;
                            sparkLine.LabelText = string.Empty;
                        }
                    }
                    catch
                    {
                        sparkLine.Closes = null;
                        sparkLine.LabelText = string.Empty;
                    }
                }
                else
                {
                    sparkLine.Closes = null;
                    sparkLine.LabelText = string.Empty;
                }
                sparkLine.InvalidateSurface();
            }
        }

        private float _scale;
        private int _count;
        private int _offset;
        private decimal _minPrice;
        private decimal _maxPrice;

        protected override void OnPaintSurface(SKPaintSurfaceEventArgs e)
        {
            base.OnPaintSurface(e);

            SKImageInfo imageInfo = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;

            canvas.Clear();

            if (Width == 0 ||
                Height == 0 ||
                Closes == null ||
                Closes.Length == 0)
            {
                return;
            }

            _scale = imageInfo.Width / (float)Width;

            decimal first = Closes.First();
            _minPrice = Closes.Min();
            _maxPrice = Closes.Max();
            if (first > 0)
            {
                if (_maxPrice - first > first - _minPrice)
                {
                    _minPrice = first - (_maxPrice - first);
                }
                else
                {
                    _maxPrice = first + (first - _minPrice);
                }
                if (_maxPrice < first * 1.01m)
                {
                    _maxPrice = first * 1.01m;
                }
                if (_minPrice > first * 0.99m)
                {
                    _minPrice = first * 0.99m;
                }
            }

            _count = Math.Max(MIN_COUNT, Closes.Length);
            _offset = _count - Closes.Length;

            using (SKPath path = new SKPath())
            {
                (float x, float y) = ToCoordinate(0, first);
                path.MoveTo(x, y);
                (x, y) = ToCoordinate(Closes.Length - 1, first);
                path.LineTo(x, y);

                using (SKPaint paint = new SKPaint
                {
                    Style = SKPaintStyle.Stroke,
                    Color = new SKColor(0x55, 0x55, 0x55),
                    StrokeWidth = STROKE_WIDTH,
                    IsAntialias = true,
                    PathEffect = SKPathEffect.CreateDash(new[] { 1f, 9f }, 10f),
                    StrokeCap = SKStrokeCap.Round
                })
                {
                    canvas.DrawPath(path, paint);
                }
            }

            using (SKPath path = new SKPath())
            {
                (float x, float y) = ToCoordinate(0, first);
                path.MoveTo(x, y);
                for (int i = 1; i < Closes.Length; i++)
                {
                    (x, y) = ToCoordinate(i, Closes[i]);
                    path.LineTo(x, y);
                }

                SKColor color;
                decimal last = Closes.Last();
                if (last > first)
                {
                    color = Colors.GainForeground.ToSKColor();
                }
                else if (last < first)
                {
                    color = Colors.LoseForeground.ToSKColor();
                }
                else
                {
                    color = Colors.UnchangedForeground.ToSKColor();
                }

                using (SKPaint paint = new SKPaint
                {
                    Style = SKPaintStyle.Stroke,
                    Color = color,
                    StrokeWidth = STROKE_WIDTH,
                    IsAntialias = false,
                    StrokeCap = SKStrokeCap.Round,
                    StrokeJoin = SKStrokeJoin.Round
                })
                {
                    canvas.DrawPath(path, paint);
                }

                (x, y) = ToCoordinate(Closes.Length - 1, first);
                path.LineTo(x, y);

                using (SKPaint paint = new SKPaint
                {
                    Style = SKPaintStyle.Fill,
                    Color = color.WithAlpha(0x10),
                    IsAntialias = false
                })
                {
                    canvas.DrawPath(path, paint);
                }
            }
        }

        private (float x, float y) ToCoordinate(int i, decimal price)
        {
            return (
                x: ((i + _offset) / (float)_count * ((float)Width - STROKE_WIDTH) + STROKE_WIDTH / 2) * _scale,
                y: (_minPrice == _maxPrice ? (float)Height / 2f : (float)(_maxPrice - price) / (float)(_maxPrice - _minPrice) * ((float)Height - STROKE_WIDTH) + STROKE_WIDTH / 2) * _scale
            );
        }
    }
}
