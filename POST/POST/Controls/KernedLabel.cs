﻿using Xamarin.Forms;

namespace POST.Controls
{

    public class KernedLabel : Label
    {

        public static readonly BindableProperty LetterSpacingProperty = BindableProperty.Create("LetterSpacing", typeof(float), typeof(KernedLabel), defaultValue: 0f);
        public float LetterSpacing
        {
            get => (float)GetValue(LetterSpacingProperty);
            set => SetValue(LetterSpacingProperty, value);
        }
    }
}
