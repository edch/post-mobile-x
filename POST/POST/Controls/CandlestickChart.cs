﻿using POST.Constants;
using POST.Models;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Linq;
using Xamarin.Forms;

namespace POST.Controls
{

    public class CandlestickChart : SKCanvasView
    {

        private const int MIN_COLUMNS = 20;
        private const float STROKE_WIDTH = 1f;
        private const float CANDLE_WIDTH_RATIO = 0.6f;

        private static readonly Lazy<SKPaint> PRICE_LINE_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = new SKColor(0x17, 0x17, 0x17)
        });
        private static readonly Lazy<SKPaint> PREV_LINE_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            PathEffect = SKPathEffect.CreateDash(new[] { 1f, 9f }, 10f),
            Color = new SKColor(0x55, 0x55, 0x55)
        });
        private static readonly Lazy<SKPaint> VOL_BARS_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.StrokeAndFill,
            IsAntialias = true
        });
        private static readonly Lazy<SKPaint> TIME_LABELS_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Fill,
            Color = SKColors.White,
            IsAntialias = true,
            Typeface = SKTypeface.FromFamilyName((OnPlatform<string>)App.Current.Resources["DINProBold"], SKFontStyleWeight.Black, SKFontStyleWidth.Expanded, SKFontStyleSlant.Upright),
            TextEncoding = SKTextEncoding.Utf32
        });
        private static readonly Lazy<SKPaint> TIME_TICKS_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = new SKColor(0x17, 0x17, 0x17),
            IsAntialias = true
        });
        private static readonly Lazy<SKPaint> MAX_VOL_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Fill,
            Color = SKColors.White,
            IsAntialias = true,
            Typeface = SKTypeface.FromFamilyName((OnPlatform<string>)App.Current.Resources["DINProBold"], SKFontStyleWeight.Black, SKFontStyleWidth.Expanded, SKFontStyleSlant.Upright),
            TextEncoding = SKTextEncoding.Utf32
        });
        //private static readonly Lazy<SKPaint> AREA_FILL_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        //{
        //    Style = SKPaintStyle.Fill,
        //    Color = new SKColor(0x0a, 0x59, 0x37, 0x88),
        //    IsAntialias = true
        //});
        //private static readonly Lazy<SKPaint> AREA_STROKE_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        //{
        //    Style = SKPaintStyle.Stroke,
        //    StrokeCap = SKStrokeCap.Round,
        //    StrokeJoin = SKStrokeJoin.Round,
        //    Color = Colors.GainForeground.ToSKColor(),
        //    IsAntialias = true
        //});
        private static readonly Lazy<SKPaint> AREA_FILL_PAINT_LOSE_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Fill,
            Color = new SKColor(0x79, 0x1c, 0x19, 0x88),
            IsAntialias = true
        });
        private static readonly Lazy<SKPaint> AREA_FILL_PAINT_GAIN_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Fill,
            Color = new SKColor(0x0a, 0x59, 0x37, 0x88),
            IsAntialias = true
        });
        private static readonly Lazy<SKPaint> AREA_STROKE_PAINT_LOSE_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            StrokeCap = SKStrokeCap.Round,
            StrokeJoin = SKStrokeJoin.Round,
            Color = Colors.LoseForeground.ToSKColor(),
            IsAntialias = true
        });
        private static readonly Lazy<SKPaint> AREA_STROKE_PAINT_GAIN_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            StrokeCap = SKStrokeCap.Round,
            StrokeJoin = SKStrokeJoin.Round,
            Color = Colors.GainForeground.ToSKColor(),
            IsAntialias = true
        });
        private static readonly Lazy<SKPaint> GREEN_STICK_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.StrokeAndFill,
            StrokeCap = SKStrokeCap.Square,
            StrokeJoin = SKStrokeJoin.Miter,
            Color = Colors.GainForeground.ToSKColor(),
            IsAntialias = true
        });
        private static readonly Lazy<SKPaint> RED_STICK_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.StrokeAndFill,
            StrokeCap = SKStrokeCap.Square,
            StrokeJoin = SKStrokeJoin.Miter,
            Color = Colors.LoseForeground.ToSKColor(),
            IsAntialias = true
        });
        private static readonly Lazy<SKPaint> YELLOW_STICK_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            StrokeCap = SKStrokeCap.Square,
            StrokeJoin = SKStrokeJoin.Miter,
            Color = Colors.UnchangedForeground.ToSKColor(),
            IsAntialias = true
        });
        private static readonly Lazy<SKPaint> PRICE_LABELS_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Fill,
            IsAntialias = true,
            Typeface = SKTypeface.FromFamilyName((OnPlatform<string>)App.Current.Resources["DINProBold"], SKFontStyleWeight.Black, SKFontStyleWidth.Expanded, SKFontStyleSlant.Upright),
            TextEncoding = SKTextEncoding.Utf32
        });
        public static readonly Lazy<SKPaint> PRICE_PLACEHOLDERS_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Fill,
            Color = new SKColor(0x00, 0x00, 0x00, 0x88)
        });
        public static readonly Lazy<SKPaint> HIGH_LOW_PLACEHOLDERS_PAINT_LAZY = new Lazy<SKPaint>(() => new SKPaint
        {
            Style = SKPaintStyle.Fill
        });

        private static readonly Lazy<SKColor> GAIN_BACKGROUND_LAZY = new Lazy<SKColor>(() => Colors.GainBackground.ToSKColor());
        private static readonly Lazy<SKColor> LOSE_BACKGROUND_LAZY = new Lazy<SKColor>(() => Colors.LoseBackground.ToSKColor());
        private static readonly Lazy<SKColor> UNCHANGED_BACKGROUND_LAZY = new Lazy<SKColor>(() => Colors.UnchangedBackground.ToSKColor());
        private static readonly Lazy<SKColor> GAIN_DARK_BACKGROUND_LAZY = new Lazy<SKColor>(() => Colors.GainDarkBackground.ToSKColor());
        private static readonly Lazy<SKColor> LOSE_DARK_BACKGROUND_LAZY = new Lazy<SKColor>(() => Colors.LoseDarkBackground.ToSKColor());
        private static readonly Lazy<SKColor> UNCHANGED_DARK_BACKGROUND_LAZY = new Lazy<SKColor>(() => Colors.UnchangedDarkBackground.ToSKColor());
        private static readonly Lazy<SKColor> GAIN_FOREGROUND_LAZY = new Lazy<SKColor>(() => Colors.GainForeground.ToSKColor());
        private static readonly Lazy<SKColor> LOSE_FOREGROUND_LAZY = new Lazy<SKColor>(() => Colors.LoseForeground.ToSKColor());
        private static readonly Lazy<SKColor> UNCHANGED_FOREGROUND_LAZY = new Lazy<SKColor>(() => Colors.UnchangedForeground.ToSKColor());

        private readonly SKPath _path = new SKPath();

        public static BindableProperty StockProperty = BindableProperty.Create("Stock", typeof(Stock), typeof(CandlestickChart));
        public Stock Stock
        {
            get => (Stock)GetValue(StockProperty);
            set => SetValue(StockProperty, value);
        }

        public Stock Stock2 { get; set; }

        public static BindableProperty StockChartProperty = BindableProperty.Create("StockChart", typeof(StockChart), typeof(CandlestickChart));
        public StockChart StockChart
        {
            get => (StockChart)GetValue(StockChartProperty);
            set => SetValue(StockChartProperty, value);
        }

        public static BindableProperty TimeUnitProperty = BindableProperty.Create("TimeUnit", typeof(StockChartTimeUnit), typeof(CandlestickChart), defaultValue: StockChartTimeUnit.Day);
        public StockChartTimeUnit TimeUnit
        {
            get => (StockChartTimeUnit)GetValue(TimeUnitProperty);
            set => SetValue(TimeUnitProperty, value);
        }

        public static BindableProperty PaddingProperty = BindableProperty.Create("Padding", typeof(Thickness), typeof(CandlestickChart), defaultValue: new Thickness(0.0));
        public Thickness Padding
        {
            get => (Thickness)GetValue(PaddingProperty);
            set => SetValue(PaddingProperty, value);
        }

        public static BindableProperty VolumeBarsHeightProperty = BindableProperty.Create("VolumeChartHeight", typeof(double), typeof(CandlestickChart), defaultValue: 40.0);
        public double VolumeBarsHeight
        {
            get => (double)GetValue(VolumeBarsHeightProperty);
            set => SetValue(VolumeBarsHeightProperty, value);
        }

        private float _scale;
        private int _columns;
        private decimal _minPrice;
        private decimal _maxPrice;
        private decimal _maxVolume;

        public CandlestickChart()
        {
            Store.ChartMode.Subscribe(chartMode =>
            {
                InvalidateSurface();
            });
        }

        protected override void OnPaintSurface(SKPaintSurfaceEventArgs e)
        {
            base.OnPaintSurface(e);

            if (Width - Padding.Left - Padding.Right <= 0 ||
                Height - Padding.Top - Padding.Bottom - VolumeBarsHeight <= 0 ||
                StockChart == null ||
                StockChart.Count == 0)
            {
                return;
            }

            SKImageInfo imageInfo = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;

            _scale = imageInfo.Width / (float)Width;

            // Calculate min & max
            _minPrice = StockChart.Min(tuple => tuple.Low);
            _maxPrice = StockChart.Max(tuple => tuple.High);
            _maxVolume = StockChart.Max(tuple => tuple.Volume);

            // Include previous price in min & max calculation
            //if (Stock != null && Stock.Previous.HasValue)
            //{
            //    if (Stock.Previous.Value < _minPrice)
            //    {
            //        _minPrice = Stock.Previous.Value;
            //    }

            //    if (Stock.Previous.Value > _maxPrice)
            //    {
            //        _maxPrice = Stock.Previous.Value;
            //    }
            //}

            // Expand chart if it had zero price range
            if (_minPrice == _maxPrice)
            {
                IncrementPrice(ref _maxPrice);
                _minPrice -= (_maxPrice - _minPrice);
            }

            // Center chart at previous price (optional. comment to disable centering)
            //if (Stock != null && Stock.Previous.HasValue)
            //{
            //    if (_maxPrice - Stock.Previous.Value > Stock.Previous.Value - _minPrice)
            //    {
            //        //_minPrice = Stock.Previous.Value - (_maxPrice - Stock.Previous.Value);
            //        _minPrice = Stock.Previous.Value * 2 - _maxPrice;
            //    }
            //    else
            //    {
            //        //_maxPrice = Stock.Previous.Value + (Stock.Previous.Value - _minPrice);
            //        _maxPrice = Stock.Previous.Value * 2 - _minPrice;
            //    }
            //}

            _columns = Math.Max(MIN_COLUMNS, StockChart.Count);

            canvas.Clear();

            (decimal minPrice, decimal priceInterval) = CalculatePriceInterval();

            // Layers, bottom to top order
            DrawPriceLines(canvas, minPrice, priceInterval);
            DrawPreviousLine(canvas);
            DrawTimes(canvas);
            DrawVolumeBars(canvas);
            DrawMaxVolume(canvas);
            switch (Store.ChartMode.GetState())
            {
                case ChartMode.Line:
                    DrawAreaChart(canvas);
                    break;
                case ChartMode.Candlestick:
                    DrawCandlesticks(canvas);
                    break;
            }
            DrawPrices(canvas, minPrice, priceInterval, colorCoded: true);
            DrawLowHigh(canvas);
        }

        private void DrawPriceLines(SKCanvas canvas, decimal minPrice, decimal priceInterval)
        {

            // Short circuit singularity or flipped interval
            if (priceInterval <= 0m)
            {
                return;
            }

            for (decimal price = minPrice; price - _maxPrice < priceInterval; price += priceInterval)
            {
                ToY(price, out float priceY);
                PRICE_LINE_PAINT_LAZY.Value.StrokeWidth = STROKE_WIDTH * _scale;
                canvas.DrawLine(0, priceY, (float)Width * _scale, priceY, PRICE_LINE_PAINT_LAZY.Value);
            }
        }

        private void DrawPreviousLine(SKCanvas canvas)
        {
            if (Stock2 != null && Stock2.Previous.HasValue)
            {
                ToY(Stock2.Previous.Value, out float prevY);
                PREV_LINE_PAINT_LAZY.Value.StrokeWidth = 2.0f * STROKE_WIDTH * _scale;
                canvas.DrawLine(0, prevY, (float)Width * _scale, prevY, PREV_LINE_PAINT_LAZY.Value);
            }
        }

        private void DrawVolumeBars(SKCanvas canvas)
        {
            /*
            float bottom = (float)(Height - Padding.Bottom) * _scale;
            for (int col = 0; col < StockChart.Count; col++)
            {
                ToCandlestickX(col, out float left, out _, out float right);
                ToVolumeY(StockChart[col].Volume, out float y);

                SKColor color;
                if (StockChart[col].Close > StockChart[col].Open)
                {
                    color = GAIN_BACKGROUND_LAZY.Value;
                }
                else if (StockChart[col].Close < StockChart[col].Open)
                {
                    color = LOSE_BACKGROUND_LAZY.Value;
                }
                else
                {
                    color = UNCHANGED_FOREGROUND_LAZY.Value;
                }

                VOL_BARS_PAINT_LAZY.Value.StrokeWidth = STROKE_WIDTH * _scale;
                VOL_BARS_PAINT_LAZY.Value.Color = color;
                canvas.DrawRect(new SKRect(left, y, right, bottom), VOL_BARS_PAINT_LAZY.Value);
            }
            */

            if (Stock2 != null && Stock2.Previous.HasValue)
            {
                float bottom = (float)(Height - Padding.Bottom) * _scale;
                for (int col = 0; col < StockChart.Count; col++)
                {
                    ToCandlestickX(col, out float left, out _, out float right);
                    ToVolumeY(StockChart[col].Volume, out float y);

                    SKColor color;
                    decimal close = StockChart[col].Close;
                    decimal prevClose = col == 0 ? Stock2.Previous.Value : StockChart[col - 1].Close;
                    if (close < prevClose)
                    {
                        color = LOSE_BACKGROUND_LAZY.Value;
                    }
                    else
                    {
                        color = GAIN_BACKGROUND_LAZY.Value;
                    }

                    VOL_BARS_PAINT_LAZY.Value.StrokeWidth = STROKE_WIDTH * _scale;
                    VOL_BARS_PAINT_LAZY.Value.Color = color;
                    canvas.DrawRect(new SKRect(left, y, right, bottom), VOL_BARS_PAINT_LAZY.Value);
                }
            }
        }

        private void DrawTimes(SKCanvas canvas)
        {
            TimeSpan? prev = null;
            TIME_LABELS_PAINT_LAZY.Value.TextSize = 11f * _scale;
            TIME_TICKS_PAINT_LAZY.Value.StrokeWidth = STROKE_WIDTH * _scale;
            for (int col = 0; col < StockChart.Count; col++)
            {
                TimeSpan time = StockChart[col].Time.TimeOfDay;
                if (prev == null ||
                    (time.Subtract(prev.Value) >= TimeSpan.FromHours(1) && time.Minutes == 0) ||
                    (time.Subtract(prev.Value) >= TimeSpan.FromMinutes(90) && time.Minutes == 30))
                {

                    ToX(col, out float x);
                    ToX(col - 1, out float xPrev);
                    float xTick = (x + xPrev) / 2f;

                    // Draw tick
                    canvas.DrawLine(
                        p0: new SKPoint(xTick, 0.0f),
                        p1: new SKPoint(xTick, (float)(Height - Padding.Bottom + 5) * _scale),
                        paint: TIME_TICKS_PAINT_LAZY.Value
                    );

                    string text;
                    if (time.Minutes != 0)
                    {
                        text = $"{time.Hours}:{time.Minutes:00}";
                    }
                    else
                    {
                        text = $"{time.Hours}";
                    }
                    if (prev.HasValue && time.Subtract(prev.Value) > TimeSpan.FromMinutes(90))
                    {
                        TimeSpan splitTime = prev.Value;
                        if (splitTime.Minutes <= 30)
                        {
                            splitTime = splitTime.Add(TimeSpan.FromMinutes(30 - splitTime.Minutes));
                            text = $"{splitTime.Hours}:{splitTime.Minutes:00}/{text}";
                        }
                        else
                        {
                            splitTime = splitTime.Add(TimeSpan.FromMinutes(60 - splitTime.Minutes));
                            text = $"{splitTime.Hours}/{text}";
                        }
                    }

                    // Measure text
                    float width = TIME_LABELS_PAINT_LAZY.Value.MeasureText(text);
                    float left = xTick - width / 2f;
                    if (left + width > Width * _scale - Padding.Right * _scale)
                    {
                        left = (float)Width * _scale - (float)Padding.Right * _scale - width;
                    }

                    // Draw text
                    canvas.DrawText(text, new SKPoint(left, (float)Height * _scale), TIME_LABELS_PAINT_LAZY.Value);
                    prev = time;
                }
            }
        }

        private void DrawMaxVolume(SKCanvas canvas)
        {

            // Short circuit if chart is empty
            if (StockChart.Count == 0)
            {
                return;
            }

            int midCol = Math.Max(StockChart.Count, MIN_COLUMNS) / 2;

            // Find best max point
            int bestCol = 0;
            for (int col = 0; col < StockChart.Count; col++)
            {
                if (StockChart[col].Volume > StockChart[bestCol].Volume)
                {
                    bestCol = col;
                }
                else if (StockChart[col].Volume == StockChart[bestCol].Volume)
                {
                    if (Math.Abs(col - midCol) < Math.Abs(bestCol - midCol))
                    {
                        bestCol = col;
                    }
                }
            }

            // Draw max vol
            decimal max = StockChart[bestCol].Volume;
            ToCandlestickX(bestCol, out _, out float center, out _);
            ToVolumeY(max, out float y);
            MAX_VOL_PAINT_LAZY.Value.TextSize = 11f * _scale;
            string text = $"{max:N0}";

            // Measure text
            float width = MAX_VOL_PAINT_LAZY.Value.MeasureText(text);
            float left = center - width / 2f;
            if (left < Padding.Left * _scale)
            {
                left = (float)Padding.Left * _scale;
            }

            if (left + width > Width * _scale - Padding.Right * _scale)
            {
                left = (float)Width * _scale - (float)Padding.Right * _scale - width;
            }

            // Draw text
            canvas.DrawText(text, new SKPoint(left, y - 5.0f * _scale), MAX_VOL_PAINT_LAZY.Value);
        }

        private void DrawAreaChart(SKCanvas canvas)
        {
            float x, y;

            // Draw area
            if (Stock2 != null && Stock2.Previous.HasValue)
            {
                _path.Rewind();
                ToX(0, out x);
                ToY(Stock2.Previous.Value, out y);
                _path.MoveTo(new SKPoint(x, y));
                for (int col = 0; col < StockChart.Count; col++)
                {
                    ToX(col, out x);
                    ToY(StockChart[col].Close, out y);
                    _path.LineTo(new SKPoint(x, y));
                }
                if (StockChart.Count > 0)
                {
                    ToX(StockChart.Count - 1, out x);
                    ToY(Stock2.Previous.Value, out y);
                    _path.LineTo(new SKPoint(x, y));
                }
                _path.Close();
                if (Stock2 != null)
                {
                    if (Stock2.OHLC.HasValue &&
                    Stock2.Previous.HasValue &&
                    Stock2.Previous.Value != 0)
                    {
                        if (Stock2.OHLC.Value.Close >= Stock2.Previous.Value)
                        {
                            canvas.DrawPath(_path, AREA_FILL_PAINT_GAIN_LAZY.Value);
                        }
                        else
                        {
                            canvas.DrawPath(_path, AREA_FILL_PAINT_LOSE_LAZY.Value);
                        }
                    }
                    else
                    {
                        canvas.DrawPath(_path, AREA_FILL_PAINT_GAIN_LAZY.Value);
                    }
                }
            }

            if (StockChart.Count > 0)
            {
                _path.Rewind();
                ToX(0, out x);
                ToY(StockChart[0].Close, out y);
                _path.MoveTo(new SKPoint(x, y));
                for (int col = 1; col < StockChart.Count; col++)
                {
                    ToX(col, out x);
                    ToY(StockChart[col].Close, out y);
                    _path.LineTo(new SKPoint(x, y));
                }

                if (Stock2 != null)
                {
                    if (Stock2.OHLC.HasValue &&
                    Stock2.Previous.HasValue &&
                    Stock2.Previous.Value != 0)
                    {
                        if (Stock2.OHLC.Value.Close >= Stock2.Previous.Value)
                        {
                            AREA_STROKE_PAINT_GAIN_LAZY.Value.StrokeWidth = STROKE_WIDTH * _scale;
                            canvas.DrawPath(_path, AREA_STROKE_PAINT_GAIN_LAZY.Value);
                        }
                        else
                        {
                            AREA_STROKE_PAINT_LOSE_LAZY.Value.StrokeWidth = STROKE_WIDTH * _scale;
                            canvas.DrawPath(_path, AREA_STROKE_PAINT_LOSE_LAZY.Value);
                        }
                    }
                    else
                    {
                        AREA_STROKE_PAINT_GAIN_LAZY.Value.StrokeWidth = STROKE_WIDTH * _scale;
                        canvas.DrawPath(_path, AREA_STROKE_PAINT_GAIN_LAZY.Value);
                    }
                }
            }
        }

        private void DrawCandlesticks(SKCanvas canvas)
        {
            GREEN_STICK_PAINT_LAZY.Value.StrokeWidth = STROKE_WIDTH * _scale;
            RED_STICK_PAINT_LAZY.Value.StrokeWidth = STROKE_WIDTH * _scale;
            YELLOW_STICK_PAINT_LAZY.Value.StrokeWidth = STROKE_WIDTH * _scale;
            for (int col = 0; col < StockChart.Count; col++)
            {
                ToCandlestickX(col, out float left, out float middle, out float right);
                ToY(StockChart[col], out float openY, out float highY, out float lowY, out float closeY);

                if (StockChart[col].Close > StockChart[col].Open)
                {
                    // Green stick
                    canvas.DrawRect(new SKRect(left, closeY, right, openY), GREEN_STICK_PAINT_LAZY.Value);
                    canvas.DrawLine(middle, highY, middle, closeY, GREEN_STICK_PAINT_LAZY.Value);
                    canvas.DrawLine(middle, openY, middle, lowY, GREEN_STICK_PAINT_LAZY.Value);
                }
                else if (StockChart[col].Close < StockChart[col].Open)
                {
                    // Red stick
                    canvas.DrawRect(new SKRect(left, openY, right, closeY), RED_STICK_PAINT_LAZY.Value);
                    canvas.DrawLine(middle, highY, middle, lowY, RED_STICK_PAINT_LAZY.Value);
                }
                else
                {
                    // Yellow stick
                    canvas.DrawLine(left, openY, right, closeY, YELLOW_STICK_PAINT_LAZY.Value);
                    canvas.DrawLine(middle, highY, middle, lowY, YELLOW_STICK_PAINT_LAZY.Value);
                }
            }
        }

        private void DrawPrices(SKCanvas canvas, decimal minPrice, decimal priceInterval, bool colorCoded = false)
        {

            // Short circuit singularity or flipped interval
            if (priceInterval <= 0m)
            {
                return;
            }

            PRICE_LABELS_PAINT_LAZY.Value.TextSize = 11f * _scale;

            for (decimal price = minPrice; price - _maxPrice < priceInterval; price += priceInterval)
            {
                ToY(price, out float priceY);
                string text = $"{price:N0}";

                // Color coding
                SKColor color;
                if (colorCoded && Stock2 != null && Stock2.Previous.HasValue)
                {
                    if (price > Stock2.Previous.Value)
                    {
                        color = GAIN_FOREGROUND_LAZY.Value;
                    }
                    else if (price < Stock2.Previous.Value)
                    {
                        color = LOSE_FOREGROUND_LAZY.Value;
                    }
                    else
                    {
                        color = UNCHANGED_FOREGROUND_LAZY.Value;
                    }
                }
                else
                {
                    color = SKColors.White;
                }

                PRICE_LABELS_PAINT_LAZY.Value.Color = color;
                // Measure text
                float width = PRICE_LABELS_PAINT_LAZY.Value.MeasureText(text);

                // Draw placeholder
                canvas.DrawRect(
                    x: 2.0f * _scale,
                    y: priceY - 10.0f * _scale,
                    w: width + 6.0f * _scale,
                    h: 18.0f * _scale,
                    paint: PRICE_PLACEHOLDERS_PAINT_LAZY.Value
                );

                // Draw text
                canvas.DrawText(text, new SKPoint(5.0f * _scale, priceY + 3.0f * _scale), PRICE_LABELS_PAINT_LAZY.Value);
            }
        }

        public void DrawLowHigh(SKCanvas canvas)
        {

            // Short circuit if chart is empty
            if (StockChart.Count == 0)
            {
                return;
            }

            int midCol = Math.Max(StockChart.Count, MIN_COLUMNS) / 2;

            {
                // Find best low point
                int bestCol = 0;
                for (int col = 0; col < StockChart.Count; col++)
                {
                    if (StockChart[col].Low < StockChart[bestCol].Low)
                    {
                        bestCol = col;
                    }
                    else if (StockChart[col].Low == StockChart[bestCol].Low)
                    {
                        if (Math.Abs(col - midCol) < Math.Abs(bestCol - midCol))
                        {
                            bestCol = col;
                        }
                    }
                }

                // Draw low
                decimal low = StockChart[bestCol].Low;
                ToX(bestCol, out float x);
                ToY(low, out float y);
                SKColor placeholderColor;
                SKColor textColor;
                if (Stock2 != null && Stock2.Previous.HasValue)
                {
                    if (low > Stock2.Previous.Value)
                    {
                        placeholderColor = GAIN_DARK_BACKGROUND_LAZY.Value;
                        textColor = GAIN_FOREGROUND_LAZY.Value;
                    }
                    else if (low < Stock2.Previous.Value)
                    {
                        placeholderColor = LOSE_DARK_BACKGROUND_LAZY.Value;
                        textColor = LOSE_FOREGROUND_LAZY.Value;
                    }
                    else
                    {
                        placeholderColor = UNCHANGED_DARK_BACKGROUND_LAZY.Value;
                        textColor = UNCHANGED_FOREGROUND_LAZY.Value;
                    }
                }
                else
                {
                    placeholderColor = GAIN_BACKGROUND_LAZY.Value;
                    textColor = SKColors.White;
                }
                PRICE_LABELS_PAINT_LAZY.Value.Color = textColor;
                string text = $"L: {low:N0}";

                // Measure text
                float width = PRICE_LABELS_PAINT_LAZY.Value.MeasureText(text);
                float left = x - width / 2f - 3.0f * _scale;
                if (left < Padding.Left * _scale)
                {
                    left = (float)Padding.Left * _scale;
                }

                if (left + width + 6.0f * _scale > Width * _scale - Padding.Right * _scale)
                {
                    left = (float)Width * _scale - (float)Padding.Right * _scale - width - 6.0f * _scale;
                }

                // Draw placeholder
                HIGH_LOW_PLACEHOLDERS_PAINT_LAZY.Value.Color = placeholderColor;
                canvas.DrawRect(
                    x: left,
                    y: y + 5f * _scale,
                    w: width + 6.0f * _scale,
                    h: 18f * _scale,
                    paint: HIGH_LOW_PLACEHOLDERS_PAINT_LAZY.Value
                );

                // Draw text
                canvas.DrawText(text, new SKPoint(left + 3.0f * _scale, y + 17.0f * _scale), PRICE_LABELS_PAINT_LAZY.Value);
            }

            {
                // Find best high point
                int bestCol = 0;
                for (int col = 0; col < StockChart.Count; col++)
                {
                    if (StockChart[col].High > StockChart[bestCol].High)
                    {
                        bestCol = col;
                    }
                    else if (StockChart[col].High == StockChart[bestCol].High)
                    {
                        if (Math.Abs(col - midCol) < Math.Abs(bestCol - midCol))
                        {
                            bestCol = col;
                        }
                    }
                }

                // Draw high
                decimal high = StockChart[bestCol].High;
                ToX(bestCol, out float x);
                ToY(high, out float y);
                SKColor placeholderColor;
                SKColor textColor;
                if (Stock2 != null && Stock2.Previous.HasValue)
                {
                    if (high > Stock2.Previous.Value)
                    {
                        placeholderColor = GAIN_DARK_BACKGROUND_LAZY.Value;
                        textColor = GAIN_FOREGROUND_LAZY.Value;
                    }
                    else if (high < Stock2.Previous.Value)
                    {
                        placeholderColor = LOSE_DARK_BACKGROUND_LAZY.Value;
                        textColor = LOSE_FOREGROUND_LAZY.Value;
                    }
                    else
                    {
                        placeholderColor = UNCHANGED_DARK_BACKGROUND_LAZY.Value;
                        textColor = UNCHANGED_FOREGROUND_LAZY.Value;
                    }
                }
                else
                {
                    placeholderColor = GAIN_BACKGROUND_LAZY.Value;
                    textColor = SKColors.White;
                }
                PRICE_LABELS_PAINT_LAZY.Value.Color = textColor;
                string text = $"H: {high:N0}";

                // Measure text
                float width = PRICE_LABELS_PAINT_LAZY.Value.MeasureText(text);
                float left = x - width / 2f - 3.0f * _scale;
                if (left < Padding.Left * _scale)
                {
                    left = (float)Padding.Left * _scale;
                }

                if (left + width + 6.0f * _scale > Width * _scale - Padding.Right * _scale)
                {
                    left = (float)Width * _scale - (float)Padding.Right * _scale - width - 6.0f * _scale;
                }

                // Draw placeholder
                HIGH_LOW_PLACEHOLDERS_PAINT_LAZY.Value.Color = placeholderColor;
                canvas.DrawRect(
                    x: left,
                    y: y - 23f * _scale,
                    w: width + 6.0f * _scale,
                    h: 18f * _scale,
                    paint: HIGH_LOW_PLACEHOLDERS_PAINT_LAZY.Value
                );

                // Draw text
                canvas.DrawText(text, new SKPoint(left + 3.0f * _scale, y - 11.0f * _scale), PRICE_LABELS_PAINT_LAZY.Value);
            }
        }

        private void IncrementPrice(ref decimal price)
        {
            if (price < 200)
            {
                price++;
            }
            else if (price < 500)
            {
                price += 2;
            }
            else if (price < 2000)
            {
                price += 5;
            }
            else if (price < 5000)
            {
                price += 10;
            }
            else
            {
                price += 25;
            }
        }

        private (decimal minPrice, decimal priceInterval) CalculatePriceInterval()
        {

            // Calculate minInterval
            decimal minInterval;
            if (_minPrice < 200)
            {
                minInterval = 1m;
            }
            else if (_minPrice < 500)
            {
                minInterval = 2m;
            }
            else if (_minPrice < 2000)
            {
                minInterval = 5m;
            }
            else if (_minPrice < 5000)
            {
                minInterval = 10m;
            }
            else
            {
                minInterval = 25m;
            }
            decimal priceInterval = minInterval;

        // Calculate visual spacing
        calculateSpacing:
            ToY(_minPrice, out float minPriceY);
            ToY(_minPrice + priceInterval, out float firstIntervalY);
            float spacing = minPriceY - firstIntervalY;

            // Short circuit singularity
            if (spacing == 0f)
            {
                return (_minPrice, 0m);
            }

            // Check if spacing was too dense
            if (spacing < 40f * _scale)
            {
                priceInterval += minInterval;
                goto calculateSpacing;
            }

            // Align Previous Price to price interval
            if (Stock2 != null && Stock2.Previous.HasValue)
            {
                decimal minPrice = Stock2.Previous.Value - Math.Ceiling((Stock2.Previous.Value - _minPrice) / priceInterval) * priceInterval;
                return (minPrice, priceInterval);
            }
            else
            {
                return (_minPrice, priceInterval);
            }
        }

        private void ToCandlestickX(int i, out float left, out float middle, out float right)
        {

            // project column to X-axis
            left = (i + (1f - CANDLE_WIDTH_RATIO) / 2f) / _columns * (float)Width * _scale;
            middle = (i + 0.5f) / _columns * (float)Width * _scale;
            right = (i + 0.5f + CANDLE_WIDTH_RATIO / 2f) / _columns * (float)Width * _scale;

            // add left and right padding
            left = (float)Padding.Left * _scale + left * (float)(Width - Padding.Right - Padding.Left) / (float)Width;
            middle = (float)Padding.Left * _scale + middle * (float)(Width - Padding.Right - Padding.Left) / (float)Width;
            right = (float)Padding.Left * _scale + right * (float)(Width - Padding.Right - Padding.Left) / (float)Width;
        }

        private void ToX(int i, out float x)
        {

            // project column to X-axis
            x = (i + 0.5f) / _columns * (float)Width * _scale;

            // add left and right padding
            x = (float)Padding.Left * _scale + x * (float)(Width - Padding.Right - Padding.Left) / (float)Width;
        }

        private void ToY(StockChartTuple tuple, out float openY, out float highY, out float lowY, out float closeY)
        {
            ToY(tuple.Open, out openY);
            ToY(tuple.High, out highY);
            ToY(tuple.Low, out lowY);
            ToY(tuple.Close, out closeY);
        }

        private void ToY(decimal price, out float y)
        {

            // project price to Y-axis
            y = _minPrice == _maxPrice ? (float)Height / 2f * _scale : (float)(_maxPrice - price) / (float)(_maxPrice - _minPrice) * (float)Height * _scale;

            // add top and bottom padding, and volume chart padding
            y = (float)Padding.Top * _scale + y * (float)(Height - Padding.Top - Padding.Bottom - VolumeBarsHeight - 40) / (float)Height;
        }

        private void ToVolumeY(decimal volume, out float y)
        {

            // project volume to Y-axis
            y = (float)Height * _scale - (float)Padding.Bottom * _scale - (float)VolumeBarsHeight * _scale + (float)(_maxVolume - volume) / (float)_maxVolume * (float)(VolumeBarsHeight) * _scale;
        }
    }
}
