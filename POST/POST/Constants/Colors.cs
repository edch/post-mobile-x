﻿using Xamarin.Forms;

namespace POST.Constants
{

    public static class Colors
    {

        public const int RUNNING_TRADE_GRADIENT_LEVELS = 15;

        public static readonly Color Transparent = Color.FromRgba(0x00, 0x00, 0x00, 0x00);
        public static readonly Color Primary = Color.FromRgb(0x1b, 0x1e, 0x26);
        public static readonly Color Accent = Color.FromRgb(0xf8, 0x8c, 0x2a);

        public static readonly Color UndeterminedBackground = Color.FromUint(0xff04060A);
        public static readonly Color GainBackground = Color.FromUint(0xff15b26e);
        public static readonly Color LoseBackground = Color.FromUint(0xfff23932);
        public static readonly Color UnchangedBackground = Color.FromUint(0xff04060A);

        public static readonly Color GainDarkBackground = Color.FromUint(0xff0a5937);
        public static readonly Color LoseDarkBackground = Color.FromUint(0xff791c19);
        public static readonly Color UnchangedDarkBackground = Color.FromUint(0xff79730a);

        public static readonly Color UndeterminedForeground = Color.FromUint(0xffffffff);
        public static readonly Color GainForeground = Color.FromUint(0xff1be28c);
        public static readonly Color LoseForeground = Color.FromUint(0xffff413a);
        public static readonly Color UnchangedForeground = Color.FromUint(0xfff2e715);

        public static readonly Color MatchedOrderStatus = Color.FromUint(0xff35adf0);
        public static readonly Color OpenOrderStatus = Color.FromUint(0xff15b26e);
        public static readonly Color CancelledOrderStatus = Color.FromUint(0xfff23932);
        public static readonly Color RejectedOrderStatus = Color.FromUint(0xffffffff);

        public static readonly Color D_BrokerType = Color.FromUint(0xffa75beb);
        public static readonly Color F_BrokerType = Color.FromUint(0xffffff00);

        public static readonly Color TitleViewDefaultColor = Color.FromUint(0xffffffff);
        public static readonly Color FieldNameColor = Color.FromUint(0xff707070);
        public static readonly Color FieldValueDefaultColor = Color.FromUint(0xffffffff);

        public static readonly Color PageBackgroundDialogColor = Color.FromUint(0xffffffff);
        public static readonly Color PageTextDialogColor = Color.FromUint(0xff000000);

        public static readonly Color ButtonBackgroundOnWhiteColor = Color.FromUint(0xfff2f2f2);

        public static readonly Color PageBackgroundColor = Color.FromUint(0xff090D16);

        public static readonly Color AllCashButtonBackgroundColor = Color.FromUint(0xff75ceeb);
        public static readonly Color AllLimitButtonBackgroundColor = Color.FromUint(0xffFCAEA3);

        public static readonly Color NetralSortColor = Color.FromUint(0x20FFFFFF);
        public static readonly Color AscDescSortColor = Color.FromUint(0xff0c9ce7);

        public static readonly Color PageTitleBackgroundDialogColor_WhiteTheme = Color.FromUint(0xfff4f4f4);
        public static readonly Color PageBackgroundDialogColor_WhiteTheme = Color.FromUint(0xfff9f9f9);
        public static readonly Color UndeterminedForeground_WhiteTheme = Color.FromUint(0xff000000);
        public static readonly Color GainForeground_WhiteTheme = Color.FromUint(0xff176a17);
        public static readonly Color LoseForeground_WhiteTheme = Color.FromUint(0xfff61a1a);
        public static readonly Color UnchangedForeground_WhiteTheme = Color.FromUint(0xffa863c8);
        public static readonly Color AskBackground_WhiteTheme = Color.FromUint(0x190a663a);
        public static readonly Color BidBackground_WhiteTheme = Color.FromUint(0x19f23932);
        public static readonly Color Accent_WhiteTheme = Color.FromUint(0xffdb6d0a);
        public static readonly Color NetralSortColor_WhiteTheme = Color.FromUint(0x20000000);

        public static readonly Color Add1Color = Color.FromUint(0xffe6e6e6);
        public static readonly Color Add2Color = Color.FromUint(0xffdedede);
        public static readonly Color Add3Color = Color.FromUint(0xffd6d6d6);
        public static readonly Color Add4Color = Color.FromUint(0xffcfcfcf);
        public static readonly Color Add5Color = Color.FromUint(0xffc7c7c7);
        public static readonly Color Add6Color = Color.FromUint(0xffbfbfbf);

        public static readonly Color ListViewSeparatorColor = Color.FromUint(0xff1f222b);

        public static readonly Color DangerColor = Color.FromUint(0xFFF23932);

        public static readonly Color SuccessColor = Color.FromUint(0xFF15B26E);

        public static readonly Color ControlBackgroundColor = Color.FromUint(0x55707070);

        public static readonly Color RunningTradeColor1 = Color.FromUint(0xff87dcfc);
        public static readonly Color RunningTradeColor2 = Color.FromUint(0xffdad8a7);

        public static readonly Color SpecialNotationForegroundColor = Color.FromUint(0xFFe2a64b);
        public static readonly Color SpecialNotationBackgroundColor = Color.FromUint(0xFF090D16);

        //public static readonly Color _1BrokerType = Color.FromUint(0xff00ffff);
        //public static readonly Color _2BrokerType = Color.FromUint(0xff7fff00);
        //public static readonly Color _3BrokerType = Color.FromUint(0xffff00ff);        
    }
}
