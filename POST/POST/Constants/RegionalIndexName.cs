﻿namespace POST.Constants
{

    public static class RegionalIndexName
    {

        public static string For(string code)
        {
            switch (code)
            {
                case "AORD": return "All Ordinaries";
                case "BSE30": return "S&P BSE SENSEX";
                case "CAC40": return "CAC40";
                case "DJ.FUT": return "Dow Jones Industrial Average Futures";
                case "DJIA": return "Dow Jones Industrial Average";
                case "FTSE": return "FTSE 100 Index";
                case "GDAXI": return "DAX Index";
                case "HSI": return "Hang Seng Index";
                case "HSI.FUT": return "Hang Seng Index Futures";
                case "KLSE": return "Kuala Lumpur Stock Exchange";
                case "KOSPI": return "Korea Composite Stock Price Index";
                case "KOSPI200": return "KOSPI 200 Index";
                case "N225": return "Nikkei 225";
                case "NDXI": return "Nasdaq Composite Index";
                case "NDXI.FUT": return "Nasdaq Composite Index Futures";
                case "NYSE": return "New York Stock Exchange";
                case "PSE": return "Philippine Stock Exchange";
                case "SHCI": return "Stock Holding Corporation of India";
                case "SPX": return "S&P 500 Index";
                case "SPX.FUT": return "S&P 500 Index Futures";
                case "STI": return "Straits Times Index";
                case "TWSE": return "Taiwan Stock Exchange Weighted Index";
                default: return string.Empty;
            }
        }
    }
}
