﻿namespace POST.Constants
{

    public static class CurrencyFlag
    {

        private const string AUD = "AUD";
        private const string CAD = "CAD";
        private const string CNY = "CNY";
        private const string EUR = "EUR";
        private const string GBP = "GBP";
        private const string HKD = "HKD";
        private const string JPY = "JPY";
        private const string KRW = "KRW";
        private const string MYR = "MYR";
        private const string NZD = "NZD";
        private const string PHP = "PHP";
        private const string SAR = "SAR";
        private const string SGD = "SGD";
        private const string THB = "THB";
        private const string TWD = "TWD";
        private const string USD = "USD";
        private const string FLAG_AUD = "🇦🇺";
        private const string FLAG_CAD = "🇨🇦";
        private const string FLAG_CNY = "🇨🇳";
        private const string FLAG_EUR = "🇪🇺";
        private const string FLAG_GBP = "🇬🇧";
        private const string FLAG_HKD = "🇭🇰";
        private const string FLAG_JPY = "🇯🇵";
        private const string FLAG_KRW = "🇰🇷";
        private const string FLAG_MYR = "🇲🇾";
        private const string FLAG_NZD = "🇳🇿";
        private const string FLAG_PHP = "🇵🇭";
        private const string FLAG_SAR = "🇸🇦";
        private const string FLAG_SGD = "🇸🇬";
        private const string FLAG_THB = "🇹🇭";
        private const string FLAG_TWD = "🇹🇼";
        private const string FLAG_USD = "🇺🇸";

        /// <summary>
        /// Returns emoji. Don't edit return value by hand.
        /// </summary>
        public static string For(string currencyCode)
        {
            switch (currencyCode)
            {
                case AUD: return FLAG_AUD;
                case CAD: return FLAG_CAD;
                case CNY: return FLAG_CNY;
                case EUR: return FLAG_EUR;
                case GBP: return FLAG_GBP;
                case HKD: return FLAG_HKD;
                case JPY: return FLAG_JPY;
                case KRW: return FLAG_KRW;
                case MYR: return FLAG_MYR;
                case NZD: return FLAG_NZD;
                case PHP: return FLAG_PHP;
                case SAR: return FLAG_SAR;
                case SGD: return FLAG_SGD;
                case THB: return FLAG_THB;
                case TWD: return FLAG_TWD;
                case USD: return FLAG_USD;
                default: return string.Empty;
            }
        }
    }
}
