﻿using POST.Models;

namespace POST.Constants
{

    /// <summary>
    /// FontAwesome Pro reference can be found in https://fontawesome.com/icons?d=gallery
    /// 1. Click on icon you want to use
    /// 2. Copy Unicode, eg. f0d8
    /// 3. Use a unicode const here using \u escape sequence. eg. \uf0d8
    /// </summary>
    public static class Glyphs
    {

        public const string Undetermined = "";
        public const string Gaining = "\uf0de";
        public const string Losing = "\uf0dd";
        public const string Unchanged = "";
        public const string BackgroundSort = "\uf0dc";

        public const string SortAlphabeticAsc = "\uf15d";
        public const string SortAlphabeticDesc = "\uf15e";
        public const string SortNumericAsc = "\uf162";
        public const string SortNumericDesc = "\uf163";

        public static string ForSortKey(SortKey sortKey)
        {
            switch (sortKey)
            {
                case SortKey.CodeAsc:
                    return SortAlphabeticAsc;
                case SortKey.CodeDesc:
                    return SortAlphabeticDesc;
                case SortKey.ChangePercentAsc:
                    return SortNumericAsc;
                case SortKey.ChangePercentDesc:
                    return SortNumericDesc;
                default:
                    return "";
            }
        }

        /// <summary>
        /// https://fontawesome.com/icons/bars?style=regular
        /// </summary>
        public const string RunningTrade = "\uf0c9";

        /// <summary>
        /// https://fontawesome.com/icons/industry?style=regular
        /// </summary>
        public const string IndicesAndCurrencies = "\uf275";

        /// <summary>
        /// https://fontawesome.com/icons/chart-line?style=regular
        /// </summary>
        public const string Market = "\uf201";

        public const string Stock = "\uf080";

        public const string Chart = "\uf1fe";

        /// <summary>
        /// https://fontawesome.com/icons/newspaper?style=regular
        /// </summary>
        public const string News = "\uf1ea";

        /// <summary>
        /// https://fontawesome.com/icons/clipboard-check?style=regular
        /// </summary>
        public const string Orders = "\uf46c";

        public const string Rights = "\uf0d6";

        /// <summary>
        /// https://fontawesome.com/icons/folder-open?style=regular
        /// </summary>
        public const string Portfolio = "\uf07c";

        /// <summary>
        /// https://fontawesome.com/icons/user-cog?style=regular
        /// </summary>
        public const string Alerts = "\uf0f3";

        public const string Gear = "\uf013";

        public const string Inbox = "\uf01c";

        public const string BrokerAccDist = "\uf21b";

        /// <summary>
        /// https://fontawesome.com/icons/question-circle?style=regular
        /// </summary>
        public const string Support = "\uf059";

        /// <summary>
        /// https://fontawesome.com/icons/info-circle?style=regular
        /// </summary>
        public const string About = "\uf05a";

        /// <summary>
        /// https://fontawesome.com/icons/sign-out-alt?style=regular
        /// </summary>
        public const string Logout = "\uf2f5";

        /// <summary>
        /// https://fontawesome.com/icons/share?style=regular
        /// </summary>
        public const string TellAFriend = "\uf064";
    }
}
