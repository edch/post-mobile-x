﻿namespace POST.Constants
{

    public static class StockNameFallback
    {

        public static string For(string stockCode)
        {
            switch (stockCode)
            {
                case "ASII": return "Astra International Tbk.";
                default: return string.Empty;
            }
        }
    }
}
