﻿using Firebase.Iid;
using POST.DataSources;
using POST.Pages;
using POST.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POST
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        public static NavigationPage NavigationPage;
        private readonly ICollection<IDisposable> _disposables = new List<IDisposable>();
        private CancellationTokenSource _cancellationTokenSourceIsLogin = new CancellationTokenSource(TimeSpan.FromSeconds(10));
        private readonly CancellationTokenSource _cancellationTokenSource;

        private bool _timerRunning = false;
        private int isLoginCounter = 0;
        private bool isForcedLogout = false;

        public MainPage()
        {
            InitializeComponent();

            SidebarMenu.ListView.ItemSelected += ListView_ItemSelectedAsync;
            NavigationPage = Detail as NavigationPage;

            DataFeedClient.StateManager.Current.BrokerInitRequestAsync();

            // Listen to session availability change
            _disposables.Add(Store.AthenaSession
                .Select(session => session != null)
                .DistinctUntilChanged()
                .Subscribe(isLoggedIn =>
                {
                    if (!isLoggedIn)
                    {
                        Application.Current.MainPage = new LoginPage(isForcedLogout);
                    }
                })
            );

            _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(20));
            RegisterDeviceToAlertServer(_cancellationTokenSource.Token);
        }

        public async void CheckIsLoginAsync()
        {
            _cancellationTokenSourceIsLogin.Cancel();
            _cancellationTokenSourceIsLogin = new CancellationTokenSource(TimeSpan.FromSeconds(10));
            int result = await IsLoginAsync(_cancellationTokenSourceIsLogin.Token);
            if (result == -1)
            {
                isLoginCounter = 0;
                isForcedLogout = true;
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                if (Store.AthenaSession.GetState() != null)
                {
                    await Store.AthenaSession.GetState().Client.LogoutAsync(new CancellationTokenSource(TimeSpan.FromSeconds(5)).Token);
                }

                Store.TradingAccounts.Clear();
                Store.AthenaSession.ClearSession();
            }
            else if (result == 1)
            {
                isLoginCounter = 0;
            }
            else if (result == 2 || result == 3 || result == 4)
            {
                isLoginCounter++;
            }

            if (isLoginCounter >= 3)
            {
                isForcedLogout = true;
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count != 0)
                {
                    await PopupNavigation.Instance.PopAllAsync();
                }

                if (Store.AthenaSession.GetState() != null)
                {
                    await Store.AthenaSession.GetState().Client.LogoutAsync(new CancellationTokenSource(TimeSpan.FromSeconds(5)).Token);
                }

                Store.TradingAccounts.Clear();
                Store.AthenaSession.ClearSession();
            }
        }

        private async Task<int> IsLoginAsync(CancellationToken cancellationToken)
        {
            try
            {
                string result = await Store.AthenaSession.GetState().Client.IsLoginAsync(cancellationToken);
                if (!string.IsNullOrWhiteSpace(result))
                {
                    if (result == "false")
                    {
                        return -1;
                    }
                    else
                    {
                        return 1;
                    }
                }
                else
                {
                    return 2;
                }
            }
            catch (OperationCanceledException)
            {
                return 3;
            }
            catch (Exception)
            {
                return 4;
            }
            finally
            {
            }
        }

        ~MainPage()
        {
            if (!_cancellationTokenSource.IsCancellationRequested)
            {
                _cancellationTokenSource.Cancel();
            }
            foreach (IDisposable disposable in _disposables)
            {
                disposable.Dispose();
            }
            _disposables.Clear();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            IsVisible = true;
            if (!_timerRunning)
            {
                Device.StartTimer(TimeSpan.FromSeconds(20), () =>
                {
                    if (IsVisible)
                    {
                        CheckIsLoginAsync();
                    }
                    if (!IsVisible)
                    {
                        _timerRunning = false;
                    }
                    return IsVisible;
                });
                _timerRunning = true;
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            IsVisible = false;
            if (!_cancellationTokenSourceIsLogin.IsCancellationRequested)
            {
                _cancellationTokenSourceIsLogin.Cancel();
            }
        }

        private async void RegisterDeviceToAlertServer(CancellationToken cancellationToken)
        {

            try
            {
                string strResponse = await AlertClient.Default.RegisterDeviceAsync(Store.LoginForm.GetState().UserId, FirebaseInstanceId.Instance.Token, DependencyService.Get<IDeviceProperties>().PackageName, cancellationToken);
                //FirebaseMessaging.Instance.SubscribeToTopic("U-STPG032468");
            }
            catch
            {
                //DependencyService.Get<IToastMessage>().Show("Request failed.");
            }

        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (NavigationPage.Navigation.NavigationStack.Count <= 1)
                {
                    bool AnswerYes = await Application.Current.MainPage.DisplayAlert("Confirmation", "Exit Application?", "Yes", "No");
                    if (AnswerYes)
                    {
                        if (Device.RuntimePlatform == Device.Android)
                        {
                            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
                        }
                        else if (Device.RuntimePlatform == Device.iOS)
                        {
                            Thread.CurrentThread.Abort();
                        }
                    }
                }
                else
                {
                    base.OnBackButtonPressed();
                }
            }
            );
            return true;
        }

        private async void ListView_ItemSelectedAsync(object sender, SelectedItemChangedEventArgs e)
        {
            Models.MenuItem item = e.SelectedItem as Models.MenuItem;
            if (item == null)
            {
                return;
            }

            if (item.TargetType == typeof(Models.MenuItem.LogoutMenu))
            {
                if (Store.AthenaSession.GetState() != null)
                {
                    await Store.AthenaSession.GetState().Client.LogoutAsync(new CancellationTokenSource(TimeSpan.FromSeconds(5)).Token);
                }

                Store.TradingAccounts.Clear();
                Store.AthenaSession.ClearSession();
                return;
            }

            if (item.TargetType == typeof(IShareDialog))
            {
                await DependencyService.Get<IShareDialog>().ShowAsync("Tell a Friend", "POST Mobile", "https://play.google.com/apps/testing/id.co.post.postmobile4");
                return;
            }

            try
            {
                Page page = (Page)Activator.CreateInstance(item.TargetType);
                page.Title = item.Title;

                NavigationPage = new NavigationPage(page);
                Detail = NavigationPage;
                IsPresented = false;

                SidebarMenu.ListView.SelectedItem = null;
            }
            catch (TargetInvocationException exc)
            {
                throw exc.InnerException;
            }
        }

        public void GotoAlertPage()
        {
            try
            {
                AlertsPage alertsPage = new AlertsPage
                {
                    Title = "Alerts"
                };
                alertsPage.GotoAlertNotificationPage();
                NavigationPage = new NavigationPage(alertsPage);
                Detail = NavigationPage;
                IsPresented = false;

                SidebarMenu.ListView.SelectedItem = null;
            }
            catch (TargetInvocationException exc)
            {
                throw exc.InnerException;
            }
        }

        public void GotoTradeDonePage()
        {
            try
            {
                OrdersPage ordersPage = new OrdersPage
                {
                    Title = "Orders"
                };
                ordersPage.GotoTradeDonePage();
                NavigationPage = new NavigationPage(ordersPage);
                Detail = NavigationPage;
                IsPresented = false;

                SidebarMenu.ListView.SelectedItem = null;
            }
            catch (TargetInvocationException exc)
            {
                throw exc.InnerException;
            }
        }
    }
}