﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using POST.Services;

namespace POST.Droid.Services {
	public class ShareDialog : IShareDialog {

		private readonly Context _context;
		public ShareDialog() {
			_context = Application.Context;
		}

		public Task ShowAsync(string title, string message, string url) {
			if (title == null) throw new ArgumentNullException(nameof(title));
			if (url == null) throw new ArgumentNullException(nameof(url));

			Intent intent = new Intent(Intent.ActionSend);
			intent.SetType("text/plain");
			intent.PutExtra(Intent.ExtraSubject, title);
			intent.PutExtra(Intent.ExtraText, $"{message} {url}");

			Intent chooserIntent = Intent.CreateChooser(intent, title);
			chooserIntent.SetFlags(ActivityFlags.ClearTop);
			chooserIntent.SetFlags(ActivityFlags.NewTask);
			_context.StartActivity(chooserIntent);

			return Task.FromResult(0);
		}
	}
}