﻿
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Common;
using Android.OS;
using Android.Util;
using Plugin.Fingerprint;
using POST.Droid.Services;
using Xamarin.Forms;

namespace POST.Droid
{

    [Activity(Theme = "@style/MainTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait, LaunchMode = LaunchMode.SingleTask)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        private static readonly string TAG = "MainActivity";

        internal static readonly string TRADEPRICECHANNEL_ID = "TradePriceChannel";
        internal static readonly int NOTIFICATION_ID = 100;

        protected override void OnCreate(Bundle bundle)
        {
            Forms.SetFlags("UseLegacyRenderers");

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            Rg.Plugins.Popup.Popup.Init(this);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            Xamarin.Forms.DependencyService.Register<ShareDialog>();
            Xamarin.Forms.DependencyService.Register<DeviceProperties>();
            Xamarin.Forms.DependencyService.Register<ToastMessage>();

            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            CrossFingerprint.SetCurrentActivityResolver(() => this);

            //AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            //{
            //    System.Exception ex = (System.Exception)args.ExceptionObject;
            //    Console.WriteLine(ex.Message);
            //};

            IsPlayServicesAvailable();

            CreateNotificationChannel();

            LoadApplication(new App());
        }

        protected override void OnNewIntent(Intent intent)
        {
            if (intent.Extras != null)
            {
                foreach (string key in intent.Extras.KeySet())
                {
                    string value = intent.Extras.GetString(key);
                    Log.Debug(TAG, "Key: {0} Value: {1}", key, value);
                }

                //if (intent.GetStringExtra("message") == "price")
                //{
                //    ((App)App.Current).GotoAlertPage();                    
                //}
                //else if (intent.GetStringExtra("message") == "trade")
                //{
                //    ((App)App.Current).GotoTradeDonePage();                    
                //}
            }
        }

        public override void OnBackPressed()
        {
            if (Rg.Plugins.Popup.Popup.SendBackPressed(base.OnBackPressed))
            {
            }
        }

        public bool IsPlayServicesAvailable()
        {
            ToastMessage toastMessage = new ToastMessage();

            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                {
                    toastMessage.Show(GoogleApiAvailability.Instance.GetErrorString(resultCode));
                }
                else
                {
                    toastMessage.Show("This device is not supported for Alert Notification. Please Install Google Play Service on Playstore.");
                }
                return false;
            }
            else
            {
                //toastMessage.Show("Google Play Services is available.");                
                return true;
            }
        }

        private void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }

            NotificationChannel tradePricechannel = new NotificationChannel(TRADEPRICECHANNEL_ID,
                                                  "Trade Price Notifications",
                                                  NotificationImportance.High)
            {
                Description = "Channel for trade, price"
            };

            NotificationManager notificationManager = (NotificationManager)GetSystemService(Android.Content.Context.NotificationService);
            notificationManager.CreateNotificationChannel(tradePricechannel);
        }
    }
}

