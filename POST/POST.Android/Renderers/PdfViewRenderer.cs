﻿using Android.Content;
using POST.Controls;
using POST.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(PdfView), typeof(PdfViewRenderer))]
namespace POST.Droid.Renderers
{

    public class PdfViewRenderer : WebViewRenderer
    {

        public PdfViewRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<WebView> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                PdfView pdfView = Element as PdfView;
                Control.Settings.AllowUniversalAccessFromFileURLs = true;
                Control.LoadUrl($"file:///android_asset/pdfjs/web/viewer.html?file={pdfView.Uri}");
            }
        }
    }
}