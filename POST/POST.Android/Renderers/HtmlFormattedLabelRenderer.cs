﻿using Android.Content;
using Android.Text;
using Android.Text.Util;
using Android.Widget;
using POST.Controls;
using POST.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(HtmlFormattedLabel), typeof(HtmlFormattedLabelRenderer))]

namespace POST.Droid.Renderers
{
    public class HtmlFormattedLabelRenderer : LabelRenderer
    {
        public HtmlFormattedLabelRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (Control != null && Element != null && !string.IsNullOrWhiteSpace(Element.Text))
            {
                Linkify.AddLinks(Control, MatchOptions.All);
                Control?.SetText(Html.FromHtml(Element.Text), TextView.BufferType.Spannable);
            }
        }

        //protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    base.OnElementPropertyChanged(sender, e);

        //    if (e.PropertyName == Label.TextProperty.PropertyName)
        //    {
        //        if (Control != null && Element != null && !string.IsNullOrWhiteSpace(Element.Text))
        //        {
        //            Linkify.AddLinks(Control, MatchOptions.All);
        //            Control?.SetText(Html.FromHtml(Element.Text), TextView.BufferType.Spannable);
        //        }
        //    }
        //}
    }
}