﻿using Android.Content;
using Java.Lang;
using POST.Controls;
using POST.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(KernedLabel), typeof(KernedLabelRenderer))]
namespace POST.Droid.Renderers
{

    public class KernedLabelRenderer : LabelRenderer
    {

        public KernedLabelRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (Element is KernedLabel kernedLabel)
            {
                try
                {
                    Control.LetterSpacing = kernedLabel.LetterSpacing;
                    UpdateLayout();
                }
                catch (NoSuchMethodError) { }
            }
        }
    }
}