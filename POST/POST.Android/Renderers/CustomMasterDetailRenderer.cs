﻿using Android.Content;
using Android.Util;
using Android.Views;

[assembly: Xamarin.Forms.ExportRenderer(typeof(Xamarin.Forms.MasterDetailPage), typeof(POST.Droid.Renderers.CustomMasterDetailPageRenderer))]
namespace POST.Droid.Renderers
{
    internal class CustomMasterDetailPageRenderer : Xamarin.Forms.Platform.Android.AppCompat.MasterDetailPageRenderer
    {

        private bool firstTime = true;

        public CustomMasterDetailPageRenderer(Context context) : base(context) { }

        public override void AddView(View child)
        {
            if (firstTime)
            {
                firstTime = false;
                base.AddView(child);
            }
            else
            {
                LayoutParams p = (LayoutParams)child.LayoutParameters;
                p.Width = (int)TypedValue.ApplyDimension(ComplexUnitType.Dip, 280f, Context.Resources.DisplayMetrics);
                base.AddView(child, p);
            }
        }
    }
}