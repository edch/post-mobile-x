﻿using Android.Content;
using Android.Graphics;

[assembly: Xamarin.Forms.ExportRenderer(typeof(Xamarin.Forms.NavigationPage), typeof(POST.Droid.Renderers.AndroidNavigationRenderer))]
namespace POST.Droid.Renderers
{
    public class AndroidNavigationRenderer : Xamarin.Forms.Platform.Android.AppCompat.NavigationPageRenderer
    {
        private Android.Support.V7.Widget.Toolbar toolbar;
        private static Typeface _fontAwesomeLight = null;
        public static Typeface GetFontAwesomeLight(Context context)
        {
            if (_fontAwesomeLight == null)
            {
                _fontAwesomeLight = Typeface.CreateFromAsset(context.ApplicationContext.Assets, "FontAwesomeProLight.otf");
            }

            return _fontAwesomeLight;
        }

        public AndroidNavigationRenderer(Context context) : base(context) { }

        public override void OnViewAdded(Android.Views.View child)
        {
            base.OnViewAdded(child);
            if (child.GetType() == typeof(Android.Support.V7.Widget.Toolbar))
            {
                toolbar = (Android.Support.V7.Widget.Toolbar)child;
                toolbar.ChildViewAdded += Toolbar_ChildViewAdded;
            }
        }

        private void Toolbar_ChildViewAdded(object sender, ChildViewAddedEventArgs e)
        {
            if (e.Child.GetType() == typeof(Android.Support.V7.Widget.ActionMenuView))
            {
                Android.Support.V7.Widget.ActionMenuView actionMenuView = (Android.Support.V7.Widget.ActionMenuView)e.Child;
                actionMenuView.ChildViewAdded += ActionMenuView_ChildViewAdded;

                // Maximum of 1 visible main icon in toolbar
                toolbar.ChildViewAdded -= Toolbar_ChildViewAdded;
            }
        }

        private void ActionMenuView_ChildViewAdded(object sender, ChildViewAddedEventArgs e)
        {
            if (e.Child.GetType() == typeof(Android.Support.V7.View.Menu.ActionMenuItemView))
            {
                Android.Support.V7.View.Menu.ActionMenuItemView actionMenuItemView = (Android.Support.V7.View.Menu.ActionMenuItemView)e.Child;
                actionMenuItemView.Typeface = Typeface.CreateFromAsset(Context.Assets, "FontAwesomeProLight.otf");
            }
        }
    }
}
