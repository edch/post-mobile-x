﻿using Android.App;
using Android.Widget;
using POST.Services;

namespace POST.Droid.Services
{
    public class ToastMessage : IToastMessage
    {
        private Toast toastMessage = null;

        public void Show(string message)
        {
            if (toastMessage != null)
            {
                toastMessage.Cancel();
            }
            toastMessage = Toast.MakeText(Application.Context, message, ToastLength.Long);
            toastMessage.Show();

        }
    }
}