﻿using Android.App;
using Android.Content;
using POST.Services;
using System;
using System.Threading.Tasks;

namespace POST.Droid.Services
{
    public class ShareDialog : IShareDialog
    {

        private readonly Context _context;
        public ShareDialog()
        {
            _context = Application.Context;
        }

        public Task ShowAsync(string title, string message, string url)
        {
            if (title == null)
            {
                throw new ArgumentNullException(nameof(title));
            }

            Intent intent = new Intent(Intent.ActionSend);
            intent.SetType("text/html");
            intent.PutExtra(Intent.ExtraSubject, title);
            intent.PutExtra(Intent.ExtraText, $"{message} {url}");

            Intent chooserIntent = Intent.CreateChooser(intent, title);
            chooserIntent.SetFlags(ActivityFlags.ClearTop);
            chooserIntent.SetFlags(ActivityFlags.NewTask);
            _context.StartActivity(chooserIntent);

            return Task.FromResult(0);
        }
    }
}