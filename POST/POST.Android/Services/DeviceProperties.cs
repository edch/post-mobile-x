﻿
using Android.App;
using Android.Content;
using Android.OS;
using Android.Telephony;
using POST.Services;

namespace POST.Droid.Services
{
    public class DeviceProperties : IDeviceProperties
    {
        public string OSVersion => Build.VERSION.Release;

        public string OS => "Android";

        public string DeviceType => Build.Brand + " " + Build.Model;

        public string DeviceId
        {
            get
            {
                return Android.Provider.Settings.Secure.GetString(Application.Context.ContentResolver, Android.Provider.Settings.Secure.AndroidId); ;
            }
        }

        public string PackageName => Application.Context.PackageName;

        public string VersionName => Application.Context.PackageManager.GetPackageInfo(Application.Context.PackageName, 0).VersionName;

        public string VersionCode => Application.Context.PackageManager.GetPackageInfo(Application.Context.PackageName, 0).LongVersionCode.ToString();

        public string Carrier
        {
            get
            {
                TelephonyManager manager = (TelephonyManager)Application.Context.GetSystemService(Context.TelephonyService);
                return manager.NetworkOperatorName.ToString();
            }
        }

    }
}