﻿using Android.App;
using Android.Content;
using Firebase.Iid;
using POST.DataSources;
using System;
using System.Threading;

namespace POST.Droid.Services
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class MyFirebaseIIDService : FirebaseInstanceIdService
    {
        public override void OnTokenRefresh()
        {
            try
            {
                string refreshedToken = FirebaseInstanceId.Instance.Token;
                RegisterDeviceToAlertServer(refreshedToken);
            }
            catch
            {

            }
        }


        private async void RegisterDeviceToAlertServer(string refreshedToken)
        {

            try
            {
                if (Store.LoginForm.GetState() != null)
                {
                    string strResponse = await AlertClient.Default.RegisterDeviceAsync(Store.LoginForm.GetState().UserId, refreshedToken, new DeviceProperties().PackageName, new CancellationTokenSource(TimeSpan.FromSeconds(20)).Token);
                }
            }
            catch
            {
            }

        }
    }
}