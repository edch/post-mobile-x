﻿using Android.App;
using Android.Content;
using Android.Support.V4.App;
using Firebase.Messaging;
using System.Collections.Generic;

namespace POST.Droid.Services
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        private const string TAG = "MyFirebaseMsgService";
        public override void OnMessageReceived(RemoteMessage message)
        {
            //Log.Debug(TAG, "CollapseKey: " + message.CollapseKey);
            //Log.Debug(TAG, "Data: " + message.Data);
            //Log.Debug(TAG, "MessageId: " + message.MessageId);
            //Log.Debug(TAG, "From: " + message.From);
            //Log.Debug(TAG, "To: " + message.To);
            //Log.Debug(TAG, "ToString: " + message.ToString());            
            string body = message.GetNotification().Body;
            //Log.Debug(TAG, "Notification Message Body: " + body);

            SendNotification(body, message.Data);
        }

        public void SendNotification(string messageBody, IDictionary<string, string> data)
        {
            Intent intent = new Intent(this, typeof(MainActivity));
            intent.SetFlags(ActivityFlags.NewTask);

            if (data != null)
            {
                foreach (string key in data.Keys)
                {
                    intent.PutExtra(key, data[key]);
                }
            }

            string strTitle = "";
            if (messageBody.Contains("Trade Done"))
            {
                strTitle = "Trade Done";
                intent.PutExtra("message", "trade");
            }
            else if (messageBody.Contains("is higher than") || messageBody.Contains("is lower than"))
            {
                strTitle = "Price Alert";
                intent.PutExtra("message", "price");
            }

            PendingIntent pendingIntent = PendingIntent.GetActivity(this,
                                                          MainActivity.NOTIFICATION_ID,
                                                          intent,
                                                          PendingIntentFlags.OneShot);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, MainActivity.TRADEPRICECHANNEL_ID)
                                      .SetSmallIcon(Resource.Drawable.logo)
                                      .SetContentTitle(strTitle)
                                      .SetContentIntent(pendingIntent)
                                      .SetContentText(messageBody)
                                      .SetAutoCancel(true)
                                      .SetPriority(4);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.From(this);
            notificationManager.Notify(MainActivity.NOTIFICATION_ID, notificationBuilder.Build());

            ////////////////////////////////////////////////////////////

            //Intent secondIntent = null;
            //secondIntent = new Intent(this, typeof(MainActivity));

            //string strTitle = "";
            //if (messageBody.Contains("Trade Done"))
            //{
            //    strTitle = "Trade Done";
            //    secondIntent.PutExtra("message", "trade");
            //}
            //else if (messageBody.Contains("is higher than") || messageBody.Contains("is lower than"))
            //{
            //    strTitle = "Price Alert";
            //    secondIntent.PutExtra("message", "price");
            //}

            //// Create a task stack builder to manage the back stack:
            //Android.App.TaskStackBuilder stackBuilder = Android.App.TaskStackBuilder.Create(this);

            //// Add all parents of SecondActivity to the stack:
            //stackBuilder.AddParentStack(Java.Lang.Class.FromType(typeof(MainActivity)));

            //// Push the intent that starts SecondActivity onto the stack:
            //stackBuilder.AddNextIntent(secondIntent);

            //// Obtain the PendingIntent for launching the task constructed by
            //// stackbuilder. The pending intent can be used only once (one shot):
            //const int pendingIntentId = 0;
            //PendingIntent pendingIntent =
            //    stackBuilder.GetPendingIntent(pendingIntentId, PendingIntentFlags.OneShot);

            //// Instantiate the builder and set notification elements, including
            //// the pending intent:
            //NotificationCompat.Builder builder = new NotificationCompat.Builder(this, MainActivity.CHANNEL_ID)
            //    .SetContentIntent(pendingIntent)
            //    .SetContentTitle(strTitle)
            //    .SetContentText(messageBody)
            //    .SetSmallIcon(Resource.Drawable.logo);

            //// Build the notification:
            //Notification notification = builder.Build();

            //// Get the notification manager:
            //NotificationManager notificationManager =
            //    GetSystemService(Context.NotificationService) as NotificationManager;

            //// Publish the notification:
            //const int notificationId = 0;
            //notificationManager.Notify(notificationId, notification);

        }
    }
}